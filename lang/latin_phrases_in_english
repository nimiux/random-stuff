Latin phrases and words used in English
*A fortiori* - adj. and adv. [literally, from the stronger]For a stronger reason; all the more.
*A posteriori* - adj. and adv. [literally, from the latter] inductive; relating to or derived by reasoning from observed facts.
*A priori* - adj. and adv. [literally, from the former] deductive; relating to or derived by reasoning from self-evident propositions; presupposed by experience; being without examination or analysis; presumptive; formed or conceived beforehand.
*Ad hominem* - adj. [literally, to the man] appealing to feelings or prejudices rather than intellect; marked by an attack on an opponent's character rather than by an answer to the contentions made.
*Ad infinitum* - adv. or adj. [literally, to the infinite] without end or limit.
*Ad nauseam* - adv. [literally, to sea-sickness] to a sickening or excessive degree.
*Camera obscura* - n. [literally, dark chamber] a darkened enclosure having an aperture usu. provided with a lens through which light from external objects enters to form an image of the objects on the opposite surface.
*Carpe diem* - n. [literally, pluck the day] the enjoyment of the pleasures of the moment without concern for the future.
*Casus belli* - n., sing. and pl. [literally, occasion of war] an event or action that justifies or allegedly justifies a war or conflict.
*Caveat* - n. [literally, let him beware] a warning enjoining one from certain acts or practices; an explanation to prevent misinterpretation; a legal warning to a judicial officer to suspend a proceeding until the opposition has a hearing.
*De facto* - adv. [literally, from the fact] in reality; actually.
*De jure* - adv. [literally, concerning the law] by right; of right.
*Dictum* - n., pl. dicta, also dictums [literally, a thing said] a noteworthy statement: as a: a formal pronouncement of a principle, proposition, or opinion b: an observation intended or regarded as authoritative; a judicial opinion on a point other than the precise issue involved in determining a case.
*Et alii* - [literally, and others] and others; abbreviated as /et al/.
*Et cetera* [literally, and the rest] and others esp. of the same kind: and so forth; abbreviated as etc.
*Ex parte* - adv. or adj. [literally, from part] on or from one side or party only--used of legal proceedings; from a one-sided or partisan point of view.
*Floruit* - n. [literally, he flourished] a period of flourishing (as of a person or movement).
*Habitat* - n. [literally, it inhabits] the place or environment where a plant or animal naturally or normally lives and grows; the typical place of residence of a person or a group; a housing for a controlled physical environment in which people can live under surrounding inhospitable conditions (as under the sea; the place where something is commonly found.
*In camera* - adv. [literally, in a chamber] in private: secretly.
*In loco parentis* - adv. [literally, in place of a parent] in the place of a parent. n. regulation or supervision by an administrative body (as at a university) acting /in loco parentis/.
*In medias res* - adv. [literally, into the midst of things] in or into the middle of a narrative or plot.
*Ipse dixit* - n. [literally, he himself said it] an assertion made but not proved.
*Ipso facto* - adv. [literally, by the fact itself] by that very fact or act; as an inevitable result.
*Lingua franca* - n., pl lingua francas or linguae francae [literally, Frankish language] a common language consisting of Italian mixed with French, Spanish, Greek, and Arabic that was formerly spoken in Mediterranean ports; any of various languages used as common or commercial tongues among peoples of diverse speech; something resembling a common language.
*Magna cum laude* - adv or adj [literally, with great praise] with great distinction .
*Magnum opus* - n. [literally, a great work] a great work, esp: the greatest achievement of an artist or writer.
*Memento* - n., pl -tos or -toes [literally, remember!, imperative of meminisse, to remember] something that serves to warn or remind.
*Memento mori* - n. [literally, remember that you must die] a reminder of mortality; esp: death's-head.
*Mirabile visu* - [literally, wonderful to see] wonderful to behold.
*Mirabile dictu* [literally, wonderful to say] wonderful to relate.
*Ne plus ultra* - n. [literally, (go) no more beyond] the highest point capable of being attained: acme; the most profound degree of a quality or state.
*Noli me tangere* - n. [literally, do not touch me; fr. Jesus' words to Mary Magdalene (Jn 20:17)] a warning against touching or interference.
*Nolo contendere* - n. [literally, I do not wish to contend] a plea in a criminal prosecution that without admitting guilt subjects the defendant to conviction but does not preclude denying the truth of the charges in a collateral proceeding.
*Non sequitur* - n. [literally, it does not follow] an inference that does not follow from the premises; specif: a fallacy resulting from a simple conversion of a universal affirmative proposition or from the transposition of a condition and its consequent; a statement (as a response) that does not follow logically from anything previously said.
*Nota bene* - [literally, mark well] used to call attention to something important.
*Pax* - n. esp, cap: a period of general stability in international affairs under the influence of a dominant military power--usu. used in combination with a latinized name (/Pax Americana, Pax Brittanica, Pax Romana)./
*Per capita* - adv. or adj. [literally, by heads] equally to each individual; per unit of population: by or for each person /per capita of any state in the union./
*Per diem* - adv. [literally, by the day] by the day; for each day; adj. based on use or service by the day: daily; paid by the day; n.  pl per diems a daily allowance; a daily fee.
*Persona grata* - adj. [literally, a pleasing person] personally acceptable or welcome.
*Persona non grata* - adj. [literally, a not-pleasing person] personally unacceptable or unwelcome.
*Post mortem* - [literally, after death] adj. occuring or done after death; pertaining to a post-mortem examination; n. a post-mortem examination, esp. an autopsy.
*Post partum* - [literally, after birth] adj. of or occuring in the period shortly after childbirth.
*Prima facie* - adv. [literally, at first appearance] at first view: on the first appearance; adj. true, valid, or sufficient at first impression: apparent ; self-evident; legally sufficient to establish a fact or a case unless disproved .
*Pro forma* -adj [literally, for form] made or carried out in a perfunctory manner or as a formality; provided in advance to prescribe form or describe items .
*Quod erat demonstrandum* - [literally, which had to be shown] which was to be proved.
*Rara avis* - n. pl. rara avises or rarae aves [literally, rare bird] rarity.
*Res ipsa loquitur* - [literally, the affair itself speaks] the affair speaks for itself.
*RIP* - [acronym of /requiescat in pace/] abbreviation, may he rest in peace, may she rest in peace; /requiescant in pace/ = may they rest in peace.
*Sine die* - adv. [literally, without a day] without any future date being designated (as for resumption): indefinitely .
*Sine qua non* - n., pl. sine qua nons; also sine quibus non [literally, without which not] something absolutely indispensable or essential.
*Sui generis* - adj. [literally, of its own kind] constituting a class alone: unique, peculiar.
*Summa cum laude* - adv. or adj. [literally, with highest praise] with highest distinction --compare /cum laude, magna cum laude/.
*Tabula rasa* - n., pl. /tabulae rasae/ [literally, smoothed or erased tablet] the mind in its hypothetical primary blank or empty state before receiving outside impressions; something existing in its original pristine state.
