$ cat ssh-rotate
#!/bin/dash
 
alias p=printf
 
usage () { p "%s - SSH key rotation
  -c             - Use ssh-copy-id to distribute SSH public keys of any type
  -d             - Use ssh-copy-id in dry-run mode
  -e             - Edit/copy existing ed25519 entry to a new key
  -h hostfile    - Target hosts [host port username]
  -k keyfile.pub - Deploy a specific public key
  -m             - Migrate id_ed25519 keypair - archive old, place new
  -r rounds      - Set rounds for auto-generated ed25519
  -w             - Wipe existing id_ed25519.pub from authorized_keys
" "$0"; }
 
err () { usage; p '\nerror:'; for x; do p ' %s' "$x"; done; p '\n'; exit; }
 
[ -z "$SSH_AUTH_SOCK" -o -z "$SSH_AGENT_PID" ] && err 'no agent'
 
set -eu; unset IFS  # http://redsymbol.net/articles/unofficial-bash-strict-mode/
copyid= dryrun= edit= hf= kprv= migrate= rounds=100 wipe= format=\
'=%s=%s=%d====================================================================='
 
while getopts cdeh:k:mr:uw arg
do case "$arg" in
        c) copyid=1                ;;
        d) dryrun=1                ;;
        e) edit=1                  ;;
        h) hf="$OPTARG"            ;;
        k) kprv="${OPTARG%[.]pub}" ;;
        m) migrate=1               ;;
        r) rounds="$OPTARG"        ;;
        u) usage; exit             ;;
        w) wipe=1                  ;;
   esac
done
 
[ "$wipe" -a -z "$kprv" ] && err 'You must specify a key to wipe'
[ -z "$hf"   ]  && err 'no hosts'
[ -z "$kprv" ]  && kprv=~/.ssh/id_ed25519_pre$(date +%Y%m%d); kpub="${kprv}.pub"
[ -f "$kprv" ]  || ssh-keygen -t ed25519 -o -a "$rounds" -f "$kprv"
[ -f "$kpub" ]  || err 'public key not found'                    # ssh-keygen -y
ssh-add "$kprv" || err 'key rejected by agent'
 
ktyp="$(sed 's/[ ].*$//' $kpub)"
 
nrip="$(sed 's_^[^ ]*[ ]__; s_[ ][^ ]*$__' "$kpub")"
[ ssh-ed25519 = "$ktyp" ] &&
  orip="$(sed 's_^[^ ]*[ ]__; s_[ ][^ ]*$__' ~/.ssh/id_ed25519.pub)"

chk255 () { [ ssh-ed25519 != "$ktyp" ] && err 'requires id_ed25519'; true; }
 
while read host port user <&9
do case "$host" in [#]*) continue;; esac

   [ -z "$host" -o -z "$port" -o -z "$user" ] && err "bad host file"
 
   printf "$format" "$user" "$host" "$port" | cut -c-80

   [ "$copyid" ] && ssh-copy-id    -i "$kpub" -p "$port" "$user@$host"
 
   [ "$dryrun" ] && ssh-copy-id -n -i "$kpub" -p "$port" "$user@$host"
 
   [ "$edit" ]   && { chk255
                      ssh -p "$port" "$user@$host" \
                        sed -i.rotate "'\_${orip}_{p;s__${nrip}_;}'" \
                          '~/.ssh/authorized_keys'; }

#  [ "$wipe" ]   && { chk255
#                     prmpt="$(ssh -p "$port" "$user@$host" sed -n \
#                       "'\_${orip}_p'" '~/.ssh/authorized_keys')"
#                     [ -z "$prmpt" ] && continue
#                     p '%s\n\n%s - Confirm key wipe (Y/n)? ' "$prmpt" "$host"
#                     read check
#                     [ n = "$check" ] && continue
#                     [ Y = "$check" ] &&
#                       ssh -p "$port" "$user@$host" \
#                         sed -i.rotate "'\_${orip}_d'" \
#                           '~/.ssh/authorized_keys' || err 'Wipe aborted'; }

done 9< "$hf"
 
if [ "$migrate" ]
then chk255
     mv -iv ~/.ssh/id_ed25519 ~/.ssh/id_ed25519_$(date +%Y%m%d)
     mv -iv ~/.ssh/id_ed25519.pub ~/.ssh/id_ed25519.pub_$(date +%Y%m%d)
     mv -iv "$kprv" ~/.ssh/id_ed25519
