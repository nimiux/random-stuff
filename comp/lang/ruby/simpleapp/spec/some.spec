require 'simpleapp/some'


describe Some do
    config.expect_with(:rspec) { |c| c.syntax = :should }
    context "when first created" do
        it "is empty" do
            some = Some.new
            some.should be_empty
        end
    end
end
    
