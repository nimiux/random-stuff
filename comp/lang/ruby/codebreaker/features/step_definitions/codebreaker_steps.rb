RSpec.configure do |config|
  config.expect_with :rspec do |c|
    c.syntax = [:should, :expect]
  end
end

class Output
  def messages
    @messages ||= []
  end
  def puts(message)
    messages << message
  end
end
def outputx
  @output ||= Output.new
end

Given(/^I am not yet playing$/) do
end

When(/^I start a new game$/) do
  game = Codebreaker::Game.new(outputx)
  game.start
end

Then(/^I should see "([^"]*)"$/) do |message|
  outputx.messages.should include(message)
end

