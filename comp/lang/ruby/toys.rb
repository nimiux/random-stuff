#!/usr/bin/env ruby

require 'map_by_method'
require 'ap'

kitty_toys =
    [:shape => 'sock', :fabric => 'cashmere'] +
    [:shape => 'mouse', :fabric => 'calico'] +
    [:shape => 'eggroll', :fabric => 'chenille']
kitty_toys.sort_by { |toy| toy[:fabric] }

#kitty_toys.sort_by { |toy| toy[:shape] }.each do |toy|
#      puts "Blixy has a #{ toy[:shape] } made of #{ toy[:fabric] }"
#end

def get(a)
    a[:shape]
end
ap kitty_toys.map get $_
