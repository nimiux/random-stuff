ruby -v
# Ruby Version Manager (rmv) to handle various versions of ruby in the same machine
gem
gem install rails
mkdir rails_projects
cd rails_projects
rails new first_app
cd first_app
vi Gemfile
bundle install
rails server
git config --global alias.co checkout
git config --global core.editor "vi"
git checkout -f
git remote add origin git@github.com:<username>/first app.git
git push -u origin master
git remote add origin git@github.com:railstutorial/first app.git
# git Branch
git checkout -b modify-README
Switched to a new branch 'modify-README'
git branch
master
$ git mv README.rdoc README.md
$ subl README.md
$ git commit -a -m "Improve the README file"
$ git checkout master
Switched to branch 'master'
$ git merge modify-README
$ git branch -d modify-README
# For illustration only; don't do this unless you
$ git checkout -b topic-branch
$ <really screw up the branch>
$ git add .
$ git commit -a -m "Major screw up"
$ git checkout master
$ git branch -D topic-branch
git push
heroku

