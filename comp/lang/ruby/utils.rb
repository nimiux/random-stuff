#!/usr/bin/env ruby

def to_binary program
    program.bytes.map { |byte| byte.to_s(2).rjust(8, '0') }
end

puts to_binary "puts 'Hello world!'"

def umop_apisdnfoo txt
    txt.tr(
        letters='ahbmfnjpdrutwqye',
        letters.reverse).reverse
end

#print umop_apisdnfoo gets
