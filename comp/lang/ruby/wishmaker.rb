#!/usr/bin/env ruby

def make_wish wish
    puts "Wiѕh granted: #{wish}"
end

class WishMaker
  def initialize energy, zone
    @energy = energy
    @zone = zone
  end
  def grant( wish )
    if wish.length > 15 or wish.include? ' '
      raise ArgumentError, "Bad wish."
    end
    if @energy.zero?
      raise Exception, "No energy left."
    end
    @energy -= 1
    make_wish( wish )
  end
end

wm = WishMaker.new 4, "east";
wm.grant "invisibility"
wm.grant "invisibility"
wm.grant "invisibility"
wm.grant "invisibility"
wm.grant "invisibility"
