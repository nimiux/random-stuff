
%{
#include <stdio.h>
#include "defs.h"
extern Object *new_object();
%}

%union {
  int   in;
  char *ch;
}

%token DRAW DEFINE                          /* verbs */
%token LINE BOX POLYGON CIRCLE ELLIPSE      /* shapes */
%token BLACK WHITE SOLID DOTTED FILL        /* attributes */
%token <ch> IDENTIFIER
%token <in> INTEGER

%start stats

%%

stats
  :
  | stats draw_stat
  | stats define_stat
  | stats error ';'	{ yyerrok; }
  ;

draw_stat
  : DRAW IDENTIFIER ';'
    { append_objlst(lookup($2)); }
  | DRAW object ';'
    { append_objlst(new_object(&anObject)); }
  | error IDENTIFIER ';'			/* missing DRAW */
    { append_objlst(lookup($2));  yyerrok; }
  | error object ';'				/* missing DRAW */
    { append_objlst(new_object(&anObject)); }
  | DRAW IDENTIFIER error			/* missing ';' */
    { append_objlst(lookup($2)); }
  | DRAW object error				/* missing ';' */
    { append_objlst(new_object(&anObject)); }
  | error object error				/* missing ';' */
    { append_objlst(new_object(&anObject)); }
  ;

define_stat
  : DEFINE IDENTIFIER '=' object ';'
    { install($2, new_object(&anObject)); }
  | error IDENTIFIER '=' object ';'		/* missing DEFINE */
    { install($2, new_object(&anObject)); }
  | error IDENTIFIER error object ';'		/* missing DEFINE & '=' */
    { install($2, new_object(&anObject)); }
  | error IDENTIFIER '=' object error		/* missing DEFINE & ';' */
    { install($2, new_object(&anObject)); }
  | error IDENTIFIER error object error		/* missing DEFINE, '=', ';' */
    { install($2, new_object(&anObject)); }
  | DEFINE error '=' object ';'		/* fix missing or wrong IDENTIFIER */
  | DEFINE error '=' object error	/* fix missing or wrong IDENTIFIER */
  | DEFINE IDENTIFIER error object ';'		/* insert missing '=' */
    { install($2, new_object(&anObject)); }
  | DEFINE IDENTIFIER '=' object error		/* missing ';' */
    { install($2, new_object(&anObject)); }
  | DEFINE IDENTIFIER error object error	/* insert missing '=' & ';' */
    { install($2, new_object(&anObject)); }
  ;

object
  : shape '(' params ')'
  | shape attrs '(' params ')'
  ;

shape
  : LINE    { anObject.shape = LINE; }
  | BOX     { anObject.shape = BOX;  }
  | POLYGON { anObject.shape = POLYGON; }
  | CIRCLE  { anObject.shape = CIRCLE; }
  | ELLIPSE { anObject.shape = ELLIPSE; }
  | error			/* fix wrong or missing shape */
  ;

attrs
  : attr
  | attrs attr
  ;

attr
  : style
  | color
  | filling
  | error	/* fix missing or wrong attribute */
  ;

style
  : SOLID  { anObject.style = SOLID; }
  | DOTTED { anObject.style = DOTTED; }
  ;

color
  : BLACK { anObject.color = BLACK; }
  | WHITE { anObject.color = WHITE; }
  ;

filling
  : FILL BLACK { anObject.fill = BLACK; }
  | FILL WHITE { anObject.fill = WHITE; }
  | FILL error { anObject.fill = BLACK; }	/* insert missing color */
  ;

params
  : point
  | params ',' point
  | params error point		/* insert missing ',' */
	{ yyerrok; }
  ;

point
  : INTEGER INTEGER
    { anObject.x_coord[anObject.npoints] = $1;
      anObject.y_coord[anObject.npoints++] = $2;
    }
  | error		/* ignore wrong or missing point */
  | INTEGER error
    { anObject.x_coord[anObject.npoints] = $1;
      anObject.y_coord[anObject.npoints++] = -32768;
    }
  ;
