
# line 3 "parse.y"
#include <stdio.h>
#include "defs.h"
extern Object *new_object();

# line 8 "parse.y"
typedef union  {
  int   in;
  char *ch;
} YYSTYPE;
#define YYSUNION /* %union occurred */
#define DRAW 257
#define DEFINE 258
#define LINE 259
#define BOX 260
#define POLYGON 261
#define CIRCLE 262
#define ELLIPSE 263
#define BLACK 264
#define WHITE 265
#define SOLID 266
#define DOTTED 267
#define FILL 268
#define IDENTIFIER 269
#define INTEGER 270
YYSTYPE yylval, yyval;
#define YYERRCODE 256
FILE *yytfilep;
char *yytfilen;
int yytflag = 0;
int svdprd[2];
char svdnams[2][2];

int yyexca[] = {
  -1, 1,
  0, -1,
  -2, 0,
  0,
};

#define YYNPROD 49
#define YYLAST 250

int yyact[] = {
       7,      16,      51,      50,      11,      12,      13,      14,
      15,      26,      67,      21,      20,      22,      17,      16,
      73,      49,      11,      12,      13,      14,      15,      55,
      66,      19,       4,       5,       6,      42,      77,      53,
      54,      64,      63,      71,      48,      64,      44,      47,
      28,      69,      61,      59,      40,      38,      24,      31,
      30,       9,      29,      27,      10,       3,       2,      18,
       1,       0,       0,       0,       0,       0,       0,       0,
       0,       0,       0,       0,      52,       0,       0,       0,
      45,      46,       0,       0,       0,       0,       0,       0,
       0,       0,       0,       0,       0,       0,       0,       0,
       0,       0,       0,      68,      56,      57,      58,       0,
       0,       0,       0,       0,       0,      75,      76,       0,
       0,       0,       0,       0,       0,       0,       0,       0,
       0,       0,       0,       0,       0,       0,       0,       0,
       0,       0,       0,       0,       0,       0,       0,       0,
       0,       0,       0,       0,       0,       0,       0,       0,
       0,       0,       0,       0,       0,       0,       0,       0,
       0,       0,       0,       0,       0,       0,       0,       0,
       0,       0,       0,       0,       0,       0,       0,       0,
       0,       0,       0,       0,       0,       0,       0,       0,
       0,       0,       0,       0,       0,       0,       0,       0,
       0,       0,       0,       0,       0,       0,       0,       0,
       0,       0,       0,       0,       0,       0,       0,       0,
       0,       0,       0,       0,       0,      16,       0,       0,
      11,      12,      13,      14,      15,       0,       0,       0,
      23,       0,       8,       0,       0,      74,       0,       0,
       0,       0,      32,       0,       0,       0,       0,       0,
      43,      32,      35,      36,      33,      34,      37,       0,
      72,      35,      36,      33,      34,      37,      70,      62,
      60,      41,      39,      25,       0,      65,       0,       0,
       0,      65,
};

int yypact[] = {
   -1000,    -230,   -1000,   -1000,     -59,    -255,    -244,   -1000,
     -48,     -13,     -31,   -1000,   -1000,   -1000,   -1000,   -1000,
   -1000,     -14,     -15,     -32,     -23,   -1000,    -241,    -241,
   -1000,   -1000,    -253,     -38,   -1000,   -1000,   -1000,   -1000,
   -1000,   -1000,   -1000,   -1000,   -1000,    -233,   -1000,   -1000,
   -1000,   -1000,    -241,    -241,    -241,     -16,     -17,      -7,
   -1000,    -246,   -1000,    -253,   -1000,   -1000,   -1000,   -1000,
     -18,     -24,     -43,   -1000,   -1000,   -1000,   -1000,   -1000,
    -253,    -253,   -1000,   -1000,     -11,   -1000,   -1000,   -1000,
   -1000,   -1000,   -1000,   -1000,   -1000,   -1000,
};

int yypgo[] = {
       0,      56,      54,      53,      49,      52,      39,      51,
      40,      50,      48,      47,      36,
};

int yyr1[] = {
       0,       1,       1,       1,       1,       2,       2,       2,
       2,       2,       2,       2,       3,       3,       3,       3,
       3,       3,       3,       3,       3,       3,       4,       4,
       5,       5,       5,       5,       5,       5,       7,       7,
       8,       8,       8,       8,       9,       9,      10,      10,
      11,      11,      11,       6,       6,       6,      12,      12,
      12,
};

int yyr2[] = {
       0,       0,       2,       2,       3,       3,       3,       3,
       3,       3,       3,       3,       5,       5,       5,       5,
       5,       5,       5,       5,       5,       5,       4,       5,
       1,       1,       1,       1,       1,       1,       1,       2,
       1,       1,       1,       1,       1,       1,       1,       1,
       2,       2,       2,       1,       3,       3,       2,       1,
       2,
};

int yychk[] = {
   -1000,      -1,      -2,      -3,     256,     257,     258,      59,
     269,      -4,      -5,     259,     260,     261,     262,     263,
     256,     269,      -4,     269,     256,      59,      61,     256,
      59,     256,      40,      -7,      -8,      -9,     -10,     -11,
     256,     266,     267,     264,     265,     268,      59,     256,
      59,     256,      61,     256,      61,      -4,      -4,      -6,
     -12,     270,     256,      40,      -8,     264,     265,     256,
      -4,      -4,      -4,      59,     256,      59,     256,      41,
      44,     256,     270,     256,      -6,      59,     256,      59,
     256,      59,     256,     -12,     -12,      41,
};

int yydef[] = {
       1,      -2,       2,       3,       0,       0,       0,       4,
       0,       0,       0,      24,      25,      26,      27,      28,
      29,       0,       0,       0,       0,       7,       0,       0,
       8,      11,       0,       0,      30,      32,      33,      34,
      35,      36,      37,      38,      39,       0,       5,       9,
       6,      10,       0,       0,       0,       0,       0,       0,
      43,       0,      47,       0,      31,      40,      41,      42,
       0,       0,       0,      13,      15,      14,      16,      22,
       0,       0,      46,      48,       0,      12,      20,      19,
      21,      17,      18,      44,      45,      23,
};

/* 
 * YACCPAR.C - PCYACC LR parser driver routine for use with improved errorlib
 *
 *	(c) Copyright 1989, Abraxas Software, Inc.
*/

#include "errorlib.h"

#define yyerrok		errfl = 0
#define yyclearin	if (token == 0) return 1; else token = -1
#define YYFLAG -1000
#define YYERROR goto yyerrlab
#define YYACCEPT return(0)
#define YYABORT return(1)

#ifndef YYMAXDEPTH
#define YYMAXDEPTH	100
#endif

YYSTYPE yyv[YYMAXDEPTH];
int token = -1; /* input token */
int errct = 0;  /* error count */
int errfl = 0;  /* error flag */


int yyparse(void)
{
     int yys[YYMAXDEPTH];
     int yyj, yym;
     YYSTYPE *yypvt;
     int yystate, *yyps, yyn;
     YYSTYPE *yypv;
     int *yyxi;

#ifdef YYDEBUG
     printf("state 0\n");
#endif
     yystate = 0;
     token = -1;
     errct = 0;
     errfl = 0;
     yyps = yys - 1;
     yypv = yyv - 1;

yystack:    /* put a state and value onto the stack */
     if ( ++yyps > yys + YYMAXDEPTH ) {
       yyerror( "yacc stack overflow", 0 );
       return(1);
     }
     *yyps = yystate;
     ++yypv;
     *yypv = yyval;

yynewstate:
     yyn = yypact[yystate];

     if ( yyn <= YYFLAG )
	  goto yydefault; /* simple state with only default action */

     if ( token < 0 ) {		/* no lookahead token? */
	  if ( (token = yylex()) < 0 )
	       token = 0;
     }
     yyn += token;
     if ( yyn < 0 || yyn >= YYLAST )	/* no entry within table? */
	  goto yydefault;	/* simple state with only default action */

     if ( yychk[yyn = yyact[yyn]] == token ) {	/* valid shift? */
#ifdef YYDEBUG
	  printf("shift on [%s] to state %d\n", yydisplay(token), yyn);
#endif
	  token = -1;		/* lookahead token is read */
	  yyval = yylval;
	  yystate = yyn;
	  if ( errfl > 0 )	/* still in error state? */
	       --errfl;
	  goto yystack;
     }

yydefault:	/* simple state with only default action */
     if ( (yyn = yydef[yystate]) == -2 ) {
	  if ( token < 0 ) {	/* no lookahead token? */
	       if ( (token = yylex()) < 0 )
		    token = 0;
	  }

          /* look through exception table */
          for ( yyxi = yyexca; *yyxi != -1 || yyxi[1] != yystate ; yyxi += 2 )
	       ;		/* VOID */
          while( *(yyxi += 2) >= 0 ) {
	       if ( *yyxi == token )
	            break;
          }
          if ( (yyn = yyxi[1]) < 0 ) {
#ifdef YYDEBUG
	       printf("accept\n");
#endif
	       return 0;   /* accept */
	  }
     }

     if ( yyn == 0 ) {		/* error? */
	  switch( errfl ) {
	       case 0:   /* brand new error */
		    yyerror( "syntax error", yydisplay( token ));
		    if ( (yyn = yypact[yystate]) > YYFLAG && yyn < YYLAST) {
			 register int x;

			 for ( x = (yyn > 0) ? yyn : 0; x < YYLAST; ++x ) {
			      if ( yychk[yyact[x]] == x - yyn &&
					x - yyn != YYERRCODE )
				   yyerror( 0, yydisplay( x - yyn ));
			 }
		    }
		    yyerror( 0, 0 );
yyerrlab:
		    ++errct;

	  case 1:
	  case 2: /* incompletely recovered error ... try again */
		    errfl = 3;

		    /* find a state where "error" is a legal shift action */
		    while ( yyps >= yys ) {
			 yyn = yypact[*yyps] + YYERRCODE;
			 if ( yyn >= 0 && yyn < YYLAST &&
					yychk[yyact[yyn]] == YYERRCODE ){
			      /* simulate a shift of "error" */
			      yystate = yyact[yyn];
#ifdef YYDEBUG
			      printf("shift on [error] to state %d\n",
				    yystate);
#endif
			      goto yystack;
			 }
		         yyn = yypact[*yyps];
			 /* the current yyps has no shift on "error",
				pop stack */
			 --yyps;
			 --yypv;
#ifdef YYDEBUG
			printf("error: pop state %d, uncovering state %d\n",
			      yyps[1], yyps[0]);
#endif
		    }

	    /* there is no state on the stack with an error shift ... abort */
yyabort:

#ifdef YYDEBUG
		    printf("abort\n");
#endif
		    return 1;

	       case 3:  /* no shift yet; clobber input char */
#ifdef YYDEBUG
		    printf("error: discard [%s]\n", yydisplay(token));
#endif
		    if ( token == 0 )	/* EOF? */
			 goto yyabort;		/* don't discard EOF, quit */
		    token = -1;
		    goto yynewstate;   /* try again in the same state */
	  }
     }

     /* reduction by production yyn */
     yyps -= yyr2[yyn];
     yypvt = yypv;
     yypv -= yyr2[yyn];
     yyval = yypv[1];
     yym = yyn;
     /* consult goto table to find next state */
     yyn = yyr1[yyn];
     yyj = yypgo[yyn] + *yyps + 1;
     if ( yyj >= YYLAST || yychk[yystate = yyact[yyj]] != -yyn )
	  yystate = yyact[yypgo[yyn]];
#ifdef YYDEBUG
     printf("reduce w/ rule %d, uncover state %d, go to state %d\n", yym,
	  *yyps, yystate);
#endif
     switch (yym) {
	  
case 4:
# line 27 "parse.y"
{ yyerrok; } break;
case 5:
# line 32 "parse.y"
{ append_objlst(lookup(yypvt[-1].ch)); } break;
case 6:
# line 34 "parse.y"
{ append_objlst(new_object(&anObject)); } break;
case 7:
# line 36 "parse.y"
{ append_objlst(lookup(yypvt[-1].ch));  yyerrok; } break;
case 8:
# line 38 "parse.y"
{ append_objlst(new_object(&anObject)); } break;
case 9:
# line 40 "parse.y"
{ append_objlst(lookup(yypvt[-1].ch)); } break;
case 10:
# line 42 "parse.y"
{ append_objlst(new_object(&anObject)); } break;
case 11:
# line 44 "parse.y"
{ append_objlst(new_object(&anObject)); } break;
case 12:
# line 49 "parse.y"
{ install(yypvt[-3].ch, new_object(&anObject)); } break;
case 13:
# line 51 "parse.y"
{ install(yypvt[-3].ch, new_object(&anObject)); } break;
case 14:
# line 53 "parse.y"
{ install(yypvt[-3].ch, new_object(&anObject)); } break;
case 15:
# line 55 "parse.y"
{ install(yypvt[-3].ch, new_object(&anObject)); } break;
case 16:
# line 57 "parse.y"
{ install(yypvt[-3].ch, new_object(&anObject)); } break;
case 19:
# line 61 "parse.y"
{ install(yypvt[-3].ch, new_object(&anObject)); } break;
case 20:
# line 63 "parse.y"
{ install(yypvt[-3].ch, new_object(&anObject)); } break;
case 21:
# line 65 "parse.y"
{ install(yypvt[-3].ch, new_object(&anObject)); } break;
case 24:
# line 74 "parse.y"
{ anObject.shape = LINE; } break;
case 25:
# line 75 "parse.y"
{ anObject.shape = BOX;  } break;
case 26:
# line 76 "parse.y"
{ anObject.shape = POLYGON; } break;
case 27:
# line 77 "parse.y"
{ anObject.shape = CIRCLE; } break;
case 28:
# line 78 "parse.y"
{ anObject.shape = ELLIPSE; } break;
case 36:
# line 95 "parse.y"
{ anObject.style = SOLID; } break;
case 37:
# line 96 "parse.y"
{ anObject.style = DOTTED; } break;
case 38:
# line 100 "parse.y"
{ anObject.color = BLACK; } break;
case 39:
# line 101 "parse.y"
{ anObject.color = WHITE; } break;
case 40:
# line 105 "parse.y"
{ anObject.fill = BLACK; } break;
case 41:
# line 106 "parse.y"
{ anObject.fill = WHITE; } break;
case 42:
# line 107 "parse.y"
{ anObject.fill = BLACK; } break;
case 45:
# line 114 "parse.y"
{ yyerrok; } break;
case 46:
# line 119 "parse.y"
{ anObject.x_coord[anObject.npoints] = yypvt[-1].in;
      anObject.y_coord[anObject.npoints++] = yypvt[-0].in;
    } break;
case 48:
# line 124 "parse.y"
{ anObject.x_coord[anObject.npoints] = yypvt[-1].in;
      anObject.y_coord[anObject.npoints++] = -32768;
    } break;
     }
     goto yystack;  /* stack new state and value */
}
