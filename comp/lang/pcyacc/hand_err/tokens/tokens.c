/*
 * TOKENS.C - convert a PCYACC C header file to list of quoted strings for
 *	use with improved error reporting (errorlib.c).
 *
 *	(c) Copyright 1989, Abraxas Software, Inc.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

static enum {no_change, capitalize, all_lower, all_upper} force = no_change;
static int under2blank = 0;		/* change all underscores to blanks */

static void tokens(FILE *, char *);


main(argc, argv)
char *argv[];
{
     char buffer[256];		/* filename buffer */
     char *infile;
     FILE *infp;
     static char *outfile = "";
     static const char usage[] =
	"usage: tokens [-b] [-c] [-l] [-o<outfile>] [-u] [<infile>]\n";

     fprintf(stderr, "Abraxas Software (R) TOKENS version - 1.0\n");
     fprintf(stderr, "Copyright (C) Abraxas Software, Inc. 1989.  "
     	"All rights reserved.\n");
     /* process options */
     while (--argc > 0 && **++argv == '-') {
          switch ((*argv)[1]) {
	       case 'b':	/* convert underscores to blanks */
	       case 'B':
		    under2blank = 1;
		    break;
	       case 'c':	/* Capitalize */
	       case 'C':
		    force = capitalize;
		    break;
	      case 'l':		/* all lowercase */
	      case 'L':
		    force = all_lower;
		    break;
	      case 'o':		/* output <filename> */
	      case 'O':
		    outfile = (*argv) + 2;
		    break;
	      case 'u':
	      case 'U':
	           force = all_upper;
		   break;
	      default:
		   fprintf(stderr, "ignored bad flag: %s\n%s", *argv, usage);
	  }
     }

     /* open input file */
     infile = (argc == 0) ? "yytab.h" : *argv;	/* supply default infile */
     if ((infp = fopen(infile, "r")) == NULL) {	/* file doesn't exist? */
	  if (strchr(infile, '.') != NULL) {	/* no extension present? */
	       strcat(strcpy(buffer, infile), ".h");
	       infp = fopen(buffer, "r");
    	  }
     }
     if (infp == NULL) {
	  fprintf(stderr, "can't find %s\n", infile);
	  exit(EXIT_FAILURE);
     }

     tokens(infp, outfile);	/* do the conversion */
     fclose(infp);

     /* report any trailing unused arguments */
     if (argc > 1) {	/* additional arguments? */
	  fprintf(stderr, "ignored extra argument%s:", (argc == 2) ? "" : "s");
	  while (--argc > 0) {
	       ++argv;
	       fprintf(stderr, " %s", *argv);
          }
	  fprintf(stderr, "\n%s", usage);
     }
     exit(EXIT_SUCCESS);
}

/* tokens - convert header file to quote delimited string */
static void tokens(infp, outfile)
FILE *infp;
char *outfile;
{
     static const char define_word[] = "#define ";
     register char *l;
     char line[256];	/* input line buffer */
     FILE *outfp;
     register char *t;

     /* open output file */
     if (*outfile == '\0')		/* -o<outfile> not supplied? */
	  outfile = "yytok.h";	/* default output file name */
     if ((outfp = fopen(outfile, "w")) == NULL)
	  fprintf(stderr, "can't open %s\n", outfile);
     else {
	  while ((l = fgets(line, sizeof line, infp)) != NULL) {
	       while (*l == ' ' || *l == '\t')	/* skip leading whitespace */
		    ++l;
	       /* note: the ANSI C standard allows spaces, horizontal tabs,
		  and comments to precede and follow the '#' in preprocessor
		  directives.  This program does not recognize #define
		  statements that are preceded by comments nor those where
		  anything separates the '#' and the "define".  It does
		  recognize #define statements preceded by spaces and
		  horizontal tabs.
	       */
	       if (strncmp(l, define_word, sizeof(define_word) - 1) == 0) {
		    l += sizeof(define_word) - 2; /* n.b., trailing ' ' & '\0' */
		    /* skip whitespace between "#define" and token name */
		    do {
	 		++l;
		    } while (isspace(*l));
		    for (t = l; !isspace(*t); ++t)	/* find end of token name */
			 ;
		    *t = '\0';	/* mark end of token name */
		    /* do any requested changes to the case of letters */
	            switch (force) {
		         case capitalize:
		              strlwr(l + 1);
		 	      *l = toupper(*l);
			      break;
		         case all_lower:
		              strlwr(l);
			      break;
		         case all_upper:
		              strupr(l);
			      break;
	            }
	            if (under2blank) {
	  	         /* replace all underscores in token name by spaces */
	 	         for (t = l; (t = strchr(t, '_')) != NULL;  )
			      *t++ = ' ';
	           }
	           fprintf( outfp, "\"%s\",\n", l );
	       }
	  }
	  fclose(outfp);
     }
}
