
/*================== PCYACC ==========================================

            ABRAXAS SOFTWARE (R) PCYACC
      (C)COPYRIGHT PCYACC 1986-88, ABRAXAS SOFTWARE, INC.
               ALL RIGHTS RESERVED

======================================================================*/


extern int debug;

extern FILE *fopen(),
            *infp,
            *listfp,
            *tempfp,
            *tracefp,
            *outfp;

