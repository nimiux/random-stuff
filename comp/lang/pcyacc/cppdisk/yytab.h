#define C_CONSTANT 257
#define F_CONSTANT 258
#define I_CONSTANT 259
#define STRING 260
#define IDENTIFIER 261
#define TAG 262
#define TYP 263
#define VAR 264
#define AND_EQUAL 265
#define DIVIDE_EQUAL 266
#define DOUBLE_AMPERSAND 267
#define DOUBLE_COLON 268
#define DOUBLE_EQUAL 269
#define DOUBLE_LEFT_ANGLE 270
#define DOUBLE_MINUS 271
#define DOUBLE_PLUS 272
#define DOUBLE_RIGHT_ANGLE 273
#define DOUBLE_VERTICAL_BAR 274
#define EXOR_EQUAL 275
#define GREATER_EQUAL 276
#define LEFT_SHIFT_EQUAL 277
#define LESS_EQUAL 278
#define MINUS_EQUAL 279
#define MOD_EQUAL 280
#define NOT_EQUAL 281
#define OR_EQUAL 282
#define PLUS_EQUAL 283
#define POINTER 284
#define RIGHT_SHIFT_EQUAL 285
#define TIMES_EQUAL 286
#define TRIPLE_DOT 287
#define Asm 288
#define Auto 289
#define Break 290
#define Case 291
#define Class 292
#define Const 293
#define Continue 294
#define Default 295
#define Do 296
#define Else 297
#define Enum 298
#define Extern 299
#define For 300
#define Friend 301
#define Goto 302
#define If 303
#define Inline 304
#define Operator 305
#define Overload 306
#define Public 307
#define Register 308
#define Return 309
#define Static 310
#define Struct 311
#define Switch 312
#define This 313
#define Typedef 314
#define Union 315
#define Unsigned 316
#define Virtual 317
#define While 318
#define Char 319
#define Delete 320
#define Double 321
#define Float 322
#define Int 323
#define Long 324
#define New 325
#define Short 326
#define Sizeof 327
#define Void 328
#define SIZEOBJ 329
#define SIZETYPE 330
#define PREINC 331
#define POSTINC 332
#define PREDEC 333
#define POSTDEC 334
#define UMINUS 335
#define UPLUS 336
#define ADDROF 337
#define DEREF 338
#define VECDEL 339
