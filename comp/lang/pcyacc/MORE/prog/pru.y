%{
struc {
      int i;
      char val[90];
      }
%}

%union {
       struct estado *reg;
       int    est;
       }
%token <est> A B C
%type <reg> s z

%start s
%%
s      : A s B { $$ = $2; }
       | z     { $$ = $1; }
       ;
z      : C z C { $$ = $2; }
       |     { $$ = NULL; }
       ;
%%

