
# line 5 "y.y"
typedef union  {        struct estado *reg_est;
                        struct tran   *reg_tran;
                        int     est;
                } YYSTYPE;
#define YYSUNION /* %union occurred */
#define EST 257
#define CAR 258
YYSTYPE yylval, yyval;
#define YYERRCODE 256

# line 59 "y.y"


struct tran *cons_tran(car, est_dest)
        char car;
        int est_dest;
{
struct tran *p_tran;

        p_tran= (struct tran *) malloc(sizeof(struct tran));
        if (p_tran==NULL)
                {
                printf("<<- error: memoria insuficiente.");
                exit(0);
                }
        p_tran->sig= NULL;
        p_tran->car= car;
        p_tran->est= cons_est(est_dest)->num;
        return(p_tran);
}


/************************************************************************/
/************************************************************************/
/************************************************************************/

struct estado *cons_est (id_est, fin)
int nro_est;

{
struct estado *p_est;
int n;

for (p_est= est_ini, n=0; p_est != NULL; p_est= p_est->sig, n++)
        if (p_est->iden == id_est)
                return(p_est);
p_est= (struct estado *) malloc (sizeof(struct estado));
if (p_tran==NULL)
        {
        printf("<<- error: memoria insuficiente.");
        exit(0);
        }
if (ult_est != NULL)
        ult_est->sig= p_est;
else
        est_ini= p_est;
ult_est= p_est;
p_est->sig= NULL;
p_est->trans= NULL;
p_est->id= id_est;
p_est->num= n_est++;
p_est->fin= final;
p_est->trans= NULL;
return(p_est);
}

#include <stdio.h>
FILE *yytfilep;
char *yytfilen;
int yytflag = 0;
int svdprd[2];
char svdnams[2][2];

int yyexca[] = {
  -1, 1,
  0, -1,
  -2, 0,
  0,
};

#define YYNPROD 9
#define YYLAST 13

int yyact[] = {
       9,      12,      10,       5,       8,       3,       1,       2,
       6,       7,       4,       0,      11,
};

int yypact[] = {
    -254,   -1000,    -254,   -1000,    -258,    -255,   -1000,    -258,
   -1000,    -256,   -1000,   -1000,   -1000,
};

int yypgo[] = {
       0,      10,       9,       4,       7,       5,       6,
};

int yyr1[] = {
       0,       6,       4,       4,       5,       1,       2,       2,
       3,
};

int yyr2[] = {
       0,       1,       1,       2,       2,       2,       1,       2,
       2,
};

int yychk[] = {
   -1000,      -6,      -4,      -5,      -1,     257,      -5,      -2,
      -3,     258,     257,      -3,     257,
};

int yydef[] = {
       0,      -2,       1,       2,       0,       0,       3,       4,
       6,       0,       5,       7,       8,
};

int *yyxi;
/*****************************************************************/
/* PCYACC LALR parser driver routine -- a table driven procedure */
/* for recognizing sentences of a language defined by the        */
/* grammar that PCYACC analyzes. An LALR parsing table is then   */
/* constructed for the grammar and the skeletal parser uses the  */
/* table when performing syntactical analysis on input source    */
/* programs. The actions associated with grammar rules are       */
/* inserted into a switch statement for execution.               */
/*****************************************************************/


#ifndef YYMAXDEPTH
#define YYMAXDEPTH 200
#endif
#ifndef YYREDMAX
#define YYREDMAX 1000
#endif
#define PCYYFLAG -1000
#define WAS0ERR 0
#define WAS1ERR 1
#define WAS2ERR 2
#define WAS3ERR 3
#define yyclearin pcyytoken = -1
#define yyerrok   pcyyerrfl = 0
YYSTYPE yyv[YYMAXDEPTH];     /* value stack */
int pcyyerrct = 0;           /* error count */
int pcyyerrfl = 0;           /* error flag */
int redseq[YYREDMAX];
int redcnt = 0;
int pcyytoken = -1;          /* input token */


yyparse()
{
  int statestack[YYMAXDEPTH]; /* state stack */
  int      j, m;              /* working index */
  YYSTYPE *yypvt;
  int      tmpstate, tmptoken, *yyps, n;
  YYSTYPE *yypv;


  tmpstate = 0;
  pcyytoken = -1;
#ifdef YYDEBUG
  tmptoken = -1;
#endif
  pcyyerrct = 0;
  pcyyerrfl = 0;
  yyps = &statestack[-1];
  yypv = &yyv[-1];


  enstack:    /* push stack */
#ifdef YYDEBUG
    printf("at state %d, next token %d\n", tmpstate, tmptoken);
#endif
    if (++yyps - &statestack[YYMAXDEPTH] > 0) {
      yyerror("pcyacc internal stack overflow");
      return(1);
    }
    *yyps = tmpstate;
    ++yypv;
    *yypv = yyval;


  newstate:
    n = yypact[tmpstate];
    if (n <= PCYYFLAG) goto defaultact; /*  a simple state */


    if (pcyytoken < 0) if ((pcyytoken=yylex()) < 0) pcyytoken = 0;
    if ((n += pcyytoken) < 0 || n >= YYLAST) goto defaultact;


    if (yychk[n=yyact[n]] == pcyytoken) { /* a shift */
#ifdef YYDEBUG
      tmptoken  = pcyytoken;
#endif
      pcyytoken = -1;
      yyval = yylval;
      tmpstate = n;
      if (pcyyerrfl > 0) --pcyyerrfl;
      goto enstack;
    }


  defaultact:


    if ((n=yydef[tmpstate]) == -2) {
      if (pcyytoken < 0) if ((pcyytoken=yylex())<0) pcyytoken = 0;
      for (yyxi=yyexca; (*yyxi!= (-1)) || (yyxi[1]!=tmpstate); yyxi += 2);
      while (*(yyxi+=2) >= 0) if (*yyxi == pcyytoken) break;
      if ((n=yyxi[1]) < 0) { /* an accept action */
        if (yytflag) {
          int ti; int tj;
          yytfilep = fopen(yytfilen, "w");
          if (yytfilep == NULL) {
            fprintf(stderr, "Can't open t file: %s\n", yytfilen);
            return(0);          }
          for (ti=redcnt-1; ti>=0; ti--) {
            tj = svdprd[redseq[ti]];
            while (strcmp(svdnams[tj], "$EOP"))
              fprintf(yytfilep, "%s ", svdnams[tj++]);
            fprintf(yytfilep, "\n");
          }
          fclose(yytfilep);
        }
        return (0);
      }
    }


    if (n == 0) {        /* error situation */
      switch (pcyyerrfl) {
        case WAS0ERR:          /* an error just occurred */
          yyerror("syntax error");
          yyerrlab:
            ++pcyyerrct;
        case WAS1ERR:
        case WAS2ERR:           /* try again */
          pcyyerrfl = 3;
	   /* find a state for a legal shift action */
          while (yyps >= statestack) {
	     n = yypact[*yyps] + YYERRCODE;
	     if (n >= 0 && n < YYLAST && yychk[yyact[n]] == YYERRCODE) {
	       tmpstate = yyact[n];  /* simulate a shift of "error" */
	       goto enstack;
            }
	     n = yypact[*yyps];


	     /* the current yyps has no shift on "error", pop stack */
#ifdef YYDEBUG
            printf("error: pop state %d, recover state %d\n", *yyps, yyps[-1]);
#endif
	     --yyps;
	     --yypv;
	   }


	   yyabort:
            if (yytflag) {
              int ti; int tj;
              yytfilep = fopen(yytfilen, "w");
              if (yytfilep == NULL) {
                fprintf(stderr, "Can't open t file: %s\n", yytfilen);
                return(1);              }
              for (ti=1; ti<redcnt; ti++) {
                tj = svdprd[redseq[ti]];
                while (strcmp(svdnams[tj], "$EOP"))
                  fprintf(yytfilep, "%s ", svdnams[tj++]);
                fprintf(yytfilep, "\n");
              }
              fclose(yytfilep);
            }
	     return(1);


	 case WAS3ERR:  /* clobber input char */
#ifdef YYDEBUG
          printf("error: discard token %d\n", pcyytoken);
#endif
          if (pcyytoken == 0) goto yyabort; /* quit */
	   pcyytoken = -1;
	   goto newstate;      } /* switch */
    } /* if */


    /* reduction, given a production n */
#ifdef YYDEBUG
    printf("reduce with rule %d\n", n);
#endif
    if (yytflag && redcnt<YYREDMAX) redseq[redcnt++] = n;
    yyps -= yyr2[n];
    yypvt = yypv;
    yypv -= yyr2[n];
    yyval = yypv[1];
    m = n;
    /* find next state from goto table */
    n = yyr1[n];
    j = yypgo[n] + *yyps + 1;
    if (j>=YYLAST || yychk[ tmpstate = yyact[j] ] != -n) tmpstate = yyact[yypgo[n]];
    switch (m) { /* actions associated with grammar rules */
      
      case 1:
# line 16 "y.y"
      {
                      est_ini= yypvt[-0].est;
                      } break;
      case 4:
# line 23 "y.y"
      {
                              yypvt[-1].reg_est->tran= yypvt[-0].reg_tran;
                              } break;
      case 5:
# line 27 "y.y"
      {
                              if ((yypvt[-0].int==1)||(yypvt[-0].int==0))
                                      yyval.reg_est= cons_est(yypvt[-1].int, yypvt[-0].int);
                              else
                                      {
                                      printf("<<- error: indicación de estado final erronea");
                                      exit(0);
                                      }
                              } break;
      case 6:
# line 37 "y.y"
      {
                              yyval.reg_tran= yypvt[-0].reg_tran;
                              } break;
      case 7:
# line 40 "y.y"
      {
                              yypvt[-1].reg_tran->sig= yypvt[-0].reg_tran;
                              yyval.reg_tran= yypvt[-0].reg_tran;
                                      /* enlaza el registro devuelto en $2 con
                                       * el ultimo elemento de la lista ya
                                       * construida ($1), y $2 pasa a ser el
                                       * ultimo elemento
                                       * */
                              } break;
      case 8:
# line 50 "y.y"
      {
                              yyval.reg_tran= cons_tran(yypvt[-1].int, yypvt[-0].int);
                                      /* construye un registro de transicion y
                                       * le asigna el caracter y estado devuelto
                                       * por el lex. Devuelve puntero al registro
                                       * creado.
                                       * */
                              } break;    }
    goto enstack;
}
