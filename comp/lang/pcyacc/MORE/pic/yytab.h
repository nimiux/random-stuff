
typedef union  {
  int   in;
  char *ch;
} YYSTYPE;
extern YYSTYPE yylval;
#define DRAW 257
#define DEFINE 258
#define LINE 259
#define BOX 260
#define POLYGON 261
#define CIRCLE 262
#define ELLIPSE 263
#define BLACK 264
#define WHITE 265
#define SOLID 266
#define DOTTED 267
#define FILL 268
#define IDENTIFIER 269
#define INTEGER 270
