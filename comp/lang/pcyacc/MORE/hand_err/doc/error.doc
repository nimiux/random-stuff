	      Advanced Error Processing with PCYACC

	   (c) Copyright 1989, Abraxas Software, Inc.

    Syntax error processing breaks naturally into three parts: reporting
(tell the user what went wrong), handling (how to fix up the input to
continue parsing), and recovery (how to fix up the output or internal data
structures).  Completely automated error reporting is included in PCYACC.
PCYACC has a good, general mechanism for error handling that allows
tailoring for your specific application.  Error recovery is specific to each
application--some guidelines and examples will be given and some of the
pitfalls pointed out.

     Section 1 of this manual describes how to use the error reporting code
in ERRORLIB.C and ERRORLIB.H.  Section 1.2 is a reference manual for the
error reporting functions in ERRORLIB.C.  All parsers on this disk use the
error reporting code.  In \INTOPOST are three versions of the infix to
postfix translator described in Chapter VII of the PCYACC manual.  They
illustrate how to use the supplied error reporting code and several ways to
use the error handling mechanism.  They are explained in detail in section 2
of this manual.  In \ANSIC is an ANSI C syntax analyzer with error
processing added.  It ignores preprocessor lines and semantics, has a full
lexical scanner, and has basic error handling.  It is briefly explained in
section 2.4 and is a more extensive example of the techniques outlined
earlier in section 2.  In \PIC is a version of the processor for the PIC
graphic description language with extensive error handling and recovery.  It
is explained in section 3.

     Section 4 describes how to build (yacc, compile, and link) parsers and
is a reference manual for the TOKENS.EXE program.  All programs on this disk
have MAKEFILEs for additional examples on how to build a parser.


1. Error Reporting

     PCYACC's error reporting mechanism reports the error number, type
("syntax error" or "stack overflow"), where the error was detected, the
erroneous token's type and the expected/allowed token types.  For example:

	[error 1] file 'errors', line 1: syntax error
        actual: '&' expecting: '+', '-', ';'
	[error 2] file 'errors', line 3: syntax error
	actual:	';' expecting: ')', '+', '-'
	[error 3] file 'errors', line 6 near "55": syntax error
	actual: CONSTANT expecting: '+', '-', ';'
	[error 4] file 'errors', line 8 near "v": syntax error
	actual: VARIABLE expecting: '+', '-', ';'

     The actual versus expected display in the second line of the error
message prints the token that caused the error and the tokens that are
acceptable at this point in the parse.  These include "[end of file]",
single characters (e.g., '+', '-', or ';'), or any of the terminals declared
"%token" in the declaration section.


1.1. Integration with a Lexical Scanner

     When you use the supplied error reporting code, "errorlib.c", in your
program, your lexical scanner needs to pass information about the location
of the current token to the "errorlib" routines.  The integer variable
"yylineno" should be set non-negative before calling "yyparse" and
incremented as each new input line is read.  This usually means setting it
to 1 in the main program and incrementing it as each newline ('\n')
character is processed by "yylex".  If your lexical analyzer uses a lookahead
character, initialize it to '\n' and set "yylineno" to 0 in the main program.
The input file name should be copied into the character array "yyerrsrc".
Error messages are written to the "yyerrfile" stream.  The FILE pointer
"yyerrfile" is initially set to "stderr".  You may assign another FILE
pointer to it to redirect error messages.  The character pointer "yyerrtok"
should be either "NULL" or point to a NUL-terminated character string
containing the current input token text.  Look at "yylex" in "SIMPLE.Y" for
an example.  All of these variables are initialized so that if your code
doesn't set them, they won't show up in the error messages.


1.2. YYERROR Calling Conventions

     If you wish to use the error reporting code in "errorlib.c" for other
errors, read this section.  Otherwise, skip it.  The prototype for the
"yyerror" routine called by the "yyparse" in "yaccpar.c" is:

	extern void yyerror(char *msg, char *token);

When a syntax error is detected, "yyparse" calls "yyerror" with the error
message ("syntax error") and a string containing the name of the actual
token.  It then repeatedly calls "yyerror" with NULL and a string containing
the name of an expected token.  Finally it calls "yyerror" with two NULL
pointers to reset it.

     For the simpler case of just an error message with no actual and
expected tokens, like a stack overflow, "yyparse" calls "yyerror" with a
message string and a NULL pointer.  Note that a NULL pointer in the second
argument always indicates the end of the error message.

     The token number to string conversion routine, "yydisplay" is available
for use.  It takes a single integer argument, a token as returned by
"yylex", and returns a pointer to the token's name in a NUL-terminated
string.  Do not depend on the string value surviving past another call to
"yydisplay".


2. Error Handling

     The simplest error handling mechanism is none, i.e., quit after
reporting the first error.  Except for simple, line-at-a-time interactive
applications, this is unacceptable.  More sophisticated error handling
involves changing the input in the vicinity of the error.  PCYACC uses a
combination of deletion and insertion.

     To control the error handling process, additional rules are inserted in
the grammar.  These error rules are ignored during normal parsing.  Each
error rule has the "error" keyword somewhere on the right side.  When a
syntax error occurs, the parser stack is popped until a state with a shift
on "error" is found.  The "error" token is inserted ahead of the token that
caused the error in the input stream (the lookahead token) and parsing
continues in the recovery mode.  The parser will remain in the recovery mode
until 3 consecutive tokens are read and shifted without error.  Syntax errors
in recovery mode are not reported.  Any tokens read between syntax errors in
recovery mode are discarded.

     Popping the parse stack effectively deletes tokens to left of the error
detection point from the parse.  Zero or more tokens to the right of the
error are deleted from the parse during error handling.

     If popping the parse stack never finds a state with a shift on "error",
the parser aborts ("yyparse" returns with a non-zero value).  Encountering
EOF in the recovery mode always aborts the parser.  If the parser is going to
be robust and continue trying in the face of multiple errors, there must a
state with a shift on "error" low on the stack.  The initial state, State 0,
is always the bottom state on the parse stack.  For this reason, it is a
good idea to have an "error" rule for the start symbol of the grammar.


2.1. Simple Recovery

    For a unstructured, statement oriented language, skipping to the end of
the statement on an error is a good strategy.  The file "\INTOPOST\SIMPLE.Y"
illustrates this technique.  This program is the infix to postfix translator
("\INTOPOST\INTOPOST.Y" on the program disk) with two additional rules:

	infix_prog : error ';'			{ yyerrok; }
		   | infix_prog error ';'	{ yyerrok; }
	           ;

These two rules ensure that there is always a state with a shift on "error"
somewhere on the stack.  Normally the parser would not get out of the
recovery mode until parsing the ';' and the two tokens after that.  The
semantic action "yyerrok" says in effect: I know what's going on, it's okay
to resume real parsing so leave the recovery mode and let me know if there
are any syntax errors from here on.  This technique is easy to apply and
works well.  The one syntax error detected per statement limit is reasonable
for short statements and meshes well with most users' expectations.

     The infix to postfix translator is simple enough to do error recovery
by discarding output from a bad expression.  At initialization and at the
end of each complete expression, the position of the output file is saved in
"start".  If a syntax error is detected, the output file is backed up to the
expression "start", discarding any partial postfix expression.  Translation
resumes with the next expression.  Look at "\INTOPOST\SIMPLE.Y" for the
details.

2.2. Improved Recovery

     Detecting more than one syntax error per expression requires more error
rules.  For your language, determine which tokens are significant and are
unlikely to be misspelled or mistyped.  Most are either terminators, infix
operators, or grouping markers.  Terminators mark the end of significant
pieces of the input.  Examples are the ';' at the end of statements and the
',' in argument lists in C and Pascal.  Add a rule or two with the error
token just before the terminator.  The error rules added to "SIMPLE.Y" are a
good example of this case.  Infix operators appear between their operands
(like =, %, /, and || in C).  The C binary operators and the tertiary
conditional operator (e.g., "(a == 0) ? b : c") are infix operators.  For
binary operators, add one rule like the rule(s) for the lowest precedence
operator with the operator replaced by "error".  In the infix grammar,
addition and subtraction are the lowest precedence operators and the
following rule is added: 

	infix_expr : infix_expr error infix_term
		   ;

Using the lowest precedence gives the widest coverage for error handling and
gives the surrounding operators higher precedence, which is usually the
users' understanding of the error correction.

     For grouping markers, add an error rule to the construct that goes
inside the markers.  For example, in C:

	compound_statement : '{' statement_list '}'
			   ;
	statement_list : statement
	  	       | statement_list statement
	  	       ;
	statement : labeled_statement
	  	  | basic_statement
	  	  | compound_statement
	  	  | error ';'		{ yyerrok; }
	  	  ;

This rule can be generalized to: add an error rule for the middle of any
construct with three or more tokens or nonterminals, if the first symbol is
unique to the context.  The error rule can be down a level or two as in the
example above or in the rule itself.  For example:

	declaration : declaration_specifiers error ';' { yyerrok; }
		    ;

If the last symbol is a token and is unlikely to be mistyped, add the
"yyerrok;" semantic action, otherwise let the 3 successfully shifted tokens
rule apply.  In the \ANSIC subdirectory is a grammar for ANSI C with error
rules added according to these heuristics (see section 2.4 for more on the
ANSI C syntax analyzer).

     These strategies are adapted from strategies and tactics outlined by
Axel Schreiner in his book listed at the end of this file.  He outlines some
techniques and notes that they don't work with several of the most common
YACC implementations.  PCYACC is one of them.  The above rules do work with
PCYACC and can be generalized to more complex constructs.

     The exact effect of error handling is very tied to the parsing
strategy.  Three good rules of thumb are: 1) when "error" appears just
before a terminal, input will be skipped up to that terminal, 2) when
"error" appears just before a non-terminal, a dummy token with no meaning
will be inserted, and 3) when "error" appears at the end of a rule, the
token it substitutes for is inserted.  The second and third rules have
implications for error recovery that are outlined below.  In the "improved"
grammar, it is possible for several states with a shift on "error" to be on
the stack at any given time.  Remember that the top one counts.  For
example, if an error is detected at the beginning of a statement (i.e., at
the beginning of the file or just after a ';'), rule 1 applies and input is
skipped up to the next ';' (or EOF).  If an error is detected within an
expression and outside of parentheses, rule 2 applies and the inserted
"error" token is effectively an addition operator.  Inside parentheses, an
"error" token is inserted and the lookahead token (the one that caused the
error) will be examined, if it can be the start of an expression (a
CONSTANT, IDENTIFIER, or left parenthesis) then the inserted "error" token
is again a pseudo addition operator.  Otherwise, it is treated like a
closing parenthesis.


2.3. Doing Your Own Parser Recovery

     If you would rather do your own error handling, hooks are provided in
PCYACC to do so.  In the simplest usage, just add a rule that says the start
symbol of the grammar can be an "error".  This adds a shift on "error" to
the state on the bottom of the parse stack so the action that does your
recovery will always be called.  For example:

	infix_prog : error	{ recover();  yyerrok;  yyclearin; }
		  ;

You write the "recover" function.  It should advance the input stream to a
point where parsing can continue (for example, the start of an infix
expression).  The "yyerrok" action kicks the parser out of the recovery mode
as in previous parsers.  The "yyclearin" action empties the lookahead token.
The lookahead token is the last one read by the parser.  In the case above,
it is the token that caused the error.  For a rule with the "error" in the
final position, it is either the last terminal of the rule or the first
terminal after the rule, if lookahead is needed to determine which parsing
action to take.  Examine the ".lrt" file if the difference is critical.

     The obvious way to write "recover" for the infix to postfix translator
is:

	recover()
	{
	     int t;

	     while ((t = yylex()) != ';' && t != 0)	/* EOF == 0 */
	          ;
	     fseek(outf, start, SEEK_SET);
	}

This works but occasionally discards too much input.  For an infix expression
like "(1 + 1;" with an error detected at the ';', the above code will start
discarding input with the token after the ';' that the parser has already
read into the lookahead token and the entire next expression will be
discarded.  The parser's lookahead token is in "int token;".  Rewriting
"recover" to use this information produces:

	recover()
	{
	     while (token != ';' && token != 0)	/* EOF == 0 */
	          token = yylex();
	     fseek(outf, start, SEEK_SET);
	}

This change makes the ``Do It Yourself'' error processing version behave the
same as the "SIMPLE.Y" translator.  Error recovery is also the same.  The
complete source code is in "DIY.Y".


2.4. The ANSI C Parser

     The error rules in the ANSI C syntax analyzer in \ANSIC are a straight
forward implementation of the heuristics given in section 2.2.  The start
symbol is "translation_unit" and an error rule has been added to it to
guarantee that a shift on "error" state is always somewhere on the parse
stack.  Two rules are added to "function_definition" to handle missing or
incorrect text in the middle of a function header.  Error rules for
"declaration" handle an incorrect "declaration_specifiers" and an incorrect
"init_declarator_list".  As a catch-all for incorrect structure
declarations, an error rule is added to "struct_declaration" to discard all
text through the terminating semicolon and then exit recovery mode.  The
same is true for "statement".

     Semicolons are presumed to be correct and in the examples above
rocovery mode is terminated and normal parsing begins again.  For the other
error rules, the three correct tokens rule is used to quit recovery mode.

     The lists within grouping markers are: "enumerated_list" and
"identifier_list".  The former is enclosed in braces and the error rule is
added to "enumerator".  The latter is enclosed in parentheses and the error
rule is added to "identifier_list" directly.

     In C, the lowest precedence operator in expressions is the comma
operator.  The error rule for "expression" catches all missing or incorrect
operators in expressions.  The rule of three is used to exit recovery mode.
Badly garbled expressions can lead any error handling strategy astray.  The
rule of three reduces the number of cascading error messages (multiple
messages from one error).

     The ANSI C grammar has examples of all of the error rule heuristics
given previously.


3. Error Recovery

     Error recovery (repairing the output of the parser) is similar to error
handling.  The simplest is none, i.e., don't generate any output if a syntax
error occurs.  This is often acceptable.  Continued type checking in
strongly typed languages and other areas where syntax and semantics overlap
requires some continued semantic processing.  Other solutions for error
recovery are: back out the incorrect actions to some safe state, or correct
the semantics, making safe or neutral assumptions about the user's intent
(e.g., C compilers assume that an undeclared variable is either an "int" or
some probable type based on the context of its first use).  The simple and
do-it-yourself programs in the previous section illustrated a little error
recovery.  The error recovery in the improved program is described in
section 3.1.  The PIC program in the
\PIC directory has more extensive error recovery.  It extensively
illustrates the principles of good recovery.  It is described in section 3.2.


3.1. Error Recovery in IMPROVED.Y

     The "improved" grammar attempts to always output a valid postfix
expression.  For errors detected at the start of an expression, nothing has
been output for the expression so no fix up is needed.  For the error rule

	infix_expr : infix_expr error infix_term
		   ;

two operands have been recognized and something needs to be done to combine
both values into one; addition was arbitrarily picked and a '+' is output.
Without the addition, the end of the expression would be reached with two
values still on the stack of the postfix evaluator.  Parentheses in infix
expressions have no semantics and so no fix up is done for the error rules of
"infix_fact".  Error recovery needs to track error handling.  It is still
possible for the "improved" infix to postfix translator to output incorrect
postfix but for widely spaced errors, the output will at least be valid even
if not what the user intended.


3.2. Error Recovery in PIC

     PIC is a simple graphics description language.  A program to run PIC on
a CGA video system is included in the \PIC directory on the program disk and
is described in Chapter XIII of the manual.  In the \PIC directory on this
disk is a version with extensive error recovery added.  Read Chapter XIII
and then this section.  The differences between the two versions are limited
to the error recovery changes so FC or any other file comparison utility
will pinpoint the changes.

     An error rule is added to the grammar for "stats" to guarantee a shift
on "error" in the state on the bottom of the stack (State 1 in this case).
This last chance error recovery simply discards very badly formed statements.

     Missing DRAW and DEFINE keywords, missing equals signs, and missing
semicolons are repaired by the rules added to "draw_stat" and "define_stat".
The keywords and terminals are redundant, some can be missing without losing
any important information.  The rules show how the statements can be garbled
and the necessary information still retrieved.  The 6th and 7th rules for
"define_stat" cover a missing IDENTIFIER.  Parsing of the Object can
continue, but there is no name for the object and it is discarded.

     After each Object is used, the values of "anObject" are re-initialized
to default values (by "clr_object()" in YYSUB.C).  These values are used
when a shape or attribute is not specified, is incorrectly specified, or its
value discarded during error recovery.  The default values are the most
likely values (e.g., white borders, black fill), the most general (e.g.,
polygon), or some other safe value.  Error rules are added for missing or
incorrect shapes, attributes, fill colors, and commas to permit error
handling.

     Incorrect points are handled slightly differently.  Completely wrong
points are discarded.  A single INTEGER is assumed to be the X coordinate
and -32768 is used for the Y coordinate.  When an Object is copied from
anObject, the unspecified points are set to (-32768,-32768).  No check is
made that the number of points for a shape is correct.  Due to the way
signed integers are represented, there is no +32768 and so the value -32768
is impossible to get through the lexical scanner.  This impossible value is
used to signal a missing value.

     The error rules in PIC.Y are extensive enough that all states with
shifts have shifts on "error", except those only accessible in recovery
mode.  The stack never needs to be popped to uncover a state with a shift on
"error", so correctly parsed input is never discarded by error handling.  If
your semantic actions use the semantic stack to pass dynamically allocated
memory pointers about, this behavior is very useful to allow deallocating
the memory when errors render the information it carries useless.  To get
this behavior, you may need to add error rules in certain areas of the
grammar that are not otherwise needed for good error handling.

     Good error handling is a requirement for good error recovery.  Global
variables used by the semantic actions must be initialized before parsing
begins and re-initialized to safe (e.g., 0, white borders, black fill) or
significant values (e.g., -32768, or other "impossible" or unlikely values)
at good breaking points (e.g., end of statement).  The intention is that all
variables have well defined values at all times so error do not produce
erratic behavior.  Dynamically allocated memory must be tracked carefully to
prevent repeated loss of significant pieces, especially ones that are
expected to have only limited scope or lifetime (e.g., C local variables).
This may necessitate additional error rules that parsing alone does not
require.  Sufficient redundancy in a construct may allow it's semantics to
survive missing or incorrect parts.  Good error recovery involves extensive
defaulting or initialization, good repair of missing information, and robust
post-parse handling of possibly incomplete information.


4. Building Parsers

     Translating parser source code to an executable image involves PCYACC,
TOKENS.EXE, a C compiler, and the linker.  TOKENS converts the C header file
output by PCYACC into a list of token names in quotes for the error
reporting in "ERRORLIB.C".  For the simplest case of a parser entirely in
one file, for example, "HELLO.Y": 1) copy YACCPAR.C from \INTOPOST on this
disk to your working directory, 2) copy TOKENS.EXE from \TOKENS to a
directory in the PATH list (e.g., \BIN), 3) copy ERRORLIB.* from \INTOPOST
to your working directory, and 4) YACC, tokenize, compile, and link like
this:

	pcyacc -d -pyaccpar.c hello.y
	tokens
	cl hello.c errorlib.c

This example assumes that "cl" is your C compiler.  A MAKE utility makes
builds less tedious.  The MAKEFILEs on this disk assume Microsoft's C
compiler and have been tested with Microsoft's NMAKE utility.  The rest of
this section describes TOKENS in more detail.


4.1. TOKENS program

     The TOKENS program reads the C header file (also called the Token
Definition file) produced by PCYACC and writes a file to be included in
"errorlib.c", the advanced error reporting code.  The C header file is
created when PCYACC is run with either the -d or -D option (see Chapter III,
sections 2 and 3.2).  The C header file name is "yytab.h" with the -d
option, the base name of the grammar description file with an extension ".h"
with the -D option, and any name you choose with the -D<hf> option.

TOKENS is invoked by simply typing TOKENS from the DOS prompt.  This chapter
explains the command line format and the possible options.


4.2. Command Line Format

TOKENS can be invoked by typing TOKENS, followed by zero or more command
line options, followed by an optional file name.  For example:

              TOKENS [options] [<filename>]

<filename> is the name of the C header file produced by PCYACC.  If no
extension is given and <filename> cannot be found, then <filename> with the
extension ".h" is tried.  If no <filename> is specified, "yytab.h" is used.


4.3. Command Line Options

Command line options are used to override default actions or file name
conventions.  Available options are described below:

-b: Change all underscores in token names to blanks.  For example,
      "arith_exp" becomes "arith exp".

-c: Force the first letter of each token to upper case, the rest to
      lower case.  For example, "aNycaSe" becomes "Anycase".

-l: Force all letters of each token to lower case.

-o<tf>: Output is to <tf> instead of the default "yytok.h".

-u: Force all letters of each token to upper case.

The option letter's case does not matter, i.e., -b and -B mean the same.


4.4. Using Command Line Options

This section shows you how to use the command line options.  The following
example, HELLO.Y, is used throughout this section:

	%{
	#include "errorlib.h"
	%}

	%token WORLD
	%token HELLO

	%%

	greetings : HELLO ',' WORLD ;

To build a parser from HELLO.Y using the default actions of PCYACC and
TOKENS as much as possible, do the following actions (assuming CL is the C
compiler):

	pcyacc -d -pyaccpar.c hello.y
	tokens
	cl hello.c errorlib.c

Another possibility is using the grammar base name for the C header file.
In this example, the token names used in error messages are capitalized:

	pcyacc -D -pyaccpar.c hello.y
	tokens -c hello
	cl hello.c errorlib.c

Normally, TOKENS writes to "yytok.h".  The supplied "errorlib.c" code
includes "yytok.h".  If you change this file name to something else, for
example "hello.tok", run the following procedure to build your parser:

	pcyacc -pyaccpar.c -D hello.y
	tokens -ohello.tok hello
	cl hello.c errorlib.c

The TOKENS program converts the C header file that defines the tokens that
the parser expects from the lexical scanner to a printable form for the
improved error reporting.


5. Wrapup

     Advanced error processing involves first: reporting the error and its
location in the input with the errorlib routines.  Syntax error information
is automatically passed by the parser as generated by PCYACC.  Any location
information is passed by the lexical scanner.  Second: error handling
depends on error rules you put in the grammar.  The heuristics given cover
the common situations and can be extended to almost any grammar.  Where
error rules and the built-in error recovery mechanism are not adequate,
hooks are provided for you to roll your own error handling.  Third: error
recovery involves keeping all internal data structures in a useable state
even when the information in the input is incomplete or invalid.



Bibliography
1. "Compiler Design and Construction, Second Edition", Arthur Pyster, Van
   Nostrand Rheinhold Co., New York, NY.
2. "Introduction to Compiler Construction with UNIX", Axel Schreiner and H.
   George Friedman, Jr., Prentice-Hall, Inc., Englewood Cliffs, NJ, pg. 65
3. "Compiler Construction" (Lecture Notes in Computer Science, No. 21),
   Bauer and Eickel, ed., Springer-Verlag, New York, NY.
   a) "What the Compiler Should Tell the User", James Horning.  Batch
      oriented, but still a good starting point.
   b) "Error Recovery and Correction - An Introduction to the Literature",
      David Gries.  Where to go from here.
