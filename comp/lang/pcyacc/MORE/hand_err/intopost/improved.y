/*
 * INTOPOST Example w/ improved error recovery.
 *
 *	(c) Copyright 1989, Abraxas Software, Inc.
*/

%{

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "errorlib.h"

FILE *inf;			/* input file */
FILE *outf;			/* output file */
char  outfn[] = "postfix.txt";	/* output file name */

int   yyparse(void);
%}

%union {
  char *oprnd;
}
%token CONSTANT
%token VARIABLE
%type  <oprnd> CONSTANT VARIABLE
%start infix_prog

%%

infix_prog : infix_expr ';'
             { fprintf(outf, " ;\n"); }
           | infix_prog infix_expr ';'
             { fprintf(outf, " ;\n"); }
	   | error ';'
	     { yyerrok;  fprintf(outf, " ;\n"); }
	   | infix_prog error ';'
	     { yyerrok;  fprintf(outf, " ;\n"); }
           ;

infix_expr : infix_term
           | infix_expr '+' infix_term
             { fprintf(outf, " +"); }
           | infix_expr '-' infix_term
             { fprintf(outf, " -"); }
	   | infix_expr error infix_term
	     { yyerrok;  fprintf(outf, " +"); }
           ;

infix_term : infix_fact
           | infix_term '*' infix_fact
             { fprintf(outf, " *"); }
           | infix_term '/' infix_fact
             { fprintf(outf, " /"); }
           ;

infix_fact : CONSTANT
             { fprintf(outf, " %s", $1); }
           | VARIABLE
             { fprintf(outf, " %s", $1); }
           | '(' infix_expr ')'
           | '(' infix_expr error
           ;
%%

int  nxtch;		/* lookahead character for yylex() */

main(argc, argv)
char *argv[];
{
     int status;	/* yyparse() status */

     fprintf(stdout, "\n*********************************************************\n");
     fprintf(stdout, "*   INTOPOST: INfix TO POSTfix expression translator    *\n");
     fprintf(stdout, "*                                                       *\n");
     fprintf(stdout, "*     Usage: intopost <infixfile>                       *\n");
     fprintf(stdout, "*     1) prepare a infix source file                    *\n");
     fprintf(stdout, "*        e.g. egfile                                    *\n");
     fprintf(stdout, "*        1+2*3;                                         *\n");
     fprintf(stdout, "*        9+8*7-6/5;                                     *\n");
     fprintf(stdout, "*        a+b*100;                                       *\n");
     fprintf(stdout, "*        use semicolon ; to terminate an expression     *\n");
     fprintf(stdout, "*     2) invoke intopost                                *\n");
     fprintf(stdout, "*        intopost egfile                                *\n");
     fprintf(stdout, "*     3) the result of translation is saved in          *\n");
     fprintf(stdout, "*        the file postfix.txt                           *\n");
     fprintf(stdout, "*                                                       *\n");
     fprintf(stdout, "*********************************************************\n\n\n");
     if (argc != 2) {
	  fprintf(stderr, "not enough arguments, abort \n");
	  exit(1);
     }

     /* open input file */
     if ((inf = fopen(argv[1], "r")) == NULL) {
	  fprintf(stderr, "Can't open file: \"%s\"\n", argv[1]);
	  exit(1);
     }
     strcpy(yyerrsrc, argv[1]);

     /* open output file */
     if ((outf = fopen(outfn, "w")) == NULL) {
	  fprintf(stderr, "Can't open file: \"%s\"\n", outfn);
	  exit(1);
     }

     fprintf(stdout, "translation in progress ... \n");

     /* initialize lexical scanner (yylex) */
     nxtch = getc(inf);	
     yylineno = 1;

     if ((status = yyparse()) != 0)
       fprintf(outf, " ;\n");

     fclose(inf);
     fclose(outf);

     if (status) {
	  fprintf(stdout, "\n*********************************************************\n");
	  fprintf(stdout,   "*   INTOPOST: INfix TO POSTfix expression translator    *\n");
	  fprintf(stdout,   "*                                                       *\n");
	  fprintf(stdout,   "*     abnormal termination                              *\n");
	  fprintf(stdout,   "*     error in translation                              *\n");
	  fprintf(stdout,   "*     bye!                                              *\n");
	  fprintf(stdout,   "*                                                       *\n");
	  fprintf(stdout,   "*********************************************************\n");
	  exit(1);
     }

     fprintf(stdout, "\n*********************************************************\n");
     fprintf(stdout,   "*   INTOPOST: INfix TO POSTfix expression translator    *\n");
     fprintf(stdout,   "*                                                       *\n");
     fprintf(stdout,   "*     normal termination                                *\n");
     fprintf(stdout,   "*     see file postfix.txt for result                   *\n");
     fprintf(stdout,   "*     bye!                                              *\n");
     fprintf(stdout,   "*                                                       *\n");
     fprintf(stdout,   "*********************************************************\n");
}

#define POOLSZ 2048
char chpool[POOLSZ];	/* CONSTANT and IDENTIFIER string pool */
int  avail = 0;		/* first free in chpool[] */

yylex()			/* lexical scanner */
{
     static char chbuf[] = "?";	/* for error reporting of 1 char tokens */
     int i, j, toktyp;

     /* skip whitespace */
     while ((nxtch==' ') || (nxtch=='\t') || (nxtch=='\n')) {
	  if (nxtch=='\n')
    	       ++yylineno;
	  nxtch = getc(inf);
     }
     if (nxtch == EOF) {
	  --yylineno;
	  return 0;
     }
     if (isdigit(nxtch)) {		/* numeric CONSTANT? */
	  toktyp = CONSTANT;
	  yyerrtok = yylval.oprnd = chpool + avail;
	  do {
	       chpool[avail++] = (char) nxtch;
	  } while (isdigit(nxtch=getc(inf)));
	  chpool[avail++] = '\0';
     }
     else if (isalpha(nxtch)) {		/* IDENTIFIER? */
	  toktyp = VARIABLE;
	  yyerrtok = yylval.oprnd = chpool + avail;
	  do {
	       chpool[avail++] = (char) nxtch;
	  } while (isalnum(nxtch=getc(inf)));
	  chpool[avail++] = '\0';
     }
     else {
	  toktyp = nxtch;
	  chbuf[0] = (char) nxtch;	/* for error reporting */
	  yyerrtok = chbuf;
	  nxtch = getc(inf);
     }
     return toktyp;
}
