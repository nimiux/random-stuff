
# line 8 "simple.y"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "errorlib.h"

FILE *inf;		/* input file */
FILE *outf;		/* output file */
char  outfn[] = "postfix.txt";		/* output file name */
long  start;		/* position of start of postfix expression */

int   yyparse(void);

# line 23 "simple.y"
typedef union  {
  char *oprnd;
} YYSTYPE;
#define YYSUNION /* %union occurred */
#define CONSTANT 257
#define VARIABLE 258
YYSTYPE yylval, yyval;
#define YYERRCODE 256

# line 63 "simple.y"


#define EOF_CHAR	'\032'		/* MS-DOS EOF character */

int  nxtch;	/* next input character - lookahead for lexical scanner */

main(argc, argv)
char *argv[];
{
     int status;	/* yyparse() status */

     fprintf(stdout, "\n*********************************************************\n");
     fprintf(stdout, "*   INTOPOST: INfix TO POSTfix expression translator    *\n");
     fprintf(stdout, "*                                                       *\n");
     fprintf(stdout, "*     Usage: intopost <infixfile>                       *\n");
     fprintf(stdout, "*     1) prepare a infix source file                    *\n");
     fprintf(stdout, "*        e.g. egfile                                    *\n");
     fprintf(stdout, "*        1+2*3;                                         *\n");
     fprintf(stdout, "*        9+8*7-6/5;                                     *\n");
     fprintf(stdout, "*        a+b*100;                                       *\n");
     fprintf(stdout, "*        use semicolon ; to terminate an expression     *\n");
     fprintf(stdout, "*     2) invoke intopost                                *\n");
     fprintf(stdout, "*        intopost egfile                                *\n");
     fprintf(stdout, "*     3) the result of translation is saved in          *\n");
     fprintf(stdout, "*        the file postfix.txt                           *\n");
     fprintf(stdout, "*                                                       *\n");
     fprintf(stdout, "*********************************************************\n\n\n");
     if (argc != 2) {
	  fprintf(stderr, "not enough arguments, abort \n");
	  exit(1);
     }

     /* open input file */
     if ((inf=fopen(argv[1], "r")) == NULL) {
	  fprintf(stderr, "Can't open file: \"%s\"\n", argv[1]);
	  exit(1);
     }
     strcpy(yyerrsrc, argv[1]);

     /* open output file */
     if ((outf=fopen(outfn, "w")) == NULL) {
	  fprintf(stderr, "Can't open file: \"%s\"\n", outfn);
	  exit(1);
     }

     fprintf(stdout, "translation in progress ... \n");

     /* initialize lexical scanner */
     nxtch = getc(inf);
     yylineno = 1;

     start = ftell(outf);	/* for semantic recovery */
     if ((status = yyparse()) != 0)
       fseek(outf, start, SEEK_SET);	/* backup over final error */
     fputc(EOF_CHAR, outf);	/* insure something written after backup */
     fclose(inf);
     fclose(outf);

     if (status) {
	  fprintf(stdout, "\n*********************************************************\n");
	  fprintf(stdout,   "*   INTOPOST: INfix TO POSTfix expression translator    *\n");
	  fprintf(stdout,   "*                                                       *\n");
	  fprintf(stdout,   "*     abnormal termination                              *\n");
	  fprintf(stdout,   "*     error in translation                              *\n");
	  fprintf(stdout,   "*     bye!                                              *\n");
	  fprintf(stdout,   "*                                                       *\n");
	  fprintf(stdout,   "*********************************************************\n");
	  exit(1);
     }

     fprintf(stdout, "\n*********************************************************\n");
     fprintf(stdout,   "*   INTOPOST: INfix TO POSTfix expression translator    *\n");
     fprintf(stdout,   "*                                                       *\n");
     fprintf(stdout,   "*     normal termination                                *\n");
     fprintf(stdout,   "*     see file postfix.txt for result                   *\n");
     fprintf(stdout,   "*     bye!                                              *\n");
     fprintf(stdout,   "*                                                       *\n");
     fprintf(stdout,   "*********************************************************\n");
}

#define POOLSZ 2048
char chpool[POOLSZ];	/* "identifier" and "constant" string pool */
int  avail = 0;		/* first free location in "chpool" */

yylex()			/* lexical scanner */
{
     static char chbuf[] = "?";
     int i, j, toktyp;

     /* skip whitespace */
     while ((nxtch==' ') || (nxtch=='\t') || (nxtch=='\n')) {
	  if (nxtch=='\n')
    	       ++yylineno;
	  nxtch = getc(inf);
     }
     if (nxtch == EOF) {
	  --yylineno;
	  return 0;
     }
     if (isdigit(nxtch)) {		/* numeric CONSTANT? */
	  toktyp = CONSTANT;
	  yyerrtok = yylval.oprnd = chpool + avail;
	  do {
	       chpool[avail++] = (char) nxtch;
	  } while (isdigit(nxtch=getc(inf)));
	  chpool[avail++] = '\0';
     }
     else if (isalpha(nxtch)) {		/* IDENTIFIER? */
	  toktyp = VARIABLE;
	  yyerrtok = yylval.oprnd = chpool + avail;
	  do {
	       chpool[avail++] = (char) nxtch;
	  } while (isalnum(nxtch=getc(inf)));
	  chpool[avail++] = '\0';
     }
     else {	/* single character operator (or mistake) */
	  toktyp = nxtch;
	  chbuf[0] = (char) nxtch;	/* for error reporting */
	  yyerrtok = chbuf;
	  nxtch = getc(inf);
     }
     return toktyp;
}
FILE *yytfilep;
char *yytfilen;
int yytflag = 0;
int svdprd[2];
char svdnams[2][2];

int yyexca[] = {
  -1, 1,
  0, -1,
  -2, 0,
  0,
};

#define YYNPROD 14
#define YYLAST 224

int yyact[] = {
       8,      19,      12,       8,      13,       8,      14,      12,
      15,      13,      24,       5,      12,      16,      13,       2,
       4,       9,      18,       1,       0,       0,       0,      11,
      17,       0,       0,      22,      23,      20,      21,       0,
       0,       0,       0,       0,       0,       0,       0,       0,
       0,       0,       0,       0,       0,       0,       0,       0,
       0,       0,       0,       0,       0,       0,       0,       0,
       0,       0,       0,       0,       0,       0,       0,       0,
       0,       0,       0,       0,       0,       0,       0,       0,
       0,       0,       0,       0,       0,       0,       0,       0,
       0,       0,       0,       0,       0,       0,       0,       0,
       0,       0,       0,       0,       0,       0,       0,       0,
       0,       0,       0,       0,       0,       0,       0,       0,
       0,       0,       0,       0,       0,       0,       0,       0,
       0,       0,       0,       0,       0,       0,       0,       0,
       0,       0,       0,       0,       0,       0,       0,       0,
       0,       0,       0,       0,       0,       0,       0,       0,
       0,       0,       0,       0,       0,       0,       0,       0,
       0,       0,       0,       0,       0,       0,       0,       0,
       0,       0,       0,       0,       0,       0,       0,       0,
       0,       0,       0,       0,       0,       0,       0,       0,
       0,       0,       0,       0,       0,       0,       0,       0,
       0,       0,       0,       0,       0,       0,       0,       0,
       0,       0,       0,       0,       0,       0,       0,       0,
       0,       0,       0,       0,       0,       0,       0,       0,
       0,       0,       0,       0,       0,       0,       0,       0,
       0,       0,       0,       0,       0,       0,       0,       0,
      10,       6,       7,       3,       6,       7,       6,       7,
};

int yypact[] = {
     -37,     -40,     -36,     -53,     -34,   -1000,   -1000,   -1000,
     -35,     -41,     -58,   -1000,     -35,     -35,   -1000,     -35,
     -35,     -31,   -1000,   -1000,     -34,     -34,   -1000,   -1000,
   -1000,
};

int yypgo[] = {
       0,      19,      15,      16,      11,
};

int yyr1[] = {
       0,       1,       1,       1,       1,       2,       2,       2,
       3,       3,       3,       4,       4,       4,
};

int yyr2[] = {
       0,       2,       3,       2,       3,       1,       3,       3,
       1,       3,       3,       1,       1,       3,
};

int yychk[] = {
   -1000,      -1,      -2,     256,      -3,      -4,     257,     258,
      40,      -2,     256,      59,      43,      45,      59,      42,
      47,      -2,      59,      59,      -3,      -3,      -4,      -4,
      41,
};

int yydef[] = {
       0,      -2,       0,       0,       5,       8,      11,      12,
       0,       0,       0,       1,       0,       0,       3,       0,
       0,       0,       2,       4,       6,       7,       9,      10,
      13,
};

/* 
 * YACCPAR.C - PCYACC LR parser driver routine for use with improved errorlib
 *
 *	(c) Copyright 1989, Abraxas Software, Inc.
*/

#include "errorlib.h"

#define yyerrok		errfl = 0
#define yyclearin	if (token == 0) return 1; else token = -1
#define YYFLAG -1000
#define YYERROR goto yyerrlab
#define YYACCEPT return(0)
#define YYABORT return(1)

#ifndef YYMAXDEPTH
#define YYMAXDEPTH	100
#endif

YYSTYPE yyv[YYMAXDEPTH];
int token = -1; /* input token */
int errct = 0;  /* error count */
int errfl = 0;  /* error flag */


int yyparse(void)
{
     int yys[YYMAXDEPTH];
     int yyj, yym;
     YYSTYPE *yypvt;
     int yystate, *yyps, yyn;
     YYSTYPE *yypv;
     int *yyxi;

#ifdef YYDEBUG
     printf("state 0\n");
#endif
     yystate = 0;
     token = -1;
     errct = 0;
     errfl = 0;
     yyps = yys - 1;
     yypv = yyv - 1;

yystack:    /* put a state and value onto the stack */
     if ( ++yyps > yys + YYMAXDEPTH ) {
       yyerror( "yacc stack overflow", 0 );
       return(1);
     }
     *yyps = yystate;
     ++yypv;
     *yypv = yyval;

yynewstate:
     yyn = yypact[yystate];

     if ( yyn <= YYFLAG )
	  goto yydefault; /* simple state with only default action */

     if ( token < 0 ) {		/* no lookahead token? */
	  if ( (token = yylex()) < 0 )
	       token = 0;
     }
     yyn += token;
     if ( yyn < 0 || yyn >= YYLAST )	/* no entry within table? */
	  goto yydefault;	/* simple state with only default action */

     if ( yychk[yyn = yyact[yyn]] == token ) {	/* valid shift? */
#ifdef YYDEBUG
	  printf("shift on [%s] to state %d\n", yydisplay(token), yyn);
#endif
	  token = -1;		/* lookahead token is read */
	  yyval = yylval;
	  yystate = yyn;
	  if ( errfl > 0 )	/* still in error state? */
	       --errfl;
	  goto yystack;
     }

yydefault:	/* simple state with only default action */
     if ( (yyn = yydef[yystate]) == -2 ) {
	  if ( token < 0 ) {	/* no lookahead token? */
	       if ( (token = yylex()) < 0 )
		    token = 0;
	  }

          /* look through exception table */
          for ( yyxi = yyexca; *yyxi != -1 || yyxi[1] != yystate ; yyxi += 2 )
	       ;		/* VOID */
          while( *(yyxi += 2) >= 0 ) {
	       if ( *yyxi == token )
	            break;
          }
          if ( (yyn = yyxi[1]) < 0 ) {
#ifdef YYDEBUG
	       printf("accept\n");
#endif
	       return 0;   /* accept */
	  }
     }

     if ( yyn == 0 ) {		/* error? */
	  switch( errfl ) {
	       case 0:   /* brand new error */
		    yyerror( "syntax error", yydisplay( token ));
		    if ( (yyn = yypact[yystate]) > YYFLAG && yyn < YYLAST) {
			 register int x;

			 for ( x = (yyn > 0) ? yyn : 0; x < YYLAST; ++x ) {
			      if ( yychk[yyact[x]] == x - yyn &&
					x - yyn != YYERRCODE )
				   yyerror( 0, yydisplay( x - yyn ));
			 }
		    }
		    yyerror( 0, 0 );
yyerrlab:
		    ++errct;

	  case 1:
	  case 2: /* incompletely recovered error ... try again */
		    errfl = 3;

		    /* find a state where "error" is a legal shift action */
		    while ( yyps >= yys ) {
			 yyn = yypact[*yyps] + YYERRCODE;
			 if ( yyn >= 0 && yyn < YYLAST &&
					yychk[yyact[yyn]] == YYERRCODE ){
			      /* simulate a shift of "error" */
			      yystate = yyact[yyn];
#ifdef YYDEBUG
			      printf("shift on [error] to state %d\n",
				    yystate);
#endif
			      goto yystack;
			 }
		         yyn = yypact[*yyps];
			 /* the current yyps has no shift on "error",
				pop stack */
			 --yyps;
			 --yypv;
#ifdef YYDEBUG
			printf("error: pop state %d, uncovering state %d\n",
			      yyps[1], yyps[0]);
#endif
		    }

	    /* there is no state on the stack with an error shift ... abort */
yyabort:

#ifdef YYDEBUG
		    printf("abort\n");
#endif
		    return 1;

	       case 3:  /* no shift yet; clobber input char */
#ifdef YYDEBUG
		    printf("error: discard [%s]\n", yydisplay(token));
#endif
		    if ( token == 0 )	/* EOF? */
			 goto yyabort;		/* don't discard EOF, quit */
		    token = -1;
		    goto yynewstate;   /* try again in the same state */
	  }
     }

     /* reduction by production yyn */
     yyps -= yyr2[yyn];
     yypvt = yypv;
     yypv -= yyr2[yyn];
     yyval = yypv[1];
     yym = yyn;
     /* consult goto table to find next state */
     yyn = yyr1[yyn];
     yyj = yypgo[yyn] + *yyps + 1;
     if ( yyj >= YYLAST || yychk[yystate = yyact[yyj]] != -yyn )
	  yystate = yyact[yypgo[yyn]];
#ifdef YYDEBUG
     printf("reduce w/ rule %d, uncover state %d, go to state %d\n", yym,
	  *yyps, yystate);
#endif
     switch (yym) {
	  
case 1:
# line 34 "simple.y"
{ fprintf(outf, " ;\n");  start = ftell(outf); } break;
case 2:
# line 36 "simple.y"
{ fprintf(outf, " ;\n");  start = ftell(outf); } break;
case 3:
# line 38 "simple.y"
{ yyerrok;  fseek(outf, start, SEEK_SET); } break;
case 4:
# line 40 "simple.y"
{ yyerrok;  fseek(outf, start, SEEK_SET); } break;
case 6:
# line 45 "simple.y"
{ fprintf(outf, " +"); } break;
case 7:
# line 47 "simple.y"
{ fprintf(outf, " -"); } break;
case 9:
# line 52 "simple.y"
{ fprintf(outf, " *"); } break;
case 10:
# line 54 "simple.y"
{ fprintf(outf, " /"); } break;
case 11:
# line 58 "simple.y"
{ fprintf(outf, " %s", yypvt[-0].oprnd); } break;
case 12:
# line 60 "simple.y"
{ fprintf(outf, " %s", yypvt[-0].oprnd); } break;
     }
     goto yystack;  /* stack new state and value */
}
