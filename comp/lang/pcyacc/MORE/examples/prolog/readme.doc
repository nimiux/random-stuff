
LOREN COBB, CORRALES SOFTWARE, CORRALES, NM.

DESCRIPTION:  A Yacc grammar specification for
Prolog programs, based on the informal description
of the Edinburgh syntax in "Programming in Prolog",
by Clocksin & Mellish, 2nd Ed, Springer, 1984.

DATE:     26 September, 1988.
REVISED:   7 October,   1988.

NOTE 1:  New operators declared with the "op"
predicate cannot be recognized by Yacc-generated
compilers or interpreters.

NOTE 2:  For maximum flexibility, built-in functions
(append, etc.) should be recognized and evaluated at
EXECUTION time, not at COMPILE time. Similarly, the
compiler should translate infix operator expressions,
e.g. a+b, into functorial form, e.g. +(a,b).  The
execution-time interpreter should then be responsible
for evaluating all functorial expressions.

NOTE 3:  The only reserved words in this grammar
are the following: is, not, mod, spy, nospy.

NOTE 4:  The lexical analyzer (yylex) supplied here
does not recognize double quotes ("), and supports
single quotes (') only when they are used to dis-
tinguish capitalized atoms from variables.

NOTE 5:  Remove the definition of myDebug to suppress
the printing of debug messages. To obtain even more
detailed debug messages, insert a line that reads
#define YYDEBUG, and use the -v option on the yacc
call.


