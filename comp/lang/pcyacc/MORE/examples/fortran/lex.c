
/*
=====================================================================
=====================================================================
*/

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "global.h"
#include "f77.h"

/*--------------------------------------------------------------------
  region boundary constants, etc.
--------------------------------------------------------------------*/
#define COMCOL 1
#define LABMIN 1
#define LABMAX 5
#define CONCOL 6
#define STAMIN 7
#define STAMAX 72

#define TOKMAX 16
#define VALMAX 128

#define STAR	'*'

/*--------------------------------------------------------------------
  globals, externals
--------------------------------------------------------------------*/
extern int informat;		/* scanning a format statement */
int  scount;			/* current scanning position */
char tv[VALMAX];

extern FILE *fin;
int lexdebug  = 0;
int lexdebug2 = 0;

/*--------------------------------------------------------------------
  keywords and search routine, etc.
--------------------------------------------------------------------*/
typedef struct {
  char	kname[TOKMAX];
  int	kval;
} tab;

tab kwdtable[] = {
		"ASSIGN",		ASSIGN,
		"assign",		ASSIGN,
		"BACKSPACE",		BACKSPACE,
		"backspace",		BACKSPACE,
		"BLOCK",		BLOCK,
		"block",		BLOCK,
		"CALL",			CALL,
		"call",			CALL,
		"CLOSE",		CLOSE,
		"close",		CLOSE,
		"COMMON",		COMMON,
		"common",		COMMON,
		"CONTINUE",		CONTINUE,
		"continue",		CONTINUE,
		"DATA",			DATA,
		"data",			DATA,
		"DIMENSION",		DIMENSION,
		"dimension",		DIMENSION,
		"DO",			DO,
		"do",			DO,
		"ELSE",			ELSE,
		"else",			ELSE,
		"ELSEIF",		ELSEIF,
		"elseif",		ELSEIF,
		"END",			END,
		"end",			END,
		"ENDFILE",		ENDFILE,
		"endfile",		ENDFILE,
		"ENDIF",		ENDIF,
		"endif",		ENDIF,
		"ENDTRY",		ENTRY,
		"entry",		ENTRY,
		"EQUIVALENCE",		EQUIVALENCE,
		"equivalence",		EQUIVALENCE,
		"EXTERNAL",		EXTERNAL,
		"external",		EXTERNAL,
		"FORMAT",		FORMAT,
		"format",		FORMAT,
		"FUNCTION",		FUNCTION,
		"function",		FUNCTION,
		"GO",			GO,
		"go",			GO,
		"GOTO",			GOTO,
		"goto",			GOTO,
		"IF",			IF,
		"if",			IF,
		"IMPLICIT",		IMPLICIT,
		"implicit",		IMPLICIT,
		"INQUIRE",		INQUIRE,
		"inquire",		INQUIRE,
		"INTRINSIC",		INTRINSIC,
		"intrinsic",		INTRINSIC,
		"OPEN",			OPEN,
		"open",			OPEN,
		"PARAMETER",		PARAMETER,
		"parameter",		PARAMETER,
		"PAUSE",		PAUSE,
		"pause",		PAUSE,
		"PRINT",		PRINT,
		"print",		PRINT,
		"PROGRAM",		PROGRAM,
		"program",		PROGRAM,
		"READ",			READ,
		"read",			READ,
		"RETURN",		RETURN,
		"return",		RETURN,
		"REWIND",		REWIND,
		"rewind",		REWIND,
		"SAVE",			SAVE,
		"save",			SAVE,
		"STOP",			STOP,
		"stop",			STOP,
		"SUBROUTINE",		SUBROUTINE,
		"subroutine",		SUBROUTINE,
		"THEN",			THEN,
		"then",			THEN,
		"TO",			TO,
		"to",			TO,
		"WRITE",		WRITE,
		"write",		WRITE,
		"CHARACTER",		CHARACTER,
		"character",		CHARACTER,
		"COMPLEX",		COMPLEX,
		"complex",		COMPLEX,
		"DOUBLE",		DOUBLE,
		"double",		DOUBLE,
		"INTEGER",		INTEGER,
		"integer",		INTEGER,
		"LOGICAL",		LOGICAL,
		"logical",		LOGICAL,
		"PRECISION",		PRECISION,
		"precision",		PRECISION,
		"REAL",			REAL,
		"real",			REAL,
		"FALSE",		_FALSE,
		"false",		_FALSE,
		"TRUE",			_TRUE,
		"true",			_TRUE,
		"ACCESS",		ACCESS,
		"access",		ACCESS,
		"BLANK",		BLANK,
		"blank",		BLANK,
		"DIRECT",		DIRECT,
		"direct",		DIRECT,
		"ERR",			ERR,
		"err",			ERR,
		"EXIST",		EXIST,
		"exist",		EXIST,
		"FILE",			_FILE,
		"file",			_FILE,
		"FMT",			FMT,
		"fmt",			FMT,
		"FORM",			FORM,
		"form",			FORM,
		"FORMATTED",		FORMATTED,
		"formatted",		FORMATTED,
		"IOSTAT",		IOSTAT,
		"iostat",		IOSTAT,
		"NAME",			NAME,
		"name",			NAME,
		"NAMED",		NAMED,
		"named",		NAMED,
		"NEXTREC",		NEXTREC,
		"nextrec",		NEXTREC,
		"NUMBER",		NUMBER,
		"number",		NUMBER,
		"OPENED",		OPENED,
		"opened",		OPENED,
		"REC",			REC,
		"rec",			REC,
		"RECL",			RECL,
		"recl",			RECL,
		"SEQUENTIAL",		SEQUENTIAL,
		"sequential",		SEQUENTIAL,
		"STATUS",		STATUS,
		"status",		STATUS,
		"UNFORMATTED",		UNFORMATTED,
		"unformatted",		UNFORMATTED,
		"UNIT",			UNIT,
		"unit",			UNIT,
		"AND",			AND,
		"and",			AND,
		"EQ",			EQ,
		"eq",			EQ,
		"EQV",			EQV,
		"eqv",			EQV,
		"GE",			GE,
		"ge",			GE,
		"GT",			GT,
		"gt",			GT,
		"LE",			LE,
		"le",			LE,
		"LT",			LT,
		"lt",			LT,
		"NE",			NE,
		"ne",			NE,
		"NEQV",			NEQV,
		"neqv",			NEQV,
		"OR",			OR,
		"or",			OR,
		"NOT",			NOT,
		"not",			NOT,
		"$EOT",			IDENTIFIER,
};

tab fmtable[] = {
		"A",			A,
		"a",			A,
		"BN",			BN,
		"bn",			BN,
		"BZ",			BZ,
		"bz",			BZ,
		"D",			D,
               "d",			D,
		"E",			E,
		"e",			E,
		"F",			F,
		"f",			F,
		"G",			G,
		"g",			G,
		"I",			I,
		"i",			I,
		"L",			L,
		"l",			L,
		"P",			P,
		"p",			P,
		"S",			S,
		"s",			S,
		"SP",			SP,
		"sp",			SP,
		"SS",			SS,
		"ss",			SS,
		"T",			T,
		"t",			T,
		"TL",			TL,
		"tl",			TL,
		"TR",			TR,
		"tr",			TR,
		"X",			X,
		"x",			X,
		"$EOT",			IDENTIFIER,
};

search(t, v)
tab   t[];
char *v;
{
  int i;

  for (i=0; strcmp("$EOT", t[i].kname); i++) {
    if (!strcmp(v, t[i].kname)) break;
  }
  return (t[i].kval);
}

/*--------------------------------------------------------------------
  region test & set functions
--------------------------------------------------------------------*/

iscomreg()
{
  return (scount == COMCOL);
}

islabreg()
{
  return ((scount >= LABMIN) && (scount <= LABMAX));
}

isconreg()
{
  return (scount == CONCOL);
}

isstareg()
{
  return ((scount >= STAMIN) && (scount <= STAMAX));
}

iscom(c)
char c;
{
  return (c == STAR || c == 'c' || c == 'C');
}

notcon(c)
char c;
{
  return (c == ' ' || c == '0');
}

incscount()
{
  scount++;
  if (lexdebug2) printf("scanning position: %d\n", scount);
}

yylex()
{
  int t;

  t = yylex1();
  if (lexdebug) printf("tok: %d, val: %s\n", t, yylval.sval);
  return (t);
}

yylex1()
{
  int  c, i;
  int  done;
  int  tt;

  c = getc(fin);
  incscount();
  while (isspace(c) || isconreg()) { /* skip space or continuation symbol */
    if (c != '\n') incscount();
    else scount = 1;
    c = getc(fin);
  }
  if (iscomreg() && iscom(c)) { /* skip comment line */
    c = getc(fin);
    while (c != '\n') c = getc(fin);
    scount = 0;
    return (yylex1());
  }
  if (c == EOF) return (0);

  if (islabreg() && isdigit(c)) {
    i = 0;
    while (islabreg()) {
      tv[i++] = c;
      c = getc(fin);
      incscount();
    }
    if (! notcon(c)) return (yylex1()); /* skip stuff before a continuation */
    tv[i++] = '\0';
    return (INT_CONST);
  } else if (isstareg()) {
    if (c == '\'') {
      i = 0;
      tv[i++] = '\"';
      done = FALSE;
      c = getc(fin);
      incscount();
      while (! done) {
        if (c == '\n') {		/* string can't cross lines for now */
          done = TRUE;
          scount = 0;
        } else if (c == '\'') {
          c = getc(fin);
          if (c != '\'') {		/* end of string */
            done = TRUE;
            ungetc(c, fin);
          } else {			/* an ' within a string */
            tv[i++] = c;
            c = getc(fin);
            scount += 2;
          }
        } else {
          tv[i++] = c;
          c = getc(fin);
          incscount();
        }				/* end if */
      }				/* end while */
      tv[i++] = '\"';
      tv[i++] = '\0';
      return (STRING);
    } else if (isdigit(c)) {
      i = 0;
      tt = INT_CONST;
      while (isdigit(c)) {
        tv[i++] = c;
        c = getc(fin);
        incscount();
      }
      if (c == '.') {
        tt = REAL_CONST;
        tv[i++] = '.';
        c = getc(fin);
        incscount();
        while (isdigit(c)) {
          tv[i++] = c;
          c = getc(fin);
          incscount();
        }
      } 
      ungetc(c, fin);
      scount--;
      tv[i++] = '\0';
      return (tt);
    } else if (c == '.') {
      c = getc(fin);
      incscount();
      i = 0;
      if (isdigit(c)) {		/*  a real constant */
        tv[i++] = '.';
        while (isdigit(c)) {
          tv[i++] = c;
          c = getc(fin);
          incscount();
        }
        ungetc(c, fin);
        scount--;
        tv[i++] = '\0';
        return(REAL_CONST);
      } else if (isalpha(c)) {		/* a dot delimited key word */
        while (isalpha(c)) {
          tv[i++] = islower(c) ? toupper(c) : c ;
          c = getc(fin);
          incscount();
        }
        if (c != '.') {
          ungetc(c, fin);
          scount--;
        }
        tv[i++] = '\0';
        return (search(kwdtable, tv));
      } else return ('.');
    } else if (isalpha(c)) {		/* an identifier or r wrod */
      i = 0;
      while (isalnum(c)) {
        tv[i++] = c;
        c = getc(fin);
        incscount();
      }
      ungetc(c, fin);
      scount--;
      tv[i++] = '\0';
      if (informat) return (search(fmtable, tv));
      return (search(kwdtable, tv));
    } else if (c == '*') {
      c = getc(fin);
      if (c == '*') {
        incscount();
        return (POW);
      } else {
        ungetc(c, fin);
        return ('*');
      }
    } else if (c == '/') {
      c = getc(fin);
      if (c == '/') {
        incscount();
        return (CAT);
      } else {
        ungetc(c, fin);
        return ('/');
      }
    } else {
      return (c);
    }
  }
  return (UNKNOWN);
}

/*
setreg()
{
  regflag = NONREG;
  if (iscomreg()) regflag |= COMREG;
  if (islabreg()) regflag |= LABREG;
  if (isconreg()) regflag |= CONREG;
  if (isstareg()) regflag |= STAREG;
}

peekline()
{
  int c, cc;

  fillarray(peekbuf, PEEKMAX, BLANK);
  cc = 0;
  c = getc(fin);
  while (cc < PEEKMAX && c != EOF && c != NEWLINE) {
    peekbuf[cc++] = c;
    c = getc(fin);
  }
  if (c == EOF) return(EOF);	/* in this case, extra chars are ignored
  return (cc);
}

getln()
{
  int c, cc, continue;

  if (peekflag == EOF) return(EOF);
  if (peekflag == PEEKMAX) 	/* there is something in the peek buffer
    for (cc=0; cc<PEEKMAX; cc++) linebuf[cc] = peekbuf[cc];
  else cc = 0;
  continue = TRUE;
  c = getc(fin);
  while (continue && c != EOF) {
    if (c != NEWLINE) {
      linbuf[cc++] = c;
      c = getc(fin);
    } else {
      peekflag = peekline();
      if (peekbuf[COMCOL] == STAR ||
          peekbuf[COMCOL] == 'c'  ||
          peekbuf[COMCOL] == 'C'  ||
         (peekbuf[CONCOL] != BLANK &&
          peekbuf[CONCOL] != '0')) continue = FALSE;
      else c = getc(fin);
    }
  }
  if (c == EOF) if (cc == 0) return(EOF); else peekflag = EOF;
  return(cc);
}
*/

