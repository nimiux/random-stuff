
/*
=====================================================================
=====================================================================
*/

%{

#include "global.h"

/* some global control variable */
int informat  = FALSE;

%}

%union {
  int   ival;
  char *sval;
}

%token ASSIGN
%token BACKSPACE
%token BLOCK
%token CALL
%token CLOSE
%token COMMON
%token CONTINUE
%token DATA
%token DIMENSION
%token DO
%token ELSE
%token ELSEIF
%token END
%token ENDFILE
%token ENDIF
%token ENTRY
%token EQUIVALENCE
%token EXTERNAL
%token FORMAT
%token FUNCTION
%token GO
%token GOTO
%token IF
%token IMPLICIT
%token INQUIRE
%token INTRINSIC
%token OPEN
%token PARAMETER
%token PAUSE
%token PRINT
%token PROGRAM
%token READ
%token RETURN
%token REWIND
%token SAVE
%token STOP
%token SUBROUTINE
%token THEN
%token TO
%token WRITE

%token CHARACTER
%token COMPLEX
%token DOUBLE
%token INTEGER
%token LOGICAL
%token PRECISION
%token REAL

%token _FALSE
%token _TRUE
%token H_STRING
%token IDENTIFIER
%token INT_CONST
%token REAL_CONST
%token STRING

%token ACCESS
%token BLANK
%token DIRECT
%token ERR
%token EXIST
%token _FILE
%token FMT
%token FORM
%token FORMATTED
%token IOSTAT
%token NAME
%token NAMED
%token NEXTREC
%token NUMBER
%token OPENED
%token REC
%token RECL
%token SEQUENTIAL
%token STATUS
%token UNFORMATTED
%token UNIT

%token A
%token BN
%token BZ
%token D
%token E
%token F
%token G
%token I
%token L
%token P
%token S
%token SP
%token SS
%token T
%token TL
%token TR
%token X

%token AND
%token CAT
%token EQ
%token EQV
%token GE
%token GT
%token LE
%token LT
%token NE
%token NEQV
%token OR
%token POW

%type <sval> IDENTIFIER
%type <sval> STRING
%type <sval> INT_CONST
%type <sval> REAL_CONST

%left WAIT
%left INT_CONST IDENTIFIER REAL

%left EQV NEQV
%left OR
%left AND
%left EQ NE LE LT GE GT

%left '+' '-' CAT
%left '*' '/'

%right NEG POS NOT
%right POW

%start f77prog

%%

/*--------------------------------------------------------------------
  f77program: zero or more program units
--------------------------------------------------------------------*/

f77prog
  :
  | f77prog prog_unit
  ;

/*--------------------------------------------------------------------
  program unit
--------------------------------------------------------------------*/

prog_unit
  : main_program
  | function_subprogram
  | subroutine_subprogram
  | block_data_subprogram
  ;

program_statement
  : PROGRAM IDENTIFIER
  ;

function_statement
  :           FUNCTION IDENTIFIER
  |           FUNCTION IDENTIFIER '('            ')'
  |           FUNCTION IDENTIFIER '(' IDENTIFIER ')'
  |           FUNCTION IDENTIFIER '(' IDENTIFIER ',' identifier_list ')'
  | type_spec FUNCTION IDENTIFIER
  | type_spec FUNCTION IDENTIFIER '('            ')'
  | type_spec FUNCTION IDENTIFIER '(' IDENTIFIER ')'
  | type_spec FUNCTION IDENTIFIER '(' IDENTIFIER ',' identifier_list ')'
  ;

subroutine_statement
  : SUBROUTINE IDENTIFIER
  | SUBROUTINE IDENTIFIER '('            ')'
  | SUBROUTINE IDENTIFIER '(' IDENTIFIER ')'
  | SUBROUTINE IDENTIFIER '(' IDENTIFIER ',' identifier_list ')'
  ;

block_data_statement
  : BLOCK DATA		%prec WAIT
  | BLOCK DATA IDENTIFIER
  ;

label
  :
  | INT_CONST
  ;

labeled_end_statement
  : label end_statement
  ;

end_statement
  : END
  ;

main_program
  :                   program_body labeled_end_statement
  | program_statement program_body labeled_end_statement
  ;

function_subprogram
  : function_statement program_body labeled_end_statement
  ;

subroutine_subprogram
  : subroutine_statement program_body labeled_end_statement
  ;

block_data_subprogram
  : block_data_statement program_body labeled_end_statement
  ;

program_body
  : labeled_statement
  | program_body labeled_statement
  ;

labeled_statement
  : label statement
  ;

statement
  : common_statement
  | data_statement
  | dimension_statement
  | entry_statement
  | equivalence_statement
  | executable_statement
  | external_statement
  | format_statement
  | implicit_statement
  | intrinsic_statement
  | parameter_statement
  | save_statement
  | statement_function_statement
  | type_statement
  ;

/*--------------------------------------------------------------------
  common statement
--------------------------------------------------------------------*/

common_statement
  : COMMON common_list
  | COMMON common_lists
  ;

common_lists
  : '/' IDENTIFIER '/' common_list
  | common_lists '/' IDENTIFIER '/' common_list
  ;

common_list
  : common_entity
  | common_list ',' common_entity
  ;

common_entity
  : IDENTIFIER
  | array_declarator
  ;

/*--------------------------------------------------------------------
  data statement
--------------------------------------------------------------------*/

data_statement
  : DATA datanames datavalues
  ;

datanames
  : dataname
  | datanames ',' dataname
  ;

dataname
  : IDENTIFIER
  | IDENTIFIER '(' expression_list ')'
  | data_implied_do_list
  ;

datavalues
  : datavalue
  | datavalues ',' datavalue
  ;

datavalue
  : '/' datavalue_list '/'
  ;

datavalue_list
  : value '*' value
  ;

value
  : constant
  | IDENTIFIER
  ;

data_implied_do_list
  : implied_do_list
  ;

implied_do_list
  : '(' do_list ',' IDENTIFIER '=' expression ')'
  | '(' do_list ',' IDENTIFIER '=' expression ',' expression ')'
  ;

do_list
  : do_item
  | do_list ',' do_item
  ;

do_item
  : IDENTIFIER
  | IDENTIFIER '(' expression_list ')'
  ;

/*--------------------------------------------------------------------
  dimension statement
--------------------------------------------------------------------*/

dimension_statement
  : DIMENSION array_declarators
  ;

array_declarators
  : array_declarator
  | array_declarators ',' array_declarator
  ;

array_declarator
  : IDENTIFIER '(' dim_bound_list ')'
  ;

dim_bound_list
  : dim_bound
  | dim_bound_list ',' dim_bound
  ;

dim_bound
  : '*'
  | expression
  | expression ':' expression
  ;

/*--------------------------------------------------------------------
  entry statement
--------------------------------------------------------------------*/

entry_statement
  : ENTRY IDENTIFIER
  | ENTRY IDENTIFIER '('            ')'
  | ENTRY IDENTIFIER '(' IDENTIFIER ')'
  | ENTRY IDENTIFIER '(' IDENTIFIER ',' identifier_list ')'
  ;

/*--------------------------------------------------------------------
  equivalence statement
--------------------------------------------------------------------*/

equivalence_statement
  : EQUIVALENCE equivalence_lists
  ;

equivalence_lists
  : equivalence_list
  | equivalence_lists ',' equivalence_list
  ;

equivalence_list
  : '(' equivalence_entities ')'
  ;

equivalence_entities
  : equivalence_entity
  | equivalence_entities ',' equivalence_entity
  ;

equivalence_entity
  : IDENTIFIER
  | IDENTIFIER '(' expression_list ')'
  ;

/*--------------------------------------------------------------------
  external statement
--------------------------------------------------------------------*/

external_statement
  : EXTERNAL identifier_list
  ;

identifier_list
  : IDENTIFIER
  | identifier_list ',' IDENTIFIER
  ;

/*--------------------------------------------------------------------
  implicit statement
---------------------------------------------------------------------*/

implicit_statement
  : IMPLICIT implicit_lists
  ;

implicit_lists
  : implicit_list
  | implicit_lists ',' implicit_list
  ;

implicit_list
  : type_spec '(' implicit_specs ')'
  ;

implicit_specs
  : implicit_spec
  | implicit_specs ',' implicit_spec
  ;

implicit_spec
  : IDENTIFIER
  | IDENTIFIER '-' IDENTIFIER
  ;

/*--------------------------------------------------------------------
  intrinsic statement
--------------------------------------------------------------------*/

intrinsic_statement
  : INTRINSIC identifier_list
  ;

/*--------------------------------------------------------------------
  parameter statement
--------------------------------------------------------------------*/

parameter_statement
  : PARAMETER '(' parameter_specs ')'
  ;

parameter_specs
  : parameter_spec
  | parameter_specs ',' parameter_spec
  ;

parameter_spec
  : IDENTIFIER '=' expression
  ;

/*--------------------------------------------------------------------
  save statement
--------------------------------------------------------------------*/

save_statement
  : SAVE saved_entities
  ;

saved_entities
  : saved_entity
  | saved_entities ',' saved_entity
  ;

saved_entity
  : IDENTIFIER
  | '/' IDENTIFIER '/'
  ;

/*--------------------------------------------------------------------
  statement function statement
---------------------------------------------------------------------*/

statement_function_statement
  : IDENTIFIER '('            ')' '=' expression
  | IDENTIFIER '(' IDENTIFIER ')' '=' expression
  | IDENTIFIER '(' IDENTIFIER ',' identifier_list ')' '=' expression
  ;

/*--------------------------------------------------------------------
  type statement
--------------------------------------------------------------------*/

type_statement
  : type_spec typed_entities
  ;

type_spec
  : INTEGER
  | REAL
  | DOUBLE PRECISION
  | COMPLEX
  | LOGICAL
  | CHARACTER
  | CHARACTER '*' length_spec
  | CHARACTER '*' length_spec ','
  ;

typed_entities
  : typed_entity
  | typed_entities ',' typed_entity
  ;

typed_entity
  : IDENTIFIER
  | IDENTIFIER '*' length_spec
  | array_declarator
  | array_declarator '*' length_spec
  ;

length_spec
  : '(' '*'        ')'
  | '(' expression ')'
  | INT_CONST
  ;

/*--------------------------------------------------------------------
  executable statement starts here
--------------------------------------------------------------------*/

executable_statement
  : arithmetic_if_statement
  | assignment_statement
  | backspace_statement
  | block_if_statement
  | call_statement
  | close_statement
  | continue_statement
  | do_statement
  | else_if_statement
  | else_statement
  | end_if_statement
  | endfile_statement
  | goto_statement
  | inquire_statement
  | logical_if_statement
  | open_statement
  | pause_statement
  | print_statement
  | read_statement
  | return_statement
  | rewind_statement
  | stop_statement
  | write_statement
  ;

/*--------------------------------------------------------------------
  arithmetic if
--------------------------------------------------------------------*/

arithmetic_if_statement
  : IF '(' expression ')' INT_CONST ',' INT_CONST ',' INT_CONST
  ;

/*--------------------------------------------------------------------
  assignment
--------------------------------------------------------------------*/

assignment_statement
  : IDENTIFIER '=' expression
  | IDENTIFIER '(' expression_list ')' '=' expression
  | ASSIGN INT_CONST TO IDENTIFIER
  ;

/*--------------------------------------------------------------------
  backspace
--------------------------------------------------------------------*/

backspace_statement
  : BACKSPACE expression
  | BACKSPACE '(' io_info_list ')'
  ;

/*--------------------------------------------------------------------
  block if
--------------------------------------------------------------------*/

block_if_statement
  : IF '(' expression ')' THEN
  ;

/*--------------------------------------------------------------------
  call
--------------------------------------------------------------------*/

call_statement
  : CALL IDENTIFIER
  | CALL IDENTIFIER '(' expression_list ')'
  | CALL IDENTIFIER '('                 ')'
  ;

/*--------------------------------------------------------------------
  close
--------------------------------------------------------------------*/

close_statement
  : CLOSE '(' expression ')'
  | CLOSE '(' io_info_list ')'
  ;

/*--------------------------------------------------------------------
  continue
--------------------------------------------------------------------*/

continue_statement
  : CONTINUE
  ;

/*--------------------------------------------------------------------
  do
--------------------------------------------------------------------*/

do_statement
  : DO INT_CONST     IDENTIFIER '=' expression ',' expression
  | DO INT_CONST ',' IDENTIFIER '=' expression ',' expression
  | DO INT_CONST     IDENTIFIER '=' expression ',' expression ',' expression
  | DO INT_CONST ',' IDENTIFIER '=' expression ',' expression ',' expression
  ;

/*--------------------------------------------------------------------
  else if
--------------------------------------------------------------------*/

else_if_statement
  : elseif '(' expression ')' THEN
  ;

elseif
  : ELSE IF
  | ELSEIF
  ;

/*--------------------------------------------------------------------
  else
---------------------------------------------------------------------*/

else_statement
  : ELSE
  ;

/*--------------------------------------------------------------------
  end if
--------------------------------------------------------------------*/

end_if_statement
  : END IF
  | ENDIF
  ;

/*--------------------------------------------------------------------
  end file
--------------------------------------------------------------------*/

endfile_statement
  : ENDFILE expression
  | ENDFILE '(' io_info_list ')'
  ;

/*--------------------------------------------------------------------
 goto's
--------------------------------------------------------------------*/

goto_statement
  : unconditional_goto
  | computed_goto
  | assigned_goto
  ;

unconditional_goto
  : goto INT_CONST
  ;

computed_goto
  : goto '(' int_const_list ')'     expression
  | goto '(' int_const_list ')' ',' expression
  ;

assigned_goto
  : goto IDENTIFIER
  | goto IDENTIFIER     '(' int_const_list ')'
  | goto IDENTIFIER ',' '(' int_const_list ')'
  ;

int_const_list
  : INT_CONST
  | int_const_list ',' INT_CONST
  ;

goto
  : GOTO
  | GO TO
  ;

/*--------------------------------------------------------------------
  inquire
--------------------------------------------------------------------*/

inquire_statement
  : INQUIRE '(' expression ')'
  | INQUIRE '(' io_info_inquire_list ')'
  ;


io_info_inquire_list
  : io_info_inquire_item
  | io_info_inquire_list ',' io_info_inquire_item
  ;

io_info_inquire_item
  : UNIT        '=' expression
  | _FILE       '=' expression
  | ERR         '=' INT_CONST
  | IOSTAT      '=' inquire_name
  | EXIST       '=' inquire_name
  | OPENED      '=' inquire_name
  | NUMBER      '=' inquire_name
  | NAMED       '=' inquire_name
  | NAME        '=' inquire_name
  | ACCESS      '=' inquire_name
  | SEQUENTIAL  '=' inquire_name
  | DIRECT      '=' inquire_name
  | FORM        '=' inquire_name
  | FORMATTED   '=' inquire_name
  | UNFORMATTED '=' inquire_name
  | RECL        '=' inquire_name
  | NEXTREC     '=' inquire_name
  | BLANK       '=' inquire_name
  ;

inquire_name
  : IDENTIFIER
  | IDENTIFIER '(' expression_list ')'
  ;

/*--------------------------------------------------------------------
  logical if
--------------------------------------------------------------------*/

logical_if_statement
  : IF '(' expression ')' executable_statement
  ;

/*--------------------------------------------------------------------
  open
--------------------------------------------------------------------*/

open_statement
  : OPEN '(' expression ')'
  | OPEN '(' io_info_list ')'
  ;

/*--------------------------------------------------------------------
  pause
--------------------------------------------------------------------*/

pause_statement
  : PAUSE		%prec WAIT
  | PAUSE INT_CONST
  | PAUSE STRING
  ;

/*--------------------------------------------------------------------
  print
--------------------------------------------------------------------*/

print_statement
  : PRINT format_identifier
  | PRINT format_identifier ',' io_list
  ;

/*--------------------------------------------------------------------
  read
--------------------------------------------------------------------*/

read_statement
  : READ '*' ','                   io_list
  | READ '(' control_info_list ')'		%prec WAIT
  | READ '(' control_info_list ')' io_list
  ;

control_info_list
  : expression ',' format_identifier
  | expression ',' io_info_list
  ;

io_info_list
  : io_info_item
  | io_info_list ',' io_info_item
  ;

io_info_item
  : FMT    '=' format_identifier
  | UNIT   '=' expression
  | REC    '=' expression
  | END    '=' INT_CONST
  | ERR    '=' INT_CONST
  | IOSTAT '=' IDENTIFIER
  | IOSTAT '=' IDENTIFIER '(' expression_list ')'
  | _FILE  '=' expression
  | STATUS '=' expression
  | ACCESS '=' expression
  | FORM   '=' expression
  | RECL   '=' expression
  | BLANK  '=' expression
  ;

io_list
  : io_item
  | io_list ',' io_item
  ;

io_item
  : expression
  | io_implied_do_list
  ;

io_implied_do_list
  : implied_do_list
  ;

format_identifier
  : expression
  | '*'
  ;

/*--------------------------------------------------------------------
  return
--------------------------------------------------------------------*/

return_statement
  : RETURN expression
  | RETURN		%prec WAIT
  ;

/*--------------------------------------------------------------------
  stop
--------------------------------------------------------------------*/

stop_statement
  : STOP		%prec WAIT
  | STOP INT_CONST
  | STOP STRING
  ;

/*--------------------------------------------------------------------
  rewind
--------------------------------------------------------------------*/

rewind_statement
  : REWIND expression
  | REWIND '(' io_info_list ')'
  ;

/*--------------------------------------------------------------------
  write
--------------------------------------------------------------------*/

write_statement
  : WRITE '*' ','                   io_list
  | WRITE '(' control_info_list ')'		%prec WAIT
  | WRITE '(' control_info_list ')' io_list
  ;

/*--------------------------------------------------------------------
  format statement -- the most wonderful thing in FORTRAN
--------------------------------------------------------------------*/

format_statement
  : format '(' format_spec ')'
  | format '('             ')'
  ;

format
  : FORMAT
  ;

format_spec
  : INT_CONST fmt_spec1
  |           fmt_spec
  ;

fmt_spec
  : fmt_spec1
  | fmt_spec2
  ;

fmt_spec1
  : fmt_spec1_item
  | fmt_spec1_item ',' format_spec
  ;

fmt_spec2
  : fmt_spec2_item
  | fmt_spec2_item ',' format_spec
  ;

fmt_spec1_item
  : '(' format_spec ')'
  | I INT_CONST
  | I REAL_CONST
  | A
  | A INT_CONST
  | L INT_CONST
  | real_spec_item
  | '/'
  | ':'
  ;

real_spec_item
  : F REAL_CONST
  | E REAL_CONST
  | E REAL_CONST E INT_CONST
  | D REAL_CONST
  | G REAL_CONST
  | G REAL_CONST E INT_CONST
  ;

fmt_spec2_item
  : INT_CONST P
  | INT_CONST P real_spec_item
  | STRING
  | INT_CONST H_STRING
  | T INT_CONST
  | TL INT_CONST
  | TR INT_CONST
  | INT_CONST X
  | S
  | SP
  | SS
  | BN
  | BZ
  ;

/*--------------------------------------------------------------------
  expressions
--------------------------------------------------------------------*/

expression_list
  : expression
  | expression_list ',' expression
  ;

expression
  : primary
  | expression '+' expression
  | expression '-' expression
  | expression '*' expression
  | expression '/' expression
  | expression POW expression
  | expression EQV expression
  | expression NEQV expression
  | expression AND expression
  | expression OR  expression
  | expression EQ  expression
  | expression NE  expression
  | expression GT  expression
  | expression GE  expression
  | expression LT  expression
  | expression LE  expression
  | expression CAT expression		/* string concat */
  ;

primary
  : IDENTIFIER
  | REAL '(' expression ')'		/* pathological case */
  | IDENTIFIER '(' expression_list ')'	/* array reference or function call */
  | IDENTIFIER '('                 ')'
  | constant
  | '+' primary		%prec POS
  | '-' primary		%prec NEG
  | NOT primary
  | '(' expression ')'
  ;

constant
  : arith_const
  | logic_const
  | STRING
  ;

arith_const
  : INT_CONST
  | REAL_CONST
  | complex_const
  ;

complex_const
  : '(' num_const ',' num_const ')'
  ;

num_const
  :     INT_CONST
  |     REAL_CONST
  | '+' INT_CONST
  | '+' REAL_CONST
  | '-' INT_CONST
  | '-' REAL_CONST
  ;

logic_const
  : _TRUE
  | _FALSE
  ;



