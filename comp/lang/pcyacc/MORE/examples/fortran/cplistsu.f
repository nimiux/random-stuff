      SUBROUTINE CPLIST(NUM, PLIST, NP)
      INTEGER  NUM, PLIST, NP
      DIMENSION PLIST(200)
      INTEGER  X, DIV, SQR
      LOGICAL  ISPRIME
      
      NP = 0
      DO 99 X = 2, NUM
        ISPRIME = .TRUE.
        DIV = 2
        SQR = SQRT(REAL(X))
 10     IF (ISPRIME .AND. (DIV .LE. SQR)) THEN
           IF (MOD(X,DIV) .EQ. 0) THEN
              ISPRIME = .FALSE.
           ENDIF
           DIV = DIV + 1
           GOTO 10
        ENDIF
        IF (ISPRIME) THEN
           NP = NP + 1
           PLIST(NP) = X
        ENDIF
 99   CONTINUE
      RETURN
      END
