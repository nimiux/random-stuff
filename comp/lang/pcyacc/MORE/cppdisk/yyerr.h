
/*================== PCYACC ==========================================

            ABRAXAS SOFTWARE (R) PCYACC
      (C)COPYRIGHT PCYACC 1986-88, ABRAXAS SOFTWARE, INC.
               ALL RIGHTS RESERVED

======================================================================*/


extern int errpos;
extern int errflag;
extern int errcount;
extern int errtoken;

extern void yyerror();
extern void errproc();
extern void error();


