
MODULE ABRAXAS LANGUAGE COBOL

DECLARE PCC CURSOR FOR
  SELECT DISTINCT COLOR, CITY
  FROM P
  WHERE P.WEIGHT > 10
  AND   P.CITY  <> 'PARIS'
  ORDER BY P.COLOR DESC

PROCEDURE PCCMOD CITY CHAR(15);
  OPEN PCC
  INSERT INTO PCC ( COLOR, CITY )
    VALUES        ( 'RED', 'ROME')
  CLOSE PCC
;


