
typedef union  {
  int   i;
  float r;
  char *s;
} YYSTYPE;
extern YYSTYPE yylval;
#define _AND 257
#define _ARRAY 258
#define _BEGIN 259
#define _CASE 260
#define _CONST 261
#define _DIV 262
#define _DO 263
#define _DOWNTO 264
#define _ELSE 265
#define _END 266
#define _FILE 267
#define _FOR 268
#define _FORWARD 269
#define _FUNCTION 270
#define _GOTO 271
#define _IF 272
#define _IN 273
#define _LABEL 274
#define _MOD 275
#define _NIL 276
#define _NOT 277
#define _OF 278
#define _OR 279
#define _PACKED 280
#define _PROCEDURE 281
#define _PROGRAM 282
#define _RECORD 283
#define _REPEAT 284
#define _SET 285
#define _THEN 286
#define _TO 287
#define _TYPE 288
#define _UNTIL 289
#define _VAR 290
#define _WHILE 291
#define _WITH 292
#define _IDENT 293
#define _INT 294
#define _REAL 295
#define _STRING 296
#define _ASSIGN 297
#define _NE 298
#define _GE 299
#define _LE 300
#define _DOTDOT 301
#define _UNARY 302
