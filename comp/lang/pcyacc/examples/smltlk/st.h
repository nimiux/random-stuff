
typedef union  {
  float n;
  char  c;
  char *s;
} YYSTYPE;
extern YYSTYPE yylval;
#define CHARACTER 257
#define IDENTIFIER 258
#define LEFTARROW 259
#define NUMBER 260
#define STRING 261
#define UPARROW 262
#define ADD 263
#define SLSH 264
#define BSLSH 265
#define MUL 266
#define TLD 267
#define LT 268
#define GT 269
#define EQ 270
#define AT 271
#define MOD 272
#define OR 273
#define AND 274
#define QMK 275
#define NOT 276
#define KWMSG 277
#define BIMSG 278
#define UNMSG 279
#define ONESP 280
#define TWOSP 281
