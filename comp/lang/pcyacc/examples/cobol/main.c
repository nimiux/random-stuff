/*
**-----------------------------------------------------------------**
**  MAIN.c:  Grammar driver for COBOL 85                           **
**                                                                 **
**  (c) Copyright 1988-1989 Abraxas Software, Inc.                 **
**-----------------------------------------------------------------**
*/


#include <stdio.h>

extern FILE *yyin;

main(argc, argv)
int argc;
char **argv;
{

  if (argc < 2) {
    printf("Usage: %s <filename>\n", argv[0]);
    exit(1);
  }
  yyin = fopen(argv[1], "r");
  if (yyin == NULL) {
    perror("Unable to open file");
    exit(1);
  }
  if (!yyparse()) printf("syntax ok\n");
  else printf("incorrect syntax\n");
}

yyerror(s)
char *s;
{
  fprintf(stderr, "%s\n", s);
}


