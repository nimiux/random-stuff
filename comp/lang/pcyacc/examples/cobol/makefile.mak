
MMODEL  =-AL
CFLAGS  =-c
LFLAGS  =-Fecobol
OBJECTS = main.obj cobol.obj cobol_l.obj

cobol.exe:	$(OBJECTS)
		cl $(LFLAGS) $(MMODEL) $(OBJECTS)
main.obj:	main.c
		cl $(CFLAGS) $(MMODEL) main.c
cobol_l.c:	cobol_l.l
		pclex  cobol_l.l
cobol_l.obj:	cobol_l.c cobol.h
		cl $(CFLAGS) $(MMODEL) cobol_l.c
cobol.c:	cobol.grm
		pcyacc -D cobol.grm
cobol.obj:	cobol.c
		cl $(CFLAGS) $(MMODEL) cobol.c
clean:
		erase *.obj 
                erase cobol.c 
                erase cobol_l.c 
                erase cobol.h
