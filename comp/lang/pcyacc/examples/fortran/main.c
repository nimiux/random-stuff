
/*
=====================================================================
=====================================================================
*/

#include <stdio.h>
#include "global.h"
#include "f77.h"

static char progid[] = "F2C: Fortran ===> C Source to Source Translator, Version 1.0";
static char copyid[] = "Copyright (c) 19XX, YYYYY Inc.";

extern int scount;
extern int informat;
extern char tv[];
FILE  *fopen(), *fin, *fout;

main(argc, argv)
int   argc;
char *argv[];
{

  printf("%s\n", progid);
  printf("%s\n\n", copyid);

  if (argc < 2) {
    printf ("Usage: f2c <fortran_program>\n");
    exit(1);
  }

  fin = fopen(argv[1], "r");
  if (fin == NULL) {
    printf("Can't open file: \"%s\"\n", argv[1]);
    exit(1);
  }

  printf("Processing Source File: \"%s\"\n", argv[1]);

  yylval.sval = tv;
  scount = 0;
  informat = FALSE;
  if (yyparse()) {
    printf("Error(s) in source FORTRAN program\n");
  } else {
    printf("Parsing successful\n");
  }

  fclose(fin);
}

yyerror(s)
char *s;
{
  printf("%s\n", s);
}



