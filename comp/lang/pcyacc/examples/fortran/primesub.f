      SUBROUTINE BSEARCH(KEY,PLIST,NP,INDEX)
      INTEGER KEY,PLIST,NP,INDEX
      DIMENSION PLIST(NP)
      INTEGER HALF,UNDERLIMIT,UPLIMIT
      LOGICAL FOUND
      
      UPLIMIT=NP
      UNDERLIMIT=1
      FOUND=.FALSE.
 10   IF ((.NOT. FOUND) .AND. (UNDERLIMIT.LE.UPLIMIT)) THEN
         HALF=(UNDERLIMIT+UPLIMIT)/2
         IF (KEY .LT. PLIST(HALF)) THEN
            UPLIMIT=HALF-1
         ELSE
            IF (KEY .GT. PLIST(HALF)) THEN
               UNDERLIMIT=HALF+1
            ELSE 
               FOUND=.TRUE.
            ENDIF
         ENDIF
         GOTO 10 
      ENDIF
      IF (.NOT. FOUND) THEN
         INDEX = 0
      ELSE 
         INDEX = HALF
      ENDIF
      RETURN 
      END
