%define  NULL            0x00
%define CR              0x0d, 0x0a

%define SECTORSIZE      0x200 
%define SECTORSPERBLOCK 0x02
%define BLOCKLENGTH     (SECTORSIZE * SECTORSPERBLOCK)

%define EMBRYOORG       0x8000 
%define EMBRYOSEGMENT   0x0000 
