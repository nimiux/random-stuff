
; This program is free software; you can redistribute it and/or
; modify it under the terms of the GNU General Public License
; as published by the Free Software Foundation; either version 2
; of the License, or (at your option) any later version.
;

;%include "defs.inc"

; Uninitialized variables
section .bss

section .text
        org 0x0
        jmp short _embryo

%include "print.S"
%include "utils.S"
_embryo:
        mov si, KeyMsg
        call print
        jmp $
        call getkey

getkey:
        mov si, KeyMsg
        call printstring
        mov ah, 0x0
        int 0x16
        ret

otherprintbytehex:
        mov ah, 0x0e     ; Teletype output
        xor bx, bx       ; Page 0, color 0 (graphic only)
        mov cl, al
        mov dl, 1
        shr al,4
.digit  cmp al, 0x09
        jbe .next
        add al, '7'
.next   add al, '0'
        int 0x10
        mov al, cl
        and al, 0x0f
        dec dl
        jz .digit
        ret

printbyte:
        mov ah, 0x0e             ; Teletype output
        xor bx, bx              ; Page 0, color 0 (graphic only)
        int 0x10
        ret

printstring:
        mov ah, 0x0e            ; Teletype output
        xor bx, bx              ; Page 0, color 0 (graphic only)
.char   lodsb
        or al, al
        jnz .print
        ret
.print  int 0x10
        jmp short .char

;  section .data
; Initialized variables

; Attributed Strings
EmbryoMsg       db 6
        db 'E', 00000111b, 'm', 00000001b, 'b', 00000010b
        db 'r', 00000011b, 'y', 00000100b, 'o', 00000101b

; Colured Strings
LoadMsg         db 00000111b, 14, 'Embryo Running'

; Strings
KeyMsg          db 12, '32 bit mode.'

; Pad the sector to 1024 bytes
times 0x400 - ($ - $$) db 0xa5
