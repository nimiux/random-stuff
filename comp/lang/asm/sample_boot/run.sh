#¡/bin/bash

TMPDIR=/tmp
nasm -fbin bootloader.asm -o ${TMPDIR}/bootloader.bin
nasm -fbin kernel.asm -o ${TMPDIR}/kernel.bin
cat ${TMPDIR}/bootloader.bin ${TMPDIR}/kernel.bin > ${TMPDIR}/result.bin
qemu-system-i386 ${TMPDIR}/result.bin
