.data

msg:
    .ascii      "Hello world!\n"
len = . - msg

.text
.global _start

_start:
    /* syscalls are when the program hands things to the kernel */
    mov %r0, $1     /* our status is stdout */
    ldr %r1, =msg   /* message location in memory */
    ldr %r2, =len   /* message length in bytes */
    mov %r7, $4     /* write is syscall #4 */
    swi $0          /* invoke syscall */

    /* syscall exit (int status) */
    mov %r0, $0     /* our status is now exit */
    mov %r7, $1     /* exit is syscall #1 */
    swi $0          /* invoke syscall */
     

