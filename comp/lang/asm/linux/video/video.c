#include <stdio.h>

void printch(char display_ch) {
    static unsigned char display_page, display_attr, dysplya_ch;

    /* get current display page number */
    asm ("\tmov $0x0f, %ah\n\t"); // function number
    asm ("int $0x10\n\t"); // BIOS video interrupt
    //asm ("mov %bh, $display_page\n\t"); // get display page
    
    /* get current display attribute */
    asm ("mov $0x08, %ah\n\t"); // function number
    asm ("mov $display_page, %bh\n\t"); // set display page
    asm ("int $0x10\n\t"); // BIOS video interrupt
    //asm ("mov %ah, $display_attr\n\t"); // get display attribute
    
    /* print character with current settings */
    asm ("mov $0x09, %ah\n\t"); // function number
    asm ("mov $display_page, %bh\n\t"); // set display page
    asm ("mov $1, %bh\n\t"); // set number of characters 
    asm ("mov $display_ch, %al \n\t");// set character for display
    asm ("mov $display_attr, %bl\n\t"); // set attribute
    asm ("int $0x10\n"); // BIOS video interrupt
}

int main(void) {
    //clrscr();
    //textcolor(0);
    //textbackground(1);
    
    putchar('*');
    printch('*');
    
    getchar();
    
    //textcolor(1);
    //textbackground(2);
    //clrscr();
    return 0;
}
