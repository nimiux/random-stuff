all : fat12.bin test.exe

install : fat12.bin /mnt/load.bin
	-umount /mnt
	-umount /dev/fd0
	dd bs=1 if=fat12.bin skip=0  count=3   of=/dev/fd0
	dd bs=1 if=fat12.bin skip=36 count=476 of=/dev/fd0 seek=36

# this target for testing purposes only
test : fat12.asm
	nasm -f bin -dDOS=1 -o fat12.com fat12.asm
# the leading hyphen should supress the error if NDISASM is not installed
	-ndisasm -o 0x100 fat12.com >fat12.lst

clean :
	rm -f *.bin *.exe *.com *.obj *.lst

fat12.bin : fat12.asm
	nasm -f bin -o fat12.bin fat12.asm
# the leading hyphen should supress the error if NDISASM is not installed
	-ndisasm -o 0x100 fat12.bin >fat12.lst

/mnt/load.bin : test.exe
	-mount /dev/fd0 /mnt
	cp -f test.exe /mnt/load.bin

test.exe : show-bmp.asm cow8.bmp
	nasm -f bin -o show-bmp.bin show-bmp.asm
# define DOS to disable run-time check for DOS (SHOW-BMP will assume
# DOS is present, and use INT 21h AH=4Ch to exit). This lets you
# put TEST.EXE on a floppy, and load it from DOS using FAT12.COM
# This is for testing purposes only!
#	nasm -f bin -dDOS=1 -o show-bmp.bin show-bmp.asm
	cat show-bmp.bin cow8.bmp >test.exe
