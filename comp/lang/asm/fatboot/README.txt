First-stage bootloader for FAT12 (DOS/Windows) floppies
Chris Giese <geezer@execpc.com>, http://www.execpc.com/~geezer

This code is public domain (no copyright).
You can do whatever you want with it.

This code updated, fixed, and released on Nov 26, 2002

================================================================
DESCRIPTION
================================================================
The first-stage loader will find a file (the "second stage")
in the root directory of a FAT12 disk. It will then load and
execute this second stage file.

The second stage must be in binary format (entry point = start
of file; with no headers or relocations), and it must run in
or start running in 16-bit real mode.

The name of the second stage, its load address, and its ORG
value are all defined by macros near the top of the first-
stage source code file:

Macro	Default value	Explanation
-----	-------------	-----------
SS_NAME LOAD.BIN        Name of second stage file.

SS_ADR	10000h		Absolute addresss where second stage
			will be loaded.

SS_ORG	100h		ORG value of second stage.

On entry to the second-stage code:
        CS = (SS_ADR - SS_ORG) / 16
        IP = SS_ORG
        DS = CS     (TINY memory model)
        ES = CS
SS and SP are not changed from the values used by the first-stage
code. They point to a small but usable stack below 10000h.

================================================================
BUILD
================================================================
DOS/Windows users will need John Fine's Partial Copy (PARTCOPY,
PCOPY):
	http://my.execpc.com/~geezer/johnfine/#zero

To build this code, you must have NASM:
	http://nasm.sourceforge.net

To get a listing of the boot code, you also need NDISASM,
which is the disassembler companion to NASM. The listing
file is not essential for the operation of the loader.

Linux users need Make. Make is also recommended for users of
DOS or Windows, though a batch file is also provided.

To build and install this code, put a FAT12 floppy in the first
floppy drive (A: under DOS/Windows, /dev/fd0 under Linux).
If you have DOS or Windows, and Make, type:
	make install

If you have DOS or Windows, but no Make (or if you don't want to
use Make), type:
	install.bat

Under Linux, type:
        make -f linux.mak install

No linker is required. The TEST.EXE file is created by magic :)

Do not use RAWRITE to install the bootsector. RAWRITE will
overwrite the first FAT, and the boot code will not work.

================================================================
TESTING
================================================================
The test program SHOW-BMP.ASM switches the screen to 640x480x256
graphics mode and displays an attached .BMP file.

The video board in your PC:
- Must have VBE (VESA BIOS extensions)
- Must support VESA video mode 101h (640x480x256)
- Must support VBE 1.x banked framebuffer for this mode

If you have DOS or Windows, run the TEST.EXE file created by the
build process above. It should display the test image, then
return to DOS when you press a key. If you see the message:

	Error in EXE file
or
	Error in .BMP file attached to this bootsector.
	It must be a 256-color uncompressed 640x480 .BMP file

make sure the .BMP file is correct. It should be 308278 bytes
in size.

Boot the PC from the floppy disk prepared above. It should
display a message prompting you to eject the floppy disk and
press a key. After you press a key, the test image will be
displayed on the screen. Examine it carefully to see if there
are any breaks or irregularities. These may indicate that the
first-stage loader does not function correctly.

The first-stage loader is too small (512 bytes) to contain
meaningful error message strings. If an error occurs:
- The PC speaker will beep
- The loader will wait for you to press a key, then attempt
  to boot again. You may eject the floppy disk before pressing
  a key to boot from the hard drive instead.
- A blinking blue-on-white letter will be displayed in the
  upper left corner of the screen:
   Letter   Error
   ------   -----
   F	    Second stage file not 'F'ound
   R	    Error while 'R'eading disk
   M	    Second stage will not fit into conventional 'M'emory

================================================================
NOTES
================================================================
The code in FAT12.ASM and SHOW-BMP.ASM should work even on 8088
CPUs (IBM PC-XT). The code can be made smaller by rewriting it
with the assumption that it will run on an 80188/80186 CPU or
better. Even smaller code can be had by assuming a 32-bit CPU
(386SX or better).

Relatively few values in the code are "hard-wired". Some numbers
are calculated at run-time, from values in the BIOS parameter
block (BPB). Other values are assemble-time constants, but are
easily changed.

If you really need an extra 32K of memory for your second stage:
- change FS_ORG to 7C00h
- change ADR_STACK to 0A00h. This moves the stack (ADR_STACK),
  directory buffer (ADR_DIRBUF), and FAT buffers (ADR_FATBUF)
  below the code in memory
- change SS_ADR to 8000h
You will not be able to build and run the code as a DOS .COM
file if you do this.

The code that actually loads the second stage will load in units
of clusters. This is not a concern for the FAT12 filesystem
because FAT12 usually has 1 cluster = 1 sector. For FAT16,
however, you may have up to 128 sectors per cluster. Loading a
large file with such a large cluster size may cause high
conventional memory (the EBDA, or video memory) to be
overwritten.

DOS or Windows may become so confused by a bad FAT bootsector
that they won't even format the disk. Fix this problem by
writing all 512 bytes of this bootsector to the floppy:

DOS/Windows:
	partcopy fat12.bin 0 200 -f0
Linux:
	dd bs=1 if=fat12.bin skip=0  count=512 of=/dev/fd0

This overwrites the ENTIRE bootsector, including the BIOS
parameter block (BPB). The default BPB in the code assumes
a 1.44 meg floppy.
