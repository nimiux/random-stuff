@rem Build and install first-stage loader
@rem
nasm -f bin -o fat12.bin fat12.asm
ndisasm -o 0x100 fat12.bin >fat12.lst
partcopy fat12.bin  0   3 -f0
partcopy fat12.bin 24 1DC -f0 24
@rem
@rem Build and install second-stage test program
@rem
nasm -f bin -o show-bmp.bin show-bmp.asm
copy /b /y show-bmp.bin + cow8.bmp test.exe
copy /y test.exe a:\load.bin
