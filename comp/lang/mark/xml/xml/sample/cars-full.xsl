<?xml version="1.0" encoding="UTF-8"?>
<!-- <!DOCTYPE cars SYSTEM "cars.dtd"> -->
<!DOCTYPE cars [
<!ELEMENT cars (car)*>
<![CDATA [ Cars ]]>
<!ELEMENT car (manufacturer,model,engine)>
<!ELEMENT manufacturer (#PCDATA)>
<!ELEMENT model (#PCDATA)>
<!ELEMENT engine (name,cc)>
<!ATTLIST engine type CDATA #REQUIRED>
<!ELEMENT name (#PCDATA)>
<!ELEMENT cc (#PCDATA)>
]>
<cars>
   <car>
      <manufacturer>Seat</manufacturer>
      <model>Ibiza</model>
      <engine type="diesel" cant="2">
	 <name>TDI</name>
	 <cc>1900</cc>
      </engine>
   </car>

   <car>
      <manufacturer>Citroen</manufacturer>
      <model>C5</model>
      <engine type="gas">
	 <name>QQI</name>
	 <cc>2100</cc>
      </engine>
   </car>
</cars>

<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http:/www.w3-org/1999/XSL/Transform">
<xsl:output method="xml"
doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"/>
<xml:template match="/">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>Coches</title></head>
<body>
<xsl:apply-templates select="car"/>
</body>
</html>
</xml:template>
<xml:template match="car">
<h2><xsl:value-of select="car/manufacturer"/><h2>
</xml:template>
</xsl:stylesheet>

