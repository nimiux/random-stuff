<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet  version="1.0" xmlns:xsl="http:/www.w3.org/1999/XSL/Transform">
<xsl:output method="html"/>
<!-- <xsl:output method="xml"
doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"/> -->
<xsl:template match="/">
<html>
<!--xmlns="http://www.w3.org/1999/xhtml">-->
<head><title>Coches</title></head>
<body>
<xsl:apply-templates select="car"/>
</body>
</html>
</xsl:template>

<xsl:template match="car">
<h2><xsl:value-of select="car/manufacturer"/></h2>
</xsl:template>

</xsl:stylesheet>



