<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html"/>
<xsl:template match="/">
<html>
<xsl:apply-templates/>
</html>
</xsl:template>
<xsl:template match="dis">
<h1>Identificador:</h1><xsl:value-of select="@cache"/>
</xsl:template>

<xsl:template match="mess">
<h2>Identificador:</h2><xsl:value-of select="@id"/>
<h2>Texto:</h2><xsl:value-of select="mess"/>
</xsl:template>

</xsl:stylesheet>
