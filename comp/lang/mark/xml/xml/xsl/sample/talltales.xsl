<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns="http://www.w3.org/1999/xhtml">

   <xsl:template match="/">
      <html xmlns="http://www.w3.org/1999/xhtml">
	 <head>
	 <title>An XSLT CSS sample</title>
	 <link rel="stylesheet" type="text/css" href="talltales.css"/>
	 </head>
	 <body> 
	    <xsl:for-each select="talltales/tt">
	       <div>
		  <xsl:apply-templates select="question"/>
		  <xsl:apply-templates select="a"/>
		  <xsl:apply-templates select="b"/>
		  <xsl:apply-templates select="c"/>
	       </div>
	    </xsl:for-each>
	 </body>
      </html>
   </xsl:template>

   <xsl:template match="question">
      <div class="shine">
	 <xsl:value-of select="."/>
      </div>
   </xsl:template>
      
   <xsl:template match="a|b|c">
      <div class="a">
	 <xsl:value-of select="."/>
      </div>
   </xsl:template>
</xsl:stylesheet>
