<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns="http://www.w3.org/1999/xhtml">

   <xsl:template match="/">
      <html xmlns="http://www.w3.org/1999/xhtml">
	 <head>
	 </head>
	 <body style="background-image: url(background.gif); background-repeat:
			repeat">
	    <xsl:for-each select="talltales/tt">
	       <div style="width:700px; padding:10px; margin: 10px;
		     border:5px groove #353A54; background-color:#353A54">
		  <xsl:apply-templates select="question"/>
		  <xsl:apply-templates select="a"/>
		  <xsl:apply-templates select="b"/>
		  <xsl:apply-templates select="c"/>
	       </div>
	    </xsl:for-each>
	 </body>
      </html>
   </xsl:template>

   <xsl:template match="question">
      <div style="color:#F4EECA; font-family:Verdana,Arial; font-size:13pt">
	 <xsl:value-of select="."/>
      </div>
   </xsl:template>
      
   <xsl:template match="a|b|c">
      <div style="color:#6388A0; font-family:Verdana,Arial; font-size:12pt;
		          text-indent:14px">
	 <xsl:value-of select="."/>
      </div>
   </xsl:template>
</xsl:stylesheet>
