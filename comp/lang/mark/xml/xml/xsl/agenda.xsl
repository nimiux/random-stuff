<?xml version="1.0" encoding="ISO-8859-15"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
  <html>
    <body>
      <xsl:comment>######################</xsl:comment>
      Lenguaje:<xsl:value-of select="agenda/@lang"/>
      <xsl:comment>###########My list###########</xsl:comment>
      <h2>This is my agenda</h2>
    <table border="1">
        <tr bgcolor="#9acd32">
	    <th align="left">Nombre</th>
            <th align="left">Apellido</th>
            <th align="left">e-mail</th>
        </tr>
    <xsl:for-each select="agenda/entrada">
        <tr>
	    <td><xsl:value-of select="nombre"/></td>
            <td><xsl:value-of select="apellido"/></td>
            <td><xsl:value-of select="email"/></td>
        </tr>
    </xsl:for-each>
        </table>
  </body>
    </html>
    </xsl:template>
</xsl:stylesheet>
