<?xml version="1.0"?>

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns="http://www.w3.org/1999/xhtml">

   <xsl:template match="/">
      <html xmlns="http://www.w3.org/1999/xhtml">
	 <head>
	 <title><xsl:value-of select="title"/></title>
	 <link rel="stylesheet" type="text/css" href="/css/styles.css"/>
	 </head>
	 <body> 
	    <xsl:apply-templates select="./header"/>
	    
	    <!--<xsl:for-each select="talltales/tt">
	       <div>
		  <xsl:apply-templates select="question"/>
		  <xsl:apply-templates select="a"/>
		  <xsl:apply-templates select="b"/>
		  <xsl:apply-templates select="c"/>
	       </div>
	    </xsl:for-each> -->
	 </body>
      </html>
   </xsl:template>
   
   <xsl:template match="header">
      <div class="header">
	 <xsl:value-of select="."/>
      </div>
   </xsl:template>

   <xsl:template match="link">
      <a>
	 <xsl:attribute name="href">
	    <xsl:value-of select="@url"/>
	 </xsl:attribute>
	 <xsl:value-of select="."/>
      </a>
   </xsl:template>
</xsl:stylesheet>
