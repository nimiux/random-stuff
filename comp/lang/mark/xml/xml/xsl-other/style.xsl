<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:fn="http://www.w3.org/2005/xpath-functions"
		extension-element-prefixes="fn">
	<xsl:output method="html" indent="yes"/>

	<xsl:template match="/">
	<html>
		<head>
			<xsl:apply-templates/>
		</head>
		<body>
		<xsl:text>
			Add DOCTYPE html.....
		</xsl:text>
				<!-- <xsl:for-each select="agenda/entrada">
				<hr/>
				<h1 id="header"><xsl:value-of select="nombre"/> </h1>
				<p id="normal"><xsl:value-of select="apellido"/> </p>
				<xsl:apply-templates select="email"/>
					<hr/>
			</xsl:for-each> -->
		</body>
	</html>
	</xsl:template>

	<xsl:template match="metadata">
		<xsl:for-each select="*">
				<xsl:value-of select="string-join(.)"/>
				<meta name="{translate('a','a','A')}">
			</meta>
		</xsl:for-each>
			<!-- <meta name="Keywords"
					content="<xsl:value-of select="keywords"/>" /> -->
	</xsl:template>

	<!--<xsl:template match="links">
		<xsl:for-each select="link">
			<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
		</xsl:for-each>

		<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
			<link rel="stylesheet" type="text/css"
				href="css/site.css"/>

	<xsl:template match="header">
		<xsl:choose>
			<xsl:when test="email/@link = yes">
				<a>
					<xsl:attribute name="href">
						<xsl:value-of select="email"/>
					</xsl:attribute>
					<xsl:value-of select="email"/>
				</a>
			</xsl:when>
			<xsl:otherwise>
				<p><xsl:value-of select="email"/></p>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
  
	<xsl:template match="email">
		<xsl:choose>
			<xsl:when test="email/@link = yes">
				<a>
					<xsl:attribute name="href">
						<xsl:value-of select="email"/>
					</xsl:attribute>
					<xsl:value-of select="email"/>
				</a>
			</xsl:when>
			<xsl:otherwise>
				<p><xsl:value-of select="email"/></p>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template> -->
</xsl:stylesheet>
<!-- vim: set ts=4 : --> 
