<?xml version="1.0"?>
<!--  -->

<!-- Use this stylesheet  -->

<xsl:stylesheet version="1.0"
   xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" indent="yes"/>

  <xsl:template match="/">
    <html>
    <head>
   <link rel="stylesheet" type="text/css" href="/home/chema/dev/xml/xmlsite/css/site.css"/>
    </head>
    <body>
   <xsl:for-each select="agenda/entrada">
      <hr/>
      <h1 id="header"><xsl:value-of select="nombre"/> </h1>
      <p id="normal"><xsl:value-of select="apellido"/> </p>
      <xsl:apply-templates select="email"/>
      <hr/>
   </xsl:for-each>
    </body>
    </html>
  </xsl:template>
  
  <xsl:template match="email">
    <xsl:choose>
   <xsl:when test="email/@link = yes">
      <a>
      <xsl:attribute name="href">
         <xsl:value-of select="email"/>
      </xsl:attribute>
      <xsl:value-of select="email"/>
      </a>
   </xsl:when>
   <xsl:otherwise>
   <p><xsl:value-of select="email"/></p>
   </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
