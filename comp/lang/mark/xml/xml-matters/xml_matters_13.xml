<?xml version="1.0" encoding="UTF-8"?>
<article ratings="auto" toc="auto">
  <seriestitle>XML MATTERS #13</seriestitle>
  <papertitle> XML and Compression</papertitle>
  <subtitle>Exploring the entropy of documents</subtitle>
  <author company=" Gnosis Software, Inc."
          jobtitle="Interior Designer"
          name="David Mertz, Ph.D.">
    <img src="http://gnosis.cx/cgi-bin/img_dqm.cgi">
    David Mertz....  David may be reached at mertz@gnosis.cx; his
    life pored over at <a href="http://gnosis.cx/publish/.">http://gnosis.cx/publish/.</a>  Suggestions and
    recommendations on this, past, or future, columns are welcomed.
  </author>
  <date month="August" year="2001" />
  <zone name="xml" />
  <meta name="KEYWORDS" content="Mertz" />
  <abstract>
    This article looks at a number of approaches to wrapping
    compression around XML documents.  The special structures in
    XML tend to allow certain improvements over the most naive
    compression techniques.  Code exploring several techniques is
    provided in the article.
  </abstract>
<p>
<heading refname="h1" type="major" toc="yes">Introduction</heading>
 </p>
<p>
  XML as a format has a lot of nice properties--it is a perfectly
  general way of representing arbitrary data structures, and it
  is human readable (more of less).  But XML has one very notable
  unpleasant property.  XML documents are VERBOSE; not just a
  <i>little</i> on the wordy side, but grotesquely, morbidly obese,
  almost unbelievably huge.  Much of the time, this drawback of
  XML really makes no difference--DASD is cheap, and the time on
  the wire might be only a small part of the total time in the
  process.  But at other times, bandwidth and storage space can
  be important.
</p>
<p>
  To quantify things, it is not at all unusual for XML documents
  that represent table-oriented data to be three times as large
  as a CSV or database representation, or even than a flat file.
  A similar increase is typical in representing EDI data (such as
  for ebXML projects).  In many of these context, one starts out
  with multi-megabyte data sources, and making them multiples
  larger can be inconvenient, especially for transmission
  purposes.  For example, for purposes of this article, I created
  an XML representation of an approximately 1 megabyte Apache
  access logfile.  This created an XML document 3.18 times the
  size of the underlying line-oriented log.  The only
  <i>information</i> added in the process was some descriptive names
  for fields, but that could have also been specified in a single
  header line of less than a hundred bytes.  Moreover, my
  specific XML representation did not include any namespaces in
  the tags, which would have increased the size further.
</p>
<p>
  When one thinks about compressing documents, one normally
  thinks first of general compression algorithms like Lempel-Ziv
  and Huffman, and of the common utilities that implement
  variations on them.  Specifically, on Unix-like platforms, what
  comes first to mind is usually the utility <code type="inline">gzip</code>; on other
  platforms, <code type="inline">zip</code> is more common (using utilities such as
  <code type="inline">pkzip</code>, <code type="inline">info-zip</code> and <code type="inline">WinZip</code>).  <code type="inline">gzip</code> turns out to be
  quite consistently better than <code type="inline">zip</code>, but only by small
  margins.  These utilities indeed tend substantially to reduce
  the size of XML files.  But it also turns out that one can
  obtain considerably better compression rates by two means,
  either individually or in combination.
</p>
<p>
  The first technique is to use the Burrows-Wheeler compression
  algorithm rather than Lempel-Ziv sequential algorithms.  In
  particular, the somewhat less common utility <code type="inline">bzip2</code> (or its
  associated libraries and APIs) is an implementation of
  Burrows-Wheeler for many system platforms (and is accompanied
  by full source and a BSD-style license).  Burrows-Wheeler
  operates by grouping related strings in a uncompressed source,
  rather than in the Lempel-Ziv style of building up a dictionary
  of string occurences.  <code type="inline">bzip2</code> consistently obtains better
  compression than <code type="inline">gzip</code> across many file types, but the effect
  is especially dramatic for XML documents.  On the down side,
  <code type="inline">bzip2</code> is generally slower than <code type="inline">gzip</code>.  Then again the
  slowness of bandwidth will very often swamp speed differences
  in CPU or memory-bound algorithms.
</p>
<p>
  The second technique is to take advantage of the very specific
  structure of XML documents to produce more <i>compressible</i>
  representations.  The XMill utility is one implementation of
  this technique, and it is available (with C++ source) under a
  liberal license from AT&amp;T. XMill, however, seems to require
  certain click-through style limitations on its licensing, and
  cannot be distributed by other parties directly (at least as I
  understand it).  I have created my own implementation of the
  same general technique, and the implementation is presented in
  this article.  The code herein is released to the public
  domain, and the technique as implemented was developed
  independently and has only a "bird-eye view" similarity to
  XMill--sometimes XMill does better, and sometimes I do (but
  XMill is probably always faster than my initial implementation,
  which only pays attention to compression results).
</p>
<p>
<heading refname="h1" type="major" toc="yes">Comparing Basic Algorithms</heading>
 </p>
<p>
  For purposes of comparison in this article I obtained or
  created four base documents.  The first was Shakespeare's play
  <attribution>Hamlet</attribution> as an XML document (see resources).  The markup
  includes tags such as <code type="inline">&lt;PERSONA&gt;</code>, <code type="inline">&lt;SPEAKER&gt;</code> and <code type="inline">&lt;LINE&gt;</code>
  which map fairly naturally to the typographic forms one might
  encounter in a printed copy.  In order to make comparisons of
  just how the XML markup contributes to document size and
  compressibility, I derived from <code type="inline">hamlet.xml</code> a document
  <code type="inline">hamlet.txt</code> that simply removed all the XML tags, but left the
  content.  This derivation is <b>not</b> reversible, and is a strict
  loss of information.  A person reading <code type="inline">hamlet.txt</code> would not
  have a great deal of difficulty determining semantically which
  content is a "speaker" name and which a "line", for example,
  but there is no easy way a computer could regenerate the source
  XML document.
</p>
<p>
  The next two documents are an Apache Weblog file (a compact set
  of line-oriented records) and an XML document created from
  this.  Since the source document is the log file, no
  information is lost in the transformation, and it is trivial to
  recreate exactly the original format from the XML.  Of course,
  it is not possible to use an XML parser, or DOM, or a
  validator, or a DTD, with the logfile format.  Let's take a
  look at the sizes of the base documents:
</p>
<code type="section">
<heading refname="code1" type="code" toc="yes">Directory listing of base documents</heading>
 288754  hamlet.xml
 188830  hamlet.txt
 949474  weblog.txt
3021921  weblog.xml
</code>
<p>
  In both cases, the XML is much larger.  In the <attribution>Hamlet</attribution>
  example, the comparison is not entirely fair since the actual
  information content of the text version is also less.  But for
  the Weblog file, the XML starts to look fairly bad.  However,
  not everything is quite as it appears.  Compression programs do
  a fairly good job of boiling documents down to their actual
  information content, and meaningless padding tends towards zero
  size in the compressed version (asymptotically, and if all is
  happy).  Let's try <code type="inline">gzip</code>, <code type="inline">zip</code> and <code type="inline">bzip2</code>:
</p>
<code type="section">
<heading refname="code1" type="code" toc="yes">Directory listing of compressed Shakespeare</heading>
  78581  hamlet.xml.gz
  72505  hamlet.txt.gz
  78696  hamlet.xml.zip
  72620  hamlet.txt.zip
  57522  hamlet.xml.bz2
  56743  hamlet.txt.bz2
</code>
<p>
  -
</p>
<code type="section">
<heading refname="code1" type="code" toc="yes">Directory listing of compressed Weblog</heading>
  91029  weblog.txt.gz
 115524  weblog.xml.gz
  91144  weblog.txt.zip
 115639  weblog.xml.zip
  56156  weblog.txt.bz2
  66994  weblog.xml.bz2
</code>
<p>
  There are a number of interesting things in the above sizes.
  For both styles of documents--for every compression
  technique--the size differences in compressed files is much
  smaller than between the XML versus text originals.  It is also
  noteworthy that <code type="inline">gzip</code> and <code type="inline">zip</code> cluster very closely together
  for corresponding cases, while <code type="inline">bzip2</code> does much better all the
  time.  Moreover, when using <code type="inline">bzip2</code> on the Shakespeare
  document, the compressed size difference between the text and
  XML formats is nearly negligible, despite the 53% larger size
  of the XML base document.
</p>
<p>
  However, the Weblog stands out as as a problem case.  While
  compression narrows the bloat of the XML conversion quite a
  bit, even the <code type="inline">bzip2</code> version still lets the XML markup
  increase the size by about 20%.  Not necessarily a disaster,
  but it feels like we should ideally be able to compress down to
  the true information content
</p>
<p>
<heading refname="h1" type="major" toc="yes">Reversible Transformations</heading>
 </p>
<p>
  An XML document has a rather inefficient form when it comes to
  compression, actually.  As we will see <code type="inline">bzip2</code> somewhat
  alleviates this inefficency by regrouping strings.  But at
  heart, XML documents are a jumble of fairly dissimilar
  parts--tags, attributes, elements bodies of different types.
  If we could take each relatively homogeneous set of things, and
  group them close to each other in a transformed file, standard
  compressors would have more to work with.  For example, if
  every <code type="inline">&lt;host&gt;</code> tag-body in our Weblog occurs near the others,
  the block of stuff which contains the IP address of hosts will
  be easy to compress.  The trick here is to come up with a way
  of transforming an XML document into something that contains
  <b>all</b> the same information, but structures the layout in such a
  compressor-friendly style.
</p>
<p>
  The utilities <code type="inline">xml2struct.py</code> and <code type="inline">struct2xml.py</code> do exactly
  what is desired.  The versions below have a few limitations,
  but demonstrate the principles involved.  Some limitations are
  that each document is limited to 253 distinct tags, and that
  attributes and processing instructions are not handled.  Fixing
  those limits should not change the jist of the results,
  however.  XMill performs a similar transformation, but with
  some extra options and with fewer limitations.
</p>
<p>
  The general format of a "struct" document is as follows:
</p>
<code type="section">
<heading refname="code1" type="code" toc="yes">Struct document format</heading>
1.  A list of tags that occur in the original XML document,
    separated by newline characters.
2.  A section delimeter: 0x00 (the null byte)
3.  A compact representation of the overall document
    structure, where each start tag is represented by a
    single byte, and the occurence of content is marked by
    a 0x02 byte.
4.  Another section delimeter: 0x00 (the null byte)
5.  The contents of all the elements indicated in the
    document structure schematic, grouped by element type.
    Each individual content item is delimited by a 0x02
    byte, while the start of a elements of a new type is
    delimited by a 0x01 byte (the last not strictly needed,
    but it makes reversing the transformation easier).
</code>
<p>
  Below is complete Python code to perform and reverse the
  described transformation.  It would be simple to implement this
  transformation in another programming language also:
</p>
<code type="section">
<heading refname="code1" type="code" toc="yes">xml2struct.py</heading>
<b class="blue">import</b> sys
<b class="blue">import</b> xml.sax
<b class="blue">from</b> xml.sax.handler <b class="blue">import</b> *

<b class="blue">class</b> StructExtractor(ContentHandler):
    <b class="green">"""Create a special structure/content form of an XML document"""</b>
    <b class="blue">def</b> startDocument(self):
        self.taglist = []
        self.contentdct = {}
        self.state = []             <b class="red"># stack for tag state</b>
        self.newstate = <b>0</b>           <b class="red"># flag for continuing chars in same elem</b>
        self.struct = []            <b class="red"># compact document structure</b>

    <b class="blue">def</b> endDocument(self):
        sys.stdout.write(<b class="green">'\n'</b>.join(self.taglist))
                                    <b class="red"># Write out the taglist first
</b>        sys.stdout.write(chr(<b>0</b>))    <b class="red"># section delimiter \0x00</b>
        sys.stdout.write(<b class="green">''</b>.join(self.struct))
                                    <b class="red"># Write out the structure list
</b>        sys.stdout.write(chr(<b>0</b>))    <b class="red"># section delimiter \0x00</b>
        <b class="blue">for</b> tag <b class="blue">in</b> self.taglist:    <b class="red"># Write all content lists</b>
            sys.stdout.write(chr(<b>2</b>).join(self.contentdct[tag]))
            sys.stdout.write(chr(<b>1</b>)) <b class="red"># delimiter between content types</b>

    <b class="blue">def</b> startElement(self, name, attrs):
        <b class="blue">if</b> <b class="blue">not</b> name <b class="blue">in</b> self.taglist:
            self.taglist.append(name)
            self.contentdct[name] = []
            <b class="blue">if</b> len(self.taglist) &amp;gt; <b>253</b>:
                <b class="blue">raise</b> ValueError, <b class="green">"More than 253 tags encountered"</b>
        self.state.append(name)     <b class="red"># push current tag</b>
        self.newstate = <b>1</b>           <b class="red"># chars go to new item</b>
                                    <b class="red"># single char to indicate tag
</b>        self.struct.append(chr(self.taglist.index(name)+<b>3</b>))

    <b class="blue">def</b> endElement(self, name):
        self.state.pop()            <b class="red"># pop current tag off stack</b>
        self.newstate = <b>1</b>           <b class="red"># chars go to new item</b>
        self.struct.append(chr(<b>1</b>))  <b class="red"># \0x01 is endtag in struct</b>

    <b class="blue">def</b> characters(self, ch):
        currstate = self.state[-<b>1</b>]
        <b class="blue">if</b> self.newstate:           <b class="red"># either add new chars to state item</b>
            self.contentdct[currstate].append(ch)
            self.newstate = <b>0</b>
            self.struct.append(chr(<b>2</b>))
                                    <b class="red"># \0x02 content placeholder in struct
</b>        <b class="blue">else</b>:                       <b class="red"># or append the chars to current item</b>
            self.contentdct[currstate][-<b>1</b>] += ch

<b class="blue">if</b> __name__ == <b class="green">'__main__'</b>:
    parser = xml.sax.make_parser()
    handler = StructExtractor()
    parser.setContentHandler(handler)
    parser.parse(sys.stdin)</code>
<p>
  Using SAX rather than DOM, makes this tranformation fairly time
  efficient, even though time was not a large consideration in
  developing it.  To reverse the transformation:
</p>
<code type="section">
<heading refname="code1" type="code" toc="yes">struct2xml.py</heading>
<b class="blue">def</b> struct2xml(s):
    tags, struct, content = s.split(chr(<b>0</b>))
    taglist = tags.split(<b class="green">'\n'</b>)      <b class="red"># all the tags</b>
    contentlist = []                <b class="red"># list-of-lists of content items</b>
    <b class="blue">for</b> block <b class="blue">in</b> content.split(chr(<b>1</b>)):
        contents = block.split(chr(<b>2</b>))
        contents.reverse()          <b class="red"># pop off content items from end</b>
        contentlist.append(contents)
    state =  []                     <b class="red"># stack for tag state</b>
    skeleton = []                   <b class="red"># templatized version of XML</b>
    <b class="blue">for</b> c <b class="blue">in</b> struct:
        i = ord(c)
        <b class="blue">if</b> i &amp;gt;= <b>3</b>:                  <b class="red"># start of element</b>
            i -= <b>3</b>                  <b class="red"># adjust for struct tag index offset</b>
            tag = taglist[i]        <b class="red"># spell out the tag from taglist</b>
            state.append(tag)       <b class="red"># push current tag</b>
            skeleton.append(<b class="green">'&amp;lt;%s&amp;gt;'</b> % tag)
                                    <b class="red"># insert the element start tag
</b>        <b class="blue">elif</b> i == <b>1</b>:                <b class="red"># end of element</b>
            tag = state.pop()       <b class="red"># pop current tag off stack</b>
            skeleton.append(<b class="green">'&amp;lt;/%s&amp;gt;'</b> % tag)
                                    <b class="red"># insert the element end tag
</b>        <b class="blue">elif</b> i == <b>2</b>:                <b class="red"># insert element content</b>
            tag = state[-<b>1</b>]
            item = contentlist[taglist.index(tag)].pop()
            item = item.replace(<b class="green">'&amp;amp;'</b>,<b class="green">'&amp;amp;amp;'</b>)
            skeleton.append(item)   <b class="red"># add bare tag to indicate content</b>
        <b class="blue">else</b>:
            <b class="blue">raise</b> ValueError, <b class="green">"Unexpected structure tag: ord(%d)"</b> % i

    <b class="blue">return</b> <b class="green">''</b>.join(skeleton)

<b class="blue">if</b> __name__ == <b class="green">'__main__'</b>:
    <b class="blue">import</b> sys
    <b class="blue">print</b> struct2xml(sys.stdin.read()),</code>
<p>
<heading refname="h1" type="major" toc="yes">Compressing The Transforms</heading>
 </p>
<p>
  The real meat of the discussed tranformation comes when we try
  to compress the results.  If all is as desired, <code type="inline">foo.struct</code>
  will be significantly more compressible than <code type="inline">foo.xml</code>, even
  though the two contain identical information (which is provable
  since they are symmetrically derivable).  In a sense,
  <code type="inline">xml2struct.py</code> is already a sort of compression algorithm (it
  produces somewhat smaller files), but the real point is not to
  use it directly but to aid further compression.
</p>
<p>
  Let's look at some sizes, including a few repeated from above.
  Some results from XMill are thrown in for comparison, these
  include the name <code type="inline"><b>.xmi.</b></code> (XMill is available in versions
  using <code type="inline">gzip</code> and <code type="inline">bzip2</code> algorithms):
</p>
<code type="section">
<heading refname="code1" type="code" toc="yes">Directory listing of "structured XML"</heading>
 228610  hamlet.struct
  57533  hamlet.struct.bz2
  57522  hamlet.xml.bz2
  71060  hamlet.struct.gz
  78581  hamlet.xml.gz
  61823  hamlet.xmi.bz2
  75247  hamlet.xmi.gz
</code>
<p>
  The results on this prose document are somewhat mixed.
  "Restructuring" the XML document assists <code type="inline">gzip</code> quite a bit.
  But it turns out that plain old <code type="inline">bzip2</code> on the original XML
  file does 11 bytes better at generating a compressible
  structure than do my attempts.  Of course, I am comforted that
  XMill behaves similarly--but worse--than my tranformation.
</p>
<p>
  We do a quite a bit better with Weblog files.  Here it actually
  pays off.
</p>
<code type="section">
<heading refname="code1" type="code" toc="yes">Directory listing 2 of "structured XML"</heading>
1764816  weblog.struct
  59955  weblog.struct.bz2
  66994  weblog.xml.bz2
  56156  weblog.txt.bz2
  76183  weblog.struct.gz
 115524  weblog.xml.gz
  59409  weblog.xmi.bz2
  74439  weblog.xmi.gz
</code>
<p>
  As before, restructuring the XML Weblog aids <code type="inline">gzip</code>
  compression extremely significantly.  But since <code type="inline">gzip</code> is not
  our preferred technique anymore, this is only moderately
  interesting.  What is of genuine interest is that we have also
  improved the compression rate of the--already
  wonderful--'bzip2' by 11%.  While maybe not earth-shattering,
  this is enough to matter when worrying about megabytes.  For
  what it's worth, XMill does a bit better than <code type="inline">xml2struct.py</code>
  in this case.  What is further interesting is that our
  compression of this restructured XML is within 7% of the best
  obtained compression of the original textual Weblog.
</p>
<p>
<heading refname="h1" type="major" toc="yes">Conclusion</heading>
 </p>
<p>
  The utility presented here is a preliminary attempts, but even
  in early form it does surprisingly well--at least in some
  cases--of squeezing those last bytes out of compressed XML
  files.  With a little refinement and experimentation, I expect
  that a few percent more reduction could be obtained.  Part of
  what makes writing this utility hard is that <code type="inline">bzip2</code> does
  <i>such</i> a good job to start with.  I was honestly surprised by
  just how effective the Burrows-Wheeler algorithm was when I
  started empirical testing.
</p>
<p>
  There are some commercial utilities that attempt to perform XML
  compression in a manner that utilizes knowledge of the specific
  DTDs of compressed documents.  It is quite likely that these
  techniques obtain additional compression.  However,
  <code type="inline">xml2struct.py</code> and XMill have the nice advantage of being
  simple command line tools that one can transparently apply to
  XML files.  Custom programming of every compression is not
  always desirable or possible--but where it is, squeezing out
  even more bytes might be an obtainable goal.
</p>
<p>
<heading refname="h1" type="major" toc="yes">Resources</heading>
 </p>
<p>
  Much of the inspiration for this article comes from the work of
  the XMill XML compressor.  Information and a downloadable
  version can be found at the below link.  The license requires a
  click-through, and the download page unfortunately seems to
  have a buggy script that does not allows downloads from all
  sites.
</p>
<blockquote>    <a href="http://www.research.att.com/sw/tools/xmill/">http://www.research.att.com/sw/tools/xmill/</a>
</blockquote>
<p>
  The complete plays of Shakespeare can be found in XML form at
  the below resource.  The document <code type="inline">hamlet.xml</code> used for testing
  purposes was obtained there:
</p>
<blockquote>    <a href="http://www.ibiblio.org/xml/examples/shakespeare/">http://www.ibiblio.org/xml/examples/shakespeare/</a>
</blockquote>
<p>
  The 1994 paper <attribution>A Block-sorting Lossless Data Compression
  Algorithm</attribution>, by M. Burrows and D.J. Wheeler, introduced the
  algorithm known now as Burrows-Wheeler.  This technique is
  implemented in the fairly popular <code type="inline">bzip2</code> utility:
</p>
<blockquote>    <a href="http://gatekeeper.dec.com/pub/DEC/SRC/research-reports/abstracts/src-rr-124.html">http://gatekeeper.dec.com/pub/DEC/SRC/research-reports/abstracts/src-rr-124.html</a>
</blockquote>
<p>
  Many Unix-like systems include <code type="inline">bzip2</code> as part of their
  standard distribution.  For other platforms--or for newer
  versions--'bzip2' can be found at:
</p>
<blockquote>    <a href="http://sources.redhat.com/bzip2/">http://sources.redhat.com/bzip2/</a>
</blockquote>
<p>
  I wrote what I believe is a good general introduction to data
  compression.  It can be found at:
</p>
<blockquote>    <a href="http://www.gnosis.cx/publish/programming/compression_primer.html">http://www.gnosis.cx/publish/programming/compression_primer.html</a>
</blockquote>
<p>
<heading refname="h1" type="major" toc="yes">About The Author</heading>
 </p>
</article>
