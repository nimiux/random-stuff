<?php

function wlog($s) {
   print($s."\n"); 
}

class Socket {

   var $_type = AF_INET;
   var $_socket = null;
   
   // getprotobyname
   // getservbyname
   var $_address;
   var $_port;
  
   function Socket($t, $a, $p) {
      $this->_type=$t;    
      $this->_address=$a;    
      $this->_port=$p;    
   }
   
   function Destroy() {
      socket_close($this->_socket);
   }

   function error($s) {
      if (isset($this->_socket)) {
	 $e=socket_strerror(socket_last_error($this->_socket));
	 wlog($s.$e);
      } 
   }

   function init() {
      $type=($this->_type == (AF_INET?SOL_TCP:SOL_SOCKET));
      if ($this->_socket=socket_create($this->_type, SOCK_STREAM, $type)) {
	 return true; 
      } else {
	 error('Error::init ');
	 return false;
      }
   }

}

class ServerSocket extends Socket {

  
   function ServerSocket($t, $a, $p) {
      parent::Socket($t, $a, $p);
   }

   function bind() {
      if ($this->_type == AF_INET) {
	 $r=socket_bind($this->_socket, $this->_address, $this->_port);
      } else {
	 $r=socket_bind($this->_socket, $this->_address);
      }
      if (!$r) {
	 $this->error('Error::bind1 ');
	 return false;
      } else {
	 return true;
      }
   }

   function getclient() {
      if (!socket_listen($this->_socket, 5)) {
	 $this->error('Error::bind2 ');
	 return false;
      } else {
	 wlog("GOT CLIENT!");
	 if (false === ($csock=socket_accept($this->_socket))) {
	    $this->error('Error::bind3 ');
	    return false;
	 } else {
	 wlog("GOT A CLIENT!");
	    return $csock;
	 }
      }
   }
   
   function releaseclient($s) {
      socket_close($s);
   }
   
   function receivesend ($socket, & $s) { 
      wlog("En receivesend .\n"); 
      $clientdata=socket_read($socket, 2048, PHP_NORMAL_READ);
      if ($clientdata === false) {
	 $this->error('Error::recv1 ');
	 return false;
      } else {
         wlog("En receivesend 1.\n"); 
	 $s="ALgIEN te ha escuchado:".$clientdata;
	 $r=socket_write($socket, $s, strlen($s));
	 if ($r === false) {
	    $this->error('Error::recv2 ');
	    return false;
	 } else {
      wlog("En receivesend 2.\n"); 
	    if ($r != strlen($s)) {
	        $this->error('Error::recv3 ');
	        return false;
	    } else {
      wlog("En receivesend 3.\n"); 
	        wlog('Se ha enviado: '.$s); 
		return true;
	    } 
	 } 
      } 
   } 
}

class ClientSocket extends Socket {

  
   function ServerSocket($t, $a, $p) {
      parent::Socket($t, $a, $p);
   }


   function bind() {
      if (!($r=socket_connect($this->_socket, $this->_address, $this->_port))) {
	 $this->error('Error::bind1 ');
	 return false;
      } else {
	 return true;
      }
   }

   function sendreceive (& $s) { 
      wlog("En sendreceive.\n"); 
      if (false === ($r=socket_write($this->_socket, $s, strlen($s)))) {
	 $this->error('Error::send1 ');
	 return false;
      } else {
      wlog("En sendreceive 1.\n"); 
	 if ($r != strlen($s)) {
	    $this->error('Error::send2 ');
	    return false;
	 } else {
      wlog("En sendreceive 2.\n"); 
	    if (false === ($read=socket_read($this->_socket, 2048, PHP_NORMAL_READ))) {
	       $this->error('Error::send3 ');
	       return false;
	    } else {
      wlog("En sendreceive 3.\n"); 
	       $s=$read;
	       wlog($read); 
	       return true;
	    } 
	 } 
      } 
   } 
}


?>
