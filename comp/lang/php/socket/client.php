<?php 

include('socket.php');

if (isset($argv[1]) && isset($argv[2])) {
   $ss = new ClientSocket(AF_UNIX, $argv[1],$argv[2]);
   if (!$ss->init()) {
      print("Error al inicializar el socket\n");
   } else {
      if (!$ss->bind()) {
	 print("Error al hacer el bind.\n");
      } else {
	 $s=$argv[3]."\n";
	 if (false === $ss->sendreceive($s)) {
	    print("Error al enviar\n");
	 } else {
	    print("OK\n");
	 }
      }
   } 
   $ss->Destroy();
} else {
   print("Uso: $argv[0] <host> <port>\n");
}

?>
