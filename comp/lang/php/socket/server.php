<?php 

include('socket.php');

$alive=true;

if (isset($argv[1]) && isset($argv[2])) {
   $ss = new ServerSocket(AF_UNIX, $argv[1],$argv[2]);
   if (!$ss->init()) {
      print("Error al inicializar el socket\n");
   } else {
      if (!$ss->bind()) {
	 print("Error al hacer bind del socket\n");
      } else {
	 while ($alive) {
	    if (false === ($socket=$ss->getclient())) {
	       print("Error al aceptar conn\n");
	    } else {
	       if (!($ss->receivesend($socket, $o))) {
		  print("Error al enviar\n");
	       } else {
		  print("Transaction processed: $o\n");
		  $alive=($o!='END');
	       }
	    $ss->releaseclient($socket);
	    usleep(100000);
	    }
	 }
      }
   }  
   $ss->Destroy();
}  else {
   print("Uso: $argv[0] <host> <port>\n");
}
