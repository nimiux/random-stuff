<?php

function pc_tab_expand($a) {
   $tab_stop=8;
   while (strstr($a,"\t")) {
      $a=preg_replace('/^([^\t]*)(\t+)/e',
                      "'\\1'.str_repeat(' ',strlen('\\2') *
                      $tab_stop - strlen('\\1') % $tab_stop)", $a);
   }
   return $a;
}

function pc_tab_unexpand($x) {
   $tab_stop=8;
   $lines=explode("\n",$x);
   for($i=0,$j=count($lines);$i<$j;$i++) {
      $lines[$i]=pc_tab_expand($lines[$i]);
      $e=preg_split("/(.\{$tab_stop})/",$lines[$i],-1,PREG_SPLIT_DELIM_CAPTURE);
      $lastbit=array_pop($e);
      if(!isset($lastbit)) { $lastbit= ''; }
      if ($lastbit == str_repeat(' ',$tab_stop)) { $lastbit= "\t"; }
      for ($m=0,$n=count($e);$m<$n;$m++) {
         $e[$m]=preg_replace('/  +$/',"\t",$e[$m]);
      }
      $lines[$i]=join('',$e).$lastbit;
   }
   $x=join("\n",$lines);
   return $x;
}

?>
