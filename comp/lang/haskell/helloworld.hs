module Main where

main = putStrLn "Hello, World!"

computation a b c d = (a + b^2+ c^3 + d^4)
check = 1 + 2^2 + 3^3 + 5^4

fillOne   = computation 1  -- specify "a"
fillTwo   = fillOne 2      -- specify "b"
fillThree = fillTwo 3      -- specify "c"
answer    = fillThree 5    -- specify "d"



