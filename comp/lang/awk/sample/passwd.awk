#!/bin/awk -f
# -*- mode: awk; -*-

BEGIN {
    FS=":";
}
{
    print $5 " has account " $1;
}
END {
    people="folks";
    printf "That's all %s!", people;
}


