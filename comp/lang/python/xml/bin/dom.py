## Document Object Model


import sys
import cStringIO
from UserDict import UserDict
import cStringIO
from xml.dom.ext.reader import Sax2
from xml.dom.ext import Print, PrettyPrint
from xml.dom import Document 
from xml.dom import DocumentType
from xml.dom.NodeFilter import NodeFilter
from xml.marshal import generic
from xml.dom import implementation
from xml.dom import EMPTY_NAMESPACE, XML_NAMESPACE
from xml.dom import minidom 


def normalize(text):
   """ Normalizes a text """
   return " ".join(text.split())

class Pea(UserDict):
   def __init__(self, list):
      UserDict.__init__(self)
      for elem in list:
	 self[elem]=''
   
   def __createdoc(self, name):
      self.doctype=implementation.createDocumentType(name, None, None)
      self.doc= implementation.createDocument(EMPTY_NAMESPACE, name, self.doctype)
      self.docelem=sel.doc.documentElement

   def __print(self):
      PrettyPrint(doc)

   def createtext(self):
      text=self.doc.createTextNode("Text")
      self.docelem.appendChild(text)
   

if __name__ == '__main__':
   print "Python: %s" % sys.version


   a=Pea(['a', 'b'])


   try: 
      file=sys.argv[1]
   except Exception:
      file='comic.xml'

   comic=""" <collection>
   <comic title="Sandman" number='621'>
      <writer>Neil Gaiman</writer>
      <penciller pages="1-9,18-24">Glyn Dillon</penciller>
      <penciller pages='10-17'>Charles Vess</penciller>
   </comic>
   </collection> """
   input = cStringIO.StringIO(comic)

   # create Reader object
   reader = Sax2.Reader()

   # parse the document
   doc = reader.fromStream(input)
   input.close()
   print "This is the doc:"
   Print(doc, sys.stdout)
   print "\nThis is the pretty doc:"
   PrettyPrint(doc, sys.stdout)


   print "GEN TIME....."
   dt=implementation.createDocumentType("process", None, None)
   doc= implementation.createDocument(EMPTY_NAMESPACE, "process", dt)
   docelem=doc.documentElement

   docelem.setAttributeNS(XML_NAMESPACE, "xml:lang", "en")
   text=doc.createTextNode("Text")
   docelem.appendChild(text)
   print "This is my doc:"
   PrettyPrint(doc)

   print "WALKING...."
   walker = doc.createTreeWalker(docelem, NodeFilter.SHOW_ELEMENT, None, 0)

   while 1:
      print walker.currentNode.tagName
      next = walker.nextNode()
      if next is None: break

   # Minidom
   xmldoc=minidom.parse('russiansample.xml')
   title=xmldoc.getElementsByTagName('title')[0].firstChild.data
   title
   print title
   convertedtitle = title.encode('koi8-r')
   convertedtitle
   print convertedtitle

   # XPath
   from xml.ns import XSLT
   #The XSLT namespace http://www.w3.org/1999/XSL/Transform
   NS = XSLT.BASE

   from xml.xpath import Context, Evaluate
   #Create an XPath context with the given DOM node
   #With no other nodes in the context list
   #(list size 1, current position 1)
   #And the given prefix/namespace mapping
   con = Context.Context(doc, 1, 1, processorNss={"xsl": NS})

   #Evaluate the XPath expression and return the resulting
   #Python list of nodes
   result = Evaluate("//xsl:*", context=con)

   # MARSHALLING
   from xml.marshal import generic
   xml_string=generic.dumps( (1, 2.0, 'name', [2,3,5,7]) )

   from xml.marshal.generic import Marshaller
   marshal = Marshaller()
   obj = {1: [2, 3], 'a': 'b'}
   #Dump to a string
   xml_form = marshal.dumps(obj)

   import xml.dom.ext.c14n 
