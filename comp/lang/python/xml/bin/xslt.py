#!/usr/bin/python

import sys
import libxml2
import libxslt


if __name__ == "__main__":
   try:
      # XML file and Style Sheet
      file=sys.argv[1]
      stylesheet=sys.argv[2]
   except: 
      print("Use: %s <xmlfile> <xslfile>" % (sys.argv[0]))
      sys.exit() 

   styledoc = libxml2.parseFile(stylesheet)
   style = libxslt.parseStylesheetDoc(styledoc)
   doc = libxml2.parseFile(file)
   result = style.applyStylesheet(doc, None)
   #style.saveResultToFilename("foo", result, 0)
   style.saveResultToFile(sys.stdout, result)
   style.freeStylesheet()
   doc.freeDoc()
   result.freeDoc()

