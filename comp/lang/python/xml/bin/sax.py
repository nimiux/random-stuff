import sys
from xml.sax import make_parser
from xml.sax.handler import feature_namespaces
from xml.sax import saxutils 
from xml.sax import ContentHandler
import string

def normalize(text):
   """ Normalizes a text """
   return " ".join(text.split())

class FindIssue(saxutils.DefaultHandler):

   def __init__(self, title, number):
      self.search_title, self.search_number = title, number

   def error(self, exception):
      sys.stderr.write("\%s\n" % exception)

   def startElement(self, name, attrs):
      print "Starting element %s..." % name
      # If it's not a comic element, ignore it
      if name != 'comic': return
      # Look for the title and number attributes (see text)
      title = attrs.get('title', None)
      number = attrs.get('number', None)
      if (title == self.search_title and number == self.search_number):
	 print title, '#' + str(number), 'found'

class FindWriter(ContentHandler):
   def __init__(self, search_name):
      self.search_name = normalize(search_name)
      self.inWriterContent = 0

   def startElement(self, name, attrs):
      if name == 'comic':
	    title = normalize(attrs.get('title', ""))
	    number = normalize(attrs.get('number', ""))
	    self.this_title=title
	    self.this_number=number
      elif name == 'writer':
	 self.inWriterContent = 1
	 self.writerName=""
   
   def endElement(self, name):
      if name == 'writer':
	 self.inWriterContent = 0 
	 self.writerName  = normalize(self.writerName)
	 if self.search_name == self.writerName:
	       print 'Found:', self.this_title, self.this_number

   def characters(self, char):
      if self.inWriterContent:
	 self.writerName += char
   
class FindElemNS(saxutils.DefaultHandler):

   def __init__(self, link):
      self.search_link = link 

   def error(self, exception):
      sys.stderr.write("\%s\n" % exception)

   def startElementNS(self, uri, qname, attrs):
      print "Starting element %s" % uri[1]
      print "Qualified name: %s" % qname 
      print "\t URI: %s " % uri[0]
      # If it's not a comic element, ignore it
      if uri[1] != 'elem': return
      # Look for the title and number attributes (see text)
      link = attrs.get('xlink:href', None)
      print dir(attrs)
      name=attrs.getValueByQName(qname)
      print "--> " + name 
      if (link == self.search_link):
	 print link, '#' + str(number), 'found'
   
   def endElementNS(self, localname, qname):
      print "Ending element %s %s" % localname, qname
      

if __name__ == '__main__':
   try: 
      file=sys.argv[1]
   except Exception:
      file='comic.xml'
   # Create a parser
   parser = make_parser()
   # Disable namespace processing
   parser.setFeature(feature_namespaces, 0)
   
   # Create the handler
   dh = FindIssue('Sandman', '621')
   # Tell the parser to use our handler
   print "FindIssue"
   parser.setContentHandler(dh)
   # Parse the input
   parser.parse(file)
   fw = FindWriter('Neil Gaiman')
   print "FindWriter"
   parser.setContentHandler(fw)
   parser.parse(file)
  
   parserns=make_parser()
   parserns.setFeature(feature_namespaces, 1)
   ns = FindElemNS('www.nimi')
   print "FindElemNS"
   parserns.setContentHandler(ns)
   # Parse the input
   parserns.parse('ns.xml')
