# Define your specialized handler classes
from xml.sax import ContentHandler, ...
class docHandler(ContentHandler):
   pass

# Create an instance of the handler classes
dh = docHandler()

# Create an XML parser
parser = ...

# Tell the parser to use your handler instance
parser.setContentHandler(dh)

# Parse the file; your handler's methods will get called
parser.parse(sys.stdin)
