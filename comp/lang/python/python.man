# My Python Manual
# 070809

PENDING
__future__
from __future__ import with_statement

__all__

# -*- coding: iso-8859-15 -*-
#  More precisely, the first or second line must match the regular
#      expression "coding[:=]\s*([-\w.]+)".
#
#

## Reserved words.

Conditions:
and
or
not
in
is   -Checks for same identity

Flow Control:
for
if
else
elif
while
try
except
raise    
finally
break
continue
return
pass

Modules:
from
import

Functions:
def
yield
lambda

Identifiers:
del
assert
global

Misc:    
exec
print s1, s2, s3, ...  print. print a,  <- no new line
class

## Identifiers
_    Last evaluation in interactive interpreter
_*    Don't import when "from module import *"
__*__   System defined names
__*     Class private names

Environment variables:
PYTHONSTARTUP : init program
PYTHONPATH : module search

# Decorators
 0 # No parameters
 1 # -*- coding: utf-8 -*-
 2
 3 def memo(f):
 4     cache={}
 5     def memof(arg):
 6         if not arg in cache:
 7              cache[arg]=f(arg)
 8         return cache[arg]
 9     return memof
10
11 @memo
12 def factorial(n):
13     print 'Calculando, n = ',n
14     if n > 2:
15         return n * factorial(n-1)
16     else:
17         return n
18

0 # With parameters
1 # -*- coding: utf-8 -*-
2 import random
3
4 def logger(nombre):
5     def wrapper(f):
6          def f2(*args):
7              print '===> Entrando a',nombre
8              r=f(*args)
9              print '<=== Saliendo de',nombre
0              return r
1          return f2
2      return wrapper
3
4 @logger('F1')
5 def f1():
6      print 'Estoy haciendo algo importante'
7
8 @logger('F2')
9 def f2():
0      print 'Estoy haciendo algo no tan importante'
1
2 @logger('Master')
3 def f3():
4      print 'Hago varias cosas'
5      for f in range(1,5):
6          random.choice([f1,f2])()
7
8 f3()

##Built-in Fucntions

dir(__builtins__)
dir(__builtin__)

import __builtin__


## Functional programming

"Short-circuit" conditional calls in Python
  # Normal statement-based flow control
  if <cond1>:    func1()
  elif <cond2>: func2()
  else:          func3()
  # Equivalent "short circuit" expression
  (<cond1> and func1()) or (<cond2> and func2()) or (func3())
  # Example "short circuit" expression
  >>> x = 3
  >>> def pr(s): return s
  >>> (x==1 and pr('one')) or (x==2 and pr('two')) or (pr('other'))
  'other'
  >>> x = 2
  >>> (x==1 and pr('one')) or (x==2 and pr('two')) or (pr('other'))
  'two'

Lambda with short-circuiting in Python
  >>> pr = lambda s:s
  >>> namenum = lambda x: (x==1 and pr("one")) \
  ....                   or (x==2 and pr("two")) \
  ....                   or (pr("other"))
  >>> namenum(1)
  'one'
  >>> namenum(2)
  'two'
  >>> namenum(3)
  'other'

Replacing loops
 for e in lst:  func(e)     # statement-based loop
 map(func,lst)          # map()-based loop

Map-based action sequence
# let's create an execution utility function
do_it = lambda f: f()
# let f1, f2, f3 (etc) be functions that perform actions
map(do_it, [f1,f2,f3])   # map()-based action sequence

Functional 'while' looping
 # statement-based while loop
 while <cond>:
      <pre-suite>
      if <break_condition>:
          break
      else:
          <suite>
 # FP-style recursive while loop
 def while_block():
      <pre-suite>
      if <break_condition>:
          return 1
      else:
          <suite>
      return 0
 while_FP = lambda: (<cond> and while_block()) or while_FP()
 while_FP()


Functional 'echo' loop
 # imperative version of "echo()"
 def echo_IMP():
      while 1:
          x = raw_input("IMP -- ")
          if x == 'quit':
               break
          else
               print x
 echo_IMP()
# utility function for "identity with side-effect"
def monadic_print(x):
    print x
    return x
# FP version of "echo()"
echo_FP = lambda: monadic_print(raw_input("FP -- "))=='quit' or echo_FP()
echo_FP()

Imperative code for "print big products"
 # Nested loop procedural style for finding big products
 xs = (1,2,3,4)
 ys = (10,15,3,22)
 bigmuls = []
 # ...more stuff...
 for x in xs:
      for y in ys:
          # ...more stuff...
          if x*y > 25:
              bigmuls.append((x,y))
              # ...more stuff...
 # ...more stuff...
print bigmuls

Functional approach to our goal
 bigmuls = lambda xs,ys: filter(lambda (x,y):x*y > 25, combine(xs,ys))
 combine = lambda xs,ys: map(None, xs*len(ys), dupelms(ys,len(xs)))
 dupelms = lambda lst,n: reduce(lambda s,t:s+t, map(lambda l,n=n: [l]*n, lst))
 print bigmuls((1,2,3,4),(10,15,3,22))

print [(x,y) for x in (1,2,3,4) for y in (10,15,3,22) if x*y > 25]

FP session with rebinding causing mischief
 >>>  car = lambda lst: lst[0]
 >>>  cdr = lambda lst: lst[1:]
 >>>  sum2 = lambda lst: car(lst)+car(cdr(lst))
 >>>  sum2(range(10))
 1
 >>>  car = lambda lst: lst[2]
 >>>  sum2(range(10))
 5

Python FP session with guarded rebinding
 >>> from functional import *
 >>> let = Bindings()
 >>> let.car = lambda lst: lst[0]
 >>> let.car = lambda lst: lst[2]
 Traceback (innermost last):
   File "<stdin>", line 1, in ?
   File "d:\tools\functional.py", line 976, in __setattr__
      raise BindingError, "Binding '%s' cannot be modified." % name
 functional.BindingError: Binding 'car' cannot be modified.
 >>> car(range(10))
 0

Python FP session using immutable namespaces
>>> let = Bindings()      # "Real world" function names
>>> let.r10 = range(10)
>>> let.car = lambda lst: lst[0]
>>> let.cdr = lambda lst: lst[1:]
>>> eval('car(r10)+car(cdr(r10))', namespace(let))
>>> inv = Bindings()      # "Inverted list" function names
>>> inv.r10 = let.r10
>>> inv.car = lambda lst: lst[-1]
>>> inv.cdr = lambda lst: lst[:-1]
>>> eval('car(r10)+car(cdr(r10))', namespace(inv))
17

Session showing cargo variable
 >>> def a(n):
 ...     add7 = b(n)
 ...     return add7
 ...
 >>> def b(n):
  ...      i = 7
  ...      j = c(i,n)
  ...      return j
  ...
  >>> def c(i,n):
  ...      return i+n
  ...
  >>> a(10)      # Pass cargo value for use downstream
  17

Session showing global variable
  >>> N = 10
  >>> def addN(i):
  ...      global N
  ...      return i+N
  ...
  >>> addN(7)    # Add global N to argument
  17
  >>> N = 20
  >>> addN(6)    # Add global N to argument
  26

Session showing frozen variable
  >>> N = 10
  >>> def addN(i, n=N):
  ...      return i+n
  ...
  >>> addN(5)    # Add 10
  15
  >>> N = 20
  >>> addN(6)    # Add 10 (current N doesn't matter)

Tax calculation class/instance
class TaxCalc:
    def taxdue(self):return (self.income-self.deduct)*self.rate
taxclass = TaxCalc()
taxclass.income = 50000
taxclass.rate = 0.30
taxclass.deduct = 10000
print"Pythonic OOP taxes due =", taxclass.taxdue()

Smalltalk-style tax calculation
 class TaxCalc:
      def taxdue(self):return (self.income-self.deduct)*self.rate
      def setIncome(self,income):
          self.income = income
          return self
      def setDeduct(self,deduct):
          self.deduct = deduct
          return self
      def setRate(self,rate):
          self.rate = rate
          return self
 print"Smalltalk-style taxes due =", \
        TaxCalc().setIncome(50000).setRate(0.30).setDeduct(10000).taxdue()

Functional-style tax calculations
from functional import *
taxdue        =  lambda: (income-deduct)*rate
incomeClosure  = lambda income,taxdue: closure(taxdue)
deductClosure  = lambda deduct,taxdue: closure(taxdue)
rateClosure   =  lambda rate,taxdue: closure(taxdue)
taxFP = taxdue
taxFP = incomeClosure(50000,taxFP)
taxFP = rateClosure(0.30,taxFP)
taxFP = deductClosure(10000,taxFP)
print"Functional taxes due =",taxFP()
print"Lisp-style taxes due =", \
      incomeClosure(50000,
          rateClosure(0.30,
              deductClosure(10000, taxdue)))()

FP session with guarded rebinding
 >>> from functional import *
 >>> let = Bindings()
 >>> let.car = lambda lst: lst[0]
 >>> let.car = lambda lst: lst[2]
 Traceback (innermost last):
   File "<stdin>", line 1, in ?
   File "d:\tools\functional.py", line 976, in __setattr__
  raise BindingError, "Binding '%s' cannot be modified." % name
  functional.BindingError: Binding 'car' cannot be modified.
  >>> let.car(range(10))
  0

Haskell expression-level name bindings
  -- car (x:xs) = x -- *could* create module-level binding
  list_of_list = [[1,2,3],[4,5,6],[7,8,9]]
  -- 'where' clause for expression-level binding
  firsts1 = [car x | x <- list_of_list] where car (x:xs) = x
  -- 'let' clause for expression-level binding
  firsts2 = let car (x:xs) = x in [car x | x <- list_of_list]
  -- more idiomatic higher-order 'map' technique
  firsts3 = map car list_of_list where car (x:xs) = x
  -- Result: firsts1 == firsts2 == firsts3 == [1,4,7]

Python 2.0+ expression-level name bindings
  >>> list_of_list = [[1,2,3],[4,5,6],[7,8,9]]
  >>> [car_x for x in list_of_list for car_x in
   (x[0],)]
  [1, 4, 7]

Block-level bindings with 'map()'
  >>> list_of_list = [[1,2,3],[4,5,6],[7,8,9]]
  >>> let = Bindings()
  >>> let.car = lambda l: l[0]
  >>> map(let.car,list_of_list)
  [1, 4, 7]

  # Compare Haskell expression:
  # result = func car_car
  #           where
  #               car (x:xs) = x
  #               car_car = car (car list_of_list)
  #               func x = x + x^2
  >>> [func for x in list_of_list
  ...
  for car in (x[0],)
  ...
  for func in (car+car**2,)][0]
  2

Efficient stepping down from list comprehension
  >>> [func for x in list_of_list[:1]
  ...        for car in (x[0],)
  ...        for func in (car+car**2,)][0]
  2

Trivial function factory
  >>> def foo_factory():
  ...
  def foo():
  ...
  print "Foo function from factory"
  ...
  return foo
  ...
  >>> f = foo_factory()
  >>> f()
  Foo function from factory

curry() is named after the logician Haskell Curry, whose first name is also
used to name the above-mentioned programming language. The underlying insight
of "currying" is that it is possible to treat (almost) every function as a
partial function of just one argument.
All that is necessary for currying to work is to allow the return value of
functions to themselves be functions, but with the returned functions
"narrowed" or "closer to completion."
Each successive call to a curried return function "fills in" more of the data
involved in a final computation (data attached to a procedure).
Let's illustrate currying first with a very simple example in Haskell, then
with the same example repeated in Python using the functional module:

Currying a Haskell computation
  computation a b c d = (a + b^2+ c^3 + d^4)
  check = 1 + 2^2 + 3^3 + 5^4
  fillOne     = computation 1
  -- specify "a"
  fillTwo     = fillOne 2
  -- specify "b"
  fillThree = fillTwo 3
  -- specify "c"
  answer      = fillThree 5
  -- specify "d"
  -- Result: check == answer == 657

Currying a Python computation
  >>> from functional import curry
  >>> computation = lambda a,b,c,d: (a + b**2 + c**3 + d**4)
  >>> computation(1,2,3,5)
  657
  >>> fillZero = curry(computation)
  >>> fillOne     = fillZero(1)
  # specify "a"
  >>> fillTwo     = fillOne(2)
  # specify "b"
  >>> fillThree = fillTwo(3)
  # specify "c"
  >>> answer      = fillThree(5)
  # specify "d"
  >>> answer
  657

Curried tax calculations
  from functional import *
  taxcalc = lambda income,rate,deduct: (income-(deduct))*rate
  taxCurry = curry(taxcalc)
  taxCurry = taxCurry(50000)
  taxCurry = taxCurry(0.30)
  taxCurry = taxCurry(10000)
  print "Curried taxes due =",taxCurry
  print "Curried expression taxes due =", \
        curry(taxcalc)(50000)(0.30)(10000)

Unlike with closures, we need to curry the arguments in a specific order
(left to right). But note that functional also contains an rcurry() class that
will start at the other end (right to left).

Sequential calls to functions (with same args)
 >>> def a(x):
 ...     print x,
  ...       return "a"
  ...
  >>> def b(x):
  ...       print x*2,
  ...       return "b"
  ...
  >>> def c(x):
  ...       print x*3,
  ...       return "c"
  ...
  >>> r = also(a,b,c)
  >>> r
  <functional.sequential instance at 0xb86ac>
  >>> r(5)
  5 10 15
  'a'
  >>> sequential([a,b,c],main=c)('x')
  x xx xxx
  'c'

Ask about collections of return values
  >>>  from functional import *
  >>>  isEven = lambda n: (n%2 == 0)
  >>>  any([1,3,5,8], isEven)
  1
  >>>  any([1,3,5,7], isEven)
  0
  >>>  none_of([1,3,5,7], isEven)
  1
  >>>  all([2,4,6,8], isEven)
  1
  >>>  all([2,4,6,7], isEven)
  0

Creating compositional functions
 >>>  def minus7(n): return n-7
 ...
 >>>  def times3(n): return n*3
 ...
 >>>  minus7(10)
 3
 >>>  minustimes = compose(times3,minus7)
 >>>  minustimes(10)
 9
 >>>  times3(minus7(10))
 9
 >>>  timesminus = compose(minus7,times3)
 >>>  timesminus(10)
 23
 >>>  minus7(times3(10))
 23

import functools

functools.cmp_to_key(func)
sorted(iterable, key=cmp_to_key(locale.strcoll))  # locale-aware sort order

functools.total_ordering(cls)
@total_ordering
class Student:
    def __eq__(self, other):
        return ((self.lastname.lower(), self.firstname.lower()) ==
                (other.lastname.lower(), other.firstname.lower()))
    def __lt__(self, other):
        return ((self.lastname.lower(), self.firstname.lower()) <
                (other.lastname.lower(), other.firstname.lower()))

functools.partial(func[,*args][, **keywords])
>>> from functools import partial
>>> basetwo = partial(int, base=2)
>>> basetwo.__doc__ = 'Convert base 2 string to an int.'
>>> basetwo('10010')
18

functools.update_wrapper(wrapper, wrapped[, assigned][, updated])
functools.wraps(wrapped[, assigned][, updated])
This is a convenience function for invoking partial(update_wrapper, wrapped=wrapped, assigned=assigned, updated=updated) as a function decorator when defining a wrapper function.

>>> from functools import wraps
>>> def my_decorator(f):
...     @wraps(f)
...     def wrapper(*args, **kwds):
...         print 'Calling decorated function'
...         return f(*args, **kwds)
...     return wrapper
...
>>> @my_decorator
... def example():
...     """Docstring"""
...     print 'Called example function'
...
>>> example()
Calling decorated function
Called example function
>>> example.__name__
'example'
>>> example.__doc__
'Docstring'



# Math
abs(x)
divmod(a, b)
hex(x)
oct(x)
round(x[, n])
pow(x,y[, z])

# Lists

# Types
bool([x])
complex(real[, img])
dict([mapping-or-sequence])
float([x])
frozenset([iterable])
int([x[, radix]])
list([sequence])
object()
str(object)
set([iterable])
long([x[, radix]])
tuple([sequence])

# Callables
callable(object)
apply *deprecated*

# Strings
chr(i)
ord(C)
unichr(i)
unicode([object[, encoding [, errors]]])

Constants
   s=r"Raw Mode\n" 
   s=u'Unicode Mode'
   s=ur'Unicode Raw Mode'
   s=""" Here Docs """
   s[0] s[3:7] s[:4] s[6:] s[-1]

Substitution

(M) %
(O) Mapping key. (somekey)
(O) Conversion flags.
    # The value conversion will use the "alternate form" (where defined below).
    0 The conversion will be zero padded for numeric values.
    - The converted value is left adjusted (overrrides the "0" conversion if
       both are given).
      (space). A blank should be left before a positive number (or empty
      string) produced by a signed conversion.
    + A sign character ("+" or "-") will precede the conversion (overrides a
      "space" flag).
(O) Minimum field width. If "*", the actual width is read from the next
    element of the tuple in values, and the object to convertcomes after the
    minimum field width and optional precision.
(O) Precision, given as a "." followed by the actual precision. If "*", the
    actual precision is read from the next element, and the value to convert
    comes after the precision.
(O) Length modifier
(M) Conversion type
    d, i Signed integer decimal
    u    Unsigned decimal
    o    Unsigned octal. The alternate form causes a leading zero to be
         inserted between left-hand padding and the formatting of the number
         if the leading character or the result is no already a zero.
    x, X Unsigned hexadecimal (lowercase or uppercase). The alternate form
         causes a leading '0x' or '0X' to be inserted between left-hand
         padding and the formatting of the number if the leading character
         of the result is not already a zero.
    e, E Floating point exponential format (lowercase or uppercase). The
         alternate form causes the result to always contain a decimal point,
         even if no digits follow it. The precision determines the number of
         significant digits after the decimal point and defaults to 6.
    f, F Floating point decimal format. The alternate form causes the result
         to always contain a decimal point, even if no digits follow it. The
         precision determines the number of significant digits after the
         decimal point and defaults to 6.
    g, G Floating point format. Uses exponential format if exponent is greater
         than -4 or less than precision, decimal format otherwise. The
         alternate form causes the result to always contain a decimal point,
         and trailing zeroes are not removed as they would otherwise be. The
         precision determines the number of significant digits before and
         after the decimal point and defaults to 6.
    c    Single character (accepts integer or single character string)
    r    String. Converts any python object using repr(). The precision
         determines the maximal number or characters used.
    s    String. Converts any python object using str(). If the object or
         format provided is a unicode string, the resulting string will also
         be unicode. The precision determines the maximal number or characters
         used.
    %    No arguments converted, results in a '%' character.

# Classes
type(name, bases, dict)
isinstance(object, classinfo)
issubclass(class, classinfo)
classmethod(function)  @classmethod : Creates a method that can be bounded to a Class
staticmethod(function) -> @staticmethod
id(object)
delattr(object, name)
getattr(object, name[, default])
setattr(object, name, value)
hasattr(object, name)
property(   [fget[, fset[, fdel[, doc]]]])
super(type[, object-or-type])  
super returns proxy objects. Informally speaking, a proxy object is an object with the ability to dispatch to methods of other classes via delegation.
Technically, super works by overriding the __getattribute__ method in such a way that its instances becomes proxy objects providing access to the methods in the MRO. The dispatch is done in such a way that
super(cls, instance-or-subclass).meth(*args, **kw)
corresponds to
<right method in the MRO>(instance-or-subclass, *args, **kw)

# Iterators
enumerate(iterable)
iter(o[, sentinel])
len(mapping-or-sequence)
max(s[, args...])
min(s[, args...])
sorted(iterable[, cmp[, key[, reverse]]])
reversed(sequence)
range([start,] stop[, step])
xrange([start,] stop[, step])
sum(sequence[, start])
zip([iterable, ...])
filter(function, list) -> filter(f,l) = e de l | f(e)=true
   filter(function, list) is equivalent to 
     [item for item in list if function(item)] if function is not None
     and [item for item in list if item] if function is None.
map(function, list, ...) -> map(f,l1,l2,...)= f(e1,e2,...) | ei de lj
reduce(function, sequence[, initializer]) -> reduce(f,l)= f(..f(f(e1,e2),e3),..en)

"Iterator for looping over a sequence backwards"
class Reverse:
    def __init__(self, data):
        self.data = data
        self.index = len(data)
    def __iter__(self):
        return self
    def next(self):
        if self.index == 0:
            raise StopIteration
        self.index = self.index - 1
        return self.data[self.index]

Itertools

Infinite iterators
count()          start[,step]            start, start+step, start+2*step, ...
cycle()          p                       p0, p1, p2, ..., pN, p0, p1,...
repeat()         elem[,n]                elem, elem, elem, ...  {up to n times}

Iterators terminating on the shortest input sequence:
chain()          p, q,...                p0, p1, p2, ..., pN, q0, q1, q2, ...
izip()           p, q, ...               (p[0], q[0]), (p[1], q[1]), ...
izip_longest()   p, q, ...               (p[0], q[0]), (p[1], q[1]), ... {until any seq exhausted)

compress()       data, selectors         (data[0] if selectors[0]), (data[1] if selectors[1]), ...

groupby()        iterable[,keyfunc]      (value1, iterator1), (value2, iterator2), ...  {valuei are values returned by keyfunc}
tee()            iterator, n             iterator1, iterator2, ... 

dropwhile()      pred, seq               seq[n], seq[n+1], ... {starting when pred fails}
ifilter()        pred, seq               seq[0] if pred(seq[0]), seq[1] if pred(seq[1]), ...
ifilterfalse()   pred, seq               seq[0] if not pred(seq[0]), seq[1] if not pred(seq[1]), ...
takewhile()      pred, seq               seq[0], seq[1], ... {until pred fails}

islice()         seq[,start],stop[,step] seq[start:stop:step]

imap()           func, p, q, ...         func(p0, q0), func(p1, q1), ...
starmap()        func, seq               func(*seq[0]), func(*seq[1]), ...

Combinatoric generators:
product()        p, q, ... [repeat=1]    cartesian product
permutations()   p[, r]                  r-length tuples, all possible orderings, no repeated elements
combinations()   p, r                    r-length tuples, in sorted order, no repeated elements
combinations_with_replacement()  p, r    r-length tuples, in sorted order, with repeated elements

Samples:
product('ABCD', repeat=2)       AA AB AC AD BA BB BC BD CA CB CC CD DA DB DC DD
permutations('ABCD', 2)         AB AC AD BA BC BD CA CB CD DA DB DC
combinations('ABCD', 2)         AB AC AD BC BD CD
combinations_with_replacement('ABCD', 2)        AA AB AC AD BB BC BD CC CD DD

# Modules
__import__
reload(module)

# Collections
namedtuple()  factory function for creating tuple subclasses with named fields
deque     list-like container with fast appends and pops on either end    
Counter   dict subclass for counting hashable objects     
OrderedDict   dict subclass that remembers the order entries were added   
defaultdict   dict subclass that calls a factory function to supply missing values    

# Code
compile(string, filename, kind[, flags[, dont_inherit]])
   kind     exec, eval, single
eval(expression[, globals[, locals]])
execfile(filename[, globals[, locals]])

# Environment
globals()
locals()
vars([object])

# Misc
help([object])
type(object)
dir([object])
cmp(x,y)
file(filename[, mode[, bufsize]])
raw_input([prompt])
repr(object)
slice([start,] stop[, step])

# Exceptions
   try: 
      except error: 
      except (error1,error2,...): 
      except error, e: 
      else: 
      finally:

# Assigning
#
# There are actually three levels of copying a variable in Python.
# 1. If you do a straight forward assignment, you're copying the reference - 
# in other words, you're giving the variable a new name or an alias and 
# content changes under either name will be reflected in the same objects. 
# Our charlie and thecat is a good example of where you might do something 
# like this.
# 2. If you use the deepcopy function, then you'll be cloning an object and 
# you'll duplicate its content, so that any subsequent amendment to the 
# original or the copy will be unique to the original or to the copy. If you 
# had an object that contained this year's data and you wanted a new object 
# for next year's data too, you might do a deep copy ... then change next 
# year's data to reflect the event you are expecting to happen. Next year's 
# data changes but this year's remains unaltered.
# 3. The third (intermediate) level of copying in Python uses a list slice 
# notation and is described as a shallow copy. If you have a table of staff 
# members and you shallow copy it, you'll end up with a new table of staff 
# members. Take the copy, add in some extra staff, take some away, and only 
# the copy is effected. However, each staff member object refers to the same 
# data from both the original and new data sets, so if you change a property 
# of an individual staff member - say their home address is altered - then 
# that will change in both "copy"s. Once again, in the correct circumstance 
# this is a very natural way to handle data.

import copy
x = copy.copy(y)        # make a shallow copy of y
x = copy.deepcopy(y)    # make a deep copy of y
secondyear = team[:]
thirdyear = team

# Switch
#
def switch(var, value, switch, default = lambda x: x):
    """ A switch statement in Python 
        switch must be a dictionary of the form:

        { 'value1': lambda x: lambdafunc 
        , 'value2': lambda x: lambdafunc 
        ...
        , 'valueN': lambda x: lambdafunc 
        }
    """
    try:
        return switch[value](var)
    except KeyError:
        return default(var)


# Classes

    __cmp__ all comparison operators
    __eq__  == operator
    __ne__  != operator
    __repr__  string representation
    __iter__  Iterable
    __call__  Callable
    
# Class creation

def class_with_method(func):
    class klass: pass
    setattr(klass, func.__name__, func)
    return klass

def say_foo(self): print 'foo'

    Foo = class_with_method(say_foo)
    foo = Foo()
    foo.say_foo()


# Old style clases (classobj and instance)
    x.__class__
    class Myclass(Class,...) 
        # Initializer
        def __init__(self,...
        # Deletion
        def __del__(self,...

    Getters and setters
      class A:
       def __getattr__(self, name):
        #Called when an attribute lookup has not found the attribute in the 
        #usual places (i.e. it is not an instance attribute nor is it found in 
        #the class tree for self). name is the attribute name. This method 
        #should return the (computed) attribute value or raise an 
        #AttributeError exception.
        return 'This is the value of attribute  ' + name
            
       def __setattr__(self, name, value):
        #Infinite recursion:
        #exec("self."+name+'="New value: '+str(value)+'"')
        #Value should be inserted manually in instance's dict
        self.__dict__[name]=value
        #For new-style classes, rather than accessing the instance dictionary, 
        #it should call the base class method with the same name, for example, 
        #"object.__setattr__(self, name, value)".
                
# New style (type and class)
    type(x)
        class Myclass(object):
        #or
        class Myclass(type): 
        # Instance Methods:
        # Initializer
        def __init__(self,...
        # Constructor
        def __new__(self, *args, **kargs)

    Getters and setters
    __getattribute__(self, name)
    #Called unconditionally to implement attribute accesses for instances of 
    #the class. If the class also defines __getattr__(), the latter will not be 
    #called unless __getattribute__() either calls it explicitly or raises an 
    #AttributeError. This method should return the (computed) attribute value
    # or raise an AttributeError exception. In order to avoid infinite 
    # recursion in this method, its implementation should always call the base 
    # class method with the same name to access any attributes it needs, for 
    # example, 
            #"object.__getattribute__(self, name)".
            #
            #
            #
        
Descriptors
#The following methods only apply when an instance of the class containing 
#the method (a so-called descriptor class) appears in the class dictionary 
#of another new-style class, known as the owner class. In the examples below,
#``the attribute'' refers to the attribute whose name is the key of the 
#property in the owner class' __dict__. Descriptors can only be implemented 
#as new-style classes themselves.
# If an object defines both __get__ and __set__, it is considered a 
# # data descriptor. Descriptors that only define __get__ are called 
# non-data descriptors (they are typically used for methods but other uses 
# are possible).
#

__get__(    self, instance, owner)
#Called to get the attribute of the owner class (class attribute access) 
#or of an instance of that class (instance attribute access). owner is 
#always the owner class, while instance is the instance that the attribute 
#was accessed through, or None when the attribute is accessed through the 
#owner. This method should return the (computed) attribute value or raise 
#an AttributeError exception. 

__set__(    self, instance, value)
#Called to set the attribute on an instance instance of the owner class to 
#a new value, value. 

__delete__(        self, instance)
#Called to delete the attribute on an instance instance of the owner class. 
#

# Properties
class ClassWithProperty(object):    
    def __SetTheProperty(self, value):
        print "Setting the property"
        self.__m_the_property = value

    def __GetTheProperty(self):
        print "Getting the property"
        return self.__m_the_property

    def __DelTheProperty(self):
        print "Deleting the property"
        del self.__m_the_property

    TheProperty = property(fget=__GetTheProperty,
        fset=__SetTheProperty,
        fdel=__DelTheProperty,
        doc="The property description.")

    def __GetReadOnlyProperty(self):
        return "This is a calculated value."

    ReadOnlyProperty = property(fget=__GetReadOnlyProperty)

Static methods
    class MyClass(object):
        def SomeMethod(x):
            print x
        SomeMethod = staticmethod(SomeMethod)
            
Slots
    class X(object):
    __slots__ = ["m", "n"]
        
    >>> x = X()
    >>> x.m = 10
    >>> x.n = 10
    >>> x.k = 3
    Traceback (most recent call last):
    File "<interactive input>", line 1, in ?
    AttributeError: 'X' object has no attribute 'k'

Old style and new style
    __private-element
    Myclass.__module__
    Myclass.__doc__
    Myclass.__name__
    Myclass.__bases__
    Myclass.__dict__
    Myclass.__call__

Singleton classes
#El siguiente es un ejemplo de implementación de Singleton en Python 
#(no es thread safe)
        
    class  Singleton (object):
        instance = None       
        def __new__(cls, *args, **kargs): 
            if cls.instance is None:
                cls.instance = object.__new__(cls, *args, **kargs)
            return cls.instance
    #Usage
    mySingleton1 =  Singleton()
    mySingleton2 =  Singleton()
    #                                           
    #mySingleton1 y mySingleton2 son la misma instancia
    assert mySingleton1 is mySingleton2
    #                                           
    # Y otra posibilidad interesante es implementarlo como una metaclase:
    #          
    class Singleton(type):
                                
        def __init__(cls, name, bases, dct):
            cls.__instance = None
            type.__init__(cls, name, bases, dct)
                        
        def __call__(cls, *args, **kw):
            if cls.__instance is None:
                cls.__instance = type.__call__(cls, *args,**kw)
            return cls.__instance
        
    class A:
        __metaclass__ = Singleton
        # Definir aquí el resto de la interfaz
        
    a1 = A()
    a2 = A()
        
    assert a1 is a2

## Operators

import operator

## Containers 
class A:
    def __init__(self, list):
        self.__list=list
    def printlist(self):
        print self.__list
    def __len__(self):
        return len(self.__list)
    def __list__(self):
        return self
    def __getitem__(self, key):
        return self.__list[key]
    def __setitem__(self, key, value):
        self.__list[key]=value
    def __delitem__(self, key):
        self.__list.remove(key)
    def __contains__(self, item):
        try:
            return (self.__list.index(item) >= 0)
        except Exception, e:
            return False


# Generators
def reverse(data):
    for index in range(len(data)-1, -1, -1):
        yield data[index]


# Context Management Protocol
# with expression as var:
#    with-block
# Expression must evaluate to an object that implements this protocol
# Protocol
# 1. expression is evaluated
# 2. __enter__ method is executed. Returnes value is assigned to var
# 3. with-block is executed.
# 4. __exit__(type, value, traceback) is executed in case an exception occurs 
#    in with-block. If exits returns False exception is raised to next level. 
#    True exception is finished. If no exception __exit__ is called with None.

# Datatypes

Unicode:

import sys
sys.getdefaultencoding()
sys.setdefaultencoding('utf-8')

sys.stdin.encoding
sys.stdout.encoding

print u'test'
print unicode('test')
print u'año'
print unicode('año')  # uses default codec -> Error
print unicode('año', 'utf-8')
print u'test\u2605'
print u'text\N{BLACK STAR}'
print  unichr(0x1d160)

s = u'año'
s
print s
# Convert to other formating
s.encode('utf-8')
s.encode('utf-16')
s.encode('ascii', 'strict') # Default method
UnicodeEncodeError
s.encode('ascii', 'ignore')
‘ao’
s.encode('ascii', 'replace')
‘a?o’

import unicodedata

def to_unicode_or_bust(
    obj, encoding='utf-8'):
    if isinstance(obj, basestring):
        if not isinstance(obj, unicode):
            obj = unicode(obj, encoding)
    return obj

List SLices
   l=['a', 1,..]
   l[0:2]=[1,12]
   l[0:2]=[]
   l[1:1]=[2,3]
   l[:0]=l
   l[:]=[]

List Comprehension
   [ expression(x) for x in l]

Tuples ()
   t = e1,e2,...
   t=()
   v1,v2,...=t

Sets
   s=set(seq)
   s=set(string)
   a | b 
   a & b
   a - b
   a ^ b 

Dictionaries:
A mapping object maps hashable values to arbitrary objects
Mutable object

   dict(pepe=321,luis=2132)
   d = {'pepe': 21321, 'luis':213} 
   dict([('pepe', 21321), ('luis', 1321)])
   
   items()
   keys()
   values()
   iteritems()
   iterkeys()
   itervalues()

User Defined Functions
   def function(par=value,par=value, *arguments, **keywords):

Lambdas
   def incr(n)
      return lambda: x: x+n

   map(lambda x: x+1, list)


Files
   file.write(format % arguments)
   print format % (arguments)
   f=open(filename,mode)
   f.read(size)
   f.readline()
   f.readlines()
   for line in f:
   with open(filename) as f: for line in f:
   f.write(line);
   f.seek(off={0,1,2))
   f.close()

Errors
   class MyError(Exception):
      def __init__(self, value):
         self.value = value
      def __str__(self):
       return repr(self.value)
   ...
   raise erortype, errormsg
   raise erortype(errormsg)
   ...
   try:
      raise MyError(2*2)
   except MyError, e:
      print' My exception occurred, value:', e.value


Modules:
*.py Python source
*.pyc Byte compiled
*.pyo Byte compiled optimized

   if __name__ == "__main__":
       pass

   __init.py__ files required to treat directories as containing packages
   __name__
   import __builtin__ 
   from m import item1,item2,... 

    import imp
    def __import__ 


System
   sys
   os
   shutil
   glob
   datetime
   time
Data
   pickle # Convert to string representation
      dump(x,f)
      load(f)
   shelve
   string
   math
   random
   csv
   Queue
   array
   Collections
   bisect
   heapq
   decimal
   deque
   logging
Debug
   pdb
   timeit
   profile
   pstats
Devel
   getopt
   re
   doctest
   unittest
   gettext
   locale
   codecs
   ihooks
   rexec
   repr
   pprint
   textwrap
   struct
   threading
Packages
   imp
Mem
   gc
   weakref
Compress
   zlib
   gzip
   bz2
   zipfile
   tarfile
XML
   xmlrpclib
   SimpleXMLRPCServer
   xml.dom
   xml.sax
   pyexpat
   xmlproc
   pyxie
Lang
Net
   urllib2
   smtplib
   poplib
   email

Database
   Mother
   pyPgSQL

################### PYTHON DEVELOPMENT ######################
GUI Devel

easygui
pyKDE
pyGTK
pyQt
Tkinter
wxPython

Web

modpython
Zope
Django
TruboGears
web2py
webpy
CherryPy
################### PYTHON DEVELOPMENT ######################
