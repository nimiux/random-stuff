import copy
from pyPgSQL import PgSQL


# Queries

class Query:
    
    def __init__(self, relation):
        self.__error="OK"
        self.__relation=relation
        connection=relation.getconnection()
        self.__cursor=connection.cursor()

    def geterror(self):
        return self.__error
    
    def _execute(self, sqltext):
        try:
            print "executing %s" % (sqltext)
            return self.__cursor.execute(sqltext)
        except Exception, e:
            self.__error=e.message
    
    def __iter__(self):
        return self

    def next(self):
        try:
            record=self.__cursor.fetchone() 
            if record == None:
                raise StopIteration
            return record    
        except Exception, e:
            self.__error=e.message
            self.close()
            raise StopIteration

    def close(self):
        try:
            self.__cursor.close()
        except Exception, e:
            print "EXCEPTION(CLOSE): "+e.message

    def __del__(self):
        print self.__class__.__name__ + " class destroyed"
        self.close()

    def selectsqltext(self, columns, tablename, condition=''):
        if condition:
            condition='WHERE '+condition
        return "SELECT %s FROM %s %s"  % ( columns, tablename, condition)
        
    def insertsqltext(self, columns, tablename, values):
        return "INSERT INTO %s (%s) VALUES (%s)" % (tablename, columns, self.__values)
    
    def _getcolumns(self):
        columns='a.attname as "Column"' \
                ', pg_catalog.format_type(a.atttypid, a.atttypmod) as "Datatype"'
        tablename='pg_catalog.pg_attribute a'
        condition=" a.attnum > 0"  \
                  "  AND NOT a.attisdropped" \
                  "  AND a.attrelid = (" \
                  "      SELECT c.oid" \
                  "      FROM pg_catalog.pg_class c" \
                  "      LEFT JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace" \
                  "            WHERE c.relname = '%s'" \
                  "                AND pg_catalog.pg_table_is_visible(c.oid))" \
                  % (self.__relation.gettablename().lower())
        self._execute(self.selectsqltext(columns, tablename, condition))
        #return (self.selectsqltext(columns, tablename, condition))


# Types of Queries

class RawQuery(Query):
    def __init__(self, relation, sqltext):
        Query.__init__(self, relation)
        Query._execute(self, sqltext)

class SelectQuery(Query):
    def __init__(self, relation, condition=''):
        Query.__init__(self, relation)
        self.__condition=condition
        Query._execute(self, self.selectsqltext(relation.getcolumns(), relation.gettablename(), self.__condition))

class InsertQuery(Query):
    def __init__(self ,relation, values):
        Query.__init__(self, relation)
        self.__values=values
        Query._execute(self, self.insertsqltext(relation.getcolumns(), relation.gettablename(), self.__values))

# Connection

class Connection:
    def __init__(self, database, user, password, host=None, port=None):
        self.__database=database
        self.__user=user
        self.__host=host
        self.__port=port
        self.__connected=False
        try:
            self.__connection=PgSQL.connect(database=database,user=user, \
                                            password=password,host=host, port=port)
            self.__connected=True
        except Exception, e:
            print("Unable to connect to %s@%s. Error is: %s" % (database, user, e.message))
    
    def getconnection(self):
        return self.__connection

    def connected(self):
        return self.__connected
    
    def close(self):
        if self.connected():
            try:
                self.__connection.close()
            except Exception, e:
                print("Error disconnecting from %s@%s. Error is: %s" \
                        % (self.__database, self.__user, e.message))
            finally:
                self.__connected=False
        
# Relations

class Relation:
    def __init__(self, connection, tablename):
        self.__connection=connection
        self.__tablename=tablename
        #self.__selectquery=RawQuery(self, Query(self)._getcolumns())
        q=Query(self);
        q._getcolumns()
        self.__dict__['__columns']={}
        for e in q:
            d={}
            d['type']=e[1]
            d['value']=None                                 
            self.__columns[e[0]]=d
        print self.__columns

    def __getattr__(self, name):
        return self.__dict__['__columns'][name][value]
    
    #def __setattr__(self, name, value):
    #    self.columns[name][value]=value

    def getconnection(self):
        return self.__connection.getconnection()
    
    def gettablename(self):
        return self.__tablename

    def getcolumn(self, columname):
        return self.__columns[columname]['value']
    
    def setcolumn(self, columname, value):
        self.__columns[columname]['value'] =value
    
    def getcolumns(self):
        return ", ".join(self.__columns.keys())

    def insert(self, values):
        q=InsertQuery(self, '')
        return q.geterror()
    
    def select(self, condition=''):
        try:
            self.__selectquery.close()
            del(self.__selectquery)
        except AttributeError:
            pass
        self.__selectquery=SelectQuery(self, condition)
        return self.__selectquery.geterror()

    def __iter__(self):
        return self
    
    def next(self):
        return self.__selectquery.next()

    def modify(self, columns):
        pass

# A fucktory fon Relations

class Factory(object):
    __instance=None
    __connection=None
    __classes={}
    def __new__(cls, *args, **kargs): 
        #try:
            #cls.__connection=kargs['connection']
        #except KeyError, e:
            #try:
                #cls.__connection=args[0]
            #except IndexError, e:
                #return None

        if cls.__instance is None:
            cls.__instance = object.__new__(cls , *args, **kargs)
        return cls.__instance

    def getconnection(cls):
        return cls.__connection

    def get(cls, instance):
        retinstance=None
        if cls.__connection:
            try: 
                instancename=instance.__name__
            except AttributeError, e:
                try: 
                    instancename=instance.__class__.__name__
                except AttributeError, e:
                    retinstance=None
            try:
                retinstance=copy.deepcopy(cls.__classes[instancename])
            except KeyError:
                try:
                    exec("from " + instancename +  " import "+instancename)
                    inst=eval(instancename+"(cls.getconnection())")
                    cls.__classes[instancename]=inst
                    retinstance=copy.deepcopy(inst)
                except ImportError, e:
                    retinstance=None
        return retinstance


