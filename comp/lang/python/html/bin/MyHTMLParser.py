""" My HTML Parser """

from sgmllib import SGMLParser

class MyHTMLParser (SGMLParser):

   def reset(self):
      # extend (called by SGMLParser.__init__)
      SGMLParser.reset(self)
      self.urls=[]

   def unknown_starttag(self, tag, attrs):
      print "".join(["%s=%s" % (key, value) for key, value in attrs])
   
   def unknown_starttag(self, tag, attrs):
      print "Ending Tag: %(tag)s" % locals()

   def start_a(self, attrs):
      href=[value for key, value in attrs if key =='href']
      self.urls.extend(href)
      print "".join(["%s=%s" % (key, value) for key, value in attrs])
   
   def end_a(self, attrs):
      print "Endig a tag"

if __name__ == "__main__":
      print locals()
      print "\n".join(["%s=%s" % (key, value) for key, value in globals().items()])
