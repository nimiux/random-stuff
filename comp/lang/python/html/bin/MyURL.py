""" My URL Library """
      
      #print sys._getframe().f_lineno
      #print sys._getframe().f_code.co_filename

import sys 
import urllib2

class DefaultErrorHandler(urllib2.HTTPDefaultErrorHandler):
   def http_error_default(self, request, fp, code, msg, headers):
      result = urllib2.HTTPError(request.get_full_url(), code, msg, headers, fp)
      result.status = code
      return result

class SmartRedirectHandler(urllib2.HTTPRedirectHandler):
   def http_error_301(self, request, fp, code, msg, headers):
      result = urllib2.HTTPRedirectHandler.http_error_301(self, request, fp, code, msg, headers)
      result.status = code
      return result
   def http_error_302(self, request, fp, code, msg, headers):
      result = urllib2.HTTPRedirectHandler.http_error_302(self, request, fp, code, msg, headers)
      result.status = code
      return result

class MyURL:
   """MyURL/1.0 """
   userheaders = { 'useragent':'User-Agent',
		   'ifmodifiedsince':'If-Modified-Since', 
		   'ifnonematch':'If-None-Match', 
		   'acceptencoding':'Accept-Encoding', 
		   'contentencoding':'Content-Encoding', 
	         }
   serverheaders = { 'date':'date',  
		     'server':'server',
		     'contenttype':'content-type',
		     'contentencoding':'Content-Encoding',
		     'lastmodified':'last-modified',
		     'etag':'etag',
		     'contentlength':'content-length',
		     'acceptranges':'accept-ranges',
		     'connection':'connection',
		   }

   def __init__(self,url):
      """ Init instance, creates request """
      self.opener=urllib2.build_opener(DefaultErrorHandler(), SmartRedirectHandler)
      self.request=urllib2.Request(url)

   def __open(self, request):
      self.stream=self.opener.open(request)
      serverheaders=self.stream.headers.dict

   def __read(self):
      return self.stream.read()
   
   def __close(self):
      self.opener.close()

   def printdict(self, dict):
      print "\n".join(["%s=%s" % (key, value) for key,value in dict.items()])
      

   def printresheaders(self):
      print 'Response Headers'
      self.printdict(self.stream.headers.dict)
      


#def openAnything(source, etag=None, lastmodified=None, agent=USER_AGENT):
   ## non-HTTP code omitted for brevity
   #if urlparse.urlparse(source)[0] == 'http':
      ## open URL with urllib2
      #request = urllib2.Request(source)
      #request.add_header('User-Agent', agent)
      #if etag:
	 #request.add_header('If-None-Match', etag)
      #if lastmodified:
	 #request.add_header('If-Modified-Since', lastmodified)
      #request.add_header('Accept-encoding', 'gzip')
      #opener = urllib2.build_opener(SmartRedirectHandler(), DefaultErrorHandler())
      #return opener.open(request)

#def fetch(source, etag=None, last_modified=None, agent=USER_AGENT):
#'''Fetch data and metadata from a URL, file, stream, or string'''
#result = {}
#f = openAnything(source, etag, last_modified, agent)
#result['data'] = f.read()
#if hasattr(f, 'headers'):
## save ETag, if the server sent one
   #result['etag'] = f.headers.get('ETag')
## save Last-Modified header, if the server sent one
   #result['lastmodified'] = f.headers.get('Last-Modified')
   #if f.headers.get('content-encoding', '') == 'gzip':
## data came back gzip-compressed, decompress it
      #result['data'] = gzip.GzipFile(fileobj=StringIO(result['data']])).read()
      #if hasattr(f, 'url'):
	 #result['url'] = f.url
	 #result['status'] = 200
	 #if hasattr(f, 'status'):
	    #result['status'] = f.status
	    #f.close()
	    #return result


   # CLONE ZONE
   def getcontentencoding(self):
      """ Get me in server """
      try:
	 method=sys._getframe().f_code.co_name[3:]
	 return self.stream.headers.dict[self.serverheaders[method]]
      except KeyError:
	 return ""

   def getlastmodified(self):
      """ Get last modification time in server """
      method=sys._getframe().f_code.co_name[3:]
      return self.stream.headers.dict[self.serverheaders[method]]
   
   def getetag(self):
      """ Get last modification time in server """
      method=sys._getframe().f_code.co_name[3:]
      return self.stream.headers.dict[self.serverheaders[method]]
   
   def getserver(self):
      """ Get server """
      method=sys._getframe().f_code.co_name[3:]
      return self.stream.headers.dict[self.serverheaders[method]]
     
   
   def setuseragent(self, value):
      """ Changes the useragent of the request """
      attr=sys._getframe().f_code.co_name[3:]
      self.request.add_header(self.userheaders[attr], value)
   
   def setifmodifiedsince(self, value):
      """ Changes t of the request """
      attr=sys._getframe().f_code.co_name[3:]
      self.request.add_header(self.userheaders[attr], value)
   
   def setifnonematch(self, value):
      """ Changes t of the request """
      attr=sys._getframe().f_code.co_name[3:]
      self.request.add_header(self.userheaders[attr], value)
   
   def setacceptencoding(self, value):
      """ Changes t of the request """
      attr=sys._getframe().f_code.co_name[3:]
      self.request.add_header(self.userheaders[attr], value)

   # CLONE ZONE
   def open(self):
      """ Returns URLs content """
      try:
	 self.__open(self.request)
	 data=self.__read()
	 self.__close()
      except Exception:
	 print ("Error opening %s" % self.request.get_full_url())
	 data=""
      return "CONTENT: %s. %s" % (self.getcontentencoding(), data)
      



# also, by calling sys._getframe(1), you can get this information
# for the *caller* of the current function.  So you can package
# this functionality up into your own handy functions:
#def whoami():
#    import sys
#        return sys._getframe(1).f_code.co_name
#
#	me  = whoami()

# this uses argument 1, because the call to whoami is now frame 0.
# and similarly:
#def callersname():
#    import sys
#        return sys._getframe(2).f_code.co_name
#
#	him = callersname()
  

#GZIP
#import gzip
#>>> f = opener.open(request)
#>>> f.headers.get('Content-Encoding')
#'gzip'
#>>> data = gzip.GzipFile(fileobj=f).read()

#>>> compresseddata = f.read()
#>>> len(compresseddata)
#6289
#>>> import StringIO
#compressedstream = StringIO.StringIO(compresseddata)
#>>> import gzip
#>>> gzipper = gzip.GzipFile(fileobj=compressedstream)
#>>> data = gzipper.read()
#>>> print data


if __name__ == "__main__":
   print "Testing Lib..."
   try:
      url=sys.argv[1]
   except Exception:
      url='http://www.someplace.org'
   print url
   u=MyURL(url)
   print u.open()
   u.printresheaders()
