options = {"content_type": "text/plain"}

from mod_python import apache

try:
    from mod_python.util import parse_qsl
except ImportError:
	from cgi import parse_qsl

def get_env(req):
	#   grab GET variables
	req.add_common_vars()
	req.content_type    = options['content_type']
	query               = req.subprocess_env['QUERY_STRING']

	#   grab POST variables
	if req.subprocess_env['REQUEST_METHOD'] == 'POST':
		query += '&' + req.read()

	#   break down the urlencoded query string
	query       = parse_qsl(query)
	http_var    = dict(query)
	return http_var

def handler(req):
	http_env = get_env(req)

	#   lookie ma, we got an environment!
	for key, value in http_env.items():
		req.write("%s: %s\n" % (key, value))

	return apache.OK
