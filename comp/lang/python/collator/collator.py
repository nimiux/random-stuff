# -*- coding: utf-8 -*-

from operator import attrgetter, itemgetter
from pyuca import Collator
c = Collator("allkeys.txt")

import pdb

def conv(c):
    d = { u'á': 'a', u'ñ': 'n', }
    try:
        c = d[c]
    except KeyError, why:
        pass 
    return c

def spa(s):
    return ''.join(map(conv, s))

class Word():
    def __init__(self, w):
        self.word = w

print spa(u'álemañ')

words = [ 'zopenco', 'alemán', 'ratín', 'nono', 'ñaka', 'áleman' ]
#print sorted(words)
l = []
for i in  [ u'zopenco', u'alemán', u'ásia', u'ratín', u'nono', u'ñaka', u'áleman' , 'bilbo', 'ASUAN', 'ZAMORA']:
    l.append(Word( i)) 
print [ i.word for i in sorted(l, key=attrgetter('word'))]
print "--------------"
sortkey='word'
pdb.set_trace()
decorated = [(eval("word.%s" % (sortkey)), i, word) for i, word in enumerate(l)]
decorated.sort(key=lambda x: c.sort_key(x[0]))
print [eval("word.%s" % (sortkey)) for (key, i, word) in decorated]


