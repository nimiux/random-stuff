# -*- coding: utf-8 -*-
#autor: edward1738@gmail.com
#version: 1.0
#fecha: 16 febrero de 2012
#blog: http://softcx.blogspot.com


import sys


def mcd( numeros ):
	'''
		Calcula el maximo común divisor
		* Numeros: es una lista de enteros
	'''
	mcd = 1
	numeros.sort() #ordena la lista de menor a mayor
	for i in range( 2, numeros[-1]+1 ): #recorre hasta el numero mayor de la lista (el último)
		if esDivisible(i, numeros):
			mcd = i
	return mcd

def esDivisible( numero, lista):
	'''
		Return True: 
			si el numero no deja resto 
			con todos los números de la lista
	'''
	divisible = True
	for i in lista:
		if not i % numero == 0:
			divisible = False
			break;
	return divisible

def validar( numeros ):
	'''
		True: 
			si todos los elementos
			de la lista son números y mayores a 0
	'''
	correcto = True
	for i in numeros:
		try:
			some = int(i)
			if some <= 0:
				correcto = False
				break;
		except ValueError:	
			correcto = False
			break;
	return correcto

if __name__ == "__main__":
	if len(sys.argv) >= 2:
		print sys.argv[1:]
		if validar(sys.argv[1:]):
			lista = [ int(i) for i in sys.argv[1:] ]
			resultado = mcd ( lista )
			print "MCD = ", resultado
		else:
			print "Solo se permite números enteros"
	
