#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2012 nimiux
#
# by $Author: $
#
# This software is governed by a license. See
# LICENSE.txt for the terms of this license.

# $Id: $

import sys
import argparse

#from pyosis.os.device import LVM
#from pyosis.os.shell import RemoteShell
from pyosis.infra.url import URL
#from pyosis.mark.xmldoc import XmlNode
import pdb

def shell(args):
    print('I am the shell func')
    print("My args are %s" % args)

def main():
    parser = argparse.ArgumentParser(description='Manages servers',
                                    epilog='EPILOG',
                                    #prog='PROGRAM',
                                    #usage='USAGE',
                                    add_help=True,
                                    argument_default='shell',
                                    #parents='',
                                    #prefix_chars='',
                                    #conflict_handler='',
                                    #formatter_class=''
                                    )

    myname = parseurl(sys.argv[0]).filename
    parser.add_argument('host', help='Host to connect to')
    
    parser.set_defaults(subparser_name='shell')
    subparsers = parser.add_subparsers(title='Host Actions',
                                        description='DESCRIPTION',
                                        help='Valid Host Actions',
                                        dest='subparser_name')

    parser_shell = subparsers.add_parser('shell', aliases=['sh'],
                                            help='Shell')
    parser_shell.set_defaults(func=shell)

    parser_tripwire = subparsers.add_parser('tripwire', aliases=['tw'],
                                            help='Tripwire actions')
    parser_tripwire.add_argument('action', choices=('approve', 'report'),
                                help='Tripwire action', nargs='?',
                                default='report')

    p = parser.parse_args()
    host = getattr(p,'host', None) or myname
    print("Host: %s" % host)
    p.func(p)

if __name__ == '__main__':
    import pdb; pdb.set_trace()
    #l=[h for h in hs if hs.name=='server']
    myname = URL(sys.argv[0])
    #myname.islink
    #main()

