from FrogBase import FrogBase
class Frog2(FrogBase):
	def title(self):
		return "Frog 2 Page"
	# We don't override .htTitle, so it defaults to "Frog 2 Page" too.
	def body(self):
		return " ... more info about frogs ..."


