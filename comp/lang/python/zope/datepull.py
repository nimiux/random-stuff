#################################################
datepull python methods
by Phil Aylesworth
phila@purpleduck.com
version 0.1.0, June 15, 2000
#################################################

import string, sys, regex
from time import time, gmtime, localtime, asctime
import DateTime

# set these to change the behavior
prevyr   = 1   # number of years previous to show in year pull down
futyr    = 3   # number of future years to show in year pull down
showtbd  = 'TBD'   # include TBD in pull down and what string to use. Empty string for no TBD entry
tbd_date = '1980/01/01'

# increment futyr so that it works as expected
futyr    = futyr + 1

########################################
#  display a set of pull down menues   #
#   with the current date selected     #
########################################

def datepull(self, tagname):

    months = ('','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec')
    this_yr = localtime(time())[0]
    Day = int(getattr(self, tagname))
    value = DateTime.DateTime(Day, 'Canada/Eastern')
    value_yr = value.year()
    value_mo = value.month()
    value_dd = value.day()
    value_st = "%4d/%02d/%02d" % (value_yr, value_mo, value_dd)
    string = ''

if showtbd:
    if (value_st==tbd_date):
        tbd = 1
        value_mo = 0
        value_yr = 0
        value_dd = 0
    else:
        tbd = 0

    # Day
    string = string + "<select name=\"%s_dd:int\">\n" % tagname
    if showtbd:
        if tbd:
            string = string + "  <option value=\"0\" selected></option>\n"
        else:
            string = string + "  <option value=\"0\"></option>\n"
    for day in range(1,32):
        if (day==value_dd):
            string = string + "  <option value=\"%d\" selected>%d</option>\n" % (day, day)
        else:
            string = string + "  <option value=\"%d\">%d</option>\n" % (day, day)
    string = string + "</select>, \n\n"

    # Month
    string = string + "<select name=\"%s_mo:int\">\n" % tagname
    if showtbd:
        if tbd:
            string = string + "  <option value=\"0\" selected>%s</option>\n" % showtbd
        else:
            string = string + "  <option value=\"0\">%s</option>\n" % showtbd
    for month in range(1,13):
        if (month==value_mo):
            string = string + "  <option value=\"%d\" selected>%s</option>\n" % (month, months[month])
        else:
            string = string + "  <option value=\"%d\">%s</option>\n" %  (month, months[month])
    string = string + "</select> \n\n"

    # Year
    string = string + "<select name=\"%s_yr:int\">\n" % tagname
    if tbd:
        if showtbd:
            string = string + "  <option value=\"0\" selected></option>\n"
        for year in range(this_yr-1, this_yr+4):
            string = string + "  <option value=\"%d\">%d</option>\n" % (year, year)
    else:
        if showtbd:
            string = string + "  <option value=\"0\"></option>\n"
        for year in range(value_yr-1, value_yr+4):
            if (year==value_yr):
                string = string + "  <option value=\"%d\" selected>%d</option>\n" % (year, year)
            else:
                string = string + "  <option value=\"%d\">%d</option>\n" % (year, year)
    string = string + "</select>\n"
    return (string)

########################################
#   Put the date back together again   #
########################################

def datepush(self, REQUEST, tagname):
    yr = tagname + "_yr"
    mo = tagname + "_mo"
    dd = tagname + "_dd"
    y=REQUEST.get(yr)
    m=REQUEST.get(mo)
    d=REQUEST.get(dd)
    if m==0:
        string = tbd_date
    else:
        string = "%4d/%02d/%02d" % (y, m, d)
    return string
