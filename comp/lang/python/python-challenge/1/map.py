# <+PROJECT+>
# -*- coding: utf-8 -*-
#
# Copyright <+YEAR+> <+AUTHOR+>
#
# by $Author: $
#
# This software is governed by a license. See
# LICENSE.txt for the terms of this license.

# $Id: $

# Documentation string
__doc__="""<+DOCUMENTATION+>"""

# Version
__version__='$Revision: $'[11:-2]

#__docformat__ = "restructuredtext en"

msg1= "g fmnc wms bgblr rpylqjyrc gr zw fylb. rfyrq ufyr amknsrcpq ypc dmp. bmgle gr gl zw fylb gq glcddgagclr ylb rfyr'q ufw rfgq rcvr gq qm jmle. sqgle qrpgle.kyicrpylq() gq pcamkkclbcb. lmu ynnjw ml rfc spj."
msg2="map.html"

def trans(a):
    step = ord('z') - ord('a') + 1
    return not a.isalpha() and a or  chr(ord('a')+((ord(a)+2-ord('a')) % step))
    
print ''.join([trans(i) for i in msg1])
print ''.join([trans(i) for i in msg2])
