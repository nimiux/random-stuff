#!/usr/bin/env python
# $Id: autosnap.py,v 1.5 2008/11/19 04:20:25 niallo Exp $

import fnmatch
import ftplib
import getopt
import hashlib
import os
import sys

MIRROR="ftp.openbsd.org"
PATH="/pub/OpenBSD/snapshots/"
DROP_DIR="."

ARCH=os.uname()[4]
# list of files not to download - globs supported
FILE_EXCEPT = ['*.iso*']

def usage():
    print >> sys.stderr, "autosnap.py [-a arch] [-d drop dir] [-m mirror] [-p path]"
    sys.exit(2)

def main():
    ftp = ftplib.FTP(MIRROR)
    ftp.login()
    ftp.cwd("%s/%s" %(PATH, ARCH))
    files = ftp.nlst()
    remove = []
    for p in FILE_EXCEPT:
        remove.extend(fnmatch.filter(files, p))
    for r in remove:
        files.remove(r)
    for f in files:
        print "fetching file %s" %(f)
        ftp.retrbinary("RETR %s" %(f), open("%s/%s" %(DROP_DIR, f), 'wb').write, 4096)
    ftp.quit()
    if 'MD5' in files:
        print "Verifying MD5sums"
        f = open("%s/MD5" %(DROP_DIR), "r")
        md5sums = {}
        for line in f:
            filename = line[line.index('(')+1:line.index(')')]
            if filename in files:
                hash = line.split('=')[1].strip()
                md5sums[filename] = str(hash)
        f.close()
        files.remove('MD5')
        good = 0
        for filename in md5sums.keys():
            f = open("%s/%s" %(DROP_DIR, filename), "r")
            d = f.read()
            f.close()
            m = hashlib.md5()
            m.update(d)
            digest = m.hexdigest()
            if digest == md5sums[filename]:
                print "%s OK" %(filename)
                good += 1
            else:
                print "%s FAIL" %(filename)
        print "%d/%d files verified OK" %(good, len(md5sums))
        if good == len(md5sums):
            sys.exit(0)
        else:
            sys.exit(1)
if __name__ == "__main__":
    try:
        opts, args = getopt.getopt(sys.argv[1:], "a:d:m:p:")
    except getopt.GetoptError:
        usage()
        sys.exit(2)
    for o, a in opts:
        if o == "-a":
            ARCH = a
        if o == "-d":
            DROP_DIR = a
        if o == "-m":
            MIRROR = a
        if o == "-p":
            PATH = a
    main()

