# <+PROJECT+>
# -*- coding: utf-8 -*-
#
# Copyright <+YEAR+> <+AUTHOR+>
#
# by $Author: $
#
# This software is governed by a license. See
# LICENSE.txt for the terms of this license.

# $Id: $

"""<+DOCUMENTATION+>"""

# Version
__version__='$Revision: $'[11:-2]

#__docformat__ = "restructuredtext en"

class String(str):
    def __init__(self, *args, **kargs):
        super(String, self).__init__("Hola")

class List(list):
    def __init__(self, *args, **kargs):
        super(List, self).__init__([1,2])

l=List(1,2)
print l
s=String('9','a')
print s
