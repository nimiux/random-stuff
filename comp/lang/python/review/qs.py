def qs(l):
   return len(l) and qs([i for i in l[1:] if i<l[0]]) + [l[0]] + qs([i for
i in l[1:] if i>l[0]]) or []

import random
l=range(100)
random.shuffle(l)
print l
print qs(l)
