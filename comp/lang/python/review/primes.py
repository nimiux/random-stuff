def posints():
    return range(4,100,2)

primes = (x for x in posints() if all(x % y != 0 for y in xrange(2, int(x ** 0.5) + 1)))
print [i for i in primes]

