import sys
import subprocess

soundfile = 'typewriter-key-1.wav'

def main():
	window_id = sys.argv[1]
	cmd = ['xev', '-id', window_id]
	                                   
	p1 = subprocess.Popen(cmd, stdout=subprocess.PIPE)
	while True:
		line = p1.stdout.readline()
		print line
		if line.find('atom 0x14d') > -1:
			subprocess.Popen(['aplay', soundfile],
			stderr=open('/dev/null', 'w'))


if __name__ == '__main__':
	main()

