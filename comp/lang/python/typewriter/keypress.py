#!/usr/bin/env python
## A tiny, nifty script for playing musical notes on each keypress.
##
##  Copyright Sayan "Riju" Chakrabarti (sayanriju) 2009
##  me[at]sayanriju[dot]co[dot]cc ##
##      Released under WTFPL Version 2
## (DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE)
##  Copy of license text can be found online at ##      http://sam.zoy.org/wtfpl/COPYING
## http://rants.sayanriju.co.cc/script-to-make-tick-tick-sound-on-keypress

from Xlib.display import Display
import subprocess
import time

KEYPRESSFILE = 'typewriter-key-1.wav'
RETURNFILE = 'typewriter-line-break-1.wav'

notes=[440,494,523,587,659,698,784]

ZERO,SHIFT,ALT,CTL=[],[],[],[]
ENTER = [0, 0, 0, 0, 16, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
for i in range(0,32):
    ZERO.append(0)
    if i==6:
        SHIFT.append(4)
    else:
        SHIFT.append(0)
    if i==4:
        CTL.append(32)
    else:
        CTL.append(0)
    if i==8:
        ALT.append(1)
    else:
        ALT.append(0)

ignorelist=[ZERO,ALT,SHIFT,CTL]

def main():
    disp = Display()    # connect to display

    while 1:    #event loop
        keymap = disp.query_keymap()
        if keymap not in ignorelist:
            filename = RETURNFILE if keymap == ENTER else KEYPRESSFILE
            subprocess.Popen(['aplay', filename], stderr=open('/dev/null',
                'w'))
            time.sleep(0.1)


if __name__ == '__main__':
    main()
