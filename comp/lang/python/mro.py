class BaseBase(object):
    def method(self):
        print 'BaseBase'

class Base1(BaseBase):
    pass

class Base2(BaseBase):
    def method(self):
        print 'Base2'

class MyClass(Base1, Base2):
    pass

def L(klass):
    return [k.__name__ for k in klass.mro()]

print L(MyClass)



