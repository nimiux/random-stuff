<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE stylesheet [
<!ENTITY nl  "&#xa;" > 
]> 
<!-- carriage return -->

<!-- <xsl:value-of select="format-number(elem, '$#,##0.00')"/> -->
        
<!-- <xsl:output use-character-maps="xul" />
<xsl:character-map name="xul">
<xsl:output-character character="&amp;" string='&amp;'/>
</xsl:character-map> -->

<xsl:stylesheet version="1.0" 
      xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
      xmlns:date="http://exslt.org/dates">

    <xsl:param name="host"></xsl:param>
    <xsl:param name="action"></xsl:param> 

    <!-- <xsl:include href=""/> -->

    <xsl:output method="text" indent="no" encoding="UTF-8" media-type="text/plain"/>

    <xsl:variable name="nl">&#x9;</xsl:variable>
    <xsl:variable name="tab">&#x9;</xsl:variable>
    <!-- $tab -->
    <xsl:variable name="app">CBS</xsl:variable>

    <xsl:variable name="sshdefaultport">22</xsl:variable>
    <xsl:variable name="sshcommand">ssh</xsl:variable>
    <xsl:variable name="scpcommand">scp</xsl:variable>
        
    <xsl:variable name="now" select="date:date-time"/> 
        

    <xsl:template match="/">
        <xsl:text>&#xa;</xsl:text>
        <xsl:choose>
            <xsl:when test="not($host)">
                <xsl:apply-templates select="child::hosts">
                    <xsl:with-param name="action" select="$action"/>
                </xsl:apply-templates>
            </xsl:when>
            <xsl:otherwise>
                <xsl:apply-templates select="hosts/host[@name = $host]">
                    <xsl:with-param name="action" select="$action"/>
                </xsl:apply-templates>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="hosts">
        <xsl:apply-templates select="host"/>
    </xsl:template>

    <xsl:template match="host">
        <xsl:param name="action"/>
        
        <xsl:variable name="username" select="@sshusername"/>
        <xsl:variable name="dns" select="@dns"/>
        <xsl:variable name="port">
            <xsl:choose>
                <xsl:when test="not(@port)">
                    <xsl:value-of select="$sshdefaultport"/>
                </xsl:when>
                <xsl:otherwise><xsl:value-of select="@port"/></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
                
        <xsl:apply-templates select="knocker" mode="ascending">
            <xsl:with-param name="dns" select="$dns"/>
        </xsl:apply-templates>
        
        <xsl:choose>
            <xsl:when test="not($action)">

                <xsl:apply-templates select="tripwire">
                    <xsl:with-param name="username" select="$username"/>
                    <xsl:with-param name="dns" select="$dns"/>
                    <xsl:with-param name="port" select="$port"/>
                </xsl:apply-templates>
        
                <xsl:apply-templates select="portage">
                    <xsl:with-param name="username" select="$username"/>
                    <xsl:with-param name="dns" select="$dns"/>
                    <xsl:with-param name="port" select="$port"/>
                </xsl:apply-templates>

                <xsl:apply-templates select="backup">
                    <xsl:with-param name="username" select="$username"/>
                    <xsl:with-param name="dns" select="$dns"/>
                    <xsl:with-param name="port" select="$port"/>
                </xsl:apply-templates>
            </xsl:when>
            <xsl:when test="$action = 'shell'">
                <xsl:call-template name="Shell">
                    <xsl:with-param name="username" select="$username"/>
                    <xsl:with-param name="dns" select="$dns"/>
                    <xsl:with-param name="port" select="$port"/>
                    <xsl:with-param name="command" select="''"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <!--<xsl:apply-templates>
                    <xsl:value-of select="$action"/>
                </xsl:apply-templates> -->
            </xsl:otherwise>
        </xsl:choose>
        
        <xsl:apply-templates select="knocker" mode="descending">
            <xsl:with-param name="dns" select="$dns"/>
        </xsl:apply-templates>
        
    </xsl:template>
        
    <xsl:template match="knocker" mode="ascending">
        <xsl:param name="dns"/>

        <xsl:for-each select="knock">
        <xsl:sort select="position()" order="ascending"/>
            <xsl:text>knock </xsl:text>
            <xsl:value-of select="$dns"/> 
            <xsl:text> </xsl:text>
            <xsl:value-of select="."/> 
            <xsl:text>&#xa;</xsl:text>
            <xsl:text>&#xa;</xsl:text>
        </xsl:for-each>
    </xsl:template>
    
    <xsl:template match="knocker" mode="descending">
        <xsl:param name="dns"/>

        <xsl:for-each select="knock">
        <xsl:sort select="position()" order="descending"/>
            <xsl:text>knock </xsl:text>
            <xsl:value-of select="$dns"/> 
            <xsl:text> </xsl:text>
            <xsl:value-of select="."/> 
            <xsl:text>&#xa;</xsl:text>
        </xsl:for-each>
    </xsl:template>
    
    <xsl:template match="tripwire">
        <xsl:param name="username"/>
        <xsl:param name="dns"/>
        <xsl:param name="port"/>

        <xsl:text>Tripwire Actions</xsl:text>
        <xsl:text>&#xa;</xsl:text>
    </xsl:template>
    
    <xsl:template match="portage">
        <xsl:param name="username"/>
        <xsl:param name="dns"/>
        <xsl:param name="port"/>

        <xsl:text>Portage Actions</xsl:text>
        <xsl:text>&#xa;</xsl:text>
    </xsl:template>
    
    <xsl:template match="backup">
        <xsl:param name="username"/>
        <xsl:param name="dns"/>
        <xsl:param name="port"/>
            
        <!-- <xsl:variable name="localdir" select="string-join(($app, $dns, $now), '-')"/> -->
        <xsl:variable name="localdir" select="concat($app, '-', $dns, '-', $now)"/>

        <xsl:text>Backup Actions</xsl:text>
        <xsl:text>&#xa;</xsl:text>
        
        <xsl:call-template name="MakeLocalDir">
            <xsl:with-param name="dir" select="$localdir"/>
        </xsl:call-template>

        <xsl:apply-templates select="directories">
            <xsl:with-param name="username" select="$username"/>
            <xsl:with-param name="dns" select="$dns"/>
            <xsl:with-param name="port" select="$port"/>
            <xsl:with-param name="tmpdir" select="@tmpdir"/>
        </xsl:apply-templates>

        <xsl:apply-templates select="databases">
            <xsl:with-param name="username" select="$username"/>
            <xsl:with-param name="dns" select="$dns"/>
            <xsl:with-param name="port" select="$port"/>
        </xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="directories">
        <xsl:param name="username"/>
        <xsl:param name="dns"/>
        <xsl:param name="port"/>

        <xsl:apply-templates select="preactions">
            <xsl:with-param name="username" select="$username"/>
            <xsl:with-param name="dns" select="$dns"/>
            <xsl:with-param name="port" select="$port"/>
        </xsl:apply-templates>

        <xsl:apply-templates select="directory">
            <xsl:with-param name="username" select="$username"/>
            <xsl:with-param name="dns" select="$dns"/>
            <xsl:with-param name="port" select="$port"/>
            <xsl:with-param name="tmpdir" select="@tmpdir"/>
        </xsl:apply-templates>

        <xsl:apply-templates select="postactions"> 
            <xsl:with-param name="username" select="$username"/>
            <xsl:with-param name="dns" select="$dns"/>
            <xsl:with-param name="port" select="$port"/>
        </xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="preactions | postactions">
        <xsl:param name="username"/>
        <xsl:param name="dns"/>
        <xsl:param name="port"/>
    
    <!--    <xsl:apply-templates select="command"> 
            <xsl:with-param name="username" select="$username"/>
            <xsl:with-param name="dns" select="$dns"/>
            <xsl:with-param name="port" select="$port"/>
        </xsl:apply-templates>  -->
    </xsl:template> 
    
    <xsl:template match="directory">
        <xsl:param name="username"/>
        <xsl:param name="dns"/>
        <xsl:param name="port"/>
        <xsl:param name="tmpdir"/>

        <xsl:call-template name="CreatePack">
            <xsl:with-param name="username" select="$username"/>
            <xsl:with-param name="dns" select="$dns"/>
            <xsl:with-param name="port" select="$port"/>
            <xsl:with-param name="dir" select="$tmpdir"/>
        </xsl:call-template>

        <xsl:text>&#xa;</xsl:text>
    </xsl:template>
    
    <xsl:template match="databases">
        <xsl:param name="username"/>
        <xsl:param name="dns"/>
        <xsl:param name="port"/>

        <xsl:text>Databases to backup</xsl:text> 
        <xsl:apply-templates select="database"> 
            <xsl:with-param name="username" select="$username"/>
            <xsl:with-param name="dns" select="$dns"/>
            <xsl:with-param name="port" select="$port"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="database">
        <xsl:param name="username"/>
        <xsl:param name="dns"/>
        <xsl:param name="port"/>

        <xsl:text>&#xa;</xsl:text>
    </xsl:template>
    
    <xsl:template match="command">
        <xsl:param name="username"/>
        <xsl:param name="dns"/>
        <xsl:param name="port"/>
        
        <xsl:call-template name="Shell">
            <xsl:with-param name="username" select="$username"/>
            <xsl:with-param name="dns" select="$dns"/>
            <xsl:with-param name="port" select="$port"/>
            <xsl:with-param name="command" select="."/>
        </xsl:call-template>
    </xsl:template>

    <!-- Named Templates -->


    <xsl:template name="Shell">
        <xsl:param name="username"/>
        <xsl:param name="dns"/>
        <xsl:param name="port"/>
        <xsl:param name="command"/>

        <xsl:variable name="conn" select="concat($username, '@', $dns)"/>
        <xsl:variable name="ssh" select="concat($sshcommand, ' -p ', $port, ' ', $conn)"/>
        <xsl:value-of select="concat($ssh, ' ', $command)"/> 
        <xsl:text>&#xa;</xsl:text>
    </xsl:template>
    
    <xsl:template name="Copy">
        <xsl:param name="username"/>
        <xsl:param name="dns"/>
        <xsl:param name="port"/>
        <xsl:param name="src"/>
        <xsl:param name="dst"/>

        <xsl:variable name="conn" select="concat($username, '@', $dns)"/>
        <xsl:variable name="scp" select="concat($scpcommand, ' -P ', $port, ' ', $conn)"/>
        <xsl:value-of select="src" />
        <xsl:value-of select="dst" />
    </xsl:template>
        
    <xsl:template name="MakeLocalDir">
        <xsl:param name="dir"/>
        <xsl:text>mkdir </xsl:text><xsl:value-of select="$dir"/>
        <xsl:text>&#xa;</xsl:text>
    </xsl:template>
    
    <xsl:template name="MakeRemoteDir" mode="remote">
        <xsl:param name="username"/>
        <xsl:param name="dns"/>
        <xsl:param name="dir"/>

    </xsl:template>

    <xsl:template name="CreatePack">
        <xsl:param name="username"/>
        <xsl:param name="dns"/>
        <xsl:param name="port"/>
        <xsl:param name="dir"/>
        <xsl:param name="out"/>

        <xsl:call-template name="Shell">
            <xsl:with-param name="username" select="$username"/>
            <xsl:with-param name="dns" select="$dns"/>
            <xsl:with-param name="port" select="$port"/>
            <xsl:with-param name="command">
                <xsl:text></xsl:text>
                <xsl:text>"sudo find </xsl:text>
                <xsl:value-of select="$dir"/>
                <xsl:text> -print | </xsl:text>
                <xsl:text>sudo cpio -o -H crc | gzip - > </xsl:text> 
                <xsl:value-of select="out"/>
                <xsl:text>"</xsl:text>
            </xsl:with-param>
        </xsl:call-template>
    </xsl:template>
    
    
</xsl:stylesheet>
