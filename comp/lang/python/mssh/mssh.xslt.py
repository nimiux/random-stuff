#!/usr/bin/env /usr/bin/python

# This processes a host.xsd file

import sys
from pyvos.mark.xslt import applystylesheet
from pyvos.osal.command import Batch

import libxml2
# Memory debug specific

#libxml2.debugMemory(1)

if __name__ == "__main__":
    #try:
        file="hosts.xml"
        stylesheet="hosts.xsl"
        try:
            host=sys.argv[1]
        except IndexError, e:
            print("TODO: make all hosts")
            sys.exit(1)
        
        try:
            action=sys.argv[2]
        except IndexError, e:
            print("TODO: make a shell")
            action = 'shell'

        host = ("\'%s\'" % (host))
        action = ("\'%s\'" % (action))
        params = dict ( [ [ "host", host ], [ "action", action ] ] )

        s = applystylesheet(file, stylesheet, params)
        c = s.split("\n")
        c = [i for i in c if i != ''] 
        if len(c):
            print "Folloging commans will be executed"
            print "\n".join(c)
            print "Execute?"
            i = sys.stdin.readline()
            if i[0] == "y" or i[0] == "Y":
                b = Batch(c, stop = True)
                br = b.run()
                if br.success():
                    print "Actions completed"
                    print br
                else:
                    print br.getfirsterror()
    
        # Memory debug specific
        if libxml2.debugMemory(1) == 0:
            print "OK"
        else:
            print "Memory leak %d bytes" % (libxml2.debugMemory(1))
            libxml2.dumpMemory()
    #except:
        #print("Use: %s <xmlfile> <xslfile>" % (sys.argv[0]))
        #sys.exit(1)

