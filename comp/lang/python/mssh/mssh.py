#!/usr/bin/env /usr/bin/python

import pdb

import sys
import subprocess

# TODO: XML Functionality integration into PYVOS.

from pyvos.commands.batch import Batch 
from pyvos.commands.command import OSCommandResult, OSCommand
from pyvos.exception import OSCommandError
from pyvos.infra import getArgument
import pyvos.osal.fs
import pyvos.infra
import pyvos.mark.xmldoc
import pyvos.doc.doc

import xml.dom.minidom 

VERBOSE = False
KNOCK = False 
HOSTFILENAME = '/home/oper/hosts.xml'

myname = 'mssh'
    
# TODO in xml file
portagelogdir = "portagelogdir"
defaultbackuptmpdir = "/tmp"
defaultdatabaseengine = "/tmp"

# Commands
sshcommand = "/usr/bin/ssh"
scpcommand = "/usr/bin/scp"
wakecommand = "/usr/bin/wakeonlan"
knockcommand = "/usr/bin/knock"

alllabel = "all"

wakeaction = "wake"
killaction = "kill"
execaction = "exec"
cpaction = "cp"
defaultaction = "shell"

# Export

def deletekey(dict, key):
    try:
        x = dict[key]
        del(dict[key])
    except KeyError, e:
        x = None
    return x

def logcond(s, v=True):
    if v:
        print s

def getAttribute(node, name):
    # TODO: used str to clean Unicode
    return str(node.getAttribute(name))

def getAttributes(node, attr = [], defs = {}):
    d = {} 
    for n in attr:
        a = str(getAttribute(node, n))
        if not a: 
            try:
                a = defs[n]
            except KeyError:
                a = ''
        d[n] = a 
    return d 

def getChildren(node):
    # TODO
    # Use a list and a map to i.tagName
    d = {}
    if node:
        for e in node.childNodes:
            if e.nodeType == e.ELEMENT_NODE:
                d[e.tagName] = e
    return d

def getList(node, tag = None):
    if not tag:
        l = [ { i.tagName: i } for i in node.childNodes 
                            if i.nodeType == i.ELEMENT_NODE ]
    else:
        l = []
        for n in node.getElementsByTagName(tag):
            l.append( { n.tagName: getText(n.childNodes)} )
    return l


def getText(nodelist):
    rc = ""
    for node in nodelist:
        if node.nodeType == node.TEXT_NODE:
            rc += node.data
    return rc

# Helpers
def connectstring(hostnode):
    conn= "%(sshusername)s@%(dns)s" % \
           getAttributes(hostnode, [ 'sshusername', 'dns' ]) 
    return conn

def getsshcommand(hostnode, copy = False):
    if copy:
        command = "%s -P" % (scpcommand)
    else:
        command = "%s -p" % (sshcommand)

    return command + " %(port)s" % \
           getAttributes(hostnode,[ 'port' ], { 'port': '22' })

def rsynccommand(hostnode, srcdir, dstdir):
    connect = connectstring(hostnode)
    d =  getAttributes(hostnode, [ 'port', 'sshusername', 'dns' ], { 'port': '22' })
    d['connect'] = connect 
    d['srcdir'] = srcdir
    d['dstdir'] = dstdir
    c = "rsync -e 'ssh -p %(port)s' -avzt --delete %(connect)s:%(srcdir)s %(dstdir)s" % (d)
    return c 

def remotecommand(hostnode, command = None):
    c = "%s %s" % (getsshcommand(hostnode), connectstring(hostnode))
    if command:
        c = "%s '%s'" % (c, command)
    return c

def getcpcommand (hostnode, args):
    cc = getsshcommand(hostnode, True)
    conn = connectstring(hostnode) + ':'

    l = args.split(" ")
    try:
        f = l[0].upper()
        src = l[1]
        dst = l[2]
        if f == 'H':
            dst = conn + dst
        else:
            if f == 'C':
                src = conn + src 
            else:
                raise IndexError
    except IndexError:
        return None
       
    c = "%s -r %s %s" % \
        (getsshcommand(hostnode, True), src, dst)
    return c

# Handlers

def processHosts(time, hosts, host = alllabel, action = defaultaction, pars = ""): 
    # TODO: ???
    if not host: host = alllabel
    if not action: action = defaultaction

    handled = False
    for h in hosts.getElementsByTagName('host'):
        hostname = getAttribute(h, "name")
        if hostname and (host == alllabel or hostname == host):
            r = processHost(time, h, action, pars)
            logcond(r)
            handled = True

    if not handled:
        if host == alllabel:
            # TODO
            # If no host handled exec action + pars
            logcond("No hosts found in file")
        else:
            logcond("Host %s not found in file" % (host))

def processHost(time, hostnode, action, pars): 
    logcond("Handling host: %s" % (getAttribute(hostnode, "name")), VERBOSE)

    actions = getChildren(hostnode)
    wake = deletekey(actions, 'wake')
    knock = deletekey(actions, 'knocker')
    
    try:
        # Python 2.4. compatible
        try:
            if action == wakeaction:
                r = handlewake(hostnode, wake, pars).run()
            else:
                # TODO DON'T Knock the door until arguments are OK
                logcond("Knocking the door...", KNOCK)
                r = handleknocker(knock)
                if not r:
                    if action != alllabel:
                        try:
                            actions = { action: actions[action] }
                        except KeyError:
                            actions = { action: None }
                    r = handleActions(time, hostnode, actions, pars)
                    #raise Exception()
        except KeyboardInterrupt:
            logcond("Action %s cancelled" % (action))

        except Exception, e:
            # TODO: Use MyException
            # TODO: Check r fot success to remove the msg bellow.
            logcond("Something happened... %s" % (e))

    finally:
        if action != wakeaction:
            logcond("Closing the door...", KNOCK)
            r1 = handleknocker(knock, False)
            # TODO check for error
        return r

def handleActions(time, hostnode, actions, pars):
    m = ""
    hit = False
    for action in actions.items():
        actionname = action[0]
        logcond("Processing action %s ..." % (actionname), VERBOSE)
        c = processtag(time, hostnode, action, pars)
        if c is not None:
            logcond("Executing action %s..." % (actionname), VERBOSE)
            hit = True
            if isinstance(c, OSCommand):
                r = c.run()
                m += r.getinfo()
            elif isinstance(c, basestring):
                m += c 
            else:
                r = c.run()
                m += r.getinfo()
                #m += "Action returned: %s" % (r)
        if not hit:
            if pars:
                actionname += ' ' + pars
            c = handleexec(time, hostnode, action, actionname)
            r = c.run()
            m += r.getinfo()
    return m 

def processtag(time, hostnode, action, pars=None): 
    actionname = action[0]
    actioninfo = action[1]
    try:
        h = "handle%s(time, hostnode, actioninfo, pars)" % (actionname)
        exec('r = ' + h)
    except NameError:
        r = None
    return r

# Mandatory handlers

def handleactions(hostnode, actions, action):
    b = Batch(shell=True)
    try:
        acts = actions[action] 
    except KeyError:
        return b

    l = getList(acts)
    for i in l:
        (t, a) = i.popitem()
        a = getText(a.childNodes)
        if t == 'remote':
            c = remotecommand(hostnode, a)
        else:
            c = a
        b.add(c)
    return b

def handlewake(hostnode, wakenode, pars):
    addr = getAttributes(hostnode, attr = [ 'hwaddr' ])['hwaddr']
    if not addr:
        return "No address for host"

    d = getChildren(wakenode)
    b = handleactions(hostnode, d, 'preactions')

    b.add("%s %s" % (wakecommand, addr))
    
    b.add(handleactions(hostnode, d, 'postactions'))
    return b

def handleknocker(node, ascending = True):
    r = None
    c = None
    if KNOCK and node:
        dns = getAttribute(node.parentNode, 'dns')
        if not dns:
            r = "No dns for host"
        else:
            #pdb.set_trace()
            portlist = [ i['knock'] for i in getList(node, 'knock') ]
            if not ascending:
                portlist.reverse()
    
            command = "%s %s %s" % (knockcommand, dns,  " ".join(portlist))
            rs = Batch(command).run()
    
            if not rs.success:
                r = rs.error 
    return r

def handleshell(time, hostnode, actioninfo, args):
    return OSCommand(remotecommand(hostnode, args), stdin=None, stdout=None, shell=True, createpipe=False)
    #return OSCommand(remotecommand(hostnode, args), stdin=None, stdout=None, shell=True)

def handleexec(time, hostnode, actioninfo, args):
    return handleshell(time, hostnode, actioninfo, args)

def handlecp(time, hostnode, actioninfo, args):
    command = getcpcommand(hostnode, args)
    if not command:
        return "Invalid copy parameters: %s" % (args)
    return Batch(command)

def handlekill(time, hostnode, actioninfo, args):
    d = getChildren(actioninfo)
    b = Batch(shell=True)
    b.add(handleactions(hostnode, d, 'preactions'))
    b.add(remotecommand(hostnode, 'sudo /sbin/poweroff'))
    b.add(handleactions(hostnode, d, 'postactions'))
    return b

def handlereboot(time, hostnode, actioninfo, args):
    d = getChildren(actioninfo)
    b = Batch(shell=True)
    b.add(handleactions(hostnode, d, 'preactions'))
    b.add(remotecommand(hostnode, 'sudo /sbin/shutdown -r now'))
    b.add(handleactions(hostnode, d, 'postactions'))
    return b

def handlebackup(time, hostnode, actioninfo, args):
    b = Batch(stop = True, pipes = False, shell=True)

    d = getAttributes(hostnode, [ 'name' ])
    back = "backup-%s-%s" % (d['name'], time)
    dir = back + '-all'
    #pdb.set_trace()
    try:
        pyvos.osal.fs.fs.makedir(dir)
    except OSCommandError, why:
        b = "%s directory could not be created: %s" % (dir, why)
        return b

    for actionitem in getChildren(actioninfo).items():
        b.add(processtag(time, hostnode, actionitem, back))
    return b

def handlersync(time, hostnode, actioninfo, args):
    b = Batch(stop = True, pipes = False, shell=True)

    l = args.split()
    # Use getoptions
    try:
        srcdir = l[0]
    except:
        return "I need at least remote dir."
    try:
        dstdir = l[1]
    except:
        dstdir = '.'

    #pdb.set_trace()
    i = getChildren(actioninfo)
    b = handleactions(hostnode, i, 'preactions')
    b.add(rsynccommand(hostnode, srcdir, dstdir))
    b.add(handleactions(hostnode, i, 'postactions'))
    return b 

def handledirectories(time, hostnode, actioninfo, pars):
    # TODO Don't create file at host, try using command's stdout
    parent = actioninfo.parentNode

    i = getChildren(actioninfo)

    tmpdir = getAttribute(parent, 'tmpdir')
    if not tmpdir: tmpdir = defaultbackuptmpdir

    b = handleactions(hostnode, i, 'preactions')
    
    packs= '' 
    for d in actioninfo.getElementsByTagName('directory'):
        n = getText(d.childNodes)
        packfile = tmpdir + '/' + pars + n.replace('/', '-') + '.cpio.gz'
        command = "cd / ; sudo find %s -print | sudo cpio -o -H crc | sudo gzip - > %s" \
                    % (n, packfile)
        b.add(remotecommand(hostnode, command))
        b.add(getcpcommand(hostnode, "c %s %s" % (packfile, pars + '-all')))
        b.add(remotecommand(hostnode, "rm %s" % (packfile)))
    
    b.add(handleactions(hostnode, i, 'postactions'))
    return b 

def getbackuppostgresqlbatch(hostnode, database, user, pwd, localdbfile, remotedbfile):
    # TODO Postgresql wrapper
    b = Batch()
    command = ''
    # hostname:port:database:usename:password
    pline = "*:*:%s:%s:%s" % (database, user, pwd)
    command += "echo \"%s\" > .pgpass ; sudo chmod 600 .pgpass" % (pline)
    command += " ; pg_dump -U %s -F c -b --file=%s %s" \
                % (user, remotedbfile, database)
    b.add(remotecommand(hostnode, command))

    b.add(getcpcommand(hostnode, "c %s %s" % (remotedbfile, localdbfile)))

    b.add(remotecommand(hostnode, "rm %s ; rm .pgpass" % (remotedbfile)))
    return b

def handledatabases(time, hostnode, actioninfo, pars):
    # TODO Don't create file at host, try using command's stdout
    parent = actioninfo.parentNode
    
    tmpdir = getAttribute(parent, 'tmpdir')
    if not tmpdir: tmpdir = defaultbackuptmpdir

    i = getChildren(actioninfo)

    b = handleactions(hostnode, i, 'preactions')
    
    engines = { 'postgresql': getbackuppostgresqlbatch }
    localdir = pars + '-all'
    for dn in actioninfo.getElementsByTagName('database'):
        us = getAttribute(dn, 'user')
        # TODO: Use a function to handle null attributes
        pw = getAttribute(dn, 'pass')
        if not pw: pw = ''
        db = getText(dn.childNodes)
        logcond("Backing up database: %s. Connecting using: %s" % (db, us), VERBOSE)
    
        # TODO: Use a function to handle null attributes
        en = getAttribute(dn, 'engine')
        if not en:
            en = defaultdatabaseengine
    
        dbfile = "%s.bak" % (db)
        localdbfile = "%s/%s" % (localdir, dbfile)
        remotedbfile = "%s/%s" % (tmpdir, dbfile)
        try:
            b.add(engines[en](hostnode, db, us, pw, localdbfile, remotedbfile))
        except IndexError:
            # TODO: Handle this in loop
            b.add("echo \"Invalid database engine\"")
    
    b.add(handleactions(hostnode, i, 'postactions'))
    return b 


def handleportage(time, hostnode, actioninfo, pars):

    app = 'portage'
    d = { 'sync': dosync, 'emerge': doemerge, 'update': doupdate }

    i = getChildren(actioninfo)
    b = handleactions(hostnode, i, 'preactions')

    pars = pars.split(' ')
    do = pars[0]
    pars = ' '.join(pars[1:])
    data = { 'hostnode': hostnode, 'params': pars }

    r = pyvos.infra.switch(do, d, \
                           lambda x: "invalid %s option: %s" % (app, do), data)

    if isinstance(r, basestring):
        return r 

    b.add(r) 
    b.add(handleactions(hostnode, i, 'postactions'))
    #pdb.set_trace()
    return b

def handletripwire(time, hostnode, actioninfo, pars):

    app = 'tripwire'
    d = { 'check': docheck, 'lastreport': dolastreport, 'approve': doapprove }

    i = getChildren(actioninfo)
    b=Batch(shell=True)
    b.add(handleactions(hostnode, i, 'preactions'))

    pars = pars.split(' ')
    do = pars[0]
    pars = ' '.join(pars[1:])
    data = { 'hostnode': hostnode, 'params': pars }

    r = pyvos.infra.switch(do, d, \
                           lambda x: "invalid %s option: %s" % (app, do), data)

    if isinstance(r, basestring):
        return r 
    b.add(r) 
    b.add(handleactions(hostnode, i, 'postactions'))
    return b

# portage Dos

def dosync(data):
    hostnode = data['hostnode']
    pars = data['params']
    

    b = Batch(pipes = False)
    b.add(remotecommand(hostnode, "mkdir -p " + portagelogdir))
    c = "sudo nohup emerge --sync >> %s/emergesync 2>> %s/emergesyncerror &" \
                      % (portagelogdir, portagelogdir)
    b.add(remotecommand(hostnode, c))
    return b

def emergecommand(f = '', logdir = portagelogdir):
    return "sudo nohup emerge -%suDN world " \
           ">> %s/emergelog 2>> %s/emergeerror &"  % (f, logdir, logdir)

def doemerge(data):
    hostnode = data['hostnode']
    pars = data['params']

    b = Batch(pipes = False)
    b.add(remotecommand(hostnode, "mkdir -p " + portagelogdir))
    b.add(remotecommand(hostnode, emergecommand('p')))
    return b

def doupdate(data):
    hostnode = data['hostnode']
    pars = data['params']

    b = Batch(pipes = False)
    b.add(remotecommand(hostnode, "mkdir -p " + portagelogdir))
    b.add(remotecommand(hostnode, emergecommand()))
    return b

# Tripwire Dos

def docheck(data):
    hostnode = data['hostnode']
    pars = data['params']

    b = Batch(pipes = False)
    b.add(remotecommand(hostnode, "sudo /usr/sbin/tripwire --check"))
    return b

def getlastreport(hostnode):
    #pdb.set_trace()
    hostname = getAttribute(hostnode, 'name')
    c = remotecommand(hostnode,
                 "sudo ls -1rt /var/lib/tripwire/report/%s*" % (hostname))
    r = Batch(c, shell=True).run()
    if r.success:
        # TODO: Use function for getting last not null list item AND String
        # class
        report = [ i for i in r.getdata() if i ][-1].strip('\n')
        r = OSCommandResult(0, [ report ], [])
    return r

def dolastreport(data):
    hostnode = data['hostnode']
    pars = data['params']

    r = getlastreport(hostnode)
    if r.success:
        report = r.getinfo()
        c = "sudo /usr/sbin/twprint --print-report --twrfile %s" \
                              % (report)
        return Batch(remotecommand(hostnode, c), shell=True)
    else:
        return r.geterror()

def doapprove(data):
    hostnode = data['hostnode']
    pars = data['params']

    r = getlastreport(hostnode)
    #pdb.set_trace()
    if r.success:
        report = r.getdata()[0]
        c = "sudo /usr/sbin/tripwire --update --accept-all --twrfile %s" \
                                   % (report)
        return Batch(remotecommand(hostnode, c), shell=True)
    else:
        return r.getinfo()

# Main

def main(time):
    logcond("Please convert me into a PyvOS entry!")
    com = pyvos.osal.fs.fs.basename(getArgument(0))
    host = getArgument(1)
    action = getArgument(2)
    parpos = 3
    try: 
        cl = com.split('-')
        action = cl[1]
        parpos = 2
    except IndexError:
        if cl[0] != myname:
            # Called with hostname 
            action = host
            host = cl[0] 
            parpos = 2

    pars = getArgument(parpos, all = True)

    logcond("HOST: %s. ACTION: %s. PARS: %s" % \
              (str(host), str(action), str(pars)), VERBOSE)

    from xml.dom.minidom import parse, parseString
    xmldoc = parse(HOSTFILENAME)

    if not xmldoc:
        logcond("Error opening: %s" % (HOSTFILENAME))
    else:
        #pdb.set_trace()
        logcond("Using file: %s" % (HOSTFILENAME), VERBOSE)
        processHosts(time, xmldoc, host, action, pars)
        xmldoc.unlink()

if __name__ == "__main__":
    main(pyvos.infra.now)

