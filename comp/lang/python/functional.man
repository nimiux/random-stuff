Listing 1. "Short-circuit" conditional calls in Python
# Normal statement-based flow control
if <cond1>:   func1()
elif <cond2>: func2()
else:         func3()

# Equivalent "short circuit" expression
(<cond1> and func1()) or (<cond2> and func2()) or (func3())

# Example "short circuit" expression
>>> x = 3
>>> def pr(s): return s
>>> (x==1 and pr('one')) or (x==2 and pr('two')) or (pr('other'))
'other'
>>> x = 2
>>> (x==1 and pr('one')) or (x==2 and pr('two')) or (pr('other'))



Listing 2. Lambda with short-circuiting in Python

>>> pr = lambda s:s
>>> namenum = lambda x: (x==1 and pr("one")) \
....                  or (x==2 and pr("two")) \
....                  or (pr("other"))
>>> namenum(1)
'one'
>>> namenum(2)
'two'
>>> namenum(3)
'other'


Listing 3. Replacing loops

for e in lst:  func(e)      # statement-based loop
map(func,lst)           # map()-based loop

By the way, a similar technique is available for a functional approach to sequential program flow. That is, imperative programming mostly consists of statements that amount to "do this, then do that, then do the other thing." map() lets us do just this:

Listing 4. Map-based action sequence

# let's create an execution utility function
do_it = lambda f: f()

# let f1, f2, f3 (etc) be functions that perform actions

map(do_it, [f1,f2,f3])   # map()-based action sequence

Listing 5. Functional 'while' looping in Python

# statement-based while loop
while <cond>:
    <pre-suite>
    if <break_condition>:
        breakelse:
        <suite>

# FP-style recursive while loop
def while_block():
    <pre-suite>
    if <break_condition>:
        return 1
    else:
        <suite>
    return 0

while_FP = lambda: (<cond> and while_block()) or while_FP()
while_FP()

Our translation of while still requires a while_block() function that may itself contain statements rather than just expressions. But we might be able to apply further eliminations to that function (such as short circuiting the if/else in the template). Also, it is hard for <cond> to be useful with the usual tests, such as while myvar==7, since the loop body (by design) cannot change any variable values (well, globals could be modified in while_block()). One way to add a more useful condition is to let while_block() return a more interesting value, and compare that return for a termination condition. It is worth looking at a concrete example of eliminating statements:
Listing 6. Functional 'echo' loop in Python

# imperative version of "echo()"
def echo_IMP():
    while 1:
        x = raw_input("IMP -- ")
        if x == 'quit':
            breakelseprint x
echo_IMP()

# utility function for "identity with side-effect"
def monadic_print(x):
    print x
    return x

# FP version of "echo()"
echo_FP = lambda: monadic_print(raw_input("FP -- "))=='quit' or echo_FP()
echo_FP()

What we have accomplished is that we have managed to express a little program that involves I/O, looping, and conditional statements as a pure expression with recursion (in fact, as a function object that can be passed elsewhere if desired). We do still utilize the utility function monadic_print(), but this function is completely general, and can be reused in every functional program expression we might create later (it's a one-time cost). Notice that any expression containing monadic_print(x)evaluates to the same thing as if it had simply contained x. FP (particularly Haskell) has the notion of a "monad" for a function that "does nothing, and has a side-effect in the process."

Eliminating side-effects

After all this work in getting rid of perfectly sensible statements and substituting obscure nested expressions for them, a natural question is "Why?!" All of my descriptions of FP are achieved in Python. But the most important characteristic—and the one likely to be concretely useful—is the elimination of side-effects (or at least their containment to special areas like monads). A very large percentage of program errors—and the problem that drives programmers to debuggers—occur because variables obtain unexpected values during the course of program execution. Functional programs bypass this particular issue by simply not assigning values to variables at all.

Let's look at a fairly ordinary bit of imperative code. The goal here is to print out a list of pairs of numbers whose product is more than 25. The numbers that make up the pairs are themselves taken from two other lists. This sort of thing is moderately similar to things that programmers actually do in segments of their programs. An imperative approach to the goal might look like:

Listing 7. Imperative Python code for "print big products"

# Nested loop procedural style for finding big products
xs = (1,2,3,4)
ys = (10,15,3,22)
bigmuls = []
# ...more stuff...
for x in xs:
    for y in ys:
        # ...more stuff...
        if x*y > 25:
            bigmuls.append((x,y))
            # ...more stuff...
# ...more stuff...
print bigmuls

This project is small enough that nothing is likely to go wrong. But perhaps our goal is embedded in code that accomplishes a number of other goals at the same time. The sections commented with "more stuff" are the places where side-effects are likely to lead to bugs. At any of these points, the variables xs, ys, bigmuls, x, y might acquire unexpected values in the hypothetical abbreviated code. Furthermore, after this bit of code is done, all the variables have values that may or may not be expected and wanted by later code. Obviously, encapsulation in functions/instances and care regarding scope can be used to guard against this type of error. And you can always del your variables when you are done with them. But in practice, the types of errors indicated are common.

A functional approach to our goal eliminates these side-effect errors altogether. A possible bit of code is:



Listing 8. Functional approach to our goal

bigmuls = lambda xs,ys: filter(lambda (x,y):x*y > 25, combine(xs,ys))
combine = lambda xs,ys: map(None, xs*len(ys), dupelms(ys,len(xs)))
dupelms = lambda lst,n: reduce(lambda s,t:s+t, map(lambda l,n=n: [l]*n, lst))
print bigmuls((1,2,3,4),(10,15,3,22))

We bind our anonymous (lambda) function objects to names in the example, but that is not strictly necessary. We could instead simply nest the definitions. For readability we do it this way; but also because combine() is a nice utility function to have anyway (produces a list of all pairs of elements from two input lists). dupelms() in turn is mostly just a way of helping out combine(). Even though this functional example is more verbose than the imperative example, once you consider the utility functions for reuse, the new code in bigmuls() itself is probably slightly less than in the imperative version.

The real advantage of this functional example is that absolutely no variables change any values within it. There are no possible unanticipated side-effects on later code (or from earlier code). Obviously, the lack of side-effects, in itself, does not guarantee that the code is correct, but it is nonetheless an advantage. Notice, however, that Python (unlike many functional languages) does not prevent rebinding of the names bigmuls, combine and dupelms. If combine() starts meaning something different later in the program, all bets are off. You could work up a Singleton class to contain this type of immutable bindings (as, say, s.bigmuls and so on); but this column does not have room for that.

One thing distinctly worth noticing is that our particular goal is tailor-made for a new feature of Python 2. Rather than either the imperative or functional examples given, the best (and functional) technique is:

print [(x,y) for x in (1,2,3,4) for y in (10,15,3,22) if x*y > 25] 


 Unfortunately, the very same expression sum2(range(10)) evaluates to two different things at two points in our program, even though this expression itself does not use any mutable variables in its arguments.

The module functional, fortunately, provides a class called Bindings (proposed to Keller by yours truly) that prevents such rebindings (at least accidentally, Python does not try to prevent a determined programmer who wants to break things). While use of Bindings requires a little extra syntax, it makes it difficult for accidents to happen. In his examples within the functional module, Keller names a Bindings instance let (I presume after the let keyword in ML-family languages). For example, we might do:
Listing 2. Python FP session with guarded rebinding

>>> from functional import *
>>> let = Bindings()
>>> let.car = lambda lst: lst[0]
>>> let.car = lambda lst: lst[2]
Traceback (innermost last):
  File "<stdin>", line 1, in ?
  File "d:\tools\functional.py", line 976, in __setattr__
    raise BindingError, "Binding '%s' cannot be modified." % name
functional.BindingError:  Binding 'car' cannot be modified.
>>> car(range(10))
0

Obviously, a real program would have to do something about catching these "BindingError"s, but the fact they are raised avoids a class of problems.

Along with Bindings, functional provides a namespace function to pull off a namespace (really, a dictionary) from a Bindings instance. This comes in handy if you want to compute an expression within a (immutable) namespace defined in a Bindings. The Python function eval() allows evaluation within a namespace. An example should clarify:
Listing 3. Python FP session using immutable namespaces

>>> let = Bindings()      # "Real world" function names
>>> let.r10 = range(10)
>>> let.car = lambda lst: lst[0]
>>> let.cdr = lambda lst: lst[1:]
>>> eval('car(r10)+car(cdr(r10))', namespace(let))
>>> inv = Bindings()      # "Inverted list" function names
>>> inv.r10 = let.r10
>>> inv.car = lambda lst: lst[-1]
>>> inv.cdr = lambda lst: lst[:-1]
>>> eval('car(r10)+car(cdr(r10))', namespace(inv))
17

Back to top
Closures

One very interesting concept in FP is a closure. In fact, closures are sufficiently interesting to many developers that even generally non-functional languages like Perl and Ruby include closures as a feature. Moreover, Python 2.1 currently appears destined to add lexical scoping, which will provide most of the capabilities of closures.

So what is a closure, anyway? Steve Majewski has recently provided a nice characterization of the concept on the Python newsgroup:

That is, a closure is something like FP's Jekyll to OOP's Hyde (or perhaps the roles are the other way around). A closure, like an object instance, is a way of carrying around a bundle of data and functionality, wrapped up together.

Let's step back just a bit to see what problem both objects and closures solve, and also to see how the problem can be solved without either. The result returned by a function is usually determined by the context used in its calculation. The most common -- and perhaps the most obvious -- way of specifying this context is to pass some arguments to the function that tell it what values it should operate on. But sometimes also, there is a natural distinction between "background" and "foreground" arguments -- between what the function is doing this particular time, and the way the function is "configured" for multiple potential calls.

There are a number of ways to handle background, while focussing on foreground. One way is to simply "bite the bullet" and, at every invocation, pass every argument a function needs. This often amounts to passing a number of values (or a structure with multiple slots) up and down a call chain, on the possibility the values will be needed somewhere in the chain. A trivial example might look like:
Listing 4. Python session showing cargo variable

>>> def a(n):
...     add7 = b(n)
...     return add7
...
>>> def b(n):
...     i = 7
...     j = c(i,n)
...     return j
...
>>> def c(i,n):
...     return i+n
...
>>> a(10)     # Pass cargo value for use downstream
17

In the cargo example, within b(), n has no purpose other than being available to pass on to c(). Another option is to use global variables:
Listing 5. Python session showing global variable

>>> N = 10
>>> def addN(i):
...     global N
...     return i+N
...
>>> addN(7)   # Add global N to argument
17
>>> N = 20
>>> addN(6)   # Add global N to argument
26

The global N is simply available whenever you want to call addN(), but there is no need to pass the global background "context" explicitly. A somewhat more Pythonic technique is to "freeze" a variable into a function using a default argument at definition time:
Listing 6. Python session showing frozen variable

>>> N = 10
>>> def addN(i, n=N):
...     return i+n
...
>>> addN(5)   # Add 10
15
>>> N = 20
>>> addN(6)   # Add 10 (current N doesn't matter)
16

Our frozen variable is essentially a closure. Some data is "attached" to the addN() function. For a complete closure, all the data present when addN() was defined would be available at invocation. However, in this example (and many more robust ones), it is simple to make enough available with default arguments. Variables that are never used by addN() thereby make no difference to its calculation.

Let's look next at an OOP approach to a slightly more realistic problem. The time of year has prompted my thoughts about those "interview" style tax programs that collect various bits of data -- not necessarily in a particular order -- then eventually use them all for a calculation. Let's create a simplistic version of this:
Listing 7. Python-style tax calculation class/instance

class TaxCalc:
    deftaxdue(self):return (self.income-self.deduct)*self.rate
taxclass = TaxCalc()
taxclass.income = 50000
taxclass.rate = 0.30
taxclass.deduct = 10000
print"Pythonic OOP taxes due =", taxclass.taxdue()

In our TaxCalc class (or rather, in its instance), we can collect some data -- in whatever order we like -- and once we have all the elements needed, we can call a method of this object to perform a calculation on the bundle of data. Everything stays together within the instance, and further, a different instance can carry a different bundle of data. The possibility of creating multiple instances, differing only their data is something that was not possible in the "global variable" or "frozen variable" approaches. The "cargo" approach can handle this, but for the expanded example, we can see it might become necessary to start passing around numerous values. While we are here, it is interesting to note how a message-passing OOP style might approach this (Smalltalk or Self are similar to this, and so are several OOP xBase variants I have used):
Listing 8. Smalltalk-style (Python) tax calculation

class TaxCalc:
    deftaxdue(self):return (self.income-self.deduct)*self.rate
    def setIncome(self,income):
        self.income = income
        return self
    def setDeduct(self,deduct):
        self.deduct = deduct
        return self
    def setRate(self,rate):
        self.rate = rate
        return self
print"Smalltalk-style taxes due =", \
      TaxCalc().setIncome(50000).setRate(0.30).setDeduct(10000).taxdue()

Returning self with each "setter" allows us to treat the "current" thing as a result of every method application. This will have some interesting similarities to the FP closure approach.

With the Xoltar toolkit, we can create full closures that have our desired property of combining data with a function, and also allowing multiple closure (nee objects) to contain different bundles:
Listing 9. Python Functional-style tax calculations

from functional import *

taxdue        = lambda: (income-deduct)*rate
incomeClosure = lambda income,taxdue: closure(taxdue)
deductClosure = lambda deduct,taxdue: closure(taxdue)
rateClosure   = lambda rate,taxdue: closure(taxdue)

taxFP = taxdue
taxFP = incomeClosure(50000,taxFP)
taxFP = rateClosure(0.30,taxFP)
taxFP = deductClosure(10000,taxFP)
print"Functional taxes due =",taxFP()

print"Lisp-style taxes due =", \
      incomeClosure(50000,
          rateClosure(0.30,
              deductClosure(10000, taxdue)))()

Each closure function we have defined takes any values defined within the function scope, and binds those values into the global scope of the function object. However, what appears as the function's global scope is not necessarily the same as the true module global scope, nor identical to a different closure's "global" scope. The closure simply "carries the data" with it.

In our example, we utilize a few particular functions to put specific bindings within a closure's scope (income, deduct, rate). It would be simple enough to modify the design to put any arbitrary binding into scope. We also -- just for the fun of it -- use two slightly different functional styles in the example. The first successively binds additional values into closure scope; by allowing taxFP to be mutable, these "add to closure" lines can appear in any order. However, if we were to use immutable names like tax_with_Income, we would have to arrange the binding lines in a specific order, and pass the earlier bindings to the next ones. In any case, once everything necessary is bound into closure scope, we can call the "seeded" function.

The second style looks a bit more like Lisp, to my eyes (the parentheses mostly). Beyond the aesthetic, two interesting things happen in the second style. The first is that name binding is avoided altogether. This second style is a single expression, with no statements used (see Part 1 for a discussion of why this matters).

The other interesting thing about the "Lisp-style" use of the closures is how much it resembles the "Smalltalk-style" message-passing methods given above. Both essentially accumulate values along the way to calling the taxdue() function/method (both will raise errors in these crude versions if the right data is not available). The "Smalltalk-style" passes an object between each step, while the "Lisp-style" passes a continuation. But deep down, functional and object-oriented programming amount to much the same thing.

Back to top
Tail recursion

In this installment, we have knocked off a bit more of the domain of functional programming. What remains is less (and provably simpler?) than before (the title of the section is a minor joke; unfortunately, its concept is not explained herein). Reading the functional module's source is an excellent way to continue exploring a number of FP concepts. The module is very well commented, and provides examples for most of its functions/classes. Not covered in this column are a number of simplifying meta-functions that make the combinations and interaction of other functions simpler to handle. These are definitely worth examining for a Python programmer seeking to continue the exploration of functional paradigms. 


Expression bindings

Never content with partial solutions, one reader -- Richard Davies -- raised the issue of whether we might move bindings all the way into individual expressions. Let's take a quick look at why we might want to do that, and also show a remarkably elegant means of expression provided by a comp.lang.python contributor.

Let's first recall the Bindings class of the functional module. Using the attributes of that class, we were able to assure that a particular name means only one thing within a given block scope:
Listing 1: Python FP session with guarded rebinding

>>> from functional import *
>>> let = Bindings()
>>> let.car = lambda lst: lst[0]
>>> let.car = lambda lst: lst[2]
Traceback (innermost last):
  File "<stdin>", line 1, in ?
    File "d:\tools\functional.py", line 976, in __setattr__
       
       raise BindingError, "Binding '%s' cannot be modified." % name
       functional.BindingError:  Binding 'car' cannot be modified.
       >>> let.car(range(10))
       0

       The Bindings class does what we want within a module or function def scope, but there is no way to make it work within a single expression. In ML-family languages, however, it is natural to create bindings within a single expression:
       Listing 2: Haskell expression-level name bindings

       -- car (x:xs) = x  -- *could* create module-level binding
       list_of_list = [[1,2,3],[4,5,6],[7,8,9]]

       -- 'where' clause for expression-level binding
       firsts1 = [car x | x <- list_of_list] where car (x:xs) = x

       -- 'let' clause for expression-level binding
       firsts2 = let car (x:xs) = x in [car x | x <- list_of_list]

       -- more idiomatic higher-order 'map' technique
       firsts3 = map car list_of_list where car (x:xs) = x

       -- Result: firsts1 == firsts2 == firsts3 == [1,4,7]

       Greg Ewing observed that it is possible to accomplish the same effect using Python's list comprehensions; we can even do it in a way that is nearly as clean as Haskell's syntax:
       Listing 3: Python 2.0+ expression-level name bindings

       >>> list_of_list = [[1,2,3],[4,5,6],[7,8,9]]
       >>> [car_x for x in list_of_list for car_x in
        (x[0],)]
        [1, 4, 7]

        This trick of putting an expression inside a single-item tuple in a list comprehension does not provide any way of using expression-level bindings with higher-order functions. To use the higher-order functions, we still need to use block-level bindings, as with:
        Listing 4: Python block-level bindings with 'map()'

        >>> list_of_list = [[1,2,3],[4,5,6],[7,8,9]]
        >>> let = Bindings()
        >>> let.car = lambda l: l[0]
        >>> map(let.car,list_of_list)
        [1, 4, 7]

        Not bad, but if we want to use map(), the scope of the binding remains a little broader than we might want. Nonetheless, it is possible to coax list comprehensions into doing our name bindings for us, even in cases where a list is not what we finally want:
        Listing 5: "Stepping down" from Python list comprehension

        # Compare Haskell expression:
        # result = func car_car
        #          where
        #              car (x:xs) = x
        #              car_car = car (car list_of_list)
        #              func x = x + x^2
        >>> [func for x in list_of_list
        ...       
        for car in (x[0],)
        ...       
        for func in (car+car**2,)][0]
        2

        We have performed an arithmetic calculation on the first element of the first element of list_of_list while also naming the arithmetic calculation (but only in expression scope). As an "optimization" we might not bother to create a list longer than one element to start with, since we choose only the first element with the ending index 0:
        Listing 6: Efficient stepping down from list comprehension

        >>> [func for x in list_of_list[:1]
        ...       for car in (x[0],)
        ...       for func in (car+car**2,)][0]
        2

        Back to top
        Higher-order functions: currying

        Three of the most general higher-order functions are built into Python: map(), reduce(), and filter(). What these functions do -- and the reason we call them "higher-order" -- is take other functions as (some of) their arguments. Other higher-order functions, but not these built-ins, return function objects.

        Python has always given users the ability to construct their own higher-order functions by virtue of the first-class status of function objects. A trivial case might look like this:
        Listing 7: Trivial Python function factory

        >>> def foo_factory():
        ...    
        def foo():
        ...        
        print 
        "Foo function from factory"
        ...    
        return foo
        ...
        >>> f = foo_factory()
        >>> f()
        Foo function from factory

        The Xoltar Toolkit, which I discussed in Part 2 of this series, comes with a nice collection of higher-order functions. Most of the functions that Xoltar's functional module provides are ones developed in various traditionally functional languages, and whose usefulness have been proven over many years.

        Possibly the most famous and most important higher-order function is curry(). curry() is named after the logician Haskell Curry, whose first name is also used to name the above-mentioned programming language. The underlying insight of "currying" is that it is possible to treat (almost) every function as a partial function of just one argument. All that is necessary for currying to work is to allow the return value of functions to themselves be functions, but with the returned functions "narrowed" or "closer to completion." This works quite similarly to the closures I wrote about in Part 2 -- each successive call to a curried return function "fills in" more of the data involved in a final computation (data attached to a procedure).

        Let's illustrate currying first with a very simple example in Haskell, then with the same example repeated in Python using the functional module:
        Listing 8: Currying a Haskell computation

        computation a b c d = (a + b^2+ c^3 + d^4)
        check = 1 + 2^2 + 3^3 + 5^4

        fillOne   = computation 1 
        -- specify "a"
        fillTwo   = fillOne 2     
        -- specify "b"
        fillThree = fillTwo 3     
        -- specify "c"
        answer    = fillThree 5   
        -- specify "d"
        -- Result: check == answer == 657

        Now in Python:
        Listing 9: Currying a Python computation

        >>> from functional import curry
        >>> computation = lambda a,b,c,d: (a + b**2 + c**3 + d**4)
        >>> computation(1,2,3,5)
        657
        >>> fillZero  = curry(computation)
        >>> fillOne   = fillZero(1)  
        # specify "a"
        >>> fillTwo   = fillOne(2)   
        # specify "b"
        >>> fillThree = fillTwo(3)   
        # specify "c"
        >>> answer    = fillThree(5) 
        # specify "d"
        >>> answer
        657

        It is possible to further illustrate the parallel with closures by presenting the same simple tax-calculation program used in Part 2 (this time using curry()):
        Listing 10: Python curried tax calculations

        from functional import *

        taxcalc = lambda income,rate,deduct: (income-(deduct))*rate

        taxCurry = curry(taxcalc)
        taxCurry = taxCurry(50000)
        taxCurry = taxCurry(0.30)
        taxCurry = taxCurry(10000)
        print "Curried taxes due =",taxCurry

        print "Curried expression taxes due =", \
              curry(taxcalc)(50000)(0.30)(10000)

              Unlike with closures, we need to curry the arguments in a specific order (left to right). But note that functional also contains an rcurry() class that will start at the other end (right to left).

              The second print statement in the example at one level is a trivial spelling change from simply calling the normal taxcalc(50000,0.30,10000). In a different level, however, it makes rather clear the concept that every function can be a function of just one argument -- a rather surprising idea to those new to it.

              Back to top
              Miscellaneous higher-order functions

              Beyond the "fundamental" operation of currying, functional provides a grab-bag of interesting higher-order functions. Moreover, it is really not hard to write your own higher-order functions -- either with or without functional. The ones in functional provide some interesting ideas, at the least.

              For the most part, higher-order functions feel like "enhanced" versions of the standard map(), filter(), and reduce(). Often, the pattern in these functions is roughly "take a function or functions and some lists as arguments, then apply the function(s) to list arguments." There are a surprising number of interesting and useful ways to play on this theme. Another pattern is "take a collection of functions and create a function that combines their functionality." Again, numerous variations are possible. Let's look at some of what functional provides.

              The functions sequential() and also() both create a function based on a sequence of component functions. The component functions can then be called with the same argument(s). The main difference between the two is simply that sequential() expects a single list as an argument, while also() takes a list of arguments. In most cases, these are useful for function side effects, but sequential() optionally lets you choose which function provides the combined return value:
              Listing 11: Sequential calls to functions (with same args)

              >>> def a(x):
              ...     print x,
              ...     return "a"
              ...
              >>> def b(x):
              ...     print x*2,
              ...     return "b"
              ...
              >>> def c(x):
              ...     print x*3,
              ...     return "c"
              ...
              >>> r = also(a,b,c)
              >>> r
              <functional.sequential instance at 0xb86ac>
              >>> r(5)
              5 10 15
              'a'
              >>> sequential([a,b,c],main=c)('x')
              x xx xxx
              'c'

              The functions disjoin() and conjoin() are similar to sequential() and also() in terms of creating new functions that apply argument(s) to several component functions. But disjoin() asks whether any component functions return true (given the argument(s)), and conjoin() asks whether all components return true. Logical shortcutting is applied, where possible, so some side effects might not occur with disjoin(). joinfuncs() is similar to also(), but returns a tuple of the components' return values rather than selecting a main one.

              Where the previous functions let you call multiple functions with the same argument(s), any(), all(), and none_of() let you call the same function against a list of arguments. In general structure, these are a bit like the built-in map(), reduce(), filter() functions. But these particular higher-order functions from functional ask Boolean questions about collections of return values. For example:
              Listing 12: Ask about collections of return values

              >>> from functional import *
              >>> isEven = lambda n: (n%2 == 0)
              >>> any([1,3,5,8], isEven)
              1
              >>> any([1,3,5,7], isEven)
              0
              >>> none_of([1,3,5,7], isEven)
              1
              >>> all([2,4,6,8], isEven)
              1
              >>> all([2,4,6,7], isEven)
              0

              A particularly interesting higher-order function for those with a little bit of mathematics background is compose(). The composition of several functions is a "chaining together" of the return value of one function to the input of the next function. The programmer who composes several functions is responsible for making sure the outputs and inputs match up -- but then, that is true any time a programmer uses a return value. A simple example makes it clear:

              Listing 13: Creating compositional functions

              >>> def minus7(n): return n-7
              ...
              >>> def times3(n): return n*3
              ...
              >>> minus7(10)
              3
              >>> minustimes = compose(times3,minus7)
              >>> minustimes(10)
              9
              >>> times3(minus7(10))
              9
              >>> timesminus = compose(minus7,times3)
              >>> timesminus(10)
              23
              >>> minus7(times3(10))
              23

              Back to top
              Until next time

              I hope this latest look at higher-order functions will arouse readers' interest in a certain style of thinking. By all means, play with it. Try to create some of your own higher-order functions; some might well prove useful and powerful. Let me know how it goes; perhaps a later installment of this ad hoc series will discuss the novel and fascinating ideas that readers continue to provide.
