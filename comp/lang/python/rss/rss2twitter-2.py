#!/usr/bin/env python

# python rss reader -> twitter post

from unicodedata import normalize
import feedparser, pickle, os, sys, twitter, urllib, datetime
import pdb

CONSUMER_KEY = "KhmljGDk50sBYow15MViLg"
CONSUMER_SECRET = "jN2XMcIwNopec5iUpGxTEb4Qz3JGOD0sjTzmziMyY"
ACCESS_TOKEN = "401494813-tayEyZ8IV0AknjvJ8GvJD5uqZPCIxsBoM2yY8cCY"
ACCESS_TOKEN_SECRET = "VUPTwZ692UaRBu8o7x5is1Te4OrhcCjbNZxit4zU154"

def now():
    return datetime.datetime.now()

class RSS2Twitter:
    def __init__(self, filename, url):
        self.filename=filename
        self.url=url
        a = self.twApi=twitter.Api(consumer_key=CONSUMER_KEY,
                               consumer_secret=CONSUMER_SECRET,
                               access_token_key=ACCESS_TOKEN,
                               access_token_secret=ACCESS_TOKEN_SECRET)

        if os.path.exists(self.filename):
            self.itemsDB = pickle.load(file(self.filename, 'r+b'))
        else:
            self.itemsDB = {} 

    def getLatestFeedItems(self, items = 5):
        feed=feedparser.parse(self.url);
        it=feed["items"]
        it_ret=it[0:items]
        return it_ret

    def twitIt(self, items):
        print "BEGIN TWEETING (%s)" % now()
        oldItems=pItems=0
        for it in items:
            if self.itemPublished(it) == None:
                print "--------------\n"
                txt=it["title"] +" "+self.shortenurl(it["link"])
                print normalize('NFKD', txt).encode('ascii','ignore')
                status = self.twApi.PostUpdate(txt)
                print "status: ", status.text
                pItems=pItems+1
        print "Total items: ", len(items)
        print "published: ",pItems
        print "old stuff: ",len(items) - pItems
        print "END TWEETING (%s)" % now()

    def itemPublished (self, item):
        if self.itemsDB.has_key(item["link"]) == True:
            return True
        else:
            self.itemsDB[item["link"]]=item["title"]
            pickle.dump(self.itemsDB, file(self.filename, 'w+b'))
            return None

    def shortenurl(self, url):
        try:
            data = urllib.urlencode(dict(url=url, source="RSS2Twit"))
            encodedurl="http://www.tinyurl.com/api-create.php?"+data
            instream=urllib.urlopen(encodedurl)
            ret=instream.read()
            instream.close()
            if len(ret)==0:
                return url
            return ret
        except IOError, e:
            raise "urllib error."

if __name__ == "__main__":
    # run it like python rss2twitter.py oi.dat (oi.dat is the posted item db)
    # update username and passwd with your twitter account data, surrounding
    # them with quotes.
    url="http://nibbler.es/cms/news/aggregator/RSS"
    r2t=RSS2Twitter(sys.argv[1], url)
    its=r2t.getLatestFeedItems()
    r2t.twitIt(its)
