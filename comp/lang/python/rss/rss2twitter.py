#!/usr/bin/env python

# python rss reader -> twitter post

import unicodedata
import feedparser, pickle, os, sys, twitter, urllib, datetime

TWMAXCHARS = 140

CONSUMER_KEY="Y34XH7oP8wITAYqoNWzTBg"
CONSUMER_SECRET="WYPQS1QtjCL1G7u8CXKwNtZrahULxwaO0xxw2Dj5WI"
ACCESS_TOKEN="401409769-egU6SeRVJF4Hh6Hqw6sb2SdJc88f7ueds1LH7FMW"
ACCESS_TOKEN_SECRET="Ej1IXxWvRxYjK5zS27gsHpN0j20TkFKkNrSOKwX0"

def normalize(self, form='NFKD', encoding='ascii', errors='ignore'):
    return unicodedata.normalize(form, self).encode(encoding, errors)

def now():
    return datetime.datetime.now()

class RSS2Twitter:
    def __init__(self, filename, url):
        self.filename=filename
        self.url=url
        a = self.twApi=twitter.Api(consumer_key=CONSUMER_KEY,
                               consumer_secret=CONSUMER_SECRET,
                               access_token_key=ACCESS_TOKEN,
                               access_token_secret=ACCESS_TOKEN_SECRET)

        if os.path.exists(self.filename):
            self.itemsDB = pickle.load(file(self.filename, 'r+b'))
        else:
            self.itemsDB = {} 

    def getLatestFeedItems(self, items=5):
        feed=feedparser.parse(self.url);
        it=feed["items"]
        it_ret=it[0:items]
        return it_ret

    def twitIt(self, items):
        published=False
        print("BEGIN TWEETING (%s)" % now())
        oldItems=pItems=0
        for it in items:
            if not self.itemPublished(it):
                print("--------------\n")
                #txt=it["title"] +" "+self.shortenurl(it["link"])
                link = it["link"].replace('localhost:8080','nibbler.es')
                slink = self.shortenurl(link)
                maxlen = TWMAXCHARS - len(slink)
                txt=it["title"]
		if len(txt) > maxlen -1:
                    txt=txt[:maxlen-5] +"... "
                txt = txt + " " + slink
                print(normalize(txt))
		import pdb
		pdb.set_trace()
                try:
                        status = self.twApi.PostUpdate(txt)
                        print(normalize("status: " + status.text))
                        pItems=pItems+1
                        self.itemsDB[it["link"]]=it["title"]
                        published=True
                except Exception as why:
                        print(why)
                print("--------------\n")
        if published:
            pickle.dump(self.itemsDB, file(self.filename, 'w+b'))
        print("Total items: %s" % len(items))
        print("published: %s" % pItems)
        print("old stuff: %s " % (len(items) - pItems))
        print("END TWEETING (%s)" % now())

    def itemPublished(self, item):
        return self.itemsDB.has_key(item["link"])

    def shortenurl(self, url):
        try:
            data = urllib.urlencode(dict(url=url, source="RSS2Twit"))
            encodedurl="http://www.tinyurl.com/api-create.php?"+data
            instream=urllib.urlopen(encodedurl)
            ret=instream.read()
            instream.close()
            if len(ret)==0:
                return url
            return ret
        except IOError as e:
            raise "urllib error."

if __name__ == "__main__":
    # run it like python rss2twitter.py oi.dat (oi.dat is the posted item db)
    # update username and passwd with your twitter account data, surrounding
    # them with quotes.
    url="http://localhost:8080/cms/news/aggregator/RSS"
    r2t=RSS2Twitter(sys.argv[1], url)
    its=r2t.getLatestFeedItems(items=5)
    r2t.twitIt(its)
