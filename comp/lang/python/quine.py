Language: Python

Author: Unknown

a='a=%r;print a%%a';print a%a


Author: Frank Stajano (fstajano@orl.co.uk)

l='l=%s;print l%%`l`';print l%`l`


Author: Greg Stein (gstein@microsoft.com)

x='x=%s\012print x%%`x`'
print x%`x`


Author: Joseph DeVincentis <devjoe@wilma.che.utexas.edu>.


q='"'
q1="'"
n='\n'
s='\\'
r1="def f(a,s1,s2,s3,s4):"
r2=" print a+'1='+q+s1+q+n+a+'2='+q+s2+q"
r3=" print a+'3='+q+s3+q+n+a+'4='+q+s4+q"
r4=" print s1+n+s2+n+s3+n+s4"
def f(a,s1,s2,s3,s4):
 print a+'1='+q+s1+q+n+a+'2='+q+s2+q
 print a+'3='+q+s3+q+n+a+'4='+q+s4+q
 print s1+n+s2+n+s3+n+s4
p1="print 'q='+q1+q+q1+n+'q1='+q+q1+q"
p2="print 'n='+q1+s+'n'+q1+n+'s='+q1+s+s+q1"
p3="f('r',r1,r2,r3,r4)"
p4="f('p',p1,p2,p3,p4)"
print 'q='+q1+q+q1+n+'q1='+q+q1+q
print 'n='+q1+s+'n'+q1+n+'s='+q1+s+s+q1
f('r',r1,r2,r3,r4)
f('p',p1,p2,p3,p4)


Author: Terry Reedy (tjreedy@udel.edu)
Note: works as a command line input string

import sys;f=sys.stdout;
x='import sys;f=sys.stdout;x=%s;f.write(x%%`x`)';f.write(x%`x`)


Author: Terry Reedy (tjreedy@udel.edu)
Note: works as an interactive string input.  The double quotes could theoretically be removed.

"x='x=%s;x%%`x`';x%`x`"


Author: Omar Antolin (omar@galois.fciencias.unam.mx)

a = ['print "a =", a', 'for s in a: print s']
print "a =", a
for s in a: print s

Author:   Magnus Lie Hetland
Note: bash command line that runs a Python command

python -c "x='python -c %sx=%s; print x%%(chr(34),repr(x),chr(34))%s'; print x%(chr(34),repr(x),chr(34))"
