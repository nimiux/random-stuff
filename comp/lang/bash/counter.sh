#!/bin/bash

COUNTER=0

function reset() {
	echo "Reiniciando..."
	COUNTER=0
}

function end() {
	echo "Terminando..."
	exit
}

function main() {
	echo "Iniciando..."
	while true ; do
		COUNTER=$((COUNTER + 1))
		echo "Paso ${COUNTER}"
		sleep 3
	done
}

trap reset SIGUSR1
trap end SIGTERM

main

