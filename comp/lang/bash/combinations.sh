#!/bin/sh

local combs=
echo $'\n'"USE=\""{x,-x}" "{y,-y}\"""

for flag in $( eval $(grep '^IUSE=' libdv-1.0.0-r2.ebuild) ; echo ${IUSE} ) ; do
    flag=${flag#+}
    flag=${flag#-}
    combs+='" "{'"${flag},-${flag}"'}'
done
combs="\$'\\n'IUSE=\\\"${combs#\" \"}\\\""
eval "echo ${combs}"

