#!/bin/bash -E
# -E any trap on ERR is inherited by shell functions, command substitutions, and
#    commands executed in a subshell environment. The ERR trap is normally not
#    inherited in such cases.

declare -A __ERRORS
__ERRORS=( \
    [  2]="Filesystem object does not exists" \
    [126]="Invalid path" \
    [127]="Invalid command" \
    [128]="Function %s not found" \
    [129]="Function %s is declared in more than one module" \
    [254]="Invalid number of arguments in function %s" \
    [255]="Not defined error" \
    )
#trap 'error_handler run ${LINENO}' ERR

declare -A __MODULES

function is_function?() {
    local object="${1}"

    # Other way to test:
    #return type "${object}" | grep -q '^function$' 2> ${NULL}
    declare -f "${object}" > ${NULL}
}

# Helpers
function real_name() {
    local path="${1}"

    readlink -f ${path}
}

function search_function() {
    local fn="${1}"
    local funcs=$(grep "^function ${fn}() {$" ${BASHARYMODULESDIR}/*)
    local numberoffuncs=$(echo -n "${funcs}" | grep -c .)
    local module

    case ${numberoffuncs} in
        0)
            error_handler 128 ${fn}
        ;;
        1)
            # TODO Use bash substitution
            module=$(echo -n "$funcs" | cut -d\: -f 1 | xargs basename | cut -d\. -f 1)
            require ${module}
        ;;
        *)
            error_handler 129 ${fn}
        ;;
    esac
}

function run_script() {
    local script="${1}"
    shift
    local arguments="${*}"

    source "${script}" ${arguments}
}

function run_shell() {
    local option1="${1}"

    # TODO Use shopts
    if [[ "${option1}" == "-v" ]] ; then
        echo "Version $VERSION"
    else
        msgko "TODO Launch bashary shell"
        echo
    fi
}

function require() {
    local modulename=${1}

    if [[ ! ${__MODULES[${modulename}]} ]] ; then
        source "${BASHARYMODULESDIR}/${modulename}.sh"
        __MODULES[${modulename}]=1
    fi
}

# Errors
function error_message() {
    local message=${*}

    echo -e "\e[1;31m${message}\e[0m"
}

function error_handler() {
    local status_code="$?"
    local error_code="${1}"
    local number='^[[:digit:]]+$'
    local template=""
    local arguments=""

    [[ -z ${error_code} ]] && error_code=254
    if [[ ${error_code} =~ ${number} ]] ; then
        template=${__ERRORS[${error_code}]}
        shift
    fi
    arguments=${*}
    case ${error_code} in
        not_configured)
            error_message "The package has not been properly configured. Please run make install"
        ;;
        *)
            error_message "Bashary failed!!!!"
            error_message $(printf "${template}" ${arguments})
        ;;
    esac
    exit ${status_code}
}

# Main
BASHARYCONFFILE="/etc/conf.d/bashary"
[[ -f "${BASHARYCONFFILE}" ]] || error_handler not_configured
source ${BASHARYCONFFILE}
[[ -d "${BASHARYMODULESDIR}" ]] || error_handler not_configured

require core

# TODO. Use uppercase for global vars
myname=$(basename "${0}")
myrealname=$(basename $(real_name "${0}"))

if [[ ${myname} == ${myrealname} ]] ; then
    scripttorun="${1}"
    if [[ -f ${scripttorun} ]] ; then
        myname=${scripttorun}
        run_script ${scriptorun} ${*}
    else
        run_shell ${scripttorun}
    fi
else
    # TODO Check if function exits and require needed module before calling it
    search_function ${myname}
    ${myname} ${*}
fi
