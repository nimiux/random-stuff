# Core functions and definitions
# kk.sh
[[ ${DEBUG:-0} != 0 ]] && echo "In module ${1##/*}"

# Definitions
NULL="/dev/null"
ZERO="/dev/zero"
RANDOM="/dev/urandom"

## Dates
DATEFORMAT="%Y%m%d"
TIMEFORMAT="%H%M%S"
DATETIMEFORMAT="${DATEFORMAT}-${TIMEFORMAT}"

require colors

# Function: length
#
# Length of string.
#
# Parameters:
#
# string - The input string.
#
# Returns:
#
# The length of the input string.
#
function length() {
    local string="${1}"

    echo ${#string}
}

function nchar() {
    char="${1}"
    len="${2}"

    head -c ${len} ${ZERO} | tr '\0' ${char}
}

function substr() {
    local string="${1}"
    local start="${2}"
    local end="${3}"

    [[ -n ${start} ]] && start="(${start})"
    [[ -z ${end} ]] && echo "${string:${start}}" \
            || echo "${string:${start}:${end}}"
}

function toupper() {
    local string="${1}"

    echo ${srtring} | tr '[:lower:]' '[:upper:]'
}

function tolower() {
    local string="${1}"

    echo ${srtring} | tr '[:upper:]' '[:lower:]'
}

function ltrim() {
    local string="${1}"
    local char="${2:- }"

    echo -n "${string#"${str%%[![:space:]]*"}"}"}"
}

function rtrim() {
    local string="${1}"
    local char="${2:- }"

    echo -n "${string%"${str##*[![:space:]]"}"}"}"
}

function firstword() {
    local string="${*}"

    echo $(cut -d\  -f 1 ${string})
}

function randword() {
    local special="${1}"
    local len="${2}"
    local deflen=32
    local chars

    [ "$special" == "0" ] && char="[:alnum:]" || char="[:graph:]"
    cat ${RANDOM} | tr -cd "$char" | head -c ${len:-$deflen}
}

# Date
function dateformat() {
    date "+${*}"
}

function today() {
    dateformat ${DATEFORMAT}
}

function now() {
    dateformat ${TIMEFORMAT}
}

function todaynow() {
    dateformat ${DATETIMEFORMAT}
}

function delta_now() {
    local days="${1}"

    echo $(date --date "now ${days} day" +${DATEFORMAT})
}

# Messages
function message() {
    local ref="${1}"
    shift
    local message="${*}"

    echo -en "${!ref}${message}${TextReset}"
}

function messageln() {
    message ${*}
    echo
}

function bold() {
    message Bold ${*}
}

function underline() {
    message Underline ${*}
}

function blink() {
    message Blink ${*}
}

function invert() {
    message Invert ${*}
}

function hide() {
    message Hide ${*}
}

function msg() {
    message Green "${*}"
}

function msgerror() {
    message Red "${*}"
}

function msgwarn() {
    message BMagenta ${*}
}

function msgok() {
    [ "${VERBOSE}" = "y" ] && msgerror "OK: ${*}"
}

function msgko() {
    msgerror "KO: ${*}"
}

function die() {
    # TODO Admit exit code?
    local exitcode="${1}" || 1
    shift

    msgerror "ERROR: ${*}. Exiting..." && exit ${exitcode}
}

# Banners
function banner() {
    local prefix="${1}"
    shift
    local msg=${*}

    is_function? ${prefix} && prefix=$(${prefix}) || prefix=""
    echo "======================="
    echo "${prefix} - ${msg}"
    echo "======================="
}

function banner_todaynow() {
    banner todaynow ${*}
}
