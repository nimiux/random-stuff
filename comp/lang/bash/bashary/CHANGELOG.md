# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## 0.0.0 - 2016-07-26
### Added
- Initial version of the project

