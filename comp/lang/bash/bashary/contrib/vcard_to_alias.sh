#!/bin/sh

# vcard_to_alias {
reset {
    tracer=0
    nick=""
    long=""
    mail=""
}

IFS=$'\n'
local vcardfile="${1--}"
local tracer nick long mail
reset
for line in $(grep -Ev "^$|^#" "${vcardfile}") ; do
    verb=$(echo $line|cut -d ':' -f 1|cut -d ';' -f 1| tr -d '\r')
    case $verb in
        BEGIN)
            tracer=$((tracer + 1))
            ;;
        FN)
            long=$(echo $line|cut -d ':' -f 2| tr -d '\r')
            ;;
        EMAIL)
            mail=$(echo $line|cut -d ':' -f 2| tr -d '\r')
            nick=$(echo $mail|cut -d '@' -f 1| tr -d '\r')
            ;;
        END)
            tracer=$((tracer + 1))
            ;;
    esac
    if [ $tracer -eq 2 ] ; then
        if [ ${#mail} -ne 0 ] ; then
            echo "alias $long <${mail}>"
        fi
        reset
    fi
done
