#!/bin/bash
[[ "$PAM_USER" == "root" ]] && exit 0

SESSION_COUNT="$(w -h "$PAM_USER" | wc -l)"

#if (( SESSION_COUNT == 0 )) && [[ "$PAM_TYPE" == "close_session" ]]; then
if [[ "$PAM_TYPE" == "close_session" ]]; then
  pkill -u "$PAM_USER"
fi
