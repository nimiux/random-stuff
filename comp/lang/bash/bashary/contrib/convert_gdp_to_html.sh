#!/bin/sh

# Translates a gentoo gpd xml file to an html file
FROMLANG="en"
TOLANG="es"
ROOTDIR="/home/dev/gentoo-doc-es/gentoo"
GENTOODOCDIR="${ROOTDIR}/xml/htdocs"

[ "${1}" -a "${2}" ] || echo "Usage: ${0} <type> <filename>" && exit 1
TYPE="${1}"
FILENAME="${2}"

xsltproc -path ${GENTOODOCDIR}/${TYPE}/${TOLANG} ${GENTOODOCDIR}/xsl/guide.xsl \
         ${GENTOODOCDIR}/${TYPE}/${TOLANG}/${FILENAME}.xml > \
         ${GENTOODOCDIR}/${TYPE}/${TOLANG}/${FILENAME}.html
