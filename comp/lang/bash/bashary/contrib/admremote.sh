#!@INTERPRETER@

require path
require net
require system

host="$(file_name)"
command_line="${*}"
command="${1}"

source $HOME/.hosts
REMOTEHOST=$(toupper ${host})
REMOTEPORT="$(toupper ${host})PORT"
CONNSTRING="${!REMOTEHOST}"
PORT="${!REMOTEPORT:-22}"

case "${command}" in
    approve)
        twapprove ${PORT} ${CONNSTRING} ${host}
    ;;
    get)
        shift
        echo cpremote ${CONNSTRING}:/${1} ${2:-.} ${PORT}
        cpremote ${CONNSTRING}:/${1} ${2:-.} ${PORT}
    ;;
    put)
        shift
        cpremote ${1} ${CONNSTRING}:/${2} ${PORT}
    ;;
    vi)
        shift
        viremote ${1}
    ;;
    *)
        remotecommand ${PORT} ${CONNSTRING} "${*}"
esac

# vi:ts=4
