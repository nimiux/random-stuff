HOST=`hostname`

/usr/sbin/twadmin --generate-keys --site-keyfile /etc/tripwire/site.key
/usr/sbin/twadmin --generate-keys --local-keyfile /etc/tripwire/${HOST}-local.key

/usr/sbin/twadmin --create-cfgfile --cfgfile /etc/tripwire/tw.cfg --site-keyfile /etc/tripwire/site.key /etc/tripwire/twcfg.txt
/usr/sbin/twadmin --create-polfile --polfile /etc/tripwire/tw.pol --site-keyfile /etc/tripwire/site.key /etc/tripwire/twpol.txt

/usr/sbin/tripwire --init --cfgfile /etc/tripwire/tw.cfg --polfile /etc/tripwire/tw.pol --site-keyfile /etc/tripwire/site.key --local-keyfile /etc/tripwire/${HOST}-local.key
/usr/sbin/tripwire --check
/usr/sbin/twprint --print-report --twrfile /var/lib/tripwire/report/${HOST}-
/usr/sbin/tripwire --check ;  export LANG=C ; /usr/sbin/tripwire --update --twrfile /var/lib/tripwire/report/${HOST}-

