## Colors
# Reset
TextReset='\e[0m'       # Text Reset

# Regular Colors
Black='\e[0;30m'        # Black
Red='\e[0;31m'          # Red
Green='\e[0;32m'        # Green
Yellow='\e[0;33m'       # Yellow
Blue='\e[0;34m'         # Blue
Magenta='\e[0;35m'      # Magenta
Cyan='\e[0;36m'         # Cyan
White='\e[0;37m'        # White

# Bold
Bold='\e[1m'            # Bold
NoBold='\e[21m'         # Deactivate Bold
BBlack='\e[1;30m'       # Black
BRed='\e[1;31m'         # Red
BGreen='\e[1;32m'       # Green
BYellow='\e[1;33m'      # Yellow
BBlue='\e[1;34m'        # Blue
BMagenta='\e[1;35m'     # Magenta
BCyan='\e[1;36m'        # Cyan
BWhite='\e[1;37m'       # White

# Dim
Dim='\e[2m'             # Dim
NoDim='\e[22m'          # Deactivate Dim
DBlack='\e[2;30m'       # Black
DRed='\e[2;31m'         # Red
DGreen='\e[2;32m'       # Green
DYellow='\e[2;33m'      # Yellow
DBlue='\e[2;34m'        # Blue
DMagenta='\e[2;35m'     # Magenta
DCyan='\e[2;36m'        # Cyan
DWhite='\e[2;37m'       # White

# Colors
DBlack='\e[30m'         # Black
DRed='\e[31m'           # Red
DGreen='\e[32m'         # Green
DYellow='\e[33m'        # Yellow
DBlue='\e[34m'          # Blue
DMagenta='\e[35m'       # Magenta
DCyan='\e[2;36m'        # Cyan
DLightGrey='\e[37m'     # White

# Underline
Underline='\e[4m'       # Underline
UBlack='\e[4;30m'       # Black
URed='\e[4;31m'         # Red
UGreen='\e[4;32m'       # Green
UYellow='\e[4;33m'      # Yellow
UBlue='\e[4;34m'        # Blue
UMagenta='\e[4;35m'     # Magenta
UCyan='\e[4;36m'        # Cyan
UWhite='\e[4;37m'       # White

# Background
On_Black='\e[40m'       # Black
On_Red='\e[41m'         # Red
On_Green='\e[42m'       # Green
On_Yellow='\e[43m'      # Yellow
On_Blue='\e[44m'        # Blue
On_Magenta='\e[45m'      # Magenta
On_Cyan='\e[46m'        # Cyan
On_White='\e[47m'       # White

# High Intensity
IBlack='\e[0;90m'       # Black
IRed='\e[0;91m'         # Red
IGreen='\e[0;92m'       # Green
IYellow='\e[0;93m'      # Yellow
IBlue='\e[0;94m'        # Blue
IMagenta='\e[0;95m'      # Magenta
ICyan='\e[0;96m'        # Cyan
IWhite='\e[0;97m'       # White

# Bold High Intensity
BIBlack='\e[1;90m'      # Black
BIRed='\e[1;91m'        # Red
BIGreen='\e[1;92m'      # Green
BIYellow='\e[1;93m'     # Yellow
BIBlue='\e[1;94m'       # Blue
BIMagenta='\e[1;95m'     # PMagenta
BICyan='\e[1;96m'       # Cyan
BIWhite='\e[1;97m'      # White

# High Intensity backgrounds
On_IBlack='\e[0;100m'   # Black
On_IRed='\e[0;101m'     # Red
On_IGreen='\e[0;102m'   # Green
On_IYellow='\e[0;103m'  # Yellow
On_IBlue='\e[0;104m'    # Blue
On_IMagenta='\e[0;105m'  # Magenta
On_ICyan='\e[0;106m'    # Cyan
On_IWhite='\e[0;107m'   # White

Blink='\e[5m'           # Blink
NoBlink='\e[25m'        # No Blink
Invert='\e[7m'          # Invert
Hide='\e[8m'            # Hide

# Function: morecolors
#
# Shows a gradient of colors
#
function morecolors() {
    for colorcode in {93..88} {124..129}; do
        echo -en "\e[48;5;${colorcode}m \e[0m"
    done
    for colorcode in {0..255} ; do
        printf "\x1b[38;5;${colorcode}m${colorcode}"
    done
}


