#!/bin/sh
# spewtomoz.sh
# You shouldn't install this dumb script for all users because it only
# uses one pipe and one port.  Have it pick a better name for the pipe,
# and set the port from an environment variable.
TEMP="/tmp/spewtomoz"
PORT=8088
rm -f $TEMP
mkfifo --mode=600 $TEMP
# netcat is the fun part of this script.
# -l:            listen for an incoming connection
# -q 1:          wait 1s after EOF and quit
# -s 127.0.0.1   only use the lo interface
# -p $PORT       use $PORT
netcat -l -q 1 -s 127.0.0.1 -p $PORT < $TEMP &> /dev/null &
# send the HTTP headers, followed by a blank line.
echo "HTTP/1.1 200 OK" >> $TEMP
echo -n "Content-type: " >> $TEMP
file -bni $1 2> /dev/null >> $TEMP
echo >> $TEMP
# Get started sending the file...
cat $1 >> $TEMP &
# Wait a second and tell the user's Mozilla, wherever it is, to start
# viewing the file.  This works over the X protocol.
# (the date is to blow the cache and may not be necessary)
sleep 1 && gnome-moz-remote http://localhost:$PORT/`date +%s`
# end spewtomoz.sh

