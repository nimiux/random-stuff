# Functional programming

function dup() {
    :
}

function map() {
    local fun="$@"

    while read elem; do
        echo $elem | $fun
    done
}

function othermap() {
    find $path -type f -print0 | while read -d $'\0' object ; do
       ...$object...
    done
}

# reduce (function, list)
# The reduces function reduces a list (or other structure) to a single value by "folding"
# a binary operator (function) between successive elements of the list.
# bash list {1..3} | reduce lambda a b : 'echo $(($a + $b))'
function reduce() {
    local fun="$@"
    local acc

    read acc
    while read elem; do
        acc="$(printf "%d$acc\n$elem" | $fun)"
    done
    echo $acc
}

# list(var..)
# The list function prints each element to a new line in order.
# bash list '1' '2' '3'
function list() {
    for i in "$@"; do
        echo "$i"
    done
}

# rlist(var..)
# The reverse list function prints each element to a new line in reverse order.
# bash rlist '1' '2' '3'
function rlist() {
    i="$#"
    while [ $i -gt 0 ]; do
          eval "f=\${$i}"
          echo "$f "
          i=$((i-1))
    done
}

#filter(function, list)
#The filter function compares elements in a list using a given function, in ascending order. A new list is returned containing only list elements that satify the function. eg., ``bash list 'hello' 'world' | filter lambda a : 'echo $(strcmp "$a" "hello")' `` result: ``bash hello ``
function filter() {
    local fun="$@"
    resp=
    while read elem; do
        acc="$(printf "${elem}" | $fun)"
        if [ $acc -eq 1 ]; then
            resp+=" $elem"
        fi
    done
    echo $resp
}

# match(function, list)
# The match function compares elements in a list using a given function, in ascending order.
# The first matching element is returned and if no match then nothing is returned.
# bash list 'hello' 'world' | match lambda a : 'echo $(strcmp "$a" "world")'
# result: ``bash world ``
function match() {
    local fun="$@"
    while read elem; do
        acc="$(printf "${elem}" | $fun)"
        if [ $acc -eq 1 ]; then
            echo $elem
            break
        fi
    done
}

# position(function, list)
# The position function compares elements in a list using a given function, in ascending order.
# The position of the first matching element is returned. Initial position is 0. Nothing is returned for no match.
# bash list 'hello' 'world' | position lambda a : 'echo $(strcmp "$a" "hello")'
# `` result: ``bash 0 ``
function position() {
    local fun="$@"
    pos=0
    while read elem; do
        acc="$(printf "${elem}" | $fun)"
        if [ $acc -eq 1 ]; then
            echo $pos
            break
        fi
        pos=$((pos+1))
    done
}

# lambda (var.., function)
# The lambda function represents an anonymous function.
# The lambda function is used as a predicate for other functions.
# bash lambda a b : 'echo $(($a - $b))' ``
#alias -g 'λ'="lambda "
function λ() {
    local fun
    local y
    local i
    lam() {
        unset last

        for last; do
            shift
            #if [[ $last = "." || $last = ":" || $last = "->" || $last = "→" ]]; then
            if [[ $last = ":" ]]; then
                echo "$@"
                return
            else
                echo "read $last;"
            fi
        done
    }
    y="stdin"
    for i in "$@"; do
        # if [[ $i = "." || $i = ":" || $i = "->" || $i = "→" ]]; then
        if [[ $i = ":" ]]; then
            y="args"
        fi
    done
    if [[ "$y" = "stdin" ]]; then
        read fun
        eval $(lam "$@ : $fun")
    else
        eval $(lam "$@")
    fi
    unset y
    unset i
    unset fun
}

#partial(result_function, function, args...)
#Partially apply `function` to `args`, store the resulting function in `result_function`. ``bash add() { expr $1 + $2 } partial adder3 add 3 adder3 6 `` result: ``bash 9 ``
function partial() {
    local exportfun=$1
    shift
    local fun=$1
    shift
    local params=$*
    eval "$exportfun() {
        more_params=\$*;
        $fun $params \$more_params;
    }"
}

#compose(result_function, outer_function, inner_function)
#Compose functions. Remember: `(outer o inner)(x) = outer(inner(x))` ``bash add() { expr $1 + $2 } square() { expr $1 \* $1 } compose square_of_sum square add square_of_sum 2 3 `` rersult: ``bash 25 `` `

function compose() {
    exportfun=$1
    shift
    f1=$1
    shift
    f2=$1
    shift
    eval "$exportfun() {
              $f1 \$($f2 \$*);
          }"
}

# Functional
#function print() {
#    echo -n "$@"
#}
#
#function cons() {
#    echo $@
#    command \cat
#}
#
#function drop() {
#    command \tail -n +$(( ${1} + 1 ))
#}
#
#function take() {
#    command \head -n -${1}
#}
#
#function tail() {
#    drop 1
#}
#
#function head() {
#    take 1
#}
#
#function last() {
#    \tail -n 1
#}
#
#function list() {
#    for i in "$@"; do
#        echo "$i"
#    done
#}
#
#function unlist() {
#    echo "$@"
#}
#
#function map() {
#    while read x; do
#        echo $x | $@
#    done
#}
#
#function foldl() {
#    local f="$@"
#    read acc
#    while read elem; do
#        acc="$(echo "$acc\n$elem" | $f)"
#    done
#    echo "$acc"
#}
#
#function scanl() {
#    local f="$@"
#    read acc
#    while read elem; do
#        acc="$(echo "$acc\n$elem" | $f)"
#        echo "$acc"
#    done
#}
#
#function lambda() {
#    function lam() {
#        unset last
#        unset ars
#        for last; do
#            shift
#            if [[ $last = "." || $last = ":" || $last = "->" || $last = "→" ]]; then
#                echo "$@"
#                return
#            else
#                echo "read $last;"
#            fi
#        done
#    }
#    local y="stdin"
#    for i in "$@"; do
#        if [[ $i = "." || $i = ":" || $i = "->" || $i = "→" ]]; then
#            y="args"
#        fi
#    done
#    if [[ "$y" = "stdin" ]]; then
#        read fun
#        eval $(lam "$@ : $fun")
#    else
#        eval $(lam "$@")
#    fi
#    unset y
#    unset i
#    unset fun
#}

#function append() {
#    local lines $@
#    while read xs; do
#        echo $xs
#    done
#}
#
#function sum() {
#    foldl λ a b . 'echo $(($a + $b))'
#}
#
#function product() {
#    foldl λ a b . 'echo $(($a * $b))'
#}
#
#function factorial() {
#    seq 1 $1 | product
#}
