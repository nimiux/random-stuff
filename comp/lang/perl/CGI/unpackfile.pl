#!/usr/bin/env perl

use strict;
use warnings;
use utf8;

use CGI;
use CGI::Carp qw ( fatalsToBrowser );
use File::Basename;
use Mail::Mailer qw(sendmail);
use Archive::Extract;
use Unicode::Normalize;

my $on = 1;
$CGI::POST_MAX = 1024 * 5000;
my $upload_dir = '/tmp/uploads';

my %errors = (
    INVALID_FILENAME => "Invalid filename provided",
);

sub decompose {
    my $decomposed = NFKD( shift );
    $decomposed =~ s/\p{NonspacingMark}//g;
    return $decomposed;
}

sub mydie {
    my $errorcode = shift;
    my $errormsg = $errors{$errorcode};

    die $errormsg if $errormsg;
    die "No error message, error code is: " . $errorcode;
}

sub sendmail {
    #my $headers = shift;
    #my $body =  shift;
    my ($headers, $body) = @_;

    my $mailer = Mail::Mailer->new;
    $mailer->open($headers);
    print $mailer $body;
    $mailer->close or die "couldn't send whole message: $!\n";
}

sub sendmyemail {
    my %mailheaders = (
        To      => 'somebody@someplace.org',
        From    => 'thisbody@thisplace.org',
        Subject => "File uploaded this place",
    );
    my $body = "File: " . shift;
    sendmail(\%mailheaders, $body);
}

sub sanename {
    my $safe_filename_characters = "a-zA-Z0-9_.-";
    my $filename = decompose(shift);
    $filename or mydie('INVALID_FILENAME');
    my ( $name, $path, $extension ) = fileparse ( $filename, '..*' );
    $filename = $name . $extension;
    $filename =~ tr/ /_/;
    $filename =~ s/[^$safe_filename_characters]//g;
    if ( $filename =~ /^([$safe_filename_characters]+)$/ ) {
        $filename = $1;
    } else {
        die "Filename contains invalid characters";
    }
    return $filename;
}

sub get_upload_destination {
    my $filename = sanename(shift);
    my $upload_dir = shift;

    return "$upload_dir/$filename";
}

sub uploadfile {
    my $query = shift;
    my $filename = $query->param("program");
    print $filename;
    die;
    my $upload_dir = shift;
    my $upload_destination = get_upload_destination($filename, $upload_dir);
    my $upload_filehandle = $query->upload("file");

    open ( UPLOADFILE, ">$upload_destination" ) or die "$!";
    binmode UPLOADFILE;
    while ( <$upload_filehandle> ) {
        print UPLOADFILE;
    }
    close UPLOADFILE;
    return  $upload_destination;
}

sub unpackfile {
    my $filepath = shift;
    my $ae = Archive::Extract->new( archive => $filepath );
    return $ae if $ae and $ae->extract( to => shift );
    return 0;
}

sub success {
    print "Ok!";
}

sub failure {
    print <<END_HTML;
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "DTD/xhtml1-strict.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Thanks!</title>
    <style type="text/css">
    img {border: none;}
    </style>
    </head>
    <body>
    <p>Thanks for uploading your stuff</p>
    </body>
    </html>
END_HTML
}

sub main() {
    my $query = new CGI;
    my $code = $query->param("code");

    my $filedest = uploadfile ($query, $upload_dir);
    my $uresult = unpackfile ($filedest, $upload_dir);
    $uresult and

    print $query->header();
}

$on and main() or print "Not implemented";
