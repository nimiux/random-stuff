#!perl/bin/perl

# Guardamos la l�nea del registro en una variable local.

    $LogLine = "152.163.195.39, -, 6/13/97, 21:37:00, W3SVC"
             ", OWSLEY, 207.77.84.202, 328, 60, 29, 304, 0"
             ", GET, /wml/homepage.gif, -";

# Extraemos los componentes utilizando la funci�n split()

    ($ClientIP, $Dummy, $Date, $Time, $SvcName,
        $SrvrName, $SrvrIP, $CPUTime, $BytesRecv,
        $BytesSent, $SvcStatus, $NTStatus,
        $Operation, $Target, $Dummy) =
     split (/,/, $LogLine);

# Imprimimos los valores en la pantalla.
    print "Client's IP address = $ClientIP\n";
    print "Date of request = $Date\n";
    print "Time of request = $Time\n";
    print "Service name = $SvcName\n";
    print "Server name = $SrvrName\n";
    print "Server's IP address = $SrvrIP\n";
    print "Processing time = $CPUTime\n";
    print "Received $BytesRecv bytes of data\n";
    print "Sent $BytesSent bytes of data\n";
    print "Server returned status of $SvcStatus\n";
    print "Windows NT returned status code $NTStatus\n";
    print "Operation requested = $Operation\n";
    print "Target of operation = $Target\n";

#                    Fin de logs.pl