#!perl/bin/perl

# hitcnt.pl
#

# Primera versi�n. Utiliza un �nico archivo de registro de IIS,
# donde obtiene todas las visitas  las direcciones IP de los 
# usuarios. Versi�n de l�nea de comandos. La salida va a parar
# a la pantalla.

# Define una ruta de un archivo.
    $LogDir = "c:/winnt/system32/logfiles/";
    $LogFile = "in970502.log";
    $LogPath = $LogDir.$LogFile;

# Intenta abrir el archivo de registro. Si no lo consigue, cierra el
# programa.

    open (LOG, $LogPath) || die "Can't open $LogPath: $!\n";

# Recorre todas las l�neas del archivo de registro y extrae la
# informaci�n de la entrada.

    $n = 0;                # Inicia el contador.

    while (<LOG>)
        {
    ($ClientIP, $Dummy, $Date, $Time, $SvcName,
        $SrvrName, $SrvrIP, $CPUTime, $BytesRecv,
        $BytesSent, $SvcStatus, $NTStatus,
        $Operation, $Target, $Dummy) =
     split (/,/);
    # Guarda las direcciones IP del cliente y aumenta el valor del
    # contador.

        $IPArray[$n] = $ClientIP;
        $n++;
        }                     # Fin de while (<LOG>)

    close (LOG);            # Cierra el archivo de registro.

# Guarda el n�mero total de visitas e inicia los dos arrays para las
# IP y el n�mero de visitas recibidas desde cada una de ellas.

    $TotalHits = $n;
    @IPHits = ();
    @NumHits = ();
    $HitCount = 0;

# Recorre todos los elementos del array @IPArray y ordena las IP,
# incrementando el contador de cada una de ellas.

    for ($n = 0, $i = 0; $n < $TotalHits; $n++)
        {
        for ($p = 0; $p < $HitCount; $p++)
            {
            if ($IPArray[$n] eq $IPHits[$p])
                {
                $NumHits[$p]++;
                last;            
                }
            }

    # Si $p == $HitCount, no se habr� detectado ninguna visita. Se
    # tratar� de una nueva direcci�n IP. Se a�ade a la lista.

        if ($p == $HitCount)
            {
            $IPHits[$HitCount] = $IPArray[$n];
            $NumHits[$HitCount]++;
            $HitCount++;
            }
        }         # Fin de for ($n = 0...)

# Imprime los resultados.

    print "On 05/02/97:\n\n";

    for ($n = 0; $n < $HitCount; $n++)
        {
        print "$IPHits[$n] registered $NumHits[$n] hits\n";
        }

#				Fin de hitcnt.pl