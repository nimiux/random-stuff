#!perl/bin/perl

# hitcnt1.pl
#
# Segunda versi�n. Recorre todos los archivos del directorio de
# registro del servidor IIS.
# Extrae las visitas y las direcciones IP de donde proceden.
# Versi�n de l�nea de comandos. La salida va a parar a la pantalla.

# Define la ruta del directorio de registro.

    $LogDir = "c:/winnt/system32/logfiles";

# Intenta abrir el directorio de registro. Si no lo consigue, cierra 
# el programa.

    opendir (LOGD, $LogDir) || die "Can't open $LogDir: $!\n";

# Obtiene una lista con los archivos de registro y la guarda en un
# array.

    @LogFiles = readdir (LOGD);

# Recorre la lista y evita las entradas . y .. .

    foreach $LogFile (@LogFiles)
        {
        if (($LogFile eq ".") || ($LogFile eq ".."))
            {
            next;
            }

    # Intenta abrir el archivo de registro. Si no lo consigue, cierra
    # el programa.

        $LogPath = $LogDir."/".$LogFile;
           open (LOG, $LogPath) || die "Can't open $LogPath: $!\n";

    # Recorre todas las l�neas del archivo de registro y extrae la
    # informaci�n de la entrada.

        $n = 0;                # Inicia el contador.

        while (<LOG>)
            {
            ($ClientIP, $Dummy, $Date, $Time, $SvcName,
                $SrvrName, $SrvrIP, $CPUTime, $BytesRecv,
                $BytesSent, $SvcStatus, $NTStatus,
                $Operation, $Target, $Dummy) = split (/,/);

        # Guarda las direcciones IP del cliente y aumenta el valor 
        # del contador.

            $IPARRAY[$N] = $CLIENTIP;
            $n++;
            }                     # Fin while (<LOG>)

        close (LOG);            # Cierra el archivo de registro.

    # Guarda el n�mero total de visitas e inicia los dos arrays para 
    # las IP y el n�mero de visitas recibidas desde cada una de ellas.
        $TotalHits = $n;
        @IPHits = ();
        @NumHits = ();
        $HitCount = 0;
    # Recorre todos los elementos del array @IPArray y ordena las IP,
    # incrementando el contador de cada una de ellas.

        for ($n = 0, $i = 0; $n < $TotalHits; $n++)
            {
            for ($p = 0; $p < $HitCount; $p++)
                {
                if ($IPArray[$n] eq $IPHits[$p])
                    {
                    $NumHits[$p]++;
                    last;             # Igual que break en C
                    }
                }

      # Si $p == $HitCount, no se habr� detectado ninguna visita. Se
      # tratar� de una nueva direcci�n IP. Se a�ade a la lista.
 
           if ($p == $HitCount)
            {
                $IPHits[$HitCount] = $IPArray[$n];
                $NumHits[$HitCount]++;
                $HitCount++;
            }
        }                     # fin de for ($n = 0...)

    # Imprime los resultados despu�s de dar formato a la fecha
    # procedente del nombre del archivo.

        if ($LogFile = ~ /(..)(..)(..)(..)/)
            {
            $year = $2;
            $month = $3;
            $day = $4;
            }

        print "On $month/$day/$year:\n\n";
 
        for ($n = 0; $n < $HitCount; $n++)
            {
            print "$IPHits[$n] registered $NumHits[$n] hits\n";
            }

        print "\n";        # L�nea adicional.

    }                     # Fin de foreach $LogFile...

# Cierra el directorio.

    closedir (LOGDIR);

#                    Fin de hitcnt1.pl