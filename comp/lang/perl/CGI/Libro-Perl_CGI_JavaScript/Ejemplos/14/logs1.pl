#!perl/bin/perl

# logs1.pl
#
# Un script en Perl para leer, extraer e imprimir el contenido del
# archivo de registro del servidor Sambar Web.
#

# Guardamos el nombre del archivo en una variable local con la ruta
# completa.

    $LogFile = "c:/sambar/logs/access.log";

# Abrimos el archivo; si no se puede hacer, se detiene la ejecuci�n
# del programa.

    open (LOG, $LogFile) || die "Can't open $LogFile: $!\n";

# Lee, extrae e imprime todas las l�neas del registro.

    while (<LOG>)
        {
        $LogLine = $_;            # Guarda la l�nea localmente.

    # Elimina los caracteres que no se van a necesitar.

        $LogLine = ~ s/\ [|\ ]|\ "//g;
        chop ($LogLine);
    # Extrae los componentes utilizando split()
        ($ClientIP, $Dummy, $UserName, $DateTime, $TimeZone,
           $Operation,$Target, $HTTPVers, $SrvrStatus,
           $NTStatus,$BytesXfer) = split (/[ ]+/, $LogLine);

    # Imprime los valores en la pantalla.
        print "Client's IP address = $ClientIP\n";
        print "Name of user on client = $UserName\n";
        print "Date and time of request = $DateTime\n";
        print "Operation requested = $Operation\n";
        print "Operation target = $Target\n";
        print "Server returned status of $SrvrStatus\n";
        print "Windows NT returned status code $NTStatus\n";
        print "Transferred $BytesXfer bytes of data\n\n";
        }             # Fin de while (<LOG>)

    close (LOG);     # Cerramos el archivo de registro.

#                    Fin de logs1.pl