# Copyright Eric C. Herrmann 1998
use Win32::ODBC;
require "readPostInput.cgi";
%cars = readPostInput();
#Define the Data Source Name
$DSN = "Auto Ads";
# Create an Win32::ODBC object
my $myDb = Win32::ODBC->new($DSN);
#Verify the connection is valid
if (! $myDb){
  print "Failed to Connect $DSN\n";
  Win32::ODBC::DumpError();
  #You can't do anything without a connection so die here
  die;
}
#Using the data read by readPostInput in the cars hash
#create the SQL statement
$SQL = "Select model, year, price, comments From $cars{Make} ";
#Checkboxes are only returned if they are checked.  We can
#use this to our advantage to determine if the user wants
#an odered search result
if (( defined ($cars{Model} || defined ($cars{Year}) || defined $cars{Price}) ) ){
  $SQL .= " Order By ";
  my $count = 0 ;
  #At least one type of ordered search was requested.
  #The foreach processes only those list items that are defined
  foreach $order (  $cars{Model}, $cars{Year}, $cars{Price}){
     #Each time through the loop at the order request to
     #The SQL statement
     $orderList .= " $order,";
     #If count is greater than zero we know we have an
     #ordered search.
     $count++;
  }
  if ($count){
     #Get rid of the trailing comma
     chop $orderList;
     $SQL .= $orderList ;
  }
}
if ($myDb->Sql($SQL)){
  print "Error getting Make info for $car{Make}\n";
  $myDb->DumpError();
}
print<<"EOF";
Content-Type: text/html
<html>
<title> $cars{Make} Classifieds </title>
<body>
<center>
<h1> You Searched for $cars{Make} </h1>
EOF
#if there is any data in orderlist then tell the user the
#type of order requested
if ($orderList){
  @orderRequested = split(/,/,$orderList);
  #Save the list separator's previous value
  $prevSeparator = $";
  #This will print out , AND between each element of the array
  local $" = ", AND";
  print "<h2> Ordered by  @orderRequested </h2>\n";
  #Return the list separator to it's previous value
  $" = $prevSeparator;
}
#The cellpadding puts some space around the data in the table cell
#The 70% width tag force the last cell to take up most of the screen
print<<"EOF";
<table border=1 cellpadding=5>
  <tr>
     <th Align=left>Model
     <th Align=left>Year
     <th Align=left>Price
     <th Align=left width=70%>Additional Information
  </tr>
EOF
while ($myDb->FetchRow()){
  %model = $myDb->DataHash;
print<<"EOF";
  <tr>
     <td Align=left>$model{model}
     <td Align=left>$model{year}
     <td Align=left>$model{price}
     <td Align=left width=70%>$model{comments}
  </tr>
EOF
}
print<<"EOF";
</table>
</center>
</body>
</html>
EOF

