# Copyright Eric C. Herrmann 1998
use Win32::ODBC;
#Define the Data Source Name
$DSN = "Auto Ads";
# Create an Win32::ODBC object
my $myDb = Win32::ODBC->new($DSN);
#Verify the connection is valid
if (! $myDb){
   print "Failed to Connect $DSN\n";
   Win32::ODBC::DumpError();
  #You can't do anything without a connection so die here
  die;
}
#Read the classified section
open (ADS, "<$ARGV[0]") || die "Invalid file as first param $ARGV[0], $!";
@ads = <ADS>;
close ADS;
#Log the errors to file for later review
# The filename will be unique because the Process ID is
# appended using the special variable $$
open (ERRORFILE, ">errorList$$.txt") || die ;
(@times) = localtime(time);
#Date the file
$time = $times[2] . ':' . $times[1] . ':' . $times[0];
print ERRORFILE "$time\n";
for ($i=0; $i<=$#ads; $i++){
  #match the transportation line
  #The on-line classified file has a unique header before each car info line
  if ($ads[$i] =~ /TRANSPORTATION\s+-\s+(\w+)H/){
     my $make = $1;
     #Get the next line that has the car data
     my $carLine = $ads[++$i];
     #This regular expression pulls out the relevant info from an ad.
     #all the ads list the year model price and phone number
     #If an ad doesn't have this info it will be ignored
     ($year,$model,$comments1, $price, $comments2, $phone) =
     ($carLine =~ /.*?(\d+)\s*(\w+)\s*(.*)\$(\d+,?\d*)\s*(.*)(\d{3}-\d{4})/);
      #This creates a 2030 bug but we'll live with it
      if (length ($year) <= 2){
        if ($year > 30){
           $year = "19" . $year;
        }
        else{
           $year = "20" . $year;
        }
      }
      #The price has to match a comma that occurs sometimes
      #so remove it to keep all the prices in the same format
      $price =~ s/,//;
      #create a unique table id
      $ad_ID = $i . "_" . time;
      #put all the extra stuff into one scalar
      $comments = $comments1 . $comments2;
     #Insert the car data into the model table
     $SQL = qq|Insert Into $make (ad_ID, model, year,
                                  phone, comments, price)
               Values ('$ad_ID', '$model', '$year',
                       '$phone','$comments', '$price')|;
     if ($myDb->Sql($SQL)){
        $prevFH = select(ERRORFILE);
        print "Error Creating $make\n";
        Win32::ODBC::DumpError();
        select ($prevFH);
     }
  }
}
close ERRORFILE;
#Always close the Data Base connection
$myDb->Close();

