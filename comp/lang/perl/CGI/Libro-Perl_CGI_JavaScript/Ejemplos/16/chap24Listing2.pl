# Copyright Eric C. Herrmann 1998
use Cwd;
use Win32::ODBC;
#Define the driver type for this data base
$DriverType = "Microsoft Access Driver (*.mdb)";
#Define the Data Source Name
$DSN = "Email Contacts";
#Describe the Data Source Name
#The format must is required
$Description = "Description=Email List and Contact Information";
#The file name of the data base.
#This must be created before you can connect to the DSN
$DataBase = "EmailContacts.mdb";
#Set the directory to current directory
$dir = cwd();
#Configure the DSN
if (Win32::ODBC::ConfigDSN(ODBC_ADD_DSN,
                          $DriverType,
                          ("DSN=$DSN",
                          $Description,
                          "DBQ=$dir\\$DataBase",
                          "DEFAULTDIR=$dir",
                          "UID=", "PWD="))){
  print "Successful configuration of $DSN!\n";
}
else{
  print "Error Creating $DSN\n";
  #Always use the DumpError routine.
  #It tells you what is going on
  Win32::ODBC::DumpError();
  die;
}
# Create an Win32::ODBC object
# Lots of perl programmers use this notation:
# $myDBConnection = new Win32::ODBC($DSN);
# But I prefer this syntax 
my $myDb = Win32::ODBC->new($DSN);
#Create a lexical that will be accesible outside else block
my $connection;
#Verify the connection is valid
if (! $myDb){
  print "Failed to Connect $DSN\n";
  Win32::ODBC::DumpError();
  #You can't do anything without a connection so die here
  die;
}
else {
  $connection = $myDb->Connection();
  print "Successful Connection $connection, $DSN\n";
}
#Always close the Data Base connection
$myDb->Close(); 

