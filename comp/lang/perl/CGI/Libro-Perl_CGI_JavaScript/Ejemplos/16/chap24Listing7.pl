# Copyright Eric C. Herrmann 1998
use Win32::ODBC;

#Define the Data Source Name
$DSN = "Auto Ads";

# Create an Win32::ODBC object
# Lots of perl programmers use this notation:
# $myDBConnection = new Win32::ODBC($DSN);
# But I prefer this syntax
my $myDb = Win32::ODBC->new($DSN);
#Verify the connection is valid
if (! $myDb){
  print "Failed to Connect $DSN\n";
  Win32::ODBC::DumpError();
  #You can't do anything without a connection so die here
  die;
}
else {
  print "Connected to $DSN\n";
}
#Read the classified section
open (ADS, "<$ARGV[0]") || die "Invalid file as first param $ARGV[0], $!";
@ads = <ADS>;
close ADS;
#Log the errors to file for later review
# The filename will be unique because the Process ID is
# appended using the special variable $$
open (ERRORFILE, ">errorList$$.txt") || die ;
(@times) = localtime(time);
#Date the file
$time = $times[2] . ':' . $times[1] . ':' . $times[0];
print ERRORFILE "$time\n";
select ERRORFILE;
for ($i=0; $i<=$#ads; $i++){
  #match the transportation line
  #The on-line classified file has a unique header before each car info line
  if ($ads[$i] =~ /^\d+\s+-\s+(.*)<\/a>/){
     $make = $1;
     #remove commas and spaces
     $make =~ s/[,\s]//g;
     #create the table rows and columns
     #This statement should only be run once during initialization
     #if the table already exists its okay
     #Creating the table will fail but processing can continue
     $SQLStatment = qq|Create Table $make (ad_ID char(20) NOT NULL,
                                              model char(20) NOT NULL,
                                              year Integer,
                                              price Integer,
                                              phone char(10),
                                              comments char(80))|;
     if ($myDb->Sql($SQLStatment)){
         print ERRORFILE "error creating table $make\n";
         $myDb->DumpError();
     }
  }
}
close ERRORFILE;
$myDb->Close();

