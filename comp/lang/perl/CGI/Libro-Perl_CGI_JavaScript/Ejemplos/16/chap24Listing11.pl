# Copyright Eric C. Herrmann 1998
use Win32::ODBC;
#Define the Data Source Name
$DSN = "Auto Ads";

# Create an Win32::ODBC object
# Lots of perl programmers use this notation:
# $myDBConnection = new Win32::ODBC($DSN);
# But I prefer this syntax
my $myDb = Win32::ODBC->new($DSN);
#Verify the connection is valid
if (! $myDb){
  print "Failed to Connect $DSN\n";
  Win32::ODBC::DumpError();
  #You can't do anything without a connection so die here
  die;
}
@makes = $myDb->TableList;
#Remember whenever you return an HTML page
#from a CGI program you must include a valid HTTP Header
#followed by at least one blank line
print<<"EOF";
Content-Type: text/html
<html>
<body>
<form method=POST action="cgi-bin/classifiedCarSearch.cgi">
<center>
<h1>Search Our Transportation<br>
Classified ADS</h1>
<table>
  <tr>
     <th width=20%>Select a Make </th>
     <th Align=Left> Order By: </th>
  </tr>
  <tr>
     <td><select name="Make">
EOF
#This foreach loop creates option list values that
#are part of the previous select HTML statement
#The value is returned as a part of the CGI data
#The $make outside the Option tag is what the user sees.
#The newline is for convienence if debugging is required
foreach $make (@makes){
  print  qq|<Option value="$make"> $make\n|;
}
#after the option list is created we just need to
#list the order that they want to see their cars
print<<"EOF";
        </select>
     </td>
     <td witdh=80%>
        <table>
           <tr>
              <td><input type=checkbox name="Price"> Price </td>
              <td><input type=checkbox name="Model"> Model </td>
              <td><input type=checkbox name="Year"> Year </td>
           </tr>
        </table>
     </td>
  </tr>
</table>
<input type=submit Value="Get Search Results">
<input type=reset>
</form>
</center>
</body>
</html>
EOF

