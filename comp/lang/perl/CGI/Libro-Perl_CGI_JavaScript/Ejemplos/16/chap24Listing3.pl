# Copyright Eric C. Herrmann 1998
#Log the errors to file for later review
# The filename will be unique because the Process ID is
# appended using the special variable $$
open (ERRORFILE, ">errorList$$.txt") || die ;
(@times) = localtime(time);
#Date the file
$time = $times[2] . ':' . $times[1] . ':' . $times[0];
select ERRORFILE;
print  "$time\n";
$SQLStatment = qq|Create Table $make (ad_ID char(20) NOT NULL,
                                              model char(20) NOT NULL,
                                              year Integer,
                                              price Integer,
                                              phone char(10),
                                              comments char(80))|;
if ($myDb->Sql($SQLStatment)){
  print  "error creating table $make\n";
  $myDb->DumpError();
}

