# Copyright Eric C. Herrmann 1998
use Win32::ODBC;
#Define the Data Source Name
$DSN = "Email Contacts";
# Create an Win32::ODBC object
# Lots of perl programmers use this notation:
# $myDBConnection = new Win32::ODBC($DSN);
# But I prefer this syntax 
my $myDb = Win32::ODBC->new($DSN);
#Verify the connection is valid
if (! $myDb){
  print "Failed to Connect $DSN\n";
  Win32::ODBC::DumpError();
  #You can't do anything without a connection so die here
  die;
}
#create the table rows and columns
#This statement should only be run once during initialization
$SQLStatment = qq|Create Table emailContacts(address char(40) NOT NULL,
                                            lastName char(20),
                                            firstName char(10),
                                            MI char (2))|;
if ($myDb->Sql($SQLStatment)){
  print "Error creating the initial Table\n";
  Win32::ODBC->DumpError();
}
#Always close the Data Base connection
$myDb->Close();

