# Copyright Eric C. Herrmann 1998
use Win32::ODBC;
if ($#ARGV < 0){
   die "You must enter the contact list file name as the first argument";
}
#Define the Data Source Name
$DSN = "Email Contacts";

# Create an Win32::ODBC object
# Lots of perl programmers use this notation:
# $myDBConnection = new Win32::ODBC($DSN);
# But I prefer this syntax
my $myDb = Win32::ODBC->new($DSN);
#Verify the connection is valid
if (! $myDb){
  print "Failed to Connect $DSN\n";
  Win32::ODBC::DumpError();
  #You can't do anything without a connection so die here
  die;
}
#read the contact data in and add it to the new table
open (CONTACTS, "<$ARGV[0]") || 
          die "Invalid file name specified: $ARGV[0]";
@contacts = <CONTACTS>;
close (CONTACTS);
#Log the errors to file for later review
# The filename will be unique because the Process ID is
# appended using the special variable $$
open (ERRORFILE, ">errorList$$.txt") || die ;
(@times) = localtime(time);
#Date the file
$time = $times[2] . ':' . $times[1] . ':' . $times[0];
print ERRORFILE "$time\n";
#Read each line of the contact list and add it to the data base
foreach $contact (@contacts){
  ($firstName, $MI, $lastName, $emailAddress) = split(/:/,$contact);
  chomp $emailAddress;
  #Insert the contact list into the emailContacts table
  $SQL = qq|Insert Into emailContacts ( address,
                                       firstName,  MI, lastName)
                        Values ('$emailAddress',
                                '$firstName',
                                '$MI',
                                '$lastName')|;
  if ($myDb->Sql($SQL)){
     #Force the output from DumpError into the error file
     $prevFH = select(ERRORFILE);
     print "Error Creating emailContacts\n";
     Win32::ODBC::DumpError();
     select ($prevFH);
  }
}
close (ERRORFILE);
#Always close the Data Base connection
$myDb->Close();

