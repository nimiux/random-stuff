# Copyright Eric C. Herrmann 1998
use Win32::ODBC;
open (OUTFILE, ">searchCars.htm");
select (OUTFILE);
#Define the Data Source Name
$DSN = "Auto Ads";

# Create an Win32::ODBC object
my $myDb = Win32::ODBC->new($DSN);

#Verify the connection is valid
if (! $myDb){
  print "Failed to Connect $DSN\n";
  Win32::ODBC::DumpError();
  #You can't do anything without a connection so die here
  die;
}
@makes = $myDb->TableList;
(@times) = localtime(time);
#Date is stored in the tables as a day of year and year
$oldDay = $times[7] - 10;
$year = $times[5];
#If we are in a new year back up to the previous year
if ($oldDay < 1){
  $oldDay += 365;
  $year--;
}
foreach $make (@makes){
  $SQL = qq|Delete From $make Where (
                              (Day <= $oldDay AND Year = $year)
                              OR Sold = True)|;
  if ($myDb->Sql($SQL)){
     print "Error deleting from $make, $oldDay, $year\n";
     $myDb->DumpError();
  }
}
$myDb->Close();

