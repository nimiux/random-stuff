# Copyright Eric C. Herrmann 1998
use Win32::ODBC;
#Get the Available Drivers
%drivers = Win32::ODBC::Drivers();
#I want to print the driver attributes comma separated
#Load the dynamic copy of array separator with a new separator
local $" = ", ";
foreach $driver (sort keys %drivers){
   @attributes = split (/;/,$drivers{$driver});
   print "Driver = $driver\n";
  print "@attributes\n\n";
}

