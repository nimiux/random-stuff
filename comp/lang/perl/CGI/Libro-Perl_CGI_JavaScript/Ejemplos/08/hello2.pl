#!/usr/bin/perl

# hello2, a slightly more sophisticated "Hello World"

     $Hello = "Hello, World";     # String variable
     $TimeAround = 2;             # Numeric variable

     print $Hello, " for the ", $TimeAround, "nd time!", "\n";

#     End hello2.pl
