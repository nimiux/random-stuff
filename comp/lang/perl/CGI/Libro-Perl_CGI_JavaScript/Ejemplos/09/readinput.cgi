 #!/usr/bin/perl

 %postInputs = readPostInput();
 $dateCommand = "date";
 $time = `$dateCommand`;
 open (MAIL, "|/usr/sbin/sendmail -t") || return 0;

 select (MAIL);
 print << "EOF";
 To:   YOUR_ADDRESS\ @YOUR_DOMAIN.com
 From: $postInputs{ 'Contact_Email'}
 Subject: $postInputs{ 'Organization'}  Information Requested

 $time
 $postInputs{ 'Organization'}  Information Requested
 Name: $postInputs{ 'Contact_FullName'}
 Email: $postInputs{ 'Contact_Email'}
 Street Address: $postInputs{ 'Contact_StreetAddress'}
 Street Address (cont): $postInputs{ 'Contact_Address2'}
 City: $postInputs{ 'Contact_City'}
 State : $postInputs{ 'Contact_State'}
 Zip: $postInputs{ 'Contact_ZipCode'}
 Work Phone: $postInputs{ 'Contact_WorkPhone'}
 Home Phone: $postInputs{ 'Contact_HomePhone'}
 FAX: $postInputs{ 'Contact_FAX'}
 Email: $postInputs{ 'Contact_Email'}
 Comments: $postInputs{ 'comments'}


 EOF
    close(MAIL);
    select (STDOUT);
    printThankYou();

 sub readPostInput(){
    my (%searchField, $buffer, $pair, @pairs);
    if ($ENV{ 'REQUEST_METHOD'}  eq 'POST'){
       read(STDIN, $buffer, $ENV{ 'CONTENT_LENGTH'} );
       @pairs = split(/&/, $buffer);
       foreach $pair (@pairs){
          ($name, $value) = split(/=/, $pair);
          $value =~ tr/+/ /;
          $value =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;
          $name =~ tr/+/ /;
          $name =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1)) eg;
          $searchField{ $name}  = $value;
       }
    }
    return (%searchField);
 }

 sub printThankYou(){
 print << "EOF";
 Content-Type: text/html

 <HEAD>
 <TITLE>THANK YOU FOR FOR YOUR REQUEST</TITLE>
 </HEAD>
 <BODY>
 <TABLE CELLSPACING=2 CELLPADDING=2 border=0 width=600>
 <TR><th><BR>
 <center>
 <FONT SIZE=+3><B>Thank You $postInputs{ 'Contact_FullName'} </b></font>
 </center><BR><BR>

 <CENTER><B><FONT SIZE=+1>
 <P>For submitting your information. We will get back with you shortly.
 </P>
 </FONT></B><CENTER>
 </th>
 </table>
 </BODY>
 </HTML>

 EOF
 }