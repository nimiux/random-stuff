  sub Close { 
   my $self = shift;
   my $s;#=new FileHandle;
   $s = $self->{ 'socket'} ;
  if ($self->{ buffer} ) { 
    my $code = $self->{ code} ;
    print $s (&$code($self->{ buffer} ));
    delete $self->{ buffer} ;
  } 
  if ($self->{ 'multipart'} ) { print $s "\r\n-",$self->{ 'boundary'} , "-\r\n";} 
  print $s "\r\n.\r\n";
 
  $_ = <$s>; if (/^[45]/) { close $s; return $self->{ 'error'} = TRANSFAILED;} 
 
  print $s "quit\r\n";
  $_ = <$s>;
 
  close $s;
  delete $self->{ 'socket'} ;
  return 1;
 } 