  sub MailMsg { 
    my $self = shift;
    my $msg;
    if (ref $_[0] eq 'HASH') { 
       my $hash=$_[0];
       $msg=$hash->{ msg} ;
       delete $hash->{ msg} 
    }  else { 
       $msg = pop;
    } 
    return $self->{ 'error'} =NOMSG unless $msg;
 
    $self->Open(@_);
    $self->SendEx($msg);
    $self->Close;
    return $self;
 } 