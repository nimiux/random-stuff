  sub Open { 
   my $self = shift;
   if ($self->{ 'socket'} ) { 
    if ($self->{ 'error'} ) { 
     $self->Cancel;
    }  else { 
     $self->Close;
    } 
   } 
  delete $self->{ 'error'} ;
  my %changed;
  $self->{ multipart} =0;
 
  if (ref $_[0] eq 'HASH') { 
   my $key;
   my $hash=$_[0];
   foreach $key (keys %$hash) { 
    $self->{ lc $key} =$hash->{ $key} ;
    $changed{ $key} =1;
   } 
  }  else { 
   my ($from, $reply, $to, $smtp, $subject, $headers ) = @_;
 
   if ($from) { $self->{ 'from'} =$from; $changed{ 'from'} =1;} 
   if ($reply) { $self->{ 'reply'} =$reply; $changed{ 'reply'} =1;} 
   if ($to) { $self->{ 'to'} =$to;$changed{ 'to'} =1;} 
   if ($smtp) { $self->{ 'smtp'} =$smtp; $changed{ 'smtp'} =1;} 
   if ($subject) { $self->{ 'subject'} =$subject;$changed { 'subject'} =1;} 
   if ($headers) { $self->{ 'headers'} =$headers;$changed { 'headers'} =1;} 
  } 
 
  $self->{ 'to'}  =~ s/[ \ t]+/, /g if ($changed{ to} );
  $self->{ 'to'}  =~ s/,,/,/g if ($changed{ to} );
  $self->{ 'boundary'}  =~ tr/=/-/ if $changed{ boundary} ;
 
  if ($changed{ from} ) { 
   $self->{ 'fromaddr'}  = $self->{ 'from'} ;
   $self->{ 'fromaddr'}  =~ s/.*<([^\ s]*?)>/$1/; 
  } 
 
  if ($changed{ reply} ) { 
   $self->{ 'replyaddr'}  = $self->{ 'reply'} ;
   $self->{ 'replyaddr'}  =~ s/.*<([^\ s]*?)>/$1/; 
   $self->{ 'replyaddr'}  =~ s/^([^\ s]+).*/$1/; 
  } 
 
  if ($changed{ smtp} ) { 
   $self->{ 'smtp'}  =~ s/^\ s+//g; #elimina los espacios de $smtp
   $self->{ 'smtp'}  =~ s/\ s+$//g;
   $self->{ 'smtpaddr'}  = ($self->{ 'smtp'}  =~
   /^(\d{ 1,3} )\ .(\d{ 1,3} )\ .(\d{ 1,3} )\ .(\d{ 1,3} )$/)
   ? pack('C4',$1,$2,$3,$4)
   : (gethostbyname($self->{ 'smtp'} ))[4];
  } 
 
  if (!$self->{ 'to'} ) {  return $self->{ 'error'} =TOEMPTY; } 
 
  if (!defined($self->{ 'smtpaddr'} )) {  return $self->{ 'error'} =HOSTNOTFOUND($self->{ smtp} ); } 
 
  my $s = &FileHandle::new(FileHandle);
  $self->{ 'socket'}  = $s;
 
  if (!socket($s, AF_INET, SOCK_STREAM, $self->{ 'proto'} )) { 
    return $self->{ 'error'} =SOCKFAILED; } 
 
  if (!connect($s, pack('Sna4x8', AF_INET, $self->{ 'port'} , $self->{ 'smtpaddr'} ))) { 
    return $self->{ 'error'} =CONNFAILED; } 
 
  my($oldfh) = select($s); $| = 1; select($oldfh);
 
  $_ = <$s>; if (/^[45]/) {  close $s; return $self->{ 'error'} =SERVNOTAVAIL; } 
 
  print $s "helo localhost\r\n";
  $_ = <$s>; if (/^[45]/) {  close $s; return $self->{ 'error'} =COMMERROR; } 
 
  print $s "mail from: <$self->{ 'fromaddr'} >\r\n";
  $_ = <$s>; if (/^[45]/) {  close $s; return $self->{ 'error'} =COMMERROR; } 
 
  foreach (split(/, /, $self->{ 'to'} )) { 
    print $s "rcpt to: <$_>\r\n";
    $_ = <$s>; if (/^[45]/) {  close $s; return $self->{ 'error'} =USERUNKNOWN($self->{ to} , $self->{ smtp} ); } 
    } 
 
  print $s "data\r\n";
  $_ = <$s>; if (/^[45]/) {  close $s; return $self->{ 'error'} =COMMERROR; } 
 
  print $s "To: $self->{ 'to'} \r\n";
  print $s "From: $self->{ 'from'} \r\n";
  print $s "Reply-to: $self->{ 'replyaddr'} \r\n" if $self->{ 'replyaddr'} ;
  print $s "X-Mailer: Perl Mail::Sender Version $Mail::Sender::ver Jan Krynicky  <Jan\ @chipnet.cz> Czech Republic\r\n";
  if ($self->{ 'headers'} ) { print $s $self->{ 'headers'} ,"\r\n"} ;
  print $s "Subject: $self->{ 'subject'} \r\n\r\n";
 
  return $self;
 } 