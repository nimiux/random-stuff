  #!/usr/bin/perl
  if ($#ARGV < 1){ 
      print "contact file first letter file 2nd\n";
      exit 1;
      } 
  $contactFile = $ARGV[0];
  $letterFile = $ARGV[1];
   
  open (CONTACTLIST, "<$contactFile") || die "Can't open
                                       $contactFile\n";
 open (LETTER, "<$letterFile") || die "Can't open $letterFile\n";
 @contactList = <CONTACTLIST>;
 @letter = <LETTER>;
 close (CONTACTLIST);
 close (LETTER);
 $count = 0;
 for $line (@contactList){ 
 
    if ($line =~ /@/){ 
        $count++;
       open (MAIL, "|/usr/sbin/sendmail -t") || die "Can't openpipe to sendmail \n";
       ($companyName, $emailAddress, $FLName) = split(/,/,$line);
       $emailAddress =~ s/\ t//g;
       $FLName =~ /\ s*(\ w+)\ s*(\ w+)/;
       $firstName= $1;  $lastName = $2;
       chomp $lastName;
       select (MAIL);
       $subjectLine = $letter[0];
       chop $subjectLine ;
       $subjectLine = "$companyName";
       $returnAddress = "Eric.Herrmann\ @assi.net";
       print<<"EOF";
 To: $emailAddress
 From: $returnAddress
 Subject: $subjectLine
 
 EOF
       for $index (0 .. $#letter){ 
          $letterLine = $letter[$index];
          chop $letterLine;
          if ($letterLine =~ /companyName/) { 
             $letterLine =~ s/companyName/$companyName/g;
          } 
          if ($letterLine =~ /firstName/) { 
             $letterLine =~ s/firstName/$firstName/g;
          } 
          if ($letterLine =~ /lastName/) { 
             $letterLine =~ s/lastName/$lastName/g;
          } 
          print "$letterLine\n";
       } #end for loop
    close (MAIL);
    } #end if
 } 
 
 print STDOUT "\ nYou sent $count emails\n";