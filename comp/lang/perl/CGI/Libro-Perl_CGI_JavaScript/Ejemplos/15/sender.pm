 use Mail::Sender;
 $sender = new Mail::Sender({ from => 'yawp@io.com',
                             smtp => 'mail.assi.net'} );
 
 if (!(ref $sender) =~ /Sender/i){ 
    die $Mail::Sender::Error;
 } 
 
 $sender->MailMsg({ to =>'Eric.Herrmann@assi.net',
                    subject => 'Testing Sender',
                    msg => "An easy email interface?"} );
 
 if ( ($sender->{ 'error'} ) < 0) { 
    print "ERROR: $Mail::Sender::Error\ n";
 } 
 else { 
    print "Msg Sent Ok\ n";
 } 