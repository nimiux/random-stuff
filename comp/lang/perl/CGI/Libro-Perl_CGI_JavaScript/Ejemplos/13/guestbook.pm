# GuestBook.pm
#
# Header file for the routines to add data to and read it back from 
# the Web page Guest Book.
#

    require ("d:/pub/scripts/perl-cgi/html.pm")
        || die ("Can't find header file\n");

# Some useful constants.

    # Format for pack ()
    $GuestEntryStruct     = "a30a30a30a30a30a30a256l";
    # Path to guest book file
    $GuestBookPath        = "/pub/http/perl-cgi/ngbook.dat";

# Indexes of elements in the packed structure.

    $FirstName            = 0;
    $LastName             = 1;
    $City                 = 2;
    $State                = 3;
    $Country              = 4;
    $EMail                = 5;
    $Comments             = 6;
    $NumEntryTime         = 7;
    $NumElements          = 8;    # Number of elements in structure.
    $RecordSize           = 440;  # All the sizes added up.
    
# Days of the week.
 
    @WeekDay = ("Sunday", "Monday", "Tuesday",
                "Wednesday", "Thursday", "Friday",
                "Saturday");
 
# Months of the year.
 
    @Month = ("January", "February", "March", "April",
                 "May", "June", "July", "August",
                 "September", "October", "November", "December");
 
#                     End GuestBook.pm
