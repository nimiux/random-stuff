#!/perl/bin/perl

    require ("d:/pub/scripts/perl-cgi/GuestBook.pm") || 
        die ("Can't find GuestBook header file: $!\n");

# Get the POSTed information from STDIN and put it in $post_info.

    read (STDIN, $post_info, $ENV {'CONTENT_LENGTH'});

# Split off the fields, which are delimited with '&',
# and put them in @InfoArray.

    @InfoArray = split (/&/, $post_info);

# Go through each element in @InfoArray, split off the
# "variable=" part, then translate pluses into spaces and 
# any escaped hex chars back into their real character values.

    for ($n = 0; @InfoArray[$n]; $n++)
        {
        ($dummy, $temp) = split (/=/, @InfoArray[$n]);
        $temp =~ tr/+/ /;
        $temp =~ s/%([\dA-Fa-f][\dA-Fa-f])/pack ("C", hex ($1))/eg;
        @InfoArray [$n] = $temp;
        }

# Now we'll check to see if we have anything to write
# to the guest book.  We need a first or last name, at 
# least; otherwise, we'll jump around the routines that 
# write this stuff to the guest book file.

    if ((length (@InfoArray[$FirstNameIndex]) != 0)
            || (length (@InfoArray [$LastNameIndex]) != 0))
        {

    # Tack the current time to the end of the array.

        $time = time ();                        # Get the current time.
        @InfoArray[$NumEntryTime] = $time;      # Put it in the array.

    # Pack the data into a binary structure, open the guest book
    # file for appending, and write it all out to disk.

        $GuestEntry = pack ($GuestEntryStruct, @InfoArray);
        open (GUEST_LOG, ">>$GuestBookPath")
            || die "Can't open guest book: $!";
        print GUEST_LOG $GuestEntry;
        close (GUEST_LOG);
        }                    # End if ((length...)

# Finally, we put up a cute little HTML document announcing
# that everything's done, with a link to the guest book viewer.

    &HTML_Header ("All done!");
    print "<BODY>\n";
    print "<CENTER>\n";
    print "<H1>Thanks for taking the time to ";
    print "sign our guest book...</H1>\n";
    print "<HR>\n";
    print "Click <A HREF=\"/scripts/perl-cgi/guestbook.pl\">here</A> ";
    print "to view the guest book.<BR>\n";
    &HTML_Ender ();

#                    End addguest.pl
