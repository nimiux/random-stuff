# Go through each element in @InfoArray, split off the
# "variable=" part, then translate pluses into spaces and 
# any escaped hex chars back into their real character values.

    for ($n = 0; @InfoArray[$n]; $n++)
        {
        ($dummy, $temp) = split (/=/, @InfoArray[$n]);
        $temp =~ tr/+/ /;
        $temp =~ s/%([\dA-Fa-f][\dA-Fa-f])/pack ("C", hex ($1))/eg;
            if ($temp =~ /[;<>&\*`|]/)
            {
            print "Illegal entry!!\n";
            exit;
            }
        @InfoArray [$n] = $temp;
        }
