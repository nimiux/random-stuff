#!perl/bin/perl

# guestbook.pl
#
# Reads records from the guest book file specified in 
# guestbook.pm, formats them in HTML and sends them
# to a Web page.

# Get the header file; scream loudly and exit if it can't be
# found.  Otherwise, define the title string.

    require ("d:/pub/scripts/perl-cgi/GuestBook.pm") || 
        die ("Can't find GuestBook header file: $!\n");

    $Title = "Perl-CGI Guest Book Entries";

# Attempt to open the guest book file.  Again, this is
# a fatal error if it doesn't succeed.

    open (GUEST_LOG, $GuestBookPath) || 
        die "Can't open guest book: $!";

# Set up the HTML document.

    &HTML_Header ($Title);
    print "<BODY>\n";
    print "<H1 ALIGN=\"CENTER\">$Title</H1>\n";
    print "<HR>\n";

# Read records and display them in a while loop.  The test
# at the top of the while block fails when all the records
# have been read.

    while (read (GUEST_LOG, $buffer, $GuestEntrySize))
        {

    # Use unpack to load the record into an array of fields based
    # on the same template we used with pack to format them for
    # the file.

        @InfoArray = unpack ($GuestEntryStruct, $buffer);

    # Loop through the elements of the array and remove any NULL
    # padding from the strings, in case this is being run on a browser
    # that prints spaces for NULLs.

        for ($n = 0; $n < ($NumElements - 1); $n++)
            {
            $InfoArray[$n] =~ s/\0//g;
            }

    # Load separate variables with the elements in @InfoArray.

        ($FirstName, $LastName, $City, $State, $Country, $Email,
            $Comments, $NumAccessTime) = @InfoArray;

        print "<STRONG>Name:</STRONG><BR>\n";
        printf ("%s %s<BR>\n", $FirstName, $LastName);
        print "<STRONG>E-mail address:</STRONG><BR>\n";
        print $Email, "<BR>\n";
        print "<STRONG>From:</STRONG><BR>\n";
        print $City, " ", $State, " ", $Country, "<BR>\n";
        print "<STRONG>On:</STRONG><BR>\n";
    
    # Set up a string time description after running the 4-byte
    # time value through localtime ().

        ($sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst)
            = localtime ($NumAccessTime);
        
        print "$WeekDay[$wday], $Month[$mon] $mday, ", $year + 1900;
        print " at $hour:";
        if ($min < 10)
            {
            print "0";
            }
        print "$min:";
        if ($sec < 10)
            {
            print "0";
            }
        print "$sec <BR>\n";
        print "<STRONG>Comments:</STRONG><BR>\n";
        print $Comments, "<BR>\n";
        print "<HR>\n";
        }

    close (GUEST_LOG);
    &HTML_Footer ();

#                    End GuestBook.pl
