#!/perl/bin/perl

# access.pl
#
# Primera versi�n. Crea o abre un archivo con un n�mero,
# lo incrementa y guarda su valor.
    $CountFile = "counter.dat";    # Nombre del archivo del contador.
# Abre y lee el archivo. Si no existe "contects", tomar� su valor 
# como "0".

    open (COUNT, $CountFile);
    $Counter = <COUNT>;            # Lee el contenido.

# Cierra el archivo y lo vuelve a abrir para imprimir su contenido.

    close (COUNT);
    open (COUNT, ">$CountFile");

# Aumenta $Counter y vuelve a escribir su valor. Muestra un mensaje
# con el nuevo valor. Cierra y sale del archivo.

    $Counter += 1;
    print COUNT $Counter;
    print "$CountFile has been written to $Counter times.\n";
    close (COUNT);

# End access.pl