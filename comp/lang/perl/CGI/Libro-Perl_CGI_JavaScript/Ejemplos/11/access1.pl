#!/perl/bin/perl
 
# access.pl
#
# Second version.  Creates or opens a file with a number
# in it, increments the number, writes it back, then displays
# the result in a message on a Web page.
 
    require "perl-cgi/html.pl";                # Get HTML header, ender.
    $CountFile = "counter.dat";                # Name of counter file.
    $PageTitle = "Web Page Access Counter";    # Web page title.
 
# Open the file and read it.  If it doesn't exist, its "contents"
# will be read into a program variable as "0".
 
    open (COUNT, $CountFile);
    $Counter = <COUNT>;                        # Read the contents.
 
# Close the file, then reopen it for output.
 
    close (COUNT);
    open (COUNT, ">$CountFile");
 
# Increment $Counter, then write it back out.  Put up a message
# with the new value.  Close the file.
 
    $Counter += 1;
    print COUNT $Counter;
    close (COUNT);
 
# Put the result up in a standard HTML document.
 
    &HTML_Header ($PageTitle);            # HTTP header info.
    print "<BODY>\n";
    print "<H1>$PageTitle</H1>\n";        # Big heading.
    print "<HR>\n";                       # Draw a rule.
    print "<H3>You are visitor #$Counter ";
    print "to our Web page!</H3>\n";
    &HTML_Ender;


# End access.pl
 
