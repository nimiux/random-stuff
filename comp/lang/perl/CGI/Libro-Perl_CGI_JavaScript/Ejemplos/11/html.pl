# html.pl

# Contiene las subrutinas de la cabecera y del finalizador encargados
# de configurar documentos HTML a partir del c�digo en Perl.

# Configura una cabecera HTML est�ndar, entregando el t�tulo de la 
# p�gina a trav�s de la l�nea de comandos.

sub HTML_Header
{
      # Crea la l�nea de apertura HTTP est�ndar.
    print "Content-type: text/html", "\n\n";
      # Especifica el documento HTML.
    print "<HTML>", "\n\n";                  
      # Especifica la secci�n de la cabecera.
    print "<HEAD>", "\n\n";                  
      # Coloca la l�nea del t�tulo.
    print "<TITLE>", "@_", "</TITLE>", "\n\n";
      # Secci�n de la cabecera de finalizaci�n.
    print "</HEAD>", "\n\n";                 

}                    #    Fin de HTML_Header.pl.

# Configura una secci�n para el pie HTML.  Es decir,
# se limita a finalizar las secciones BODY y HTML.

sub HTML_Ender
{
    print "\n", "</BODY>", "\n\n";
    print "</HTML>", "\n\n";

}                    # Fin de HTML_Ender
1;
