function table_getValue(row,col) {
 return this.data[row*this.columns+col]
}
function table_setValue(row,col,value) {
 this.data[row*this.columns+col]=value
}
function table_set(contents) {
 var n = contents.length
 for(var j=0;j<n;++j) this.data[j]=contents[j]
}
function table_isHeader(row,col) {
 return this.header[row*this.columns+col]
}
function table_makeHeader(row,col) {
 this.header[row*this.columns+col]=true
}
function table_makeNormal(row,col) {
 this.header[row*this.columns+col]=false
}
function table_makeHeaderRow(row) {
 for(var j=0;j<this.columns;++j)
  this.header[row*this.columns+j]=true
}
function table_makeHeaderColumn(col) {
 for(var i=0;i<this.rows;++i)
  this.header[i*this.columns+col]=true
}
function table_write(doc) {
 doc.write("<TABLE BORDER="+this.border+">")
 for(var i=0;i<this.rows;++i) {
  doc.write("<TR>")
  for(var j=0;j<this.columns;++j) {
   if(this.header[i*this.columns+j]) {
    doc.write("<TH>")
    doc.write(this.data[i*this.columns+j])
    doc.write("</TH>")
   }else{
    doc.write("<TD>")
    doc.write(this.data[i*this.columns+j])
    doc.write("</TD>")
   }
  }
  doc.writeln("</TR>")
 }
 doc.writeln("</TABLE>")
}
function table(rows,columns) {
 this.rows = rows
 this.columns = columns
 this.border = 0
 this.data = new Array(rows*columns)
 this.header = new Array(rows*columns)
 this.getValue = table_getValue
 this.setValue = table_setValue
 this.set = table_set
 this.isHeader = table_isHeader
 this.makeHeader = table_makeHeader
 this.makeNormal = table_makeNormal
 this.makeHeaderRow = table_makeHeaderRow
 this.makeHeaderColumn = table_makeHeaderColumn
 this.write = table_write
}
