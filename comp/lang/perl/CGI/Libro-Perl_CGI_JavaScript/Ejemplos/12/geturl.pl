#!/perl/bin/perl

# geturl.pl
#
# A little Perl script to read, decode and print the names
# and values passed to it from an HTML form through CGI.

# Get HTML header, ender, define the page title.

    require "/pub/scripts/perl-cgi/html.pl";  # Full path.
    $Title = "Get Information From A URL";

# Get the query string.

    $QueryString = $ENV{'QUERY_STRING'};

# Use split to make an array of name-value pairs broken at
# the ampersand character.

    @NameValuePairs = split (/&/, $QueryString);

# Put up an HTML header, page title and a rule.
    
    &HTML_Header ($Title);
    print "<BODY>\n";
    print "<H1>$Title</H1>\n";
    print "<HR>\n";

# Split each of the name-value pairs and print them on the page.

    foreach $NameValue (@NameValuePairs)
        {
        ($Name, $Value) = split (/=/, $NameValue);
        print "Name = $Name, value = $Value<BR>\n";
        }

# End the HTML document.

    &HTML_Ender;

#                    End geturl.pl
