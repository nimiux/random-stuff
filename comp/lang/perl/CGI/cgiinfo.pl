#!/usr/bin/env perl

print "Content-Type: text/html\n\n";

#HTTP_*
my @envvars = qw(
    AUTH_TYPE
    CONTENT_LENGTH
    CONTENT_TYPE
    GATEWAY_INTERFACE
    PATH_INFO
    PATH_TRANSLATED
    QUERY_STRING
    REMOTE_ADDR
    REMOTE_HOST
    REMOTE_IDENT
    REMOTE_USER
    REQUEST_METHOD
    SCRIPT_NAME
    SERVER_NAME
    SERVER_PORT
    SERVER_PROTOCOL
    SERVER_SOFTWARE
    USER
    LOGNAME
);

sub printenvvar() {
    # This function uses an external array of varibales
    print "$_  =  $ENV{$_}<br>";
}

sub printenvfun {
    # This function uses the function-oriented approach of CGI module
    print header;
    print start_html("Environment");
    print h1("Variables");
    print start_ul;
    foreach my $key (sort(keys(%ENV))) {
        li("$key = $ENV{$key}");
    }
    print end_ul;
    print end_html;
}

sub printenvobj {
    # This function uses the object-oriented approach of CGI module

    my $cgi = new CGI;
    print $cgi->header;
    print $cgi->start_html('Environment');
    print $cgi->h1('Variables');
    foreach my $key (sort(keys(%ENV))) {
        print "$key = $ENV{$key}<br>\n";
    }
    print $cgi->end_html();
}

printenvvar for @envvars;
printenvfun;
printenvobj;
