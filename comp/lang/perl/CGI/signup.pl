#!/usr/bin/env perl

use strict;
use warnings;

use CGI qw(:standard);
use CGI::Carp qw(warningsToBrowser fatalsToBrowser);

#use Mail::Sendmail;

%mail = ( To      => 'somebody@someplace.com',
          From    => 'otherbody@thisplace.com',
          Message => "This is a very short message"
         );

sendmail(%mail) or die $Mail::Sendmail::error;
#print "OK. Log says:\n", $Mail::Sendmail::log;

print header;
print start_html("Done");
print h2("Your data has been procesed");

my %form;
foreach my $p (param()) {
    $form{$p} = param($p);
    print "$p = $form{$p}<br>\n";
}
print end_html;
