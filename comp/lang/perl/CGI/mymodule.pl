#!/usr/bin/env perl
#
# wget http://www.cpan.org/.../Package.tar...
# tar xvf Package.tar...
# cd Package
#
# if Makefile.PL
# perl Makefile.PL
# or
# perl Makefile.PL PREFIX=/place/to/store/packages
# make
# make test
# make install
# if Build.PL, install first Module::Build
# perl Build.PL
# or
# perl Build.PL --install_base /place/to/store/packages
# perl Build
# perl Build test
# perl Build install
#
# export PERL5LIB=/place/to/store/packages
# or
# use lib qw(/place/to/store/packages);
#
# @INC is a list of places to look fot
# use happens at compile time, so if we want to modify
# @INC to include a dir, we need:
# BEGIN { unshift @INC, '/place/to/store/packages' }

# Env stuff
sub printenvfun {
    # This function uses the function-oriented approach of CGI module
    print header;
    print start_html("Environment");
    print h1("Variables");
    print start_ul;
    foreach my $key (sort(keys(%ENV))) {
        li("$key = $ENV{$key}");
    }
    print end_ul;
    print end_html;
}

sub printenvobj {
    # This function uses the object-oriented approach of CGI module

    my $cgi = new CGI;
    print $cgi->header;
    print $cgi->start_html('Environment');
    print $cgi->h1('Variables');
    foreach my $key (sort(keys(%ENV))) {
        print "$key = $ENV{$key}<br>\n";
    }
    print $cgi->end_html();
}

