%{

%}


espacio         [ /t/n]+
letra           [a-zA-Z]
digito          [0-9]+
identificador   {letra}({letra}|{digito})*

%x CODIGO_C UNION REGLAS

%%

"%{"            {
                BEGIN CODIGO_C;
                return(TP_LLAVEA);
                }

"%}"            {
                BEGIN 0;
                return(TP_LLAVE_C);
                }

<CODIGO_C>\n    {
                return(CARAC_COD);
                }

<CODIGO_C>.    {
                return(CARAC_COD);
                }

"%union"        {
                BEGIN UNION;
                return(TP_UNION);
                }

<UNION>"{"              {
                        return(LLAVEA);
                        }
{identificador}  {
                        return(ID);
                        }

<UNION>"}"      {
                BEGIN 0;
                return(LLAVEC);
                }

"%token"        {
                return(TP_TOKEN);
                }

"<"             {
                return(MENOR);
                }

">"             {
                return(MAYOR);
                }

"%type"         {
                return(TP_TYPE);
                }

"%%"            {
                BEGIN REGLAS;

                return(TP_TP);
                }

<REGLAS>":"     {
                return(DOS_P);
                }

<REGLAS>"|"     {
                return(T_ALT);
                }

<REGLAS>"["     {
                return(T_CORCHA);
                }

<REGLAS>"]"     {
                return(T_CORCHC);
                }

<REGLAS>"*"     {
                return(T_AST);
                }

<REGLAS>"+"     {
                return(T_MAS);
                }

<REGLAS>"("     {
                return(T_PARA);
                }

<REGLAS>":"     {
                return(T_PARC);
                }

";"             {
                return(PUNT_CO);
                }

<REGLAS>"%%"    {
                BEGIN CODIGO_C;
                return(TP_TP);
                }









                



