#ifndef __SMALL__
   #error HAY QUE COMPILAR EN EL MODELO SMALL DEL TURBO-C
#endif

#pragma warn -eff

/*#include "c-code.h"*/
#include <stdio.h>
#include <ctype.h>

/***************************************************************/
/*                                                             */
/*     DEFINICION DE ALIAS PARA LOS TIPOS BASICOS              */
/*                                                             */
/***************************************************************/

typedef char  byte;
typedef int   word;
typedef long  lword;
typedef void *ptr;             /* El tipo 'Puntero gen�rico' */
/* float no necesita alias */

typedef byte array;              /* Alias de byte */
typedef byte record;



/***************************************************************/
/*                                                             */
/*     DEFINICION DEL CONJUNTO DE REGISTROS Y LA PILA          */
/*                                                             */
/***************************************************************/

/*
 *   Permite incluir este fichero en varios, pero definiendo e inicializando
 *   las tablas y dem�s variables solo en uno de ellos, que ser� el que
 *   defina la macro ALLOC antes de incluirlo.
 */

#ifdef ALLOC
#  define CLASS                 /* Reserva espacio para las variable, */
#  define I(x) x                /* y permite su inicializaci�n. */
#else
#  define CLASS extern          /* Declara las variables como externas, */
#  define I(x)                  /* y no las inicializa. */
#endif

struct _words { word low, high; };
struct _bytes { byte b0, b1, b2, b3; };

typedef union reg
{
 ptr    pp;           /* Puntero gen�rico */
 float  f;            /* Un Palabra larga de 32 bits */
 lword  l;            /* Palabra larga de 32 bits */
 struct _words w;     /* Dos palabras de 16 bits */
 struct _bytes b;     /* Cuatro bytes */
}
reg;

CLASS reg r0, r1, r2, r3, r4, r5, r6, r7,
	  r8, r9, rA, rB, rC, rD, rE, rF;

#define SDEPTH 2048

CLASS reg stack[SDEPTH];              /* Definicion de la pila */
CLASS reg global[SDEPTH];             /*Memoria global*/
CLASS reg cadena[SDEPTH];             /*Memoria cadena*/
CLASS reg *__sp I(= &stack[SDEPTH]);  /* Puntero a la cima de la pila */
CLASS reg *__fp I(= &stack[SDEPTH]);  /* Puntero al registro de activacion */
CLASS reg *hp   I(=&global[0]);      /* Puntero a memoria global */
CLASS reg *cp   I(=&cadena[0]);      /* Puntero a cima de cadena */

#define fp ((char *) __fp)            /* fp se incrementa de 1 en 1 */
#define sp ((char *) __sp)            /* sp se incrementa de 1 en 1 */

#define SEG(segmento)                 /* Indica un cambio de segmento */

/***************************************************************/
/*                                                             */
/*     DEFINICION DE LAS CLASES DE ALMACENAMIENTOS             */
/*                                                             */
/***************************************************************/


#define public              /* Las subrutinas ser�n 'public' */
#define common              /* Las v. globales no inic. ser�n 'common' */
#define private   static    /* No lo usaremos */
#define external  extern    /* No lo usaremos */


#define ALIGN(tipo)         /* Alinea la sig. var. como ese tipo */

/***************************************************************/
/*                                                             */
/*     DEFINICION DE LAS DIRECTIVAS DE ACCESO A LA PILA        */
/*                                                             */
/***************************************************************/


#define W   * (word   *)
#define B   * (byte   *)
#define L   * (lword  *)
#define F   * (float  *)
#define P   * (ptr    *)
#define WP  * (word  **)
#define BP  * (byte  **)
#define LP  * (lword **)
#define FP  * (float **)
#define PP  * (ptr   **)

/***************************************************************/
/*                                                             */
/*   DEFINICION DE LAS INSTRUCCIONES PARA EL MANEJO DE LA PILA */
/*                                                             */
/***************************************************************/

/*
#define push(n)      (--__sp)->l = (lword) (n)
#define pop(t)       (t) ((__sp++)->l)
*/

#define es_n_float(numero) ((((numero)-(numero)+1)/2)!=0)
#define es_t_float(tipo)   es_n_float( (tipo)0 )


#define push(num)    (   es_n_float(num) ? ((--__sp)->f = (float)(num))   \
					 : ((--__sp)->l = (lword)(num))   )
#define pop(tipo)    (   es_t_float(tipo) ? (tipo) ((__sp++)->f)   \
					  : (tipo) ((__sp++)->l)   )

/***************************************************************/
/*                                                             */
/*   DEFINICION DE LAS INSTRUCCIONES PARA LAS SUBRUTINAS       */
/*                                                             */
/***************************************************************/


#define PROC(nombre,clase)    clase nombre() {
#define ENDP(nombre)          ret(); }

/*  'call' apila la direcci�n simb�lica de retorno y salta a la subrutina.
*/
#define call(nombre) ( (--__sp)->pp = #nombre,  (*(void(*)(void))(nombre))() )

/* 'ret' desapila la direccion simb�lica de retorno y vuelve de la subrutina.
*/
#define ret()           __sp++; return

/*  Apilo el antiguo 'fp'.
**  Cargo 'sp' en 'fp', para que apunte a la cima de la pila.
**  Reservo espacio en la pila para las variables locales y temporales.
*/
#define link(num_elementos)   ( (--__sp)->pp = (char *)__fp,  \
				__fp = __sp,                  \
				__sp -= (num_elementos)       )

/*  Libero el espacio reservado para las variables locales y temporales.
**  Cargo en 'fp' el valor antiguo, que est� en la cima de la pila.
*/
#define unlink()              ( __sp = __fp,                  \
				__fp = (reg *)(__sp++)->pp    )


/*  Desplazamiento l�gico a la derecha (Logical Right Shift).
*/
#define lrs(x,n)    ((x) = (unsigned long)(x) >> (n)))


/*****************************************************************/
/*                                                               */
/*   DEFINICION DE LAS INSTRUCCIONES PARA CONVERSIONES DE TIPO   */
/*                                                               */
/*****************************************************************/


#define int_a_char(reg)        ( reg.b.b0    =  (byte)  reg.w.low )
#define int_a_float(reg)       ( reg.f       =  (float) reg.w.low )
#define float_a_int(reg)       ( reg.w.low   =  (word)  reg.f     )
#define float_a_char(reg)      ( reg.b.b0    =  (byte)  reg.f     )
#define char_a_float(reg)      ( reg.f       =  (float) reg.b.b0  )
#define char_a_int(reg)        ( reg.w.low   =  (word)  reg.b.b0  )
#define float_a_boolean(reg)   ( reg.w.low   =   0.0 != reg.f     )

/*****************************************************************/
/*                                                               */
/*   DEFINICION DE LAS INSTRUCCIONES PARA LECTURA/IMPRESION      */
/*                                                               */
/*****************************************************************/

#define escribe(reg)    printf( "%f", reg.l      )
#define imprime_string(reg)   printf( "%s", reg.pp     )
#define leer_char(reg)       scanf( "%c",  reg.pp  )
#define leer_word(reg)       scanf( "%d",  reg.pp  )
#define leer_float(reg)      { float f; scanf( "%f", &f ); *(float*)(reg.pp)=f; }
#define leer_string(reg)     scanf( "%s",  reg.pp  )

void mi_scanf( char *formato, void *p )
{
   char buffer[40], ch;
   int i = 0;

   ch = getchar();
   while ( isspace(ch) )
      ch=getchar();
   if ( ch==EOF )
      return;
   while ( !isspace(ch) )
   {
      if ( i<39 )
	 buffer[i++] = ch;
      ch=getchar();
   }
   buffer[i] = '\0';
   sscanf( buffer, formato, p );
}


/***************************************************************/
/*                                                             */
/*   DEFINICION DE LAS INSTRUCCIONES PARA EL CONTROL DE FLUJO  */
/*                                                             */
/***************************************************************/


#define EQ(a,b)     if ( (a) == (b) )
#define NE(a,b)     if ( (a) != (b) )
#define LT(a,b)     if ( (a) <  (b) )
#define LE(a,b)     if ( (a) <= (b) )
#define GT(a,b)     if ( (a) >  (b) )
#define GE(a,b)     if ( (a) >= (b) )

#define BIT(bit,s)    if ( (s) & (1<<(bit)) )


#define _main main
