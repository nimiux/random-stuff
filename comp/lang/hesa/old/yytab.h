#define TP_LLAVE_A 258
#define TP_LLAVE_C 259
#define TP_TP 260
#define PROD 261
#define TIPO 262
#define UNION 263
#define ID 264
#define CARACTER 265

typedef union {
    tabla_id   id_ref;
    lista_sel  *p_sel;
    lista_id   *p_id;
    char       caract;
} YYSTYPE;
extern YYSTYPE yylval;
