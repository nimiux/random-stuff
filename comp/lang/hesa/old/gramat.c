
#include <stdio.h>

#include "est_dat.h"
#include "herram.h"
#include "genera.h"


void procesa_union(lista_id *lista);

tabla_id procesa_campo(tabla_id tipo, tabla_id campo);
void procesa_hojas(tabla_id campo, lista_id *lista);

lista_sel *procesa_opcion(lista_sel *opciones, tabla_id nueva_op);
void procesa_seleccion(tabla_id padre, lista_sel *opciones);

void procesa_lista(tabla_id id);

void procesa_componente(tabla_id etiq);
void procesa_agregado(tabla_id id, lista_id *lista);

void procesa_estructuras(void);

void libera_arboles_seleccion(void);
void libera_raiz_seleccion(tabla_id id);

typedef union
 {
  tabla_id   id_ref;
  lista_sel  *p_sel;
  lista_id   *p_id;
  char       caract;
 } YYSTYPE;
#define YYSUNION /* %union occurred */
#define TP_LLAVE_A 257
#define TP_LLAVE_C 258
#define TP_TP 259
#define PROD 260
#define TIPO 261
#define UNION 262
#define ID 263
#define CARACTER 264
YYSTYPE yylval, yyval;
#define YYERRCODE 256



void yyerror(char *s)
{
 extern int yylines;
 printf("\nhesa: %s , linea %d\n",s,yylines);
}


void procesa_union(lista_id *lista)
{
 genera_union(lista);
 libera_lista(lista);
}

tabla_id procesa_campo(tabla_id tipo, tabla_id campo)
{

 actualiza_clase(tipo,DEF_TIPO);
 actualiza_clase(campo,CAMPO_UNION);
 actualiza_tipo(campo,tipo);

 return(campo);
}


void procesa_hojas(tabla_id campo, lista_id *lista)
{
 lista_id *p;
 int longitud,i=1;
 tabla_id simbolo;

 longitud = lista_longitud(lista);
 while(i<=longitud)
 {
  p = identificador_iesimo(lista,i);
  simbolo = identificador_simbolo(p);
  actualiza_campo(simbolo,campo);
  i++;
 }
 libera_lista(lista);
}


lista_sel *procesa_opcion(lista_sel *opciones, tabla_id nueva_op)
{
 lista_sel *p;

 if(extrae_clase(nueva_op)==RAIZ_SELECCION)
   {
    p = extrae_arbol(nueva_op);
    actualiza_clase(nueva_op, SELECCION);
   }
 else
   {
    p = const_opc(nueva_op);
    actualiza_arbol(nueva_op,p);
   }
 return(anade_herm(opciones,p));
}


void procesa_seleccion(tabla_id padre, lista_sel *opciones)
{
 lista_sel *p_padre;

 actualiza_clase(padre,RAIZ_SELECCION);

 p_padre = extrae_arbol(padre);
 if(p_padre == NULL)
  {
   p_padre = const_opc(padre);
   actualiza_arbol(padre,p_padre);
  }
 else
   actualiza_clase(padre,SELECCION);
  
 anade_hijos(p_padre, opciones);
}


void procesa_lista(tabla_id id)
{
 actualiza_clase(id,LISTA);
 genera_lista(id);
}

void procesa_agregado(tabla_id id, lista_id *lista)
{
 actualiza_clase(id,AGREGADO);
 genera_agregado(id,lista);
 libera_lista(lista);
}

void procesa_componente(tabla_id etiq)
{
 actualiza_clase(etiq,ETIQUETA);
}

void procesa_estructuras(void)
{
 genera_prototipos_h();
 genera_definiciones();
 genera_lista_generica();
 genera_terminales();
 genera_atributos();
 genera_prototipos_c();
 genera_selecciones();
 libera_arboles_seleccion();
 genera_funciones();
}	      

void libera_arboles_seleccion(void)
{
 recorre_tabla((void(*)(tabla_id))libera_raiz_seleccion);
}

void libera_raiz_seleccion(tabla_id id)
{
 if(extrae_clase(id)==RAIZ_SELECCION)
   libera_arbol(extrae_arbol(id));
}
FILE *yytfilep;
char *yytfilen;
int yytflag = 0;
int svdprd[2];
char svdnams[2][2];

int yyexca[] = {
  -1, 1,
  0, -1,
  -2, 0,
  -1, 32,
  58, 27,
  44, 27,
  -2, 20,
  0,
};

#define YYNPROD 29
#define YYLAST 140

int yyact[] = {
       4,      17,      49,      48,      47,       7,       5,      46,
      44,      43,      32,      30,      25,      19,      13,      14,
       2,      16,      29,      37,       9,      36,      26,      41,
      39,      27,      38,      34,      21,      12,      24,      23,
      22,      15,      20,      10,       8,      40,       6,       3,
       1,      18,      31,      11,      42,      35,      33,       0,
       0,      28,       0,       0,       0,       0,       0,       0,
       0,       0,       0,       0,       0,       0,       0,       0,
       0,       0,       0,      45,       0,       0,       0,       0,
       0,       0,       0,       0,       0,       0,       0,       0,
       0,       0,       0,       0,       0,       0,       0,       0,
       0,       0,       0,       0,       0,       0,       0,       0,
       0,       0,       0,       0,       0,       0,       0,       0,
       0,       0,       0,       0,       0,       0,       0,       0,
       0,       0,       0,       0,       0,       0,       0,       0,
       0,       0,       0,       0,       0,       0,       0,       0,
       0,       0,       0,       0,       0,       0,       0,       0,
       0,       0,       0,      13,
};

int yypact[] = {
    -241,   -1000,   -1000,    -258,    -257,   -1000,   -1000,    -103,
   -1000,    -249,    -244,    -124,   -1000,    -250,    -251,   -1000,
     -38,   -1000,   -1000,     -34,    -251,   -1000,   -1000,   -1000,
   -1000,    -242,    -252,   -1000,   -1000,    -253,     -41,    -105,
     -16,     -35,   -1000,     -21,    -254,    -255,   -1000,    -256,
    -259,    -260,    -261,   -1000,   -1000,   -1000,   -1000,   -1000,
   -1000,   -1000,
};

int yypgo[] = {
       0,      46,      27,      45,      44,      43,      29,      42,
      40,      39,      38,      36,      35,      34,      33,      28,
      32,      31,      30,
};

int yyr1[] = {
       0,      11,       8,       9,       9,      10,       5,       5,
       6,      12,      12,      14,       4,       4,      13,      13,
      15,      15,      15,      16,       7,       7,      17,      18,
       1,       1,       2,       3,       3,
};

int yyr2[] = {
       0,       0,       8,       0,       2,       4,       1,       2,
       3,       0,       2,       5,       1,       2,       1,       2,
       1,       1,       1,       3,       1,       3,       4,       3,
       1,       3,       3,       1,       3,
};

int yychk[] = {
   -1000,      -8,     257,      -9,     258,     264,     -10,     262,
     -11,     123,     -12,      -5,      -6,     263,     259,     -14,
     261,     125,      -6,     263,     -13,     -15,     -16,     -17,
     -18,     263,      60,      59,     -15,     260,     263,      -7,
     263,      -1,      -2,      -3,      62,     124,      42,      59,
      58,      44,      -4,     263,     263,      -2,     263,     263,
     263,     263,
};

int yydef[] = {
       0,      -2,       3,       0,       0,       4,       1,       0,
       9,       0,       0,       0,       6,       0,       0,      10,
       0,       5,       7,       0,       2,      14,      16,      17,
      18,       0,       0,       8,      15,       0,       0,      19,
      -2,      23,      24,       0,       0,       0,      22,       0,
       0,       0,      11,      12,      21,      25,      27,      26,
      28,      13,
};

int *yyxi;
/*****************************************************************/
/* PCYACC LALR parser driver routine -- a table driven procedure */
/* for recognizing sentences of a language defined by the        */
/* grammar that PCYACC analyzes. An LALR parsing table is then   */
/* constructed for the grammar and the skeletal parser uses the  */
/* table when performing syntactical analysis on input source    */
/* programs. The actions associated with grammar rules are       */
/* inserted into a switch statement for execution.               */
/*****************************************************************/


#ifndef YYMAXDEPTH
#define YYMAXDEPTH 200
#endif
#ifndef YYREDMAX
#define YYREDMAX 1000
#endif
#define PCYYFLAG -1000
#define WAS0ERR 0
#define WAS1ERR 1
#define WAS2ERR 2
#define WAS3ERR 3
#define yyclearin pcyytoken = -1
#define yyerrok   pcyyerrfl = 0
YYSTYPE yyv[YYMAXDEPTH];     /* value stack */
int pcyyerrct = 0;           /* error count */
int pcyyerrfl = 0;           /* error flag */
int redseq[YYREDMAX];
int redcnt = 0;
int pcyytoken = -1;          /* input token */


yyparse()
{
  int statestack[YYMAXDEPTH]; /* state stack */
  int      j, m;              /* working index */
  YYSTYPE *yypvt;
  int      tmpstate, tmptoken, *yyps, n;
  YYSTYPE *yypv;


  tmpstate = 0;
  pcyytoken = -1;
#ifdef YYDEBUG
  tmptoken = -1;
#endif
  pcyyerrct = 0;
  pcyyerrfl = 0;
  yyps = &statestack[-1];
  yypv = &yyv[-1];


  enstack:    /* push stack */
#ifdef YYDEBUG
    printf("at state %d, next token %d\n", tmpstate, tmptoken);
#endif
    if (++yyps - &statestack[YYMAXDEPTH] > 0) {
      yyerror("pcyacc internal stack overflow");
      return(1);
    }
    *yyps = tmpstate;
    ++yypv;
    *yypv = yyval;


  newstate:
    n = yypact[tmpstate];
    if (n <= PCYYFLAG) goto defaultact; /*  a simple state */


    if (pcyytoken < 0) if ((pcyytoken=yylex()) < 0) pcyytoken = 0;
    if ((n += pcyytoken) < 0 || n >= YYLAST) goto defaultact;


    if (yychk[n=yyact[n]] == pcyytoken) { /* a shift */
#ifdef YYDEBUG
      tmptoken  = pcyytoken;
#endif
      pcyytoken = -1;
      yyval = yylval;
      tmpstate = n;
      if (pcyyerrfl > 0) --pcyyerrfl;
      goto enstack;
    }


  defaultact:


    if ((n=yydef[tmpstate]) == -2) {
      if (pcyytoken < 0) if ((pcyytoken=yylex())<0) pcyytoken = 0;
      for (yyxi=yyexca; (*yyxi!= (-1)) || (yyxi[1]!=tmpstate); yyxi += 2);
      while (*(yyxi+=2) >= 0) if (*yyxi == pcyytoken) break;
      if ((n=yyxi[1]) < 0) { /* an accept action */
        if (yytflag) {
          int ti; int tj;
          yytfilep = fopen(yytfilen, "w");
          if (yytfilep == NULL) {
            fprintf(stderr, "Can't open t file: %s\n", yytfilen);
            return(0);          }
          for (ti=redcnt-1; ti>=0; ti--) {
            tj = svdprd[redseq[ti]];
            while (strcmp(svdnams[tj], "$EOP"))
              fprintf(yytfilep, "%s ", svdnams[tj++]);
            fprintf(yytfilep, "\n");
          }
          fclose(yytfilep);
        }
        return (0);
      }
    }


    if (n == 0) {        /* error situation */
      switch (pcyyerrfl) {
        case WAS0ERR:          /* an error just occurred */
          yyerror("syntax error");
          yyerrlab:
            ++pcyyerrct;
        case WAS1ERR:
        case WAS2ERR:           /* try again */
          pcyyerrfl = 3;
	   /* find a state for a legal shift action */
          while (yyps >= statestack) {
	     n = yypact[*yyps] + YYERRCODE;
	     if (n >= 0 && n < YYLAST && yychk[yyact[n]] == YYERRCODE) {
	       tmpstate = yyact[n];  /* simulate a shift of "error" */
	       goto enstack;
            }
	     n = yypact[*yyps];


	     /* the current yyps has no shift on "error", pop stack */
#ifdef YYDEBUG
            printf("error: pop state %d, recover state %d\n", *yyps, yyps[-1]);
#endif
	     --yyps;
	     --yypv;
	   }


	   yyabort:
            if (yytflag) {
              int ti; int tj;
              yytfilep = fopen(yytfilen, "w");
              if (yytfilep == NULL) {
                fprintf(stderr, "Can't open t file: %s\n", yytfilen);
                return(1);              }
              for (ti=1; ti<redcnt; ti++) {
                tj = svdprd[redseq[ti]];
                while (strcmp(svdnams[tj], "$EOP"))
                  fprintf(yytfilep, "%s ", svdnams[tj++]);
                fprintf(yytfilep, "\n");
              }
              fclose(yytfilep);
            }
	     return(1);


	 case WAS3ERR:  /* clobber input char */
#ifdef YYDEBUG
          printf("error: discard token %d\n", pcyytoken);
#endif
          if (pcyytoken == 0) goto yyabort; /* quit */
	   pcyytoken = -1;
	   goto newstate;      } /* switch */
    } /* if */


    /* reduction, given a production n */
#ifdef YYDEBUG
    printf("reduce with rule %d\n", n);
#endif
	if (yytflag && redcnt<YYREDMAX) redseq[redcnt++] = n;
    yyps -= yyr2[n];
    yypvt = yypv;
    yypv -= yyr2[n];
    yyval = yypv[1];
    m = n;
    /* find next state from goto table */
    n = yyr1[n];
    j = yypgo[n] + *yyps + 1;
    if (j>=YYLAST || yychk[ tmpstate = yyact[j] ] != -n) tmpstate = yyact[yypgo[n]];
    switch (m) { /* actions associated with grammar rules */

	  case 1:
	  {genera_def_arbol();} break;
      case 2:
	  {procesa_estructuras();} break;
      case 4:
	  {genera_cabecera(yypvt[-0].caract);} break;
      case 5:
	  {procesa_union(yypvt[-1].p_id);} break;
      case 6:
	  {yyval.p_id = anade_identificador(NULL,yypvt[-0].id_ref);} break;
      case 7:
	  {yyval.p_id = anade_identificador(yypvt[-1].p_id,yypvt[-0].id_ref);} break;
      case 8:
	  {yyval.id_ref = procesa_campo(yypvt[-2].id_ref,yypvt[-1].id_ref);} break;
	  case 11:
	  {procesa_hojas(yypvt[-2].id_ref,yypvt[-0].p_id);} break;
      case 12:
	  {yyval.p_id = anade_identificador(NULL,yypvt[-0].id_ref);} break;
      case 13:
	  {yyval.p_id = anade_identificador(yypvt[-1].p_id,yypvt[-0].id_ref);} break;
      case 19:
	  {procesa_seleccion(yypvt[-2].id_ref,yypvt[-0].p_sel);} break;
      case 20:
	  {yyval.p_sel = procesa_opcion(NULL,yypvt[-0].id_ref);} break;
      case 21:
	  {yyval.p_sel = procesa_opcion(yypvt[-2].p_sel,yypvt[-0].id_ref);} break;
      case 22:
	  {procesa_lista(yypvt[-3].id_ref);} break;
      case 23:
	  {procesa_agregado(yypvt[-2].id_ref,yypvt[-0].p_id);} break;
      case 24:
	  {yyval.p_id = yypvt[-0].p_id;} break;
      case 25:
	  {yyval.p_id = une_listas(yypvt[-2].p_id,yypvt[-0].p_id);} break;
      case 26:
	  {yyval.p_id = yypvt[-2].p_id;} break;
      case 27:
	  {procesa_componente(yypvt[-0].id_ref);
      		 yyval.p_id = anade_identificador(NULL,yypvt[-0].id_ref);} break;
      case 28:
	  {procesa_componente(yypvt[-0].id_ref);
      		 yyval.p_id = anade_identificador(yypvt[-2].p_id,yypvt[-0].id_ref);} break;    }
	goto enstack;
}
