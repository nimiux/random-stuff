///////////////////////////////
// Analizador l�xico
///////////////////////////////
class Analex extends Lexer;

options {
charVocabulary = '\3'..'\377';
k=2; // Necesario para distinguir entre el inicio de 
     // un elemento "<(LETRA)" � "</" y el de un 
     // comentario "<!"
}


BLANCO : (' '|'\t'|NUEVA_LINEA)
         {$setType(Token.SKIP);}
       ;
    
protected NUEVA_LINEA: "\r\n"
         {newline();}
       ;
       
// La secuencia "--" no est� permitida dentro de un 
// comentario
COMENTARIO_BLOQUE : "<!--" (
                        ~('-')
                       | ('-' ~'-' ) => '-'
                    )* "-->"
                  {$setType(Token.SKIP);}
                  ;

// Comentarios de bloque resueltos con la opci�n 
// greedy. Con k=3 permitir�a que "--" apareciese 
// dentro de un comentario. Cosa que, por simplificar, 
// no contempla XML.
//COMENTARIO_BLOQUE : "<!--" 
//                   (options {greedy=false;} :.)* 
//                    "-->"
//                  {$setType(Token.SKIP);}
//                  ;
 
// Los nombres de elementos pueden contener letras, 
// d�gitos, guiones bajos y dos puntos
protected LETRA : 'a'..'z'
      | 'A'..'Z'
      ; 
protected DIGITO : '0'..'9'
      ;

protected IDENT : (LETRA | '_')(LETRA|DIGITO|'_'|':')*
                ;

APERTURA : '<' IDENT'>'
         ;
         
CIERRE : "</" IDENT '>'
       ;
       
// En el texto no pueden aparecer ni '<' ni '&', en su 
// lugar se podr�n usar las cadenas "&lt;" y "&amp;"
TEXTO : (~('<'|'&')|"&lt;"|"&amp;")+
      ;
