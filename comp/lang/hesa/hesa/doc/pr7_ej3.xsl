<?xml version="1.0" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="xml" indent="yes"/> 
 
<xsl:variable name="lugar">Sevilla</xsl:variable>
 
<xsl:template match="/precipitaciones">
    <precipitaciones>
        <lugar>
           <xsl:value-of select="$lugar"/>
        </lugar>
        <xsl:apply-templates select="registro"/> 
    </precipitaciones>
</xsl:template>
 
<xsl:template match="registro">
    <xsl:if test="./lugar = $lugar">
        <registro>
            <fecha> <xsl:value-of select="fecha"/> </fecha>
            <litros-m2> <xsl:value-of select="litros-m2"/> </litros-m2>
        </registro>
    </xsl:if>
</xsl:template>
 
</xsl:stylesheet>
