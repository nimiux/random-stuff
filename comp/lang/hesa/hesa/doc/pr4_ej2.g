///////////////////////////////////
// Anasint.g: Analizador sint�ctico
///////////////////////////////////
header{
import java.io.*;
import java.util.Hashtable;}

class Anasint extends Parser;

//---------------------------------
// Atributos y m�todos auxiliares
//---------------------------------
{  
   Hashtable variables = new Hashtable();

   public int valorNumero(Token n) {
      return new Integer(n.getText()).intValue();
   }

   public int valorVariable(Token v) {
      int res;
      try {
         res = ((Integer)variables.get(v.getText()))
               .intValue();
      } catch(NullPointerException npe)
 	{res=0;}
      return res;
   }
   
   public void asignaValor(Token v, int n) {
      variables.put(v.getText(),new Integer(n));
   } 
}

//---------------------------------
// Reglas
//---------------------------------

ordenes : (orden) + ;

orden   : (IDENT "=") => asignacion   
        | expresion 
        ;

asignacion {int e;}
        :  i:IDENT "=" e=expr ";"
	   {asignaValor(i,e); 
	    System.out.println("Asignacion: "+i.getText()+"="+e);}
        ;

expresion {int e;}
        : e=expr ";"
           {System.out.println("Expresion: "+e);}
        ;

expr returns [int res=0] {int e1, e2;}
	: e1=expr_mult {res=e1;}
	  (("+" e2=expr_mult) {res=res+e2;}
	  |("-" e2=expr_mult) {res=res-e2;})* 
	;

expr_mult returns [int res=0] {int e1, e2;}
	: e1=expr_base {res=e1;}
	  (("*" e2=expr_base) {res=res*e2;}
	  |("/" e2=expr_base) {res=res/e2;})*
	;
	exception catch[ArithmeticException ae]
	  {System.out.println("-->Divisi�n por cero");
           res=0;}

expr_base returns [int res=0] {int e;}
	: n:NUMERO 
           {res=valorNumero(n);}
        | "(" e=expr ")"
           {res=e;}
        | i:IDENT
 	   {res=valorVariable(i);}
        ;
