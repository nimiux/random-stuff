///////////////////////////////
// Analizador sintáctico
///////////////////////////////
class Anasint extends Parser;

// El uso de EOF obliga a que el símbolo entrada reconozca 
// el fichero completo 
entrada : instrucciones EOF ;

instrucciones : (instruccion)+;

instruccion : asignacion
            | bucle
            | condicional            
            ;      
             
asignacion : IDENT "=" expr ";" ;
                      
bucle : WHILE "(" expr ")" bloque ;
            
condicional : IF "(" expr ")" bloque ;
            
bloque : instruccion
       | "{" instrucciones "}"
       ;

expr : expr_suma (("<"|"<="|">"|">="|"=="|"!=") expr_suma)? ;

expr_suma : exp_mult (("+"|"-") exp_mult)* ;

exp_mult : exp_base (("*"|"/") exp_base)* ;

exp_base : NUMERO
         | IDENT
         | "(" expr ")"
         ;
