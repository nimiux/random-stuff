///////////////////////////////
// Analizador sintáctico
///////////////////////////////
class Anasint extends Parser;

options {
   buildAST=true;
   k=3;
}

tokens {
   DECVARS;
   DECVAR;
   DECFUNCS;
   DECFUNC;
   INSTRS;
   PROGRAMA;
   LLAMADA;
}

//-----------------------------
// Regla principal
//-----------------------------
programa  : dec_variables dec_funciones EOF! 
             {## = #(#[PROGRAMA,"PROGRAMA"],##);}
          ;


//-----------------------------
// Declaración de variables
//-----------------------------
dec_variables : (dec_variable)*
                 {##=#(#[DECVARS,"DECVARS"],##);}
              ;

dec_variable : t:tipo! idents[#t] ";"!;

tipo : INT | CHAR | VOID;

idents[AST t] : dec_ident[t] (","! dec_ident[t])* ;

dec_ident[AST t] : IDENT
                    {AST tipo = astFactory.dupTree(t);
                     ##=#(#[DECVAR,"DECVAR"],tipo,##);};



//-----------------------------
// Declaración de funciones
//-----------------------------
dec_funciones : (dec_funcion)*
                 {##=#(#[DECFUNCS,"DECFUNCS"],##);};

dec_funcion : tipo IDENT "("! ")"! "{"! dec_variables instrucciones "}"!
                 {##=#(#[DECFUNC,"DECFUNC"],##);};



//-----------------------------
// Instrucciones 
//-----------------------------
instrucciones : (instruccion)*
                 {##=#(#[INSTRS,"INSTRS"],##);};

instruccion : asignacion
            | bucle
            | condicional            
            | ruptura
            | llamada
            ;      
             
asignacion : IDENT "="^ expr ";"! ;
                      
bucle : WHILE^ "("! expr ")"! bloque ;

condicional : IF^ "("! expr ")"! bloque else_opc ;

ruptura : BREAK ";"! ;

llamada : IDENT "("! ")"! ";"!
           {##=#(#[LLAMADA,"LLAMADA"],##);};

  
else_opc : ELSE! bloque 
         | 
         ;

bloque : instruccion
       | "{"! instrucciones "}"!
       ;

//-----------------------------
// Expresiones
//-----------------------------
expr : expr_suma (("<"^|"<="^|">"^|">="^|"=="^|"!="^) expr_suma)? ;

expr_suma : exp_mult (("+"^|"-"^) exp_mult)* ;

exp_mult : exp_base (("*"^|"/"^) exp_base)* ;
      
exp_base : NUMERO
         | IDENT
         | "("! expr ")"!
         ;