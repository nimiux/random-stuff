package awtsample;

import java.applet.Applet;
import java.awt.Frame;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.Dialog;
import java.awt.Button;
import java.awt.Rectangle;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.BorderLayout;
import java.awt.Color;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseEvent;

public class awtsample extends Applet implements ActionListener, MouseListener, MouseMotionListener { 

   private Frame f;

   private Panel p;

   private Button b1;
   private Button b2;
   private TextField tf1;

   public awtsample () {
	 System.out.println("En construccion");
	 f= new Frame("Titulo");
	 p=new Panel();
	 tf1=new TextField();
   }

   public void init () {
      try {
	 f.setLayout(new FlowLayout());

	 tf1.setText("Inicio");

	 b1=new Button("Boton 1");
	 b1.addActionListener(new ActionListener () {
	       public void actionPerformed(ActionEvent e) {
		  Dialog d= new Dialog(f, "Titulo", false); 
		  d.setVisible(true);
	       } } );

	 b2=new Button("Boton 2");

	 p.setBackground(Color.yellow);
	 
	 //f.setSize(100,100);
	 p.addMouseListener(this);
	 p.addMouseMotionListener(this);
	 f.setBackground(Color.blue);
	 p.add(tf1);
	 p.add(b1);
	 p.add(b2);
	 f.add(p);
	 f.pack();
	 f.setVisible(true);
      } catch(Exception e) {
	 //ClientMsg.mostrarError( this, "Error en inicializacion del Applet" + e.toString());
	 System.err.println("Error inicializando applet: ");
      }
   }

   public void actionPerformed (ActionEvent e )  {
	 tf1.setText("Inicio");
      System.out.println("Master"); 
   }
  
   // MouseListener events
   public void mouseEntered (MouseEvent e )  {
      System.out.println("La pulga entro"); 
   }
   
   public void mousePressed (MouseEvent e )  {
      System.out.println("La pulga se apreto"); 
   }

   public void mouseClicked (MouseEvent e )  {
      System.out.println("La pulga cliqueo"); 
   }


   public void mouseReleased (MouseEvent e )  {
      System.out.println("La pulga se libero"); 
   }

   public void mouseExited (MouseEvent e )  {
      System.out.println("La pulga salto;"); 
   } 

   // MouseMotionListener events
   public void mouseMoved (MouseEvent e )  {
      System.out.println("La pulga se movio"); 
   }
   
   public void mouseDragged (MouseEvent e )  {
      System.out.println("La pulga pillo"); 
   }

}
