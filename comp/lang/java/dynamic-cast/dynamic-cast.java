import java.util.Vector; 

public class k { 
  
 private static Object dynamicCast ( Object obj, Object typeToCastTo) 
   throws ClassNotFoundException, ClassCastException { 
   Class cls = Class.forName ( typeToCastTo.getClass().getName() ); 
   if ( cls.isInstance(obj)) { 
      return obj; 
   } else { 
     throw new ClassCastException(); 
   } 
 } 

 public static void main (String argv[]) { 
   System.out.println("Hola"); 
   try { 
     System.out.println (dynamicCast( "This is a String", new String() )); 
   } catch (ClassNotFoundException e)  { 
     System.out.println ("Clas Not found excp."); 
   } catch (ClassCastException e) { 
      System.out.println ("Clas Cast excp."); 
   } 

   try { 
     System.out.println (dynamicCast( "This is another String", new Vector() )); 
   } catch (ClassNotFoundException e)  { 
     System.out.println ("Clas Not found excp."); 
   } catch (ClassCastException e) { 
      System.out.println ("Clas Cast excp."); 
   } 
   System.out.println("Adios"); 
   } 
} 

