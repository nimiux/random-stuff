/* $Header: AQDemoServlet.java 12-apr-2001.22:28:58 rbhyrava Exp $ */

/* Copyright (c) Oracle Corporation 2001. All Rights Reserved. */

/*
   DESCRIPTION
     AQ Demo Servlet for Apache/Jserv configuration
     The SID, HOST, PORT values should be changed appropriately for the demo to      run.

   PRIVATE CLASSES
    <list of private classes defined - with one-line descriptions>

   NOTES
    <other useful comments, qualifications, etc.>

   MODIFIED    (MM/DD/YY)
    rbhyrava    04/12/01 - Merged rbhyrava_aqxmldemos
    rbhyrava    04/02/01 - Creation
 */

/**
 *  @version $Header: AQDemoServlet.java 12-apr-2001.22:28:58 rbhyrava Exp $
 *  @author  rbhyrava
 *  @since   release specific (what release of product did this appear in)
 */

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import oracle.AQ.*;
import oracle.AQ.xml.*;
import java.sql.*;
import oracle.jms.*;
import javax.jms.*;
import java.io.*;
import oracle.jdbc.pool.*;

/**
 * This is a sample AQ Servlet for Apache/Jserv webserver configuration.
 */
public class AQDemoServlet extends oracle.AQ.xml.AQxmlServlet20
{

  //========================================================================
  /*
   *  getDBDrv - specify the database to which the servlet will connect
   */
  public AQxmlDataSource createAQDataSource() throws AQxmlException
  {
    AQxmlDataSource  db_drv = null;
    String sid = "your_oracle_sid_here";
    String port = "your_oracle_listener_port_here";
    String host = "your_host_name_here";
 
    System.err.println("AQDemoServlet - setting db driver using");
    System.err.println("sid:" + sid + " port:" + port + " host:" + host ) ;

    if (sid.equals("your_oracle_sid_here")) {
       System.err.println("AQDemoServlet:error-change sid,host,port parameters");
    }

    db_drv = new AQxmlDataSource("scott", "tiger", sid, host, port);
    System.err.print("sid: "+ db_drv.getSid() ) ;
    System.err.print(" host: "+ db_drv.getHost() ) ;
    System.err.print(" port: "+ db_drv.getPort() ) ;
    System.err.print(" cachesize: "+ db_drv.getCacheSize() ) ;
    System.err.println(" cachescheme: "+ db_drv.getCacheScheme() ) ;

    return db_drv;
  }

 //========================================================================
  public void init(ServletConfig cfg)
  {
      AQxmlDataSource  db_drv = null;

      try
      {
          super.init(cfg);
          /*
          AQjmsOracleDebug.setLogStream(System.err);
          AQjmsOracleDebug.setTraceLevel(5);
          AQjmsOracleDebug.setDebug(true);
          */

          db_drv = this.createAQDataSource();
          setAQDataSource(db_drv);

          setManualInvalidation(false);
          setSessionMaxInactiveTime(30);
      }
      catch (AQxmlException aq_ex)
      {
        System.out.println("AQ exception: " + aq_ex);
        aq_ex.printStackTrace();

        aq_ex.getNextException().printStackTrace();
      }
      catch (Exception ex)
      {
        System.out.println("Exception: " + ex);
        ex.printStackTrace();
      }
  }
}

