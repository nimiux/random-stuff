import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;

public class Reader {
   public static void main(String args[]) {

      /* Este código lee de la entrada estándar, te vale para los pipes también */
      try {
         BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
         String str = "";
         while (str != null) {
            str = stdin.readLine();
            dosomethingwith(str);
         }
      } catch (IOException e) {
         System.out.println("ERROR");
   } 

   /* Este otro código lee de un fichero que se especifica en el parámtero 1 del programa */
   try {
      File f = new File(args[0]);
      FileInputStream fis = new FileInputStream(f);
      BufferedReader stdin = new BufferedReader(new InputStreamReader(fis));
      String str = "";
      while (str != null) {
         str = stdin.readLine();
         dosomethingwith(str);
      }
   } catch (IOException e) {
      System.out.println("ERROR");
}  
}

static void dosomethingwith (String s) {
    System.out.println("Do s'thing with..."+s);
}

}
