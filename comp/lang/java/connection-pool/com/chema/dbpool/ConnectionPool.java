/*
 * @(#)ConnectionPool.java	1.0 01/12/03
 * 
 */

package com.chema.dbpool;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;
import java.util.Enumeration;

import java.io.PrintWriter;

import com.chema.log.LogWriter;

/**
 * <p>A Pool of database connections.
 * 
 * @version     1.0 10/01/2002
 * @author      Jose Maria Alonso
 */
public class ConnectionPool {

   private String name;
   private String URL;
   private String user;
   private String password;
   private int maxConns;
   private int timeOut;
   private LogWriter logWriter;
   private int checkedOut;
   private Vector freeConnections = new Vector();

   /**
    * Creates a database connection pool.
    * 
    * @param name Name of the Pool.
    * @param URL URL of the database.
    * @param user username of the database.
    * @param password Pasword for the database user.
    * @param maxConns Maximun number of database connections allowed.
    * @param initConns Initial number of database connections.
    * @param timeOut Timeout in seconds for a connection to be established.
    * @param pw PrintWriter for the LogWriter of the connection pool.
    * @param logLevel log level used in the LogWriter.
    */
   public ConnectionPool ( String name
                         , String URL
                         , String user
                         , String password
                         , int maxConns
                         , int initConns
                         , int timeOut
                         , PrintWriter pw
                         , int logLevel)
   {
       this.name = name;
       this.URL = URL;
       this.user = user;
       this.password = password;
       this.maxConns = maxConns;
       this.timeOut = ( timeOut > 0 ? timeOut : 10 );

       logWriter = new LogWriter ( name, logLevel, pw );
       initPool ( initConns );

       logWriter.log ( LogWriter.INFO, "New database pool created" );
       String lf = System.getProperty ( "line.separator" );
       logWriter.log ( LogWriter.DEBUG
                     , lf +
                       " url=" + URL + lf +
                       " user=" + user + lf +
                       " password=" + password + lf +
                       " initconns=" + initConns + lf +
                       " maxconns=" + maxConns + lf +
                       " logintimeout=" + this.timeOut);
       logWriter.log ( LogWriter.DEBUG, getStats());
   }

   /**
    * Initializes a database pool.
    * 
    * @param initConns Initial number of database connections.
    */
   private void initPool ( int initConns )
   {
       for ( int i = 0; i < initConns; i++ )
       {
           try
           {
              Connection pc = newConnection();
              freeConnections.addElement ( pc );
           }
           catch (SQLException e) { }
       }
   }

   /**
    * Gets a database connection from the pool using the default timeout.
    * 
    * @return The connection.
    */
   public Connection getConnection() throws SQLException
   {
       logWriter.log ( LogWriter.DEBUG, "Request for connection received" );
       try
       {
           return getConnection ( timeOut * 1000 );
       }
       catch (SQLException e)
       {
           logWriter.log ( LogWriter.ERROR, e, "Exception getting connection" );
           throw e;
       }
   }

   /**
    * Gets a database connection from the pool using a specific timeout.
    * 
    * @param timeout Timeout in miliseconds
    * @return The connection.
    */
   private synchronized Connection getConnection ( long timeout ) throws SQLException
   {
       // Get a pooled Connection from the cache or a new one.
       // Wait if all are checked out and the max limit has been reached.
       long startTime = System.currentTimeMillis();
       long remaining = timeout;
       Connection conn = null;
       while ( ( conn = getPooledConnection() ) == null )
       {
           try
           {
               logWriter.log ( LogWriter.DEBUG
                             , "Waiting for connection. Timeout=" + remaining );
               wait ( remaining );
          }
          catch (InterruptedException e) { }
          remaining = timeout - (System.currentTimeMillis() - startTime);
          if ( remaining <= 0 )
          {
              // Timeout has expired
              logWriter.log ( LogWriter.DEBUG, "Time-out while waiting for connection" );
              throw new SQLException ( "getConnection() timed-out" );
          }
       }
       // Check if the Connection is still OK
       if ( !isConnectionOK ( conn ) )
       {
            // It was not OK. Try again with the remaining timeout
           logWriter.log ( LogWriter.ERROR, "Removed selected bad connection from pool" );
           return getConnection(remaining);
       }
       checkedOut++;
       logWriter.log ( LogWriter.INFO, "Delivered connection from pool" );
       logWriter.log ( LogWriter.DEBUG, getStats() );
       return conn;
   }

   /**
    * Tests the status of a database connection.
    * 
    * @param The connection
    * @return True id the connection is OK, false otherwise.
    */
   private boolean isConnectionOK ( Connection conn )
   {
       Statement testStmt = null;
       try
       {
           if ( !conn.isClosed() )
           {
               // Try to createStatement to see if it's really alive
               testStmt = conn.createStatement();
               testStmt.close();
           } else {
               return false;
           }
       } catch ( SQLException e )
       {
           if ( testStmt != null )
           {
              try
              {
                 testStmt.close();
              } catch ( SQLException se ) { }
           }
           logWriter.log(LogWriter.ERROR, e, "Pooled Connection was not okay" );
           return false;
       }
       return true;
   }

   /**
    * Retrieves a database connection from the pool.
    * 
    * @return The connection.
    */

   private Connection getPooledConnection() throws SQLException
   {
       Connection conn = null;
       if ( freeConnections.size() > 0 )
       {
          // Pick the first Connection in the Vector to get round-robin usage
          conn = (Connection) freeConnections.firstElement();
          freeConnections.removeElementAt(0);
       } else if ( ( maxConns == 0 ) || ( checkedOut < maxConns ) )
       {
          conn = newConnection();
       }
       return conn;
   }

   /**
    * Creates a new database connection.
    * 
    * @return The connection.
    */
   private Connection newConnection() throws SQLException
   {
       Connection conn = null;
       if (user == null) {
          conn = DriverManager.getConnection ( URL );
       }
       else {
          conn = DriverManager.getConnection ( URL, user, password );
       }
       logWriter.log ( LogWriter.INFO, "Opened a new connection" );
       return conn;
   }

   /**
    * Frees a database connection.
    * 
    * @param Connection to be freed
    */
   public synchronized void freeConnection(Connection conn)
   {
       // Put the connection at the end of the Vector
       freeConnections.addElement ( conn );
       checkedOut--;
       notifyAll();
       logWriter.log ( LogWriter.INFO, "Returned connection to pool" );
       logWriter.log ( LogWriter.DEBUG, getStats() );
   }

   /**
    * Releases the database pool, freeing all connections.
    * 
    */
   public synchronized void release()
   {
       Enumeration allConnections = freeConnections.elements();
       while ( allConnections.hasMoreElements() )
       {
           Connection con = (Connection) allConnections.nextElement();
           try
           {
               con.close();
               logWriter.log(LogWriter.INFO, "Closed connection");
           } catch (SQLException e)
           {
               logWriter.log(LogWriter.ERROR, e, "Couldn't close connection" );
           }
       }
       freeConnections.removeAllElements();
   }

   /**
    * Gets Pool Statistics.
    * 
    * @return String containing statistics.
    */
   private String getStats() 
   {
      return "Total connections: " + ( freeConnections.size() + checkedOut ) +
             " Available: " + freeConnections.size() +
             " Checked-out: " + checkedOut;
   }
}