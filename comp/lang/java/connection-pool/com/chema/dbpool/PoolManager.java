/*
 * @(#)PoolManager.java	1.0 01/12/03
 * 
 */

package com.chema.dbpool;

import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;
import java.io.PrintWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.Hashtable;
import java.util.Vector;
import java.util.Properties;
import java.util.Enumeration;
import java.util.StringTokenizer;

import com.chema.log.LogWriter;
import com.chema.conf.Resource;
import com.chema.conf.Constants;

/**
 * <p>Manages database pool connections.
 *
 * @version     1.0 10/01/2002
 * @author      Jose Maria Alonso
 */
public class PoolManager implements Constants {

   static private PoolManager instance;
   static private int clients;

   private LogWriter logWriter;
   private PrintWriter pw;

   private Vector drivers = new Vector();
   private Hashtable pools = new Hashtable();

   /**
    * Creates a <code>PoolManager</code>.
    * 
    */
   private PoolManager()
   {
       init();
   }

   /**
    * Gets an unique PoolManager Instance.
    *
    * @return The PoolManager Instance.
    */
   static synchronized public PoolManager getInstance()
   {
       if ( instance == null )
       {
          instance = new PoolManager();
       }
       clients++;
       return instance;
   }
   /**
    * Initializes the PoolManager.
    * 
    */
 
   private void init()
   {
       // Log to System.err until we have read the logfile property
       pw = new PrintWriter ( System.err, true );
       logWriter = new LogWriter ("PoolManager", LogWriter.INFO, pw);
       InputStream is = getClass().getResourceAsStream ( 
                              Resource.load ( Constants.DatabaseProperties ) );
       Properties dbProps = new Properties();
       try
       {
           dbProps.load ( is );
       }
       catch (Exception e)
       {
           logWriter.log ( LogWriter.ERROR
                         , "Can't read the properties file. " + 
                           "Make sure db.properties is in your CLASSPATH" );
           return;
       }
      String logFile = dbProps.getProperty ( "logfile" );
      if ( logFile != null )
      {
           try
           {
               pw = new PrintWriter(new FileWriter(logFile, true), true);
               logWriter.setPrintWriter(pw);
           }
           catch (IOException e)
           {
               logWriter.log ( LogWriter.ERROR
                             , "Can't open the log file: " + logFile +
                               ". Using System.err instead" );
           }
      }
      loadDrivers ( dbProps );
      createPools ( dbProps );
   }

   /**
    * Loads Database drivers from Properties.
    * 
    * @param props Properties.
    */
   private void loadDrivers ( Properties props )
   {
       String driverClasses = props.getProperty ( "drivers" );
       StringTokenizer st = new StringTokenizer ( driverClasses );
       while ( st.hasMoreElements() )
       {
           String driverClassName = st.nextToken().trim();
           try
           {
               Driver driver = (Driver) 
                                Class.forName ( driverClassName ).newInstance();
               DriverManager.registerDriver ( driver );
               drivers.addElement ( driver );
               logWriter.log ( LogWriter.INFO
                             , "Registered JDBC driver " + driverClassName );
           }
           catch (Exception e)
           {
               logWriter.log ( LogWriter.ERROR, e
                           , "Can't register JDBC driver: " + driverClassName );
           }
       } 
   } 

   /**
    * Creates a Database Pool from Properties.
    * 
    * @param props Properties.
    */
   private void createPools ( Properties props )
   {
       Enumeration propNames = props.propertyNames();
       while ( propNames.hasMoreElements() )
       {
           String name = (String) propNames.nextElement();
           if ( name.endsWith ( ".url" ) )
           {
               String poolName = name.substring ( 0, name.lastIndexOf(".") );
               String url = props.getProperty(poolName + ".url");
               if (url == null)
               {
                   logWriter.log ( LogWriter.ERROR
                                 , "No URL specified for " + poolName );
                   continue;
               }
               String user = props.getProperty(poolName + ".user");
               String password = props.getProperty(poolName + ".password");
               String maxConns = props.getProperty(poolName + ".maxconns", "0");
               int max;
               try
               {
                   max = Integer.valueOf(maxConns).intValue();
               }
               catch (NumberFormatException e)
               {
                   logWriter.log ( LogWriter.ERROR
                                 , "Invalid maxconns value " + maxConns + " for " + 
                                   poolName );
                   max = 0;
               }
               String initConns = props.getProperty ( poolName + ".initconns", "0" );
               int init;
               try
               {
                   init = Integer.valueOf ( initConns ).intValue();
               }
               catch (NumberFormatException e)
               {
                   logWriter.log ( LogWriter.ERROR
                                 , "Invalid initconns value " + initConns +
                               " for " + poolName );
                   init = 0;
               }
               String loginTimeOut = props.getProperty ( poolName + ".logintimeout", "5" );
               int timeOut;
               try
               {
                   timeOut = Integer.valueOf ( loginTimeOut ).intValue();
               } catch (NumberFormatException e)
               {
                   logWriter.log ( LogWriter.ERROR
                                 , "Invalid logintimeout value " + loginTimeOut +
                                   " for " + poolName );
                   timeOut = 5;
               } 
               String logLevelProp = props.getProperty ( poolName + ".loglevel"
                                                , String.valueOf ( LogWriter.ERROR ) );
               int logLevel = LogWriter.INFO;
               if ( logLevelProp.equalsIgnoreCase ( "none" ) )
               {
                   logLevel = LogWriter.NONE;
               }
               else if ( logLevelProp.equalsIgnoreCase ( "error" ) )
               {
                   logLevel = LogWriter.ERROR;
               }
               else if ( logLevelProp.equalsIgnoreCase ( "debug" ) )
               {
                   logLevel = LogWriter.DEBUG;
               }
               ConnectionPool pool = new ConnectionPool ( poolName
                                                        , url
                                                        , user
                                                        , password
                                                        , max
                                                        , init
                                                        , timeOut
                                                        , pw
                                                        , logLevel);
               pools.put(poolName, pool);
           }
       }
   } 

   /**
    * Returns a connection from a database pool.
    * 
    * @param name Pool�s name.
    * @return a connection
    */
   public Connection getConnection ( String name )
   {
       Connection conn = null;
       ConnectionPool pool = (ConnectionPool) pools.get ( name );
       if ( pool != null )
       {
           try
           {
               conn = pool.getConnection();
           }
           catch (SQLException e)
           {
               logWriter.log ( LogWriter.ERROR, e
                             , "Exception getting connection from " + name );
           }
       }
       return conn;
   }

   /**
    * Frees a connection from a database pool.
    * 
    * @param name Pool�s name.
    * @param con The connection to be freed.
    */
   public void freeConnection ( String name, Connection con )
   {
       ConnectionPool pool = (ConnectionPool) pools.get ( name );
       if ( pool != null )
       {
           pool.freeConnection(con);
       }
   }

   /**
    * Releases all database pools.
    * 
    */
   public synchronized void release()
   {
       // Wait until called by the last client
       if ( --clients != 0 )
       {
          return;
       }
       Enumeration allPools = pools.elements();
       while ( allPools.hasMoreElements() )
       {
           ConnectionPool pool = (ConnectionPool) allPools.nextElement();
           pool.release();
       }
       Enumeration allDrivers = drivers.elements();
       while ( allDrivers.hasMoreElements() )
       {
           Driver driver = (Driver) allDrivers.nextElement();
           try
           {
               DriverManager.deregisterDriver(driver);
               logWriter.log ( LogWriter.INFO
                             , "Deregistered JDBC driver " + 
                               driver.getClass().getName() );
           }
           catch (SQLException e)
           {
               logWriter.log ( LogWriter.ERROR, e
                             , "Couldn't deregister JDBC driver: " +
                               driver.getClass().getName() );
           }
       }
   }
}