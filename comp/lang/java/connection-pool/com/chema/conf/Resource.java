/*
 * @(#)Resource.java	1.0 01/12/03
 * 
 */

package com.chema.conf;

import java.io.InputStream;
import java.util.Properties;
import com.chema.log.LogException;

/**
 * <p>Manager to access system resources.
 * 
 * @version     1.0 10/01/2002
 * @author      Jose Maria Alonso
 */
public class Resource implements Constants {

   private Properties _rpProps;

   /**
    * Creates a Manager using default resource.
    * 
    * @param name Name of the Pool.
    */
   public Resource()
   {
       init(Constants.ApplicationResource);
   }

   /**
    * Creates a Manager using a resource's name.
    * 
    * @param name Name of the Resource.
    */
   public Resource(String S)
   {
       init(S);
   }

   /**
    * Retrieves an integer key value Property.
    * 
    * @param key Key
    * @return Key value.
    */
   public String loadRes(int key)
   {
       Integer i = new Integer(key);
       return _rpProps.getProperty(i.toString());
   }

   /**
    * Retrieves a String key value Property.
    * 
    * @param key Key
    * @return Key value.
    */
   public String loadRes(String key)
   {
       return _rpProps.getProperty(key);
   }

   /**
    * Inits Manager Properties using name of resource.
    * 
    * @param S Name of the Resource
    */
   private void init(String S)
   {
      InputStream is = Resource.class.getResourceAsStream(S);
      _rpProps = new Properties();

      try {
         _rpProps.load(is);
      }
      catch (Exception e) {
         LogException.log(e,this.getClass().getName() + "::init()");
      }
   }

   /**
    * Retrieves a String key value from Resource.
    * 
    * @param S Name of the Resouce
    * @param key Key
    * @return Key value.
    */
   private static String _load(String S, String key)
   {
      InputStream is = Resource.class.getResourceAsStream(S);
      Properties _rpProps = new Properties();

      try {
         _rpProps.load(is);
      }
      catch (Exception e) {
         LogException.log(e,"Resource::_load(String,String)");
         return "";
      }
      String result =  _rpProps.getProperty(key);
      if (result == null) {
         LogException.msg("Resource::_load(String,String)\n\tKey not found: " + key + "\n\tFile: " + S);
      }
      return result;
   }

   /**
    * Retrieves an integer key value from Default Resource.
    * 
    * @param key Key
    * @return Key value.
    */
   public static String load(int i)
   {
       Integer ii = new Integer(i);
       return _load(Constants.ApplicationResource, ii.toString());
   }

   /**
    * Retrieves an Integer key value from Resource.
    * 
    * @param S Name of resource
    * @param i Key
    * @return Key value.
    */
   public static String load(String S, int i)
   {
       Integer ii = new Integer(i);
       return _load(S,ii.toString());
   }

   /**
    * Retrieves a String key value from Default Resource.
    * 
    * @param S Name of resource
    * @param i Key
    * @return Key value.
    */
   public static String load(String key)
   {
       return _load(ApplicationResource,key);
   }

   /**
    * Retrieves a String key value from Resource.
    * 
    * @param S Name of the Resouce
    * @param key Key
    * @return Key value.
    */
   public static String load(String S, String key)
   {
       return _load(S,key);
   }

}