/*
 * @(#)Constants.java	1.0 01/12/03
 * 
 */

package com.chema.conf;

/**
 * <p>Application wide constants.
 * 
 * @version     1.0 10/01/2002
 * @author      Jose Maria Alonso
 */
public interface Constants {
   public static final String ApplicationResource = "/conf/app.properties";
   public static final String Database = "Database";
   public static final String DatabaseProperties = "DatabaseProperties";
//   public static final String StdRepProperties = "StdRepProperties";
//   public static final String BaseQR = "BaseQR";
   public static final String ExceptionLog = "ExceptionLog";
}