/*
 * @(#)LogException.java	1.0 01/12/03
 * 
 */

package com.chema.log;

import java.io.PrintWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.sql.SQLException;

import com.chema.conf.Constants;
import com.chema.conf.Resource;

/**
 * <p>Logs Exceptions.
 * 
 * @version     1.0 10/01/2002
 * @author      Jose Maria Alonso
 */
public class LogException implements Constants
{

   /**
    * Writes a message to a default log file.
    * 
    * @param msg Message
    */
    public static void msg(String msg)
    {
        PrintWriter pw;
        String sLog = Resource.load(Constants.ExceptionLog);
        if (sLog == null || sLog == "")
           sLog = "/exp.txt";
        try {
           pw = new PrintWriter(new FileWriter(sLog, true), true);
           pw.println(msg);
           pw.flush();
        }
        catch(IOException e) {
        }
        pw = null;
    }

   /**
    * Logs an exception to a default log file.
    * 
    * @param e Exception
    * @param msg Message preceeding the exception log.
    */
    private static void _log(Exception e, String msg)
    {
        PrintWriter pw;
        String sLog = Resource.load(Constants.ExceptionLog);
        if (sLog == null || sLog == "")
           sLog = "/exp.txt";
        try {
           pw = new PrintWriter(new FileWriter(sLog, true), true);
           pw.println(msg + e.getMessage());
           e.printStackTrace(pw);
           pw.flush();

        }
        catch(IOException ex) {
        }

        pw = null;
    }

   /**
    * Logs an exception.
    * 
    * @param e Exception
    */
    public static void log(Exception e)
    {
        _log(e,"[" + new Date() + "]  ");
    }

   /**
    * Logs an exception.
    * 
    * @param e Exception
    * @param msg Message after the exception log.
    */
    public static void log(Exception e, String msg)
    {
        _log(e,"[" + new Date() + "]  " + msg + " - ");
    }

   /**
    * Logs an SQL exception.
    * 
    * @param e Exception
    */
    public static void log(SQLException e)
    {
        SQLException eNext;
        String sNext = "";
        if ((eNext = e.getNextException()) != null)
           sNext = "\n\tNext exception: " + eNext.getMessage() +
                   "\n\tNext SQL state: " + eNext.getSQLState() +
                   "\n\tNext Error code: " + eNext.getErrorCode();
        _log(e,"[" + new Date() + "]  \n\tSQL state: " +
               e.getSQLState() +
               "\n\tError code:" + e.getErrorCode() + sNext);
    }


   /**
    * Logs an SQL Exception.
    * 
    * @param e Exception.
    * @return msg Message before the exception log.
    */
    public static void log(SQLException e, String msg)
    {
        SQLException eNext;
        String sNext = "";
        if ((eNext = e.getNextException()) != null)
           sNext = "\n\tNext exception: " + eNext.getMessage() +
                   "\n\tNext SQL state: " + eNext.getSQLState() +
                   "\n\tNext Error code: " + eNext.getErrorCode();

        _log(e,"[" + new Date() + "]  " +
               msg +
               " - \n\tSQL state: " + e.getSQLState() +
               "\n\tError code:" + e.getErrorCode() + sNext);
    }
}