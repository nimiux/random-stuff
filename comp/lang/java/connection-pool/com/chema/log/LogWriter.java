/*
 * @(#)LogWriter.java	1.0 01/12/03
 * 
 */

package com.chema.log;

import java.io.PrintWriter;
import java.util.Date;
import java.io.StringWriter;

/**
 * <p>Manages log messages.
 * 
 * @version     1.0 10/01/2002
 * @author      Jose Maria Alonso
 */
public class LogWriter {

   public static final int NONE  = 0;
   public static final int ERROR = 1;
   public static final int INFO  = 2;
   public static final int DEBUG = 3;

   private static final String ERROR_TEXT = "Error";
   private static final String INFO_TEXT  = "Info";
   private static final String DEBUG_TEXT = "Debug";

   private PrintWriter pw;
   private String owner;
   private int logLevel;

   /**
    * Creates a log writer.
    * 
    * @param owner Owner of the writer.
    * @param logLevel Severity log level of the writer.
    * @param pw PrintWriter to be used to log messages.
    */
   public LogWriter ( String owner, int logLevel, PrintWriter pw )
   {
       this.pw = pw;
       this.owner = owner;
       this.logLevel = logLevel;
   }

   /**
    * Creates a log writer without a PrintWriter.
    * 
    * @param owner Owner of the writer.
    * @param logLevel Severity log level of the writer.
    */
   public LogWriter ( String owner, int logLevel )
   {
       this ( owner, logLevel, null );
   }

   /**
    * Retrieves the severity log level.
    * 
    * @return The severity level.
    */
   public int getLogLevel()
   {
       return logLevel;
   } 

   /**
    * Retrieves the PrintWriter.
    * 
    * @return The PrintWriter.
    */
   public PrintWriter getPrintWriter()
   {
       return pw;
   }

   /**
    * Sets the log level.
    * 
    * @param logLevel log level.
    */
   public void setLogLevel ( int logLevel )
   {
       this.logLevel = logLevel;
   }

   /**
    * Sets the PrintWriter.
    * 
    * @param pw The PrintWriter.
    */
   public void setPrintWriter ( PrintWriter pw )
   {
       this.pw = pw;
   }

   /**
    * Logs a message.
    * 
    * @param severityLevel Severiry level of the message. 
    * @param msg Message to be logged.
    */
   public void log ( int severityLevel, String msg )
   {
       if ( pw != null )
       { 
           if (severityLevel <= logLevel)
           {
              pw.println ( "[" + new Date() + "]  " +
                           getSeverityString(severityLevel) + 
                           ": " + owner + ": " + msg);
           }
       }
   }

   /**
    * Logs a message including the trace of an exception.
    * 
    * @param severityLevel Severiry level of the message. 
    * @param t Exception to be traced.
    * @param msg Message to be logged.
    */
   public void log ( int severityLevel, Throwable t, String msg )
   {
       log ( severityLevel, msg + " : " + toTrace(t) );
   }

   /**
    * Gets the severity String of the input severity level.
    * 
    * @param severityLevel Severiry level. 
    * @return Severity String.
    */
   private String getSeverityString ( int severityLevel )
   {
       switch ( severityLevel )
       {
           case ERROR: return ERROR_TEXT;
           case INFO:  return INFO_TEXT;
           case DEBUG: return DEBUG_TEXT;
           default:    return "Unknown";
       }
   }

   /**
    * Gets the trace String of an exception.
    * 
    * @param e The exception. 
    * @return Trace of the exception.
    */
   private String toTrace ( Throwable e )
   {
       StringWriter sw = new StringWriter();
       PrintWriter pw = new PrintWriter( sw );
       e.printStackTrace( pw );
       pw.flush();
       return sw.toString();
   }
}