/* Recuperaci�n de propiedades de Sistema desde un programa Java 
   Estas propiedades se pasan como 
     java -Dprop=value 
   en la m�qina virtual */

public class checkProp {
    public static void main(String[] args) 
    {
       System.out.println("Hola Nota. La propiedad weblogic.kk vale:");
       String a = System.getProperty("weblogic.kk");
       if (a == null)
       {
          a = "Not set.";
       }
       System.out.println(a);
       for (int i = 0; i < args.length; i++)
         System.out.print(i == 0 ? args[i] : " " + args[i]);
       System.out.println();
    }
}
