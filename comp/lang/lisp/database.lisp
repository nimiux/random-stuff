(defmacro add-record (record database)
  `(push ,record ,database))

(defmacro dump-db (db)
  `(format t "Contents of the database:~%~{~a~%~}" ,db))

(defun save-db (db filename)
  (with-open-file (out filename
                  :direction :output
                  :if-exists :supersede)
                  (with-standard-io-syntax
                   (princ db out)))) 

(defun load-db (db filename)
  (with-open-file (in filename)
                  (with-standard-io-syntax
                   (setf db (read in)))))

(defstruct (cd
  (:print-function
   (lambda (struct stream depth)
           (declare (ignore depth))
           (format stream
                   "~{~a:~10t~a~%~}"
                   (list :title (cd-title struct)
                         :artist (cd-artist struct)
                         :rating (cd-rating struct)
                         :ripped (cd-ripped struct))))))
  title
  artist
  (rating 0)
  (ripped nil))

(setf mycd (make-cd :title "A" :artist "B" :rating 10))

(defparameter *db* nil)
(add-record (make-cd :title "A" :artist "1" :rating 3 :ripped t) *db*)
(add-record (make-cd :title "B" :artist "2" :rating 2 :ripped t) *db*)
(add-record (make-cd :title "C" :artist "3" :rating 5 :ripped t) *db*)

(dump-db *db*)
(save-db *db* "cd.database")
(setf *mydb* nil)
(load-db *mydb* "cd.database")
