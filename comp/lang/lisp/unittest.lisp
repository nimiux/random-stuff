(defvar *test-name* nil)

(defmacro deftest (name parameters &body body)
  "Define a test function. Within a test function we can call
   other test functions or use 'check' to run individual test
   cases."
  `(defun ,name ,parameters
     (let ((*test-name* (append *test-name* ,name)))
       ,@body)))

(defmacro combine-results (&body forms)
  "Combine the results (as booleans) of evaluating 'forms' in order"
  (with-gensyms (result)
    `(let ((result t))
       ,@(loop for f in forms collect `(unless ,f (setf result nil)))
       result)))

(defmacro check (&body forms)
  "Runs each expression in 'forms' as a test case. "
  `(combine-results
     ,@(loop for f in forms collect `(report-result ,f ',f))))

(defun report-result (result form)
  "Report the result of a single test case. Called by 'check'"
  (format t "~:[FAIL~;pass~] ... ~a: ~a~%" result *test-name* form)
  result
  )

(deftest test-+ ()
  (check
    (= (+ 1 2) 3)
    (= (+ 1 2 3) 6)))

(test-+)

(deftest test-* ()
  (check
    (= (* 1 2) 3)
    (= (* 1 2 3) 6)))

(macroexpand-1 '(check
    (= (+ 1 2) 3)
    (= (+ 1 2 3) 6)
    (= (+ 1 2 3 4) 10)))


(

(deftest test-arithmethic ()
  (combine-results
    (test-+)
    (test-*)))
  )

(test-arithmethic)
