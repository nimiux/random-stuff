(output-stream-p *standard-output*)
(write-char #\x *standard-output*)
(input-stream-p *standard-input*)
(read-char *standard-input*)
(with-open-file (my-stream "data.txt" :direction :output)
  (print "my data" my-stream))
(with-open-file (my-stream "data.txt" :direction :input)
  (read my-stream))
(let ((animal-noises '((dog . woof) (cat . meow))))
  (with-open-file (my-stream "animal-noises.txt" :direction :output)
                  (print animal-noises my-stream)))

; vim: set ts=4 :
