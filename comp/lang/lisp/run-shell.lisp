(require :sb-posix)

(define-alien-routine "execl" int
(path c-string)
(arg0 c-string)
(arg1 c-string))

(defun run-shell ()
(let ((pid (sb-posix:fork)))
     (if (= pid 0)
       (execl "/bin/sh" "/bin/sh" nil)
       (let ((status (make-array 1 :element-type '(signed-byte 32))))
            (sb-posix:waitpid pid 0 status)))))

; vim: set ts=4 :
