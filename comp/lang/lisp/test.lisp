(defun a (n &rest body)
  (list n body))

(defun b (n &optional (m 3 m-supplied-p) &rest s)
  (list n m m-supplied-p s))

(defun maxpair (n)
  (dotimes (i n)
    (dotimes (j 10)
      (when (> (* i j) n)
	(return-from maxpair (list i j))))))
