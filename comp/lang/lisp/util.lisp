;;;; Lisp utils
;;;; Most taken from Paul Graham's On Lisp

;;; Lists
(proclaim '(inline last1 single append1 conc1 mklist))

;; returns the last element in a list
(defun last1 (lst)
  (car (last lst)))

;; tests whether something is a list of one element
(defun single (lst)
  (and (consp lst) (not (cdr lst))))

;; attaches an element to the end of a list
(defun append1 (lst obj)
  (append lst (list obj)))

;; attaches an element to the end of a list, destructively
(defun conc1 (lst obj)
  (nconc lst (list obj)))

;; ensures that something is a list
(defun mklist (obj)
  (if (listp obj) obj (list obj)))

;; compares two sequences and returns t only if the first is longer
(defun longer (x y)
  (labels ((compare (x y)
             (and (consp x)
                  (or (null y)
                      (compare (cdr x) (cdr y))))))
    (if (and (listp x) (listp y))
      (compare x y)
      (> (length x) (length y)))))

;; returns what some would have returned for successive cdrs of the list
(defun filter (fn lst)
  (let ((acc nil))
    (dolist (x lst)
      (let ((val (funcall fn x)))
        (if val (push val acc))))
    (nreverse acc)))

;; returns a new list in which the elments of the list are grouped into sublists of length n
(defun group (source n)
  (if (zerop n) (error "zero length"))
  (labels ((rec (source acc)
             (let ((rest (nthcdr n source)))
               (if (consp rest)
                 (rec rest (cons (subseq source 0 n) acc))
                 (nreverse (cons source acc))))))
    (if source
        (rec source nil)
        nil)))

;; returns a list of all the atoms that are elements of a list, or elements of its elements, and so on
(defun flatten (x)
  (labels ((rec (x acc)
             (cond ((null x) acc)
                   ((atom x) (cons x acc))
                   (t (rec (car x) (rec (cdr x) acc))))))
    (trace rec)
    (rec x nil)))

#+nil
(flatten '(some (test data)))

;; removes every leaf for which the function returns true
(defun prune (test tree)
  (labels ((rec (tree acc)
             (cond ((null tree) (nreverse acc))
                   ((consp (car tree))
                    (rec (cdr tree)
                         (cons (rec (car tree) nil) acc)))
                   (t (rec (cdr tree)
                           (if (funcall test (car tree))
                             acc
                             (cons (car tree) acc)))))))
    (rec tree nil)))

;;; Search

;; returns the first element of a list and the function applied to this element which the function returns t
(defun find2 (fn lst)
  (if (null lst)
    nil
    (let ((val (funcall fn (car lst))))
      (if val
        (values (car lst) val)
        (find2 fn (cdr lst))))))

;; returns true y first element happens before second in the list
(defun before (x y lst &key (test #'eql))
  (and lst
       (let ((first (car lst)))
         (cond ((funcall test y first) nil)
               ((funcall test x first) lst)
               (t (before x y (cdr lst) :test test))))))

(defun after (x y lst &key (test #'eql))
  (let ((rest (before y x lst :test test)))
    (and rest (member x rest :test test))))

;; returns the cdr of list from second occurence of obj
(defun duplicate (obj lst &key (test #'eql))
  (member obj
          (cdr (member obj lst :test test))
          :test test))

;; splist list in two halves using fn to test the point
(defun split-if (fn lst)
  (let ((acc nil))
    (do ((src lst (cdr src)))
      ((or (null src) (funcall fn (car src)))
       (values (nreverse acc) src))
      (push (car src) acc))))

;; takes a list and a scoring function and returns the element with the highest score
(defun most (fn lst)
  (if (null lst)
    (values nil nil)
    (let* ((wins (car lst))
           (max (funcall fn wins)))
      (dolist (obj (cdr lst))
        (let ((score (funcall fn obj)))
          (when (> score max)
            (setq wins obj
                  max score))))
      (values wins max))))

;; takes a predicate of two arguments and a list and returns the element which, according to the predicate beats all the others
(defun best (fn lst)
  (if (null lst)
    nil
    (let ((wins (car lst)))
      (dolist (obj (cdr lst))
        (if (funcall fn obj wins)
          (setq wins obj)))
      wins)))

;; takes a function and a list and returns a list of all elements for which the function yields the highest score
(defun mostn (fn lst)
  (if (null lst)
    (values nil nil)
    (let ((result (list (car lst)))
          (max (funcall fn (car lst))))
      (dolist (obj (cdr lst))
        (let ((score (funcall fn obj)))
          (cond ((> score max)
                 (setq max score
                       result (list obj)))
                ((= score max)
                 (push obj result)))))
      (values (nreverse result) max))))

;; Mappings

;; Applies a function to a range of integers
(defun mapa-b (fn a b &optional (step 1))
  (do ((i a (+ i step))
       (result nil))
    ((> i b) (nreverse result))
    (push (funcall fn i) result)))

;; Applies a function to a range of integers starting with 0
(defun map0-n (fn n)
  (mapa-b fn 0 n))

;; Applies a function to a range of integers starting with 1
(defun map1-n (fn n)
  (mapa-b fn 1 n))

;; More general mapping which receives next value and end function
(defun map-> (fn start test-fn succ-fn)
  (do ((i start (funcall succ-fn i))
       (result nil))
    ((funcall test-fn i) (nreverse result))
    (push (funcall fn i) result)))

;; Non destructive alternative to mapcan
(defun mappend (fn &rest lsts)
  (apply #'append (apply #'mapcar fn lsts)))

;; Mapcar a function over several lists
;; (mapcars #sqrt lst1 lst2) ~ (mapcar #'sqrt (append lst1 lst2))
(defun mapcars (fn &rest lsts)
  (let ((result nil))
    (dolist (lst lsts)
      (dolist (obj lst)
        (push (funcall fn obj) result)))
    (nreverse result)))

;; Recursive mapcar for trees whatever mapcar does for lists rmapcar does for trees
(defun rmapcar (fn &rest args)
  (if (some #'atom args)
    (apply fn args)
    (apply #'mapcar
           #'(lambda (&rest args)
               (apply #'rmapcar fn args))
           args)))

;; I/O

;; Reads a line of input and returns a list
(defun readlist (&rest args)
  (values (read-from-string
            (concatenate 'string "("
                         (apply #'read-line args)
                         ")"))))

;; Prompts for value. Same arguments as format except the initial stream argument
(defun prompt (&rest args)
  (apply #'format *query-io* args)
  (read *query-io*))
;; Your own repl
(defun break-loop (fn quit &rest args)
  (format *query-io* "Entering break-loop.~%")
  (loop
    (let ((in (apply #'prompt args)))
      (if (funcall quit in)
        (return)
        (format *query-io* "~A~%" (funcall fn in))))))

;; Symbols and strings
;; Converts arguments to string
(defun mkstr (&rest args)
  (with-output-to-string (s)
    (dolist (a args) (princ a s))))

;; Takes arguments and returns the symbol
(defun symb (&rest args)
  (values (intern (apply #'mkstr args))))
;; Takes a series of objects and prints and rereads them. It's a generalization of symb

(defun reread (&rest args)
  (values (read-from-string (apply #'mkstr args))))
;; Takes a symbol and returns a list of symbols made from the characters in its name
(defun explode (sym)
  (map 'list #'(lambda (c)
                 (intern (make-string 1 :initial-element c)))
       (symbol-name sym)))

;; Functions
;; Function composition
(defun compose (&rest fns)
  (if fns
    (let ((fn1 (car (last fns)))
          (fns (butlast fns)))
      #'(lambda (&rest args)
                 (reduce #'funcall fns
                         :from-end t
                         :initial-value (apply fn1 args))))
    #'identity))

;; Function complement
;;(defun complement (fn)
;;  #'(lambda (&rest args) (not (apply fn args))))
(defun complement (pred)
  (compose #'not pred))

;; Takes a function an returns a memoize version of it
(defun memoize (fn)
  (let ((cache (make-hash-table :test #'equal)))
    #'(lambda (&rest args)
        (multiple-value-bind (val win) (gethash args cache)
          (if win
              val
              (setf (gethash args cache)
                         (apply fn args)))))))

;; Function if
(defun fif (if then &optional else)
  #'(lambda (x)
            (if (funcall if x)
              (funcall then x)
              (if else (funcall else x)))))
 
;; Function union an intersection are very similar, implement them by means of a macro
;; Function intersection
(defun fint (fn &rest fns)
  (if (null fns)
    fn
    (let ((chain (apply #'fint fns)))
      #'(lambda (x)
                (and (funcall fn x) (funcall chain x))))))

;; Function union
(defun fun (fn &rest fns)
  (if (null fns)
    fn
    (let ((chain (apply #'fun fns)))
      #'(lambda (x)
                (or (funcall fn x) (funcall chain x))))))

;; List recurser
;; the first argument must be a fn of two args: the car of the list and
;; the fn which can be called to continue recursion
(defun lrec (rec &optional base)
  (labels ((self (lst)
             (if (null lst)
               (if (functionp base)
                 (funcall base)
                 base)
               (funcall rec (car lst)
                        #'(lambda ()
                                  (self (cdr lst)))))))
    #'self))

;; Tree transverse
(defun ttrav (rec &optional (base #'identity))
  (labels ((self (tree)
             (if (atom tree)
               (if (functionp base)
                 (funcall base tree)
                 base)
               (funcall rec (self (car tree))
                        (if (cdr tree)
                          (self (cdr tree)))))))
    #'self))

;; Tree recursion
(defun trec (rec &optional (base #'identity))
  (labels
    ((self (tree)
       (if (atom tree)
         (if (functionp base)
           (funcall base tree)
           base)
         (funcall rec tree
                  #'(lambda ()
                            (self (car tree)))
                  #'(lambda ()
                            (if (cdr tree)
                              (self (cdr tree))))))))
    #'self))

;;; Macros
(defmacro nlet (n letargs &rest body)
  `(labels ((,n ,(mapcar #'car letargs)
              ,@body))
     (,n ,@(mapcar #'cadr letargs))))

;; example
; (defun nlet-fact (n)
;   (nlet fact ((n n))
;         (if (zerop n)
;           1
;           (* n (fact (1- n))))))
; 
; (nlet-fact 3)
