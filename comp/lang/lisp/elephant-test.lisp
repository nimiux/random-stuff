(require :bordeaux-threads)
(require :elephant)
(require :ele-bdb)
(require :postmodern)
(require :ele-postmodern)

(in-package :cl-user)
(use-package :elephant)

;;(defpackage :mypack
;;  (:use :cl :elephant)
;;  (:import-from :css-lite :css)
;;  (:import-from :cl-json :encode-json-to-string))
;;
;;(in-package :mypack)

;; Postmodern test
;;(postmodern:connect-toplevel "test" "test" "test" "localhost")

;; Launch Elephant
;(defparameter *store* (elephant:open-store '(:bdb "/tmp/test.db")))
(defparameter *store* (open-store 
  '(:postmodern (:postgresql "localhost" "test" "test" "test" :port 5432))))

(add-to-root "my key" "my value")
(remove-from-root "my key")

(defclass my-persistent-class ()
  ((slot1 :accessor slot1)
   (slot2 :accessor slot2))
  (:metaclass persistent-metaclass))

(defparameter foo (make-instance 'my-persistent-class))

(add-to-root "foo" foo)
(add-to-root "bar" foo)

(eq (get-from-root "foo")
    (get-from-root "bar"))

(map-root (lambda (k v) (format t "key: ~a value: ~a~%" k v)))

(flush-instance-cache *store*)

