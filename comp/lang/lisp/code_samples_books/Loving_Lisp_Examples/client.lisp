(defun open-socket (host port)
  (socket-connect port host))
    
(defun client (server port a-string)
  ;; Open connection
  (let ((socket (open-socket server port)))
    (unwind-protect
	(progn
	  (format socket "~A~%" a-string)
	  (force-output socket)
	  (let ((response (read-line socket)))
	    (format t "Response from server: ~A~%" response))))
    ;; Close socket before exiting.
    (close socket)))

(defun test ()
  (client "localhost" 8000 "test string to send to server"))

