
(defvar *host-db* nil)

(defvar *host-db-filename* (or (second sb-ext:*posix-argv*) #p"/tmp/kk.db"))

(defun make-host (name dns user &optional (port 22) (pass nil))
  (list :name name :dns dns :port port :user user :pass pass))

(defun add-host (host)
  (push host *host-db*))

(defun dump-host-db ()
    (format t "~{~{~a:~10t~a~%~}~%~}~%" *host-db*))

(defun prompt-read (prompt)
  (format *query-io* "~a: " prompt)
  (force-output *query-io*)
  (read-line *query-io*))

(defun prompt-choice (prompt choices)
  (let* ((choiceslit (mapcar #'car choices))
	 (choice (intern (string-upcase
			  (prompt-read
			   (format nil "~a [~{~a/~}Q]" prompt choiceslit))))))
    (if (find choice (cons 'q choiceslit))
	choice
	(prompt-choice prompt choices))))

(defparameter *app-choices* '((p dump-host-db)
			      (a add-hosts)
			      (l load-host-db)
			      (s save-host-db)
			      (u list-host-names)))

(defun prompt-for-host ()
  (make-host
   (prompt-read "Host name")
   (prompt-read "Host's DNS")
   (prompt-read "User name")
   (or (parse-integer (prompt-read "Port [22]") :junk-allowed t) 22)
   (prompt-read "Password")))

(defun add-hosts (&optional (more "y"))
  (when (string-equal more "y")
    (add-host (prompt-for-host))
    (add-hosts (y-or-n-p "Add more?"))))

(defun save-host-db (&optional (filename *host-db-filename*))
  (with-open-file (out filename
		       :direction :output
		       :if-exists :supersede)
    (with-standard-io-syntax
      (print *host-db* out))))

(defun load-host-db (&optional (filename *host-db-filename*))
  (with-open-file (in filename)
    (with-standard-io-syntax
      (setf *host-db* (read in)))))

(defun list-host-names ()
  (format t "~{~{~a: ~a~}~%~}"
	  (mapcar (lambda (x y) (list y (getf x :name)))
		  *host-db*
		  (loop for i from 1 to (length *host-db*) collect i))))

(defun host-app ()
  (let ((choice (prompt-choice "Choose one" *app-choices*)))
    (when (not (eq choice 'q))
      (funcall (second (assoc choice *app-choices*)))
      (host-app))))
