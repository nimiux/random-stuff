(defmacro dis (args &rest body)
  '(dissassemble
    (compile nil
     (lambda ,(mapcar (lambda (a)
                        (if (consp a)
                            (cadr a)
                            a))
               args)
       (declare
        ,@(mapcar
           #'(type ,(car a1) ,(cadr a1))
        (remove-if-not #'consp args)))
     ,@body))))
