;;;; -*- mode: Lisp -*-
;;;;
;;;; This is a test of prevalence (remote)

(require :cl-prevalence)

(in-package :cl-prevalence)

(defparameter *system-directory* (pathname "/tmp/articles/"))

(defvar *system* nil)

;; Create the prevalence system
(let ((directory *system-directory*))
  ;; Delete all xml files
  (when (probe-file directory)
    (dolist (pathname (directory (merge-pathnames "*.xml" directory)))
      (delete-file pathname)))
  (setf *system* (make-prevalence-system directory)))

;; Class article

(defclass article ()
  ((id     :initarg :id
	   :accessor id)
   (title  :initarg :title
	   :accessor title)
   (author :initarg :author
	   :accessor author)
   (status :initform :new
	   :accessor status)))

(defvar *articles* nil)

;; (defun published ()
;;  (remove-if-not #'(lambda (x) (eq x :published)) *articles* :key #'status ))

;; (defun add-article (title author &optional status)
;;  (let ((article (make-instance 'article :title title :author author)))
;;	(or (null status) (setf (status article) status))
;;	(push article *articles*)))

;; Transaction functions

(defun tx-create-articles-root (system)
  (setf (get-root-object system :articles) (make-hash-table)))

(defun tx-create-article (system title author)
  (let* ((articles (get-root-object system :articles))
	 (id (next-id system))
	 (article (make-instance 'article :id id :title title :author author)))
    (setf (gethash id articles) article)))

(defun tx-delete-articles (system id)
  (let ((articles (get-root-object system :articles)))
    (remhash id articles)))

;; New id counter

(execute *system* (make-transaction 'tx-create-id-counter))

(assert (zerop (get-root-object *system* :id-counter)))

;; New hash table for all persistent articles and mapping article id to article object

(execute *system* (make-transaction 'tx-create-articles-root))

(assert (hash-table-p (get-root-object *system* :articles)))

(defvar *uno*)

(let ((article (execute *system* (make-transaction 'tx-create-article "Uno" "uno"))))
  (assert (eq (class-of article) (find-class 'article)))
  (assert (equal (title article) "Uno"))
  (assert (equal (author article) "uno"))
  (setf *uno* (id article)))

(close-open-streams *system*)

;; Now retrieve the stuff
(setf *system* (make-prevalence-system *system-directory*))

(let ((article (gethash *uno* (get-root-object *system* :articles))))
  (assert (eq (class-of article) (find-class 'article)))
  (assert (equal (title article) "Uno"))
  (assert (equal (author article) "uno")))

;;; eof
