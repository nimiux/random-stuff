;; This is a bank account example for showing CLOS

(defparameter *current-account-number* 1)

(defclass bank-account ()
  ((customer-name
    :initarg :customer-name
    :initform (error "Must supply a customer name.")
    ;:reader customer-name
    ;:writer (setf customer-name)
    :accessor customer-name
    :documentation "Customer's name")
   (balance
    :initarg :balance
    :initform 0
    :reader balance
    :documentation "Current account number")
   (account-number
    :initform (incf *current-account-number*)
    :reader account-number
    :documentation "Account number")
   (account-type
    :reader account-type
    :documentation "Type of the account"))

(defclass checking-account (bank-account))

(defclass savings-account (bank-account))

(defmethod initialize-instance :after ((account bank-account) &key)
  (let ((balance (slot-value account 'balance)))
       (setf (slot-value account 'account-type)
             (cond
              ((>= balance 100000) :gold)
              ((>= balance 50000) :silver)
              (t :bronze)))))

;(defmethod initialize-instance :after ((account bank-account)
                                       ;&key opening-bonus-percentage)
  ;(when opening-bonus-percentage
        ;(incf (slot-value account 'balance)
              ;(* (slot-value account 'balance)
                 ;(/ opening-bonus-percentage 100)))))

;(defgeneric balance (account)
  ;(:documentation "Evaluates to the current balance of the account"))

;(defmethod balance ((account bank-account))
  ;(slot-value account 'balance))

;(defgeneric (setf customer-name) (value account)
  ;(:documentation "Sets the customer name of the account to name"))

;(defmethod (setf customer-name) (value (account bank-account))
  ;(setf (slot-value account 'customer-name) value))

;(defgeneric customer-name (account)
  ;(:documentation "Evaluates to the name of the customer owning the account"))

;(defmethod customer-name ((account bank-account))
  ;(slot-value account 'customer-name))
  
(defgeneric withdraw (account amount)
  (:documentation "Withdraw the specified amount from the account.
                  Signal an error if the current balance is less than
                  amount."))

(defmethod withdraw ((account bank-account) amount)
  (when (< (balance account) amount)
        (error "Account overdrawn."))
  (decf (balance account) amount))

(defmethod withdraw ((account checking-account) amount)
  (let ((overdraft (- amount (balance account))))
       (when (plusp overdraft)
             (withdraw (overdrat-account account) overdraft)
             (incf (balance account) overdraft)))
  (call-next-method))

(defmethod withdraw ((proxy proxy-account) amount)
  (withdraw (proxied-account proxy) amount))

(defmethod withdraw ((account (eql *account-of-bank-president*)) amount)
  (let ((overdraft (- amount (balance account))))
       (when (plusp overdraft)
             (incf (balance account) (embezzle *bank* overdraft)))
       (call-next-method)))

(defmethod withdraw :before ((account checking-account) amount)
  let ((overdraft (- amount (balance account)))
       (when (plusp overdraft)
             (withdraw (overdraft-account account) overdraft)
             (incf (balance account) overdraft)))

;(defparameter a (make-instance 'bank-account))
(defparameter a (make-instance 'bank-account :customer-name "Juan"))
(print-object a *standard-output*)
(slot-value a 'customer-name)
(slot-value a 'account-number)
(slot-value a 'balance)
(slot-value a 'account-type)

  

