(print "foo")
(prin1 "foo")
(princ "foo")

(progn (print "this")
  (print "is")
  (print "a")
  (print "test")
  )

(progn (prin1 "this")
  (prin1 "is")
  (prin1 "a")
  (prin1 "test")
  )

(defun say-hello ()
  (print "Please type your name:")
  (let ((name (read)))
       (print "Nice to meet you, ")
       (print name)))

(defun add-five ()
  "Adds five to a number"
  (print "Please enter a number:")
  (let ((num (read)))
       (print "When I add five I get")
       (print (+ num 5))))

(documentation 'add-five 'function)

(defun random-animal ()
  (nth (random 5) '("dog" "tick" "tiger" "walrus" "kangaroo")))

(defun animal-table ()
  (loop repeat 10
      do (format t "~10:@<~a~>~10:@<~a~>~10:@<~a~>~%"
                 (random-animal)
                 (random-animal)
                 (random-animal))))
(animal-table)

(defparameter *animals* (loop repeat 10 collect (random-animal)))
(format t "~{I see a ~a... or was it a ~a?~%~}" *animals*)

(format t "|~{~<|~%|~,33:;~2d ~>~}|" (loop for x below 100 collect x))

; vim: set ts=4 :
