(let ((a 3))
  (defun geta () a)
  (defun seta (n) (setq a n)))

(let* ((fortunes
	'("You will become a great Lisp programmer"
	  "The force will not be with you"
	  "Take time for meditation"))
       (len (length fortunes))
       (index 0))
  (defun fortune ()
    (let ((new-fortune (nth index fortunes)))
      (setq index (1+ index))
      (if (>= index len)
	  (setq index 0))
      new-fortune)))

(fortune)

(let ((direction 'up))
  (defun toggle-counter-direction ()
    (setq direction
          (if (eq direction 'up)
                  'down
                  'up)))
  (defun counter-class ()
    (let ((counter 0))
      (lambda ()
        (if (eq direction 'up)
          (incf counter)
          (decf counter)))))) 
