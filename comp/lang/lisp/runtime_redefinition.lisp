; Lisp is the ideal enviroment for rapid prototyping: even better than Python.
; One example is that you can modify your code on-the-fly while running it
; there is no better debugging tool.

; Consider the following rectangle class
(defclass rectangle ()
  ((width :accessor rectangle-width :initarg :width)
   (height :accessor rectangle-height :initarg :height)))

; and the following method to calculate rectangle's area: 
(defmethod rectangle-area ((r rectangle))
   (* (rectangle-height r) ; typo intended!
      (rectangle-height r)))

; now if you test it:
(defparameter my-test-rect (make-instance 'rectangle :width 80 :height 20))
(rectangle-area my-test-rect)

; Lisp prints "400" for you... wait, that's not good... oops and you spot
; a typo in the rectangle-area function, so you rewrite it without quitting
; Lisp runtime environment thusly.

(defmethod rectangle-area ((r rectangle))
   (* (rectangle-width r)
      (rectangle-height r)))

; and now you can call again the test function, without recreating new instance:
(rectangle-area my-test-rect)

; you will receive the correct answer, i.e. 1600.

; Dynamic redefinition with winding/unwinding st

; It's even more impressive if we join dynamic redefinition with winding/unwinding
; of stack.

; Let us write a little drill function that calls some phrase generator function
; f n times and that prints out generated phrases with some noops added for demo
; purposes:

(defun drill (n f)
   "Repeat n times the drill phrase.  Call f to obtain i-th drill phrase."
   (dotimes (i n)
     (format t "~%Phrase ~d begins. " i)
     (sleep 1)
     (format t "~s" (funcall f i))
     (sleep 1)
     (format t " Phrase ~d ends." i)))

; and let us write a phrase generator to praise my favourite language and to
; calculate some simple arithmetics:

(defun drill-phrase-generator (i)
  "Return i-th drill phrase."
  (format nil "I love Java! BTW, ~d*~d=~d." i i (* i i)))

; Now let us run it 10 times:

(drill 10 'drill-phrase-generator)

; but oops after several repetitions we notice that my favourite programming
; language was misspelled, and the arithmetics "broken" (say), so let's stop
; the app with ^C here
; which brought us to the "application listener" where we can correct ourselves:

1. Break [4]> (defun drill-phrase-generator (i)
                (format nil "I love Common Lisp! BTW, ~d+~d=~d." i i (+ i i)))
DRILL-PHRASE-GENERATOR

; and simply continue the execution:

1. Break [4]> continue
