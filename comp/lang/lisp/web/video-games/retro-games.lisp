(in-package :cl-user)

(require :hunchentoot)
(require :cl-who)
(require :parenscript)

(defpackage :retro-games
  (:use :cl :cl-who :hunchentoot :parenscript)
  (:export startserver
           stopserver))

;(use-package :xxxx)
(in-package :retro-games)

(defclass game ()
  ((name :reader name
         :initarg :name)
   (votes :accessor votes
          :initform 0)))


(defgeneric vote-for (game)
  (:documentation "Vote for a game"))

(defmethod vote-for (user-selected-game)
  (incf (votes user-selected-game)))

(defvar *games* '())

(defun new-game (name)
  (make-instance 'game :name name))

(defun game-from-name (name)
  (find name *games* :test #'string-equal :key #'name))

(defun game-stored-p (game-name)
  (game-from-name game-name))

(defun games ()
  (sort (copy-list *games*) #'> :key #'votes))

(defun add-game (name)
  (unless (game-stored-p name)
          (push (new-game name) *games*)))

(defmacro standard-page ((&key title) &body body)
  `(with-html-output-to-string
    (*standard-output* nil :prologue t :indent t)
    (:html :xmlns "http://www.w3.org/1999/xhtml"
           :xml\:lang "en" 
           :lang "en"
           (:head 
            (:meta :http-equiv "Content-Type" 
                   :content    "text/html;charset=utf-8")
            (:title ,title)
            (:link :type "text/css" 
                   :rel "stylesheet"
                   :href "/retro.css"))
           (:body 
            (:div :id "header" ; Retro games header
                  (:img :src "/logo.jpg" 
                        :alt "Commodore 64" 
                        :class "logo")
                  (:span :class "strapline" 
                         "Vote on your favourite Retro Game"))
            ,@body))))


(defmacro define-url-fn ((name) &body body)
  `(progn
    (defun ,name ()
           ,@body)
    (push (hunchentoot:create-prefix-dispatcher
           ,(format nil "/~(~a~).html" name) ',name)
           *dispatch-table*)))

(define-url-fn retro-games
  (standard-page (:title "Retro Games")
                 (:h1 "Top Retro Games")
                 (:p "We'll write the code later...")))

(defun startserver ()
  (progn
   (setq *serverinstance* (make-instance 'hunchentoot:easy-acceptor :port 4242))
   (start *serverinstance* )))

(in-package :cl-user)

(retro-games:startserver)
