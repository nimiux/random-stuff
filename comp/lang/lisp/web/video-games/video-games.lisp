(require :cl-who)
(require :hunchentoot)
(require :parenscript)
(require :elephant)
(require :ele-bdb)
(require :css-lite)
(require :cl-json)

(in-package :cl-user)

(defpackage :game-voter
  (:use :cl :cl-who :hunchentoot :parenscript :elephant)
  (:import-from :css-lite :css)
  (:import-from :cl-json :encode-json-to-string))

(in-package :game-voter)

;; Start our web server.
(defparameter *web-server-instance*
  (make-instance 'hunchentoot:easy-acceptor :port 8080))
(start *web-server-instance*)

;; Publish all static content.
(push (create-static-file-dispatcher-and-handler "/GameVoter.png" "statics/imgs/GameVoter.png") *dispatch-table*)
(push (create-static-file-dispatcher-and-handler "/site.css" "statics/css/site.css") *dispatch-table*)

;; Launch Elephant
(defparameter *store* (elephant:open-store '(:bdb "/tmp/game-voter.db")))

;; Represent each game as an instance of the Elephant persistant
;; class.
(defpclass persistent-game ()
  ((name :reader name :initarg :name :index t)
   (votes :accessor votes :initarg :votes :initform 0 :index t)))

(defgeneric vote-for (game) 
    (:documentation "Vote for a game"))

(defmethod vote-for (user-selected-game)
  (incf (votes user-selected-game)))

;; Encapsulate the back end.

(defun game-from-name (name)
  (get-instance-by-value 'persistent-game 'name name))

(defun game-stored-p (game-name)
  (game-from-name game-name))

(defun add-game (name)
  (with-transaction () ; Protect against multiples in the DB.
    (unless (game-stored-p name)
      (make-instance 'persistent-game :name name))))

;; Returns a list of all games, sorted on popularity.
(defun games ()
  (nreverse (get-instances-by-range 'persistent-game 'votes nil nil)))

;; Automatically creates a Hunchentoot handler for the given URL (plus
;; .htm) associating it with a function of the same name.
(defmacro define-url-fn ((name) &body body)
  `(progn
     (defun ,name ()
       ,@body)
     (push (create-prefix-dispatcher ,(format nil "/~(~a~)" name) ',name) *dispatch-table*)))

;; All pages on the Game Voter site will use the following macro; less
;; to type and a uniform look of the pages (defines the header and the
;; stylesheet).

(defmacro standard-page ((&key title) &body body)
  `(with-html-output-to-string (*standard-output* nil :prologue t :indent t)
     (:html :xmlns "http://www.w3.org/1999/xhtml"  :xml\:lang "en" :lang "en"
       (:head 
         (:meta :http-equiv "Content-Type" :content "text/html;charset=utf-8")
	 (:title ,title)
	 (:link :type "text/css" :rel "stylesheet" :href "/site.css"))
	   (:body 
	     (:div :id "header" ; Start all pages with our header.
	       (:img :src "/GameVoter.png" :alt "Game Voter Logo" :class "logo")
	       (:span :class "strapline" "Vote on your favourite Video Game"))
	     ,@body))))

;;
;; The functions responsible for generating the HTML go here.
;;

(define-url-fn (index)
  (standard-page (:title "Game Voter")
                 (:h1 "Vote on your all time favourite games!")
                 (:p "Missing a game? Make it available for votes " (:a :href "new-game" "here"))
     (:h2 "Current stand")
     (:div :id "chart" ; Used for CSS styling of the links.
       (:ol
	(dolist (game (games))
	 (htm  
	  (:li (:a :href (format nil "vote?name=~a" (name game)) "Vote!")
	       (fmt "~A with ~d votes" (name game) (votes game)))))))))

(define-url-fn (new-game)
  (standard-page (:title "Add a new game")
     (:h1 "Add a new game to the chart")
     (:form :action "/game-added" :method "post" 
	    :onsubmit (ps-inline ; Client-side validation.
		       (when (= name.value "")
			 (alert "Please enter a name.")
			 (return false)))
       (:p "What is the name of the game?" (:br)
	   (:input :type "text" :name "name" :class "txt"))
       (:p (:input :type "submit" :value "Add" :class "btn")))))

(define-url-fn (game-added)
  (let ((name (parameter "name")))
    (unless (or (null name) (zerop (length name))) ; In case JavaScript is turned off.
      (add-game name))
    (redirect "/index"))) ; Display the front page.

(define-url-fn (vote)
  (let ((game (game-from-name (parameter "name"))))
    (if game
	(vote-for game))
    (redirect "/index"))) ; Back to the front page.

(defun about-page () 
      "<h1>Hunchentoot Demo</h1>This is a very simple demonstration of the Hunchentoot webserver.")

(push (create-prefix-dispatcher "/about" 'about-page) *dispatch-table*)

(princ "Done")
