(require :hunchentoot)

(defpackage :testserv
    (:use :cl
              :hunchentoot)
      (:export :start-server))

(in-package :testserv)

(defun start-server ()
  (start (make-instance 'hunchentoot:easy-acceptor :port 8080)))

(setq *dispatch-table*
        `(,(create-prefix-dispatcher "/test" 'test-page)
                ,(create-prefix-dispatcher "/about" 'about-page)))

(defun test-page ()
    (let ((name (parameter "name")))
             (if name
                     (format nil "Hi, <b>~a</b>" name)
                         "Name: <form action='/test' method='get'><input type='text' name='name' /><input type='submit' name='submit' /></form>")))

(defun about-page ()
    "<h1>Hunchentoot Demo</h1>This is a very simple demonstration of the Hunchentoot webserver.")

(start-server)
