(defpackage #:my-blog-asd
  (:use :cl :asdf))

  (in-package :my-blog-asd) 

(defsystem my-blog
  :name "my-blog"
  :version "0.0.0"
  :author "Your name"
  :description "A fantastic blog"
  :components ((:file "blog"))
  :depends-on ("elephant" "hunchentoot" "html-template"))
