(defun plot (fn min max &optional (step 1))
  (loop for i from min to max by step do
        (loop repeat (funcall fn i) do
              (format t "*"))
        (fresh-line)))

; vim: set ts=4 :
