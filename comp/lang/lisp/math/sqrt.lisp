(defun average (x y)
  (/ (+ x y) 2))


(defun mysqrt (x &optional (tolerance 1e-17))
  (labels ((good-enough-p (y)
                       (< (abs (- (* y y) x)) tolerance))
           (improve (y)
                       (average (/ x y) y))
           (try (y)
                       (if (good-enough-p y)
                           y
                           (try (improve y)))))
     (try 1)))

(mysqrt 2)
