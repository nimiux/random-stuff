(defun primep (n)
  (when (> n 1)
    (loop for fac from 2 to (isqrt n) never (zerop (mod n fac))))) 

(defun next-prime (n)
  (loop for i from n when (primep i) return i ))

(defmacro doprimes ((var start end) &body body)
  (let ((ending-value-name (gensym)))
    `(do ((,var (next-prime ,start) (next-prime (+ ,var 2)))
          (ending-value ,end))
       ((> ,var ,ending-value-name))
       ,@body)))

(doprimes (i 2 100) (print i))

; vim: set ts=4 :

