(make-string-input-stream "hello?")
;; sb-impl::string-input-strem
(read (make-string-input-stream "hello!"))
;; HELLO!
(with-input-from-string (s "It's the multiverse!")
  (read s))
;; IT
(with-output-to-string (out)
  (with-input-from-string (in "\"Can I ask who's calling?\"")
    (let ((io (make-two-way-stream in out)))
      (format io "~A It's the Jovian moon, Io!" (read io)))))
;; "Can I ask who's calling? It's the Jovian moon, Io!"
(with-open-file (s "~/monkey.txt" :direction :output :if-does-not-exist :create :if-exists :supersede)
  (format s "I had a little monkey,~%Brought him to the country,~%Fed him on ginger-bread...~%"))
;; NIL
(with-open-file (s "~/monkey.txt" :direction :input)
  (format t "~&;;; ~A" (read-line s)))
;; ;;; I had a little money,
(with-open-file (s "~/monkey.txt" :direction :input)
  (do ((line (read-line s) (read-line s nil 'eof)))
    ((eq line 'eof) "-- Marilyn Manson")
    (format t "~&;;; ~A~%" line)))

;; ;;; I had a little monkey,
;; ;;; Brought him to the country,
;; ;;; Fed him on ginger-bread...
;; -- Marilyn Manson

(with-open-file (b "~/binary-monkey.txt" :direction :output :element-type 'unsigned-byte :if-exists :supersede)
  (write-byte 109 b)
  (write-byte 111 b)
  (write-byte 110 b)
  (write-byte 107 b)
  (write-byte 101 b)
  (write-byte 121 b))
