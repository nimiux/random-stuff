(asdf:defsystem :pathnames
    :name "pathnames"
    :version "0.0.1"
    :maintainer ""
    :author ""
    :description "Pathname library from Practical Common Lisp"
    :serial t
    :components
    ((:module "src"
      :components ((:file "package")
                   (:file "pathnames")))))
