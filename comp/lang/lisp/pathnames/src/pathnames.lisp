
;(dolist (case '(:common :local))
;  (dolist (host '("MY-LISPM" "MY-VAX" "MY-UNIX"))
;    (print (make-pathname :host host :case case
;			  :directory '(:absolute "PUBLIC" "GAMES")
;			  :name "CHESS" :type "DB"))))

(in-package :pathnames)

(defun my-file-length (filename)
  (with-open-file (in filename :element-type '(unsigned-byte 8))
    (file-length in)))

(defun component-present-p (value)
  (and value (not (eql value :unspecific))))

(defun directory-pathname-p (p)
  (and
   (not (component-present-p (pathname-name p)))
   (not (component-present-p (pathname-type p)))
   p))

(defun pathname-as-directory (name)
  (let ((pname (pathname name)))
    (when (wild-pathname-p pname)
      (error "Can't reliably convert wild pathnames."))
    (if (not (directory-pathname-p name))
	(make-pathname
	 :directory (append (or (pathname-directory pname)
				(list :relative))
			    (list (file-namestring pname)))
	 :name nil
	 :type nil
	 :defaults pname)
	pname)))

(defun directory-wildcard (dirname)
  (make-pathname
   :name :wild
   :type #-clisp :wild #+clisp nil
   :defaults (pathname-as-directory dirname)))

;; list-directory
(defun list-directory (dirname)
  (when (wild-pathname-p dirname)
    (error "Can only list concrete directory names."))
  (let ((wildcard (directory-wildcard dirname)))
    #+(or sbcl cm lispworks)
    (directory wildcard)

#+clisp
(nconc
 (directory wildcard)
 (directory (clisp-subdirectories-wildcard wildcard)))))

(defun pathname-as-file (name)
  (let ((pathname (pathname name)))
    (when (wild-pathname-p pathname)
      (error "Can't reliably convert wild pathnames."))
    (if (directory-pathname-p name)
	(let* ((directory (pathname-directory pathname))
	       (name-and-type (pathname (first (last directory)))))
	  (make-pathname
	   :directory (butlast directory)
	   :name (pathname-name name-and-type)
	   :type (pathname-type name-and-type)
	   :defaults pathname))
	pathname)))

;; file-exists-p
(defun file-exists-p (pname)
  #+(or sbcl lispworks openmcl)
  (probe-file pname)

  #+clisp
  (or (ignore-errors
	(probe-file (pathname-as-file pname)))
      (ignore-errors
	(let ((directory-from (pathname-as-directory pname)))
	  (when (ext:probe-directory directory-form)
	    directory-form)))))

;; walk-directory
(defun walk-directory (dirname fn &key directories (test (constantly t)))
  (labels
      ((walk (name)
	 (cond
	   ((directory-pathname-p name)
	    (when (and directories (funcall test name))
	      (funcall fn name))
	    (dolist (x (list-directory name)) (walk x)))
	   ((funcall test name) (funcall fn name)))))
    (walk (pathname-as-directory dirname))))
