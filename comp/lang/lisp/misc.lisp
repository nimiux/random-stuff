(defun save-obj (obj filename)
  (with-open-file (out filename
                  :direction :output
                  :if-exists :supersede)
                  (with-standard-io-syntax
                   (print obj out))))

(defmacro load-obj (filename obj)
  (let ((in (gensym)))
    `(with-open-file (,in ,filename)
       (with-standard-io-syntax
         (setf ,obj (read ,in))))))

(defun show-progress (len)
  (format t "=")
  (dotimes
      (i len)
    (format t "="))
  (format t ">"))


(macroexpand '(load-obj "a.ld" b))
(defparameter *b* nil)
(defparameter a '(1 3 4))
(save-obj a "a.ld")
(load-obj "a.ld" *b*)

; Interesting stuff
(defmacro backwards (expr) (reverse expr))
(format nil "~r" 1606432345436564564565)

(defvar x)
(setf x 1)
(defun fact (x)
  (if (<= x 1)
      1
      (* x (fact (1- x)))))

