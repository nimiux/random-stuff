(defun register-allocated-fixnum ()
  (declare (optimize (speed 3) (safety 0)))
  (let ((acc 0))
    (loop for i from 1 to 100 do
	  (incf (the fixnum acc)
		(the fixnum i)))
    acc))
