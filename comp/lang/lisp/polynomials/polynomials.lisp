;;;
;;; Polynomials
;;; 2013
;;;
;;;

(in-package :polynomials)

(defun nlist (n)
  (case n
    ((0) ())
    ((1) '(0))
    (t (let ((l (nlist (1- n))))
	 (cons (1- n) l)))))

(defun clist (n v)
  (if (<= n 0)
      ()
      (cons v (clist (1- n) v))))

(defun padlist (n l &optional (x 0))
  (append (clist (- n (length l)) x) l))

(defun value-of (p v)
  :documentation "P(x). Use Horner's method"
  (reduce (lambda (x y) (+ (* x v) y)) p))

(defun canonizepols (&rest l)
  (let ((m (apply #'max
		  (mapcar #'length l))))
    (mapcar (lambda (x) (padlist m x)) l)))

(defun + (arg1 arg2 &rest arg3)
  (adding arg1 arg2 arg3))

(defun + (&rest pols)
  :documentation "Adds Polynomials"
  (reduce (lambda (x y) (mapcar '+ x y))
          (apply #'canonizepols pols)))

(defun polneg (&rest pols)
  ""
  (mapcar (lambda (x) (mapcar '- x))
          (apply #'canonizepols pols)))

(defun polsub (&rest pols)
  "Substratcs (cdr pols) from first polynomial"
  (let ((cpols (canonize pols)))
       (poladd (car cpols) (polneg (cdr cpols)))))

(polneg '(3 1 2 5) '(1) '(1 2 0 3 4))
(polneg '(3 1 2 5))

;; Fixed point

(defun fixed-point (f start &optional (tolerance 1e-4))
  (labels ((close-enough-p (u v)
                           (< (abs (- u v) tolerance)))
           (iter (old new)
                 (if (close-enough-p old new)
                     new
                     (iter new (f new)))))
           (iter start (f start))))

;; find roots:

;y(n+1)=y(n)-(P(y(n))/P'(y(n)))

(defparameter dx 1e-5)

(defun deriv (f)
  (lambda (x)
          (/ (- (f (+ x dx))
                (f x))
             dx)))

(defun newton (f guess)
  (labels ((df (deriv f)))
          (fixed-point (lambda (x) (- x (/ (f x) (df x))))
                               guess)))


; vim: set ts=4 :
