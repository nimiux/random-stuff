(defmacro nif-buggy (expr pos zero neg)
  `(let ((obscure-name ,expr))
     (cond ((plusp obscure-name) ,pos)
           ((zerop obscure-name) ,zero)
           (t ,neg))))

(let ((obscure-name 'pos)) (nif-buggy (+ 2 3) obscure-name 'zero 'neg))

(macroexpand '(nif-buggy (+ 2 3) (let ((obscure-name 'pos)) obscure-name) 'zero 'neg))
(nif-buggy (+ 2 3) (let ((obscure-name 'pos)) obscure-name) 'zero 'neg)

(macroexpand '(let ((obscure-name 'pos)) (nif-buggy (+ 2 3) obscure-name 'zero 'neg)))
(let ((obscure-name 'pos)) (nif-buggy (+ 2 3) obscure-name 'zero 'neg))


(defmacro nif (expr pos zero neg)
  (let ((g (gensym)))
    `(let ((,g ,expr))
       (cond ((plusp ,g) ,pos)
             ((zerop ,g) ,zero)
             (t ,neg)))))

(nif (+ 2 3) (let ((obscure-name 'pos)) obscure-name) 'zero 'neg)
(let ((obscure-name 'pos)) (nif (+ 2 3) obscure-name 'zero 'neg))

(defun myreverse (lst &optional (acc))
  (cond ((null lst) acc)
        (t (myreverse (cdr lst) (cons (car lst) acc)))))

(myreverse '(1 2 3 4))

(defun bad-reverse (lst)
  (let* ((len (length lst))
         (ilimit (truncate (/ len 2))))
    (do ((i 0 (1+ i))
         (j (1- len) (1- j)))
      ((>= i ilimit))
      (rotatef (nth i lst) (nth j lst)))))

(rotatef (nth 0 l) (nth 2 l))   

(setf a 1 b 2 c 3 d 4)
(rotatef a b c d)


(setf l '(1 2 3))
(nreverse l )

(defun fact (n &optional (acc 1))
  (declare (number n acc))
  (if (= n 1)
      acc
      (fact (1- n) (* acc n))))


