;;;;
;;;;
;;;;  Object store
;;;;

;;; Some samples
;;; (show :databases)
;;; (show :collections)

(require :cl-mongo)

(in-package :cl-mongo)

(defparameter *database* "mydb")

(db.use *database*)

(db.insert "foo" (kv "document" "one"))

(pp (iter (db.find "foo" :all)))
