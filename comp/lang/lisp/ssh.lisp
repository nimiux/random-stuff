;(defun ssh (username host)
;  (with-open-stream (s
;                     (run-program "ssh"
;                                  (list
;                                              "-p"
;                                              "1022"
;                                              (format nil
;                                                      "~A@~A" username host)
;                                              "bash"
;                                              "-c"
;                                              (format nil "\"cat > ~S\""
;                                                      "test"))
;                                  :input :stream
;                                  :search t))
;                    (princ "Hello world!" s)
;                    (terpri s)
;                    (finish-output s)))
;
;(ssh "eodm176" "ks394210.kimsufi.com")

;(run-program "ssh" '("-p" "1022" "eodm176@ks394210.kimsufi.com")
  ;:search t
  ;:input t 
  ;:output t)
;
;(defun ssh ()
;  (with-open-stream (stream
;                      (run-program "ssh"
;                                   :arguments (list (format nil "~A@~A" "pjb" "localhost")
;                                                    "bash"
;                                                    "-c"
;                                                    (format nil "\"cat > ~S\"" "test"))
;                                   :input :stream utput :terminal))
;    (princ "Hello world!" stream)
;    (terpri stream)
;    (finish-output stream)))

(defmacro ssh (username host port)
  `(run-program "ssh" ',(list "-p"
                             port
                             (format nil "~A@~A" username host))
                :search t
                :input t
                :output t))

(ssh "user" "server" "port")
