(defun my-cons (a b)
  (lambda (pick)
    (cond ((= pick 1) a)
          ((= pick 2) b))))


(defun my-car (x)
  (funcall x 1))

(defun my-cdr (x)
  (funcall x 2))

(defvar a (my-cons 1 2))

(my-car a)

(my-cdr a)
