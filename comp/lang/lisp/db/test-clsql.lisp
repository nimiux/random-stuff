(require :clsql)
(clsql:connect '("localhost" "test" "test" "test") :database-type :postgresql)
(format t "Connected to databases: ~a~%"
  (mapcar #'clsql:database-name (clsql:connected-databases)))
(clsql:status)
(clsql:disconnect)
