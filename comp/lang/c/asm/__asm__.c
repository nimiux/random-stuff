#define MAX_VALUE       200

/* Inline assembler
 *
 * __asm__ ( "code" : outlist : inlist : clobberlist);
 * Modifers

a	eax
b	ebx
c	ebx
d	edx
g       either a variable or an immediate
S	esi
D	edi
q	any register
r	any register
I	immediate

*/

#define load_cr3(x) __asm__ ("movl %0, %%cr3" : : "r" (x) )


#define SYS_PROCESS  1
#define SYS_EXEC     1

int exec (char *task)
{
   __asm__ __volatile__ ("movl %0, %%eax ; movl %1, %%ebx" :  : "g" \
   (SYS_PROCESS | SYS_EXEC) , "g" (task) : "eax" , "ebx"); // Clobber: "eax" , "ebx" will be modified
   __asm__ __volatile__ ("int $0x50");
   int ret;
   __asm__ __volatile__ ("movl %%eax, %0" : "=g" (ret)  );

   return ret;
}       

void *memcpy( void *dst, const void *src, unsigned int n)
{
    __asm__("cld ; rep ; movsb":  : "c" ((unsigned int) n), "S" (src), "D" (dst));
        
   return dst;
}
	            
unsigned char inportb(unsigned short int port)
{
   unsigned char val;

   //__asm__ __volatile__("inb %w1,%b0" : "=a" (val) : "d" (port) );
   return val;
}

int main(void)
{
    int i=100;
    __asm__ ("movl $0xff, %eax");
    __asm__ ("movl %0, %%eax ; movl %1, %%ebx" : :  "g" (i) , "g" (MAX_VALUE));
}
