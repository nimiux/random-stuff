#ifndef __ERRORES_H__
#define __ERRORES_H__



/* -------------------------------------------------------------------------
 * Directivas de configuracion
 * ------------------------------------------------------------------------- */

#include <config.h>


#define ERROR( c )             levantarError( (c), __LINE__, __FILE__, NULL )
#define ERROREX( c, ia )       levantarError( (c), __LINE__, __FILE__, (ia) )

#define ERR_NING                           0
#define ERR_ARGINV                         1
#define ERR_NOMEMORIA                      2
#define ERR_NOMASREGS                      3
#define ERR_NOMASETIQS                     4
#define ERR_NOMASCONTS                     5
#define ERR_NOMASVALORES                   6
#define ERR_NOMASDATOS                     7
#define ERR_FINPLANTILLA                   8
#define ERR_NOHAYSALIDA                    9
#define ERR_BRSINVALIDO                   10
#define ERR_DATAINVALIDA                  11
#define ERR_FICHABIERTO                   12
#define ERR_FICHCERRADO                   13
#define ERR_FMTINVALIDO                   14
#define ERR_OPINVALIDA                    15
#define ERR_TERM                          16

#define ERR_SO_ESINVALIDA                 17
#define ERR_SO_NOEXEC                     18
#define ERR_SO_NOSIGNAL                   19

#define ERR_BD_NOCONECT                   20
#define ERR_BD_EJEC                       21
#define ERR_BD_NOLEE                      22
#define ERR_BD_NOALOJCURS                 23



/* -------------------------------------------------------------------------
 * Declaracion de las funciones publicas del modulo
 * ------------------------------------------------------------------------- */

int ajustarIdProceso  ( long idProcesoNuevo );
int ajustarIdRegistro ( const char *ptrCodigo,
                        const char *ptrProgresivo );

int levantarError     ( int codError,
                        long numLinea,
                        const char *nombFichero,
                        const void *ptrInfoAdic );



#endif /* __ERRORES_H__ */
