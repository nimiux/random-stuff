#ifndef __GTORSALIDA_H__
#define __GTORSALIDA_H__



/* ----------------------------------------------------------------------------
 * Directivas de configuracion
 * ------------------------------------------------------------------------- */

#include <config.h>

#include <Contenedor.h>
#include <Plantilla.h>
#include <GtorEntrada.h>
#include <FicheroSalida.h>



/* ----------------------------------------------------------------------------
 * Declaracion de los tipos publicos del modulo
 * ------------------------------------------------------------------------- */

struct st_GtorSalida;

typedef struct st_GtorSalida TGtorSalida;
typedef TGtorSalida *PGtorSalida;


/*
 * Representacion del tipo de dato Gestor de Salida
 */
struct st_GtorSalida
{
	long            opciones;    /* Opciones de generacion */
	char        *nombFichero;    /* Puntero al nombre del fich. salida */
	PPlantilla       ptrPlla;    /* Puntero a la plantilla BRS */
	PGtorEntrada  ptrGtorEnt;    /* Puntero al gestor de entrada */
	PContenedor      ptrCdor;    /* Puntero al contenedor de datos */
	PFicheroSalida   ptrFSal;    /* Puntero al fichero de salida */
};



/* ----------------------------------------------------------------------------
 * Declaracion de las variables publicas del modulo
 * ------------------------------------------------------------------------- */

DECLARAR_TEMPORIZADOR( GENERAR_SALIDA )
DECLARAR_TEMPORIZADOR( GENERAR_LOTE )
DECLARAR_TEMPORIZADOR( GENERAR_REGISTRO )


/* ----------------------------------------------------------------------------
 * Declaracion de las funciones publicas del modulo
 * ------------------------------------------------------------------------- */

PGtorSalida crearGtorSalida( long opciones,
                             long numFicheros,
                             const char *nombFichero,
                             PGtorEntrada ptrGtorEnt,
                             PPlantilla ptrPlantilla,
                             PContenedor ptrContedor );

int generarSalida      ( PGtorSalida gtorsal );

int destruirGtorSalida ( PGtorSalida gtorsal );

#ifdef DEBUG
void imprimirGtorSalida( PGtorSalida gtorsal );
#endif



#endif /* __GTORSALIDA_H__ */
