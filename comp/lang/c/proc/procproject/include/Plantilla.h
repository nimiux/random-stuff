#ifndef __PLANTILLA_H__
#define __PLANTILLA_H__



/* ----------------------------------------------------------------------------
 * Directivas de configuracion
 * ------------------------------------------------------------------------- */

#include <config.h>



/* ----------------------------------------------------------------------------
 * Declaracion de los tipos publicos del modulo
 * ------------------------------------------------------------------------- */

struct st_EtiqCont;
struct st_Plantilla;

typedef struct st_EtiqCont   TEtiqCont;
typedef struct st_Plantilla TPlantilla;

typedef TEtiqCont   *PEtiqCont;
typedef TPlantilla *PPlantilla;


/*
 * Representacion de una etiqueta y su contenido
 * en memoria.
 */
struct st_EtiqCont
{
	char      codEtiq[PLLA_LGO_CODETIQ];   /* Codigo de etiqueta */
	char      clsEtiq[PLLA_LGO_CLSETIQ];   /* Clase de etiqueta */
	char      salEtiq[PLLA_LGO_SALETIQ];   /* Impresion de etiqueta */
	char      fmtEtiq[PLLA_LGO_FMTETIQ];   /* Formato de etiqueta*/
	char    restEtiq[PLLA_LGO_RESTETIQ];   /* Restriccion de etiqueta */ 
	char      codCont[PLLA_LGO_CODCONT];   /* Codigo de contenido */
	char    restCont[PLLA_LGO_RESTCONT];   /* Restriccion de contenido */
	char    tipoCont[PLLA_LGO_TIPOCONT];   /* Tipo de contenido */
	char  valorCont[PLLA_LGO_VALORCONT];   /* Valor de contenido */
	char      fmtCont[PLLA_LGO_FMTCONT];   /* Formato de contenido */
	long                  indProxEtiqIt;   /* Indice a la prox. etiq. iter. */
};

/*
 * Representacion de una plantilla en memoria.
 */
struct st_Plantilla
{
	long        opciones;   /* Opciones de plantilla */
	long        numEtiqs;   /* Numero total de etiquetas */
	long     indLectEtiq;   /* Indice al par actual (lectura) */
	long   indLectEtiqIt;   /* Indice al par actual (lect. iter.) */
	long   indPrimEtiqIt;   /* Indice a la prim. etiq. iterativa */
	long    indMarcaLect;   /* Indice de marca de lectura */
	PEtiqCont   ptrEtiqs;   /* Buffer de etiquetas */

	/* Puntero a la cadena de conexion con la base de datos */
	char cadConexion[PLLA_LGO_CADENACONEXION];
};



/* ----------------------------------------------------------------------------
 * Declaracion de las variables publicas del modulo
 * ------------------------------------------------------------------------- */

DECLARAR_TEMPORIZADOR( CARGAR_PLANTILLA )



/* ----------------------------------------------------------------------------
 * Declaracion de las funciones publicas del modulo
 * ------------------------------------------------------------------------- */

PPlantilla crearPlantilla ( long opciones,
                            const char *cadConexion );

int cargarPlantilla   ( PPlantilla plla );

int avanzarEtiqCont   ( PPlantilla plla );
int primeraEtiqCont   ( PPlantilla plla );
int proximaEtiqCont   ( PPlantilla plla );

int avanzarEtiqContIt ( PPlantilla plla );
int primeraEtiqContIt ( PPlantilla plla );
int proximaEtiqContIt ( PPlantilla plla );

int marcarEtiqCont    ( PPlantilla plla );
int irMarcaEtiqCont   ( PPlantilla plla );

PEtiqCont obtenerEtiqCont   ( PPlantilla plla );
PEtiqCont obtenerEtiqContIt ( PPlantilla plla );

int destruirPlantilla ( PPlantilla plla );

#ifdef DEBUG
void imprimirPlantilla ( PPlantilla plla );
#endif



#endif /* __PLANTILLA_H__ */
