#ifndef __CONTENEDOR_H__
#define __CONTENEDOR_H__



/* ----------------------------------------------------------------------------
 * Directivas de configuracion
 * ------------------------------------------------------------------------- */

#include <config.h>



/* ----------------------------------------------------------------------------
 * Declaracion de los tipos publicos del modulo
 * ------------------------------------------------------------------------- */

struct st_Contenedor;
struct st_Registro;
struct st_Etiqueta;
struct st_Contenido;
struct st_Valor;

typedef struct st_Contenedor TContenedor;
typedef struct st_Registro     TRegistro;
typedef struct st_Etiqueta     TEtiqueta;
typedef struct st_Contenido   TContenido;
typedef struct st_Valor           TValor;

typedef TContenedor         *PContenedor;
typedef TRegistro             *PRegistro;
typedef TEtiqueta             *PEtiqueta;
typedef TContenido           *PContenido;
typedef TValor                   *PValor;


/*
 * Representacion de un contenedor.
 */
struct st_Contenedor
{
	long               numRegistros;  /* Numero actual de registros */
	size_t                largoData;  /* Longitud de la data en memoria */
	size_t               largoUsado;  /* Longitud de la data utilizada */
	char                   *ptrData;  /* Puntero a la data en memoria */
	char              *ptrEscritura;  /* Proximo byte en donde escribir */
	PRegistro       ptrPrimRegistro;  /* Puntero al primer registro */
	PRegistro        ptrUltRegistro;  /* Puntero al ultimo registro */
	PRegistro       ptrLectRegistro;  /* Puntero al registro actual (lect) */
	PEtiqueta       ptrLectEtiqueta;  /* Puntero a la etiqueta actual (lect) */
	PEtiqueta     ptrLectEtiquetaIt;  /* Puntero a la etiqueta iter. (lect) */
	PEtiqueta    ptrEtiquetaOmitida;  /* Puntero a la ultima etiq. omitida */
	PContenido     ptrLectContenido;  /* Puntero al contenido actual (lect) */
	PContenido  ptrContenidoOmitido;  /* Puntero al ultimo cont. omitido */
	PValor             ptrLectValor;  /* Puntero al valor actual (lect) */
};

/*
 * Representacion de un registro del contenedor
 */
struct st_Registro
{
	long         tipoRegistro;    /* Indicador del tipo de registro */
	long         numEtiquetas;    /* Numero de etiquetas */
	PEtiqueta ptrPrimEtiqueta;    /* Puntero a la primera etiqueta */
	PEtiqueta  ptrUltEtiqueta;    /* Puntero a la ultima etiqueta */
	PRegistro ptrProxRegistro;    /* Puntero al proximo registro */
};

/*
 * Representacion de una etiqueta de registro
 */
struct st_Etiqueta
{
	int                numContenidos;  /* Numero de contenidos */
	PContenido      ptrPrimContenido;  /* Puntero al ultimo contenido */
	PContenido       ptrUltContenido;  /* Puntero al ultimo contenido */
	PEtiqueta        ptrProxEtiqueta;  /* Puntero a la proxima etiqueta */
	char strCodigo[CDOR_LGO_CODETIQ];  /* Puntero al nombre de la etiqueta */
};

/*
 * Representacion de un contenido de etiqueta
 */
struct st_Contenido
{
	int                   numValores;  /* Numero de valores */
	PValor              ptrPrimValor;  /* Puntero al primer valor */
	PValor               ptrUltValor;  /* Puntero al ultimo valor */
	PContenido      ptrProxContenido;  /* Puntero al proximo contenido */
	char strCodigo[CDOR_LGO_CODCONT];  /* Punero al nombre del contenido */
};

/*
 * Representacion de un valor de etiqueta
 */
struct st_Valor
{
	PValor      ptrProxValor;     /* Puntero al proximo valor */
	size_t        largoValor;     /* Longitud del valor actual */
	char         strValor[1];     /* Valor del contenido */
};



/* ----------------------------------------------------------------------------
 * Declaracion de las variables publicas del modulo
 * ------------------------------------------------------------------------- */

DECLARAR_TEMPORIZADOR( EXPANDIR_CONTENEDOR )



/* ----------------------------------------------------------------------------
 * Declaracion de las funciones publicas del modulo
 * ------------------------------------------------------------------------- */

PContenedor crearContenedor ( void );

int vaciarContenedor    ( PContenedor cdor );

int ajustarTipoRegistro ( PContenedor  cdor,
                          long tipoRegistro );

int obtenerTipoRegistro ( PContenedor      cdor,
                          long *ptrTipoRegistro );

int insertarRegistro    ( PContenedor cdor,
                          long tipoRegistro );

int insertarEtiqueta    ( PContenedor cdor,
                          char *etiq );

int insertarContenido   ( PContenedor cdor,
                          char *cont );

int insertarValor       ( PContenedor  cdor,
                          char *valor );

int agregarContenido    ( PContenedor cdor,
                          char *etiq,
                          char *cont );

int agregarValor        ( PContenedor  cdor,
                          char  *cont,
                          char *valor );

int agregarValorEx      ( PContenedor  cdor,
                          char  *etiq,
                          char  *cont,
                          char *valor );

int buscarRegistro      ( PContenedor cdor,
                          int ordRegistro );

int buscarEtiqueta      ( PContenedor cdor,
                          char *etiq );

int buscarEtiquetaIt    ( PContenedor cdor,
                          char *etiq );

int buscarContenido     ( PContenedor cdor,
                          char *cont );

int buscarValor         ( PContenedor cdor,
                          int     ordValor );

int primerRegistro      ( PContenedor cdor );
int proximoRegistro     ( PContenedor cdor );
int primeraEtiqueta     ( PContenedor cdor );
int proximaEtiqueta     ( PContenedor cdor );
int primerContenido     ( PContenedor cdor );
int proximoContenido    ( PContenedor cdor );
int primerValor         ( PContenedor cdor );
int proximoValor        ( PContenedor cdor );

PRegistro  obtenerRegistro  ( PContenedor cdor );
PEtiqueta  obtenerEtiqueta  ( PContenedor cdor );
PContenido obtenerContenido ( PContenedor cdor );
PValor     obtenerValor     ( PContenedor cdor );

int existenValores   ( PContenedor cdor );

int destruirContenedor ( PContenedor cdor );

#ifdef DEBUG
void imprimirContenedor( PContenedor cdor );
#endif



#endif /* __CONTENEDOR_H__ */
