#ifndef __FICHEROSALIDA_H__
#define __FICHEROSALIDA_H__



/* ----------------------------------------------------------------------------
 * Directivas de configuracion
 * ------------------------------------------------------------------------- */

#include <config.h>


#define FS_DELIMIT( c )                 (((c) == ' ') ||   \
                                         ((c) == '\t') ||  \
                                         ((c) == '\n'))

#define FS_FICHERO_ID( fsal, i )        ((fsal)->fichs[(i)].ptrFich)
#define FS_BUFFER_ID( fsal, i )         ((fsal)->fichs[(i)].ptrBuffer)

#define FS_FICHERO( fsal )              FS_FICHERO_ID( (fsal), (fsal)->idFichero )
#define FS_BUFFER( fsal )               FS_BUFFER_ID( (fsal), (fsal)->idFichero )



/* ----------------------------------------------------------------------------
 * Declaracion de los tipos publicos del modulo
 * ------------------------------------------------------------------------- */

struct st_FicheroSalida;
struct st_ElemFichero;

typedef struct st_FicheroSalida TFicheroSalida;
typedef struct st_ElemFichero   TElemFichero;

typedef TFicheroSalida *PFicheroSalida;
typedef TElemFichero   *PElemFichero;


/*
 * Representacion del tipo de dato Elemento de Fichero
 */
struct st_ElemFichero
{
	FILE   *ptrFich;          /* Puntero al fichero de salida */
	char *ptrBuffer;          /* Puntero al buffer de salida */
};

/*
 * Representacion del tipo de dato Fichero de Salida
 */
struct st_FicheroSalida
{
	size_t    largoBuffer;    /* Longitud del buffer de salida */
	long        idFichero;    /* Identificador del fichero a utilizar */
	long         numFichs;    /* Numero de elementos de fichero */
	TElemFichero fichs[1];    /* Arreglo de elementos de fichero */
};



/* ----------------------------------------------------------------------------
 * Declaracion de las funciones publicas del modulo
 * ------------------------------------------------------------------------- */

PFicheroSalida crearFicheroSalida ( long numFichs,
                                    size_t largoBuffer );

int abrirFicheroSalida    ( PFicheroSalida fsal,
                            const char *nombFichero );

int escribirTextoFormato  ( PFicheroSalida fsal,
                            const char *formato,
                            const char *textoEntrada );

int cerrarFicheroSalida   ( PFicheroSalida fsal );

int ajustarIdFichero      ( PFicheroSalida fsal,
                            long idFichero );

int obtenerIdFichero      ( PFicheroSalida fsal,
                            long *idFichero );

int destruirFicheroSalida ( PFicheroSalida fsal );



#endif /* __FICHEROSALIDA_H__ */
