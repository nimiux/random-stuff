#ifndef __GTORENTRADA_H__
#define __GTORENTRADA_H__



/* ----------------------------------------------------------------------------
 * Directivas de configuracion
 * ------------------------------------------------------------------------- */

#include <config.h>

#include <Contenedor.h>
#include <Plantilla.h>
#include <InterfaceBD.h>
#include <TablaEtiquetas.h>
#include <TablaContenidos.h>



/* ----------------------------------------------------------------------------
 * Declaracion de los tipos publicos del modulo
 * ------------------------------------------------------------------------- */

struct st_GtorEntrada;

typedef struct st_GtorEntrada TGtorEntrada;
typedef TGtorEntrada *PGtorEntrada;


/*
 * Representacion del tipo de dato Gestor de Entrada.
 */
struct st_GtorEntrada
{
	long            opciones;   /* Opciones de generacion */
	long           minCodigo;   /* Numero minimo de empresa/abonado */
	long           maxCodigo;   /* Numero maximo de empresa/abonado */
	long         hayMasDatos;   /* Indica la existencia o no de datos en BD */
	PPlantilla       ptrPlla;   /* Puntero a la plantilla BRS */
	PContenedor      ptrCdor;   /* Puntero al contenedor de datos */

	/* Punteros a la(s) cadena(s) de conexion con la base de datos */
	char        cadConexion1[GTORENT_LGO_CADENACONEXION];
	char        cadConexion2[GTORENT_LGO_CADENACONEXION];
};



/* ----------------------------------------------------------------------------
 * Declaracion de las variables publicas del modulo
 * ------------------------------------------------------------------------- */

DECLARAR_TEMPORIZADOR( CARGAR_DATOS )



/* ----------------------------------------------------------------------------
 * Declaracion de las funciones publicas del modulo
 * ------------------------------------------------------------------------- */

PGtorEntrada crearGtorEntrada ( long              opciones,
                                long            minEmpresa,
                                long            maxEmpresa,
                                PPlantilla         ptrPlla,
                                PContenedor        ptrCdor,
                                const char   *cadConexion1,
                                const char   *cadConexion2 );

int conectarGtorEntrada ( PGtorEntrada gtorent );
int cargarDatos         ( PGtorEntrada gtoren );
int destruirGtorEntrada ( PGtorEntrada gtoren );

int manipularSenalTerminar( int senal );

#ifdef DEBUG
void imprimirGtorEntrada( PGtorEntrada gtoren );
#endif



#endif /* __GTORENTRADA_H__ */
