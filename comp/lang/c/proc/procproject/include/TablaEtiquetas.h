#ifndef __TABLAETIQUETAS_H__
#define __TABLAETIQUETAS_H__



/* ----------------------------------------------------------------------------
 * Directivas de configuracion
 * ------------------------------------------------------------------------- */

#include <config.h>


#define TETIQ_NING                           0

#define TETIQ_NUMERO_CODETQ_AL               1
#define TETIQ_NUMERO_CODETQ_ANUN             2
#define TETIQ_NUMERO_CODETQ_AP1              3
#define TETIQ_NUMERO_CODETQ_AP2              4
#define TETIQ_NUMERO_CODETQ_BRS              5
#define TETIQ_NUMERO_CODETQ_CALL             6
#define TETIQ_NUMERO_CODETQ_CAS              7
#define TETIQ_NUMERO_CODETQ_CAT              8
#define TETIQ_NUMERO_CODETQ_CE               9
#define TETIQ_NUMERO_CODETQ_CH               10
#define TETIQ_NUMERO_CODETQ_CL               11
#define TETIQ_NUMERO_CODETQ_CLPA             12
#define TETIQ_NUMERO_CODETQ_CL_EXP           13
#define TETIQ_NUMERO_CODETQ_CM               14
#define TETIQ_NUMERO_CODETQ_CP               15
#define TETIQ_NUMERO_CODETQ_CP_ALO           16
#define TETIQ_NUMERO_CODETQ_CQ               17
#define TETIQ_NUMERO_CODETQ_CTL              18
#define TETIQ_NUMERO_CODETQ_DIRE             19
#define TETIQ_NUMERO_CODETQ_DIRI             20
#define TETIQ_NUMERO_CODETQ_DIRR             21
#define TETIQ_NUMERO_CODETQ_ENT              22
#define TETIQ_NUMERO_CODETQ_EPP              23
#define TETIQ_NUMERO_CODETQ_ID               24
#define TETIQ_NUMERO_CODETQ_INSE             25
#define TETIQ_NUMERO_CODETQ_KE               26
#define TETIQ_NUMERO_CODETQ_KH               27
#define TETIQ_NUMERO_CODETQ_KM               28
#define TETIQ_NUMERO_CODETQ_KQ               29
#define TETIQ_NUMERO_CODETQ_LO               30
#define TETIQ_NUMERO_CODETQ_LO1              31
#define TETIQ_NUMERO_CODETQ_LO2              32
#define TETIQ_NUMERO_CODETQ_ND               33
#define TETIQ_NUMERO_CODETQ_NO               34
#define TETIQ_NUMERO_CODETQ_NUME             35
#define TETIQ_NUMERO_CODETQ_PA               36
#define TETIQ_NUMERO_CODETQ_PI               37
#define TETIQ_NUMERO_CODETQ_PR               38
#define TETIQ_NUMERO_CODETQ_PROD             39
#define TETIQ_NUMERO_CODETQ_SI               40
#define TETIQ_NUMERO_CODETQ_SIS              41
#define TETIQ_NUMERO_CODETQ_TE               42
#define TETIQ_NUMERO_CODETQ_TF               43
#define TETIQ_NUMERO_CODETQ_TF1              44
#define TETIQ_NUMERO_CODETQ_TFS              45
#define TETIQ_NUMERO_CODETQ_TH               46
#define TETIQ_NUMERO_CODETQ_TM               47
#define TETIQ_NUMERO_CODETQ_TP               48
#define TETIQ_NUMERO_CODETQ_TQ               49
#define TETIQ_NUMERO_CODETQ_TR               50
#define TETIQ_NUMERO_CODETQ_VCA              51
#define TETIQ_NUMERO_CODETQ_VMI              52
#define TETIQ_NUMERO_CODETQ_VMO              53



/* ----------------------------------------------------------------------------
 * Declaracion de las funciones publicas del modulo
 * ------------------------------------------------------------------------- */

int buscarTipoEtiqueta( const char *strEtiq,
                        size_t lgoEtiq );

char * buscarNombreEtiqueta( int tipoEtiq );



#endif
