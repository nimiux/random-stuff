#ifndef __CARGARGIS_H__
#define __CARGARGIS_H__



/* ----------------------------------------------------------------------------
 * Directivas de configuracion
 * ------------------------------------------------------------------------- */

#include <config.h>

#include <ctype.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#include <InterfaceBD.h>

#define SQLCODE                             (sqlca.sqlcode)

#define NROMAX_INTENTOS_CONEX               3
#define NROMAX_PETICIONES_GIS               450
#define NRO_FILAS_X_VALIDACION              1000



/* ----------------------------------------------------------------------------
 * Declaracion de las funciones externas del modulo
 * ------------------------------------------------------------------------- */

extern void bzero(char *, int);



#endif /* __CARGARGIS_H__ */
