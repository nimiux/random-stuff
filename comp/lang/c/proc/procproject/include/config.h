
#ifndef __CONFIG_H__
#define __CONFIG_H__



/* ----------------------------------------------------------------------------
 * Directivas de configuracion
 * ------------------------------------------------------------------------- */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include <errno.h>
#include <time.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>


/*
 * Macros de compatibilidad con el manejador de base de datos
 */

#ifndef SQLCODE
#define SQLCODE                             (sqlca.sqlcode)
#endif /* SQLCODE */

#define SQLROWS                             (sqlca.sqlerrd[2])
#define SQLERRL                             (sqlca.sqlerrm.sqlerrml)
#define SQLERRC                             (sqlca.sqlerrm.sqlerrmc)

#define STR2VCHR( v, s )                       \
	do {                                       \
		strcpy( (char *) (v).arr, (s) );       \
		(v).len = strlen( (s) );               \
	} while (0)
 
#define VCHR2STR( s, v )                       \
	do {                                       \
		(v).arr[(v).len] = '\0';               \
		strcpy( (s), (v).arr );                \
	} while (0)
 
#define VCHRLIM( v )                           \
	do {                                       \
		(v).arr[(v).len] = '\0';               \
	} while (0)

#define VCHRNULL( v )                          \
	do {                                       \
		(v).arr[0] = '\0';                     \
		(v).len = 0;                           \
	} while (0)

#define AVCHRNULL( v )                         \
	do {                                       \
		memset( (v), 0, sizeof( (v) ) );       \
	} while (0)

#define IVCHR2STR( s, v, i )                   \
	do {                                       \
		if (!(i)) {                            \
			(v).arr[(v).len] = '\0';           \
			strcpy((s),(const char *)(v).arr); \
		} else {                               \
			(s)[0] = '\0';                     \
		}                                      \
	} while (0)


/*
 * Macros generales para valores de la base de datos
 */

#define BD_LGO_CADENACONEXION               256

#define BD_NUMERO_INULL                    -1
#define BD_NUMERO_INOTNULL                  0

#define BD_CARAC_SI                         'S'
#define BD_CARAC_NO                         'N'
#define BD_CARAC_FIJO                       'F'
#define BD_CARAC_VARIABLE                   'V'

#define BD_CADENA_NOMBPROCESO               "genbrs"
#define BD_CADENA_CERO                      "0"

#define BD_SQLCODE_NOTFOUND                 1403
#define BD_SQLCODE_TOOMANYROWS              1422


/*
 * Macros de configuracion del programa principal.
 */

#define PPAL_CADENA_PATRON_CNT                 "*** BRS DOCUMENT BOUNDARY ***"
#define PPAL_CADENA_NOMBFICH_CNT_PA            "reg_genbrs_a.txt"
#define PPAL_CADENA_NOMBFICH_CNT_RS            "reg_genbrs_r.txt"
#define PPAL_CADENA_NOMBFICH_CNT_AL            "reg_genbrs_h.txt"
#define PPAL_CADENA_NOMBFICH_CNT_PB            "reg_genbrs_b.txt"
#define PPAL_CADENA_NOMBFICH_PB                "bl-%02d.brs"
#define PPAL_CADENA_FNUMFICHEROS               "-nf"
#define PPAL_CADENA_FNOMBFICHERO               "-f"
#define PPAL_CADENA_FCONEXDATA                 "-cd"
#define PPAL_CADENA_FCONEXPLLA                 "-cp"
#define PPAL_CADENA_FCONEXTTES                 "-ct"
#define PPAL_CADENA_FIDPROCESO                 "-i"
#define PPAL_CADENA_FRGOINICIO                 "-ri"
#define PPAL_CADENA_FRGOFIN                    "-rf"

#define PPAL_NUMERO_MINNUMARGS                 1
#define PPAL_NUMERO_MINNUMPROCS                1
#define PPAL_NUMERO_MAXNUMPROCS               99
#define PPAL_NUMERO_MINNUMPROVS                0
#define PPAL_NUMERO_MAXNUMPROVS               53
#define PPAL_NUMERO_TODASPROVS                -1
#define PPAL_NUMERO_TODASEMPS                 -1
#define PPAL_NUMERO_TPOESPERA                  1

#define PPAL_OPCION_NING                  0x0000
#define PPAL_OPCION_MODO_SILEN            0x0001
#define PPAL_OPCION_TIPO_PA               0x0002
#define PPAL_OPCION_TIPO_RS               0x0004
#define PPAL_OPCION_TIPO_AL               0x0008
#define PPAL_OPCION_TIPO_PB               0x0010

#define PPAL_LGO_CADENACONEXION                BD_LGO_CADENACONEXION
#define PPAL_LGO_NOMBFICHERO                 256
#define PPAL_LGO_FTIPOFICHERO                  8
#define PPAL_LGO_FNUMFICHEROS                  8
#define PPAL_LGO_FNOMBFICHERO                  PPAL_LGO_NOMBFICHERO
#define PPAL_LGO_FIDPROCESO                    8
#define PPAL_LGO_FMINCODIGO                   32
#define PPAL_LGO_FMAXCODIGO                   32
#define PPAL_LGO_BUFFERCOPIAFICH            1024


/*
 * Macros de configuracion del gestor de errores.
 */

#define ERROR_LGO_MSJERROR                  1024
#define ERROR_LGO_IDREGISTRO                 128


/*
 * Macros de configuracion de las iterfaces con la base de datos
 */

#define IFACE_NUM_PROCS                      PPAL_NUMERO_MAXNUMPROCS
#define IFACE_NUM_EMPS                      64
#define IFACE_NUM_DIRS                    1024 /* 64 */
#define IFACE_NUM_TELS                      16
#define IFACE_NUM_WEBS                      16
#define IFACE_NUM_ADIS                    1024 /* 64 */
#define IFACE_NUM_GES                     1024 /* 64 */
#define IFACE_NUM_PRS                     1024 /* 64 */
#define IFACE_NUM_ABOS                    4096
#define IFACE_NUM_TFSS                       2

#define IFACE_OPCION_NING                    PPAL_OPCION_NING
#define IFACE_OPCION_TIPO_PA                 PPAL_OPCION_TIPO_PA
#define IFACE_OPCION_TIPO_RS                 PPAL_OPCION_TIPO_RS
#define IFACE_OPCION_TIPO_AL                 PPAL_OPCION_TIPO_AL
#define IFACE_OPCION_TIPO_PB                 PPAL_OPCION_TIPO_PB

#define IFACE_NUMERO_AMBI_APA_PA             1
#define IFACE_NUMERO_AMBI_APA_AL             2
#define IFACE_NUMERO_AMBI_APA_RS             3

#define IFACE_NUMERO_TIPO_ADI_CAT            1
#define IFACE_NUMERO_TIPO_ADI_CP             2
#define IFACE_NUMERO_TIPO_ADI_KE             3
#define IFACE_NUMERO_TIPO_ADI_KH             4
#define IFACE_NUMERO_TIPO_ADI_KM             5
#define IFACE_NUMERO_TIPO_ADI_KQ             6
#define IFACE_NUMERO_TIPO_ADI_PA             7
#define IFACE_NUMERO_TIPO_ADI_SI             8
#define IFACE_NUMERO_TIPO_ADI_TH             9
#define IFACE_NUMERO_TIPO_ADI_ZON           10

#define IFACE_NUMERO_TEXPAN_GE_AUP           1
#define IFACE_NUMERO_TEXPAN_GE_PRP           2
#define IFACE_NUMERO_TEXPAN_GE_LOP           3
#define IFACE_NUMERO_TEXPAN_GE_LOS           4

#define IFACE_NUMERO_TEXPAN_PR_CAT           1
#define IFACE_NUMERO_TEXPAN_PR_CP            2
#define IFACE_NUMERO_TEXPAN_PR_EPP           3
#define IFACE_NUMERO_TEXPAN_PR_KE            4
#define IFACE_NUMERO_TEXPAN_PR_KH            5
#define IFACE_NUMERO_TEXPAN_PR_KM            6
#define IFACE_NUMERO_TEXPAN_PR_KQ            7
#define IFACE_NUMERO_TEXPAN_PR_PA            8
#define IFACE_NUMERO_TEXPAN_PR_SI            9
#define IFACE_NUMERO_TEXPAN_PR_TH           10
#define IFACE_NUMERO_TEXPAN_PR_ZON          11

#define IFACE_CADENA_TIPO_PA                 "PA"
#define IFACE_CADENA_TIPO_RS                 "PR"
#define IFACE_CADENA_TIPO_AL                 "PH"

#define IFACE_LGO_CADENACONEXION             BD_LGO_CADENACONEXION
#define IFACE_LGO_TIPOENTRADA                3
#define IFACE_LGO_TIPOSALIDA                 IFACE_LGO_TIPOENTRADA
#define IFACE_LGO_INSTSQL                 1024
#define IFACE_LGO_COEMPRESA                 10
#define IFACE_LGO_COACTVAD                   5
#define IFACE_LGO_PGPCONEX                   5
#define IFACE_LGO_TXACTVAD                  71
#define IFACE_LGO_TXNOMBRE                 201
#define IFACE_LGO_NMTAMANO                   4
#define IFACE_LGO_NMOCURS                    4
#define IFACE_LGO_RZSOCIAL                 201
#define IFACE_LGO_PRMEDIO                   11
#define IFACE_LGO_PRMENUDIA                 11
#define IFACE_LGO_SRDOMICILIO               31
#define IFACE_LGO_DVVINFAAGR              2048
#define IFACE_LGO_TIPDIREC                  14
#define IFACE_LGO_PRESVIA                    2
#define IFACE_LGO_DEPROV                    71
#define IFACE_LGO_COPOSTAL                   8
#define IFACE_LGO_TXTIPOVIA                 26
#define IFACE_LGO_TXCALLE                   51
#define IFACE_LGO_DINUMERO                  31
#define IFACE_LGO_DIRESTO                   31
#define IFACE_LGO_CALLELGA                 131
#define IFACE_LGO_CALLEMAN                 131
#define IFACE_LGO_CODINE                    12
#define IFACE_LGO_TIPTELF                    7
#define IFACE_LGO_NCTELF                    10
#define IFACE_LGO_NCCTL                      2
#define IFACE_LGO_TIPWEB                     7
#define IFACE_LGO_DIWEB                    151
#define IFACE_LGO_TIPEXPAN                   2
#define IFACE_LGO_CODCLAVE                  10
#define IFACE_LGO_TXTCLAVE                  71
#define IFACE_LGO_DSCEXPAN                  81
#define IFACE_LGO_NIVEXPAN                   2
#define IFACE_LGO_TXTEXPAN                  71
#define IFACE_LGO_IDABONO                   17
#define IFACE_LGO_NOMBRE                   141
#define IFACE_LGO_APELLIDO                  31
#define IFACE_LGO_CTABONO                    3
#define IFACE_LGO_COORDENADA                 9
#define IFACE_LGO_FLAGGIS                    2
#define IFACE_LGO_CVINFADI                   3 


/*
 * Macros de configuracion de la plantilla
 */

#define PLLA_OPCION_NING                    PPAL_OPCION_NING
#define PLLA_OPCION_TIPO_PA                 PPAL_OPCION_TIPO_PA
#define PLLA_OPCION_TIPO_RS                 PPAL_OPCION_TIPO_RS
#define PLLA_OPCION_TIPO_AL                 PPAL_OPCION_TIPO_AL
#define PLLA_OPCION_TIPO_PB                 PPAL_OPCION_TIPO_PB

#define PLLA_NUM_ETIQCONT                   10

#define PLLA_LGO_CADENACONEXION             BD_LGO_CADENACONEXION
#define PLLA_LGO_TIPOSALIDA                 3
#define PLLA_LGO_CODETIQ                    11
#define PLLA_LGO_CLSETIQ                    4
#define PLLA_LGO_SALETIQ                    2
#define PLLA_LGO_FMTETIQ                    81
#define PLLA_LGO_RESTETIQ                   2
#define PLLA_LGO_CODCONT                    11
#define PLLA_LGO_RESTCONT                   2
#define PLLA_LGO_TIPOCONT                   2
#define PLLA_LGO_VALORCONT                  101
#define PLLA_LGO_FMTCONT                    101

#define PLLA_CADENA_TIPO_PA                 "PA"
#define PLLA_CADENA_TIPO_RS                 "RS"
#define PLLA_CADENA_TIPO_AL                 "AL"
#define PLLA_CADENA_TIPO_PB                 "PB"

#define PLLA_CADENA_CLSETIQDF               "DF"
#define PLLA_CADENA_CLSETIQDI               "DI"


/*
 * Macros de configuracion del contenedor
 */

#define CDOR_FCTOR_ALINEAMTO( l )           \
	(CDOR_FRONTERA_ALINEACN - ((l) % CDOR_FRONTERA_ALINEACN))

#define CDOR_FRONTERA_ALINEACN              sizeof( long )

#define CDOR_LGO_EXTEN                      786432
#define CDOR_LGO_CODETIQ                    PLLA_LGO_CODETIQ
#define CDOR_LGO_CODCONT                    PLLA_LGO_CODCONT


/*
 * Macros de configuracion del gestor de entrada
 */

#define GTORENT_OPCION_NING                 PPAL_OPCION_NING
#define GTORENT_OPCION_TIPO_PA              PPAL_OPCION_TIPO_PA 
#define GTORENT_OPCION_TIPO_RS              PPAL_OPCION_TIPO_RS
#define GTORENT_OPCION_TIPO_AL              PPAL_OPCION_TIPO_AL
#define GTORENT_OPCION_TIPO_PB              PPAL_OPCION_TIPO_PB

#define GTORENT_NUMERO_TIPOREG_PA           0
#define GTORENT_NUMERO_TIPOREG_AL           0
#define GTORENT_NUMERO_TIPOREG_RS           0

#define GTORENT_LGO_BUFFER_AJUSTE         256
#define GTORENT_LGO_CADENACONEXION          BD_LGO_CADENACONEXION
#define GTORENT_LGO_MINCODIGO               IFACE_LGO_IDABONO
#define GTORENT_LGO_MAXCODIGO               IFACE_LGO_IDABONO

#define GTORENT_CADENA_SEPARADOR_EXPANSION  " "
#define GTORENT_CADENA_COACTVAD_38          "38"
#define GTORENT_CADENA_VALORTQ_38           "DE TAPAS"
#define GTORENT_CADENA_VALORCQ_38           "COCINA DE TAPAS COCINAS TAPAS"
#define GTORENT_CADENA_VALORTE_SD           "SERVICIO A DOMICILIO"
#define GTORENT_CADENA_VALORCE_SD           "SERVICIO A DOMICILIO"


/* Fase II - Valoracion restaurantes */

#define GTORENT_NUMERO_VALORACION_CAMPSA_INF    16L
#define GTORENT_NUMERO_VALORACION_CAMPSA_SUP    19L
#define GTORENT_NUMERO_VALORACION_MICHELIN_INF  20L
#define GTORENT_NUMERO_VALORACION_MICHELIN_SUP  23L
#define GTORENT_NUMERO_VALORACION_MONDIAL_INF   24L
#define GTORENT_NUMERO_VALORACION_MONDIAL_SUP   27L


/*
 * Macros para la configuracion del gestor de salida
 */

#define GTORSAL_OPCION_NING                 PPAL_OPCION_NING
#define GTORSAL_OPCION_TIPO_PA              PPAL_OPCION_TIPO_PA
#define GTORSAL_OPCION_TIPO_RS              PPAL_OPCION_TIPO_RS
#define GTORSAL_OPCION_TIPO_AL              PPAL_OPCION_TIPO_AL
#define GTORSAL_OPCION_TIPO_PB              PPAL_OPCION_TIPO_PB

#define GTORSAL_LGO_BUFFERFMT               128
#define GTORSAL_LGO_BUFFERVALOR             1024 

#define GTORSAL_FLAG_ITERAT                  -1


/*
 * Macros de configuracion del fichero de salida
 */

#define FSAL_DEF_LGO_BUFFER                 4096

#define FSAL_OPCION_NING                    0x0000
#define FSAL_OPCION_MULTI                   0x0001

#define FSAL_LGO_RESGBUFFER                 256 
#define FSAL_LGO_NOMBFICHERO                PPAL_LGO_NOMBFICHERO


/*
 * Macros de utilidad general
 */

#ifndef MAX
#define MAX( a, b )                         ((a) > (b) ? (a) : (b))
#endif /* MAX */

#ifndef MIN
#define MIN( a, b )                         ((a) < (b) ? (a) : (b))
#endif /* MIN */

#define REPTR( t, p, bi, bf )                           \
	do {                                                \
		if ((p))                                        \
			(p) = (t) ((bf) + (((byte *)(p)) - (bi)));  \
	} while (0)

#define REPTR_B( p, bi, bf )                REPTR( byte *, p, bi, bf )
#define REPTR_R( p, bi, bf )                REPTR( PRegistro, p, bi, bf )
#define REPTR_E( p, bi, bf )                REPTR( PEtiqueta, p, bi, bf )
#define REPTR_C( p, bi, bf )                REPTR( PContenido, p, bi, bf )
#define REPTR_V( p, bi, bf )                REPTR( PValor, p, bi, bf )


/*
 * Macros de ayuda a la depuracion
 */

#define DEBUG                               1

#ifdef DEBUG
#define DBG( x )                            x
#else
#define DBG( x )
#endif

#ifdef DEBUG
#define DEFINIR_TEMPORIZADOR( n )                                      \
	long g_cont##n;                                                    \
	double g_total##n;                                                 \
	time_t g_inicio##n;
#else
#define DEFINIR_TEMPORIZADOR( n )
#endif

#ifdef DEBUG
#define DECLARAR_TEMPORIZADOR( n )                                     \
	extern long g_cont##n;                                             \
	extern double g_total##n;                                          \
	extern time_t g_inicio##n;
#else
#define DECLARAR_TEMPORIZADOR( n )
#endif

#ifdef DEBUG
#define INICIO_MEDIDA_TEMPORIZADOR( n )                               \
	++g_cont##n;                                                      \
	time( &g_inicio##n );
#else
#define INICIO_MEDIDA_TEMPORIZADOR( n )
#endif

#ifdef DEBUG
#define FIN_MEDIDA_TEMPORIZADOR( n )                                  \
	g_total##n += difftime( time( NULL ), g_inicio##n );
#else
#define FIN_MEDIDA_TEMPORIZADOR( n )
#endif

#ifdef DEBUG
#define INICIAR_TEMPORIZADOR( n )                                     \
	g_cont##n = 0;                                                    \
	g_total##n = 0.0;
#else
#define INICIAR_TEMPORIZADOR( n )
#endif

#ifdef DEBUG
#define FINALIZAR_TEMPORIZADOR( x )                                   \
	if (g_cont##x) {                                                  \
		fprintf( stderr, "Nro. llamadas a %s:  %ld.\n",               \
		         #x, g_cont##x );                                     \
		fprintf( stderr, "Tpo. promedio por llamada a %s:  %f.\n",    \
		         #x, g_total##x/g_cont##x );                          \
		fprintf( stderr, "Tpo. total de llamadas a %s:  %f.\n",       \
		         #x, g_total##x );                                    \
	} else {                                                          \
		fprintf( stderr, "%s no ha sido invocado.\n", #x );           \
	}
#else
#define FINALIZAR_TEMPORIZADOR( n )
#endif


/*
 * Incluye el modulo de gestion de errores
 */

#include <Plantilla.h>
#include <Errores.h>



/* ----------------------------------------------------------------------------
 * Declaracion de los tipos de datos publicos del modulo
 * ------------------------------------------------------------------------- */

typedef char byte;



#endif  /* __CONFIG_H__ */
