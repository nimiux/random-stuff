
#ifndef __INTERFACEBD_H__
#define __INTERFACEBD_H__



/* ----------------------------------------------------------------------------
 * Directivas de configuracion
 * ------------------------------------------------------------------------- */

#include <config.h>



/* ----------------------------------------------------------------------------
 * Declaracion de los tipos de dato publicos del modulo
 * ------------------------------------------------------------------------- */

typedef char TCoEmpresa   [IFACE_LGO_COEMPRESA];
typedef char TCoActvad    [IFACE_LGO_COACTVAD];
typedef char TPgPconex    [IFACE_LGO_PGPCONEX];
typedef char TTxActvad    [IFACE_LGO_TXACTVAD];
typedef char TTxNombre    [IFACE_LGO_TXNOMBRE];
typedef char TNmTamano    [IFACE_LGO_NMTAMANO];
typedef char TNmOcurs     [IFACE_LGO_NMOCURS];
typedef char TRzSocial    [IFACE_LGO_RZSOCIAL];
typedef char TPrMedio     [IFACE_LGO_PRMEDIO];
typedef char TPrMenuDia   [IFACE_LGO_PRMENUDIA];
typedef char TSrDomicilio [IFACE_LGO_SRDOMICILIO];
typedef char TDvVinfaAgr  [IFACE_LGO_DVVINFAAGR];
typedef char TTipDirec    [IFACE_LGO_TIPDIREC];
typedef char TPresVia     [IFACE_LGO_PRESVIA];
typedef char TDeProv      [IFACE_LGO_DEPROV];
typedef char TCoPostal    [IFACE_LGO_COPOSTAL];
typedef char TTxTipoVia   [IFACE_LGO_TXTIPOVIA];
typedef char TTxCalle     [IFACE_LGO_TXCALLE];
typedef char TDiNumero    [IFACE_LGO_DINUMERO];
typedef char TDiResto     [IFACE_LGO_DIRESTO];
typedef char TCalleLga    [IFACE_LGO_CALLELGA];
typedef char TCalleMan    [IFACE_LGO_CALLEMAN];
typedef char TCodIne      [IFACE_LGO_CODINE];
typedef char TTipTelf     [IFACE_LGO_TIPTELF];
typedef char TNcTelf      [IFACE_LGO_NCTELF];
typedef char TNcCtl       [IFACE_LGO_NCCTL];
typedef char TTipWeb      [IFACE_LGO_TIPWEB];
typedef char TDiWeb       [IFACE_LGO_DIWEB];
typedef char TTipExpan    [IFACE_LGO_TIPEXPAN];
typedef char TCodClave    [IFACE_LGO_CODCLAVE];
typedef char TTxtClave    [IFACE_LGO_TXTCLAVE];
typedef char TDscExpan    [IFACE_LGO_DSCEXPAN];
typedef char TNivExpan    [IFACE_LGO_NIVEXPAN];
typedef char TTxtExpan    [IFACE_LGO_TXTEXPAN];
typedef char TIdAbono     [IFACE_LGO_IDABONO];
typedef char TNoNombre    [IFACE_LGO_NOMBRE];
typedef char TNoApellido  [IFACE_LGO_APELLIDO];
typedef char TCtAbono     [IFACE_LGO_CTABONO];
typedef char TCoordenada  [IFACE_LGO_COORDENADA];
typedef char TFlagGis     [IFACE_LGO_FLAGGIS];



/* ----------------------------------------------------------------------------
 * Declaracion de las variables publicas del modulo
 * ------------------------------------------------------------------------- */

DECLARAR_TEMPORIZADOR( CALCULAR_CARGA )
DECLARAR_TEMPORIZADOR( P_CO_TEL )
DECLARAR_TEMPORIZADOR( P_CO_WEB )
DECLARAR_TEMPORIZADOR( P_PA_EMP )
DECLARAR_TEMPORIZADOR( P_PA_DIR )
DECLARAR_TEMPORIZADOR( P_RS_EMP )
DECLARAR_TEMPORIZADOR( P_RS_DIR )
DECLARAR_TEMPORIZADOR( P_RS_ADI )
DECLARAR_TEMPORIZADOR( P_RS_ADI_VAL )
DECLARAR_TEMPORIZADOR( P_AL_EMP )
DECLARAR_TEMPORIZADOR( P_AL_DIR )
DECLARAR_TEMPORIZADOR( P_AL_ADI )
DECLARAR_TEMPORIZADOR( P_PB_ABO )
DECLARAR_TEMPORIZADOR( P_PB_TFSEC )
DECLARAR_TEMPORIZADOR( P_GE_EXP )
DECLARAR_TEMPORIZADOR( P_GE_ZON )
DECLARAR_TEMPORIZADOR( P_PR_EXP )
DECLARAR_TEMPORIZADOR( P_PR_UNO )
DECLARAR_TEMPORIZADOR( P_PR_DOS )



/* ----------------------------------------------------------------------------
 * Declaracion de las funciones publicas del modulo
 * ------------------------------------------------------------------------- */

int iface_conectar ( const char *cadenaConexion1,
                     const char *cadenaConexion2 );

int iface_calcularCarga ( long              opciones,
                          long           numProcesos,
                          const char *cadenaConexion, 
                          long            *minCodigo,
                          long            *maxCodigo );

int iface_P_CO_TEL ( TCoEmpresa  (*coEmpresa),
                     TPgPconex    (*pgPconex),
                     TTipTelf     (**tipTelf) [],
                     TNcTelf       (**ncTelf) [],
                     long          *numFilas,
                     long         *flagHecho );

int iface_P_CO_WEB ( TCoEmpresa  (*coEmpresa),
                     TPgPconex    (*pgPconex),
                     TTipWeb       (**tipWeb) [],
                     TDiWeb         (**diWeb) [],
                     long          *numFilas,
                     long         *flagHecho );

int iface_P_PA_EMP ( TCoEmpresa   (*minEmpresa),
                     TCoEmpresa   (*maxEmpresa),
                     TCoEmpresa   (**coEmpresa) [],
                     TCoActvad     (**coActvad) [],
                     TTxActvad     (**txActvad) [],
                     TRzSocial     (**rzSocial) [],
                     long            *numFilas,
                     long           *flagHecho );

int iface_P_PA_NOM ( TCoEmpresa    (*coEmpresa),
                     TCoActvad      (*coActvad),
                     TTxNombre     (**txNombre) [],
                     TNmTamano     (**nmTamano) [],
                     TNmOcurs       (**nmOcurs) [],
                     long            *numFilas,
                     long           *flagHecho );

int iface_P_PA_DIR ( TCoEmpresa       (*coEmpresa),
                     TCoActvad         (*coActvad),
                     TRzSocial         (*rzSocial),
                     TPgPconex      (**pgPconexDi) [],
                     TTipDirec        (**tipDirec) [],
                     TPresVia          (**presVia) [],
                     TDeProv            (**deProv) [],
                     TCoPostal        (**coPostal) [],
                     TTxTipoVia      (**txTipoVia) [],
                     TTxCalle          (**txCalle) [],
                     TDiNumero        (**diNumero) [],
                     TDiResto          (**diResto) [],
                     TCalleLga        (**calleLga) [],
                     TCalleMan        (**calleMan) [],
                     TCodIne            (**codIne) [],
                     TCoordenada        (**coordX) [],
                     TCoordenada        (**coordY) [],
                     TFlagGis          (**flagGis) [],
                     long               *numFilas,
                     long              *flagHecho );

int iface_P_RS_EMP ( TCoEmpresa     (*minEmpresa),
                     TCoEmpresa     (*maxEmpresa),
                     TCoEmpresa     (**coEmpresa) [],
                     TCoActvad       (**coActvad) [],
                     TTxActvad       (**txActvad) [],
                     TRzSocial       (**rzSocial) [],
                     TPrMedio         (**prMedio) [],
                     TPrMenuDia     (**prMenuDia) [],
                     TSrDomicilio (**srDomicilio) [],
                     TDvVinfaAgr   (**dvVinfaAgr) [],
                     long              *numFilas,
                     long             *flagHecho );

int iface_P_RS_DIR ( TCoEmpresa        (*coEmpresa),
                     TCoActvad          (*coActvad),
                     TRzSocial          (*rzSocial),
                     TPrMedio            (*prMedio),
                     TPrMenuDia        (*prMenuDia),
                     TSrDomicilio    (*srDomicilio),
                     TDvVinfaAgr      (*dvVinfaAgr),
                     TPgPconex       (**pgPconexDi) [],
                     TTipDirec         (**tipDirec) [],
                     TPresVia           (**presVia) [],
                     TDeProv             (**deProv) [],
                     TCoPostal         (**coPostal) [],
                     TTxTipoVia       (**txTipoVia) [],
                     TTxCalle           (**txCalle) [],
                     TDiNumero         (**diNumero) [],
                     TDiResto           (**diResto) [],
                     TCalleLga         (**calleLga) [],
                     TCalleMan         (**calleMan) [],
                     TCodIne             (**codIne) [],
                     TCoordenada         (**coordX) [],
                     TCoordenada         (**coordY) [],
                     TFlagGis           (**flagGis) [],
                     long                *numFilas,
                     long               *flagHecho );

int iface_P_RS_ADI ( long             tipInfAdi,
                     TCoEmpresa     (*coEmpresa),
                     TCoActvad       (*coActvad),
                     TRzSocial       (*rzSocial),
                     TPrMedio         (*prMedio),
                     TPrMenuDia     (*prMenuDia),
                     TSrDomicilio (*srDomicilio),
                     TDvVinfaAgr   (*dvVinfaAgr),
                     TTipExpan      (**ctInfExt) [],
                     TCodClave      (**idInfExt) [],
                     TDscExpan      (**dvInfAdi) [],
                     long             *numFilas,
                     long            *flagHecho );

int iface_P_AL_EMP ( TCoEmpresa   (*minEmpresa),
                     TCoEmpresa   (*maxEmpresa),
                     TCoEmpresa   (**coEmpresa) [],
                     TCoActvad     (**coActvad) [],
                     TPgPconex     (**pgPconex) [],
                     TRzSocial     (**rzSocial) [],
                     long *numFilas,
                     long *flagHecho );

int iface_P_AL_DIR ( TCoEmpresa      (*coEmpresa),
                     TPgPconex       (*pgPconex),
                     TPgPconex      (**pgPconexDi) [],
                     TTipDirec        (**tipDirec) [],
                     TPresVia          (**presVia) [],
                     TDeProv            (**deProv) [],
                     TCoPostal        (**coPostal) [],
                     TTxTipoVia      (**txTipoVia) [],
                     TTxCalle          (**txCalle) [],
                     TDiNumero        (**diNumero) [],
                     TDiResto          (**diResto) [],
                     TCalleLga        (**calleLga) [],
                     TCalleMan        (**calleMan) [],
                     TCodIne            (**codIne) [],
                     TCoordenada        (**coordX) [],
                     TCoordenada        (**coordY) [],
                     TFlagGis          (**flagGis) [],
                     long               *numFilas,
                     long              *flagHecho );

int iface_P_AL_ADI ( long          tipInfAdi,
                     TCoEmpresa  (*coEmpresa),
                     TPgPconex    (*pgPconex),
                     TTipExpan   (**ctInfExt) [],
                     TCodClave   (**idInfExt) [],
                     TDscExpan   (**dvInfAdi) [],
                     long *numFilas,
                     long *flagHecho );

int iface_P_PB_ABO ( TIdAbono       (*minAbono),
                     TIdAbono       (*maxAbono),
                     TIdAbono       (**idAbono) [],
                     TNoNombre     (**noNombre) [],
                     TNoApellido   (**noApell1) [],
                     TNoApellido   (**noApell2) [],
                     TCtAbono       (**ctAbono) [],
                     TTxCalle       (**txCalle) [],
                     TDiNumero     (**diNumero) [],
                     TDiResto       (**diResto) [],
                     TCoPostal     (**coPostal) [],
                     TDeProv         (**deProv) [],
                     TCodIne         (**codIne) [],
                     TNcTelf         (**ncTelf) [],
                     TNcCtl           (**ncCtl) [],
					 TNcTelf       (**ncTelfo2) [],
                     long            *numFilas,
                     long           *flagHecho );

int iface_P_PB_TFSEC ( void );

int iface_P_GE_EXP ( long           tipExpan,
                     TCodIne        (*codIne),
                     TTxtExpan   (**txtExpan) [],
                     long          *numFilas,
                     long         *flagHecho );

int iface_P_GE_EXP2( TCodIne        (*codIne),
                     TNivExpan   (**nivExpan) [],
                     TTxtExpan   (**txtExpan) [],
                     long          *numFilas,
                     long         *flagHecho );

int iface_P_GE_ZON ( TCodClave   (*codClave),
                     TTxtClave   (*txtClave),
                     TTxtExpan  (**txtExpan) [],
                     long         *numFilas,
                     long        *flagHecho );

int iface_P_PR_EXP ( long          tipExpan,
                     TCodClave   (*codClave),
                     TTxtClave   (*txtClave),
                     TTxtExpan (**singExpan) [],
                     TTxtExpan (**plurExpan) [],
                     long         *numFilas,
                     long        *flagHecho );

int iface_P_PR_UNO ( long          tipExpan,
                     TCodClave   (*codClave),
                     TTxtClave   (*txtClave),
                     TTxtExpan (**singExpan),
                     TTxtExpan (**plurExpan) );

int iface_P_PR_DOS ( long          tipExpan,
                     TCodClave   (*codClave),
                     TTxtClave   (*txtClave),
                     TTxtExpan (**singExpan),
                     TTxtExpan (**plurExpan) );



#endif /* __INTERFACEBD_H__ */
