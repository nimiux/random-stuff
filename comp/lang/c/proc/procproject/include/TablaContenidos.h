#ifndef __TABLACONTENIDOS_H__
#define __TABLACONTENIDOS_H__



/* ----------------------------------------------------------------------------
 * Directivas de configuracion
 * ------------------------------------------------------------------------- */

#include <config.h>


#define TCONT_NING                           0

#define TCONT_NUMERO_CODCNT_AL               1
#define TCONT_NUMERO_CODCNT_ANUN             2
#define TCONT_NUMERO_CODCNT_AP1              3
#define TCONT_NUMERO_CODCNT_AP2              4
#define TCONT_NUMERO_CODCNT_AUP              5
#define TCONT_NUMERO_CODCNT_BRS              6
#define TCONT_NUMERO_CODCNT_CAL              7
#define TCONT_NUMERO_CODCNT_CALL             8
#define TCONT_NUMERO_CODCNT_CAM              9
#define TCONT_NUMERO_CODCNT_CAS              10
#define TCONT_NUMERO_CODCNT_CAT              11
#define TCONT_NUMERO_CODCNT_CCF              12
#define TCONT_NUMERO_CODCNT_CCI              13
#define TCONT_NUMERO_CODCNT_CE               14
#define TCONT_NUMERO_CODCNT_CH               15
#define TCONT_NUMERO_CODCNT_CL               16
#define TCONT_NUMERO_CODCNT_CLPA             17
#define TCONT_NUMERO_CODCNT_CL_EXP           18
#define TCONT_NUMERO_CODCNT_CM               19
#define TCONT_NUMERO_CODCNT_CP               20
#define TCONT_NUMERO_CODCNT_CP_ALO           21
#define TCONT_NUMERO_CODCNT_CQ               22
#define TCONT_NUMERO_CODCNT_CT               23
#define TCONT_NUMERO_CODCNT_CTL              24
#define TCONT_NUMERO_CODCNT_DV               25
#define TCONT_NUMERO_CODCNT_ENT              26
#define TCONT_NUMERO_CODCNT_EPP              27
#define TCONT_NUMERO_CODCNT_FG               28
#define TCONT_NUMERO_CODCNT_FX1              29
#define TCONT_NUMERO_CODCNT_FX2              30
#define TCONT_NUMERO_CODCNT_GX               31
#define TCONT_NUMERO_CODCNT_GY               32
#define TCONT_NUMERO_CODCNT_ID               33
#define TCONT_NUMERO_CODCNT_INSE             34
#define TCONT_NUMERO_CODCNT_LO               35
#define TCONT_NUMERO_CODCNT_LO1              36
#define TCONT_NUMERO_CODCNT_LO2              37
#define TCONT_NUMERO_CODCNT_LOP              38
#define TCONT_NUMERO_CODCNT_LOS              39
#define TCONT_NUMERO_CODCNT_MD               40
#define TCONT_NUMERO_CODCNT_ML1              41
#define TCONT_NUMERO_CODCNT_NM               42
#define TCONT_NUMERO_CODCNT_NO               43
#define TCONT_NUMERO_CODCNT_NUME             44
#define TCONT_NUMERO_CODCNT_NUV              45
#define TCONT_NUMERO_CODCNT_NV               46
#define TCONT_NUMERO_CODCNT_PA               47
#define TCONT_NUMERO_CODCNT_PG               48
#define TCONT_NUMERO_CODCNT_PI               49
#define TCONT_NUMERO_CODCNT_PR               50
#define TCONT_NUMERO_CODCNT_PROD             51
#define TCONT_NUMERO_CODCNT_PRP              52
#define TCONT_NUMERO_CODCNT_PV               53
#define TCONT_NUMERO_CODCNT_SI               54
#define TCONT_NUMERO_CODCNT_SIS              55
#define TCONT_NUMERO_CODCNT_TE               56
#define TCONT_NUMERO_CODCNT_TF1              57
#define TCONT_NUMERO_CODCNT_TFS              58
#define TCONT_NUMERO_CODCNT_TH               59
#define TCONT_NUMERO_CODCNT_TIP              60
#define TCONT_NUMERO_CODCNT_TM               61
#define TCONT_NUMERO_CODCNT_TP               62
#define TCONT_NUMERO_CODCNT_TQ               63
#define TCONT_NUMERO_CODCNT_TR               64
#define TCONT_NUMERO_CODCNT_TV               65
#define TCONT_NUMERO_CODCNT_VCA              66
#define TCONT_NUMERO_CODCNT_VMI              67
#define TCONT_NUMERO_CODCNT_VMO              68
#define TCONT_NUMERO_CODCNT_WB1              69
#define TCONT_NUMERO_CODCNT_ZON              70
#define TCONT_NUMERO_CODCNT_ZOS              71
#define TCONT_NUMERO_CODCNT_ZOX              72



/* ----------------------------------------------------------------------------
 * Declaracion de las funciones publicas del modulo
 * ------------------------------------------------------------------------- */

int buscarTipoContenido( const char *strCont,
                         size_t lgoCont );

char * buscarNombreContenido( int tipoCont );



#endif
