SET ECHO OFF
SET FEEDBACK OFF

CREATE OR REPLACE PACKAGE DATA_IFACE AS

	/* Declaracion del propietario de las tablas temporales 
	 *  que esta usando la aplicacion 
	 */

	K_TRUNC_ONWER_TABLAS CONSTANT VARCHAR2(30) := 'OPS$ROOT';

	/*
	 * Declaracion de tipos de dato publicos.
	 */

	--
	-- Tipos de dato comunes.
	--

	TYPE T_REC_CO_TEL IS RECORD
	(
		S_TIP_TELF          VARCHAR2(6),
		S_NC_TELF           TCL_TELEMPR.NC_TELF%TYPE
	);

	TYPE T_REC_CO_WEB IS RECORD
	(
		S_TIP_WEB           VARCHAR2(6),
		S_DI_WEB            TCL_WEBMAIL.DI_WEB_EMAIL%TYPE
	);

	TYPE T_CUR_CO_TEL  IS REF CURSOR RETURN T_REC_CO_TEL;
	TYPE T_CUR_CO_WEB  IS REF CURSOR RETURN T_REC_CO_WEB;

	--
	-- Tipos de dato para la extracci�n de P�ginas Amarillas.
	--

	TYPE T_REC_PA_EMP IS RECORD
	(
		S_CO_EMPRESA        TCL_ACTEMPR.CO_EMPRESA%TYPE,
		S_CO_ACTVAD         TCL_ACTEMPR.CO_ACTVAD%TYPE,
		S_TX_ACTVAD         TAA_ACTVAD.TX_ACTVAD%TYPE,
		S_TX_NOMBRE         VARCHAR2(200)
	);

 	TYPE T_REC_PA_NOM IS RECORD
	(
		S_TX_NOMBRE         VARCHAR2(200),
		S_TAMANO            NUMBER(3),
		S_OCURENCIA         NUMBER(3)
	);

	TYPE T_REC_PA_DIR IS RECORD
	(
		S_PG_PCONEX         TCL_DIREMPR.PG_PCONEX%TYPE,
		S_TIP_DIREC         VARCHAR2(13),
		S_PRES_VIA          VARCHAR2(1),
		S_DE_PROV           TAG_PROV.DE_PROV%TYPE,
		S_CO_POST_EMPRE     TCL_DIREMPR.CO_POST_EMPRE%TYPE,
		S_TX_TIPO_VIA_OFIC  TAG_CTPI.TX_TIPO_VIA_OFIC%TYPE,
		S_TX_CALLE_OFIC     TAG_CTPI.TX_CALLE_OFIC%TYPE,
		S_DI_NU_EMPRE       TCL_DIREMPR.DI_NU_EMPRE%TYPE,
		S_DI_RES_EMPRE      TCL_DIREMPR.DI_RES_EMPRE%TYPE,
		S_CALLE_LGA         VARCHAR2(130),
		S_CALLE_MAN         VARCHAR2(130),
		S_COD_INE           VARCHAR2(11),
		S_NU_COORD_GISX     TCL_DIREMPR.NU_COORD_GISX%TYPE,
		S_NU_COORD_GISY     TCL_DIREMPR.NU_COORD_GISY%TYPE,
		S_CR_FLAG_GIS       VARCHAR2(1)
	);

	TYPE T_CUR_PA_EMP  IS REF CURSOR RETURN T_REC_PA_EMP;
	TYPE T_CUR_PA_NOM  IS REF CURSOR RETURN T_REC_PA_NOM;
	TYPE T_CUR_PA_DIR  IS REF CURSOR RETURN T_REC_PA_DIR;

	--
	-- Tipos de dato para la extracci�n de Restaurantes.
	--

	TYPE T_REC_RS_EMP IS RECORD
	(
		S_CO_EMPRESA        TCL_ACTEMPR.CO_EMPRESA%TYPE,
		S_CO_ACTVAD         TCL_ACTEMPR.CO_ACTVAD%TYPE,
		S_TX_ACTVAD         TAA_ACTVAD.TX_ACTVAD%TYPE,
		S_TX_NOMBRE         VARCHAR2(200),
		S_PR_MEDIO          VARCHAR2(10),
		S_PR_MENU_DIA       VARCHAR2(10),
		S_SR_DOMICILIO      VARCHAR2(30),
		S_DV_INF_ADI_AGR    VARCHAR2(2000)
	);

	TYPE T_REC_RS_DIR IS RECORD
	(
		S_PG_PCONEX         TCL_DIREMPR.PG_PCONEX%TYPE,
		S_TIP_DIREC         VARCHAR2(13),
		S_PRES_VIA          VARCHAR2(1),
		S_DE_PROV           TAG_PROV.DE_PROV%TYPE,
		S_CO_POST_EMPRE     TCL_DIREMPR.CO_POST_EMPRE%TYPE,
		S_TX_TIPO_VIA_OFIC  TAG_CTPI.TX_TIPO_VIA_OFIC%TYPE,
		S_TX_CALLE_OFIC     TAG_CTPI.TX_CALLE_OFIC%TYPE,
		S_DI_NU_EMPRE       TCL_DIREMPR.DI_NU_EMPRE%TYPE,
		S_DI_RES_EMPRE      TCL_DIREMPR.DI_RES_EMPRE%TYPE,
		S_CALLE_LGA         VARCHAR2(130),
		S_CALLE_MAN         VARCHAR2(130),
		S_COD_INE           VARCHAR2(11),
		S_NU_COORD_GISX     TCL_DIREMPR.NU_COORD_GISX%TYPE,
		S_NU_COORD_GISY     TCL_DIREMPR.NU_COORD_GISY%TYPE,
		S_CR_FLAG_GIS       VARCHAR2(1)
	);

	TYPE T_REC_RS_ADI IS RECORD
	(
		S_CT_INF_EXT        TAP_VALINFA.CT_INF_EXT%TYPE,
		S_ID_INF_EXT        TAP_VALINFA.ID_INF_EXT%TYPE,
		S_DV_INF_ADI        TAP_VALINFA.DV_INF_ADI%TYPE
	);

	TYPE T_CUR_RS_EMP  IS REF CURSOR RETURN T_REC_RS_EMP;
	TYPE T_CUR_RS_DIR  IS REF CURSOR RETURN T_REC_RS_DIR;
	TYPE T_CUR_RS_ADI  IS REF CURSOR RETURN T_REC_RS_ADI;

	--
	-- Tipos de dato para la extracci�n de Alojamientos.
	--

	TYPE T_REC_AL_EMP IS RECORD
	(
		S_CO_EMPRESA        TCL_ACTEMPR.CO_EMPRESA%TYPE,
		S_CO_ACTVAD         TCL_ACTEMPR.CO_ACTVAD%TYPE,
		S_PG_PCONEX         TCL_PCONEX.PG_PCONEX%TYPE,
		S_TX_NOMBRE         VARCHAR2(200)
	);

	TYPE T_REC_AL_DIR IS RECORD
	(
		S_PG_PCONEX         TCL_DIREMPR.PG_PCONEX%TYPE,
		S_TIP_DIREC         VARCHAR2(13),
		S_PRES_VIA          VARCHAR2(1),
		S_DE_PROV           TAG_PROV.DE_PROV%TYPE,
		S_CO_POST_EMPRE     TCL_DIREMPR.CO_POST_EMPRE%TYPE,
		S_TX_TIPO_VIA_OFIC  TAG_CTPI.TX_TIPO_VIA_OFIC%TYPE,
		S_TX_CALLE_OFIC     TAG_CTPI.TX_CALLE_OFIC%TYPE,
		S_DI_NU_EMPRE       TCL_DIREMPR.DI_NU_EMPRE%TYPE,
		S_DI_RES_EMPRE      TCL_DIREMPR.DI_RES_EMPRE%TYPE,
		S_CALLE_LGA         VARCHAR2(130),
		S_CALLE_MAN         VARCHAR2(130),
		S_COD_INE           VARCHAR2(11),
		S_NU_COORD_GISX     TCL_DIREMPR.NU_COORD_GISX%TYPE,
		S_NU_COORD_GISY     TCL_DIREMPR.NU_COORD_GISY%TYPE,
		S_CR_FLAG_GIS       VARCHAR2(1)
	);

	TYPE T_REC_AL_ADI IS RECORD
	(
		S_CT_INF_EXT        TAP_VALINFA.CT_INF_EXT%TYPE,
		S_ID_INF_EXT        TAP_VALINFA.ID_INF_EXT%TYPE,
		S_DV_INF_ADI        TAP_VALINFA.DV_INF_ADI%TYPE
	);

	TYPE T_CUR_AL_EMP  IS REF CURSOR RETURN T_REC_AL_EMP;
	TYPE T_CUR_AL_DIR  IS REF CURSOR RETURN T_REC_AL_DIR;
	TYPE T_CUR_AL_ADI  IS REF CURSOR RETURN T_REC_AL_ADI;

	--
	-- Tipos de dato para la extracci�n de P�ginas Blanncas.
	--

	TYPE T_REC_PB_ABO IS RECORD
	(
		S_ID_ABONO          TOP_IGBOL.ID_ABONO%TYPE,
		S_NO_NOMBRE         TOP_IGBOL.NO_NOMBRE%TYPE,
		S_NO_APE1           TOP_IGBOL.NO_APE1%TYPE,
		S_NO_APE2           TOP_IGBOL.NO_APE2%TYPE,
		S_CT_ABONO          NUMBER(1),
		S_TX_CALLE_OFIC     TAG_CTPI.TX_CALLE_OFIC%TYPE,
		S_DI_NU_TELFO       TOP_IGBOL.DI_NU_TELFO%TYPE,
		S_DI_RES_TELFO      TOP_IGBOL.DI_RES_TELFO%TYPE,
		S_CO_POST_TELFO     TOP_IGBOL.CO_POST_TELFO%TYPE,
		S_DE_PROV           TAG_PROV.DE_PROV%TYPE,
		S_COD_INE           VARCHAR2(11),
		S_NC_TELF           TOP_TELFBOL.NC_TELF%TYPE,
		S_NC_CTL            VARCHAR2(1),
		S_NC_TELF2          TOP_TELFBOL.NC_TELF%TYPE
	);

	TYPE T_CUR_PB_ABO  IS REF CURSOR RETURN T_REC_PB_ABO;

	/*
	 * Declaracion de las constantes publicas.
	 */

	K_AMBI_APA_PA  CONSTANT TAP_CONDPUB.CO_AMBI_APA%TYPE := 'PA';
	K_AMBI_APA_RS  CONSTANT TAP_CONDPUB.CO_AMBI_APA%TYPE := 'PR';
	K_AMBI_APA_AL  CONSTANT TAP_CONDPUB.CO_AMBI_APA%TYPE := 'PH';

	K_INF_ADI_CAT CONSTANT TAP_VALINFA.CV_INF_ADI%TYPE := '03';
	K_INF_ADI_CP  CONSTANT TAP_VALINFA.CV_INF_ADI%TYPE := '05';
	K_INF_ADI_KE  CONSTANT TAP_VALINFA.CV_INF_ADI%TYPE := '11';
	K_INF_ADI_KH  CONSTANT TAP_VALINFA.CV_INF_ADI%TYPE := '07';
	K_INF_ADI_KM  CONSTANT TAP_VALINFA.CV_INF_ADI%TYPE := '21';
	K_INF_ADI_KQ  CONSTANT TAP_VALINFA.CV_INF_ADI%TYPE := '09';
	K_INF_ADI_PA  CONSTANT TAP_VALINFA.CV_INF_ADI%TYPE := '11';
	K_INF_ADI_SI  CONSTANT TAP_VALINFA.CV_INF_ADI%TYPE := '10';
	K_INF_ADI_TH  CONSTANT TAP_VALINFA.CV_INF_ADI%TYPE := '07';
	K_INF_ADI_ZON CONSTANT TAP_VALINFA.CV_INF_ADI%TYPE := '08';

	/*
	 * Declaracion de las funciones publicas.
	 */

	--
	-- Funcion de correccion del texto, (eliminacion de acentos, ...).
	--

	FUNCTION F_CO_TEXTO ( P_TEXTO IN VARCHAR2)
	RETURN  VARCHAR2;

	/*
	 * Declaracion de los procedimientos publicos.
	 */

	--
	-- Procedimientos de extracci�n comunes.
	--
	PROCEDURE P_CO_TEL ( P_CUR_SALIDA   OUT    T_CUR_CO_TEL,
	                     P_CO_EMPRESA   IN     TCL_TELEMPR.CO_EMPRESA%TYPE,
	                     P_PG_PCONEX    IN     TCL_TELEMPR.PG_PCONEX%TYPE );

	PROCEDURE P_CO_WEB ( P_CUR_SALIDA   OUT    T_CUR_CO_WEB,
	                     P_CO_EMPRESA   IN     TCL_WEBMAIL.CO_EMPRESA%TYPE,
	                     P_PG_PCONEX    IN     TCL_WEBMAIL.PG_PCONEX%TYPE );

	--
	-- Procedimientos de extracci�n para P�ginas Amarillas.
	--

	PROCEDURE P_PA_EMP ( P_CUR_SALIDA   OUT   T_CUR_PA_EMP,
	                     P_CASO         IN    NUMBER,
	                     P_MIN_EMPRESA  IN    TCL_ACTEMPR.CO_EMPRESA%TYPE := NULL,
	                     P_MAX_EMPRESA  IN    TCL_ACTEMPR.CO_EMPRESA%TYPE := NULL );

	PROCEDURE P_PA_NOM ( P_CUR_SALIDA   OUT   T_CUR_PA_NOM,
	                     P_CO_EMPRESA   IN    TCL_ACTEMPR.CO_EMPRESA%TYPE,
	                     P_CO_ACTVAD    IN    TCL_ACTEMPR.CO_ACTVAD%TYPE );

	PROCEDURE P_PA_DIR ( P_CUR_SALIDA   OUT   T_CUR_PA_DIR,
	                     P_CO_EMPRESA   IN    TCL_ACTEMPR.CO_EMPRESA%TYPE,
	                     P_CO_ACTVAD    IN    TCL_ACTEMPR.CO_ACTVAD%TYPE,
	                     P_TX_NOMBRE    IN    VARCHAR2 );

	--
	-- Procedimientos de carga de la tabla de uso temporal (TBR_ASOSREST) para Restaurantes.
	--

	PROCEDURE P_RS_AGRADI ( P_MIN_EMPRESA  IN    TCL_PCONEX.CO_EMPRESA%TYPE := NULL,
	                        P_MAX_EMPRESA  IN    TCL_PCONEX.CO_EMPRESA%TYPE := NULL );

	--
	-- Procedimientos de extracci�n para Restaurantes.
	--

	PROCEDURE P_RS_EMP ( P_CUR_SALIDA   OUT   T_CUR_RS_EMP,
	                     P_MIN_EMPRESA  IN    TCL_ACTEMPR.CO_EMPRESA%TYPE := NULL,
	                     P_MAX_EMPRESA  IN    TCL_ACTEMPR.CO_EMPRESA%TYPE := NULL );

	PROCEDURE P_RS_DIR ( P_CUR_SALIDA   OUT   T_CUR_RS_DIR,
	                     P_CO_EMPRESA   IN    TCL_ACTEMPR.CO_EMPRESA%TYPE,
	                     P_CO_ACTVAD    IN    TCL_ACTEMPR.CO_ACTVAD%TYPE,
	                     P_TX_NOMBRE  IN    VARCHAR2,
	                     P_PR_MEDIO     IN    VARCHAR2,
	                     P_PR_MENU_DIA  IN    VARCHAR2,
	                     P_SR_DOMICILIO IN    VARCHAR2,
	                     P_DV_VINFA_AGR IN    VARCHAR2 );

	PROCEDURE P_RS_ADI ( P_CUR_SALIDA   OUT   T_CUR_RS_ADI,
	                     P_CO_EMPRESA   IN    TCL_ACTEMPR.CO_EMPRESA%TYPE,
	                     P_CO_ACTVAD    IN    TCL_ACTEMPR.CO_ACTVAD%TYPE,
	                     P_TX_NOMBRE  IN    VARCHAR2,
	                     P_PR_MEDIO     IN    VARCHAR2,
	                     P_PR_MENU_DIA  IN    VARCHAR2,
	                     P_SR_DOMICILIO IN    VARCHAR2,
	                     P_DV_VINFA_AGR IN    VARCHAR2,
	                     P_CO_INF_ADI   IN    TCL_PCOVINFA.CO_INF_ADI%TYPE );

        /* Fase II - Procedimiento de extraccion de valoraciones de Restaurantes */  	
        PROCEDURE P_RS_ADI_VAL ( P_CUR_SALIDA     OUT   T_CUR_RS_ADI,
	                         P_CO_EMPRESA     IN    TCL_ACTEMPR.CO_EMPRESA%TYPE,
	                         P_CO_ACTVAD      IN    TCL_ACTEMPR.CO_ACTVAD%TYPE,
	                         P_TX_NOMBRE      IN    VARCHAR2,
	                         P_PR_MEDIO       IN    VARCHAR2,
	                         P_PR_MENU_DIA    IN    VARCHAR2,
	                         P_SR_DOMICILIO   IN    VARCHAR2,
	                         P_DV_VINFA_AGR   IN    VARCHAR2,
	                         P_CO_INF_ADI     IN    TCL_PCOVINFA.CO_INF_ADI%TYPE,
                                 P_CV_INF_ADI_INF IN    NUMBER,
                                 P_CV_INF_ADI_SUP IN    NUMBER);

	--
	-- Procedimientos de extracci�n para Alojamientos.
	--

	PROCEDURE P_AL_EMP ( P_CUR_SALIDA   OUT   T_CUR_AL_EMP,
	                     P_MIN_EMPRESA  IN    TCL_ACTEMPR.CO_EMPRESA%TYPE := NULL,
	                     P_MAX_EMPRESA  IN    TCL_ACTEMPR.CO_EMPRESA%TYPE := NULL );

	PROCEDURE P_AL_DIR ( P_CUR_SALIDA   OUT   T_CUR_AL_DIR,
	                     P_CO_EMPRESA   IN    TCL_DIREMPR.CO_EMPRESA%TYPE,
	                     P_PG_PCONEX    IN    TCL_DIREMPR.PG_PCONEX%TYPE );

	PROCEDURE P_AL_ADI ( P_CUR_SALIDA   OUT    T_CUR_AL_ADI,
	                     P_CO_EMPRESA   IN     TCL_PCOVINFA.CO_EMPRESA%TYPE,
	                     P_PG_PCONEX    IN     TCL_PCOVINFA.PG_PCONEX%TYPE,
	                     P_CO_INF_ADI   IN     TCL_PCOVINFA.CO_INF_ADI%TYPE );

	--
	-- Procedimientos de extracci�n para P�ginas Blancas.
	--

	PROCEDURE P_PB_ABO ( P_CUR_SALIDA   OUT   T_CUR_PB_ABO,
	                     P_ID_PROV      IN    VARCHAR2 := NULL,
	                     P_MIN_ABONO    IN    TOP_IGBOL.ID_ABONO%TYPE := NULL,
	                     P_MAX_ABONO    IN    TOP_IGBOL.ID_ABONO%TYPE := NULL );

	PROCEDURE P_PB_TFSEC;
   
	--
	-- Procedimiento general para truncar las tablas temporales usadas
	-- 

	PROCEDURE P_GR_TRUNC ( P_OWNER IN VARCHAR2,
	                       P_TABLE_NAME IN VARCHAR2 );

END DATA_IFACE;
/

SHOW ERRORS

CREATE OR REPLACE PACKAGE BODY DATA_IFACE AS

	/*
	 * Declaracion de las funciones publicas.
	 */

	--
	-- Funcion de correccion del texto, (eliminacion de acentos, ...).
	--

	FUNCTION F_CO_TEXTO ( P_TEXTO IN VARCHAR2)
	RETURN  VARCHAR2
	IS
		V_TEXTO VARCHAR2(2000);
	BEGIN
		V_TEXTO := REPLACE (P_TEXTO,'�','A');
		V_TEXTO := REPLACE (V_TEXTO,'�','E');
		V_TEXTO := REPLACE (V_TEXTO,'�','I');
		V_TEXTO := REPLACE (V_TEXTO,'�','O');
		V_TEXTO := REPLACE (V_TEXTO,'�','U');

		RETURN V_TEXTO ;
	END;

	/*
	 * Definicion de los procedimientos publicos.
	 */

	--
	-- Procedimientos de extraccion comunes.
	--

	PROCEDURE P_CO_TEL ( P_CUR_SALIDA   OUT    T_CUR_CO_TEL,
	                     P_CO_EMPRESA   IN     TCL_TELEMPR.CO_EMPRESA%TYPE,
	                     P_PG_PCONEX    IN     TCL_TELEMPR.PG_PCONEX%TYPE )
	AS
	BEGIN
		OPEN P_CUR_SALIDA FOR
			SELECT 'TF1' "TEXTO", NVL(A.TELF,A.FAX) "NUMERO"
			  FROM TBR_TELEMPR A
			 WHERE A.CO_EMPRESA  = P_CO_EMPRESA
			   AND A.PG_PCONEX   = P_PG_PCONEX
			   AND ROWNUM=1
			UNION ALL
			SELECT 'FX1' "TEXTO",
			       T.NC_TELF "NUMERO"
			  FROM TCL_TELEMPR T
			 WHERE T.CO_EMPRESA  = P_CO_EMPRESA
			   AND T.PG_PCONEX   = P_PG_PCONEX
			   AND T.OD_PREFE_PUBLI = ( SELECT MIN(P.OD_PREFE_PUBLI)
			                              FROM TCL_TELEMPR P
			                             WHERE P.CO_EMPRESA=T.CO_EMPRESA
			                               AND P.PG_PCONEX=T.PG_PCONEX
			                               AND P.OD_PREFE_PUBLI!=9999
			                               AND P.CR_FAX='S')
			   AND T.CR_BAJA_REG IS NULL   /* Baja l�gica */
			   AND T.CR_FAX = 'S';
	END;


	PROCEDURE P_CO_WEB ( P_CUR_SALIDA   OUT    T_CUR_CO_WEB,
	                     P_CO_EMPRESA   IN     TCL_WEBMAIL.CO_EMPRESA%TYPE,
	                     P_PG_PCONEX    IN     TCL_WEBMAIL.PG_PCONEX%TYPE )
	AS
	BEGIN
		OPEN P_CUR_SALIDA FOR
			  SELECT DECODE( W.CR_WEB_EMAIL, 'N', 'ML', 'S', 'WB' ) ||
			             W.OD_PREFE_PUBLI,
			         W.DI_WEB_EMAIL
			    FROM TCL_WEBMAIL W
			   WHERE W.CO_EMPRESA  = P_CO_EMPRESA
			     AND W.PG_PCONEX   = P_PG_PCONEX
			     AND W.CR_BAJA_REG IS NULL   /* Baja l�gica */
			ORDER BY W.CO_EMPRESA,
			         W.PG_PCONEX,
			         W.CR_WEB_EMAIL,
			         W.OD_PREFE_PUBLI;
	END;

	--
	-- Procedimientos de extracci�n para P�ginas Amarillas.
	-- CASO 1:
	-- A este caso pertenencen todos los registros cuya actividad
	-- principal no esta incluida en la table TBR_ACTICOMER, y cuya
	-- primera letra del NIF sea distinta de P,Q,S.
	-- CASO 2:
	-- A este caso pertenencen todos los registros cuya actividad
	-- principal esta incluida en la table TBR_ACTICOMER, � cuya
	-- primera letra del NIF sea igual a P,Q,S.
	--

	PROCEDURE P_PA_EMP ( P_CUR_SALIDA   OUT   T_CUR_PA_EMP,
	                     P_CASO         IN    NUMBER,
	                     P_MIN_EMPRESA  IN    TCL_ACTEMPR.CO_EMPRESA%TYPE := NULL,
	                     P_MAX_EMPRESA  IN    TCL_ACTEMPR.CO_EMPRESA%TYPE := NULL )
	AS
	BEGIN
		IF P_CASO = 1 THEN

			OPEN P_CUR_SALIDA FOR
				SELECT DISTINCT
				       PC.CO_EMPRESA,
				       AC.CO_ACTVAD,
				       AA.TX_ACTVAD,
				       NULL
				  FROM TCL_PCONEX PC,
				       TCL_NIF NF,
				       TCL_ACTEMPR AC,
				       TAA_ACTVAD AA
				 WHERE PC.CO_EMPRESA = AC.CO_EMPRESA
				   AND PC.CO_EMPRESA = NF.CO_EMPRESA
				   AND AC.CO_ACTVAD  = AA.CO_ACTVAD
				   AND AC.CO_ACTVAD NOT IN (SELECT PB.CO_ACTVAD
				                              FROM TBR_ACTINOPUB PB)
				   AND AC.CO_ACTVAD NOT IN (SELECT CM.CO_ACTVAD
				                              FROM TBR_ACTICOMER CM)
				   AND SUBSTR( NC_NIF, 1, 1 ) NOT IN ('P','Q','S')
				   AND EXISTS (SELECT NULL
						             FROM TCL_DIREMPR DR
			                  WHERE DR.CO_EMPRESA = PC.CO_EMPRESA
			                    AND DR.PG_PCONEX = PC.PG_PCONEX
			                    AND DR.CR_BAJA_REG IS NULL)
			     AND AC.PG_ACTVAD_EMPRE = (SELECT /*+ INDEX(AP XCL_ACTPCON_PK) */ 
			                                      AP.PG_ACTVAD_EMPRE
			                                 FROM TCL_ACTPCON AP
			                                WHERE AP.CO_EMPRESA = PC.CO_EMPRESA
			                                  AND AP.PG_PCONEX = PC.PG_PCONEX
			                                  AND AP.OD_PRIORIDAD = (
			                                        SELECT MIN(AP1.OD_PRIORIDAD)
			                                          FROM TCL_ACTPCON AP1
			                                         WHERE AP.CO_EMPRESA = AP1.CO_EMPRESA
			                                           AND AP.PG_PCONEX = AP1.PG_PCONEX
			                                           AND AP1.CR_BAJA_REG IS NULL)
			                                  AND AP.CR_BAJA_REG IS NULL)
			     AND EXISTS (SELECT NULL
			                   FROM TCL_PCOCPU PU, 
                           TAP_CONDPUB TP
			                  WHERE PU.CO_COND_PUBLI = TP.CO_COND_PUBLI
			                    AND TP.CO_PROD = 'PAO'
			                    AND TP.CO_AMBI_APA = K_AMBI_APA_PA
			                    AND PU.CO_EMPRESA = PC.CO_EMPRESA
			                    AND PU.PG_PCONEX = PC.PG_PCONEX
			                    AND PU.CR_BAJA_REG IS NULL
			                    AND TP.CR_BAJA_REG IS NULL)
			     AND EXISTS ( SELECT NULL 
			                    FROM TCL_TELEMPR T
			                   WHERE T.CO_EMPRESA  = PC.CO_EMPRESA
			                     AND T.PG_PCONEX = PC.PG_PCONEX
			                     AND T.OD_PREFE_PUBLI != 9999
			                     AND T.CR_BAJA_REG IS NULL ) 
			     AND PC.CR_BAJA_REG  IS NULL /* Baja logica */
			     AND NF.CR_BAJA_REG  IS NULL /* Baja logica */
			     AND NF.FE_FIN_VIGEN IS NULL /* Baja logica */
			     AND AC.CR_BAJA_REG  IS NULL /* Baja logica */
			     AND AA.CR_BAJA_REG  IS NULL /* Baja logica */
			     AND PC.CO_EMPRESA BETWEEN NVL( P_MIN_EMPRESA, 0 )
			     AND NVL( P_MAX_EMPRESA+1, 999999999 );

		ELSE

			OPEN P_CUR_SALIDA FOR
				SELECT DISTINCT
				       PC.CO_EMPRESA,
				       AC.CO_ACTVAD,
				       AA.TX_ACTVAD,
				       NVL(DECODE( PC.NO_PUBLI,
						         NULL,  DECODE( PC.NO_COMER,
									             NULL, PC.NO_NOM_PCONEX  || ' ' ||
										                  PC.NO_APE1_PCONEX || ' ' ||
										                  PC.NO_APE2_PCONEX,
										            PC.NO_COMER ),
							        PC.NO_PUBLI ),'xx')
				 FROM TCL_PCONEX PC,
				      TCL_NIF NF,
				      TCL_ACTEMPR AC,
				      TAA_ACTVAD AA
				WHERE PC.CO_EMPRESA = AC.CO_EMPRESA
				  AND PC.CO_EMPRESA = NF.CO_EMPRESA
				  AND AC.CO_ACTVAD  = AA.CO_ACTVAD
				  AND AC.CO_ACTVAD NOT IN (SELECT PB.CO_ACTVAD FROM TBR_ACTINOPUB PB)
				  AND ( AC.CO_ACTVAD IN ( SELECT CM.CO_ACTVAD FROM TBR_ACTICOMER CM )
			         OR SUBSTR(NC_NIF,1,1) IN ('P','Q','S') )
			   AND EXISTS (SELECT NULL
			                 FROM TCL_DIREMPR DR
			                WHERE DR.CO_EMPRESA = PC.CO_EMPRESA
			                  AND DR.PG_PCONEX = PC.PG_PCONEX
			                  AND DR.CR_BAJA_REG IS NULL)
			   AND AC.PG_ACTVAD_EMPRE = (SELECT /*+ INDEX(AP XCL_ACTPCON_PK) */ 
			                                    AP.PG_ACTVAD_EMPRE
			                               FROM TCL_ACTPCON AP
			                              WHERE AP.CO_EMPRESA = PC.CO_EMPRESA
			                                AND AP.PG_PCONEX = PC.PG_PCONEX
			                                AND AP.OD_PRIORIDAD = (
			                                       SELECT MIN(AP1.OD_PRIORIDAD)
			                                         FROM TCL_ACTPCON AP1
			                                        WHERE AP.CO_EMPRESA = AP1.CO_EMPRESA
			                                          AND AP.PG_PCONEX = AP1.PG_PCONEX
			                                          AND AP1.CR_BAJA_REG IS NULL)
			                                AND AP.CR_BAJA_REG IS NULL)
			   AND EXISTS (SELECT NULL
			                 FROM TCL_PCOCPU PU,
			                      TAP_CONDPUB TP
			                WHERE PU.CO_COND_PUBLI = TP.CO_COND_PUBLI
			                  AND TP.CO_PROD = 'PAO'
			                  AND TP.CO_AMBI_APA = K_AMBI_APA_PA
			                  AND PU.CO_EMPRESA = PC.CO_EMPRESA
			                  AND PU.PG_PCONEX = PC.PG_PCONEX
			                  AND PU.CR_BAJA_REG IS NULL
			                  AND TP.CR_BAJA_REG IS NULL)
			   AND EXISTS ( SELECT NULL
			                  FROM TCL_TELEMPR T
			                 WHERE T.CO_EMPRESA  = PC.CO_EMPRESA
			                   AND T.PG_PCONEX = PC.PG_PCONEX
			                   AND T.OD_PREFE_PUBLI != 9999
			                   AND T.CR_BAJA_REG IS NULL ) 
			   AND PC.CR_BAJA_REG IS NULL /* Baja logica */
			   AND NF.CR_BAJA_REG IS NULL /* Baja logica */
			   AND NF.FE_FIN_VIGEN IS NULL /* Baja logica */
			   AND AC.CR_BAJA_REG IS NULL /* Baja logica */
			   AND AA.CR_BAJA_REG IS NULL /* Baja logica */
			   AND PC.CO_EMPRESA BETWEEN NVL( P_MIN_EMPRESA, 0 )
			   AND NVL( P_MAX_EMPRESA+1, 999999999 );

		END IF;
	END;

	PROCEDURE P_PA_NOM ( P_CUR_SALIDA   OUT   T_CUR_PA_NOM,
	                     P_CO_EMPRESA   IN    TCL_ACTEMPR.CO_EMPRESA%TYPE,
	                     P_CO_ACTVAD    IN    TCL_ACTEMPR.CO_ACTVAD%TYPE )
	AS
	BEGIN
		OPEN P_CUR_SALIDA FOR
		    SELECT F_CO_TEXTO(nvl(DECODE(PC.NO_PUBLI,NULL,
                                          decode(pc.no_comer,NULL,
			                         PC.NO_NOM_PCONEX  || ' ' ||
			                         PC.NO_APE1_PCONEX || ' ' ||
			                         PC.NO_APE2_PCONEX,
                                                 pc.no_comer),
			                         PC.NO_PUBLI),'xx')),
		                nvl(length(DECODE(PC.NO_PUBLI,NULL,
                                           decode(pc.no_comer,NULL,
			                          PC.NO_NOM_PCONEX  || ' ' ||
			                          PC.NO_APE1_PCONEX || ' ' ||
			                          PC.NO_APE2_PCONEX,
                                                  pc.no_comer),
			                          PC.NO_PUBLI)),0),
--			        NVL(LENGTH(DECODE(PC.NO_PUBLI,NULL,
--			                          PC.NO_NOM_PCONEX  || ' ' ||
--			                          PC.NO_APE1_PCONEX || ' ' ||
--			                          PC.NO_APE2_PCONEX,
--			                          PC.NO_PUBLI)),0),
			        COUNT(*)
			   FROM TCL_PCONEX PC
			  WHERE PC.CO_EMPRESA = P_CO_EMPRESA
			    AND PC.PG_PCONEX IN (SELECT /*+ INDEX(AE XCL_ACTEMPR_PK) */
			                                AP.PG_PCONEX
			                           FROM TCL_ACTPCON AP,
			                                TCL_ACTEMPR AE
			                          WHERE AE.CO_EMPRESA = PC.CO_EMPRESA
			                            AND AE.CO_ACTVAD  = P_CO_ACTVAD
			                            AND AP.CO_EMPRESA = AE.CO_EMPRESA
			                            AND AP.PG_ACTVAD_EMPRE = AE.PG_ACTVAD_EMPRE
			                            AND AP.CR_BAJA_REG IS NULL
			                            AND AE.CR_BAJA_REG IS NULL)
			    AND EXISTS (SELECT NULL
			                  FROM TCL_PCOCPU PU,
			                       TAP_CONDPUB TP
			                 WHERE PU.CO_COND_PUBLI = TP.CO_COND_PUBLI
			                   AND TP.CO_PROD = 'PAO'
			                   AND TP.CO_AMBI_APA = K_AMBI_APA_PA
			                   AND PU.CO_EMPRESA = PC.CO_EMPRESA
			                   AND PU.PG_PCONEX = PC.PG_PCONEX
			                   AND PU.CR_BAJA_REG IS NULL
			                   AND TP.CR_BAJA_REG IS NULL)
			    AND EXISTS ( SELECT NULL
			            FROM TCL_TELEMPR T
			           WHERE T.CO_EMPRESA  = PC.CO_EMPRESA
			             AND T.PG_PCONEX = PC.PG_PCONEX
			             AND T.OD_PREFE_PUBLI != 9999
			             AND T.CR_BAJA_REG IS NULL ) 
			    AND PC.CR_BAJA_REG IS NULL /* Baja logica */
		            group by DECODE(PC.NO_PUBLI,NULL,
                                          decode(pc.no_comer,NULL,
			                         PC.NO_NOM_PCONEX  || ' ' ||
			                         PC.NO_APE1_PCONEX || ' ' ||
			                         PC.NO_APE2_PCONEX,
                                                 pc.no_comer),
			                         PC.NO_PUBLI),
		                length(DECODE(PC.NO_PUBLI,NULL,
                                           decode(pc.no_comer,NULL,
			                          PC.NO_NOM_PCONEX  || ' ' ||
			                          PC.NO_APE1_PCONEX || ' ' ||
			                          PC.NO_APE2_PCONEX,
                                                  pc.no_comer),
			                          PC.NO_PUBLI))
--			  GROUP BY DECODE(PC.NO_PUBLI,NULL,
--			                  PC.NO_NOM_PCONEX  || ' ' ||
--			                  PC.NO_APE1_PCONEX || ' ' ||
--			                  PC.NO_APE2_PCONEX,
--			                  PC.NO_PUBLI),
--			    LENGTH(DECODE(PC.NO_PUBLI,NULL,
--			                  PC.NO_NOM_PCONEX  || ' ' ||
--			                  PC.NO_APE1_PCONEX || ' ' ||
--			                  PC.NO_APE2_PCONEX,
--			                  PC.NO_PUBLI))
			  ORDER BY 3 DESC , 2 DESC;
	 END;


	PROCEDURE P_PA_DIR ( P_CUR_SALIDA   OUT   T_CUR_PA_DIR,
	                     P_CO_EMPRESA   IN    TCL_ACTEMPR.CO_EMPRESA%TYPE,
	                     P_CO_ACTVAD    IN    TCL_ACTEMPR.CO_ACTVAD%TYPE,
	                     P_TX_NOMBRE    IN    VARCHAR2 )
	AS
	BEGIN
		IF P_TX_NOMBRE IS NULL THEN

			OPEN P_CUR_SALIDA FOR
				SELECT D.PG_PCONEX,
				       DECODE( D.CO_PAIS, '000', 'NACIONAL', '*********' ),
				       'M',
				       F_CO_TEXTO(P.DE_PROV),
				       D.CO_POST_EMPRE,
				       C.TX_TIPO_VIA_OFIC,
				       C.TX_CALLE_OFIC,
				       F_CO_TEXTO(D.DI_NU_EMPRE),
				       F_CO_TEXTO(D.DI_RES_EMPRE),
				       C.TX_TIPO_VIA_OFIC || ' ' || C.TX_CALLE_OFIC,
				       C.TX_TIPO_VIA_OFIC || ' ' || C.TX_CALLE_OFIC || ', '
					      || F_CO_TEXTO(D.DI_NU_EMPRE),
				       D.CO_PROV || D.CO_MUNI || D.CO_ENT_COLEC ||
					      D.CO_ENT_SINGU || '00',
				       DECODE( T.NU_COORD_GISX, 0, NULL, T.NU_COORD_GISX ),
				       DECODE( T.NU_COORD_GISY, 0, NULL, T.NU_COORD_GISY ),
				       DECODE( T.NU_COORD_GISX + T.NU_COORD_GISY, 
			                  NULL, 'N', 0, 'N', 'S' )
				  FROM TCL_DIREMPR D,
				       TAG_PROV P,
				       TAG_CTPI C,
				       TMP_DIREMPR_GIS T
				 WHERE D.CO_EMPRESA = P_CO_EMPRESA
				   AND D.PG_PCONEX IN (SELECT /*+ INDEX(AE XCL_ACTEMPR_PK) */
			                                 AP.PG_PCONEX
			                            FROM TCL_ACTPCON AP,
			                                 TCL_ACTEMPR AE
			                           WHERE AE.CO_EMPRESA = D.CO_EMPRESA
			                             AND AE.CO_ACTVAD  = P_CO_ACTVAD
			                             AND AP.CO_EMPRESA = AE.CO_EMPRESA
			                             AND AP.PG_ACTVAD_EMPRE = AE.PG_ACTVAD_EMPRE
			                             AND AP.CR_BAJA_REG IS NULL
			                             AND AE.CR_BAJA_REG IS NULL)
				   AND EXISTS (SELECT NULL
			                    FROM TCL_PCOCPU PU,
			                         TAP_CONDPUB TP
			                   WHERE PU.CO_COND_PUBLI = TP.CO_COND_PUBLI
			                     AND TP.CO_PROD       = 'PAO'
			                     AND TP.CO_AMBI_APA   = K_AMBI_APA_PA
			                     AND PU.CO_EMPRESA    = D.CO_EMPRESA
			                     AND PU.PG_PCONEX     = D.PG_PCONEX
			                     AND PU.CR_BAJA_REG IS NULL
			                     AND TP.CR_BAJA_REG IS NULL)
			      AND EXISTS ( SELECT NULL
			                     FROM TCL_TELEMPR T
			                    WHERE T.CO_EMPRESA  = D.CO_EMPRESA
			                      AND T.PG_PCONEX = D.PG_PCONEX
			                      AND T.OD_PREFE_PUBLI != 9999
			                      AND T.CR_BAJA_REG IS NULL )  
				   AND P.CO_PAIS      = D.CO_PAIS
				   AND P.CO_PROV      = D.CO_PROV
				   AND C.CO_PAIS      = D.CO_PAIS
				   AND C.CO_PROV      = D.CO_PROV
				   AND C.CO_MUNI      = D.CO_MUNI
				   AND C.CO_ENT_COLEC = D.CO_ENT_COLEC
				   AND C.CO_ENT_SINGU = D.CO_ENT_SINGU
				   AND C.CO_NUCLEO    = D.CO_NUCLEO
				   AND C.CO_CALLE     = D.CO_VIA_INE
				   AND T.CO_EMPRESA(+)= D.CO_EMPRESA
				   AND T.PG_PCONEX (+)= D.PG_PCONEX
				   AND D.CR_BAJA_REG IS NULL    /* Baja l�gica */
				   AND P.CR_BAJA_REG IS NULL    /* Baja l�gica */
				   AND C.CR_BAJA_REG IS NULL;   /* Baja l�gica */

		ELSE

			OPEN P_CUR_SALIDA FOR
				SELECT D.PG_PCONEX,
				       DECODE( D.CO_PAIS, '000', 'NACIONAL', '*********' ),
				       'M',
				       F_CO_TEXTO(P.DE_PROV),
				       D.CO_POST_EMPRE,
				       C.TX_TIPO_VIA_OFIC,
				       C.TX_CALLE_OFIC,
				       F_CO_TEXTO(D.DI_NU_EMPRE),
				       F_CO_TEXTO(D.DI_RES_EMPRE),
				       C.TX_TIPO_VIA_OFIC || ' ' || C.TX_CALLE_OFIC,
				       C.TX_TIPO_VIA_OFIC || ' ' || C.TX_CALLE_OFIC || ', '
					      || F_CO_TEXTO(D.DI_NU_EMPRE),
				       D.CO_PROV || D.CO_MUNI || D.CO_ENT_COLEC ||
					      D.CO_ENT_SINGU || '00',
				       DECODE( T.NU_COORD_GISX, 0, NULL, T.NU_COORD_GISX ),
				       DECODE( T.NU_COORD_GISY, 0, NULL, T.NU_COORD_GISY ),
				       DECODE( T.NU_COORD_GISX + T.NU_COORD_GISY,
					       NULL, 'N', 0, 'N', 'S' )
				  FROM TCL_DIREMPR D,
				       TAG_PROV P,
				       TAG_CTPI C,
				       TMP_DIREMPR_GIS T
				 WHERE D.CO_EMPRESA = P_CO_EMPRESA
			    AND EXISTS (SELECT NULL 
			                  FROM TCL_PCONEX PC
                    WHERE F_CO_TEXTO(DECODE(PC.NO_COMER,NULL,
                          DECODE(PC.NO_PUBLI,NULL,
                                 PC.NO_NOM_PCONEX  || ' ' ||
                                 PC.NO_APE1_PCONEX || ' ' ||
                                 PC.NO_APE2_PCONEX,
                                 PC.NO_PUBLI),
                                 PC.NO_COMER))    = P_TX_NOMBRE
			                   AND D.PG_PCONEX  = PC.PG_PCONEX
			                   AND D.CO_EMPRESA = PC.CO_EMPRESA )
				   AND D.PG_PCONEX IN (SELECT /*+ INDEX(AE XCL_ACTEMPR_PK) */
			                                 AP.PG_PCONEX
			                            FROM TCL_ACTPCON AP,
			                                 TCL_ACTEMPR AE
			                           WHERE AE.CO_EMPRESA = D.CO_EMPRESA
			                             AND AE.CO_ACTVAD  = P_CO_ACTVAD
			                             AND AP.CO_EMPRESA = AE.CO_EMPRESA
			                             AND AP.PG_ACTVAD_EMPRE = AE.PG_ACTVAD_EMPRE
			                             AND AP.CR_BAJA_REG IS NULL
			                             AND AE.CR_BAJA_REG IS NULL)
			      AND EXISTS (SELECT NULL
			                    FROM TCL_PCOCPU PU,
			                         TAP_CONDPUB TP
			                   WHERE PU.CO_COND_PUBLI = TP.CO_COND_PUBLI
			                     AND TP.CO_PROD       = 'PAO'
			                     AND TP.CO_AMBI_APA   = K_AMBI_APA_PA
			                     AND PU.CO_EMPRESA    = D.CO_EMPRESA
			                     AND PU.PG_PCONEX     = D.PG_PCONEX
			                     AND PU.CR_BAJA_REG IS NULL
			                     AND TP.CR_BAJA_REG IS NULL)
			      AND EXISTS ( SELECT NULL
			                     FROM TCL_TELEMPR T
			                    WHERE T.CO_EMPRESA  = D.CO_EMPRESA
			                      AND T.PG_PCONEX = D.PG_PCONEX
			                      AND T.OD_PREFE_PUBLI != 9999
			                      AND T.CR_BAJA_REG IS NULL ) 
			      AND P.CO_PAIS      = D.CO_PAIS
			      AND P.CO_PROV      = D.CO_PROV
			      AND C.CO_PAIS      = D.CO_PAIS
			      AND C.CO_PROV      = D.CO_PROV
			      AND C.CO_MUNI      = D.CO_MUNI
			      AND C.CO_ENT_COLEC = D.CO_ENT_COLEC
			      AND C.CO_ENT_SINGU = D.CO_ENT_SINGU
			      AND C.CO_NUCLEO    = D.CO_NUCLEO
			      AND C.CO_CALLE     = D.CO_VIA_INE
			      AND T.CO_EMPRESA(+)= D.CO_EMPRESA
			      AND T.PG_PCONEX (+)= D.PG_PCONEX
			      AND D.CR_BAJA_REG IS NULL    /* Baja l�gica */
			      AND P.CR_BAJA_REG IS NULL    /* Baja l�gica */
			      AND C.CR_BAJA_REG IS NULL;   /* Baja l�gica */
 
		END IF;
	END;


	--
	-- Procedimientos de carga de tabla de uso temporal (TBR_ASOSREST) 
	-- para Restaurantes.
	--

    -- Procedimiento que carga la tabla TBR_ASOSREST con valores actualizados.
 
    PROCEDURE P_RS_AGRADI ( P_MIN_EMPRESA  IN    TCL_PCONEX.CO_EMPRESA%TYPE := NULL,
                            P_MAX_EMPRESA  IN    TCL_PCONEX.CO_EMPRESA%TYPE := NULL )
    AS
        -- Declaracion de variables.

        L_CO_EMPRESA       TCL_PCONEX.CO_EMPRESA%TYPE;
        L_PG_PCONEX        TCL_PCONEX.PG_PCONEX%TYPE;
        L_DV_INF_ADI_AGR   TBR_ASOSREST.TX_CT_SERV%TYPE;
        L_SR_DOMICILIO     TBR_ASOSREST.IN_DOMICILIO%TYPE;
        L_PR_MENU_DIA  	   TBR_ASOSREST.IM_MEDIO_MENUDIA%TYPE;
        L_PR_MEDIO         TBR_ASOSREST.IM_MEDIO_MENU%TYPE;
        L_NO_BRS	       TBR_ASOSREST.TX_BRS%TYPE;

        -- Cursor c1, recupera el co_empresa y el pg_pconex que pertenencen
        -- a restaurantes y son publicables.

		 CURSOR C1 IS
			 SELECT PC.CO_EMPRESA,
			        PC.PG_PCONEX,
			        DECODE( PC.NO_COMER,NULL,
			        PC.NO_NOM_PCONEX  || ' ' ||
			        PC.NO_APE1_PCONEX || ' ' ||
			        PC.NO_APE2_PCONEX,
			        PC.NO_COMER ) NO_BRS
			   FROM TCL_PCONEX PC
			  WHERE EXISTS (SELECT /*+ INDEX(PU XCL_PCOCPU_PK)*/ NULL
			                  FROM TCL_PCOCPU PU,
			                       TAP_CONDPUB TP
			                 WHERE PU.CO_COND_PUBLI = TP.CO_COND_PUBLI
			                   AND TP.CO_PROD       = 'PAO'
			                   AND TP.CO_AMBI_APA   = 'PR'
			                   AND PU.CO_EMPRESA    = PC.CO_EMPRESA
			                   AND PU.PG_PCONEX     = PC.PG_PCONEX
			                   AND PU.CR_BAJA_REG IS NULL
			                   AND TP.CR_BAJA_REG IS NULL)
			    AND EXISTS ( SELECT NULL
			                   FROM TCL_TELEMPR T
			                  WHERE T.CO_EMPRESA  = PC.CO_EMPRESA
			                    AND T.PG_PCONEX   = PC.PG_PCONEX
			                    AND T.OD_PREFE_PUBLI != 9999
			                    AND T.CR_BAJA_REG IS NULL ) 
			    AND PC.CR_BAJA_REG IS NULL /* Baja logica */
			    AND PC.CO_EMPRESA BETWEEN NVL( P_MIN_EMPRESA, 0 )
			    AND NVL( P_MAX_EMPRESA+1, 999999999 );

        -- Cursor c2, para cada uno de los co_empresa, pg_pconex
        -- recuperados por el cursor c1 recupera los codigos de la
        -- informacion adicional formados por co_inf_adi, y cv_inf_adi.

		 CURSOR C2 IS
			 SELECT CO_INF_ADI,
			        CV_INF_ADI
			   FROM TCL_PCOVINFA
			  WHERE CO_EMPRESA = L_CO_EMPRESA
			    AND PG_PCONEX  = L_PG_PCONEX
			    AND CO_INF_ADI IN ('07','09','11')
			    AND (CO_INF_ADI,CV_INF_ADI) NOT IN (SELECT '07','001' FROM DUAL)
			    AND CR_BAJA_REG IS NULL
			  ORDER BY CO_INF_ADI, CV_INF_ADI;

        -- Cursor c3, para cada uno de los co_empresa, pg_pconex
        -- recuperados por el curcor c1, recupera el precio medio
        -- y el precio del menu del dia asociados

		 CURSOR C3 IS
            	SELECT SUBSTR(DV_INF_ADI_PCONEX,1,INSTR(DV_INF_ADI_PCONEX,';',1,1)-3) ||'.'||
            	       SUBSTR(DV_INF_ADI_PCONEX,INSTR(DV_INF_ADI_PCONEX,';',1,1)-2,2),
            	       SUBSTR(DV_INF_ADI_PCONEX,INSTR(DV_INF_ADI_PCONEX,';',1,1)+1,
            	              INSTR(DV_INF_ADI_PCONEX,';',INSTR(DV_INF_ADI_PCONEX,';',1,1),1)-3) ||'.'||
            	       SUBSTR(DV_INF_ADI_PCONEX,
            	              INSTR(DV_INF_ADI_PCONEX,';',1,1)+INSTR(DV_INF_ADI_PCONEX,';',INSTR(DV_INF_ADI_PCONEX,';',1,1),1)-2,2)
			   FROM TCL_PCOVINFA
			  WHERE CO_EMPRESA = L_CO_EMPRESA
			    AND PG_PCONEX  = L_PG_PCONEX
			    AND CO_INF_ADI = '05'
			    AND CV_INF_ADI = '001'
			    AND DV_INF_ADI_PCONEX LIKE '%;%;%;%' /*Elimina datos mal formateados*/
			    AND CR_BAJA_REG IS NULL;

        -- Cursor c4, para cada uno de los co_empresa, pg_pconex
        -- recuperados por el curcor c1, recuperamos si existe �
        -- no existe el servicio a domicilio. En caso de existir,
        -- se guarda el texto "SERVICIO A DOMICILIO".
    
		 CURSOR C4 IS
			 SELECT DECODE(DV_INF_ADI_PCONEX,'S','SERVICIO A DOMICILIO',NULL)
			   FROM TCL_PCOVINFA
			  WHERE CO_EMPRESA = L_CO_EMPRESA
			    AND PG_PCONEX  = L_PG_PCONEX
			    AND CO_INF_ADI = '06'
			    AND CV_INF_ADI = '001'
			    AND CR_BAJA_REG IS NULL;
	 BEGIN
			 DELETE FROM TBR_ASOSREST
			  WHERE CO_EMPRESA BETWEEN NVL( P_MIN_EMPRESA, 0 )
			    AND NVL( P_MAX_EMPRESA+1, 999999999 );
	 COMMIT;

       FOR R1 IN C1 LOOP

          L_CO_EMPRESA := R1.CO_EMPRESA;
          L_PG_PCONEX  := R1.PG_PCONEX;
          L_NO_BRS     := R1.NO_BRS;
          L_DV_INF_ADI_AGR := NULL;

          FOR R2 IN C2 LOOP
             L_DV_INF_ADI_AGR := L_DV_INF_ADI_AGR || R2.CO_INF_ADI || R2.CV_INF_ADI || ';';
          END LOOP;

          OPEN C3;
			 L_PR_MEDIO := NULL;
			 L_PR_MENU_DIA := NULL;
          FETCH C3 INTO L_PR_MEDIO, L_PR_MENU_DIA;
          CLOSE C3;

          OPEN C4;
          L_SR_DOMICILIO := NULL;
          FETCH C4 INTO L_SR_DOMICILIO;
          CLOSE C4;

          INSERT INTO TBR_ASOSREST VALUES (L_CO_EMPRESA,
                                           L_PG_PCONEX,
                                           NVL(L_PR_MEDIO,'0'),
                                           NVL(L_PR_MENU_DIA,'0'),
                                           L_NO_BRS,
                                           NVL(L_SR_DOMICILIO,'N'),
                                           NVL(L_DV_INF_ADI_AGR,'N'));

          COMMIT;

       END LOOP;

    END;

	--
	-- Procedimientos de extracci�n para Restaurantes.
	--

	PROCEDURE P_RS_EMP ( P_CUR_SALIDA   OUT   T_CUR_RS_EMP,
	                     P_MIN_EMPRESA  IN    TCL_ACTEMPR.CO_EMPRESA%TYPE := NULL,
	                     P_MAX_EMPRESA  IN    TCL_ACTEMPR.CO_EMPRESA%TYPE := NULL )
	AS
	BEGIN
		OPEN P_CUR_SALIDA FOR
			SELECT DISTINCT
			       AC.CO_EMPRESA,
			       AC.CO_ACTVAD,
			       AA.TX_ACTVAD,
			       F_CO_TEXTO(AG.TX_BRS),
			       AG.IM_MEDIO_MENU,
			       AG.IM_MEDIO_MENUDIA,
			       AG.IN_DOMICILIO,
			       AG.TX_CT_SERV
			  FROM TBR_ASOSREST AG,
			       TCL_ACTEMPR AC,
			       TAA_ACTVAD  AA
			 WHERE AG.CO_EMPRESA = AC.CO_EMPRESA
			   AND AC.CO_ACTVAD  = AA.CO_ACTVAD
			   AND EXISTS (SELECT NULL
			                 FROM TCL_DIREMPR DR
			                WHERE DR.CO_EMPRESA = AG.CO_EMPRESA
			                  AND DR.PG_PCONEX  = AG.PG_PCONEX
                           AND DR.CR_BAJA_REG IS NULL)
			   AND AC.PG_ACTVAD_EMPRE =
			           (SELECT /*+ INDEX(AP XCL_ACTPCON_PK)*/ AP.PG_ACTVAD_EMPRE
			              FROM TCL_ACTPCON AP
			             WHERE AP.CO_EMPRESA   = AG.CO_EMPRESA
			               AND AP.PG_PCONEX    = AG.PG_PCONEX
			               AND AP.OD_PRIORIDAD =
			                       (SELECT MIN(AP1.OD_PRIORIDAD)
			                          FROM TCL_ACTPCON AP1
			                         WHERE AP.CO_EMPRESA = AP1.CO_EMPRESA
			                           AND AP.PG_PCONEX  = AP1.PG_PCONEX
			                           AND AP1.CR_BAJA_REG IS NULL)
			               AND AP.CR_BAJA_REG IS NULL)
			   AND AC.CR_BAJA_REG IS NULL  /* Baja l�gica */
			   AND AA.CR_BAJA_REG IS NULL  /* Baja l�gica */
			   AND AG.CO_EMPRESA BETWEEN NVL( P_MIN_EMPRESA, 0 )
			   AND NVL( P_MAX_EMPRESA+1, 999999999 );
	END;


	PROCEDURE P_RS_DIR ( P_CUR_SALIDA   OUT   T_CUR_RS_DIR,
	                     P_CO_EMPRESA   IN    TCL_ACTEMPR.CO_EMPRESA%TYPE,
	                     P_CO_ACTVAD    IN    TCL_ACTEMPR.CO_ACTVAD%TYPE,
	                     P_TX_NOMBRE  IN    VARCHAR2,
	                     P_PR_MEDIO     IN    VARCHAR2,
	                     P_PR_MENU_DIA  IN    VARCHAR2,
	                     P_SR_DOMICILIO IN    VARCHAR2,
	                     P_DV_VINFA_AGR IN    VARCHAR2 )
	AS
	BEGIN
		OPEN P_CUR_SALIDA FOR
			SELECT D.PG_PCONEX,
			       DECODE( D.CO_PAIS, '000', 'NACIONAL', '*********' ),
			       'M',
			       F_CO_TEXTO(P.DE_PROV),
			       D.CO_POST_EMPRE,
			       C.TX_TIPO_VIA_OFIC,
			       C.TX_CALLE_OFIC,
			       F_CO_TEXTO(D.DI_NU_EMPRE),
			       F_CO_TEXTO(D.DI_RES_EMPRE),
			       C.TX_TIPO_VIA_OFIC || ' ' || C.TX_CALLE_OFIC,
			       C.TX_TIPO_VIA_OFIC || ' ' || C.TX_CALLE_OFIC
			           || ', ' || F_CO_TEXTO(D.DI_NU_EMPRE),
			       D.CO_PROV || D.CO_MUNI || D.CO_ENT_COLEC
			           || D.CO_ENT_SINGU || '00',
			       DECODE( T.NU_COORD_GISX, 0, NULL, T.NU_COORD_GISX ),
			       DECODE( T.NU_COORD_GISY, 0, NULL, T.NU_COORD_GISY ),
			       DECODE( T.NU_COORD_GISX + T.NU_COORD_GISY,
			               NULL, 'N', 0, 'N', 'S' )
			  FROM TCL_DIREMPR D,
			       TBR_ASOSREST AG,
			       TAG_PROV P,
			       TAG_CTPI C,
			       TMP_DIREMPR_GIS T
			 WHERE D.CO_EMPRESA = P_CO_EMPRESA
			   AND D.PG_PCONEX IN (SELECT /*+ INDEX(AE XCL_ACTEMPR_PK) */
			                              AP.PG_PCONEX
			                         FROM TCL_ACTPCON AP,
			                              TCL_ACTEMPR AE
			                        WHERE AE.CO_EMPRESA = D.CO_EMPRESA
			                          AND AE.CO_ACTVAD = P_CO_ACTVAD
			                          AND AP.CO_EMPRESA = AE.CO_EMPRESA
			                          AND AP.PG_ACTVAD_EMPRE = AE.PG_ACTVAD_EMPRE
                                AND AP.CR_BAJA_REG IS NULL
                                AND AE.CR_BAJA_REG IS NULL)
			   AND AG.CO_EMPRESA     = D.CO_EMPRESA
			   AND AG.PG_PCONEX      = D.PG_PCONEX
			   AND F_CO_TEXTO(AG.TX_BRS) = P_TX_NOMBRE
			   AND AG.IM_MEDIO_MENU       = P_PR_MEDIO
			   AND AG.IM_MEDIO_MENUDIA = P_PR_MENU_DIA
			   AND AG.IN_DOMICILIO = P_SR_DOMICILIO
			   AND AG.TX_CT_SERV   = P_DV_VINFA_AGR
			   AND P.CO_PAIS       = D.CO_PAIS
			   AND P.CO_PROV       = D.CO_PROV
			   AND C.CO_PAIS       = D.CO_PAIS
			   AND C.CO_PROV       = D.CO_PROV
			   AND C.CO_MUNI       = D.CO_MUNI
			   AND C.CO_ENT_COLEC  = D.CO_ENT_COLEC
			   AND C.CO_ENT_SINGU  = D.CO_ENT_SINGU
			   AND C.CO_NUCLEO     = D.CO_NUCLEO
			   AND C.CO_CALLE      = D.CO_VIA_INE
			   AND T.CO_EMPRESA (+)= D.CO_EMPRESA
			   AND T.PG_PCONEX  (+)= D.PG_PCONEX
			   AND D.CR_BAJA_REG   IS NULL  /* Baja l�gica */
			   AND P.CR_BAJA_REG   IS NULL  /* Baja l�gica */
			   AND C.CR_BAJA_REG   IS NULL; /* Baja l�gica */
	END;


	PROCEDURE P_RS_ADI ( P_CUR_SALIDA   OUT   T_CUR_RS_ADI,
	                     P_CO_EMPRESA   IN    TCL_ACTEMPR.CO_EMPRESA%TYPE,
	                     P_CO_ACTVAD    IN    TCL_ACTEMPR.CO_ACTVAD%TYPE,
	                     P_TX_NOMBRE  IN    VARCHAR2,
	                     P_PR_MEDIO     IN    VARCHAR2,
	                     P_PR_MENU_DIA  IN    VARCHAR2,
	                     P_SR_DOMICILIO IN    VARCHAR2,
	                     P_DV_VINFA_AGR IN    VARCHAR2,
	                     P_CO_INF_ADI   IN    TCL_PCOVINFA.CO_INF_ADI%TYPE )
	AS
	BEGIN
		OPEN P_CUR_SALIDA FOR
			SELECT DISTINCT
			       V.CT_INF_EXT,
			       V.ID_INF_EXT,
			       NVL( C.DV_INF_ADI_PCONEX, V.DV_INF_ADI )
			  FROM TCL_PCOVINFA C,
			       TAP_VALINFA  V,
			       TBR_ASOSREST AG
			 WHERE C.CO_EMPRESA = P_CO_EMPRESA
			   AND C.PG_PCONEX IN (SELECT /*+ INDEX(AE XCL_ACTEMPR_PK) */
			                              AP.PG_PCONEX
			                         FROM TCL_ACTPCON AP,
			                              TCL_ACTEMPR AE
			                        WHERE AE.CO_EMPRESA = C.CO_EMPRESA
			                          AND AE.CO_ACTVAD  = P_CO_ACTVAD
			                          AND AP.CO_EMPRESA = AE.CO_EMPRESA
			                          AND AP.PG_ACTVAD_EMPRE = AE.PG_ACTVAD_EMPRE
                                AND AP.CR_BAJA_REG IS NULL
                                AND AE.CR_BAJA_REG IS NULL)
			   AND C.CO_INF_ADI = P_CO_INF_ADI
			   AND C.CV_INF_ADI <> '000'
			   AND (C.CO_INF_ADI,C.CV_INF_ADI) NOT IN (SELECT '07','001' FROM DUAL)
			   AND V.CO_INF_ADI = C.CO_INF_ADI
			   AND V.CV_INF_ADI = C.CV_INF_ADI
			   AND AG.CO_EMPRESA         = C.CO_EMPRESA
			   AND AG.PG_PCONEX          = C.PG_PCONEX
			   AND AG.IM_MEDIO_MENU      = P_PR_MEDIO
			   AND AG.IM_MEDIO_MENUDIA   = P_PR_MENU_DIA
			   AND F_CO_TEXTO(AG.TX_BRS) = P_TX_NOMBRE
			   AND AG.IN_DOMICILIO       = P_SR_DOMICILIO
			   AND AG.TX_CT_SERV         = P_DV_VINFA_AGR
			   AND C.CR_BAJA_REG    IS NULL  /* Baja l�gica */
			   AND V.CR_BAJA_REG    IS NULL; /* Baja l�gica */
	END;
	
        /* Fase II - Procedimiento de extraccion de valoraciones de Restaurantes */  	
        PROCEDURE P_RS_ADI_VAL ( P_CUR_SALIDA     OUT   T_CUR_RS_ADI,
	                         P_CO_EMPRESA     IN    TCL_ACTEMPR.CO_EMPRESA%TYPE,
	                         P_CO_ACTVAD      IN    TCL_ACTEMPR.CO_ACTVAD%TYPE,
	                         P_TX_NOMBRE      IN    VARCHAR2,
	                         P_PR_MEDIO       IN    VARCHAR2,
	                         P_PR_MENU_DIA    IN    VARCHAR2,
	                         P_SR_DOMICILIO   IN    VARCHAR2,
	                         P_DV_VINFA_AGR   IN    VARCHAR2,
	                         P_CO_INF_ADI     IN    TCL_PCOVINFA.CO_INF_ADI%TYPE,
                                 P_CV_INF_ADI_INF IN    NUMBER,
                                 P_CV_INF_ADI_SUP IN    NUMBER) 
	AS
	BEGIN
		OPEN P_CUR_SALIDA FOR
			SELECT DISTINCT
			       V.CT_INF_EXT,
			       V.ID_INF_EXT,
			       NVL( C.DV_INF_ADI_PCONEX, V.DV_INF_ADI )
			  FROM TCL_PCOVINFA C,
			       TAP_VALINFA  V,
			       TBR_ASOSREST AG
			 WHERE C.CO_EMPRESA = P_CO_EMPRESA
			   AND C.PG_PCONEX IN (SELECT /*+ INDEX(AE XCL_ACTEMPR_PK) */ 
			                              AP.PG_PCONEX
			                         FROM TCL_ACTPCON AP,
			                              TCL_ACTEMPR AE
			                        WHERE AE.CO_EMPRESA = C.CO_EMPRESA
			                          AND AE.CO_ACTVAD  = P_CO_ACTVAD
			                          AND AP.CO_EMPRESA = AE.CO_EMPRESA
			                          AND AP.PG_ACTVAD_EMPRE = AE.PG_ACTVAD_EMPRE
                                AND AP.CR_BAJA_REG IS NULL
                                AND AE.CR_BAJA_REG IS NULL)
			   AND C.CO_INF_ADI = P_CO_INF_ADI
			   AND C.CV_INF_ADI <> '000'
			   AND (C.CO_INF_ADI,C.CV_INF_ADI) NOT IN (SELECT '07','001' FROM DUAL)
			   AND V.CO_INF_ADI = C.CO_INF_ADI
			   AND V.CV_INF_ADI = C.CV_INF_ADI
                           AND TO_NUMBER(V.CV_INF_ADI) BETWEEN P_CV_INF_ADI_INF AND P_CV_INF_ADI_SUP
			   AND AG.CO_EMPRESA         = C.CO_EMPRESA
			   AND AG.PG_PCONEX          = C.PG_PCONEX
			   AND AG.IM_MEDIO_MENU      = P_PR_MEDIO
			   AND AG.IM_MEDIO_MENUDIA   = P_PR_MENU_DIA
			   AND F_CO_TEXTO(AG.TX_BRS) = P_TX_NOMBRE
			   AND AG.IN_DOMICILIO       = P_SR_DOMICILIO
			   AND AG.TX_CT_SERV         = P_DV_VINFA_AGR
			   AND C.CR_BAJA_REG    IS NULL  /* Baja logica */
			   AND V.CR_BAJA_REG    IS NULL; /* Baja logica */
	END;

	--
	-- Procedimientos de extraccion para Alojamientos.
	--

	PROCEDURE P_AL_EMP ( P_CUR_SALIDA   OUT   T_CUR_AL_EMP,
	                     P_MIN_EMPRESA  IN    TCL_ACTEMPR.CO_EMPRESA%TYPE := NULL,
	                     P_MAX_EMPRESA  IN    TCL_ACTEMPR.CO_EMPRESA%TYPE := NULL )
	AS
	BEGIN
		OPEN P_CUR_SALIDA FOR
			SELECT DISTINCT
			       PC.CO_EMPRESA,
			       AC.CO_ACTVAD,
			       PC.PG_PCONEX,
			       F_CO_TEXTO(DECODE( PC.NO_COMER,
			                          NULL, PC.NO_NOM_PCONEX || ' ' ||
			                                PC.NO_APE1_PCONEX || ' ' ||
			                                PC.NO_APE2_PCONEX,
			                          PC.NO_COMER ))
			  FROM TCL_PCONEX PC,
			       TCL_ACTEMPR AC,
			       TAA_ACTVAD  AA
			 WHERE PC.CO_EMPRESA = AC.CO_EMPRESA
			   AND AC.CO_ACTVAD  =  AA.CO_ACTVAD
			   AND EXISTS (SELECT NULL
			                 FROM TCL_DIREMPR DR
			                WHERE DR.CO_EMPRESA = PC.CO_EMPRESA
			                  AND DR.PG_PCONEX  = PC.PG_PCONEX
			                  AND DR.CR_BAJA_REG IS NULL)
			   AND AC.PG_ACTVAD_EMPRE = (SELECT /*+ INDEX(AP XCL_ACTPCON_PK)*/  
			                                    AP.PG_ACTVAD_EMPRE
			                               FROM TCL_ACTPCON AP
			                              WHERE AP.CO_EMPRESA  = PC.CO_EMPRESA
			                                AND AP.PG_PCONEX    = PC.PG_PCONEX
			                                AND AP.OD_PRIORIDAD =
			                                    (SELECT MIN(AP1.OD_PRIORIDAD)
			                                       FROM TCL_ACTPCON AP1
			                                      WHERE AP.CO_EMPRESA = AP1.CO_EMPRESA
			                                        AND AP.PG_PCONEX  = AP1.PG_PCONEX
			                                        AND AP1.CR_BAJA_REG IS NULL)
			                                AND AP.CR_BAJA_REG  IS NULL)
			   AND EXISTS (SELECT NULL
			                 FROM TCL_PCOCPU PU,
			                      TAP_CONDPUB TP
			                WHERE PU.CO_COND_PUBLI = TP.CO_COND_PUBLI
			                  AND TP.CO_PROD       = 'PAO'
			                  AND TP.CO_AMBI_APA   = K_AMBI_APA_AL
			                  AND PU.CO_EMPRESA    = PC.CO_EMPRESA
			                  AND PU.PG_PCONEX     = PC.PG_PCONEX
			                  AND PU.CR_BAJA_REG IS NULL
			                  AND TP.CR_BAJA_REG IS NULL)
			   AND EXISTS ( SELECT NULL
			                  FROM TCL_TELEMPR T
			                 WHERE T.CO_EMPRESA  = PC.CO_EMPRESA
			                   AND T.PG_PCONEX = PC.PG_PCONEX
			                   AND T.OD_PREFE_PUBLI != 9999
			                   AND T.CR_BAJA_REG IS NULL ) 
			   AND PC.CR_BAJA_REG IS NULL /* Baja l�gica */
			   AND AC.CR_BAJA_REG IS NULL /* Baja l�gica */
			   AND AA.CR_BAJA_REG IS NULL /* Baja l�gica */
			   AND PC.CO_EMPRESA BETWEEN NVL( P_MIN_EMPRESA, 0 )
			   AND NVL( P_MAX_EMPRESA+1, 999999999 );
	END;


	PROCEDURE P_AL_DIR ( P_CUR_SALIDA   OUT   T_CUR_AL_DIR,
	                     P_CO_EMPRESA   IN    TCL_DIREMPR.CO_EMPRESA%TYPE,
	                     P_PG_PCONEX    IN    TCL_DIREMPR.PG_PCONEX%TYPE )
	AS
	BEGIN
		OPEN P_CUR_SALIDA FOR
			SELECT D.PG_PCONEX,
			       DECODE( D.CO_PAIS, '000', 'NACIONAL', '*********' ),
			       'M',
			       F_CO_TEXTO(P.DE_PROV),
			       D.CO_POST_EMPRE,
			       C.TX_TIPO_VIA_OFIC,
			       C.TX_CALLE_OFIC,
			       F_CO_TEXTO(D.DI_NU_EMPRE),
			       F_CO_TEXTO(D.DI_RES_EMPRE),
			       C.TX_TIPO_VIA_OFIC || ' ' || C.TX_CALLE_OFIC,
			       C.TX_TIPO_VIA_OFIC || ' ' || C.TX_CALLE_OFIC || ', '
			           || F_CO_TEXTO(D.DI_NU_EMPRE),
			       D.CO_PROV || D.CO_MUNI || D.CO_ENT_COLEC ||
			           D.CO_ENT_SINGU || '00',
			       DECODE( T.NU_COORD_GISX, 0, NULL, T.NU_COORD_GISX ),
			       DECODE( T.NU_COORD_GISY, 0, NULL, T.NU_COORD_GISY ),
			       DECODE( T.NU_COORD_GISX + T.NU_COORD_GISY,
			               NULL, 'N', 0, 'N', 'S' )
			  FROM TCL_DIREMPR D,
			       TAG_PROV P,
			       TAG_CTPI C,
			       TMP_DIREMPR_GIS T
			 WHERE D.CO_EMPRESA   =  P_CO_EMPRESA
			   AND D.PG_PCONEX    =  P_PG_PCONEX
			   AND P.CO_PAIS      =  D.CO_PAIS
			   AND P.CO_PROV      =  D.CO_PROV
			   AND C.CO_PAIS      =  D.CO_PAIS
			   AND C.CO_PROV      =  D.CO_PROV
			   AND C.CO_MUNI      =  D.CO_MUNI
			   AND C.CO_ENT_COLEC =  D.CO_ENT_COLEC
			   AND C.CO_ENT_SINGU =  D.CO_ENT_SINGU
			   AND C.CO_NUCLEO    =  D.CO_NUCLEO
  			 AND C.CO_CALLE     =  D.CO_VIA_INE
			   AND T.CO_EMPRESA(+)=  D.CO_EMPRESA
			   AND T.PG_PCONEX (+)=  D.PG_PCONEX
			   AND D.CR_BAJA_REG  IS NULL    /* Baja l�gica */
			   AND P.CR_BAJA_REG  IS NULL    /* Baja l�gica */
			   AND C.CR_BAJA_REG  IS NULL;   /* Baja l�gica */
	END;


	PROCEDURE P_AL_ADI ( P_CUR_SALIDA   OUT    T_CUR_AL_ADI,
	                     P_CO_EMPRESA   IN     TCL_PCOVINFA.CO_EMPRESA%TYPE,
	                     P_PG_PCONEX    IN     TCL_PCOVINFA.PG_PCONEX%TYPE,
	                     P_CO_INF_ADI   IN     TCL_PCOVINFA.CO_INF_ADI%TYPE )
	AS
	BEGIN
		OPEN P_CUR_SALIDA FOR
			SELECT DISTINCT
			       V.CT_INF_EXT,
			       V.ID_INF_EXT,
			       NVL( C.DV_INF_ADI_PCONEX, V.DV_INF_ADI )
			  FROM TCL_PCOVINFA C,
			       TAP_VALINFA V
			 WHERE C.CO_EMPRESA  = P_CO_EMPRESA
			   AND C.PG_PCONEX   = P_PG_PCONEX
			   AND C.CO_INF_ADI  = P_CO_INF_ADI
			   AND C.CV_INF_ADI <> '000'
			   AND (C.CO_INF_ADI,C.CV_INF_ADI) NOT IN (SELECT '07','001' FROM DUAL)
			   AND V.CO_INF_ADI  = C.CO_INF_ADI
			   AND V.CV_INF_ADI  = C.CV_INF_ADI
			   AND C.CR_BAJA_REG IS NULL
			   AND V.CR_BAJA_REG IS NULL;
	END;


	--
	-- Procedimientos de extraccion para P�ginas Blancas.
	--

	PROCEDURE P_PB_ABO ( P_CUR_SALIDA   OUT   T_CUR_PB_ABO,
	                     P_ID_PROV      IN    VARCHAR2 := NULL,
	                     P_MIN_ABONO    IN    TOP_IGBOL.ID_ABONO%TYPE := NULL,
	                     P_MAX_ABONO    IN    TOP_IGBOL.ID_ABONO%TYPE := NULL )
	AS
	BEGIN
		IF P_ID_PROV IS NULL THEN
			OPEN P_CUR_SALIDA FOR
				SELECT /*+ INDEX(TS I_TMP_TELF_SEC)*/ B.ID_ABONO,
				       NVL(LTRIM(RTRIM(UPPER(B.TX_INS_GUIA))),UPPER(B.NO_NOMBRE)),
				       DECODE(LTRIM(RTRIM(B.TX_INS_GUIA)),NULL,UPPER(B.NO_APE1),NULL),
				       DECODE(LTRIM(RTRIM(B.TX_INS_GUIA)),NULL,UPPER(B.NO_APE2),NULL),
				       DECODE( B.CT_ABONO, 'OO', 2, 'PR', 1, 'PT', 0 ),
				       NVL(LTRIM(RTRIM(C.TX_CALLE_APA)),
				           LTRIM(RTRIM(DECODE(LTRIM(RTRIM(TX_TIPO_VIA_OFIC)),
			                 'CALLE',NULL,LTRIM(RTRIM(C.TX_TIPO_VIA_OFIC)))
				       ||' '||C.TX_CALLE_OFIC))),
				       B.DI_NU_TELFO,
				       B.DI_RES_TELFO,
				       SUBSTR(B.CO_POST_TELFO,1,5),
				       F_CO_TEXTO(P.DE_PROV),
				       B.CO_PROV_TPI || B.CO_MUNI_TPI ||
				           B.CO_ECOL_TPI || B.CO_ESIN_TPI || '00',
				       T1.NC_TELF,
				       DECODE( T1.IN_TELEMAT, NULL, 
			                 DECODE( T1.IN_TELF_TEXT, NULL, 
			                         DECODE( T1.IN_TELF_FAX, NULL, NULL, 'F' ),
				               'S' ), 'N' ) NC_CTL,
				       TS.NC_TELF NC_TELF2
				  FROM TOP_IGBOL B,
				       TOP_TELFBOL T1,
			         TMP_TELF_SEC TS,
				       TAG_PROV P,
				       TAG_CTPI C
				 WHERE B.ID_ABONO BETWEEN
				           LPAD( NVL( P_MIN_ABONO, 0 ), 16, '0' ) AND
				           LPAD( NVL( P_MAX_ABONO+1, 9999999999999999 ), 16, '0' )
				   AND T1.ID_ABONO  = B.ID_ABONO
                   AND T1.ID_ABONO = TS.ID_ABONO(+)
				   AND T1.NC_TELF NOT IN (SELECT S.NC_TELF_PRAL
				                            FROM TBP_TELFSUSTI S
				                           WHERE S.CR_BAJA_REG IS NULL)
				   AND T1.OD_PREFE_TELF = (SELECT MIN(T2.OD_PREFE_TELF)
				                             FROM TOP_TELFBOL T2
				                            WHERE T2.ID_ABONO = T1.ID_ABONO
				                              AND T2.CR_NO_USO_PUBL IS NULL
				                              AND T2.CR_BAJA_REG    IS NULL)
				   AND P.CO_PAIS         =  B.CO_PAIS_TPI
				   AND P.CO_PROV         =  B.CO_PROV_TPI
				   AND C.CO_PAIS         =  B.CO_PAIS_TPI
				   AND C.CO_PROV         =  B.CO_PROV_TPI
				   AND C.CO_MUNI         =  B.CO_MUNI_TPI
				   AND C.CO_ENT_COLEC    =  B.CO_ECOL_TPI
				   AND C.CO_ENT_SINGU    =  B.CO_ESIN_TPI
				   AND C.CO_NUCLEO       =  B.CO_NUCLEO_TPI
				   AND C.CO_CALLE        =  B.CO_CALLE_TPI
				   AND B.CR_NO_USO_PUBL  IS NULL    /* No publicable */
				   AND B.CR_BAJA_REG     IS NULL    /* Baja l�gica */
				   AND T1.CR_NO_USO_PUBL IS NULL    /* No publicable */
				   AND T1.CR_BAJA_REG    IS NULL    /* Baja l�gica */
				   AND P.CR_BAJA_REG     IS NULL    /* Baja l�gica */
				   AND C.CR_BAJA_REG     IS NULL;   /* Baja l�gica */
		ELSE
			OPEN P_CUR_SALIDA FOR
				SELECT /*+ INDEX(TS I_TMP_TELF_SEC)*/ B.ID_ABONO,
				       DECODE(UPPER(B.CT_TITULAR),'J',NVL(UPPER(B.TX_INS_GUIA),
			                                             UPPER(B.NO_NOMBRE))
			                                        ,UPPER(B.NO_NOMBRE)),
				       UPPER(B.NO_APE1),
				       UPPER(B.NO_APE2),
				       DECODE( B.CT_ABONO, 'OO', 2, 'PR', 1, 'PT', 0 ),
				       NVL( C.TX_CALLE_APA, C.TX_CALLE_OFIC ),
				       B.DI_NU_TELFO,
				       B.DI_RES_TELFO,
				       SUBSTR(B.CO_POST_TELFO,1,5),
				       F_CO_TEXTO(P.DE_PROV),
				       B.CO_PROV_TPI || B.CO_MUNI_TPI ||
				           B.CO_ECOL_TPI || B.CO_ESIN_TPI || '00',
				       T1.NC_TELF,
				       DECODE( T1.IN_TELEMAT, NULL, 
			                 DECODE( T1.IN_TELF_TEXT, NULL, 
			                         DECODE( T1.IN_TELF_FAX, NULL, NULL, 'F' )
			                 , 'S' ), 'N' ) NC_CTL,
				       TS.NC_TELF NC_TELF2
				  FROM TOP_IGBOL B,
				       TOP_TELFBOL T1,
           TMP_TELF_SEC TS,
				       TAG_PROV P,
				       TAG_CTPI C
				 WHERE B.ID_ABONO BETWEEN
				           LPAD( NVL( P_MIN_ABONO, 0 ), 16, '0' ) AND
				           LPAD( NVL( P_MAX_ABONO+1, 9999999999999999 ), 16, '0' )
				   AND T1.ID_ABONO  = B.ID_ABONO
			     AND T1.ID_ABONO = TS.ID_ABONO(+)
				   AND T1.NC_TELF NOT IN (SELECT S.NC_TELF_PRAL
				                            FROM TBP_TELFSUSTI S
				                           WHERE S.CR_BAJA_REG IS NULL)
				   AND T1.OD_PREFE_TELF = (SELECT MIN(T2.OD_PREFE_TELF)
				                             FROM TOP_TELFBOL T2
				                            WHERE T2.ID_ABONO = T1.ID_ABONO
				                              AND T2.CR_NO_USO_PUBL IS NULL
				                              AND T2.CR_BAJA_REG    IS NULL)
				   AND P.CO_PAIS         =  B.CO_PAIS_TPI
				   AND P.CO_PROV         =  B.CO_PROV_TPI
				   AND C.CO_PAIS         =  B.CO_PAIS_TPI
				   AND C.CO_PROV         =  B.CO_PROV_TPI
				   AND C.CO_MUNI         =  B.CO_MUNI_TPI
				   AND C.CO_ENT_COLEC    =  B.CO_ECOL_TPI
				   AND C.CO_ENT_SINGU    =  B.CO_ESIN_TPI
				   AND C.CO_NUCLEO       =  B.CO_NUCLEO_TPI
				   AND C.CO_CALLE        =  B.CO_CALLE_TPI
				   AND B.CR_NO_USO_PUBL  IS NULL    /* No publicable */
				   AND B.CR_BAJA_REG     IS NULL    /* Baja l�gica */
				   AND T1.CR_NO_USO_PUBL IS NULL    /* No publicable */
				   AND T1.CR_BAJA_REG    IS NULL    /* Baja l�gica */
				   AND P.CR_BAJA_REG     IS NULL    /* Baja l�gica */
				   AND C.CR_BAJA_REG     IS NULL    /* Baja l�gica */
				   AND C.CO_PROV = P_ID_PROV;
		END IF;
	END;


	PROCEDURE P_PB_TFSEC
	AS
	BEGIN
		P_GR_TRUNC(K_TRUNC_ONWER_TABLAS,'TMP_TELF_SEC');

			 INSERT INTO TMP_TELF_SEC 
			        SELECT ID_ABONO,
			               NC_TELF
			          FROM TOP_TELFBOL TS
			         WHERE TS.NC_TELF NOT IN (SELECT S.NC_TELF_PRAL
			                                    FROM TBP_TELFSUSTI S
			                                   WHERE S.CR_BAJA_REG IS NULL)
			          AND TS.OD_PREFE_TELF = (SELECT MIN(T2.OD_PREFE_TELF)+1
			                                    FROM TOP_TELFBOL T2
			                                   WHERE T2.ID_ABONO = TS.ID_ABONO
			                                     AND T2.CR_NO_USO_PUBL IS NULL
			                                     AND T2.CR_BAJA_REG    IS NULL)
			          AND TS.CR_NO_USO_PUBL IS NULL
			          AND TS.CR_BAJA_REG    IS NULL;

		COMMIT;
	END;


    PROCEDURE P_GR_TRUNC (P_OWNER IN VARCHAR2,
                          P_TABLE_NAME IN VARCHAR2)
    AS 
      w_crsor integer;
      w_rval  integer;
   BEGIN 
        /* Declaracion de los nombres de tabla permitidos */
         if   (upper(p_table_name) = 'TMP_DIREMPR_GIS') /*  C�lc. coord. GIS */
           OR (upper(p_table_name) = 'TMP_TELF_SEC')  /* Blancas Tfn. secund. */
         then
 	         w_crsor := dbms_sql.open_cursor;
 	         dbms_sql.parse(w_crsor, 'truncate table '||p_owner||'.'||
                             p_table_name,dbms_sql.v7);
 	         w_rval := dbms_sql.execute(w_crsor);
 	         dbms_sql.close_cursor(w_crsor);
         end if;
   END;

END DATA_IFACE;
/

SHOW ERRORS

SET FEEDBACK ON
SET ECHO ON
