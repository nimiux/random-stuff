/******************************************************************************
 * ++
 * Author       :
 *
 * Module Name  : 
 *
 * Description  :
 *
 * Status Diagrams :
 *
 * Mod. History :
 *
 * AUTHOR               DATE            DESCRIPTION
 * ----------------     --------------  ------------------------------------
 * Enrique Riesgo
 *
 * --
 ******************************************************************************/
#ifndef XF_ERROR_H
#define XF_ERROR_H

/******************************************************************************
 * Standards and Complance                                                    *
 ******************************************************************************/

/******************************************************************************
 * System Include Files                                                       *
 ******************************************************************************/

/******************************************************************************
 * Application Include Files                                                  *
 ******************************************************************************/

/******************************************************************************
 * Debug and Trace Options                                                    *
 ******************************************************************************/

/******************************************************************************
 * Macro Definitions                                                          *
 ******************************************************************************/
/* 
 * General return codes used by all programs and libraries of Xfera SMSC Gateway
 */
#define XF_EXIT_SUCCESS				0
#define XF_EXIT_ERROR			       -1
#define XF_EXIT_NOT_FOUND                      -2
#define XF_EXIT_TRUNC                          -3

#define XF_ORA_SUCCESS      0
#define XF_ORA_ERROR        1
#define XF_ORA_NOT_FOUND 1403

/*
 * Status/state codes of the Short Messages.
 */
#define SG_STATE_LOADED        "FETCHED"
#define SG_STATE_BLACK_NUMBER  "BLACKNUM"
#define SG_STATE_FILTERED      "FILTERED"
#define SG_STATE_PARSED        "PARAMPR"
#define SG_STATE_COMPLETED     "COMPLETED"
#define SG_STATE_SUBMITED      "SUBMITED"

#define SG_STATE_PARAM_NF      "PARAMNFND"
#define SG_STATE_PARAM_TRUNC   "PARAMTRUNC"
#define SG_STATE_PARAM_ERROR   "PARAMERROR"

#define SG_STATE_PUBLI_NF      "PUBLINFND"
#define SG_STATE_PUBLI_TRUNC   "PUBLITRUNC"

#define SG_STATE_BAD_DATE      "BADDATE"

#define SG_STATE_BAD_B_NUMBER  "BADBNUM"

#define SG_STATE_DB_ERROR      "DBERROR"

#define SG_STATE_SMSC_ERROR    "SMSCERROR"
#define SG_STATE_SMSC_REJECTED "SMSCREJCTD"
#define SG_STATE_SMSC_FULL_QUE "SMSCFQUEUE"
#define SG_STATE_SMSC_UNKNOWN  "SMSCUNKNOW"
#define SG_STATE_SMSC_BAD_MSG  "SMSCBADMSG"

/* States after the message submission */
#define SG_STATE_EN_ROUTE      "ENROUTE"
#define SG_STATE_DELIVERED     "DELIVERED"
#define SG_STATE_EXPIRED       "EXPIRED"
#define SG_STATE_DELETED       "DELETED"
#define SG_STATE_UNDELIVERED   "UNDELIVERAB"
#define SG_STATE_ACCEPTED      "ACCEPTED"
#define SG_STATE_INVALID       "INVALID"

/*
 * Error codes used in modules and programs of Xfera SMSC Gateway
 */
/* SMSC Gateway specific messages */
#define SG_MSG                                   1000
#define SG_MSG_START_RUNNING                     SG_MSG + 1
#define SG_MSG_STOPPED_BY_USER                   SG_MSG + 2
#define SG_MSG_ARGUMENT_TRUNCATION               SG_MSG + 3
#define SG_MSG_STOPPED_BY_ERROR                  SG_MSG + 4
#define SG_MSG_EXIT_MSG                          SG_MSG + 5
#define SG_MSG_STARTING_HERMES                   SG_MSG + 6
#define SG_MSG_SHUTTING_DOWN_HERMES              SG_MSG + 7
#define SG_MSG_SIGNAL_CATCHED                    SG_MSG + 8

/* External errors */
#define SG_ERR                                   2000
#define SG_ERR_ARGUMENT_NUMBER_ERROR             SG_ERR + 1
#define SG_ERR_GATEWAY_CONF_ERROR                SG_ERR + 2
#define SG_ERR_GATEWAY_STATUS_ERROR              SG_ERR + 3
#define SG_ERR_MESSAGE_TEXT_NOT_NULL             SG_ERR + 4
#define SG_ERR_PARAMS_INCORRECT                  SG_ERR + 5
#define SG_ERR_PARAM_TRUNCATION                  SG_ERR + 6
#define SG_ERR_PUBLI_TRUNCATION                  SG_ERR + 7
#define SG_ERR_BAD_B_NUMBER                      SG_ERR + 8
#define SG_ERR_BAD_FORMAT_DATE                   SG_ERR + 9

/* Database accessing errors */
#define SG_ERR_DB                                3000
#define SG_ERR_DATABASE_CONNECTION_ERROR         SG_ERR_DB + 1
#define SG_ERR_DB_FETCHING_ERROR                 SG_ERR_DB + 2
#define SG_ERR_DB_NOT_FOUND_ERROR                SG_ERR_DB + 3
#define SG_ERR_DB_UNKNOWN_ERROR                  SG_ERR_DB + 4
#define SG_ERR_DB_BEGIN_TRANS_ERROR              SG_ERR_DB + 5
#define SG_ERR_DB_INSERTING_ERROR                SG_ERR_DB + 6
#define SG_ERR_DB_DELETING_ERROR                 SG_ERR_DB + 7
#define SG_ERR_DB_UPDATING_ERROR                 SG_ERR_DB + 8
#define SG_ERR_DB_BAD_REFERENCE_DATA             SG_ERR_DB + 9
#define SG_ERR_DB_ERROR                          SG_ERR_DB + 10
#define SG_ERR_DB_WARNING                        SG_ERR_DB + 11

/* SMSC errors */
#define SG_ERR_SMSC                              4000
#define SG_ERR_SMSC_CONNECTION_ERROR             SG_ERR_SMSC + 1
#define SG_ERR_SMSC_CLOSING_ERROR                SG_ERR_SMSC + 2
#define SG_ERR_SMSC_YET_CONNECTED                SG_ERR_SMSC + 3
#define SG_ERR_SMSC_UNKNOWN_SEND_ERROR           SG_ERR_SMSC + 4
#define SG_ERR_SMSC_THROTTLING_SEND_ERROR        SG_ERR_SMSC + 5
#define SG_ERR_SMSC_FULL_QUEUE_SEND_ERROR        SG_ERR_SMSC + 6
#define SG_ERR_SMSC_BAD_MESSAGE_ERROR            SG_ERR_SMSC + 7
#define SG_ERR_SMSC_LOST_CONNECTION_ERROR        SG_ERR_SMSC + 8
#define SG_ERR_SMSC_FATAL_SEND_ERROR             SG_ERR_SMSC + 9
#define SG_ERR_SMSC_UNABLE_TO_CONNECT            SG_ERR_SMSC + 10

/******************************************************************************
 * Type Definitions                                                           *
 ******************************************************************************/

/******************************************************************************
 * Global Variables                                                           *
 ******************************************************************************/

/******************************************************************************
 * Local Function Prototypes                                                  *
 ******************************************************************************/

/******************************************************************************
 * External Function Prototypes                                               *
 ******************************************************************************/

#endif /* XF_ERROR_H */
