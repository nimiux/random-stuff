/******************************************************************************
 * ++
 * Author       : Jose Maria Alonso
 *
 * Module Name  : persona.h   
 *
 * Description  :
 *
 * Status Diagrams :
 *
 * Mod. History :
 *
 * AUTHOR               DATE            DESCRIPTION
 * ----------------     --------------  ------------------------------------
 * 
 *
 * --
 ******************************************************************************/
#ifndef _PERSONA_H
#define _PERSONA_H

/******************************************************************************
 * Standards and Complance                                                    *
 ******************************************************************************/

/******************************************************************************
 * System Include Files                                                       *
 ******************************************************************************/

/******************************************************************************
 * Application Include Files                                                  *
 ******************************************************************************/

@include "_sqlfuncs_.h"

/******************************************************************************
 * Debug and Trace Options                                                    *
 ******************************************************************************/

/******************************************************************************
 * Macro Definitions                                                          *
 ******************************************************************************/
/* Definitions for PERSONA table */
#define PERSONA_ID_LENGTH         8
#define PERSONA_NOMBRE           20
#define PERSONA_APELLIDO_1       20
#define PERSONA_APELLIDO_2       20

#define PERSONA_MAX_HOSTARRAY  1000
#define PERSONA_MIN_HOSTARRAY    10

/******************************************************************************
 * Type Definitions                                                           *
 ******************************************************************************/
/* Table PERSONA */
struct __personaRow {
  long lId;
  char szNombre         [PERSONA_NOMBRE     + 1]
  char szApellido1      [PERSONA_APELLIDO_1 + 1]
  char szApellido2      [PERSONA_APELLIDO_2 + 1]
};

typedef struct __personaRow personaRow;

struct __personaInd {
  short iId;
  short iNombre;
  short iApellido1;
  short iApellido2;
};

typedef struct __personaInd personaInd;

struct __personaRowid{
  char szRowId[19];
};

typedef struct __personaRowId personaRowId;

struct __persona {
  personaRow   rgxRow    [PERSONA_MAX_HOSTARRAY];
  personaInd   rgxInd    [PERSONA_MAX_HOSTARRAY];
  personaRowId rgxRowId  [PERSONA_MAX_HOSTARRAY];
  int wRowsReaded;

  int wRowsToRead;
};

typedef struct __persona persona;

/******************************************************************************
 * Global Variables                                                           *
 ******************************************************************************/

/******************************************************************************
 * Local Function Prototypes                                                  *
 ******************************************************************************/

/******************************************************************************
 * External Function Prototypes                                               *
 ******************************************************************************/

#endif /* _PERSONA_H */

