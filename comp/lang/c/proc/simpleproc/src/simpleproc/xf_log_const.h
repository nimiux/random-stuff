/******************************************************************************
 * ++
 * Author       :
 *
 * Module Name  : 
 *
 * Description  :
 *
 * Status Diagrams :
 *
 * Mod. History :
 *
 * AUTHOR               DATE            DESCRIPTION
 * ----------------     --------------  ------------------------------------
 * Enrique Riesgo
 *
 * --
 ******************************************************************************/
#ifndef _XF_LOG_CONST_H
#define _XF_LOG_CONST_H

/******************************************************************************
 * Standards and Complance                                                    *
 ******************************************************************************/

/******************************************************************************
 * System Include Files                                                       *
 ******************************************************************************/
#include <stdlib.h>

/******************************************************************************
 * Application Include Files                                                  *
 ******************************************************************************/
#include "xf_error.h"

/******************************************************************************
 * Debug and Trace Options                                                    *
 ******************************************************************************/

/******************************************************************************
 * Macro Definitions                                                          *
 ******************************************************************************/

/******************************************************************************
 * Type Definitions                                                           *
 ******************************************************************************/
struct xf_system_error{
  int iCode;
  char *pszFormat;
};
typedef struct xf_system_error XFSystemError;

const XFSystemError rgxXFSystemErrors[]={
  { 0, "Foo message for developing and debugging purposes" },
  { SG_MSG_START_RUNNING,             "Hermes SMSC Gateway (%s) started." },
  { SG_MSG_STOPPED_BY_USER,           "Hermes SMSC Gateway is shutting down by user request..." },
  { SG_MSG_ARGUMENT_TRUNCATION,       "FATAL ERROR: SMSC Gateway '%s' identifier invalid. String maximum length is two." },
  { SG_MSG_STOPPED_BY_ERROR,          "Hermes SMSC Gateway is shutting down by previous fatal errors..." },
  { SG_MSG_EXIT_MSG,                  "Hermes SMSC Gateway exited with code %d." },
  { SG_MSG_STARTING_HERMES,           "Hermes SMSC Gateway 1.0.4 build on %s %s starting..." },
  { SG_MSG_SHUTTING_DOWN_HERMES,      "Hermes SMSC Gateway shutting down..." },
  { SG_MSG_SIGNAL_CATCHED,            "Hermes SMSC Gateway is shutting down by signal %d request..." },

  { SG_ERR_ARGUMENT_NUMBER_ERROR,     "FATAL ERROR: This program needs one parameter to start.\n\t\tUse:\tsg_start.ksh <smsc_id>\n" },
  { SG_ERR_GATEWAY_CONF_ERROR,        "FATAL ERROR: SMSC Gateway '%s' configuration error." },
  { SG_ERR_GATEWAY_STATUS_ERROR,      "FATAL ERROR: SMSC Gateway unexpected status: '%s'." },
  { SG_ERR_MESSAGE_TEXT_NOT_NULL,     "Message text must be null when MENSAJE_TIPO is not null." },
  { SG_ERR_PARAMS_INCORRECT,          "Parameters (%d: '%s') and message '%s' not compatible." },
  { SG_ERR_PARAM_TRUNCATION,          "Message truncation adding parameters '%s'. Original text: '%s'." },
  { SG_ERR_PUBLI_TRUNCATION,          "Message truncation adding publicity '%s'. Original text: '%s'." },
  { SG_ERR_BAD_B_NUMBER,              "The number %s is not valid." },
  { SG_ERR_BAD_FORMAT_DATE,           "Invalid date format for the field %s: %d." },

  { SG_ERR_DATABASE_CONNECTION_ERROR, "FATAL ERROR: Unable to connect to database." },
  { SG_ERR_DB_FETCHING_ERROR,         "FATAL ERROR: Unable to fetch data from table %s." },
  { SG_ERR_DB_NOT_FOUND_ERROR,        "Data with PK '%s' not found in table %s." },
  { SG_ERR_DB_UNKNOWN_ERROR,          "FATAL ERROR: Unknown error accessing to database." },
  { SG_ERR_DB_BEGIN_TRANS_ERROR,      "FATAL ERROR: Unable to begin transaction." },
  { SG_ERR_DB_INSERTING_ERROR,        "FATAL ERROR: Unable to insert data in table %s." }, 
  { SG_ERR_DB_DELETING_ERROR,         "FATAL ERROR: Unable to delete data from table %s." },
  { SG_ERR_DB_UPDATING_ERROR,         "FATAL ERROR: Unable to update data in table %s." },
  { SG_ERR_DB_BAD_REFERENCE_DATA,     "Missing or incorrect referece data in table %s." },  
  { SG_ERR_DB_ERROR,                  "Oracle Database error: %s" },
  { SG_ERR_DB_WARNING,                "Oracle Database warning: %s" },

  { SG_ERR_SMSC_CONNECTION_ERROR,     "Error connecting to SMSC: SMSC-%d: %s" },
  { SG_ERR_SMSC_CLOSING_ERROR,        "Error disconnecting to SMSC: %s" },
  { SG_ERR_SMSC_YET_CONNECTED,        "Yet connected to SMSC '%s'" },
  { SG_ERR_SMSC_UNKNOWN_SEND_ERROR,   "Unknown error sending SMS %d: SMSC-%d: %s" },
  { SG_ERR_SMSC_THROTTLING_SEND_ERROR,"Throttling error sending SMS %d: SMSC-%d: %s" },
  { SG_ERR_SMSC_FULL_QUEUE_SEND_ERROR,"Full queue error sending SMS %d: SMSC-%d: %s" },
  { SG_ERR_SMSC_BAD_MESSAGE_ERROR,    "Bad message error sending SMS %d: SMSC-%d: %s" },
  { SG_ERR_SMSC_LOST_CONNECTION_ERROR,"Lost connection error sending SMS %d: SMSC-%d: %s" },
  { SG_ERR_SMSC_FATAL_SEND_ERROR,     "FATAL ERROR. Unable to send SMS %d: SMSC-%d: %s" },
  
  { SG_ERR_SMSC_UNABLE_TO_CONNECT,    "FATAL ERROR. Unable to connect to SMSC." },

  { -1, NULL } 
};

/******************************************************************************
 * Global Variables                                                           *
 ******************************************************************************/

/******************************************************************************
 * Local Function Prototypes                                                  *
 ******************************************************************************/

/******************************************************************************
 * External Function Prototypes                                               *
 ******************************************************************************/

#endif /* _XF_LOG_CONST_H */


