
/* Result Sets Interface */
#ifndef SQL_CRSR
#  define SQL_CRSR
  struct sql_cursor
  {
    unsigned int curocn;
    void *ptr1;
    void *ptr2;
    unsigned long magic;
  };
  typedef struct sql_cursor sql_cursor;
  typedef struct sql_cursor SQL_CURSOR;
#endif /* SQL_CRSR */

/* Thread Safety */
typedef void * sql_context;
typedef void * SQL_CONTEXT;

/* Object support */
struct sqltvn
{
  unsigned char *tvnvsn; 
  unsigned short tvnvsnl; 
  unsigned char *tvnnm;
  unsigned short tvnnml; 
  unsigned char *tvnsnm;
  unsigned short tvnsnml;
};
typedef struct sqltvn sqltvn;

struct sqladts
{
  unsigned int adtvsn; 
  unsigned short adtmode; 
  unsigned short adtnum;  
  sqltvn adttvn[1];       
};
typedef struct sqladts sqladts;

static struct sqladts sqladt = {
  1,1,0,
};

/* Binding to PL/SQL Records */
struct sqltdss
{
  unsigned int tdsvsn; 
  unsigned short tdsnum; 
  unsigned char *tdsval[1]; 
};
typedef struct sqltdss sqltdss;
static struct sqltdss sqltds =
{
  1,
  0,
};

/* File name & Package Name */
struct sqlcxp
{
  unsigned short fillen;
           char  filnam[10];
};
static struct sqlcxp sqlfpn =
{
    9,
    "sample.pc"
};


static unsigned long sqlctx = 39235;


static struct sqlexd {
   unsigned int   sqlvsn;
   unsigned int   arrsiz;
   unsigned int   iters;
   unsigned int   offset;
   unsigned short selerr;
   unsigned short sqlety;
   unsigned int   occurs;
            short *cud;
   unsigned char  *sqlest;
            char  *stmt;
   sqladts *sqladtp;
   sqltdss *sqltdsp;
            void  **sqphsv;
   unsigned int   *sqphsl;
            int   *sqphss;
            void  **sqpind;
            int   *sqpins;
   unsigned int   *sqparm;
   unsigned int   **sqparc;
   unsigned short  *sqpadto;
   unsigned short  *sqptdso;
            void  *sqhstv[4];
   unsigned int   sqhstl[4];
            int   sqhsts[4];
            void  *sqindv[4];
            int   sqinds[4];
   unsigned int   sqharm[4];
   unsigned int   *sqharc[4];
   unsigned short  sqadto[4];
   unsigned short  sqtdso[4];
} sqlstm = {10,4};

/* SQLLIB Prototypes */
extern sqlcxt (/*_ void **, unsigned long *,
                   struct sqlexd *, struct sqlcxp * _*/);
extern sqlcx2t(/*_ void **, unsigned long *,
                   struct sqlexd *, struct sqlcxp * _*/);
extern sqlbuft(/*_ void **, char * _*/);
extern sqlgs2t(/*_ void **, char * _*/);
extern sqlorat(/*_ void **, unsigned long *, void * _*/);

/* Forms Interface */
static int IAPSUCC = 0;
static int IAPFAIL = 1403;
static int IAPFTL  = 535;
extern void sqliem(/*_ char *, int * _*/);

typedef struct { unsigned short len; unsigned char arr[1]; } VARCHAR;
typedef struct { unsigned short len; unsigned char arr[1]; } varchar;

/* CUD (Compilation Unit Data) Array */
static short sqlcud0[] =
{10,4130,0,0,0,
5,0,0,1,0,0,32,69,0,0,0,0,0,1,0,
20,0,0,2,0,0,27,95,0,0,4,4,0,1,0,1,9,0,0,1,9,0,0,1,10,0,0,1,10,0,0,
51,0,0,3,78,0,4,117,0,0,4,1,0,1,0,2,9,0,0,2,4,0,0,2,4,0,0,1,3,0,0,
82,0,0,4,0,0,32,148,0,0,0,0,0,1,0,
};


#line 1 "sample.pc"

/*
 *  sample1.pc
 *
 *  Prompts the user for an employee number,
 *  then queries the emp table for the employee's
 *  name, salary and commission.  Uses indicator
 *  variables (in an indicator struct) to determine
 *  if the commission is NULL.
 *
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sqlda.h>
#include <sqlcpr.h>

/* Define constants for VARCHAR lengths. */
#define     UNAME_LEN      20
#define     PWD_LEN        40

/* Declare variables.  No declare section is
   needed if MODE=ORACLE. */
/* VARCHAR     username[UNAME_LEN]; */ 
struct { unsigned short len; unsigned char arr[20]; } username;
#line 25 "sample.pc"
  /* VARCHAR is an Oracle-supplied struct */
/* varchar     password[PWD_LEN]; */ 
struct { unsigned short len; unsigned char arr[40]; } password;
#line 26 "sample.pc"
    /* varchar can be in lower case also. */

/* Define a host structure for the output values of
   a SELECT statement.  */
struct
{
    /* VARCHAR   emp_name[UNAME_LEN]; */ 
struct { unsigned short len; unsigned char arr[20]; } emp_name;
#line 32 "sample.pc"

    float     salary;
    float     commission;
} emprec;

/* Define an indicator struct to correspond
   to the host output struct. */
struct
{
    short     emp_name_ind;
    short     sal_ind;
    short     comm_ind;
} emprec_ind;

/*  Input host variable. */
int         emp_number;

int         total_queried;

/* Include the SQL Communications Area.
   You can use #include or EXEC SQL INCLUDE. */
#include <sqlca.h>

/* Declare error handling function. */
void sql_error(msg)
    char *msg;
{
    char err_msg[128];
    size_t buf_len, msg_len;

    /* EXEC SQL WHENEVER SQLERROR CONTINUE; */ 
#line 62 "sample.pc"


    printf("\n%s\n", msg);
    buf_len = sizeof (err_msg);
    sqlglm(err_msg, &buf_len, &msg_len);
    printf("%.*s\n", msg_len, err_msg);

    /* EXEC SQL ROLLBACK RELEASE; */ 
#line 69 "sample.pc"

{
#line 69 "sample.pc"
    struct sqlexd sqlstm;
#line 69 "sample.pc"
    sqlstm.sqlvsn = 10;
#line 69 "sample.pc"
    sqlstm.arrsiz = 0;
#line 69 "sample.pc"
    sqlstm.sqladtp = &sqladt;
#line 69 "sample.pc"
    sqlstm.sqltdsp = &sqltds;
#line 69 "sample.pc"
    sqlstm.iters = (unsigned int  )1;
#line 69 "sample.pc"
    sqlstm.offset = (unsigned int  )5;
#line 69 "sample.pc"
    sqlstm.cud = sqlcud0;
#line 69 "sample.pc"
    sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 69 "sample.pc"
    sqlstm.sqlety = (unsigned short)256;
#line 69 "sample.pc"
    sqlstm.occurs = (unsigned int  )0;
#line 69 "sample.pc"
    sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 69 "sample.pc"
}

#line 69 "sample.pc"

    exit(EXIT_FAILURE);
}

void procsample()
{
    char temp_char[32];

/* Connect to ORACLE--
 * Copy the username into the VARCHAR.
 */
    strncpy((char *) username.arr, "SCOTT", UNAME_LEN);

/* Set the length component of the VARCHAR. */
    username.len = (unsigned short) strlen((char *) username.arr);

/* Copy the password. */
    strncpy((char *) password.arr, "TIGER", PWD_LEN);
    password.len = (unsigned short) strlen((char *) password.arr);

/* Register sql_error() as the error handler. */
    /* EXEC SQL WHENEVER SQLERROR DO sql_error("ORACLE error--\n"); */ 
#line 90 "sample.pc"


/* Connect to ORACLE.  Program will call sql_error()
 * if an error occurs when connecting to the default database.
 */
    /* EXEC SQL CONNECT :username IDENTIFIED BY :password; */ 
#line 95 "sample.pc"

{
#line 95 "sample.pc"
    struct sqlexd sqlstm;
#line 95 "sample.pc"
    sqlstm.sqlvsn = 10;
#line 95 "sample.pc"
    sqlstm.arrsiz = 4;
#line 95 "sample.pc"
    sqlstm.sqladtp = &sqladt;
#line 95 "sample.pc"
    sqlstm.sqltdsp = &sqltds;
#line 95 "sample.pc"
    sqlstm.iters = (unsigned int  )10;
#line 95 "sample.pc"
    sqlstm.offset = (unsigned int  )20;
#line 95 "sample.pc"
    sqlstm.cud = sqlcud0;
#line 95 "sample.pc"
    sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 95 "sample.pc"
    sqlstm.sqlety = (unsigned short)256;
#line 95 "sample.pc"
    sqlstm.occurs = (unsigned int  )0;
#line 95 "sample.pc"
    sqlstm.sqhstv[0] = (         void  *)&username;
#line 95 "sample.pc"
    sqlstm.sqhstl[0] = (unsigned int  )22;
#line 95 "sample.pc"
    sqlstm.sqhsts[0] = (         int  )22;
#line 95 "sample.pc"
    sqlstm.sqindv[0] = (         void  *)0;
#line 95 "sample.pc"
    sqlstm.sqinds[0] = (         int  )0;
#line 95 "sample.pc"
    sqlstm.sqharm[0] = (unsigned int  )0;
#line 95 "sample.pc"
    sqlstm.sqadto[0] = (unsigned short )0;
#line 95 "sample.pc"
    sqlstm.sqtdso[0] = (unsigned short )0;
#line 95 "sample.pc"
    sqlstm.sqhstv[1] = (         void  *)&password;
#line 95 "sample.pc"
    sqlstm.sqhstl[1] = (unsigned int  )42;
#line 95 "sample.pc"
    sqlstm.sqhsts[1] = (         int  )42;
#line 95 "sample.pc"
    sqlstm.sqindv[1] = (         void  *)0;
#line 95 "sample.pc"
    sqlstm.sqinds[1] = (         int  )0;
#line 95 "sample.pc"
    sqlstm.sqharm[1] = (unsigned int  )0;
#line 95 "sample.pc"
    sqlstm.sqadto[1] = (unsigned short )0;
#line 95 "sample.pc"
    sqlstm.sqtdso[1] = (unsigned short )0;
#line 95 "sample.pc"
    sqlstm.sqphsv = sqlstm.sqhstv;
#line 95 "sample.pc"
    sqlstm.sqphsl = sqlstm.sqhstl;
#line 95 "sample.pc"
    sqlstm.sqphss = sqlstm.sqhsts;
#line 95 "sample.pc"
    sqlstm.sqpind = sqlstm.sqindv;
#line 95 "sample.pc"
    sqlstm.sqpins = sqlstm.sqinds;
#line 95 "sample.pc"
    sqlstm.sqparm = sqlstm.sqharm;
#line 95 "sample.pc"
    sqlstm.sqparc = sqlstm.sqharc;
#line 95 "sample.pc"
    sqlstm.sqpadto = sqlstm.sqadto;
#line 95 "sample.pc"
    sqlstm.sqptdso = sqlstm.sqtdso;
#line 95 "sample.pc"
    sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 95 "sample.pc"
    if (sqlca.sqlcode < 0) sql_error("ORACLE error--\n");
#line 95 "sample.pc"
}

#line 95 "sample.pc"


    printf("\nConnected to ORACLE as user: %s\n", username.arr);

/* Loop, selecting individual employee's results */

    total_queried = 0;

    for (;;)
    {
        emp_number = 0;
        printf("\nEnter employee number (0 to quit): ");
        gets(temp_char);
        emp_number = atoi(temp_char);
        if (emp_number == 0)
            break;

/* Branch to the notfound label when the
 * 1403 ("No data found") condition occurs.
 */
        /* EXEC SQL WHENEVER NOT FOUND GOTO notfound; */ 
#line 115 "sample.pc"


        /* EXEC SQL SELECT ename, sal, comm
            INTO :emprec INDICATOR :emprec_ind
            FROM EMP
            WHERE EMPNO = :emp_number; */ 
#line 120 "sample.pc"

{
#line 117 "sample.pc"
        struct sqlexd sqlstm;
#line 117 "sample.pc"
        sqlstm.sqlvsn = 10;
#line 117 "sample.pc"
        sqlstm.arrsiz = 4;
#line 117 "sample.pc"
        sqlstm.sqladtp = &sqladt;
#line 117 "sample.pc"
        sqlstm.sqltdsp = &sqltds;
#line 117 "sample.pc"
        sqlstm.stmt = "select ename ,sal ,comm into :s2:s1 ,:s4:s3 ,:s6:s5  \
 from EMP where EMPNO=:b2";
#line 117 "sample.pc"
        sqlstm.iters = (unsigned int  )1;
#line 117 "sample.pc"
        sqlstm.offset = (unsigned int  )51;
#line 117 "sample.pc"
        sqlstm.selerr = (unsigned short)1;
#line 117 "sample.pc"
        sqlstm.cud = sqlcud0;
#line 117 "sample.pc"
        sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 117 "sample.pc"
        sqlstm.sqlety = (unsigned short)256;
#line 117 "sample.pc"
        sqlstm.occurs = (unsigned int  )0;
#line 117 "sample.pc"
        sqlstm.sqhstv[0] = (         void  *)&emprec.emp_name;
#line 117 "sample.pc"
        sqlstm.sqhstl[0] = (unsigned int  )22;
#line 117 "sample.pc"
        sqlstm.sqhsts[0] = (         int  )0;
#line 117 "sample.pc"
        sqlstm.sqindv[0] = (         void  *)&emprec_ind.emp_name_ind;
#line 117 "sample.pc"
        sqlstm.sqinds[0] = (         int  )0;
#line 117 "sample.pc"
        sqlstm.sqharm[0] = (unsigned int  )0;
#line 117 "sample.pc"
        sqlstm.sqadto[0] = (unsigned short )0;
#line 117 "sample.pc"
        sqlstm.sqtdso[0] = (unsigned short )0;
#line 117 "sample.pc"
        sqlstm.sqhstv[1] = (         void  *)&emprec.salary;
#line 117 "sample.pc"
        sqlstm.sqhstl[1] = (unsigned int  )sizeof(float);
#line 117 "sample.pc"
        sqlstm.sqhsts[1] = (         int  )0;
#line 117 "sample.pc"
        sqlstm.sqindv[1] = (         void  *)&emprec_ind.sal_ind;
#line 117 "sample.pc"
        sqlstm.sqinds[1] = (         int  )0;
#line 117 "sample.pc"
        sqlstm.sqharm[1] = (unsigned int  )0;
#line 117 "sample.pc"
        sqlstm.sqadto[1] = (unsigned short )0;
#line 117 "sample.pc"
        sqlstm.sqtdso[1] = (unsigned short )0;
#line 117 "sample.pc"
        sqlstm.sqhstv[2] = (         void  *)&emprec.commission;
#line 117 "sample.pc"
        sqlstm.sqhstl[2] = (unsigned int  )sizeof(float);
#line 117 "sample.pc"
        sqlstm.sqhsts[2] = (         int  )0;
#line 117 "sample.pc"
        sqlstm.sqindv[2] = (         void  *)&emprec_ind.comm_ind;
#line 117 "sample.pc"
        sqlstm.sqinds[2] = (         int  )0;
#line 117 "sample.pc"
        sqlstm.sqharm[2] = (unsigned int  )0;
#line 117 "sample.pc"
        sqlstm.sqadto[2] = (unsigned short )0;
#line 117 "sample.pc"
        sqlstm.sqtdso[2] = (unsigned short )0;
#line 117 "sample.pc"
        sqlstm.sqhstv[3] = (         void  *)&emp_number;
#line 117 "sample.pc"
        sqlstm.sqhstl[3] = (unsigned int  )sizeof(int);
#line 117 "sample.pc"
        sqlstm.sqhsts[3] = (         int  )0;
#line 117 "sample.pc"
        sqlstm.sqindv[3] = (         void  *)0;
#line 117 "sample.pc"
        sqlstm.sqinds[3] = (         int  )0;
#line 117 "sample.pc"
        sqlstm.sqharm[3] = (unsigned int  )0;
#line 117 "sample.pc"
        sqlstm.sqadto[3] = (unsigned short )0;
#line 117 "sample.pc"
        sqlstm.sqtdso[3] = (unsigned short )0;
#line 117 "sample.pc"
        sqlstm.sqphsv = sqlstm.sqhstv;
#line 117 "sample.pc"
        sqlstm.sqphsl = sqlstm.sqhstl;
#line 117 "sample.pc"
        sqlstm.sqphss = sqlstm.sqhsts;
#line 117 "sample.pc"
        sqlstm.sqpind = sqlstm.sqindv;
#line 117 "sample.pc"
        sqlstm.sqpins = sqlstm.sqinds;
#line 117 "sample.pc"
        sqlstm.sqparm = sqlstm.sqharm;
#line 117 "sample.pc"
        sqlstm.sqparc = sqlstm.sqharc;
#line 117 "sample.pc"
        sqlstm.sqpadto = sqlstm.sqadto;
#line 117 "sample.pc"
        sqlstm.sqptdso = sqlstm.sqtdso;
#line 117 "sample.pc"
        sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 117 "sample.pc"
        if (sqlca.sqlcode == 1403) goto notfound;
#line 117 "sample.pc"
        if (sqlca.sqlcode < 0) sql_error("ORACLE error--\n");
#line 117 "sample.pc"
}

#line 120 "sample.pc"


/* Print data. */
        printf("\n\nEmployee   Salary    Commission\n");
        printf("--------   -------   ----------\n");

/* Null-terminate the output string data. */
        emprec.emp_name.arr[emprec.emp_name.len] = '\0';
        printf("%s      %7.2f      ",
            emprec.emp_name.arr, emprec.salary);

        if (emprec_ind.comm_ind == -1)
            printf("NULL\n");
        else
            printf("%7.2f\n", emprec.commission);

        total_queried++;
        continue;

notfound:
        printf("\nNot a valid employee number - try again.\n");

    } /* end for(;;) */

    printf("\n\nTotal rows returned was %d.\n", total_queried); 
    printf("\nG'day.\n\n\n");

/* Disconnect from ORACLE. */
    /* EXEC SQL ROLLBACK WORK RELEASE; */ 
#line 148 "sample.pc"

{
#line 148 "sample.pc"
    struct sqlexd sqlstm;
#line 148 "sample.pc"
    sqlstm.sqlvsn = 10;
#line 148 "sample.pc"
    sqlstm.arrsiz = 4;
#line 148 "sample.pc"
    sqlstm.sqladtp = &sqladt;
#line 148 "sample.pc"
    sqlstm.sqltdsp = &sqltds;
#line 148 "sample.pc"
    sqlstm.iters = (unsigned int  )1;
#line 148 "sample.pc"
    sqlstm.offset = (unsigned int  )82;
#line 148 "sample.pc"
    sqlstm.cud = sqlcud0;
#line 148 "sample.pc"
    sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 148 "sample.pc"
    sqlstm.sqlety = (unsigned short)256;
#line 148 "sample.pc"
    sqlstm.occurs = (unsigned int  )0;
#line 148 "sample.pc"
    sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 148 "sample.pc"
    if (sqlca.sqlcode < 0) sql_error("ORACLE error--\n");
#line 148 "sample.pc"
}

#line 148 "sample.pc"

    exit(EXIT_SUCCESS);
}

