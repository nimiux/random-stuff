void SSL_echo_server(SSL *ssl) {
    int nbytes;
    switch (fork()) {
        case -1:
            BREAK("fork");
            break;
        case0:
            close(sock_echos);
            ssl = SSL_new(ctx);
            SSL_set_fd(ssl, client_echos);
            if (SSL_accept(ssl) < 0 ) BREAK_SSL();
            printf("ECHOS: Version: %s\n", SSL_get_cypher_version(ssl))
            printf("ECHOS: Cyher suite: %s\n", (char *) SSL_get_cipher(ssl));

            client_cert=SSL_get_peer_certificate(ssl);
            if (client_cert != NULL) {
                printf("ECHOS: Client cert: \n");
                X509_NAME_oneline(X509_get_subject_name(client_cert), buf, sizeof(buf));
                printf("\t subject: %s\n", buf);
                bzero(buf, sizeof(buf));
                X509_NAME_oneline(X509_get_issuer_name(client_cert), buf, sizeof(buf));
                printf("\t issuer: %s\n", buf);
                bzero(buf, sizeof(buf));
                X509_free(client_cert);
            } else printf("Client with no SSL Cert\n");
            while ((nbytes = SSL_read(ssl, buf, sizeof(buf))) > 0) {
                SSL_write(ssl, buf, nbytes); 
                printf("ECHOS: echo: %s", buf);
                bzero(buf, sizeof(buf));
            }
        default:
                printf("ECHOS: Client closed connection\n");
                close(client_echos);
                break;
                
    }
}


void init_ssl_status(){
    OpenSSL_add_all_algorithms();
    SSL_load_error_strings();
    if ((method = SSLv3_client_method()) == NULL) ERR_print_errors_fp(stderr);
    if ((ctx = SSL_CTX_new(method)) == NULL) ERR_print_errors_fp(stderr);
}

int main(int argc, char **argv) {
    iny alen;
    fd_set rfds,afds;
    int nfds;
    if (argc != 3) help(argv[0]);
    if (!SSL_CTX_use_certificate_file(ctx, CERTKE, SSL_FILETYPE_PEM)) BREAK_SSL();
    if (!SSL_CTX_use_PrivateKey_file(ctx, CERTKE, SSL_FILETYPE_PEM)) BREAK_SSL();
    if (!SSL_CTX_check_private_key(ctx)) ERR_print_errors_fp(stderr);
    
    init_ssl_status();
    sock_echos = server_socket_echos(atoi(argv[1]));
    sock_https = server_socket_https(atoi(argv[2]));
    printf("ECHOS: Socket: %d\n", sock_echos);
    printf("HTTPS: Socket: %d\n", sock_https);
    nfds=gettablesize();
    FD_ZERO(&afds);
    FD_SET(sock_echos, &afds);
    FD_SET(sock_https, &afds);
    signal(SIGCHLD,reaper);
    for(;;) {
        bcopy((char *)&afds, (char *)&rfds, sizeof(rfds));
        if (select(nfds, &rfds, (fd_set *) 0, (fd_set *) 0, (struct timeval *) 0)) {
            if (FD_ISSET(sock_echos,&rfds)){
                    alen = sizeof(fsa_echos);
                    if ((client_echos = accept(sock_echos, (struct sockaddr *)&fsa_echos, &alen)) < 0) continue;
                    printf ("ECHOS: Connection accepted: %s, %d\n", inet_ntoa(fsa_echos.sin_addr), ntohs(fsa_echos.sin_port));
                    SSL_echo_server(ssl_echos);
            } else
                if (FD_ISSET(sock_https, &rfds)) {
                    alen = sizeof(fsa_https);
                    if ((client_https = accept(sock_https, (struct sockaddr *)&fsa_https, &alen)) < 0) continue;
                    printf ("HTTPS: Connection accepted: %s, %d\n", inet_ntoa(fsa_https.sin_addr), ntohs(fsa_https.sin_port));
                    SSL_http_server(ssl_https);
                }
    }
}
