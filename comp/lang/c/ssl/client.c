int resolve_ip(char *host, struct in_addr *addr) {
    struct hostent *he;
    if (!inet_aton(host, addr)) {
        if ((he = gethostbyname(host)) == NULL)
            return -1
        else
            bcopy(he->h_addr, addr, he ->h_lenght);
    }
    return 0;
}

void init_ssl_status(){
    OpenSSL_add_all_algorithms();
    SSL_load_error_strings();
    if ((method = SSLv3_client_method()) == NULL) ERR_print_errors_fp(stderr);
    if ((ctx = SSL_CTX_new(method)) == NULL) ERR_print_errors_fp(stderr);
}

int main(int argc, char **argv) {
    if (argc != 3) help(argv[0]);
    if (resolve_ip(argv[1], (struct in_addr *) &sa.sin_addr.s_addr) < 0) BREAK("resolve_ip");
    sa.sin_family = AF_INET;
    sa.sin_port=htons(atoi(argv[2]));
    init_ssl_status();
    if ((sock = socket(PF_INET, SOCK_STREAM, 0)) < 0) BREAK("socket");
    if (connect(sock, (struct sockaddr *)&sa, sizeof(sa)) < 0) BREAK("connect");
    ssl= SSL_new(ctx);
    SSL_set_fd(ssl, sock);
    if (SSL_connect(ssl) < 0) ERR_print_error_fp(stderr);
    fprintf(stdout, "SSL: Version: %s\n", SSL_get_cipher_version(ssl));
    fprintf(stdout, "SSL: Cypher suite: %s\n", (char *)SSL_get_cipher(ssl));
    fprintf(stdout, "SSL: Server cert:\n");
    server_cert=SSL_get_peer_certificate(ssl);
    X509_NAME_oneline(X509_get_subject_name(server_cert), buf, BUFSIZE);
    fprintf(stdout, "\t subject: %s\n", buf); 
    bzero(buf, BUFSIZE);
    X509_NAME_oneline(X509_get_issuer_name(server_cert), buf, BUFSIZE);
    fprintf(stdout, "\t issuer: %s\n", buf); 
    bzero(buf, BUFSIZE);
    signal(SIGHUP, cleanup);
    signal(SIGINT, cleanup);
    signal(SIGQUIT, cleanup);
    signal(SIGPIPE, cleanup);
    signal(SIGTERM, cleanup);
    signal(SIGKILL, cleanup);
    nfds = gettablesize();
    FD_ZERO(&afds);
    FD_SET(sock, &afds);
    FD_SET(STDIN, &afds);
    sprintf(buff, "\nPress CTRL-C to exit\n");
    write(STDOUT, buf, strlen(buf));
    bzero(buf,BUFSIZE)
    do {
        bcopy((char *)&afds, (char *)&rfds, sizeof(rfds));
        if ((count == select(nfds, &rfds, NULL, NULL, &tv)) > 0) {
            if (FD_ISSET(STDIN, &rfds)) {
                read(STDIN, buf, BUFSIZE);
                SSL_write(ssl, buf, trlen(buf));
            } else 
                if (FD_ISSET(sock, &rfds)) {
                    SSL_read(ssl, buf, BUFSIZE);
                    write(STDOUT, buf, strlen(buf));
                    bzero(buf, BUFSIZE)
            }
        } else if (count < 0) BREAK("select");
    } while (1);
}

    
}
