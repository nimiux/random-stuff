#include <stdio.h>
#include <string.h>

struct Alumno {
	char nombre[9], apellidos[9];
	long expediente;
};

typedef struct Alumno Alumno;

int main ( int argc, char **argv) {
	
	Alumno a;
	a.nombre[0]=0;
	a.apellidos[0]=0;
	a.expediente=90L;
	strcpy(a.nombre, "Chema");
	strcpy(a.apellidos, "Alonso");
	printf("Hello World!\n");
	printf("Alumno %d. %s,%s.\n", a.expediente, a.apellidos, a.nombre);

}
