#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "GL/gl.h"
#include "GL/glut.h"

void draw(void) {
    glClear(GL_COLOR_BUFFER_BIT);// clear the buffer bit
    glColor3f(1.0f,0.0f,0.0f);// Red
    glRectf(100.0f,150.0f,150.0f,100.0f);// rectangle 
    glFlush();
}

void init(void) {
    glClearColor(0.0f,1.0f,0.0f,1.0f); // blue
}

void ChangeSize(GLsizei w, GLsizei h) {
    if (h==0) // prevent divide by zero
        h=1;

    glViewport(0,0,w,h);// set viewport
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
     
    if(w<=h)
        glOrtho(0.0f,250.0f,0.0f,250.0f*h/w,1.0,-1.0);
    else  
        glOrtho(0.0f,250.0f*w/h,0.0f,250.0f,1.0,-1.0);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

int main(int argc,char **argv) {
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutCreateWindow("Rectangle ");
    glutDisplayFunc(draw);
    glutReshapeFunc(ChangeSize);
    init();
    glutMainLoop();
    return 0;
}
