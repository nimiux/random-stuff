#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "dyn.h"

extern void pruebame();

#define THETYPE int

int dinero=90;
static int parne=9;

struct StackNode {
	THETYPE data;
	struct StackNode *pnext;
};

typedef struct StackNode StackNode;

typedef StackNode *Stack;

void push(Stack *s, THETYPE data) {
	//StackNode *pn=(StackNode *)malloc(sizeof(StackNode));
	StackNode *pn=NEW(StackNode);
	pn->data=data;
	pn->pnext=*s;
	*s=pn;
	printf("S= %d\n", parne);
}

THETYPE *pop(Stack *s) {
	if (*s) {
		THETYPE *p=NEW(THETYPE);
		StackNode *aux=*s;
		*p=aux->data;
		*s=aux->pnext;
		//DELETE(aux);
		return p;
	} else {
		return NULL;
	}
}

int main ( int argc, char **argv) {

	Stack s=NULL;
	int *data;
	pruebame();
	push(&s,3);
	push(&s,2);
	push(&s,1);
	while (data=pop(&s)) {
		printf ("%d.\n", *data);
		DELETE(data);
	}
	printf("Saldo de bytes: %ld.\n", __bytes);
	return 0;

}

