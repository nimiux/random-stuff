#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>



#include "swap.h"

void printit(int v[]) {
	int i;
	for (i=1;i<=VELEMS;i++) {
		printf("%5d%s", v[i-1], (i%10?" ":"\n"));
	}
	printf("\n");
	printf("%d swaps.\n", __swaps);
}

// Minimo 
void MinSort (int v[]) {
	int i,j;
	for (i=0;i<VELEMS;i++) {
		for (j=i+1;j<VELEMS;j++) {
			if (v[j] < v[i]) {
				SWAP(int,v[j],v[i]); 
			}	
		}
	}
}

// Bubble
void BubbleSort (int v[]) {
	int i,j,flipped;
	i=VELEMS-1;
	flipped=1;
	while ((i>=0) && (flipped)) {
		flipped=0;
		j=0;
		while (j<i) {
			if (v[j] > v[j+1]) {
				SWAP(int,v[j],v[j+1]); 
				flipped=1;
			}	
			j++;
		}
		i--;
	}
}

// Insercion
void InsertSort (int v[]) {
	int i;
	for (i=1; i<VELEMS; i++) {
		int j=i;
		int T=v[j]; __swaps++;
		while ((j>0) && (v[j-1] > T)) {
			v[j]=v[j-1]; __swaps++;
			j--;
		}
		v[j]=T; __swaps++;
	}
	__swaps /=3;
}

// Shell 
void ShellSort (int v[]) {
	int h=1;

	while ((3*h+1) < VELEMS) {
		h=(3*h+1);
	}
	while (h>0) {
		int i;
		for (i=h-1; i<VELEMS; i++) {
			int j=i;
			int T=v[j]; __swaps++;
			while ((j>h-1) && (v[j-h] > T)) {
				v[j]=v[j-h]; __swaps++;
				j-=h;
			}
			v[j]=T; __swaps++;
		}
		h/=3;
	}
}

// Heap 
void downheap(int v[], int k, int N) {
	int T;
	T=v[k-1];
	while (k<N/2) {
		int j=2*k;
		if ((j<N) && (v[j-1] < v[j])) {
			j++;
		}
		if (T>=v[j-1]) {}
	}
}
void HeapSort (int v[]) {
	int k,N=VELEMS;

	for (k=N/2;k>0;k--) {
		downheap(v,k,N);	
	}
	do {
		N--;
	} while (N>1);
}

// QuickSort 
int split (int v[], int ini, int fin) {
	int i, pos=ini;
	for (i=ini; i<=fin; i++) {
		if (v[ini] > v[i]) 	{
			pos++;
			SWAP(int,v[pos],v[i]);
		}
	}					
	SWAP(int,v[pos],v[ini]); 
	return pos;
}

void _QuickSort(int v[], int ini, int fin) {
	if (ini < fin) {
		int pos=split(v, ini, fin);
		_QuickSort(v, ini, pos-1);
		_QuickSort(v, pos+1, fin);
	}
}	

void QuickSort(int v[]) {
	_QuickSort(v, 0, VELEMS-1);
}


int main ( int argc, char **argv) {

	// Ejem....
	/*int i;
	for (i=1;i<129;i++) {
		printf("%d. %c\n", i, i);	
	}
	char led[]={ 124, 47, 45, 92 };
	int i=0;
	while (1) {
		printf("%c", led[(i%4?i%4:0)]);
		i++;
		sleep(1);
		printf("%c", 8);
	} */




	int (*a)[] = malloc (sizeof(v));
	int (*b)[] = malloc (sizeof(v));
	int (*c)[] = malloc (sizeof(v));
	int (*d)[] = malloc (sizeof(v));
	int (*e)[] = malloc (sizeof(v));
	int (*f)[] = malloc (sizeof(v));
	memcpy(a,v,sizeof(v));
	memcpy(b,v,sizeof(v));
	memcpy(c,v,sizeof(v));
	memcpy(d,v,sizeof(v));
	memcpy(e,v,sizeof(v));
	memcpy(f,v,sizeof(v));

//	MinSort(*a);
	RESETSWAP;

//	BubbleSort(*b);
	RESETSWAP;
	
//	InsertSort(*c);
	RESETSWAP;

//	ShellSort(*d);
	RESETSWAP;
	
//	HeapSort(*e);
	RESETSWAP;

	QuickSort(*f);
	printit(*f);
	RESETSWAP;
	return 0;

}

