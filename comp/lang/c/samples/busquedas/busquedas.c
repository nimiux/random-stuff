#include <stdio.h>
#include <string.h>

#include "swap.h"

// QuickSort 
int prepara (int v[], int ini, int fin) {
	int i, pos=ini;
	for (i=ini; i<=fin; i++) {
		if (v[ini] > v[i]) 	{
			pos++;
			SWAP(int,v[pos],v[i]);
		}
	}					
	SWAP(int,v[pos],v[ini]); 
	return pos;
}

void quicksort(int v[], int ini, int fin) {
	if (ini < fin) {
		int pos=prepara(v, ini, fin);
		quicksort(v, ini, pos -1);
		quicksort(v, pos+1, fin);
	}
}	



void printit(int v[]) {
	int i;
		for (i=0;i<VELEMS;i++) {
			printf("%d. ", v[i]);
		}
		printf("\n");


}

int blineal (int v[], int elem) {
	int end=0,i=0;
	while (!end && (i<VELEMS)) {
		end=(v[i++]==elem?1:0);
	}
	return(end?i-1:-1);
}	

int bbinariai (int v[], int elem) {
	int ini=0,fin=VELEMS-1,cen;
	cen=(ini+fin/2);
	
	while ((v[cen] != elem) && (ini<fin)) {
		if (v[cen] < elem) {
			ini=cen+1;
		} else {
			fin=cen-1;
		}
		cen=(ini+fin/2);
	}
	return (v[cen]==elem?cen:-1);
}

int bbinariar (int v[], int elem, int ini, int fin) {
	int cen;
	if (ini>fin) {
		return -1;
	} else {
		cen=(int)(ini+fin/2);
		if (v[cen] < elem) {
			return bbinariar(v, elem, cen+1, fin);
		} else { 
			if (v[cen] > elem) {
				return bbinariar(v, elem, ini, cen-1);
			} else {
				printf("CEN %d.\n", cen);
				return cen;
			}
		}	
	}	
}	

int main ( int argc, char **argv) {
	
	printit(v);
	printf("Busco 3 esta en %d\n", blineal(v,3));
	quicksort(v,0, VELEMS-1);
	printf("Busco 3 esta en %d\n", bbinariai(v,3));
	printf("Busco 3 esta en %d\n", bbinariar(v,3,0,VELEMS-1));
	return 0;

}
