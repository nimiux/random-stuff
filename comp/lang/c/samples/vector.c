#include <stdio.h>
#include <string.h>

#define VELEMS 90

int main ( int argc, char **argv) {

	char (*v)[8];
	
	int i,a[VELEMS];
	long total=0;
	for (i=0;i<VELEMS;i++) {
		a[i]=2*i;
	}
	for (i=0;i<VELEMS;i++) {
		total+=a[i];
	}
	(*v)[0]='a';
	(*v)[1]='b';
	(*v)[2]='c';
	(*v)[3]='\0';
	printf("Hello World!\n");
	printf("El total es %ld.\n", total);
	printf("-------\n");
	printf(". %s.\n", *v);

}
