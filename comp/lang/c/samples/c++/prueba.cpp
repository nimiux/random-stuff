#include <iostream>

using namespace std;

class Auto {
	protected:
		int ruedas;
	public:
		inline Auto() { this->ruedas=4; cout << "Auto::Auto" << endl; }
		virtual ~Auto();
};

Auto::~Auto() {
	cout << "Auto::~Auto" << endl;
}

class Coche : public Auto {

	private:
		int plazas;
	public:
		inline Coche() { this->plazas=5; cout << "Coche::Coche" << endl;}
		~Coche();

};

Coche::~Coche() {
	cout << "Coche::~Coche" << endl;
}

int main ( int argc, char **argv) {

	Auto *a=(Auto *) new Coche();
	delete (a);
}




