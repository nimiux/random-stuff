#include <stdio.h>


#define cat(x,y) x ## Y


int main ( int argc, char **argv) {
	int i,j;
	int a[3][3];
	for (i=0; i<3; i++) {
		for (j=0; j<3; j++) {
			a[i][j]=i*j;
		}
	}
	printf("Hello World!\n");
	printf("[2,2]. %d.\n", a[2][2]);
	printf("cat(12,34)=%s\n", cat(12,34));
	printf("cat(cat(1,2),34)=%s\n", cat(12,34));
}
