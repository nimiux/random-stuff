#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void printit(int *v, int n) {
	int i;

	printf("int __swaps;\n");
	printf("#define RESETSWAP __swaps=0;\n");

	printf("#define SWAP(t,x,y) { t _temp = (x); (x)=(y); "
			"(y) =_temp; __swaps++; }\n");
	printf("#define VELEMS %d\n", n);
	printf("int v[VELEMS] = { ");
	for (i=0;i<n;i++) {
		printf("%d%c", v[i], (i==n-1?' ':','));
		printf(i%11==0?"\n":" ");
	}
	printf("};\n");
}


int main ( int argc, char **argv) {

	int i,n,c;
	int *array;
	if (argv[1] && (n=atoi(argv[1]))) {

		array=(int *)malloc(n*sizeof(int));
		memset(array,-1,n*sizeof(int));
		srand(2342);
		for (i=0; i<n;i++) {
			double cell=(double)rand()/RAND_MAX*n;
			c=(int)cell;
			while (array[c] != -1) {
				c=(c<n?c+1:0);
			}
			array[c]=i;
		}
		printit(array, n);
	}
	
	return 0;

}
