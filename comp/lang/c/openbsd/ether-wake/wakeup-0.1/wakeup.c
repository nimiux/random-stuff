/* wakeup version 0.1 */

/*
 * Copyright (c) 2005 Markus Hennecke
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the Author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR 
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <sys/types.h>

#include <libnet.h>
#include <stdio.h>

unsigned char enet_dst[6] = { 0xff, 0xff, 0xff, 0xff, 0xff, 0xff };
unsigned char enet_src[6] = { 0x00, 0xa0, 0xc9, 0x45, 0x58, 0x0b };
unsigned char mac[6] = { 0x00, 0x90, 0x27, 0xa2, 0x02, 0x5d };

int
parse_mac(const char *s, unsigned char *mac)
{
	int i, j, pos;
	char c;

	/* Check for the right lengths */
	if (strlen(s) != 17)
		return 0;

	j = 0;
	pos = 0;
	for (i = 0; i < 17; i++) {
		c = toupper(s[i]);
		switch (i) {
		case 2:
		case 5:
		case 8:
		case 11:
		case 14:
			if ((c != ':') && (c != '-'))
				return 0;
			break;
		default:
			if (! isxdigit(c))
				return 0;

			c -= 0x30;
			if (c > 9)
				c -= 7;

			mac[pos] = (j % 2) ? (mac[pos] | c) : (c << 4);

			if (j++ % 2)
				pos++;
		}
	}
			
	return 1;
}

__dead void
usage(void)
{
	fprintf(stderr, "USAGE: wakeup interface mac\n");
	exit(1);
}

int
main(int argc,  char **argv)
{
	unsigned char *packet, *payload;
	char err_buf[LIBNET_ERRBUF_SIZE];

	unsigned long src_ip, dst_ip;
	unsigned short src_port, dst_port;
	int packet_size, payload_len, c, i;

	unsigned char *device;
	struct libnet_link_int *network;
	struct ether_addr *ether_addr;

	if (argc != 3)
		usage();
	
	if (! parse_mac(argv[2], mac))
		usage();

	device = argv[1];

	dst_ip = 0xffffffff;

	src_port = 40000;
	dst_port = 9;

	if ((network = libnet_open_link_interface(device, err_buf)) == NULL) {
		libnet_error(LIBNET_ERR_FATAL,
			     "wakeup: Error opening device: %s\n", err_buf);
	}

	src_ip = ntohl(libnet_get_ipaddr(network, device, err_buf));
	if (src_ip == 0) {
		libnet_error(LIBNET_ERR_FATAL, 
			     "wakeup: Unable to read inet address from %s:"
			     " %s\n", 
			     device, err_buf);
	}

	ether_addr = libnet_get_hwaddr(network, device, err_buf);
	if (ether_addr == NULL) {
		libnet_error(LIBNET_ERR_FATAL, 
			     "wakeup: Unable to read mac address from %s:"
			     " %s\n", 
			     device, err_buf);
	}

	memcpy(enet_src, ether_addr->ether_addr_octet, 6);
		
	packet_size = LIBNET_ETH_H + LIBNET_IP_H + LIBNET_UDP_H + (17*6);

	libnet_init_packet(packet_size, &packet);
	if (packet == NULL) {
		libnet_error(LIBNET_ERR_FATAL,  
			     "wakeup: libnet_init_packet failed\n");
	}

	libnet_build_ethernet(enet_dst, enet_src, 
			      ETHERTYPE_IP, NULL, 0, packet);

	payload_len = 17 * 6;
	payload = malloc(payload_len);
	if (! payload)
		goto error;

	memcpy(payload, enet_dst, 6);
	for (i = 1; i < 17; i++) {
		memcpy(&payload[i * 6], mac, 6);
	}

	libnet_build_ip(LIBNET_UDP_H, IPTOS_LOWDELAY, 242,
			0, 1, IPPROTO_UDP, src_ip, dst_ip, 
			NULL, 0, packet + ETH_H);

	libnet_build_udp(src_port, dst_port,
			 payload, payload_len, packet + LIBNET_IP_H + ETH_H);

	if (libnet_do_checksum(packet + ETH_H, IPPROTO_UDP, LIBNET_UDP_H) == -1) {
		libnet_error(LN_ERR_FATAL,
			     "wakeup: Calculating checksum failed\n");
	}
	if (libnet_do_checksum(packet + ETH_H, IPPROTO_IP, LIBNET_IP_H) == -1) {
		libnet_error(LN_ERR_FATAL,
			     "wakeup: Calculating checksum failed\n");
	}
	c = libnet_write_link_layer(network, device, packet, packet_size);
	if (c < packet_size)
		libnet_error(LN_ERR_WARNING, 
			     "Error writing packet. "
			     "Only %d of %d bytes send\n", c, packet_size);
	free(payload);
 error:
	libnet_close_link_interface(network);

	libnet_destroy_packet(&packet);

	return 0;
}
