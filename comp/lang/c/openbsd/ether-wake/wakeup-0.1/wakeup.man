.TH WAKEUP 1 "Januar, 9th 2005"
.SH NAME
wakeup \- Generate Wake On LAN packets for a network
.SH SYNOPSIS
\fBwakeup\fP \fIinterface \fP \fImac-address\fP
.SH DESCRIPTION
\fIwakeup\fP generates an UDP packet on the specified interface. The
interface has to be an ethernet link. The ethernet frame is
constructed so that the destination address is ff:ff:ff:ff:ff:ff, the
source address is read from the interface. The UDP Packet contains the
source address of the interface and 255.255.255.255:9 as the
destination address. The payload is the "magic packet". This packet
consists of 6 Bytes with 0xff and 16 times the mac address of the
computer that should wake up.
.SS Options
.TP
\fBinterface\fP
The interface on which the packet should be generated.
.TP
\fBmac-address\fR
The mac address of the target computer. It must be given as
aa:bb:cc:dd:ee:ff or aa-bb-cc-dd-ee-ff.
.SH BUGS
None known at this time.