#include <X11/Xlib.h> 
#include <X11/keysym.h> 
#include <X11/Xutil.h> 
#include <assert.h>   
#include <unistd.h>   

void doX();
void drawit(Display *xdisp, Window xw, GC gc);

main(int argc, char **argv) {
   doX();
}


void doX( ) {
   Display *xdisp = XOpenDisplay(NULL);
   assert(xdisp);

   /* this variable will be used to store the "default" screen of the  */
   /* X server. usually an X server has only one screen, so we're only */
   /* interested in that screen.                                       */
   int screen_num;

   /* these variables will store the size of the screen, in pixels.    */
   int screen_width;
   int screen_height;

   /* this variable will be used to store the ID of the root window of our */
   /* screen. Each screen always has a root window that covers the whole   */
   /* screen, and always exists.                                           */
   Window root_window;

   /* these variables will be used to store the IDs of the black and white */
   /* colors of the given screen. More on this will be explained later.    */
   unsigned long white_pixel;
   unsigned long black_pixel;

   /* check the number of the default screen for our X server. */
   screen_num = DefaultScreen(xdisp);

   /* find the width of the default screen of our X server, in pixels. */
   screen_width = DisplayWidth(xdisp, screen_num);

   /* find the height of the default screen of our X server, in pixels. */
   screen_height = DisplayHeight(xdisp, screen_num);

   /* find the ID of the root window of the screen. */
   root_window = RootWindow(xdisp, screen_num);

   /* find the value of a white pixel on this screen. */
   white_pixel = WhitePixel(xdisp, screen_num);

   /* find the value of a black pixel on this screen. */
   black_pixel = BlackPixel(xdisp, screen_num);

   printf("Conectado al display %s\n", ";0,0");
   printf("Pantalla por defecto %d\n", screen_num);
   printf("Anchura Pantalla por defecto %d\n", screen_width);
   printf("Altura Pantalla por defecto %d\n", screen_height);

   Window xw = XCreateSimpleWindow(xdisp
                                 , DefaultRootWindow(xdisp)
                                 , 400
                                 , 400
                                 , 400
                                 , 400
                                 , 4
                                 , black_pixel 
                                 , white_pixel);
   
   XSelectInput(xdisp, xw, StructureNotifyMask 
      | EnterWindowMask   /* When de window is focused */
      | LeaveWindowMask   /* When we lwave the window */
      | ButtonPressMask   /* Events when a mouse button is pressed */
      | ButtonReleaseMask /* Events when a mouse button is released  */
      | KeyPressMask      /* Events when a keyboard button is pressed */
      | KeyReleaseMask    /* Events when a keyboard button is released  */
      | ExposureMask
      | PointerMotionMask /* Events of the pointer moving in one of the windows controlled 
                             by our application, while no mouse button is held pressed. */
      | ButtonMotionMask  /* Events of the pointer moving while one (or more) of the mouse
                             buttons is held pressed. */
      | Button1MotionMask /* Same as ButtonMotionMask, but only when the 1st mouse button 
                             is held pressed. */
      | Button2MotionMask 
      | Button3MotionMask 
      | Button4MotionMask 
      | Button5MotionMask );

   /* This variable will store the newly created property. */
   XTextProperty window_title_property;
   XTextProperty icon_name_property; 

   /* This is the string to be translated into a property. */
   char* window_title = "My X";
   char* icon_name = "Icon"; 

   /* translate the given string into an X property. */
   int rc = XStringListToTextProperty(&window_title,
				       1,
				       &window_title_property);
   /* check the success of the translation. */
   if (rc == 0) {
      printf("XStringListToTextProperty - out of memory\n");
      exit(1);
   }
   /* assume that window_title_property is our XTextProperty, and is */
   /* defined to contain the desired window title.                   */
   XSetWMName(xdisp, xw, &window_title_property); 
   /* translate the given string into an X property. */
   rc = XStringListToTextProperty(&icon_name,
				    1,
				       &icon_name_property);
   /* check the success of the translation. */
   if (rc == 0) {
      printf( "XStringListToTextProperty - out of memory\n");
   exit(1);
   }
   /* this time we assume that icon_name_property is an initialized */
   /* XTextProperty variable containing the desired icon name.      */
   XSetWMIconName(xdisp, xw, &icon_name_property); 

   XMapWindow(xdisp, xw);
   XSync(xdisp, 0);
 
   /* create a new graphical context for drawing */
   GC gc_draw = XCreateGC(xdisp, xw, 0, NULL);
   if (gc_draw < 0) {
       printf( "Error XCreateGC: \n");
     exit(0);
   }

   /* this structure will store the color data actually allocated for us. */
   XColor system_color_1, system_color_2;
   /* this structure will store the exact RGB values of the named color.  */
   /* it might be different from what was actually allocated.             */
   XColor exact_color;

   /* allocate a "red" color map entry. */
   Status st = XAllocNamedColor(xdisp,
				 screen_num,
				       "red",
				    &system_color_1,
				    &exact_color);
   /* make sure the allocation succeeded. */
   if (st == 0) {
      printf( "XAllocNamedColor - allocation of 'red' color failed.\n");
   } else {
      printf("Color entry for 'red' - allocated as (%d,%d,%d) in RGB values.\n",
	       system_color_1.red, system_color_1.green, system_color_1.blue);
   }
   /* allocate a color with values (30000, 10000, 0) in RGB. */
   system_color_2.red = 30000;
   system_color_2.green = 10000;
   system_color_2.blue = 0;
   st = XAllocColor(xdisp,
			   screen_num,
			   &system_color_2);
   /* make sure the allocation succeeded. */
   if (st == 0) {
      printf( "XAllocColor - allocation of (30000,10000,0) color failed.\n");
   } else {
      /* do something with the allocated color... */
   }

   XSetForeground(xdisp, gc_draw, system_color_1.pixel);

   /* change the fill style of this GC to 'solid'. */
   XSetFillStyle(xdisp, gc_draw, FillSolid);

   /* create a new graphical context for erasing */
   GC gc_erase = XCreateGC(xdisp, xw, 0, NULL);
   if (gc_erase < 0) {
       printf( "Error XCreateGC for erasing: \n");
     exit(0);
   }

   XSetForeground(xdisp, gc_erase, white_pixel);

   drawit(xdisp, xw, gc_draw); 
   XSync(xdisp, 0);
   
   XEvent e;
   do {
      XNextEvent(xdisp, &e);
      switch (e.type) {
	 case Expose:
	    printf("Evento Expose recibido: %d\n", e.xexpose.count);
            if (e.xexpose.count == 0) {
               drawit(xdisp, xw, gc_draw);
            }
         break;
	 case MapNotify:
	    printf("Evento MapNotify recibido.\n");
         break;
         default:
            printf("Evento de tipo: %i recibido.\n", e.type);
      }
   } while (1);
   /*} while (e.type != MapNotify);*/

   XDestroyWindow(xdisp, xw);
   sleep(10);
   XCloseDisplay(xdisp);
}

void drawit(Display *xdisp, Window xw, GC gc) {

   XDrawLine(xdisp, xw, gc, 0, 0, 200, 200);
   XDrawLine(xdisp, xw, gc, 200, 0, 0 , 200);
   XDrawPoint(xdisp, xw, gc, 10, 30);

   int x = 30, y = 40;
   int h = 15, w = 45;
   int angle1 = 0, angle2 = 2.109;
   XDrawArc(xdisp, xw, gc, x-(w/2), y-(h/2), w, h, angle1, angle2);

   /* now use the XDrawArc() function to draw a circle whose diameter */
   /* is 15 pixels, and whose center is at location '50,100'.         */
   XDrawArc( xdisp, xw, gc, 50-(15/2), 100-(15/2), 15, 15, 0, 360*64);

   /* the XDrawLines() function draws a set of consecutive lines, whose     */
   /* edges are given in an array of XPoint structures.                     */
   /* The following block will draw a triangle. We use a block here, since  */
   /* the C language allows defining new variables only in the beginning of */
   /* a block.                                                              */
   {
      /* this array contains the pixels to be used as the line's end-points. */
      XPoint points[] = {
	 {0, 0},
	 {15, 15},
	 {0, 15},
	 {0, 0}
      };
      /* and this is the number of pixels in the array. The number of drawn */
      /* lines will be 'npoints - 1'.                                       */
      int npoints = sizeof(points)/sizeof(XPoint);
      /* draw a small triangle at the top-left corner of the window. */
      /* the triangle is made of a set of consecutive lines, whose   */
      /* end-point pixels are specified in the 'points' array.       */
      XDrawLines(xdisp, xw, gc, points, npoints, CoordModeOrigin);
   }
   /* draw a rectangle whose top-left corner is at '120,150', its width is */
   /* 50 pixels, and height is 60 pixels.                                  */
   XDrawRectangle(xdisp, xw, gc, 120, 150, 50, 60);
   /* draw a filled rectangle of the same size as above, to the left of the  */
   /* previous rectangle. note that this rectangle is one pixel smaller than */
   /* the previous line, since 'XFillRectangle()' assumes it is filling up   */
   /* an already drawn rectangle. This may be used to draw a rectangle using */
   /* one color, and later to fill it using another color.                   */
   XFillRectangle(xdisp, xw, gc, 60, 150, 50, 60);
} 

