#include <X11/Xlib.h> 
#include <assert.h>   
#include <unistd.h>   

/*#define NULL (0)       */

main() {
   Display *xdisp = XOpenDisplay(":0.0");
   assert(xdisp);

   Window xw = XCreateSimpleWindow(xdisp, DefaultRootWindow(xdisp), 100, 100, 
                                    20, 20, 10, CopyFromParent, CopyFromParent);
   
   XMapWindow(xdisp, xw);
   XSelectInput(xdisp, xw, StructureNotifyMask);
   
   GC gc = XCreateGC(xdisp, xw, 0, NULL);
   //XSetForeground(xdisp, gc, whiteColor);

   for(;;) {
      XEvent e;
      XNextEvent(xdisp, &e);
      if (e.type == MapNotify)
	 break;
   }

   XDrawLine(xdisp, xw, gc, 10, 60, 180, 20);

   XFlush(xdisp);
   sleep(3);
   XCloseDisplay(xdisp);
}

