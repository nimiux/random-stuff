#include <X11/Xlib.h> 
#include <X11/keysym.h> 
#include <X11/Xutil.h> 
#include <assert.h>   
#include <unistd.h>   


#include "icon.bmp"   

void setIcon(Display *xdisp, Window xw);

void doX();
void drawit(Display *xdisp, Window xw, GC gc);

void handleButtonPress(Display *xdisp, XEvent e, GC gc_draw, GC gc_erase);
void handleMotionNotify(Display *xdisp, XEvent e, GC gc_draw, GC gc_erase); 
void handleKeyPress(Display *xdisp, XEvent e, GC gc_draw, GC gc_erase); 

main(int argc, char **argv) {
   doX();
}

void setIcon(Display *xdisp, Window xw) {
   /* pointer to the WM hints structure. */
   XWMHints* win_hints;

   /* load the given bitmap data and create an X pixmap containing it. */
   Pixmap icon_pixmap = XCreateBitmapFromData(xdisp,
						xw,
						icon_bitmap_bits,
						icon_bitmap_width,
						icon_bitmap_height);
   if (!icon_pixmap) {
      printf( "XCreateBitmapFromData - error creating pixmap\n");
      exit(1);
   }
   /* allocate a WM hints structure. */
   win_hints = XAllocWMHints();
   if (!win_hints) {
      printf( "XAllocWMHints - out of memory\n");
      exit(1);
   }
   /* initialize the structure appropriately. */
   /* first, specify which size hints we want to fill in. */
   /* in our case - setting the icon's pixmap. */
   win_hints->flags = IconPixmapHint;
   /* next, specify the desired hints data.           */
   /* in our case - supply the icon's desired pixmap. */
   win_hints->icon_pixmap = icon_pixmap;
   /* pass the hints to the window manager. */
   XSetWMHints(xdisp, xw, win_hints);
   /* finally, we can free the WM hints structure. */
   XFree(win_hints);

}

void doX( ) {
   Display *xdisp = XOpenDisplay(NULL);
   assert(xdisp);

   /* this variable will be used to store the "default" screen of the  */
   /* X server. usually an X server has only one screen, so we're only */
   /* interested in that screen.                                       */
   int screen_num;

   /* these variables will store the size of the screen, in pixels.    */
   int screen_width;
   int screen_height;

   /* this variable will be used to store the ID of the root window of our */
   /* screen. Each screen always has a root window that covers the whole   */
   /* screen, and always exists.                                           */
   Window root_window;

   /* these variables will be used to store the IDs of the black and white */
   /* colors of the given screen. More on this will be explained later.    */
   unsigned long white_pixel;
   unsigned long black_pixel;

   /* check the number of the default screen for our X server. */
   screen_num = DefaultScreen(xdisp);

   /* find the width of the default screen of our X server, in pixels. */
   screen_width = DisplayWidth(xdisp, screen_num);

   /* find the height of the default screen of our X server, in pixels. */
   screen_height = DisplayHeight(xdisp, screen_num);

   /* find the ID of the root window of the screen. */
   root_window = RootWindow(xdisp, screen_num);

   /* find the value of a white pixel on this screen. */
   white_pixel = WhitePixel(xdisp, screen_num);

   /* find the value of a black pixel on this screen. */
   black_pixel = BlackPixel(xdisp, screen_num);

   printf("Conectado al display %s\n", ";0,0");
   printf("Pantalla por defecto %d\n", screen_num);
   printf("Anchura Pantalla por defecto %d\n", screen_width);
   printf("Altura Pantalla por defecto %d\n", screen_height);

   Window xw = XCreateSimpleWindow(xdisp
                                 , DefaultRootWindow(xdisp)
                                 , 400
                                 , 400
                                 , 400
                                 , 400
                                 , 4
                                 , black_pixel 
                                 , white_pixel);
   
   XSelectInput(xdisp, xw, StructureNotifyMask 
      | EnterWindowMask   /* When de window is focused */
      | LeaveWindowMask   /* When we lwave the window */
      | ButtonPressMask   /* Events when a mouse button is pressed */
      | ButtonReleaseMask /* Events when a mouse button is released  */
      | KeyPressMask      /* Events when a keyboard button is pressed */
      | KeyReleaseMask    /* Events when a keyboard button is released  */
      | ExposureMask
      | PointerMotionMask /* Events of the pointer moving in one of the windows controlled 
                             by our application, while no mouse button is held pressed. */
      | ButtonMotionMask  /* Events of the pointer moving while one (or more) of the mouse
                             buttons is held pressed. */
      | Button1MotionMask /* Same as ButtonMotionMask, but only when the 1st mouse button 
                             is held pressed. */
      | Button2MotionMask 
      | Button3MotionMask 
      | Button4MotionMask 
      | Button5MotionMask );

   /* This variable will store the newly created property. */
   XTextProperty window_title_property;
   XTextProperty icon_name_property; 

   /* This is the string to be translated into a property. */
   char* window_title = "My X";
   char* icon_name = "Icon"; 

   /* translate the given string into an X property. */
   int rc = XStringListToTextProperty(&window_title,
				       1,
				       &window_title_property);
   /* check the success of the translation. */
   if (rc == 0) {
      printf("XStringListToTextProperty - out of memory\n");
      exit(1);
   }
   /* assume that window_title_property is our XTextProperty, and is */
   /* defined to contain the desired window title.                   */
   XSetWMName(xdisp, xw, &window_title_property); 
   /* translate the given string into an X property. */
   rc = XStringListToTextProperty(&icon_name,
				    1,
				       &icon_name_property);
   /* check the success of the translation. */
   if (rc == 0) {
      printf( "XStringListToTextProperty - out of memory\n");
   exit(1);
   }
   /* this time we assume that icon_name_property is an initialized */
   /* XTextProperty variable containing the desired icon name.      */
   XSetWMIconName(xdisp, xw, &icon_name_property); 

   setIcon(xdisp, xw);

   XMapWindow(xdisp, xw);
   XSync(xdisp, 0);
 
   /* create a new graphical context for drawing */
   GC gc_draw = XCreateGC(xdisp, xw, 0, NULL);
   if (gc_draw < 0) {
       printf( "Error XCreateGC: \n");
     exit(0);
   }

   XSetForeground(xdisp, gc_draw, black_pixel);
   /* change the fill style of this GC to 'solid'. */
   XSetFillStyle(xdisp, gc_draw, FillSolid);

   /* create a new graphical context for erasing */
   GC gc_erase = XCreateGC(xdisp, xw, 0, NULL);
   if (gc_erase < 0) {
       printf( "Error XCreateGC for erasing: \n");
     exit(0);
   }

   XSetForeground(xdisp, gc_erase, white_pixel);

   drawit(xdisp, xw, gc_draw); 
   XSync(xdisp, 0);

   
   XEvent e;
   do {
      XNextEvent(xdisp, &e);
      switch (e.type) {
	 case ButtonPress:
	    printf("Evento ButtonPress recibido.\n");
            handleButtonPress(xdisp, e, gc_draw, gc_erase);
         break;
         case KeyPress:
	    printf("Evento KeyPress recibido.\n");
            handleKeyPress(xdisp, e, gc_draw, gc_erase);
         break;
	 case MotionNotify:
	    printf("Evento MotionNotify recibido.\n");
            handleMotionNotify(xdisp, e, gc_draw, gc_erase);
         break;
	 case EnterNotify:
	    printf("Evento EnterNotify recibido.\n");
         break;
	 case LeaveNotify:
	    printf("Evento LeaveNotify recibido.\n");
         break;
	 case Expose:
	    printf("Evento Expose recibido: %d\n", e.xexpose.count);
            if (e.xexpose.count == 0) {
               drawit(xdisp, xw, gc_draw);
            }
         break;
	 case MapNotify:
	    printf("Evento MapNotify recibido.\n");
         break;
         default:
            printf("Evento de tipo: %i recibido.\n", e.type);
      }
   } while (1);
   /*} while (e.type != MapNotify);*/

   XDestroyWindow(xdisp, xw);
   sleep(10);
   XCloseDisplay(xdisp);
}

void drawit(Display *xdisp, Window xw, GC gc) {

   XDrawLine(xdisp, xw, gc, 0, 0, 200, 200);
   XDrawLine(xdisp, xw, gc, 200, 0, 0 , 200);
   XDrawPoint(xdisp, xw, gc, 10, 30);

   int x = 30, y = 40;
   int h = 15, w = 45;
   int angle1 = 0, angle2 = 2.109;
   XDrawArc(xdisp, xw, gc, x-(w/2), y-(h/2), w, h, angle1, angle2);

   /* now use the XDrawArc() function to draw a circle whose diameter */
   /* is 15 pixels, and whose center is at location '50,100'.         */
   XDrawArc( xdisp, xw, gc, 50-(15/2), 100-(15/2), 15, 15, 0, 360*64);

   /* the XDrawLines() function draws a set of consecutive lines, whose     */
   /* edges are given in an array of XPoint structures.                     */
   /* The following block will draw a triangle. We use a block here, since  */
   /* the C language allows defining new variables only in the beginning of */
   /* a block.                                                              */
   {
      /* this array contains the pixels to be used as the line's end-points. */
      XPoint points[] = {
	 {0, 0},
	 {15, 15},
	 {0, 15},
	 {0, 0}
      };
      /* and this is the number of pixels in the array. The number of drawn */
      /* lines will be 'npoints - 1'.                                       */
      int npoints = sizeof(points)/sizeof(XPoint);
      /* draw a small triangle at the top-left corner of the window. */
      /* the triangle is made of a set of consecutive lines, whose   */
      /* end-point pixels are specified in the 'points' array.       */
      XDrawLines(xdisp, xw, gc, points, npoints, CoordModeOrigin);
   }
   /* draw a rectangle whose top-left corner is at '120,150', its width is */
   /* 50 pixels, and height is 60 pixels.                                  */
   XDrawRectangle(xdisp, xw, gc, 120, 150, 50, 60);
   /* draw a filled rectangle of the same size as above, to the left of the  */
   /* previous rectangle. note that this rectangle is one pixel smaller than */
   /* the previous line, since 'XFillRectangle()' assumes it is filling up   */
   /* an already drawn rectangle. This may be used to draw a rectangle using */
   /* one color, and later to fill it using another color.                   */
   XFillRectangle(xdisp, xw, gc, 60, 150, 50, 60);
} 

void handleButtonPress(Display *xdisp, XEvent e, GC gc_draw, GC gc_erase) {

   /* store the mouse button coordinates in 'int' variables. */
   /* also store the ID of the window on which the mouse was */
   /* pressed.                                               */
   int x = e.xbutton.x;
   int y = e.xbutton.y;
   int the_win = e.xbutton.window;
   /* check which mouse button was pressed, and act accordingly. */
   switch (e.xbutton.button) {
      case Button1:
         /* draw a pixel at the mouse position. */
         XDrawPoint(xdisp, the_win, gc_draw, x, y);
      break;
      case Button2:
         /* erase a pixel at the mouse position. */
         XDrawPoint(xdisp, the_win, gc_erase, x, y);
      break;
      default: /* probably 3rd button - just ignore this event. */
         printf("3rd Button......\n");
   }
}

void handleMotionNotify(Display *xdisp, XEvent e, GC gc_draw, GC gc_erase) {
   /* store the mouse button coordinates in 'int' variables. */
   /* also store the ID of the window on which the mouse was */
   /* pressed.                                               */
   int x = e.xmotion.x;
   int y = e.xmotion.y;
   Window the_win = e.xbutton.window;
   /* if the 1st mouse button was held during this event, draw a pixel */
   /* at the mouse pointer location.                                   */
   if (e.xmotion.state & Button1Mask) {
      /* draw a pixel at the mouse position. */
      XDrawPoint(xdisp, the_win, gc_draw, x, y);
   } else {
      if (e.xmotion.state & Button2Mask) {
         /* erasea pixel at the mouse position. */
         XDrawPoint(xdisp, the_win, gc_erase, x, y);
      }
   }
}

void handleKeyPress(Display *xdisp, XEvent e, GC gc_draw, GC gc_erase) {

   /* store the mouse button coordinates in 'int' variables. */
   /* also store the ID of the window on which the mouse was */
   /* pressed.                                               */
   int x = e.xkey.x;
   int y = e.xkey.y;
   Window the_win = e.xkey.window;

   /* translate the key code to a key symbol. */
   KeySym key_symbol = XKeycodeToKeysym(xdisp, e.xkey.keycode, 0);
   switch (key_symbol) {
      case XK_1:
      case XK_KP_1: /* '1' key was pressed, either the normal '1', or */
	 XDrawPoint(xdisp, the_win, gc_draw, x, y);
      break;
      case XK_Delete: /* DEL key was pressed, erase the current pixel. */
	 XDrawPoint(xdisp, the_win, gc_erase, x, y);
      break;
      default:  /* anything else - check if it is a letter key */
	 if (key_symbol >= XK_A && key_symbol <= XK_Z) {
	    int ascii_key = key_symbol - XK_A + 'A';
	    printf("Key pressed - '%c'\n", ascii_key);
	 }
	 if (key_symbol >= XK_a && key_symbol <= XK_z) {
	    int ascii_key = key_symbol - XK_a + 'a';
	    printf("Key pressed - '%c'\n", ascii_key);
	 }
      }
}
