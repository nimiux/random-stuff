/* xsleep - trivial program to sleep and exit, for X11
**
** Copyright (C) 1992 by Jef Poskanzer <jef@acme.com>.  All rights reserved.
**
** Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions
** are met:
** 1. Redistributions of source code must retain the above copyright
**    notice, this list of conditions and the following disclaimer.
** 2. Redistributions in binary form must reproduce the above copyright
**    notice, this list of conditions and the following disclaimer in the
**    documentation and/or other materials provided with the distribution.
** 
** THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
** ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
** FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
** OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
** HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
** OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
** SUCH DAMAGE.
*/

#include <stdio.h>
#include <X11/Xlib.h>

void
main( argc, argv )
    int argc;
    char* argv[];
    {
    int argn, seconds;
    char *display_name;
    Display *display;
    char* usage = "usage:  %s [-display display] seconds\n";

    argn = 1;
    display_name = (char*) 0;

    while ( argn < argc && argv[argn][0] == '-' )
	{
	if ( strcmp( argv[argn], "-display" ) == 0 ||
	     strcmp( argv[argn], "-displa" ) == 0 ||
	     strcmp( argv[argn], "-displ" ) == 0 ||
	     strcmp( argv[argn], "-disp" ) == 0 ||
	     strcmp( argv[argn], "-dis" ) == 0 ||
	     strcmp( argv[argn], "-di" ) == 0 ||
	     strcmp( argv[argn], "-d" ) == 0 )
	    {
	    ++argn;
	    if ( argn >= argc )
		{
		(void) fprintf( stderr, usage, argv[0] );
		exit( 1 );
		}
	    display_name = argv[argn];
	    }
	}

    if ( argn >= argc )
	{
	(void) fprintf( stderr, usage, argv[0] );
	exit( 1 );
	}
    seconds = atoi( argv[argn] );
    if ( seconds <= 0 )
	{
	(void) fprintf( stderr, usage, argv[0] );
	exit( 1 );
	}
    ++argn;

    if ( argn != argc )
	{
	(void) fprintf( stderr, usage, argv[0] );
	exit( 1 );
	}

    display = XOpenDisplay( display_name );
    if ( display == (Display*) 0 )
	{
        fprintf(
	    stderr, "%s: can't open display \"%s\"\n",
	    argv[0], XDisplayName( display_name ) );
        exit(1);
	}
    
    sleep( seconds );

    XCloseDisplay( display );
    exit( 0 );
    }
