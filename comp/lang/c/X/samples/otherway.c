#include <X11/Xlib.h>
#include <X11/Xutil.h>

int main(int argc, char **argv){
   Display* display;
   int screen_number;
   Window window;
   XSetWindowAttributes attributes;
   unsigned long attributemask;
   Visual *visual;
   display = XOpenDisplay("");
   screen_number = DefaultScreen(display);
   visual= CopyFromParent;
   attributes.background_pixel=WhitePixel(display, screen_number);
   attributes.border_pixel= BlackPixel(display, screen_number);
   attributemask = CWBackPixel|CWBorderPixel;
   window = XCreateWindow(display, RootWindow(display, screen_number),
				    200,200,200,200,0,
   CopyFromParent,InputOutput,visual,attributemask,&attributes);
   XMapRaised(display, window);
   XFlush(display);
   while(1) {
   }
}
