/*Program to draw lines and rectangles--draw.c
*On Linux compile as gcc draw.c -L /usr/X11R6/lib -lX11
*/


#include<X11/Xlib.h>
#include<X11/Xutil.h>
#include<stdio.h>
#define BORDER_WIDTH 2


/* Program wide globals */
Display *theDisplay;
int theScreen;
int theDepth;
unsigned long theBlackPixel;
unsigned long theWhitePixel;


void initX(void){
}


void drawLine(Window theWindow,GC theGC,int x1,int y1,int x2,int y2){
           XDrawLine(theDisplay,theWindow,theGC,x1,y1,x2,y2);


}


void drawRectangle(Window theWindow,GC theGC,int x,int y,int width,int height){
           XDrawRectangle(theDisplay,theWindow,theGC,x,y,width,height);
}


int createGC(Window theNewWindow,GC *theNewGC){
           XGCValues theGCValues;


           *theNewGC = XCreateGC(theDisplay,theNewWindow,(unsigned long) 0,&theGCValues);
           if(*theNewGC == 0)
                      return 0;
              else{
                 XSetForeground(theDisplay,*theNewGC,theWhitePixel);
                 XSetBackground(theDisplay,*theNewGC,theBlackPixel);
                 return 1;
         }
}


Window OpenWindow(int x, int y, int width, int height, int flag,GC *theNewGC){
           XSetWindowAttributes theWindowAttributes;
           unsigned long theWindowMask;
           XSizeHints theSizeHints;
           XWMHints theWMHints;
           Window theNewWindow;


           /*Setting the attributes*/
           theWindowAttributes.border_pixel
                   =BlackPixel(theDisplay,theScreen);
           theWindowAttributes.background_pixel
                   = WhitePixel(theDisplay,theScreen);
           theWindowAttributes.override_redirect = False;



           theWindowMask = CWBackPixel|CWBorderPixel|CWOverrideRedirect;


           theNewWindow = XCreateWindow( theDisplay,
                           RootWindow(theDisplay,theScreen),
                           x,y,width,height,
                           BORDER_WIDTH,theDepth,
                           InputOutput,
                           CopyFromParent,
                           theWindowMask,
                           &theWindowAttributes);


           theWMHints.initial_state = NormalState;
           theWMHints.flags = StateHint;


           XSetWMHints(theDisplay,theNewWindow,&theWMHints);

           theSizeHints.flags = PPosition | PSize;
           theSizeHints.x = x;
           theSizeHints.y = y;
           theSizeHints.width = width;
           theSizeHints.height = height;


           XSetNormalHints(theDisplay,theNewWindow,&theSizeHints);


           if( createGC(theNewWindow,theNewGC) == 0){
                      XDestroyWindow(theDisplay,theNewWindow);
                      return( (Window) 0);
                      }


              XMapWindow(theDisplay,theNewWindow);
              XFlush(theDisplay);


              return theNewWindow;
}


void main(void){
           Window theWindow;
           GC theGC;

           theDisplay = XOpenDisplay(NULL);

           theScreen = DefaultScreen(theDisplay);
           theDepth = DefaultDepth(theDisplay,theScreen);
           theBlackPixel = WhitePixel(theDisplay,theScreen);
           theWhitePixel = BlackPixel(theDisplay,theScreen);

           theWindow = OpenWindow(100,100,300,300,0,&theGC);
           drawLine(theWindow,theGC,10,10,100,100);
      drawRectangle(theWindow,theGC,100,100,100,100);
              XFlush(theDisplay);

              sleep(10);
              XDestroyWindow(theDisplay,theWindow);
}



