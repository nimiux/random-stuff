#ifndef WIN_H
#define WIN_H 1

#include <X11/Xlib.h>
#include <X11/Xutil.h>

#define WINDOW_WIDTH 400
#define WINDOW_HEIGHT 300

class Win
{
	public:
		int  create();
		void process_event(XEvent report);
		void eventloop();
		void close();

		unsigned int width, height;
		Display *display;
		int screen;
		int depth;
		Window win;
};

#endif

