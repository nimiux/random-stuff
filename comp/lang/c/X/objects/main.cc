#include <stdio.h>
#include "main.h"

int main(int argc, char *argv[])
{
	Main m(argc, argv);
	return 0;
}

Main::Main(int argc, char *argv[])
{
	quit = 0;

	win.create();

	while(!quit)
	{
		win.eventloop();
	}

	win.close();
}

