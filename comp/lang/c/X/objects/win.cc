#include <X11/keysym.h>
#include <stdio.h>
#include "win.h"

int Win::create()
{
	char *window_name = "Simple Window";
	char *icon_name = "window";
	static XSizeHints size_hints;
	Window rootwin;

	width=WINDOW_WIDTH;
	height=WINDOW_HEIGHT;
	display = XOpenDisplay(NULL);

	if (display == NULL)
	{
		fprintf(stderr, "Failed to open display\n");
		return 0;
	}

	screen = DefaultScreen(display);
	depth = DefaultDepth(display, screen);
	rootwin = RootWindow(display, screen);
	win = XCreateSimpleWindow(display, rootwin, 10, 10, width, height, 5,
		BlackPixel(display, screen), BlackPixel(display, screen));

	size_hints.flags = PSize | PMinSize | PMaxSize;
	size_hints.min_width = width;
	size_hints.max_width = width;
	size_hints.min_height = height;
	size_hints.max_height = height;

	XSetStandardProperties(display, win, window_name, icon_name, None,
		0, 0, &size_hints); 

	XSelectInput(display, win, ButtonPressMask | KeyPressMask);
	XMapWindow(display, win);
}

void Win::close()
{
	XCloseDisplay(display);
}

void Win::eventloop()
{
  XEvent xev;
  int i, num_events;

	XFlush(display);
	num_events = XPending(display);
	while((num_events != 0))
	{
		num_events--;
		XNextEvent(display, &xev);
		process_event(xev);
	}
}

void Win::process_event(XEvent report)
{
	KeySym key;

	switch(report.type)
	{
		case KeyPress:
			key = XLookupKeysym(&report.xkey, 0);
			switch(key)
			{
				case(XK_KP_4):
				case(XK_Left):
				case(XK_KP_Left):
					printf("Left\n");
					break;
				case(XK_KP_6):
				case(XK_Right):
				case(XK_KP_Right):
					printf("Right\n");
					break;
				case(XK_KP_8):
				case(XK_Up):
				case(XK_KP_Up):
					printf("Up\n");
					break;
				case(XK_KP_2):
				case(XK_Down):
				case(XK_KP_Down):
					printf("Down\n");
					break;
				default: break;
			}
			break;

		case ButtonPressMask:
			printf("You pressed button %d\n", report.xbutton.button);
			break;

		default:
			break;
	}
}

