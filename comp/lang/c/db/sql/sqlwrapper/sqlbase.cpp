
/* Result Sets Interface */
#ifndef SQL_CRSR
#  define SQL_CRSR
  struct sql_cursor
  {
    unsigned int curocn;
    void *ptr1;
    void *ptr2;
    unsigned long magic;
  };
  typedef struct sql_cursor sql_cursor;
  typedef struct sql_cursor SQL_CURSOR;
#endif /* SQL_CRSR */

/* Thread Safety */
typedef void * sql_context;
typedef void * SQL_CONTEXT;

/* Object support */
struct sqltvn
{
  unsigned char *tvnvsn; 
  unsigned short tvnvsnl; 
  unsigned char *tvnnm;
  unsigned short tvnnml; 
  unsigned char *tvnsnm;
  unsigned short tvnsnml;
};
typedef struct sqltvn sqltvn;

struct sqladts
{
  unsigned int adtvsn; 
  unsigned short adtmode; 
  unsigned short adtnum;  
  sqltvn adttvn[1];       
};
typedef struct sqladts sqladts;

static struct sqladts sqladt = {
  1,1,0,
};

/* Binding to PL/SQL Records */
struct sqltdss
{
  unsigned int tdsvsn; 
  unsigned short tdsnum; 
  unsigned char *tdsval[1]; 
};
typedef struct sqltdss sqltdss;
static struct sqltdss sqltds =
{
  1,
  0,
};

/* File name & Package Name */
struct sqlcxp
{
  unsigned short fillen;
           char  filnam[11];
};
static const struct sqlcxp sqlfpn =
{
    10,
    "sqlbase.pc"
};


static unsigned long sqlctx = 81555;


static struct sqlexd {
   unsigned int   sqlvsn;
   unsigned int   arrsiz;
   unsigned int   iters;
   unsigned int   offset;
   unsigned short selerr;
   unsigned short sqlety;
   unsigned int   occurs;
      const short *cud;
   unsigned char  *sqlest;
      const char  *stmt;
   sqladts *sqladtp;
   sqltdss *sqltdsp;
            void  **sqphsv;
   unsigned int   *sqphsl;
            int   *sqphss;
            void  **sqpind;
            int   *sqpins;
   unsigned int   *sqparm;
   unsigned int   **sqparc;
   unsigned short  *sqpadto;
   unsigned short  *sqptdso;
            void  *sqhstv[4];
   unsigned int   sqhstl[4];
            int   sqhsts[4];
            void  *sqindv[4];
            int   sqinds[4];
   unsigned int   sqharm[4];
   unsigned int   *sqharc[4];
   unsigned short  sqadto[4];
   unsigned short  sqtdso[4];
} sqlstm = {10,4};

// Prototypes
extern "C" {
  void sqlcxt (void **, unsigned long *,
               struct sqlexd *, const struct sqlcxp *);
  void sqlcx2t(void **, unsigned long *,
               struct sqlexd *, const struct sqlcxp *);
  void sqlbuft(void **, char *);
  void sqlgs2t(void **, char *);
  void sqlorat(void **, unsigned long *, void *);
}

// Forms Interface
static const int IAPSUCC = 0;
static const int IAPFAIL = 1403;
static const int IAPFTL  = 535;
extern "C" { void sqliem(char *, int *); }

typedef struct { unsigned short len; unsigned char arr[1]; } VARCHAR;
typedef struct { unsigned short len; unsigned char arr[1]; } varchar;

/* cud (compilation unit data) array */
static const short sqlcud0[] =
{10,4130,0,0,0,
5,0,0,1,0,0,539,183,0,0,4,4,0,1,0,1,5,0,0,1,5,0,0,1,10,0,0,1,10,0,0,
36,0,0,2,0,0,539,189,0,0,4,4,0,1,0,1,5,0,0,1,5,0,0,1,5,0,0,1,10,0,0,
67,0,0,3,25,0,513,246,0,0,0,0,0,1,0,
82,0,0,4,0,0,541,271,0,0,0,0,0,1,0,
97,0,0,5,0,0,543,296,0,0,0,0,0,1,0,
112,0,0,6,0,0,544,322,0,0,0,0,0,1,0,
};


#line 1 "sqlbase.pc"
/*****************************************************************************
 * ++
 * Proyecto     : 
 *
 * Autor        : Jose Maria Alonso. 29/07/2002.
 *
 * Modulo       : sqlbase.pc
 *
 * Descripcion  : 
 *
 * Historal     :
 *
 * Autor                Fecha           Descripcion
 * ----------------     --------------  ------------------------------------
 * --
 ******************************************************************************/
#ifndef _SQLBASE_PC_
#define _SQLBASE_PC_

/******************************************************************************
 * Ajuste a los Estandares                                                    * 
 ******************************************************************************/

/******************************************************************************
 * Includes de Sistema                                                        *
 ******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

/******************************************************************************
 * Includes de Aplicacion                                                     *
 ******************************************************************************/
#include "sqlca.h"
#include "sqlcpr.h"
/* EXEC SQL INCLUDE sqlbase.h;
 */ 
#line 1 "/home/chema/cprojects/sqlwrapper/include/sqlbase.h"
/******************************************************************************
 * ++
 * Proyecto     : 
 *
 * Autor        : Jose Maria Alonso. 
 *
 * Modulo       : sqlbase.h
 *
 * Descripcion  : 
 *
 * Historal :
 *
 * Autor                Fecha           Descripcion
 * ----------------     --------------  ------------------------------------
 *
 * --
 ******************************************************************************/
#ifndef _SQLBASE_H_
#define _SQLBASE_H_

/******************************************************************************
 * Ajuste a los Estandares                                                    *
 ******************************************************************************/

/******************************************************************************
 * Includes de Sistema                                                        *
 ******************************************************************************/

/******************************************************************************
 * Includes de Aplicacion                                                     *
 ******************************************************************************/

/******************************************************************************
 * Opciones de Debug y Traza                                                  *
 ******************************************************************************/

/******************************************************************************
 * Macros                                                                     *
 ******************************************************************************/

#define ROWID_LEN                        18

#define MAX_HOST_ARRAY                  100

#define GetSQLStatus()  sqlca.sqlcode

#define ORA_SUCCESS              0
#define ORA_ERROR                1
#define ORA_LOST                 2
#define ORA_NOT_FOUND         1403
#define ORA_NOT_CONNECTED    -1012

#define USERNAME_LEN 20
#define PASSWORD_LEN 20
#define DATABASE_LEN 50

/******************************************************************************
 * Tipos                                                                      *
 ******************************************************************************/

/* Tipos necesarios para acceso a base de datos */

/* EXEC SQL BEGIN DECLARE SECTION; */ 
#line 63 "/home/chema/cprojects/sqlwrapper/include/sqlbase.h"

   struct TConnect{
      char szUsername[USERNAME_LEN];
      char szPassword[PASSWORD_LEN];
      char szDatabase[DATABASE_LEN];
   };

   typedef struct TConnect TConnect;

   struct st_rowid {
   	char szRowId         [ROWID_LEN + 1 ];
   };

   typedef struct st_rowid ST_RowId;

/* EXEC SQL END DECLARE SECTION; */ 
#line 78 "/home/chema/cprojects/sqlwrapper/include/sqlbase.h"


/******************************************************************************
 * Variables Globales                                                         *
 ******************************************************************************/

/******************************************************************************
 * Variables y funciones externas.                                            *
 ******************************************************************************/

/******************************************************************************
 * Prototipos de Funciones Publicas                                           *
 ******************************************************************************/
void sqlErrorHandler( char * pszFile, int wLineNumber );
void sqlWarningHandler( char * pszFile, int wLineNumber );
int connectDB();
int beginTrans(void);
int beginReadOnlyTrans(void);
int commitWork(void);
int rollbackWork(void);
int disconnectDB(void);
/******************************************************************************
 * Prototipos de Funciones Privadas                                           *
 ******************************************************************************/

#endif /* _SQLBASE_H_ */

#line 39 "sqlbase.pc"
#include "TTypes.h"

/******************************************************************************
 * Opciones de Debug y Traza                                                  *
 ******************************************************************************/

#ifdef TRACE_SQL_BASE
 #define TRACE(x) x
#else
 #define TRACE(x)
#endif

/******************************************************************************
 * Macros                                                                     *
 ******************************************************************************/

/******************************************************************************
 * Tipos                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Variables Globales                                                         *
 ******************************************************************************/

/* EXEC SQL BEGIN DECLARE SECTION; */ 
#line 63 "sqlbase.pc"

   TConnect *gpxConnect;
/* EXEC SQL END DECLARE SECTION; */ 
#line 65 "sqlbase.pc"


/******************************************************************************
 * Funciones Publicas                                                         *
 ******************************************************************************/

/*****************************************************************************
 * ++
 * Funcion      : sqlErrorHandler
 *
 * Descripcion  : Avisa de una condicion de error devuelto desde la base de
 *                datos.
 *
 * Parametros   : pszFile     : Nombre del fichero donde se ha producido el 
 *                              error.
 *                wLineNumber : Numero de linea donde se ha producido el error.
 * Retorno      : 
 *                
 * Historial :
 *
 * Autor                Fecha           Descripcion
 * ---------------------------------------------------------------------------
 *
 * --
 ****************************************************************************/
void sqlErrorHandler( char * pszFile, int wLineNumber ){
  char szErrorMessage [128];
  size_t wBufLen=255, wMsgLen;

  // Precondicion 
  assert( pszFile != NULL );

  wBufLen = sizeof( szErrorMessage );
  sqlglm( szErrorMessage, &wBufLen, &wMsgLen );
  szErrorMessage[wMsgLen]='\0';
  // reportLog ( pszFile, wLineNumber, "ERROR SQL: %s", szErrorMessage );
  printf ( "ERROR SQL: %s. EN %s, %d\n"
                          , szErrorMessage, pszFile, wLineNumber );
  if ( GetSQLStatus () != ORA_NOT_CONNECTED ) { 
     rollbackWork(); 
     disconnectDB();
   } 
  exit(RET_ERROR);
}

/*****************************************************************************
 * ++
 * Funcion      : sqlWarningHandler
 * 
 * Descripcion  : Avisa de una condicion de advertencia devuelta desde la base 
 *                de datos.
 *
 * Parametros   : pszFile     : Nombre del fichero donde se ha producido la
 *                              advertencia.
 *                wLineNumber : Numero de linea donde se ha producido la
 *                              advertencia.
 *
 * Retorno      : 
 *                
 * Historial :
 *
 * Autor                Fecha           Descripcion
 * ---------------------------------------------------------------------------
 *
 * --
 ****************************************************************************/
void sqlWarningHandler( char * pszFile, int wLineNumber ){
  char szErrorMessage [128];
  size_t wBufLen=255, wMsgLen;
  int wSqlCode;

  // Precondicion 
  assert( pszFile != NULL && wLineNumber >= 0 );

  wBufLen = sizeof( szErrorMessage );
  sqlglm( szErrorMessage, &wBufLen, &wMsgLen );
  szErrorMessage[wMsgLen]='\0';
  
  // reportLog ( __FILE__, __LINE__, "Advertencia SQL: %s", szErrorMessage );
  
} 

/*****************************************************************************
 * ++
 * Funcion      : connectDB
 *
 * Descripcion  : Abre una conexion con la base de datos. 
 *
 * Parametros   : 
 *             
 * Retorno      : Resultado de la creaci�n de la conexi�n.
 *                
 * Historial :
 *
 * Autor                Fecha           Descripcion
 * ---------------------------------------------------------------------------
 *
 * --
 ****************************************************************************/
int connectDB () {

  int wSQLStatus;

  TRACE ( fprintf( stdout, "sqlbase:connectDB\n" ); )

  assert ( gpxConnect );

  /* EXEC SQL WHENEVER SQLERROR DO sqlErrorHandler( __FILE__, __LINE__ ); */ 
#line 172 "sqlbase.pc"

  /* EXEC SQL WHENEVER SQLWARNING DO sqlWarningHandler( __FILE__, __LINE__ ); */ 
#line 173 "sqlbase.pc"

  
  TRACE ( fprintf( stdout, "Realizando Conexion. Usuario: %s. " \
                           "Base de datos: %s\n"
                         , gpxConnect->szUsername 
                         , gpxConnect->szDatabase); )

  /* Si la base de datos no fue especificada */
  if ( strlen( gpxConnect->szDatabase ) == 0 ){
    /* El valor por defecto de ORACLE_SID es usado */
    /* EXEC SQL CONNECT :gpxConnect->szUsername 
		 IDENTIFIED BY :gpxConnect->szPassword; */ 
#line 184 "sqlbase.pc"

{
#line 183 "sqlbase.pc"
    struct sqlexd sqlstm;
#line 183 "sqlbase.pc"
    sqlstm.sqlvsn = 10;
#line 183 "sqlbase.pc"
    sqlstm.arrsiz = 4;
#line 183 "sqlbase.pc"
    sqlstm.sqladtp = &sqladt;
#line 183 "sqlbase.pc"
    sqlstm.sqltdsp = &sqltds;
#line 183 "sqlbase.pc"
    sqlstm.iters = (unsigned int  )10;
#line 183 "sqlbase.pc"
    sqlstm.offset = (unsigned int  )5;
#line 183 "sqlbase.pc"
    sqlstm.cud = sqlcud0;
#line 183 "sqlbase.pc"
    sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 183 "sqlbase.pc"
    sqlstm.sqlety = (unsigned short)256;
#line 183 "sqlbase.pc"
    sqlstm.occurs = (unsigned int  )0;
#line 183 "sqlbase.pc"
    sqlstm.sqhstv[0] = (         void  *)(gpxConnect->szUsername);
#line 183 "sqlbase.pc"
    sqlstm.sqhstl[0] = (unsigned int  )20;
#line 183 "sqlbase.pc"
    sqlstm.sqhsts[0] = (         int  )20;
#line 183 "sqlbase.pc"
    sqlstm.sqindv[0] = (         void  *)0;
#line 183 "sqlbase.pc"
    sqlstm.sqinds[0] = (         int  )0;
#line 183 "sqlbase.pc"
    sqlstm.sqharm[0] = (unsigned int  )0;
#line 183 "sqlbase.pc"
    sqlstm.sqadto[0] = (unsigned short )0;
#line 183 "sqlbase.pc"
    sqlstm.sqtdso[0] = (unsigned short )0;
#line 183 "sqlbase.pc"
    sqlstm.sqhstv[1] = (         void  *)(gpxConnect->szPassword);
#line 183 "sqlbase.pc"
    sqlstm.sqhstl[1] = (unsigned int  )20;
#line 183 "sqlbase.pc"
    sqlstm.sqhsts[1] = (         int  )20;
#line 183 "sqlbase.pc"
    sqlstm.sqindv[1] = (         void  *)0;
#line 183 "sqlbase.pc"
    sqlstm.sqinds[1] = (         int  )0;
#line 183 "sqlbase.pc"
    sqlstm.sqharm[1] = (unsigned int  )0;
#line 183 "sqlbase.pc"
    sqlstm.sqadto[1] = (unsigned short )0;
#line 183 "sqlbase.pc"
    sqlstm.sqtdso[1] = (unsigned short )0;
#line 183 "sqlbase.pc"
    sqlstm.sqphsv = sqlstm.sqhstv;
#line 183 "sqlbase.pc"
    sqlstm.sqphsl = sqlstm.sqhstl;
#line 183 "sqlbase.pc"
    sqlstm.sqphss = sqlstm.sqhsts;
#line 183 "sqlbase.pc"
    sqlstm.sqpind = sqlstm.sqindv;
#line 183 "sqlbase.pc"
    sqlstm.sqpins = sqlstm.sqinds;
#line 183 "sqlbase.pc"
    sqlstm.sqparm = sqlstm.sqharm;
#line 183 "sqlbase.pc"
    sqlstm.sqparc = sqlstm.sqharc;
#line 183 "sqlbase.pc"
    sqlstm.sqpadto = sqlstm.sqadto;
#line 183 "sqlbase.pc"
    sqlstm.sqptdso = sqlstm.sqtdso;
#line 183 "sqlbase.pc"
    sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 183 "sqlbase.pc"
    if (sqlca.sqlcode < 0) sqlErrorHandler(__FILE__,__LINE__);
#line 183 "sqlbase.pc"
    if (sqlca.sqlwarn[0] == 'W') sqlWarningHandler(__FILE__,__LINE__);
#line 183 "sqlbase.pc"
}

#line 184 "sqlbase.pc"

    TRACE ( fprintf( stdout, "sqlbase.pc:ConnectDB(): Connectado : %s\n"
		       , gpxConnect->szUsername ); )
  }else{
    /* Uso del TNS de base de datos como argumento */
    /* EXEC SQL
      CONNECT
        :gpxConnect->szUsername IDENTIFIED BY :gpxConnect->szPassword 
        USING :gpxConnect->szDatabase; */ 
#line 192 "sqlbase.pc"

{
#line 189 "sqlbase.pc"
    struct sqlexd sqlstm;
#line 189 "sqlbase.pc"
    sqlstm.sqlvsn = 10;
#line 189 "sqlbase.pc"
    sqlstm.arrsiz = 4;
#line 189 "sqlbase.pc"
    sqlstm.sqladtp = &sqladt;
#line 189 "sqlbase.pc"
    sqlstm.sqltdsp = &sqltds;
#line 189 "sqlbase.pc"
    sqlstm.iters = (unsigned int  )10;
#line 189 "sqlbase.pc"
    sqlstm.offset = (unsigned int  )36;
#line 189 "sqlbase.pc"
    sqlstm.cud = sqlcud0;
#line 189 "sqlbase.pc"
    sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 189 "sqlbase.pc"
    sqlstm.sqlety = (unsigned short)256;
#line 189 "sqlbase.pc"
    sqlstm.occurs = (unsigned int  )0;
#line 189 "sqlbase.pc"
    sqlstm.sqhstv[0] = (         void  *)(gpxConnect->szUsername);
#line 189 "sqlbase.pc"
    sqlstm.sqhstl[0] = (unsigned int  )20;
#line 189 "sqlbase.pc"
    sqlstm.sqhsts[0] = (         int  )20;
#line 189 "sqlbase.pc"
    sqlstm.sqindv[0] = (         void  *)0;
#line 189 "sqlbase.pc"
    sqlstm.sqinds[0] = (         int  )0;
#line 189 "sqlbase.pc"
    sqlstm.sqharm[0] = (unsigned int  )0;
#line 189 "sqlbase.pc"
    sqlstm.sqadto[0] = (unsigned short )0;
#line 189 "sqlbase.pc"
    sqlstm.sqtdso[0] = (unsigned short )0;
#line 189 "sqlbase.pc"
    sqlstm.sqhstv[1] = (         void  *)(gpxConnect->szPassword);
#line 189 "sqlbase.pc"
    sqlstm.sqhstl[1] = (unsigned int  )20;
#line 189 "sqlbase.pc"
    sqlstm.sqhsts[1] = (         int  )20;
#line 189 "sqlbase.pc"
    sqlstm.sqindv[1] = (         void  *)0;
#line 189 "sqlbase.pc"
    sqlstm.sqinds[1] = (         int  )0;
#line 189 "sqlbase.pc"
    sqlstm.sqharm[1] = (unsigned int  )0;
#line 189 "sqlbase.pc"
    sqlstm.sqadto[1] = (unsigned short )0;
#line 189 "sqlbase.pc"
    sqlstm.sqtdso[1] = (unsigned short )0;
#line 189 "sqlbase.pc"
    sqlstm.sqhstv[2] = (         void  *)(gpxConnect->szDatabase);
#line 189 "sqlbase.pc"
    sqlstm.sqhstl[2] = (unsigned int  )50;
#line 189 "sqlbase.pc"
    sqlstm.sqhsts[2] = (         int  )50;
#line 189 "sqlbase.pc"
    sqlstm.sqindv[2] = (         void  *)0;
#line 189 "sqlbase.pc"
    sqlstm.sqinds[2] = (         int  )0;
#line 189 "sqlbase.pc"
    sqlstm.sqharm[2] = (unsigned int  )0;
#line 189 "sqlbase.pc"
    sqlstm.sqadto[2] = (unsigned short )0;
#line 189 "sqlbase.pc"
    sqlstm.sqtdso[2] = (unsigned short )0;
#line 189 "sqlbase.pc"
    sqlstm.sqphsv = sqlstm.sqhstv;
#line 189 "sqlbase.pc"
    sqlstm.sqphsl = sqlstm.sqhstl;
#line 189 "sqlbase.pc"
    sqlstm.sqphss = sqlstm.sqhsts;
#line 189 "sqlbase.pc"
    sqlstm.sqpind = sqlstm.sqindv;
#line 189 "sqlbase.pc"
    sqlstm.sqpins = sqlstm.sqinds;
#line 189 "sqlbase.pc"
    sqlstm.sqparm = sqlstm.sqharm;
#line 189 "sqlbase.pc"
    sqlstm.sqparc = sqlstm.sqharc;
#line 189 "sqlbase.pc"
    sqlstm.sqpadto = sqlstm.sqadto;
#line 189 "sqlbase.pc"
    sqlstm.sqptdso = sqlstm.sqtdso;
#line 189 "sqlbase.pc"
    sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 189 "sqlbase.pc"
    if (sqlca.sqlcode < 0) sqlErrorHandler(__FILE__,__LINE__);
#line 189 "sqlbase.pc"
    if (sqlca.sqlwarn[0] == 'W') sqlWarningHandler(__FILE__,__LINE__);
#line 189 "sqlbase.pc"
}

#line 192 "sqlbase.pc"

    TRACE( fprintf( stdout, "Connectado : %s@%s\n"
                          , gpxConnect->szUsername
		      , gpxConnect->szDatabase ); )
  }

  return GetSQLStatus();
}

/*****************************************************************************
 * ++
 * Funcion      : beginTrans
 *
 * Descripcion  : Inicia una transaccion con la base de datos. 
 *
 * Parametros   :
 *             
 * Retorno      : ORA_SUCCESS 
 *                
 * Historial :
 *
 * Autor                Fecha           Descripcion
 * ---------------------------------------------------------------------------
 *
 * --
 ****************************************************************************/
int beginTrans(void){
  /* EXEC SQL WHENEVER SQLERROR DO sqlErrorHandler( __FILE__, __LINE__ ); */ 
#line 219 "sqlbase.pc"

  /* EXEC SQL WHENEVER SQLWARNING DO sqlWarningHandler( __FILE__, __LINE__ ); */ 
#line 220 "sqlbase.pc"


  return ORA_SUCCESS;
}

/*****************************************************************************
 * ++
 * Funcion      : beginReadOnlyTrans
 *
 * Descripcion  : Inicia una transaccion de solo lectura con la base de datos.
 *
 * Parametros   :
 *             
 * Retorno      : Estado de la transaccion.
 *                
 * Historial :
 *
 * Autor                Fecha           Descripcion
 * ---------------------------------------------------------------------------
 *
 * --
 ****************************************************************************/
int beginReadOnlyTrans(void){
  /* EXEC SQL WHENEVER SQLERROR DO sqlErrorHandler( __FILE__, __LINE__ ); */ 
#line 243 "sqlbase.pc"

  /* EXEC SQL WHENEVER SQLWARNING DO sqlWarningHandler( __FILE__, __LINE__ ); */ 
#line 244 "sqlbase.pc"


  /* EXEC SQL SET TRANSACTION READ ONLY; */ 
#line 246 "sqlbase.pc"

{
#line 246 "sqlbase.pc"
  struct sqlexd sqlstm;
#line 246 "sqlbase.pc"
  sqlstm.sqlvsn = 10;
#line 246 "sqlbase.pc"
  sqlstm.arrsiz = 4;
#line 246 "sqlbase.pc"
  sqlstm.sqladtp = &sqladt;
#line 246 "sqlbase.pc"
  sqlstm.sqltdsp = &sqltds;
#line 246 "sqlbase.pc"
  sqlstm.stmt = "set transaction READ ONLY";
#line 246 "sqlbase.pc"
  sqlstm.iters = (unsigned int  )1;
#line 246 "sqlbase.pc"
  sqlstm.offset = (unsigned int  )67;
#line 246 "sqlbase.pc"
  sqlstm.cud = sqlcud0;
#line 246 "sqlbase.pc"
  sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 246 "sqlbase.pc"
  sqlstm.sqlety = (unsigned short)256;
#line 246 "sqlbase.pc"
  sqlstm.occurs = (unsigned int  )0;
#line 246 "sqlbase.pc"
  sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 246 "sqlbase.pc"
  if (sqlca.sqlcode < 0) sqlErrorHandler(__FILE__,__LINE__);
#line 246 "sqlbase.pc"
  if (sqlca.sqlwarn[0] == 'W') sqlWarningHandler(__FILE__,__LINE__);
#line 246 "sqlbase.pc"
}

#line 246 "sqlbase.pc"

  return GetSQLStatus();
}

/*****************************************************************************
 * ++
 * Funcion      : commitWork
 *
 * Descripcion  : Realiza commit de base de datos. 
 *
 * Parametros   :
 *             
 * Retorno      : Resultado de la operaci�n. 
 *                
 * Historial :
 *
 * Autor                Fecha           Descripcion
 * ---------------------------------------------------------------------------
 *
 * --
 ****************************************************************************/
int commitWork(void){
  /* EXEC SQL WHENEVER SQLERROR DO sqlErrorHandler( __FILE__, __LINE__ ); */ 
#line 268 "sqlbase.pc"

  /* EXEC SQL WHENEVER SQLWARNING DO sqlWarningHandler( __FILE__, __LINE__ ); */ 
#line 269 "sqlbase.pc"

  
  /* EXEC SQL COMMIT WORK; */ 
#line 271 "sqlbase.pc"

{
#line 271 "sqlbase.pc"
  struct sqlexd sqlstm;
#line 271 "sqlbase.pc"
  sqlstm.sqlvsn = 10;
#line 271 "sqlbase.pc"
  sqlstm.arrsiz = 4;
#line 271 "sqlbase.pc"
  sqlstm.sqladtp = &sqladt;
#line 271 "sqlbase.pc"
  sqlstm.sqltdsp = &sqltds;
#line 271 "sqlbase.pc"
  sqlstm.iters = (unsigned int  )1;
#line 271 "sqlbase.pc"
  sqlstm.offset = (unsigned int  )82;
#line 271 "sqlbase.pc"
  sqlstm.cud = sqlcud0;
#line 271 "sqlbase.pc"
  sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 271 "sqlbase.pc"
  sqlstm.sqlety = (unsigned short)256;
#line 271 "sqlbase.pc"
  sqlstm.occurs = (unsigned int  )0;
#line 271 "sqlbase.pc"
  sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 271 "sqlbase.pc"
  if (sqlca.sqlcode < 0) sqlErrorHandler(__FILE__,__LINE__);
#line 271 "sqlbase.pc"
}

#line 271 "sqlbase.pc"

  return GetSQLStatus();
}

/*****************************************************************************
 * ++
 * Funcion      : rollbackWork
 *
 * Descripcion  : Realiza Rollback de base de datos. 
 *
 * Parametros   :
 *             
 * Retorno      : Resultado de la operacion. 
 *                
 * Historial :
 *
 * Autor                Fecha           Descripcion
 * ---------------------------------------------------------------------------
 *
 * --
 ****************************************************************************/
int rollbackWork(void){
  /* EXEC SQL WHENEVER SQLERROR DO sqlErrorHandler( __FILE__, __LINE__ ); */ 
#line 293 "sqlbase.pc"

  /* EXEC SQL WHENEVER SQLWARNING DO sqlWarningHandler( __FILE__, __LINE__ ); */ 
#line 294 "sqlbase.pc"


  /* EXEC SQL ROLLBACK WORK; */ 
#line 296 "sqlbase.pc"

{
#line 296 "sqlbase.pc"
  struct sqlexd sqlstm;
#line 296 "sqlbase.pc"
  sqlstm.sqlvsn = 10;
#line 296 "sqlbase.pc"
  sqlstm.arrsiz = 4;
#line 296 "sqlbase.pc"
  sqlstm.sqladtp = &sqladt;
#line 296 "sqlbase.pc"
  sqlstm.sqltdsp = &sqltds;
#line 296 "sqlbase.pc"
  sqlstm.iters = (unsigned int  )1;
#line 296 "sqlbase.pc"
  sqlstm.offset = (unsigned int  )97;
#line 296 "sqlbase.pc"
  sqlstm.cud = sqlcud0;
#line 296 "sqlbase.pc"
  sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 296 "sqlbase.pc"
  sqlstm.sqlety = (unsigned short)256;
#line 296 "sqlbase.pc"
  sqlstm.occurs = (unsigned int  )0;
#line 296 "sqlbase.pc"
  sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 296 "sqlbase.pc"
  if (sqlca.sqlcode < 0) sqlErrorHandler(__FILE__,__LINE__);
#line 296 "sqlbase.pc"
}

#line 296 "sqlbase.pc"

  return GetSQLStatus();
}

/*****************************************************************************
 * ++
 * Funcion      : disconnectDB
 *
 * Descripcion  : Realiza la desconexion de la base de datos.
 *
 * Parametros   :
 *             
 * Retorno      : Resultado de la operacion.
 *                
 * Historial :
 *
 * Autor                Fecha           Descripcion
 * ---------------------------------------------------------------------------
 *
 * --
 ****************************************************************************/
int disconnectDB(void){
  TRACE ( fprintf( stdout, "sqlbase.pc:disconnectDB()\n" ); )
  /* EXEC SQL WHENEVER SQLERROR DO sqlErrorHandler( __FILE__, __LINE__ ); */ 
#line 319 "sqlbase.pc"

  /* EXEC SQL WHENEVER SQLWARNING DO sqlWarningHandler( __FILE__, __LINE__ ); */ 
#line 320 "sqlbase.pc"


  /* EXEC SQL ROLLBACK WORK RELEASE; */ 
#line 322 "sqlbase.pc"

{
#line 322 "sqlbase.pc"
  struct sqlexd sqlstm;
#line 322 "sqlbase.pc"
  sqlstm.sqlvsn = 10;
#line 322 "sqlbase.pc"
  sqlstm.arrsiz = 4;
#line 322 "sqlbase.pc"
  sqlstm.sqladtp = &sqladt;
#line 322 "sqlbase.pc"
  sqlstm.sqltdsp = &sqltds;
#line 322 "sqlbase.pc"
  sqlstm.iters = (unsigned int  )1;
#line 322 "sqlbase.pc"
  sqlstm.offset = (unsigned int  )112;
#line 322 "sqlbase.pc"
  sqlstm.cud = sqlcud0;
#line 322 "sqlbase.pc"
  sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 322 "sqlbase.pc"
  sqlstm.sqlety = (unsigned short)256;
#line 322 "sqlbase.pc"
  sqlstm.occurs = (unsigned int  )0;
#line 322 "sqlbase.pc"
  sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 322 "sqlbase.pc"
  if (sqlca.sqlcode < 0) sqlErrorHandler(__FILE__,__LINE__);
#line 322 "sqlbase.pc"
}

#line 322 "sqlbase.pc"

  return GetSQLStatus();
}

/******************************************************************************
 * Funciones Privadas                                                         *
 ******************************************************************************/

#endif /* _SQLBASE_PC_ */
