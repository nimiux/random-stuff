
/* Result Sets Interface */
#ifndef SQL_CRSR
#  define SQL_CRSR
  struct sql_cursor
  {
    unsigned int curocn;
    void *ptr1;
    void *ptr2;
    unsigned long magic;
  };
  typedef struct sql_cursor sql_cursor;
  typedef struct sql_cursor SQL_CURSOR;
#endif /* SQL_CRSR */

/* Thread Safety */
typedef void * sql_context;
typedef void * SQL_CONTEXT;

/* Object support */
struct sqltvn
{
  unsigned char *tvnvsn; 
  unsigned short tvnvsnl; 
  unsigned char *tvnnm;
  unsigned short tvnnml; 
  unsigned char *tvnsnm;
  unsigned short tvnsnml;
};
typedef struct sqltvn sqltvn;

struct sqladts
{
  unsigned int adtvsn; 
  unsigned short adtmode; 
  unsigned short adtnum;  
  sqltvn adttvn[1];       
};
typedef struct sqladts sqladts;

static struct sqladts sqladt = {
  1,1,0,
};

/* Binding to PL/SQL Records */
struct sqltdss
{
  unsigned int tdsvsn; 
  unsigned short tdsnum; 
  unsigned char *tdsval[1]; 
};
typedef struct sqltdss sqltdss;
static struct sqltdss sqltds =
{
  1,
  0,
};

/* File name & Package Name */
struct sqlcxp
{
  unsigned short fillen;
           char  filnam[12];
};
static const struct sqlcxp sqlfpn =
{
    11,
    "personas.pc"
};


static unsigned long sqlctx = 158531;


static struct sqlexd {
   unsigned int   sqlvsn;
   unsigned int   arrsiz;
   unsigned int   iters;
   unsigned int   offset;
   unsigned short selerr;
   unsigned short sqlety;
   unsigned int   occurs;
      const short *cud;
   unsigned char  *sqlest;
      const char  *stmt;
   sqladts *sqladtp;
   sqltdss *sqltdsp;
            void  **sqphsv;
   unsigned int   *sqphsl;
            int   *sqphss;
            void  **sqpind;
            int   *sqpins;
   unsigned int   *sqparm;
   unsigned int   **sqparc;
   unsigned short  *sqpadto;
   unsigned short  *sqptdso;
            void  *sqhstv[4];
   unsigned int   sqhstl[4];
            int   sqhsts[4];
            void  *sqindv[4];
            int   sqinds[4];
   unsigned int   sqharm[4];
   unsigned int   *sqharc[4];
   unsigned short  sqadto[4];
   unsigned short  sqtdso[4];
} sqlstm = {10,4};

// Prototypes
extern "C" {
  void sqlcxt (void **, unsigned long *,
               struct sqlexd *, const struct sqlcxp *);
  void sqlcx2t(void **, unsigned long *,
               struct sqlexd *, const struct sqlcxp *);
  void sqlbuft(void **, char *);
  void sqlgs2t(void **, char *);
  void sqlorat(void **, unsigned long *, void *);
}

// Forms Interface
static const int IAPSUCC = 0;
static const int IAPFAIL = 1403;
static const int IAPFTL  = 535;
extern "C" { void sqliem(char *, int *); }

 static const char *sq0001 = 
"select personas.dni ,personas.name ,personas.surname ,ROWID  from personas w\
here personas.dni=:b0           ";

typedef struct { unsigned short len; unsigned char arr[1]; } VARCHAR;
typedef struct { unsigned short len; unsigned char arr[1]; } varchar;

/* cud (compilation unit data) array */
static const short sqlcud0[] =
{10,4130,0,0,0,
5,0,0,1,108,0,521,90,0,0,1,1,0,1,0,1,5,0,0,
24,0,0,1,0,0,525,96,0,0,4,0,0,1,0,2,5,0,0,2,5,0,0,2,5,0,0,2,5,0,0,
55,0,0,1,0,0,527,117,0,0,0,0,0,1,0,
};


#line 1 "personas.pc"
#include <stdio.h>
#include <string.h>

#define SQLCA_STORAGE_CLASS extern
#include "sqlca.h"
/* EXEC SQL INCLUDE personas.h;
 */ 
#line 1 "/home/chema/cprojects/sqlwrapper/include/personas.h"
#ifndef _PERSONAS_H_
#define _PERSONAS_H_

#include "TCollection.h"
#include "TGenericDBTable.h"
#include "personasPK.h"
#include "persona.h"

/* EXEC SQL INCLUDE sqlbase.h;
 */ 
#line 1 "/home/chema/cprojects/sqlwrapper/include/sqlbase.h"
/******************************************************************************
 * ++
 * Proyecto     : 
 *
 * Autor        : Jose Maria Alonso. 
 *
 * Modulo       : sqlbase.h
 *
 * Descripcion  : 
 *
 * Historal :
 *
 * Autor                Fecha           Descripcion
 * ----------------     --------------  ------------------------------------
 *
 * --
 ******************************************************************************/
#ifndef _SQLBASE_H_
#define _SQLBASE_H_

/******************************************************************************
 * Ajuste a los Estandares                                                    *
 ******************************************************************************/

/******************************************************************************
 * Includes de Sistema                                                        *
 ******************************************************************************/

/******************************************************************************
 * Includes de Aplicacion                                                     *
 ******************************************************************************/

/******************************************************************************
 * Opciones de Debug y Traza                                                  *
 ******************************************************************************/

/******************************************************************************
 * Macros                                                                     *
 ******************************************************************************/

#define ROWID_LEN                        18

#define MAX_HOST_ARRAY                  100

#define GetSQLStatus()  sqlca.sqlcode

#define ORA_SUCCESS              0
#define ORA_ERROR                1
#define ORA_LOST                 2
#define ORA_NOT_FOUND         1403
#define ORA_NOT_CONNECTED    -1012

#define USERNAME_LEN 20
#define PASSWORD_LEN 20
#define DATABASE_LEN 50

/******************************************************************************
 * Tipos                                                                      *
 ******************************************************************************/

/* Tipos necesarios para acceso a base de datos */

/* EXEC SQL BEGIN DECLARE SECTION; */ 
#line 63 "/home/chema/cprojects/sqlwrapper/include/sqlbase.h"

   struct TConnect{
      char szUsername[USERNAME_LEN];
      char szPassword[PASSWORD_LEN];
      char szDatabase[DATABASE_LEN];
   };

   typedef struct TConnect TConnect;

   struct st_rowid {
   	char szRowId         [ROWID_LEN + 1 ];
   };

   typedef struct st_rowid ST_RowId;

/* EXEC SQL END DECLARE SECTION; */ 
#line 78 "/home/chema/cprojects/sqlwrapper/include/sqlbase.h"


/******************************************************************************
 * Variables Globales                                                         *
 ******************************************************************************/

/******************************************************************************
 * Variables y funciones externas.                                            *
 ******************************************************************************/

/******************************************************************************
 * Prototipos de Funciones Publicas                                           *
 ******************************************************************************/
void sqlErrorHandler( char * pszFile, int wLineNumber );
void sqlWarningHandler( char * pszFile, int wLineNumber );
int connectDB();
int beginTrans(void);
int beginReadOnlyTrans(void);
int commitWork(void);
int rollbackWork(void);
int disconnectDB(void);
/******************************************************************************
 * Prototipos de Funciones Privadas                                           *
 ******************************************************************************/

#endif /* _SQLBASE_H_ */

#line 10 "/home/chema/cprojects/sqlwrapper/include/personas.h"

#define PERSONAS_DNI_LEN     10
#define PERSONAS_NAME_LEN    20
#define PERSONAS_SURNAME_LEN 20

#define TRACE(x) x

/* EXEC SQL BEGIN DECLARE SECTION; */ 
#line 17 "/home/chema/cprojects/sqlwrapper/include/personas.h"


   struct personas_row {
      char szdni        [PERSONAS_DNI_LEN + 1];
      char szname       [PERSONAS_NAME_LEN + 1];
      char szsurname    [PERSONAS_SURNAME_LEN + 1];
   };

   typedef struct personas_row PersonasRow;

   struct personas_ind {
      short iszdni;
      short iszname;
      short iszsurname;
   };

   typedef struct personas_ind PersonasInd;

   struct dbpersonas {
      PersonasRow   rgxRow   [MAX_HOST_ARRAY];
      PersonasInd   rgxInd   [MAX_HOST_ARRAY];
      ST_RowId      rgxRowId [MAX_HOST_ARRAY];
   };
   
   typedef struct dbpersonas DBPersonas;

/* EXEC SQL END DECLARE SECTION; */ 
#line 43 "/home/chema/cprojects/sqlwrapper/include/personas.h"


typedef TCollection<Persona> PersonaColl;

class Personas : public TGenericDBTable {

   private:
      PersonaColl *m_pxpersonasdata;
      PersonasPK  *m_pxpersonaspk;

      /* EXEC SQL BEGIN DECLARE SECTION; */ 
#line 53 "/home/chema/cprojects/sqlwrapper/include/personas.h"

         // Miembros necesarios para el acceso mediante Host Arrays
         DBPersonas    m_xpersonas;
         PersonasRow   *m_pxpersonasrow;
         PersonasInd   *m_pxpersonasind;
         ST_RowId      *m_pxpersonasrowid;

         int m_wblocksize;
      /* EXEC SQL END DECLARE SECTION; */ 
#line 61 "/home/chema/cprojects/sqlwrapper/include/personas.h"


   private:
      void destroy();
      void init ();

   public:
      Personas ();
      ~Personas ();

      void setprimarykey ( PersonasPK *pxpersonapk ); 

      void add ( Persona *pxpersona );
      int getcount();
      Persona *getat( int iindex );

      int selectbyprimarykey();
};
#endif // _PERSONAS_H_

#line 7 "personas.pc"

Personas::Personas () {

   init();
}

Personas ::~Personas () {

   destroy();
}

void Personas::init () {

   m_pxpersonasdata   = new PersonaColl ();
   m_pxpersonaspk     = new PersonasPK ();

   m_pxpersonasrow    = m_xpersonas.rgxRow;
   m_pxpersonasind    = m_xpersonas.rgxInd;
   m_pxpersonasrowid  = m_xpersonas.rgxRowId;
   m_wblocksize       = MAX_HOST_ARRAY;

}

void Personas::destroy() {

   delete m_pxpersonasdata;
   delete m_pxpersonaspk;
}

void Personas::add ( Persona *pxpersona ) {
   m_pxpersonasdata->insert( pxpersona );

}

int Personas::getcount() {
   return m_pxpersonasdata->getcount();
}

Persona *Personas::getat( int iindex ) {
   return m_pxpersonasdata->getat( iindex );
}

void Personas::setprimarykey ( PersonasPK *pxpersonaspk) {
   if ( m_pxpersonaspk ) {
      delete m_pxpersonaspk;
	}
	m_pxpersonaspk = pxpersonaspk->clone();
}

int Personas::selectbyprimarykey () {

   int i, wtotalrows;
   BOOL bnomorerows;

   /* EXEC SQL BEGIN DECLARE SECTION; */ 
#line 61 "personas.pc"

      char pszdni [PERSONAS_DNI_LEN + 1];
   /* EXEC SQL END DECLARE SECTION; */ 
#line 63 "personas.pc"


	int ires = RET_SUCCESS;
   int inumrows = 0;
   if ( m_pxpersonaspk ) {

		if ( m_pxpersonasdata ) {
         delete m_pxpersonasdata;
      }
      m_pxpersonasdata = new PersonaColl();

      /* EXEC SQL WHENEVER SQLERROR DO sqlErrorHandler( __FILE__, __LINE__ ); */ 
#line 74 "personas.pc"

      /* EXEC SQL WHENEVER SQLWARNING DO sqlWarningHandler( __FILE__, __LINE__ ); */ 
#line 75 "personas.pc"

 
		strcpy ( pszdni, m_pxpersonaspk->getdni() );
	   printf("Apertura de cursor para DNI: %s\n",pszdni);
      /* EXEC SQL DECLARE personas CURSOR FOR
       SELECT personas.dni
            , personas.name
            , personas.surname 
            , ROWID
       FROM  personas
       WHERE personas.dni = :pszdni; */ 
#line 85 "personas.pc"

	
	   printf("A abrir cursor\n");
	   /* EXEC SQL WHENEVER NOT FOUND CONTINUE; */ 
#line 88 "personas.pc"

	   printf("En medio\n");
      /* EXEC SQL OPEN personas; */ 
#line 90 "personas.pc"

{
#line 90 "personas.pc"
      struct sqlexd sqlstm;
#line 90 "personas.pc"
      sqlstm.sqlvsn = 10;
#line 90 "personas.pc"
      sqlstm.arrsiz = 1;
#line 90 "personas.pc"
      sqlstm.sqladtp = &sqladt;
#line 90 "personas.pc"
      sqlstm.sqltdsp = &sqltds;
#line 90 "personas.pc"
      sqlstm.stmt = sq0001;
#line 90 "personas.pc"
      sqlstm.iters = (unsigned int  )1;
#line 90 "personas.pc"
      sqlstm.offset = (unsigned int  )5;
#line 90 "personas.pc"
      sqlstm.selerr = (unsigned short)1;
#line 90 "personas.pc"
      sqlstm.cud = sqlcud0;
#line 90 "personas.pc"
      sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 90 "personas.pc"
      sqlstm.sqlety = (unsigned short)256;
#line 90 "personas.pc"
      sqlstm.occurs = (unsigned int  )0;
#line 90 "personas.pc"
      sqlstm.sqhstv[0] = (         void  *)pszdni;
#line 90 "personas.pc"
      sqlstm.sqhstl[0] = (unsigned int  )11;
#line 90 "personas.pc"
      sqlstm.sqhsts[0] = (         int  )0;
#line 90 "personas.pc"
      sqlstm.sqindv[0] = (         void  *)0;
#line 90 "personas.pc"
      sqlstm.sqinds[0] = (         int  )0;
#line 90 "personas.pc"
      sqlstm.sqharm[0] = (unsigned int  )0;
#line 90 "personas.pc"
      sqlstm.sqadto[0] = (unsigned short )0;
#line 90 "personas.pc"
      sqlstm.sqtdso[0] = (unsigned short )0;
#line 90 "personas.pc"
      sqlstm.sqphsv = sqlstm.sqhstv;
#line 90 "personas.pc"
      sqlstm.sqphsl = sqlstm.sqhstl;
#line 90 "personas.pc"
      sqlstm.sqphss = sqlstm.sqhsts;
#line 90 "personas.pc"
      sqlstm.sqpind = sqlstm.sqindv;
#line 90 "personas.pc"
      sqlstm.sqpins = sqlstm.sqinds;
#line 90 "personas.pc"
      sqlstm.sqparm = sqlstm.sqharm;
#line 90 "personas.pc"
      sqlstm.sqparc = sqlstm.sqharc;
#line 90 "personas.pc"
      sqlstm.sqpadto = sqlstm.sqadto;
#line 90 "personas.pc"
      sqlstm.sqptdso = sqlstm.sqtdso;
#line 90 "personas.pc"
      sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 90 "personas.pc"
      if (sqlca.sqlcode < 0) sqlErrorHandler(__FILE__,__LINE__);
#line 90 "personas.pc"
      if (sqlca.sqlwarn[0] == 'W') sqlWarningHandler(__FILE__,__LINE__);
#line 90 "personas.pc"
}

#line 90 "personas.pc"


	   printf("Cursor abierto\n");
	   wtotalrows = 0;
      bnomorerows = FALSE;
      while (!bnomorerows) {
         /* EXEC SQL FOR :m_wblocksize
         FETCH personas INTO  :m_pxpersonasrow INDICATOR :m_pxpersonasind
                            , :m_pxpersonasrowid; */ 
#line 98 "personas.pc"

{
#line 96 "personas.pc"
         struct sqlexd sqlstm;
#line 96 "personas.pc"
         sqlstm.sqlvsn = 10;
#line 96 "personas.pc"
         sqlstm.arrsiz = 4;
#line 96 "personas.pc"
         sqlstm.sqladtp = &sqladt;
#line 96 "personas.pc"
         sqlstm.sqltdsp = &sqltds;
#line 96 "personas.pc"
         sqlstm.iters = (unsigned int  )m_wblocksize;
#line 96 "personas.pc"
         sqlstm.offset = (unsigned int  )24;
#line 96 "personas.pc"
         sqlstm.cud = sqlcud0;
#line 96 "personas.pc"
         sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 96 "personas.pc"
         sqlstm.sqlety = (unsigned short)256;
#line 96 "personas.pc"
         sqlstm.occurs = (unsigned int  )0;
#line 96 "personas.pc"
         sqlstm.sqhstv[0] = (         void  *)m_pxpersonasrow->szdni;
#line 96 "personas.pc"
         sqlstm.sqhstl[0] = (unsigned int  )11;
#line 96 "personas.pc"
         sqlstm.sqhsts[0] = (         int  )sizeof(struct personas_row);
#line 96 "personas.pc"
         sqlstm.sqindv[0] = (         void  *)&m_pxpersonasind->iszdni;
#line 96 "personas.pc"
         sqlstm.sqinds[0] = (         int  )sizeof(struct personas_ind);
#line 96 "personas.pc"
         sqlstm.sqharm[0] = (unsigned int  )0;
#line 96 "personas.pc"
         sqlstm.sqadto[0] = (unsigned short )0;
#line 96 "personas.pc"
         sqlstm.sqtdso[0] = (unsigned short )0;
#line 96 "personas.pc"
         sqlstm.sqhstv[1] = (         void  *)m_pxpersonasrow->szname;
#line 96 "personas.pc"
         sqlstm.sqhstl[1] = (unsigned int  )21;
#line 96 "personas.pc"
         sqlstm.sqhsts[1] = (         int  )sizeof(struct personas_row);
#line 96 "personas.pc"
         sqlstm.sqindv[1] = (         void  *)&m_pxpersonasind->iszname;
#line 96 "personas.pc"
         sqlstm.sqinds[1] = (         int  )sizeof(struct personas_ind);
#line 96 "personas.pc"
         sqlstm.sqharm[1] = (unsigned int  )0;
#line 96 "personas.pc"
         sqlstm.sqadto[1] = (unsigned short )0;
#line 96 "personas.pc"
         sqlstm.sqtdso[1] = (unsigned short )0;
#line 96 "personas.pc"
         sqlstm.sqhstv[2] = (         void  *)m_pxpersonasrow->szsurname;
#line 96 "personas.pc"
         sqlstm.sqhstl[2] = (unsigned int  )21;
#line 96 "personas.pc"
         sqlstm.sqhsts[2] = (         int  )sizeof(struct personas_row);
#line 96 "personas.pc"
         sqlstm.sqindv[2] = (         void  *)&m_pxpersonasind->iszsurname;
#line 96 "personas.pc"
         sqlstm.sqinds[2] = (         int  )sizeof(struct personas_ind);
#line 96 "personas.pc"
         sqlstm.sqharm[2] = (unsigned int  )0;
#line 96 "personas.pc"
         sqlstm.sqadto[2] = (unsigned short )0;
#line 96 "personas.pc"
         sqlstm.sqtdso[2] = (unsigned short )0;
#line 96 "personas.pc"
         sqlstm.sqhstv[3] = (         void  *)m_pxpersonasrowid->szRowId;
#line 96 "personas.pc"
         sqlstm.sqhstl[3] = (unsigned int  )19;
#line 96 "personas.pc"
         sqlstm.sqhsts[3] = (         int  )sizeof(struct st_rowid);
#line 96 "personas.pc"
         sqlstm.sqindv[3] = (         void  *)0;
#line 96 "personas.pc"
         sqlstm.sqinds[3] = (         int  )0;
#line 96 "personas.pc"
         sqlstm.sqharm[3] = (unsigned int  )0;
#line 96 "personas.pc"
         sqlstm.sqadto[3] = (unsigned short )0;
#line 96 "personas.pc"
         sqlstm.sqtdso[3] = (unsigned short )0;
#line 96 "personas.pc"
         sqlstm.sqphsv = sqlstm.sqhstv;
#line 96 "personas.pc"
         sqlstm.sqphsl = sqlstm.sqhstl;
#line 96 "personas.pc"
         sqlstm.sqphss = sqlstm.sqhsts;
#line 96 "personas.pc"
         sqlstm.sqpind = sqlstm.sqindv;
#line 96 "personas.pc"
         sqlstm.sqpins = sqlstm.sqinds;
#line 96 "personas.pc"
         sqlstm.sqparm = sqlstm.sqharm;
#line 96 "personas.pc"
         sqlstm.sqparc = sqlstm.sqharc;
#line 96 "personas.pc"
         sqlstm.sqpadto = sqlstm.sqadto;
#line 96 "personas.pc"
         sqlstm.sqptdso = sqlstm.sqtdso;
#line 96 "personas.pc"
         sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 96 "personas.pc"
         if (sqlca.sqlcode < 0) sqlErrorHandler(__FILE__,__LINE__);
#line 96 "personas.pc"
         if (sqlca.sqlwarn[0] == 'W') sqlWarningHandler(__FILE__,__LINE__);
#line 96 "personas.pc"
}

#line 98 "personas.pc"

         bnomorerows = (GetSQLStatus() == ORA_NOT_FOUND);
         inumrows = (sqlca.sqlerrd[2] - wtotalrows);
         wtotalrows = sqlca.sqlerrd[2];
         for (i = 0; i < inumrows; i++) {
            /*************************************\
              COMPROBAR NULLS CON INDICATORS 
            \*************************************/
            Persona *pxpersona = new Persona ( m_pxpersonasrow[i].szdni
                                             , m_pxpersonasrow[i].szname
                                             , m_pxpersonasrow[i].szsurname
                                             , m_pxpersonasrowid[i].szRowId );
            add( pxpersona);
            TRACE ( fprintf(stdout, "Procesando fila PERSONAS: %s-%s-%s\n" 
                                  , m_pxpersonasrow[i].szdni
                                  , m_pxpersonasrow[i].szname
                                  , m_pxpersonasrow[i].szsurname ); )
         }
      }
      /* EXEC SQL CLOSE personas; */ 
#line 117 "personas.pc"

{
#line 117 "personas.pc"
      struct sqlexd sqlstm;
#line 117 "personas.pc"
      sqlstm.sqlvsn = 10;
#line 117 "personas.pc"
      sqlstm.arrsiz = 4;
#line 117 "personas.pc"
      sqlstm.sqladtp = &sqladt;
#line 117 "personas.pc"
      sqlstm.sqltdsp = &sqltds;
#line 117 "personas.pc"
      sqlstm.iters = (unsigned int  )1;
#line 117 "personas.pc"
      sqlstm.offset = (unsigned int  )55;
#line 117 "personas.pc"
      sqlstm.cud = sqlcud0;
#line 117 "personas.pc"
      sqlstm.sqlest = (unsigned char  *)&sqlca;
#line 117 "personas.pc"
      sqlstm.sqlety = (unsigned short)256;
#line 117 "personas.pc"
      sqlstm.occurs = (unsigned int  )0;
#line 117 "personas.pc"
      sqlcxt((void **)0, &sqlctx, &sqlstm, &sqlfpn);
#line 117 "personas.pc"
      if (sqlca.sqlcode < 0) sqlErrorHandler(__FILE__,__LINE__);
#line 117 "personas.pc"
}

#line 117 "personas.pc"
 
   }

   return ires;
}

