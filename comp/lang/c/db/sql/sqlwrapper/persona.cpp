#include <stdio.h> 
#include <string.h> 

#include "persona.h"

Persona::Persona (  char * pszdni
                  , char * pszname
                  , char * pszsurname
                  , char * pszrowid ) {

   init ( pszdni, pszname, pszsurname, pszrowid );
}

Persona::Persona ( Persona *pxpersona ) {

   init (  pxpersona->getdni()
         , pxpersona->getname()
         , pxpersona->getsurname()
         , pxpersona->getrowid() );
}

void Persona::init (   char * pszdni
                     , char * pszname
                     , char * pszsurname
                     , char * pszrowid ) {

   setdni     ( pszdni);
   setname    ( pszname);
   setsurname ( pszsurname);
   setrowid   ( pszrowid);
}

Persona::~Persona() {

}

char *Persona::getdni() {
   return m_szdni;
}
char *Persona::getname() {
   return m_szname;
}
char *Persona::getsurname() {
   return m_szsurname;
}
char *Persona::getrowid() {
   return m_szrowid;
}

void Persona::setdni ( char * pszdni ) {
   strcpy ( m_szdni, pszdni);
}

void Persona::setname ( char * pszname ) {
   strcpy ( m_szname, pszname);
}

void Persona::setsurname ( char * pszsurname ) {
   strcpy ( m_szsurname, pszsurname);
}

void Persona::setrowid ( char * pszrowid ) {
   strcpy ( m_szrowid, pszrowid);
}


Persona *Persona::clone() {
   return new Persona ( this );
}
