
/* Result Sets Interface */
#ifndef SQL_CRSR
#  define SQL_CRSR
  struct sql_cursor
  {
    unsigned int curocn;
    void *ptr1;
    void *ptr2;
    unsigned long magic;
  };
  typedef struct sql_cursor sql_cursor;
  typedef struct sql_cursor SQL_CURSOR;
#endif /* SQL_CRSR */

/* Thread Safety */
typedef void * sql_context;
typedef void * SQL_CONTEXT;

/* Object support */
struct sqltvn
{
  unsigned char *tvnvsn; 
  unsigned short tvnvsnl; 
  unsigned char *tvnnm;
  unsigned short tvnnml; 
  unsigned char *tvnsnm;
  unsigned short tvnsnml;
};
typedef struct sqltvn sqltvn;

struct sqladts
{
  unsigned int adtvsn; 
  unsigned short adtmode; 
  unsigned short adtnum;  
  sqltvn adttvn[1];       
};
typedef struct sqladts sqladts;

static struct sqladts sqladt = {
  1,1,0,
};

/* Binding to PL/SQL Records */
struct sqltdss
{
  unsigned int tdsvsn; 
  unsigned short tdsnum; 
  unsigned char *tdsval[1]; 
};
typedef struct sqltdss sqltdss;
static struct sqltdss sqltds =
{
  1,
  0,
};

/* File name & Package Name */
struct sqlcxp
{
  unsigned short fillen;
           char  filnam[8];
};
static const struct sqlcxp sqlfpn =
{
    7,
    "main.pc"
};


static unsigned long sqlctx = 9211;


static struct sqlexd {
   unsigned int   sqlvsn;
   unsigned int   arrsiz;
   unsigned int   iters;
   unsigned int   offset;
   unsigned short selerr;
   unsigned short sqlety;
   unsigned int   occurs;
      const short *cud;
   unsigned char  *sqlest;
      const char  *stmt;
   sqladts *sqladtp;
   sqltdss *sqltdsp;
            void  **sqphsv;
   unsigned int   *sqphsl;
            int   *sqphss;
            void  **sqpind;
            int   *sqpins;
   unsigned int   *sqparm;
   unsigned int   **sqparc;
   unsigned short  *sqpadto;
   unsigned short  *sqptdso;
            void  *sqhstv[1];
   unsigned int   sqhstl[1];
            int   sqhsts[1];
            void  *sqindv[1];
            int   sqinds[1];
   unsigned int   sqharm[1];
   unsigned int   *sqharc[1];
   unsigned short  sqadto[1];
   unsigned short  sqtdso[1];
} sqlstm = {10,1};

// Prototypes
extern "C" {
  void sqlcxt (void **, unsigned long *,
               struct sqlexd *, const struct sqlcxp *);
  void sqlcx2t(void **, unsigned long *,
               struct sqlexd *, const struct sqlcxp *);
  void sqlbuft(void **, char *);
  void sqlgs2t(void **, char *);
  void sqlorat(void **, unsigned long *, void *);
}

// Forms Interface
static const int IAPSUCC = 0;
static const int IAPFAIL = 1403;
static const int IAPFTL  = 535;
extern "C" { void sqliem(char *, int *); }

typedef struct { unsigned short len; unsigned char arr[1]; } VARCHAR;
typedef struct { unsigned short len; unsigned char arr[1]; } varchar;

/* cud (compilation unit data) array */
static const short sqlcud0[] =
{10,4130,0,0,0,
};


#line 1 "main.pc"
#include <stdio.h>
#include <string.h>

/* EXEC SQL INCLUDE personas.h;
 */ 
#line 1 "/home/chema/cprojects/sqlwrapper/include/personas.h"
#ifndef _PERSONAS_H_
#define _PERSONAS_H_

#include "TCollection.h"
#include "TGenericDBTable.h"
#include "personasPK.h"
#include "persona.h"

/* EXEC SQL INCLUDE sqlbase.h;
 */ 
#line 1 "/home/chema/cprojects/sqlwrapper/include/sqlbase.h"
/******************************************************************************
 * ++
 * Proyecto     : 
 *
 * Autor        : Jose Maria Alonso. 
 *
 * Modulo       : sqlbase.h
 *
 * Descripcion  : 
 *
 * Historal :
 *
 * Autor                Fecha           Descripcion
 * ----------------     --------------  ------------------------------------
 *
 * --
 ******************************************************************************/
#ifndef _SQLBASE_H_
#define _SQLBASE_H_

/******************************************************************************
 * Ajuste a los Estandares                                                    *
 ******************************************************************************/

/******************************************************************************
 * Includes de Sistema                                                        *
 ******************************************************************************/

/******************************************************************************
 * Includes de Aplicacion                                                     *
 ******************************************************************************/

/******************************************************************************
 * Opciones de Debug y Traza                                                  *
 ******************************************************************************/

/******************************************************************************
 * Macros                                                                     *
 ******************************************************************************/

#define ROWID_LEN                        18

#define MAX_HOST_ARRAY                  100

#define GetSQLStatus()  sqlca.sqlcode

#define ORA_SUCCESS              0
#define ORA_ERROR                1
#define ORA_LOST                 2
#define ORA_NOT_FOUND         1403
#define ORA_NOT_CONNECTED    -1012

#define USERNAME_LEN 20
#define PASSWORD_LEN 20
#define DATABASE_LEN 50

/******************************************************************************
 * Tipos                                                                      *
 ******************************************************************************/

/* Tipos necesarios para acceso a base de datos */

/* EXEC SQL BEGIN DECLARE SECTION; */ 
#line 63 "/home/chema/cprojects/sqlwrapper/include/sqlbase.h"

   struct TConnect{
      char szUsername[USERNAME_LEN];
      char szPassword[PASSWORD_LEN];
      char szDatabase[DATABASE_LEN];
   };

   typedef struct TConnect TConnect;

   struct st_rowid {
   	char szRowId         [ROWID_LEN + 1 ];
   };

   typedef struct st_rowid ST_RowId;

/* EXEC SQL END DECLARE SECTION; */ 
#line 78 "/home/chema/cprojects/sqlwrapper/include/sqlbase.h"


/******************************************************************************
 * Variables Globales                                                         *
 ******************************************************************************/

/******************************************************************************
 * Variables y funciones externas.                                            *
 ******************************************************************************/

/******************************************************************************
 * Prototipos de Funciones Publicas                                           *
 ******************************************************************************/
void sqlErrorHandler( char * pszFile, int wLineNumber );
void sqlWarningHandler( char * pszFile, int wLineNumber );
int connectDB();
int beginTrans(void);
int beginReadOnlyTrans(void);
int commitWork(void);
int rollbackWork(void);
int disconnectDB(void);
/******************************************************************************
 * Prototipos de Funciones Privadas                                           *
 ******************************************************************************/

#endif /* _SQLBASE_H_ */

#line 10 "/home/chema/cprojects/sqlwrapper/include/personas.h"

#define PERSONAS_DNI_LEN     10
#define PERSONAS_NAME_LEN    20
#define PERSONAS_SURNAME_LEN 20

#define TRACE(x) x

/* EXEC SQL BEGIN DECLARE SECTION; */ 
#line 17 "/home/chema/cprojects/sqlwrapper/include/personas.h"


   struct personas_row {
      char szdni        [PERSONAS_DNI_LEN + 1];
      char szname       [PERSONAS_NAME_LEN + 1];
      char szsurname    [PERSONAS_SURNAME_LEN + 1];
   };

   typedef struct personas_row PersonasRow;

   struct personas_ind {
      short iszdni;
      short iszname;
      short iszsurname;
   };

   typedef struct personas_ind PersonasInd;

   struct dbpersonas {
      PersonasRow   rgxRow   [MAX_HOST_ARRAY];
      PersonasInd   rgxInd   [MAX_HOST_ARRAY];
      ST_RowId      rgxRowId [MAX_HOST_ARRAY];
   };
   
   typedef struct dbpersonas DBPersonas;

/* EXEC SQL END DECLARE SECTION; */ 
#line 43 "/home/chema/cprojects/sqlwrapper/include/personas.h"


typedef TCollection<Persona> PersonaColl;

class Personas : public TGenericDBTable {

   private:
      PersonaColl *m_pxpersonasdata;
      PersonasPK  *m_pxpersonaspk;

      /* EXEC SQL BEGIN DECLARE SECTION; */ 
#line 53 "/home/chema/cprojects/sqlwrapper/include/personas.h"

         // Miembros necesarios para el acceso mediante Host Arrays
         DBPersonas    m_xpersonas;
         PersonasRow   *m_pxpersonasrow;
         PersonasInd   *m_pxpersonasind;
         ST_RowId      *m_pxpersonasrowid;

         int m_wblocksize;
      /* EXEC SQL END DECLARE SECTION; */ 
#line 61 "/home/chema/cprojects/sqlwrapper/include/personas.h"


   private:
      void destroy();
      void init ();

   public:
      Personas ();
      ~Personas ();

      void setprimarykey ( PersonasPK *pxpersonapk ); 

      void add ( Persona *pxpersona );
      int getcount();
      Persona *getat( int iindex );

      int selectbyprimarykey();
};
#endif // _PERSONAS_H_

extern TConnect *gpxConnect;

int main (int argc, char *argv[]) {

  printf ("Prueba.\n");

  gpxConnect = new TConnect;

  strcpy( gpxConnect->szUsername, "KK");
  strcpy( gpxConnect->szPassword, "KK");

  connectDB();

  Personas *pxp = new Personas();
  PersonasPK *pxp2pk = new PersonasPK("1");
  Personas *pxp2 = new Personas();

  pxp2->setprimarykey( pxp2pk );

  pxp->add ( new Persona ( "1231231", "Paco", "Perez", "1") );
  pxp->add ( new Persona ( "2131212", "Perico", "Palotes", "2") );
  pxp->add ( new Persona ( "3131231", "Manolo", "Kabezabolo", "3") );
  pxp2->add ( new Persona ( "4444444", "Pepin", "Nota", "4") );

  printf ("numero de elementos.%d\n", pxp->getcount());
  printf ("numero de elementos de 2.%d\n", pxp2->getcount());
   
  printf ("Elemento: %s\n", pxp->getat(0)->getname());
  printf ("Elemento: %s\n", pxp->getat(1)->getname());
  printf ("Elemento: %s\n", pxp->getat(2)->getname());
  printf ("Elemento de  2: %s\n", pxp2->getat(pxp2->getcount()-1)->getname());

  printf ("***************\n");
  pxp2->selectbyprimarykey();
  printf ("***************\n");
  printf ("numero de elementos despues select .%d\n", pxp2->getcount());

  for (int i = 0; i < pxp2->getcount(); i++) {
     printf ("Elemento %d: %s\n", i, pxp2->getat(i)->getname());
  }
  delete pxp;
  delete pxp2;
  delete gpxConnect;

  return 0;
}
