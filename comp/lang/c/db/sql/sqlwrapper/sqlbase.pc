/*****************************************************************************
 * ++
 * Proyecto     : 
 *
 * Autor        : Jose Maria Alonso. 29/07/2002.
 *
 * Modulo       : sqlbase.pc
 *
 * Descripcion  : 
 *
 * Historal     :
 *
 * Autor                Fecha           Descripcion
 * ----------------     --------------  ------------------------------------
 * --
 ******************************************************************************/
#ifndef _SQLBASE_PC_
#define _SQLBASE_PC_

/******************************************************************************
 * Ajuste a los Estandares                                                    * 
 ******************************************************************************/

/******************************************************************************
 * Includes de Sistema                                                        *
 ******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

/******************************************************************************
 * Includes de Aplicacion                                                     *
 ******************************************************************************/
#include "sqlca.h"
#include "sqlcpr.h"
EXEC SQL INCLUDE sqlbase.h;
#include "TTypes.h"

/******************************************************************************
 * Opciones de Debug y Traza                                                  *
 ******************************************************************************/

#ifdef TRACE_SQL_BASE
 #define TRACE(x) x
#else
 #define TRACE(x)
#endif

/******************************************************************************
 * Macros                                                                     *
 ******************************************************************************/

/******************************************************************************
 * Tipos                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Variables Globales                                                         *
 ******************************************************************************/

EXEC SQL BEGIN DECLARE SECTION;
   TConnect *gpxConnect;
EXEC SQL END DECLARE SECTION;

/******************************************************************************
 * Funciones Publicas                                                         *
 ******************************************************************************/

/*****************************************************************************
 * ++
 * Funcion      : sqlErrorHandler
 *
 * Descripcion  : Avisa de una condicion de error devuelto desde la base de
 *                datos.
 *
 * Parametros   : pszFile     : Nombre del fichero donde se ha producido el 
 *                              error.
 *                wLineNumber : Numero de linea donde se ha producido el error.
 * Retorno      : 
 *                
 * Historial :
 *
 * Autor                Fecha           Descripcion
 * ---------------------------------------------------------------------------
 *
 * --
 ****************************************************************************/
void sqlErrorHandler( char * pszFile, int wLineNumber ){
  char szErrorMessage [128];
  size_t wBufLen=255, wMsgLen;

  // Precondicion 
  assert( pszFile != NULL );

  wBufLen = sizeof( szErrorMessage );
  sqlglm( szErrorMessage, &wBufLen, &wMsgLen );
  szErrorMessage[wMsgLen]='\0';
  // reportLog ( pszFile, wLineNumber, "ERROR SQL: %s", szErrorMessage );
  printf ( "ERROR SQL: %s. EN %s, %d\n"
                          , szErrorMessage, pszFile, wLineNumber );
  if ( GetSQLStatus () != ORA_NOT_CONNECTED ) { 
     rollbackWork(); 
     disconnectDB();
   } 
  exit(RET_ERROR);
}

/*****************************************************************************
 * ++
 * Funcion      : sqlWarningHandler
 * 
 * Descripcion  : Avisa de una condicion de advertencia devuelta desde la base 
 *                de datos.
 *
 * Parametros   : pszFile     : Nombre del fichero donde se ha producido la
 *                              advertencia.
 *                wLineNumber : Numero de linea donde se ha producido la
 *                              advertencia.
 *
 * Retorno      : 
 *                
 * Historial :
 *
 * Autor                Fecha           Descripcion
 * ---------------------------------------------------------------------------
 *
 * --
 ****************************************************************************/
void sqlWarningHandler( char * pszFile, int wLineNumber ){
  char szErrorMessage [128];
  size_t wBufLen=255, wMsgLen;
  int wSqlCode;

  // Precondicion 
  assert( pszFile != NULL && wLineNumber >= 0 );

  wBufLen = sizeof( szErrorMessage );
  sqlglm( szErrorMessage, &wBufLen, &wMsgLen );
  szErrorMessage[wMsgLen]='\0';
  
  // reportLog ( __FILE__, __LINE__, "Advertencia SQL: %s", szErrorMessage );
  
} 

/*****************************************************************************
 * ++
 * Funcion      : connectDB
 *
 * Descripcion  : Abre una conexion con la base de datos. 
 *
 * Parametros   : 
 *             
 * Retorno      : Resultado de la creaci�n de la conexi�n.
 *                
 * Historial :
 *
 * Autor                Fecha           Descripcion
 * ---------------------------------------------------------------------------
 *
 * --
 ****************************************************************************/
int connectDB () {

  int wSQLStatus;

  TRACE ( fprintf( stdout, "sqlbase:connectDB\n" ); )

  assert ( gpxConnect );

  EXEC SQL WHENEVER SQLERROR DO sqlErrorHandler( __FILE__, __LINE__ );
  EXEC SQL WHENEVER SQLWARNING DO sqlWarningHandler( __FILE__, __LINE__ );
  
  TRACE ( fprintf( stdout, "Realizando Conexion. Usuario: %s. " \
                           "Base de datos: %s\n"
                         , gpxConnect->szUsername 
                         , gpxConnect->szDatabase); )

  /* Si la base de datos no fue especificada */
  if ( strlen( gpxConnect->szDatabase ) == 0 ){
    /* El valor por defecto de ORACLE_SID es usado */
    EXEC SQL CONNECT :gpxConnect->szUsername 
		 IDENTIFIED BY :gpxConnect->szPassword;
    TRACE ( fprintf( stdout, "sqlbase.pc:ConnectDB(): Connectado : %s\n"
		       , gpxConnect->szUsername ); )
  }else{
    /* Uso del TNS de base de datos como argumento */
    EXEC SQL
      CONNECT
        :gpxConnect->szUsername IDENTIFIED BY :gpxConnect->szPassword 
        USING :gpxConnect->szDatabase;
    TRACE( fprintf( stdout, "Connectado : %s@%s\n"
                          , gpxConnect->szUsername
		      , gpxConnect->szDatabase ); )
  }

  return GetSQLStatus();
}

/*****************************************************************************
 * ++
 * Funcion      : beginTrans
 *
 * Descripcion  : Inicia una transaccion con la base de datos. 
 *
 * Parametros   :
 *             
 * Retorno      : ORA_SUCCESS 
 *                
 * Historial :
 *
 * Autor                Fecha           Descripcion
 * ---------------------------------------------------------------------------
 *
 * --
 ****************************************************************************/
int beginTrans(void){
  EXEC SQL WHENEVER SQLERROR DO sqlErrorHandler( __FILE__, __LINE__ );
  EXEC SQL WHENEVER SQLWARNING DO sqlWarningHandler( __FILE__, __LINE__ );

  return ORA_SUCCESS;
}

/*****************************************************************************
 * ++
 * Funcion      : beginReadOnlyTrans
 *
 * Descripcion  : Inicia una transaccion de solo lectura con la base de datos.
 *
 * Parametros   :
 *             
 * Retorno      : Estado de la transaccion.
 *                
 * Historial :
 *
 * Autor                Fecha           Descripcion
 * ---------------------------------------------------------------------------
 *
 * --
 ****************************************************************************/
int beginReadOnlyTrans(void){
  EXEC SQL WHENEVER SQLERROR DO sqlErrorHandler( __FILE__, __LINE__ );
  EXEC SQL WHENEVER SQLWARNING DO sqlWarningHandler( __FILE__, __LINE__ );

  EXEC SQL SET TRANSACTION READ ONLY;
  return GetSQLStatus();
}

/*****************************************************************************
 * ++
 * Funcion      : commitWork
 *
 * Descripcion  : Realiza commit de base de datos. 
 *
 * Parametros   :
 *             
 * Retorno      : Resultado de la operaci�n. 
 *                
 * Historial :
 *
 * Autor                Fecha           Descripcion
 * ---------------------------------------------------------------------------
 *
 * --
 ****************************************************************************/
int commitWork(void){
  EXEC SQL WHENEVER SQLERROR DO sqlErrorHandler( __FILE__, __LINE__ );
  EXEC SQL WHENEVER SQLWARNING DO sqlWarningHandler( __FILE__, __LINE__ );
  
  EXEC SQL COMMIT WORK;
  return GetSQLStatus();
}

/*****************************************************************************
 * ++
 * Funcion      : rollbackWork
 *
 * Descripcion  : Realiza Rollback de base de datos. 
 *
 * Parametros   :
 *             
 * Retorno      : Resultado de la operacion. 
 *                
 * Historial :
 *
 * Autor                Fecha           Descripcion
 * ---------------------------------------------------------------------------
 *
 * --
 ****************************************************************************/
int rollbackWork(void){
  EXEC SQL WHENEVER SQLERROR DO sqlErrorHandler( __FILE__, __LINE__ );
  EXEC SQL WHENEVER SQLWARNING DO sqlWarningHandler( __FILE__, __LINE__ );

  EXEC SQL ROLLBACK WORK;
  return GetSQLStatus();
}

/*****************************************************************************
 * ++
 * Funcion      : disconnectDB
 *
 * Descripcion  : Realiza la desconexion de la base de datos.
 *
 * Parametros   :
 *             
 * Retorno      : Resultado de la operacion.
 *                
 * Historial :
 *
 * Autor                Fecha           Descripcion
 * ---------------------------------------------------------------------------
 *
 * --
 ****************************************************************************/
int disconnectDB(void){
  TRACE ( fprintf( stdout, "sqlbase.pc:disconnectDB()\n" ); )
  EXEC SQL WHENEVER SQLERROR DO sqlErrorHandler( __FILE__, __LINE__ );
  EXEC SQL WHENEVER SQLWARNING DO sqlWarningHandler( __FILE__, __LINE__ );

  EXEC SQL ROLLBACK WORK RELEASE;
  return GetSQLStatus();
}

/******************************************************************************
 * Funciones Privadas                                                         *
 ******************************************************************************/

#endif /* _SQLBASE_PC_ */
