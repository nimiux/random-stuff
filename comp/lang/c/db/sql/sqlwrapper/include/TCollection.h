#ifndef _TCOLLECTION_H_
#define _TCOLLECTION_H_

#include "TTypes.h"
#include "TIterator.h"
#include "TListNode.h"

template <class T>
class TCollection : public TIterator<T>
{
   protected:

      TListNode<T>* m_pxfirstnode;
      TListNode<T>* m_pxlastnode;
      TListNode<T>* m_pxcurrentnode;

   private:

      void init ();
      int remove ( TListNode<T> *pxnode );

   public:

      // Constructor & destructor

      TCollection();
      ~TCollection();

      // Member functions (public)

      // List of virtual functions of parent class to implement
      int insert (T *pxelement);
      int remove ();
      int flush ();
      T *getat ( int iindex );
      T *getfirst ();
      T *getprevious ();
      T *getcurrent ();
      T *getnext ();
      T *getlast ();
      
      bool isempty();
      bool hasmoreelements();
      bool infirst();
};

// Class members implementations --------------------------------------------

template <class T>
TCollection<T>::TCollection()
{
   init();
};

template <class T>
TCollection<T>::~TCollection()
{
   flush ();
};

template <class T>
void TCollection<T>::init() {

   m_pxfirstnode     = NULL;
   m_pxlastnode      = NULL;
   m_pxcurrentnode   = NULL;
}

template <class T>
int TCollection<T>::remove ( TListNode<T> *pxnode ) {
   if ( pxnode ) {
      if ( pxnode == m_pxfirstnode ) {
         if ( pxnode == m_pxlastnode ) {
            init();
         } else {
            m_pxfirstnode = pxnode->getnextnode();
            m_pxcurrentnode = m_pxfirstnode;
            m_pxcurrentnode->setpreviousnode ( NULL );
         }
      } else {
         if ( pxnode == m_pxlastnode ) {
            m_pxlastnode = pxnode->getpreviousnode();
            m_pxcurrentnode = m_pxlastnode;
            m_pxcurrentnode->setnextnode ( NULL );

         } else {
            m_pxcurrentnode = pxnode->getnextnode();
            pxnode->getpreviousnode()->setnextnode ( m_pxcurrentnode );
            m_pxcurrentnode->setpreviousnode ( pxnode->getpreviousnode() );
         }
      }
      m_inumelements--;
      delete pxnode;
   }
   return RET_SUCCESS;
}

template <class T>
int TCollection<T>::insert (T* pxelement)
{
   if ( m_pxfirstnode == NULL ) {
      m_pxfirstnode = new TListNode<T>( pxelement );
      m_pxlastnode = m_pxfirstnode;
      m_pxcurrentnode = m_pxfirstnode;
   } else {
      TListNode<T> *pxnodeaux = new TListNode<T> ( pxelement
                                                    , m_pxlastnode
                                                    , NULL );
      m_pxlastnode->setnextnode ( pxnodeaux );
      m_pxlastnode = pxnodeaux;
   }
   m_inumelements++;
   return RET_SUCCESS;
};

template <class T>
int TCollection<T>::remove ()
{
   remove ( m_pxcurrentnode );
   return RET_SUCCESS;
};

template <class T>
int TCollection<T>::flush ()
{
   getfirst();
   while ( !isempty() ) {
      remove();
   }
   return RET_SUCCESS;
};

template <class T>
T *TCollection<T>::getat ( int iindex )
{
   if ( ++iindex <= m_inumelements ) {
      getfirst();
      for ( int i = 1; i < iindex; i++ ) {
         getnext();
      }
      return m_pxcurrentnode->getcontent();
   } else {
      return NULL;
   }
};

template <class T>
T *TCollection<T>::getfirst () {
   if ( !isempty() ) {
      m_pxcurrentnode = m_pxfirstnode;
      return getcurrent ();
   }
   return NULL;
};

template <class T>
T* TCollection<T>::getprevious () {
   if ( ! isfirst() ) {
      m_pxcurrentnode = m_pxcurrentnode->getprevious();
      return getcurrent ();
   }
   return NULL;
};

template <class T>
T *TCollection<T>::getcurrent () {
   if ( m_pxcurrentnode ) {
      return m_pxcurrentnode->getcontent();
   }
   return NULL;
};

template <class T>
T *TCollection<T>::getnext () {
   if ( hasmoreelements() ) {
      m_pxcurrentnode = m_pxcurrentnode->getnextnode();
      return getcurrent ();
   }
   return NULL;
};

template <class T>
T *TCollection<T>::getlast () {
   m_pxcurrentnode = m_pxlastnode;
   return getcurrent ();
};

template <class T>
bool TCollection<T>::isempty()
{
   return ( m_pxfirstnode == NULL );
};

template <class T>
bool TCollection<T>::hasmoreelements () {
   return ( m_pxcurrentnode != m_pxlastnode );
};

template <class T>
bool TCollection<T>::infirst () {
   return ( m_pxcurrentnode == m_pxfirstnode );
};

#endif // _TCOLLECTION_H_
