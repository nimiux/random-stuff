#ifndef _TGENERICDBTABLE_H_
#define _TGENERICDBTABLE_H_

class TGenericDBTable {

   public:

      virtual int getcount() = 0;
      virtual int selectbyprimarykey() = 0;

};
#endif // _TGENERICDBTABLE_H_
