#ifndef _PERSONA_H_
#define _PERSONA_H_

class Persona {

   private:

      char m_szdni [10];
      char m_szname [11];
      char m_szsurname [11];
      char m_szrowid [19];

   private:
      void init (   char * pszdni
                  , char * pszname
                  , char * pszsurname
                  , char * pszrowid );

   public:

      Persona (  char * pszdni
               , char * pszname
               , char * pszsurname
               , char * pszrowid );
      Persona ( Persona *p);
      ~Persona ();


      char * getdni ();
      char * getname ();
      char * getsurname ();
      char * getrowid ();

      void setdni ( char * pszdni );
      void setname ( char * pszname );
      void setsurname ( char * pszsurname );
      void setrowid ( char * pszrowid );

      Persona *clone();
};
#endif // _PERSONA_H_
