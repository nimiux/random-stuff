#ifndef _TLISTNODE_H_
#define _TLISTNODE_H_

template <class T>
class TListNode
{
   protected:

      T* m_pxcontent;
      TListNode *m_pxpreviousnode;
      TListNode *m_pxnextnode;

   private:
      void init (   T* pxcontent
                  , TListNode *pxpreviousnode
                  , TListNode *pxnextnode);

   public:

      // Constructor & destructor

      TListNode (   T* pxcontent
                  , TListNode *pxpreviousnode
                  , TListNode *pxnextnode);
      TListNode (   T* pxcontent );
      ~TListNode();

      // Member functions (public)

      T *getcontent();
      TListNode *getpreviousnode();
      TListNode *getnextnode();

      void setcontent ( T *pxcontent);
      void setpreviousnode ( TListNode *pxnode);
      void setnextnode( TListNode *pxnode);

};

template <class T>
TListNode<T>::TListNode ( T* pxcontent
                        , TListNode *pxpreviousnode
                        , TListNode *pxnextnode )
{
   init ( pxcontent, pxpreviousnode, pxnextnode );
}

template <class T>
TListNode<T>::TListNode ( T* pxcontent )
{
   init ( pxcontent, NULL, NULL );
};

template <class T>
TListNode<T>::~TListNode ()
{
   delete m_pxcontent;
};

template <class T>
void TListNode<T>::init ( T* pxcontent
                   , TListNode *pxpreviousnode
                   , TListNode *pxnextnode )
{
   m_pxcontent = pxcontent->clone();
   m_pxnextnode = pxnextnode;
   m_pxpreviousnode = pxpreviousnode;
};


template <class T>
T *TListNode<T>::getcontent ()
{
   return m_pxcontent;
};

template <class T>
TListNode<T> *TListNode<T>::getpreviousnode ()
{
   return m_pxpreviousnode;
};

template <class T>
TListNode<T> *TListNode<T>::getnextnode ()
{
   return m_pxnextnode;
};

template <class T>
void TListNode<T>::setcontent ( T *pxcontent ) {
   m_pxcontent = pxcontent;
}

template <class T>
void TListNode<T>::setnextnode ( TListNode *pxnode ) {
   m_pxnextnode = pxnode;
}

template <class T>
void TListNode<T>::setpreviousnode ( TListNode *pxnode ) {
   m_pxpreviousnode = pxnode;
}

#endif // _TLISTNODE_H_
