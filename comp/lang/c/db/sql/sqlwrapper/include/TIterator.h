#ifndef _TITERATOR_H_
#define _TITERATOR_H_

template <class T>
class TIterator {
   protected:
      int m_inumelements;

   public:
      TIterator();
      ~TIterator();
      int getcount();
};

template <class T>
TIterator<T>::TIterator() {
   m_inumelements = 0;
}

template <class T>
TIterator<T>::~TIterator() {

}

template <class T>
int TIterator<T>::getcount() {
   return m_inumelements;
}


#endif // _TITERATOR_H_
