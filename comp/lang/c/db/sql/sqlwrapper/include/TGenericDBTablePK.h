#ifndef _TGENERICTABLEPK_H_
#define _TGENERICTABLEPK_H_

class TGenericDBTablePK {

   public:

      virtual int drop() = 0;

};
#endif //_TGENERICTABLEPK_H_
