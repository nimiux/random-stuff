/******************************************************************************
 * ++
 * Proyecto     : 
 *
 * Autor        : Jose Maria Alonso. 
 *
 * Modulo       : sqlbase.h
 *
 * Descripcion  : 
 *
 * Historal :
 *
 * Autor                Fecha           Descripcion
 * ----------------     --------------  ------------------------------------
 *
 * --
 ******************************************************************************/
#ifndef _SQLBASE_H_
#define _SQLBASE_H_

/******************************************************************************
 * Ajuste a los Estandares                                                    *
 ******************************************************************************/

/******************************************************************************
 * Includes de Sistema                                                        *
 ******************************************************************************/

/******************************************************************************
 * Includes de Aplicacion                                                     *
 ******************************************************************************/

/******************************************************************************
 * Opciones de Debug y Traza                                                  *
 ******************************************************************************/

/******************************************************************************
 * Macros                                                                     *
 ******************************************************************************/

#define ROWID_LEN                        18

#define MAX_HOST_ARRAY                  100

#define GetSQLStatus()  sqlca.sqlcode

#define ORA_SUCCESS              0
#define ORA_ERROR                1
#define ORA_LOST                 2
#define ORA_NOT_FOUND         1403
#define ORA_NOT_CONNECTED    -1012

#define USERNAME_LEN 20
#define PASSWORD_LEN 20
#define DATABASE_LEN 50

/******************************************************************************
 * Tipos                                                                      *
 ******************************************************************************/

/* Tipos necesarios para acceso a base de datos */

EXEC SQL BEGIN DECLARE SECTION;
   struct TConnect{
      char szUsername[USERNAME_LEN];
      char szPassword[PASSWORD_LEN];
      char szDatabase[DATABASE_LEN];
   };

   typedef struct TConnect TConnect;

   struct st_rowid {
   	char szRowId         [ROWID_LEN + 1 ];
   };

   typedef struct st_rowid ST_RowId;

EXEC SQL END DECLARE SECTION;

/******************************************************************************
 * Variables Globales                                                         *
 ******************************************************************************/

/******************************************************************************
 * Variables y funciones externas.                                            *
 ******************************************************************************/

/******************************************************************************
 * Prototipos de Funciones Publicas                                           *
 ******************************************************************************/
void sqlErrorHandler( char * pszFile, int wLineNumber );
void sqlWarningHandler( char * pszFile, int wLineNumber );
int connectDB();
int beginTrans(void);
int beginReadOnlyTrans(void);
int commitWork(void);
int rollbackWork(void);
int disconnectDB(void);
/******************************************************************************
 * Prototipos de Funciones Privadas                                           *
 ******************************************************************************/

#endif /* _SQLBASE_H_ */
