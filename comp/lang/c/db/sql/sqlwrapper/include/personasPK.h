#ifndef _PERSONASPK_H_
#define _PERSONASPK_H_

#include "TGenericDBTablePK.h"

class PersonasPK : public TGenericDBTablePK {

   private:
      char m_szdni [10];

   private:
      void destroy();
      void init ( char *pszdni );

   public:
      PersonasPK ();
      PersonasPK ( char *pszdni );
      PersonasPK ( PersonasPK *pxpersonapk );
      ~PersonasPK ();

		char *getdni();
      void setdni( char *pszdni );

      int drop();
      
		PersonasPK *clone();
};
#endif // _PERSONASPK_H_
