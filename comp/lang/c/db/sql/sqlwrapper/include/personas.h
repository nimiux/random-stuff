#ifndef _PERSONAS_H_
#define _PERSONAS_H_

#include "TCollection.h"
#include "TGenericDBTable.h"
#include "personasPK.h"
#include "persona.h"

EXEC SQL INCLUDE sqlbase.h;

#define PERSONAS_DNI_LEN     10
#define PERSONAS_NAME_LEN    20
#define PERSONAS_SURNAME_LEN 20

#define TRACE(x) x

EXEC SQL BEGIN DECLARE SECTION;

   struct personas_row {
      char szdni        [PERSONAS_DNI_LEN + 1];
      char szname       [PERSONAS_NAME_LEN + 1];
      char szsurname    [PERSONAS_SURNAME_LEN + 1];
   };

   typedef struct personas_row PersonasRow;

   struct personas_ind {
      short iszdni;
      short iszname;
      short iszsurname;
   };

   typedef struct personas_ind PersonasInd;

   struct dbpersonas {
      PersonasRow   rgxRow   [MAX_HOST_ARRAY];
      PersonasInd   rgxInd   [MAX_HOST_ARRAY];
      ST_RowId      rgxRowId [MAX_HOST_ARRAY];
   };
   
   typedef struct dbpersonas DBPersonas;

EXEC SQL END DECLARE SECTION;

typedef TCollection<Persona> PersonaColl;

class Personas : public TGenericDBTable {

   private:
      PersonaColl *m_pxpersonasdata;
      PersonasPK  *m_pxpersonaspk;

      EXEC SQL BEGIN DECLARE SECTION;
         // Miembros necesarios para el acceso mediante Host Arrays
         DBPersonas    m_xpersonas;
         PersonasRow   *m_pxpersonasrow;
         PersonasInd   *m_pxpersonasind;
         ST_RowId      *m_pxpersonasrowid;

         int m_wblocksize;
      EXEC SQL END DECLARE SECTION;

   private:
      void destroy();
      void init ();

   public:
      Personas ();
      ~Personas ();

      void setprimarykey ( PersonasPK *pxpersonapk ); 

      void add ( Persona *pxpersona );
      int getcount();
      Persona *getat( int iindex );

      int selectbyprimarykey();
};
#endif // _PERSONAS_H_
