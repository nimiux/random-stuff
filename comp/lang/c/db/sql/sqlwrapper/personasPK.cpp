#include <string.h>
#include "personasPK.h"

PersonasPK::PersonasPK ( char *pszdni ) {
   init( pszdni );
}

PersonasPK::PersonasPK () {
   init( "" );
}

PersonasPK::PersonasPK ( PersonasPK *pxpersonapk ) {
   init ( pxpersonapk->getdni() ); 
}

PersonasPK ::~PersonasPK () {
   destroy();
}

void PersonasPK::init ( char *pszdni ) {
   setdni ( pszdni );
}

void PersonasPK::destroy() {

}

char *PersonasPK::getdni() {
   return m_szdni;
}

void PersonasPK::setdni( char *pszdni ) {
   strcpy ( m_szdni, pszdni );
}

PersonasPK *PersonasPK::clone() {
   return new PersonasPK ( getdni() );
}

int PersonasPK::drop() {

}

