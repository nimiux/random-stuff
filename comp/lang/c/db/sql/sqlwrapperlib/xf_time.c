/******************************************************************************
 * ++
 * Author       : American Management Systems
 *
 * Module Name  : xf_time.c
 *
 * Description  :
 *
 * Status Diagrams :
 *
 * Mod. History :
 *
 * AUTHOR               DATE            DESCRIPTION
 * ----------------     --------------  ------------------------------------
 * Mikel Madinabeitia    
 *
 * --
 ******************************************************************************/
#ifndef TIME_UTIL_C
#define TIME_UTIL_C

#define _POSIX_SOURCE
#define _OSF_SOURCE

/******************************************************************************
 * Standards and Complance                                                    *
 ******************************************************************************/
#include <unistd.h>

/******************************************************************************
 * System Include Files                                                       *
 ******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <assert.h>
#include <errno.h>
#include <ctype.h>

/******************************************************************************
 * Application Include Files                                                  *
 ******************************************************************************/
#include "xf_time.h"
#include "xf_error.h"

/******************************************************************************
 * Debug and Trace Options                                                    *
 ******************************************************************************/
#ifdef DEBUG_XF_TIME
#define TRACE(x) x
#else
#define TRACE(x)
#endif

/******************************************************************************
 * Macro Definitions                                                          *
 ******************************************************************************/
#define FALSE 0
#define TRUE  1

#define TIME_LENGTH    6
#define DATE_LENGTH    8
#define DAY_SECONDS    86400

/******************************************************************************
 * Type Definitions                                                           *
 ******************************************************************************/

/******************************************************************************
 * Global Variables                                                           *
 ******************************************************************************/

/******************************************************************************
 * Local Function Prototypes                                                  *
 ******************************************************************************/


/*****************************************************************************
 * ++
 * Functionname : XF_GetDate
 *
 * Description  : 
 *
 * Parameters   : 
 *
 * Global Variables
 *   Accessed   : none
 *   Modified   : none
 *
 * Returncode   : TRUE
 *                FALSE
 *
 * Modification History :
 *
 * Author               Date            Description
 * ---------------------------------------------------------------------------
 * Mikel Madinabeitia
 *
 * --
 ****************************************************************************/
long XF_GetDate(void)
{
    time_t  now;
    struct tm *pszDate;
    char rchDate[10];

    now = time((time_t *) NULL);
    pszDate = localtime(&now);

   /* Format Date and return the value  */
    strftime( rchDate,9 + 1,"%Y%m%d", pszDate);
    return (atol(rchDate));
}

/*****************************************************************************
 * ++
 * Functionname : XF_GetTime
 *
 * Description  : This function is checking if the passed string is a valid
 *                date. This function is expecting the string in the format
 *                "YYMMDD" or "YYYYMMDD"
 *
 * Parameters   : pszDate       pointer to the date string
 *
 * Global Variables
 *   Accessed   : none
 *   Modified   : none
 *
 * Returncode   : TRUE
 *                FALSE
 *
 * Modification History :
 *
 * Author               Date            Description
 * ---------------------------------------------------------------------------
 * Mikel Madinabeitia
 *
 * --
 ****************************************************************************/
long XF_GetTime(void)
{
    time_t  now;
    struct tm *pszTime;
    char rchTime[10];

    now = time((time_t *) NULL);
    pszTime = localtime(&now);

   /* Format Date and return the value  */
    strftime( rchTime,9 + 1,"%H%M%S", pszTime);
    return (atol(rchTime));
}

/*****************************************************************************
 * ++
 * Functionname : XF_IsTimeStr
 *
 * Description  : 
 *
 * Parameters   : pszTime       pointer to the time string
 *
 * Global Variables
 *   Accessed   : none
 *   Modified   : none
 *
 * Returncode   : 
 *                FALSE
 *
 * Modification History :
 *
 * Author               Date            Description
 * ---------------------------------------------------------------------------
 * Enrique Riesgo       2001-08-07
 *
 * --
 ****************************************************************************/
int  XF_IsTimeStr(char * pszTime)
{
   short        wIndex;
   char         rgchHelp[3];    /* this variable contains
                                   the HH, MM, SS */
   int          wHelp;          /* the HH, MM, SS as numeric */

   memset(rgchHelp,'\0',3);
   if (strlen(pszTime) != TIME_LENGTH)
      return(FALSE);
  
   /* check if each byte is an digit */
   for (wIndex=0;wIndex<=TIME_LENGTH;wIndex++)
      if (isalpha(pszTime[wIndex]))
         return(FALSE);
  
   /* check the hours */
 /* Let  us copy the hours in the array and check it  */
   memcpy(rgchHelp, pszTime, 2);       
   wHelp = atoi(rgchHelp);
   if ((wHelp < 0) || (wHelp > 24))
      return(FALSE);                  
  
   /* check the minutes */
 /* Let  us copy the minutes in the array and check it  */
   memcpy(rgchHelp, &pszTime[2], 2);  
   wHelp = atoi(rgchHelp);
   if ((wHelp < 0) || (wHelp > 59))
      return(FALSE);                 
  
   /* check the seconds */
 /* Let  us copy the secondss in the array and check it  */
   memcpy(rgchHelp, &pszTime[4], 2);
   wHelp = atoi(rgchHelp);
   if ((wHelp < 0) || (wHelp > 59))
      return(FALSE);               
  
   return(TRUE);
}

/*****************************************************************************
 * ++
 * Functionname : XF_IsTimeInt
 *
 * Description  : 
 *
 * Parameters   : wTime         time integer
 *
 * Global Variables
 *   Accessed   : none
 *   Modified   : none
 *
 * Returncode   : 
 *                FALSE
 *
 * Modification History :
 *
 * Author               Date            Description
 * ---------------------------------------------------------------------------
 * Enrique Riesgo       2001-08-07
 *
 * --
 ****************************************************************************/
int  XF_IsTimeInt(int wTime )
{
  int wTemp;
  
  /* Check sign */
  if ( wTime < 0 ) return FALSE;

  /* Check hour */
  wTemp = wTime / 10000;
  if ( wTemp > 23 ) return FALSE;

  /* Check minutes */
  wTemp = ( wTime % 10000 ) / 100;
  if ( wTemp > 59 ) return FALSE;

  /* Check seconds */
  wTemp = wTime % 100;
  if ( wTemp > 59 ) return FALSE;

  return TRUE;
}

/*****************************************************************************
 * ++
 * Functionname : IsDateStr()
 *
 * Description  : This function is checking if the passed string is a valid
 *                date. This function is expecting the string in the format
 *                "YYMMDD" or "YYYYMMDD"
 *
 * Parameters   : pszDate       pointer to the date string
 *
 * Global Variables
 *   Accessed   : none
 *   Modified   : none
 *
 * Returncode   : TRUE
 *                FALSE
 *
 * Modification History :
 *
 * Author               Date            Description
 * ---------------------------------------------------------------------------
 * Enrique Riesgo       2001-08-07
 *
 * --
 ****************************************************************************/
int XF_IsDateStr(char * pszDate)
{
   short wIndex;
   char  rgchHelp[DATE_LENGTH];     /* this variable contains
                                      the YY, MM, DD */
   int   wCenturyOffset;
   int   wYear;          /* the year numeric */
   int   wMonth;         /* the month numeric */
   int   wDay;           /* the day numeric */
   int   wLen;
   
   int   rwMaxDay[12] =
      {31, 28, 31, 30, 31, 30,
       31, 31, 30, 31, 30, 31}; /* the maximum days per month */

   wLen = strlen(pszDate);
  
   /* check if the length of the date can include the century or not */
   if ((wLen != DATE_LENGTH) && (wLen != DATE_LENGTH - 2))
      return(FALSE);
  
   /* date format with century */
   if (wLen == DATE_LENGTH)
      wCenturyOffset = 2;
   else
      wCenturyOffset = 0;
  
   /* check if each byte is an digit */
   for (wIndex=0; wIndex <= wLen; wIndex++)
      if (isalpha(pszDate[wIndex]))
         return(FALSE);
  
   /* copy the year */
   memset(rgchHelp,'\0',DATE_LENGTH);
   memcpy(rgchHelp, pszDate, 2 + wCenturyOffset);
   wYear = atoi(rgchHelp);
  
   /* check the year */
   if (wYear < 0)
      return(FALSE);                    /* invalid year */
  
   /* copy the month */
   memset(rgchHelp,'\0',DATE_LENGTH);
   memcpy(rgchHelp, &pszDate[wCenturyOffset+2], 2);
   wMonth = atoi(rgchHelp);
  
   /* check the month */
   if ((wMonth < 1) || (wMonth > 12))
      return(FALSE);                    /* invalid month */
  
   /* copy the days */
   memset(rgchHelp,'\0',DATE_LENGTH);
   memcpy(rgchHelp, &pszDate[wCenturyOffset+4], 2);
   wDay = atoi(rgchHelp);
  
   /* check the day */
   if ((wDay < 1) || (wDay > 31))
      return(FALSE);                    /* invalid days */
  
   /* February ? */
   if (wMonth == 2)
   {
      /* do we don't have centuries */
      if (wCenturyOffset == 0)
      {
         /* check if the year is between 94 and 99. Then I asume that we are
            in the 20th Century */
         if ((wYear >= 60) && (wYear <= 99))
            wYear += 1900;
         else
            wYear += 2000;      /* 21st Century */
      } /* no centuries in the year */

      /* if leap-over year */
      if ((wYear % 4) == 0)
         rwMaxDay[wMonth-1] = 29;       /* set the days for the leap year of
                                           February */
   } /* Feburary */
  
   if (wDay > rwMaxDay[wMonth-1])       /* invalid day for the current month */
      return(FALSE);
  
   return(TRUE);
}

/*****************************************************************************
 * ++
 * Functionname : IsDateInt()
 *
 * Description  : This function is checking if the passed integer is a valid
 *                date. This function is expecting the number in the format
 *                YYYYMMDD
 *
 * Parameters   : wDate       date number
 *
 * Global Variables
 *   Accessed   : none
 *   Modified   : none
 *
 * Returncode   : TRUE
 *                FALSE
 *
 * Modification History :
 *
 * Author               Date            Description
 * ---------------------------------------------------------------------------
 * Enrique Riesgo       2001-08-07
 *
 * --
 ****************************************************************************/
int XF_IsDateInt(int wDate )
{
  int wYear;
  int wMonth;
  int wDay;

   int   rwMaxDay[12] =
      {31, 28, 31, 30, 31, 30,
       31, 31, 30, 31, 30, 31}; /* the maximum days per month */

   if ( wDate < 0 ) return FALSE;

   wYear  = wDate / 10000;
   wMonth = ( wDate % 10000 ) / 100;
   wDay   = wDate % 100;

   /* Check the year */
   if ( wYear < 1900 ) return FALSE;

   /* Check the month */
   if ( wMonth < 1 || wMonth > 12 ) return FALSE;

   /* Check the day */
   if ( wDay < 1 || wDay > 31 ) return FALSE;

   /* February ? */
   if ( wMonth == 2 ){
     if ( ( wYear % 4 ) == 0 && wYear % 400 )
       rwMaxDay[wMonth-1] = 29;       /* set the days for the leap year of
					 february */
   }
   
   /* Check day and month */
   if ( wDay > rwMaxDay[ wMonth - 1 ] ) return FALSE;

   return TRUE;
}

/*****************************************************************************
 * ++
 * Functionname : XF_GetWeekDayStr
 *
 * Description  : 
 *
 * Parameters   : pszDate       pointer to the date string
 *
 * Global Variables
 *   Accessed   : none
 *   Modified   : none
 *
 * Returncode   : TRUE
 *                FALSE
 *
 * Modification History :
 *
 * Author               Date            Description
 * ---------------------------------------------------------------------------
 * Enrique Riesgo
 *
 * --
 ****************************************************************************/
int XF_GetWeekDayStr(char *szDate)
{
  int wDate;
  wDate = atoi(szDate);
  if ( wDate == 0 && errno != 0 ) return XF_EXIT_ERROR;
  return XF_GetWeekDayInt( wDate );
}


/*****************************************************************************
 * ++
 * Functionname : XF_GetWeekDayInt
 *
 * Description  : 
 *
 * Parameters   : 
 *
 * Global Variables
 *   Accessed   : none
 *   Modified   : none
 *
 * Returncode   : 
 *
 * Modification History :
 *
 * Author               Date            Description
 * ---------------------------------------------------------------------------
 * Enrique Riesgo
 * 
 * --
 ****************************************************************************/
int XF_GetWeekDayInt( int wDate )
{
 struct tm xDate;

 /* Precondition */
 if ( wDate <= 0 ) return XF_EXIT_ERROR;

/* Adjust time saving */
 xDate.tm_isdst=-1; 

 /* Convert Date to numbers */
 xDate.tm_hour=xDate.tm_sec=xDate.tm_min=0;
 xDate.tm_mday=wDate % 100;
 xDate.tm_mon= ( wDate / 100 ) % 100 - 1;
 xDate.tm_year=wDate / 10000 - 1900;

 /* calculate Time and give back weekday */
 mktime(&xDate);

 return xDate.tm_wday;

}

#endif /* UTIL_C */
