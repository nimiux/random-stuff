/******************************************************************************
 * ++
 * Author       : American Management Systems
 *
 * Module Name  : xf_log.c
 *
 * Description  :
 *
 * Status Diagrams :
 *
 * Mod. History :
 *
 * AUTHOR               DATE            DESCRIPTION
 * ----------------     --------------  ------------------------------------
 * Enrique Riesgo
 *
 * --
 ******************************************************************************/

#define _GNU_SOURCE
#define _OSF_SOURCE

/******************************************************************************
 * Standards and Complance                                                    *
 ******************************************************************************/

/******************************************************************************
 * System Include Files                                                       *
 ******************************************************************************/
#include <unistd.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <fcntl.h>

/******************************************************************************
 * Application Include Files                                                  *
 ******************************************************************************/
#include "xf_log.h"
#include "xf_log_const.h"

/******************************************************************************
 * Debug and Trace Options                                                    *
 ******************************************************************************/
#ifdef TRACE_LOG
#define TRACE(x) x
#else
#define TRACE(x)
#endif

#define LOG_MESSAGE_SIZE 100
#define FIN_MESSAGE_SIZE 200

XF_RTRIM( str, len ) { int i; for( i=len-1; i>=0 && (((char*)str)[i]==' '||((char*)str)[i]=='\n');i--) ((char*)str)[i]=0; }


/******************************************************************************
 * Macro Definitions                                                          *
 ******************************************************************************/

/******************************************************************************
 * Type Definitions                                                           *
 ******************************************************************************/

/******************************************************************************
 * Global Variables                                                           *
 ******************************************************************************/

/******************************************************************************
 * Local Function Prototypes                                                  *
 ******************************************************************************/
char * GetDate( char * pszBuffer, int wLength );

/******************************************************************************
 * External Function Prototypes                                               *
 ******************************************************************************/

/*****************************************************************************
 * ++
 * Functionname : ReportLog
 *
 * Description  : 
 *
 * Parameters   :
 *
 * Global Variables
 *   Accessed   :  
 *
 *   Modified   :
 *
 * Returncode   : 
 *                
 * Modification History :
 *
 * Author               Date          Description
 * ------------------   -----------   -----------------------------------------
 * Enrique Riesgo
 *
 * --
 ****************************************************************************/
void ReportLog( char * pszLogFile, char * pszErrorFile, int wErrorLine,
		char * pszFormat, va_list * xArgs ){
  int fdLog;
  static char pszMessage      [LOG_MESSAGE_SIZE];
  static char pszFinalMessage [FIN_MESSAGE_SIZE];
  static char pszDateBuffer   [20];
  
  TRACE( fprintf( stderr, "xf_log.c: ReportLog: %s\n", pszFormat ) );

  /* Formating final message to be printed */
  if ( xArgs != NULL )
    vsprintf( pszMessage, pszFormat, *xArgs );
  else
    strcpy( pszMessage, pszFormat );

  TRACE( fprintf( stderr, "xf_log.c: ReportLog: Message = '%s'\n", pszMessage ) );

  XF_RTRIM( pszMessage, strlen( pszMessage ) );
  sprintf( pszFinalMessage, "%s %s[%d] (%d:%s) %s\n", 
	   GetDate( pszDateBuffer, 20 ), "pasarela_smsc", getpid(),
	   wErrorLine, pszErrorFile, pszMessage );

  /* Open file */
  if ( -1 == ( fdLog = open( pszLogFile, O_WRONLY | O_APPEND | O_CREAT | O_SYNC, 0644 ) ) ){
    perror( "Opening log file" );
    return;
  }
  /* Lock and write the file */
  lockf( fdLog, F_LOCK, 0 );
  write( fdLog, pszFinalMessage, strlen( pszFinalMessage ) );
  lockf( fdLog, F_ULOCK, 0 );

  if ( -1 == close( fdLog ) ){
    perror( "Closing log file" );
    return;
  }
}

/*****************************************************************************
 * ++
 * Functionname : ReportLog
 *
 * Description  : 
 *
 * Parameters   :
 *
 * Global Variables
 *   Accessed   :  
 *
 *   Modified   :
 *
 * Returncode   : 
 *                
 * Modification History :
 *
 * Author               Date          Description
 * ------------------   -----------   -----------------------------------------
 * Enrique Riesgo
 *
 * --
 ****************************************************************************/
char * GetDate( char * pszBuffer, int wLength ){
  time_t xSystemDate;
  const struct tm *xNow;
  
  xSystemDate = time(NULL);
  xNow = localtime( &xSystemDate );
  
  strftime( pszBuffer, wLength, "%Y-%m-%d %T", xNow );

  return pszBuffer;
}
