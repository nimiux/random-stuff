//Generic ordered index of elements
//Author: Carlos Alberti Villanueva: 12-9-97

#ifndef __geindex_h
#define __geindex_h

#include "gedef.h"

// Definitions -------------------------------------------------------------

// #define LISTSIZE      32 // Size of the list of results
// #define MORELISTSIZE 16      // More size to increase size of list

// This class is parent of the rest: BRTree, Tree, OrderedList, Skip List
// Class Definition --------------------------------------------------------

template <class T>
class TGenericIndex{

        protected:
                
                int NumElems;           // No of elements in the index
                int Order;                      // Type of ordenation of this index (up or down)
                int ListSize;           // Size of the list to return elements
        
        public:

                // Constructor & destructor

                TGenericIndex ();
                ~TGenericIndex ();

        // Member functions

                // Define the order of sorting of an index
                inline void SetOrder (int ord) {Order = (ord==order_down) ? order_down : order_up;};

                // Return the number of elements in the index
                inline int GetCount () {return NumElems;};
                
                // Insert one element in the index based in 'value'
                // Return the number of elements or a Ret_Code on error
                virtual int Insert (long value, T* element) = 0;

                // Delete the element from the index
                // Return the number of elements or a Ret_Code on error
                virtual int Delete (long value, T* element, bool fr = false) = 0;

                // flush all the elements in the index
                // Return a Ret_Code
                virtual int Flush (bool fr = false) = 0;

                // Find the T* of the elements in the index with this value
                // Return the size of the list of elements with this 'value'
                virtual int FindSize (long value) = 0;
                // Fill the list of elements with this 'value'
                virtual int Find (long value, OUT T** list) = 0;

                // Give the number of elements in the index less or equal than this value
                virtual int LessToSize (long value) = 0;
                // Fill the list of elements less or equal than 'value'
                virtual int LessTo (long value, OUT T** list) = 0;

                // Give the number of elements in the index less or equal than this value
                virtual int GreaterToSize (long value) = 0;
                // Fill the list of elements less or equal than 'value'
                virtual int GreaterTo (long value, OUT T** list) = 0;
};

// Class Implementation ------------------------------------------------------

template <class T>
TGenericIndex<T>::TGenericIndex ()
{
        Order = order_up;
        NumElems = 0;
};
// ---------------------------------------------------------------------------
template <class T>
TGenericIndex<T>::~TGenericIndex ()
{

};


#endif