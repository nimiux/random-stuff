#include "georlist.h"
// Class members implementations --------------------------------------------

template <class T>
TGenericOrderedList<T>::TGenericOrderedList (int IniSize, int IniMoreSize)
{
        NumElems = 0;
        Size = IniSize;
        MoreSize = IniMoreSize;
        LastElement = -1;
        Elements = (T**) malloc (Size * sizeof (T*)); 
        Values = (long*) malloc (Size * sizeof (long));
};
//---------------------------------------------------------------------------
template <class T>
TGenericOrderedList<T>::~TGenericOrderedList()
{
        Flush ();
        
        // delete the elements
        free (Values);
        free (Elements);
};
//---------------------------------------------------------------------------
template <class T>
bool TGenericOrderedList<T>::IsMinor (int number1, int number2)
// Returns TRUE if val in element #number1 is minor than the value in element #number2
{
        if (Order == order_up)
                return (Values[number1] < Values[number2]);
        else
                return (Values[number1] > Values[number2]);
};
//---------------------------------------------------------------------------
template <class T>
bool TGenericOrderedList<T>::IsMinor (long val, int number)
// Returns TRUE if 'val' is minor than the value of the element in #number
{
        if (Order == order_up)
                return  (val < Values[number]);
        else
                return (val > Values[number]);
};
//---------------------------------------------------------------------------
template <class T>
bool TGenericOrderedList<T>::IsMinor (int number, long val)
// Returns TRUE if the value of the element in #number is minor than 'val'
{
        if (Order == order_up)
                return  (Values[number] < val);
        else
                return (Values[number] > val);
};
//---------------------------------------------------------------------------
template <class T>
long TGenericOrderedList<T>::GetValue (int number)
// Gives the value of the element in order 'number'
{
        if (number >= NumElems || number < 0)
                return ret_no_elem;
        else
                return Values[number];
};
//---------------------------------------------------------------------------
template <class T>
T* TGenericOrderedList<T>::GetElement (int number)
// Gives the pointer of the element in order 'number'
{
        if ((number >= NumElems) || (number < 0))
                return NULL;
        else
                return Elements[number];
};
//---------------------------------------------------------------------------
template <class T>
T* TGenericOrderedList<T>::FindElement (long value)
// Gives the pointer of the element identified by 'value'
{
   int nPosition = FindPosition(value);
   if( Values[nPosition] == value )
      return GetElement(nPosition);
   else
      return NULL;
};
//---------------------------------------------------------------------------
template <class T>
int TGenericOrderedList<T>::FindPosition (long value)
// Search the place of the first element with that value (or next if the value doesn't exist)
// Binary Search
{
        int sup = NumElems, inf = 0;
        int pivot = (sup+inf) >> 1;

        while (sup != inf) {
                if (IsMinor (pivot, value)) { // Must search in the right
                        if (inf == (sup-1))
                                pivot = inf = sup;
                        else {
                                inf = pivot;
                                pivot = (sup+inf) >> 1;
                        }
                } else { // Must search in the left
                        sup = pivot;
                        pivot = (sup+inf) >> 1;
                }
        }
        return pivot;
};
//---------------------------------------------------------------------------
template <class T>
int TGenericOrderedList<T>::Insert (long value, T* element)
// Insert one element in the list
{
        int retval, pos, i = NumElems;

        if ((retval = CheckRealloc ()) == ret_ok) {

                if (LastElement >= 0 && LastElement < NumElems && Values[LastElement] == value)
                        pos = LastElement;
                else
                        pos = FindPosition (value);

                while (i > pos) {
                        Values[i] = Values[i-1];
                        Elements[i] = Elements[i-1];
                        i--;
                }
                Values[pos] = value;
                Elements[pos] = element;
                LastElement = pos;
                NumElems ++;
        }
        return retval;
};
//---------------------------------------------------------------------------
template <class T>
int TGenericOrderedList<T>::Delete (long value, T* element, bool fr)
// Delete one element from the list if exists
{
        int retval = ret_ok, i;

        i = FindPosition (value);

        if (i < NumElems) {

                while ((Values[i] == value) && (Elements[i] != element))
                        i++;
                        
                if (Elements[i] == element && Values[i] == value) { // Found element

                        if (fr) delete element; // To free memory if the caller wants

                        if (LastElement == i) {
                                if (NumElems == 1)
                                        LastElement = -1;
                                else
                                        LastElement = 0;
                        };
                                
                        while (i < NumElems-1) {
                                Values[i] = Values[i+1];
                                Elements[i] = Elements[i+1];
                                i++;
                        }
                        NumElems --;
                        CheckRealloc();

                } else
                        retval = ret_no_elem;
        } else
                retval = ret_no_elem;

        return retval;
};
//---------------------------------------------------------------------------
template <class T>
int TGenericOrderedList<T>::Flush (bool fr)
// Free all the elements in the list
{

        if (fr) {
                int i = 0;
                while (i < NumElems)
                        delete Elements[i++]; // To free memory if the caller wants
        }

        NumElems = 0;
        Size = 1;
        LastElement = -1;

        Elements = (T **) realloc (Elements, Size * sizeof (T*)); 
        Values = (long *) realloc (Values , Size * sizeof (long));

        return ret_ok;
};
//---------------------------------------------------------------------------
template <class T>
int TGenericOrderedList<T>::FindSize (long value)
// Return the size for the list of elements with that value
{
        int cont = 0;
        int pos = FindPosition (value);

        while (pos < NumElems && Values[pos] == value) {
                cont ++;
                pos ++;
        }
        return cont;
};
//---------------------------------------------------------------------------
template <class T>
int TGenericOrderedList<T>::Find (long value, OUT T** list)
// Fill the list with the elements with this value
{
        int cont = 0;
        int pos = FindPosition (value);

        while (pos < NumElems && Values[pos] == value) {
                list[cont ++] = Elements[pos ++];
        }
        return cont;
}
//---------------------------------------------------------------------------
template <class T>
int TGenericOrderedList<T>::LessToSize (long value)
// Return the size for the list of elements with that value or less
{
        int cont = 0;

        while (cont < NumElems && (IsMinor (cont, value) || Values[cont] == value))
                cont ++;

        return cont;
};
//---------------------------------------------------------------------------
template <class T>
int TGenericOrderedList<T>::LessTo (long value, OUT T** list)
// Fill the list of elements with value less or equal to given
{
        int cont = 0;

        while (cont < NumElems && (IsMinor (cont, value) || Values[cont] == value))
                list[cont] = Elements [cont ++];

        return cont;
};
//---------------------------------------------------------------------------
template <class T>
int TGenericOrderedList<T>::GreaterToSize (long value)
// Return the size for the list of elements with that value or greater
{
        int cont = 0;
        int pos = FindPosition (value);

        while (pos < NumElems) {
                pos ++;
                cont ++;
        }
        return cont;
};
//---------------------------------------------------------------------------
template <class T>
int TGenericOrderedList<T>::GreaterTo (long value, OUT T** list)
// Fill the list of elements with value greater or equal to given
{
        int cont = 0;
        int pos = FindPosition (value);

        while (pos < NumElems) {
                list[cont++] = Elements[pos++];
        }
        return cont;
};
//---------------------------------------------------------------------------
template <class T>
int TGenericOrderedList<T>::CheckRealloc()
{
// perform reallocation if necessary (protected function)

        int ret = ret_ok;
        
        if (NumElems >= Size) {         // list full, allocate more space

                if (MoreSize > 0) {
                
                        T** telements;
                        long* tvalues;
                        
                        Size += MoreSize; // new size

                        if (telements = (T**) realloc (Elements, Size * sizeof(T*))) {
                                //first reallocation succesful
                                Elements = telements;
                                
                                if (tvalues = (long*) realloc (Values, Size * sizeof(long))) {
                                        // second reallocation succesful
                                        Values = tvalues;
                                
                                } else {                                
                                        // second reallocation failed
                                        Size -= MoreSize;
                                        Elements = (T**) realloc (Elements, Size * sizeof(T*));
                                        // There can�t be any problem for this reallocation with less elements than before
                                        ret = ret_out_of_mem;
                                }
                        } else {
                                // first reallocation failed
                                Size -= MoreSize;
                                ret = ret_out_of_mem;                   
                        }
                } else
                        ret = ret_fixed_size;

        } else if (NumElems < (Size-MoreSize)) {        // Free a block of memory from the list

                Size -= MoreSize;
                Elements = (T**) realloc (Elements, Size * sizeof(T*));
                Values = (long*) realloc (Values, Size * sizeof(long));
        }
        return ret;
};

