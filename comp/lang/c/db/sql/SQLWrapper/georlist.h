
#ifndef __georlist_h
#define __georlist_h

#include "gedef.h"
#include "geindex.h"

// Class Definitions --------------------------------------------------------

//Definition of the general list based in one coordinate (value)
template <class T>
class TGenericOrderedList : public TGenericIndex<T>
{
        protected:
                
                int Size;                               // List size (no of elements)
                int MoreSize;                   // Increment of the size (no of elements)
                T** Elements;                   // array of pointers to listed elements
                long* Values;                   // array of values of indexation
                int LastElement;                // The number of the last element inserted in the list (focused)
                                                                // It will be used to insert elements in the list more efficently

                int CheckRealloc ();    //Detect if there is no space for other element and reallocates it

                bool IsMinor (int number1, int number2); // Returns TRUE if value in element #number1 is minor than the value in element #number2
                bool IsMinor (long val, int number); // Returns TRUE if 'val' is minor than the value of the element in #number
                bool IsMinor (int number, long val); // Returns TRUE if the value of the element in #number is minor than 'val'

                inline bool IsEqual (int number1, int number2)// Returns TRUE if value in element #number1 is equal to the value in element #number2
                        {return (Values[number1] == Values[number2]);};
                inline bool IsEqual (long val, int number) // Returns TRUE if 'val' is equal to the value of the element in #number
                        {return (val == Values[number]);};
                inline bool IsEqual (int number, long val) // Returns TRUE if the value of the element in #number is equal to  'val'
                        {return (Values[number] == val);};

                int FindPosition (long value); // Search the place of the first element eith that value (or next if the value doesn't exist)
                
        public:

        // Constructor & destructor

                TGenericOrderedList(int IniSize = INIINDEXSIZE, int IniMoreSize = MORESIZE);
                ~TGenericOrderedList();

        // Member functions (public)

                long GetValue (int number); // Gives the value of the element in order 'number'
                T* GetElement (int number); // Returns pointer to the element in 'number'
      T* FindElement (long value); // Returns pointer to the element identified by 'value'

        // List of virtual functions of TGenericIndex to implement
                int Insert (long value, T* element);
                int Delete (long value, T* element, bool fr = false);
                int Flush (bool fr = false);
                int FindSize (long value);
                int Find (long value, OUT T** list);
                int LessToSize (long value);
                int LessTo (long value, OUT T** list);
                int GreaterToSize (long value);
                int GreaterTo (long value, OUT T** list);
};

#endif
