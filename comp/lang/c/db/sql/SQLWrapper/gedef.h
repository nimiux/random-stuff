#ifndef __def_h
#define __def_h

#include <malloc.h>

#ifndef OUT
#define OUT
#endif

// Macro for adding pointers/DWORDs together without C arithmetic interfering
//#define MakePtr(cast, ptr, addValue) \
 //  (cast) ((DWORD)(ptr) + (DWORD)(addValue))


// Definitions --------------------------------------------------------------

#define OBJ VOID
#define POBJ PVOID      // This will be SEDElement or whatever

#define HEAPSIZE                 80000  // heap size in objects for tree nodes
#define HEAPINISIZE              16000  // heap list habitual size in objects for tree nodes
#define RECTHEAPSIZE     16000  // heap size in objects for rectangles

#define INIINDEXSIZE       128  // initial index size (in objects)
#define MORESIZE                    64  // index expansion size (in objects)

#define MAXVAL                   65535  // Maximum value acepted for coordinates
#define MINVAL                           0      // Obviously, minimum value for coordinates

#define NODESIZE 1      // Size of the minimum block (and increase) for Size of list 'Elements'
                                        // For Tree and BR Tree

// Enumerators --------------------------------------------------------------

enum IndexMethod {
// Possible methods to sort&find that may be selected

        M_OrderedList = 1,      // Easy Method to add elements sortly, binary search, used by default
        M_BRTree,                       // Black and Red Equilibrated Tree
        M_Tree,                         // Normal Tree, less operations than previous, but possibly worse
        M_SkipList,                     // Is this method available and/or useful ??
        M_MaxMethod
};

enum RetCodes {  //Return Codes of the functions
        ret_ok = 0,
        ret_out_of_range = -1,
        ret_no_elem = -2,
        ret_out_of_mem = -3,
        ret_already_exists = -4,
        ret_invalid_param = -5,
        ret_fixed_size = -6,
        ret_no_method = -7,
        ret_no_size = -8,
   ret_no_update = -9,
};

enum OrderType {
        order_up,       //To sort the elements from smallest to biggest (upward)
        order_down      //The inverse (downward)
};

#endif
