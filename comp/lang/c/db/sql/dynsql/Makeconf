###############################################################################
# 
# Author      : Jose Maria Alonso 
#
# Module Name : Makeconf
#
# Location    : $(PROJECT_ROOT)/Makeconf
#
# Description : Makeconf for Project
#
###############################################################################

All: all

###############################################################################
# Paths and variables from environment

BINDIR=$(PROJECT_ROOT)/bin
INCDIR=$(PROJECT_ROOT)/include
LIBDIR=$(PROJECT_ROOT)/lib
SRCDIR=$(PROJECT_ROOT)/src

###############################################################################
# Oracle Libraries and Includes...

ORACLELIBPATH=$(ORACLE_HOME)/lib/

ORACLEPRECOMPHOME=$(ORACLE_HOME)/precomp
ORACLERDBMSHOME=$(ORACLE_HOME)/rdbms
ORACLEPLSQLHOME=$(ORACLE_HOME)/plsql
ORACLENETHOME=$(ORACLE_HOME)/network

include $(ORACLEPRECOMPHOME)/lib/env_precomp.mk

ORACLEINCDIR=-I$(ORACLEPRECOMPHOME)/public \
	-I$(ORACLERDBMSHOME)/public \
	-I$(ORACLERDBMSHOME)/demo \
	-I$(ORACLEPLSQLHOME)/public \
	-I$(ORACLENETHOME)/public 

###############################################################################
# Library Paths...

LIBPATH=-L/usr/local/lib \
	-L$(ORACLELIBPATH) \
	-L$(LIBDIR) \
	$(PROLDLIBS) 
	#-L$(LLIBDIR) 

###############################################################################
# Library Paths...
INCLUDE=-I.\
	-I$(INCDIR)/dir1 \
	-I$(INCDIR)/dir2 \
	-I$(INCDIR)/dir3 \
	-I$(INCDIR)/dir4 \
	-I$(INCDIR)/sample \
	$(ORACLEINCDIR)

###############################################################################
# C Compiler ...

DEFINE=-DUSE_EXP_BUF

###############################################################################
# GCC flags
GCC_CC=gcc
GCC_CPLUSPLUS=g++
GCC_CFLAGS=$(INCLUDE) $(DEFINE) -Wall -ansi

###############################################################################
# CC for Linux
LI_CC             =$(GCC_CC)
LI_CPLUSPLUS      =$(GCC_CPLUSPLUS)
LI_SO_EXT         =so
LI_SO_FLAG        =-shared
LI_CFLAGS         =$(GCC_CFLAGS) $(DEBUG)

###############################################################################
# CC for HP-UX/RISC flags
HP_CC      =cc
HP_SO_EXT  =sl
HP_SO_FLAG =-b
# Flags for development and debugging
HP_DBG_CFLAGS=$(INCLUDE) $(DEFINE) $(DEBUG) -w -DHPUX_RISC -Aa -D_HPUX_SOURCE +e +Z +O -y
# Flags for production
HP_OPT_CFLAGS=$(INCLUDE) $(DEFINE) -w -DHPUX_RISC -Aa -D_HPUX_SOURCE +e +Z +w1 +O4 -DNDEBUG -y

###############################################################################
# CC for Digital/Alpha flags
AP_CC      =cc
AP_SO_EXT  =so
AP_SO_FLAG =-shared
# Flags for development and debugging 
AP_DBG_CFLAGS=$(INCLUDE) $(DEFINE) $(DEBUG) -portable
# Flags for production
AP_OPT_CFLAGS=$(INCLUDE) $(DEFINE) -O4 -fast -ifo -inline speed -mp -om -speculate all -DNDEBUG 


###############################################################################
# Used flags
CC        = $(LI_CC)
CPLUSPLUS = $(LI_CPLUSPLUS)
CFLAGS    = $(LI_CFLAGS)

SO_EXT    = $(LI_SO_EXT)
SO_FLAG   = $(LI_SO_FLAG)

LINK      = $(LIBPATH) 

###############################################################################
# C Precompiler for Oracle 

CPRE=$(ORACLE_HOME)/bin/proc
CPREFLAGS= code=ansi \
	lines=yes \
	parse=full \
	sqlcheck=semantics \
	userid=$(PROJECT_DB_USER)/$(PROJECT_DB_PASS)@$(PROJECT_DB_URL) \
	char_map=string \
	prefetch=100 \
   define=SOLARIS

###############################################################################
# Other tools ...

RM=rm
RMFLAGS=-f

CP=cp
CPFLAGS=-f

MV=mv
MVFLAGS=-f

ECHO=/bin/echo

MAKEDEP=makedepend
MDFLAGS=$(DEFINE) $(INCLUDE)

###############################################################################
# Codes to make things look pretty...

# Color codes are of the form:
#
# <Esc>[3f;4b;xm
#
# Where f is foreground colour
#       b is background colour
#       x is 1 for bright, 4 for underline
#
# Colours: Black=0 Red=1 Green=2 Yellow=3 Blue=4 Magenta=5 Cyan=6 White=7
#
# semicolon (;) seperates the commands,
# "m" must be used to terminate the command.

# Example: this will highlight in bright red 

ON=[31\;1m
OFF=[m

###############################################################################
# Suffix rules ...

.SUFFIXES: .pc .c .o

.c.o :
	@echo "Making $@"
	$(CC) $(CFLAGS) -c $< -o $@

.pc.c :
	@echo "Making $@"
	$(CPRE) $(CPREFLAGS) iname=$@ 

