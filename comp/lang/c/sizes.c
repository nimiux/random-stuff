#include<stdio.h>
int main() {
    char charType;

    short int shortintType;
    int intType;
    long int longintType;

    float floatType;
    double doubleType;
    long double longdoubleType;

    printf("Characters\n");
    printf("\tSize of char: %zu bytes\n", sizeof(charType));

    printf("Integers\n");
    printf("\tSize of short int: %zu bytes\n", sizeof(shortintType));
    printf("\tSize of int: %zu bytes\n", sizeof(intType));
    printf("\tSize of long int: %zu bytes\n", sizeof(longintType));

    printf("Float\n");
    printf("\tSize of float: %zu bytes\n", sizeof(floatType));
    printf("\tSize of double: %zu bytes\n", sizeof(doubleType));
    printf("\tSize of long double: %zu bytes\n", sizeof(longdoubleType));

    return 0;
}

