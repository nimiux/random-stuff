/*#include <sys/types.h>*/
#include <fcntl.h>
#include <stdio.h>

#define DEVICE "/dev/ttyS0"

int main(int argc, char **argv) {
   int fd;
   if (-1 == (fd = open(DEVICE, O_RDWR))) {
      perror("ERROR:");
   } else {
      printf("Got file descriptor: %d\n", fd);
      close(fd); 
   }
}
