#ifndef DRCSrw_h
# define DRCSrw_h "$Id: rw.h,v 6.50 1999/12/19 06:06:20 wedgingt Exp $"

# include <ctype.h>

# ifdef pccompiler
#  include <io.h>
# else
#  include <unistd.h>
# endif

# include "zero.h"

extern UL chkpnt_iterations;

const char *archive_name PROTO((void));
extern FILE *open_archive PROTO((void));
extern void print_time PROTO((void));

# ifdef __STDC__
#  ifndef FACTOR_PACKAGE
#   define FACTOR char
#   define EXPONENT UL
extern int check_point(UL q, UL n, UL j, BIG_DOUBLE err, BIG_DOUBLE *x, UL mask, UL shift, int primenet);
extern void printbits(BIG_DOUBLE *x, UL q, UL n, UL totalbits, UL b, UL c, BIG_DOUBLE hi, BIG_DOUBLE lo,
                      UL mask, UL shift, const char *version_info, FILE *outfp, FILE *dupfp, int primenet);
#  endif
extern int input(int argc, char **argv, EXPONENT *q, UL *n, UL *j, BIG_DOUBLE *err, BIG_DOUBLE **x,
                 EXPONENT last, char *M, FACTOR *start, FACTOR *stop, int *all_factors, int *small_test,
                 EXPONENT *limit, UL mask, UL shift, FILE **infp, FILE **outfp, FILE **dupfp, int *primenet);
# else
extern int input();
#  ifndef FACTOR_PACKAGE
extern int check_point();
extern void printbits();
#  endif
# endif
#endif /* ifndef DRCSrw_h */
