static const char RCSsq_herm_c[] = "$Id: sq_herm.c,v 6.50 1999/12/19 06:06:20 wedgingt Exp $";

#include "setup.h"
#include "globals.h"
#include "init_fft.h"
#include "sq_herm.h"

#ifdef __STDC__
void square_hermitian(BIG_DOUBLE *x, UL n)
#else
void square_hermitian(x, n)
BIG_DOUBLE *x;
UL n;
#endif
 {
  register long i, half = n >> 1;
  register BIG_DOUBLE c, d;

  x[0] *= x[0];
  x[half] *= x[half];
  for (i = 1; i < half; i++)
   {
    c = x[i];
    d = x[n-i];
    x[n-i] = 2.0*c*d;
    x[i] = (c + d)*(c - d);
   }
 }
