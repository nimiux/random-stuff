#define DRCSfht_h "$Id: fht.h,v 6.50 1999/12/19 06:06:20 wedgingt Exp $"

#ifdef __STDC__
extern void fht(REAL *fz, UL n);
#else
extern void fht();
#endif
