#ifndef lint
static const char RCSecm3_c[] = "$Id: ecm3.c,v 6.50 1999/12/19 06:06:20 wedgingt Exp $ wedgingt@acm.org";
#endif

/* Reads exponents, known factors, and prior ECM info and tries to
   find the remaining prime factors.  It presumes the input is in the
   usual mers package format and that all the info for a particular
   exponent is together:

M( 569 )C: 15854617
M( 569 )C: 55470673
M( 569 )G: 55470674 182602768015690099110572536950
M( 569 )C: 182602768015690099110572536951
M( 569 )H: 182602768015690174955269260497
M( 569 )E: 21311 2957

   'C' lines denote factors, 'H' lines the highest trial factor
   attempted, 'G' lines denote gaps in trial factoring, and 'E' lines
   list the ECM "work" (the sum over all prior curves of the bound,
   presently) and largest bound.  Note that the primary output of ecm3
   will always be of the form:

M( 569 )C: 15854617 # ecm3.c bound 1000
M( 569 )e: 1000 5 # ecm3.c
M( 563 )d # ecm3.c zcomposite(cofactor, 5, 3)

   The first line is an example of a factor being discovered at ECM
   bound 1000; the second says that 5 curves at bound 1000 were
   attempted _after_ any factors were discovered; the third says that
   the remaining cofactor is "probably" prime, as it has passed five
   pseudo-prime tests.  These formats are also valid input to ecm3.c,
   though 'd' and 'D' for 'done' lines will mean that exponent will be
   skipped.  (Other lines will also be printed if -i is given a value
   larger than 0, but those are _not_ valid input to the program).

   Exponents on lines by themselves are also accepted as input as is
   the Cunningham Project's format:

569	15854617.55470673.182602768015690099110572536951.	Cxxx

   including the factors listed explicitly.

   Please send any questions, comments, improvements, fixes, bugs, and
   all output of the forms above to me via email:

   wedgingt@acm.org

   so I can update the "work" values in:

   http://www.garlic.com/~wedgingt/lowM.txt

   For a lengthier description of the primary output formats,
   including the cumulative "work" format, see:

   http://www.garlic.com/~wedgingt/mersfmt.html

*/

#include "setup.h"
#include "lip.h"
#include <time.h>

#ifdef macintosh
# include <console.h>
#endif

#ifndef ECM_MINBOUND
# define ECM_MINBOUND (100)
#endif

#ifndef ECM_MAXBOUND
# define ECM_MAXBOUND (5000000)
#endif

#if !defined(MAX_SPLITS)
# define MAX_SPLITS (2)
/* Mersennes that can be algebraicly factored by the program can be split into at most this many factors */
#endif

#if MAX_SPLITS < 2
# undef MAX_SPLITS
# define MAX_SPLITS (2)
#endif

#ifndef MAX_EXPONENTS
# define MAX_EXPONENTS (1000)
#endif

#if MAX_EXPONENTS < 5*MAX_SPLITS
# undef MAX_EXPONENTS
# define MAX_EXPONENTS (5*MAX_SPLITS)
#endif

#define ARRAY_SLOTS (MAX_EXPONENTS + MAX_SPLITS + 1)

#define LN10 (2.302585092994045684017991454684364207601101488628772976033327)

/* flags used in the global done[] array */
#define DONE_FLAG_DONE (0x1)
  /* bit 0: done, as in completely factored? */
#define DONE_FLAG_CHECK_PRIMALITY (0x2)
  /* bit 1: check (original or new) cofactor's primality? */
#define DONE_FLAG_SPLIT (0x4)
  /* bit 2: split exponent due to complicated algebraic factorization?  see split_LM() */
#define DONE_FLAG_L_OR_M (0x8)
  /* bit 3: for split exponents, L (bit off) or M (bit on) */

/* macros used to test done[] vs. DONE_FLAG*'s */
#define DO_ECM(entry) ((done[entry] & (DONE_FLAG_DONE|DONE_FLAG_CHECK_PRIMALITY)) == 0)
#define DONE_YET(entry) ((done[entry] & DONE_FLAG_DONE) == DONE_FLAG_DONE)
#define CHECK_PRIMALITY(entry) ((done[entry] & DONE_FLAG_CHECK_PRIMALITY) == DONE_FLAG_CHECK_PRIMALITY)
#define IS_SPLIT(entry) ((done[entry] & DONE_FLAG_SPLIT) == DONE_FLAG_SPLIT)
#define IS_SPLIT_L(entry) ((done[entry] & (DONE_FLAG_SPLIT|DONE_FLAG_L_OR_M)) == DONE_FLAG_SPLIT)
#define IS_SPLIT_M(entry) ((done[entry] & (DONE_FLAG_SPLIT|DONE_FLAG_L_OR_M)) == (DONE_FLAG_SPLIT|DONE_FLAG_L_OR_M))

/* macros to set or unset flags in done[] */
#define DONE_NOW(entry) (done[entry] &= ~(int)DONE_FLAG_CHECK_PRIMALITY, done[entry] |= DONE_FLAG_DONE)
#define DONE_CLEAR(entry) (done[entry] = 0)
#define DONE_DO_CHECK(entry) (done[entry] |= DONE_FLAG_CHECK_PRIMALITY)
#define DONE_DONT_CHECK(entry) (done[entry] &= ~(int)DONE_FLAG_CHECK_PRIMALITY)
#define DONE_SPLITTING_L(entry) ((done[entry] |= DONE_FLAG_SPLIT), (done[entry] &= ~(int)DONE_FLAG_L_OR_M))
#define DONE_SPLITTING_M(entry) (done[entry] |= (DONE_FLAG_SPLIT|DONE_FLAG_L_OR_M))

const char *program_name = NULL;
verylong one = NULL; /* constant after first set near start of main() */
unsigned long y[ARRAY_SLOTS] = { 0L }; /* Mersenne exponents */
verylong z[ARRAY_SLOTS] = { NULL }; /* remaining cofactor */
char done[ARRAY_SLOTS] = { 0L }; /* DONE_FLAG* flags */
unsigned long bounds[ARRAY_SLOTS] = { 0L }; /* initial stage one bound */
unsigned long curves[ARRAY_SLOTS] = { 0L }; /* number of curves to do */

/* if y[which] is composite, pull out the factors of z[which] that are due to y[which]'s factors */
#ifdef __STDC__
static void pull_factors(int which)
#else
static void pull_factors(which)
int which;
#endif
 {
  unsigned long yFactor = 2L, yRemaining; /* capitalizations to certainly avoid conflicts with freeLIP */
  static verylong M_y_Cofactor = NULL, zFactor = NULL, zRemainder = NULL;

  zintoz(y[which], &zFactor);
  zgcd(z[which], zFactor, &zRemainder);
  while (zscompare(zRemainder, 1) > 0)
   { /* should only occur if y[which] is not square-free */
    zdiv(z[which], zRemainder, z + which, &zFactor);
    zintoz(y[which], &zFactor);
    zgcd(z[which], zFactor, &zRemainder);
   }
  for (yRemaining = y[which]; yFactor <= yRemaining && 2L*yFactor <= y[which]; yFactor++, yFactor |= 1)
    if (yRemaining % yFactor == 0L)
     {
      for (yRemaining /= yFactor; yRemaining % yFactor == 0L; )
        yRemaining /= yFactor;
      zlshift(one, y[which]/yFactor, &M_y_Cofactor);
      zsub(M_y_Cofactor, one, &M_y_Cofactor); /* now M(y[which]/yFactor) */
      /* GCD(M(a), M(b)) == M(GCD(a, b)), so divide out M(y[which]/yFactor) */
      zgcd(z[which], M_y_Cofactor, &zFactor);
      while (zscompare(zFactor, 1) > 0)
       {
        zdiv(z[which], zFactor, z + which, &zRemainder);
        zgcd(z[which], M_y_Cofactor, &zFactor);
       }
     }
 }

/* if y[which] % 8 == 4, split z[which] into the L (z[which]) and M (z[which + 1]) halves based on */
/*  algebraic factorization: M(8*n - 4) = M(4*n - 2)*(M(4*n - 2) + 2) (difference of squares); */
/*  M(4*n - 2) will have already been pulled out by pull_factors(), but M(4*n - 2) + 2 == */
/*  2^(4*n - 2) + 1 == (2^(2*n - 1) - 2^n + 1)*(2^(2*n - 1) + 2^n + 1) == L*M.  But note that */
/*  pull_factors() may have pulled factors of L and/or M as well as M(4*n - 2), so we have to do */
/*  more zgcd()'s. */
#ifdef __STDC__
static int split_LM(int which)
#else
static int split_LM(which)
int which;
#endif
 {
  unsigned long n = (y[which] + 4)/8;
  static verylong L = NULL, M = NULL, temp = NULL;

  if (8*n - 4 != y[which] || n < 4)
    return(0); /* should not have been called; either exponent is not the right form or is very small */
  zintoz(1, &L);
  zlshift(L, n - 1, &temp); /* 2^(n - 1) */
  zsadd(temp, -1, &L); /* 2^(n - 1) - 1 == M(n) */
  zsadd(temp, 1, &M); /* 2^*(n - 1) + 1 */
  zlshift(L, n, &temp); /* 2^(2*n - 1) - 2^n */
  zsadd(temp, 1, &L);
  zlshift(M, n, &temp); /* 2^(2*n - 1) + 2^n */
  zsadd(temp, 1, &M);
  /* in case we return early: */
  DONE_CLEAR(which);
  DONE_NOW(which);
  DONE_CLEAR(which + 1);
  DONE_NOW(which + 1);
  zgcd(z[which], M, z + which + 1);
  if (zscompare(z[which + 1], 1) == 0)
    return(0); /* something is wrong */
  zgcd(z[which], L, &temp);
  if (zscompare(temp, 1) == 0)
    return(0); /* ditto */
  zdiv(z[which], temp, &M, &L);
  if (zscompare(L, 0) != 0)
    return(0); /* implies zgcd() bug */
  if (zcompare(M, z[which + 1]) != 0)
    return(0); /* something is wrong in that I don't understand algebra/situation */
  zcopy(temp, z + which);
  y[which + 1] = y[which];
  DONE_CLEAR(which);
  DONE_CLEAR(which + 1);
  DONE_DO_CHECK(which);
  DONE_DO_CHECK(which + 1);
  DONE_SPLITTING_L(which);
  DONE_SPLITTING_M(which + 1);
  bounds[which + 1] = bounds[which];
  curves[which + 1] = curves[which];
  return(1);
 }

#ifdef __STDC__
int main(int argc, char **argv)
#else
int main(argc, argv)
int argc;
char **argv;
#endif
 {
  double grow = 0.63; /* multiply bound by this after trying curves at current bound for all exponents */
    /*  except that values less than 1.0 mean to grow it via work += bound; bound = exp(grow*log(work)); */
  double bnd_tmp; /* temporary used to calculate a new bound */
  verylong answer = NULL; /* new factor from zfecm() */
  verylong b = NULL; /* factor from input, other uses */
  verylong d = NULL; /* remainder of division of Mersenne by 'known' factor; curve count from old data */
  verylong f = NULL; /* lots of uses */
  verylong ztmp; /* just for swapping within z[]; don't allocate memory to ztmp nor free it */
  unsigned long p, n = 2L; /* Mersenne exponent read and ECM iterations per P1 bound per exponent */
  unsigned long first = 32L, last = ~(unsigned long)0; /* ignore exponents outside this range */
  unsigned long bound = 1L, default_curves = 1L; /* from -b and -c on command line */
  unsigned long limit = 0L; /* maximum bound; if given, only one set of curves at this bound */
  unsigned long curve_tmp; /* for temp. storage of curve count on 'M( <exp> )e:' lines */
  unsigned long lowest_sum = ~(unsigned long)0; /* lowest sum of exponent and bound */
  int which = 0; /* index of exponent that is being worked on; borrowed for argv's index before then */
  int max; /* count of incompletely factored exponents */
  int ch; /* for reading a char at a time and scanf() return value */
  int info = 0; /* how much extra info to print */
  int zcompos; /* return value from zcomposite() */
  char *outfn = NULL; /* output file name; replaces stdout */
  char foundfac; /* found a new factor? */
  char M; /* 'C', 'G', 'H', 'U', 'E', 'e', etc. */
  char pflag = 0; /* is -p on command line? i.e., do we check the initial cofactors' primality? 0x1 bit */
#define CHECK_INITIALLY ((pflag & 0x1) == 0x1)
                  /* -P means to _only_ check cofactors: 0x2 bit */
#define CHECK_ONLY ((pflag & 0x2) == 0x2)
                  /* -L means low: work on Mersenne with lowest sum of exponent and bound first: 0x4 bit */
#define DO_LOWEST_SUM ((pflag & 0x4) == 0x4)
                  /* -D means done: print cofactor in decimal when it is thought to be prime: 0x8 bit */
#define PRINT_COFACTORS ((pflag & 0x8) == 0x8)
                  /* -C means check input factors to verify they are prime */
#define CHECK_FACTORS ((pflag & 0x10) == 0x10)
                  /*!! -d will be dup_to_stderr */
/*!! #define DUP_TO_STDERR ((pflag & 0x20) == 0x20) */
  int split = 0; /* after a call to split_LM(), this is sometimes used to modify which or max */
  int splits_warned = MAX_SPLITS; /* warn if split gets too high */

#ifdef macintosh
  argc = ccommand(&argv);
#endif
  /* setup */
  if (argv == NULL || argv[0] == NULL)
    program_name = "ecm3";
  else
    for (program_name = argv[0] + strlen(argv[0]); program_name > argv[0] && *program_name != '/'; )
      --program_name;
  if (*program_name == '/')
    ++program_name;
  setup();
  zrstarts(time((time_t)NULL));
  setlinebuf(stdout);

  /* parse the command line */
  while (!terminate && argv != NULL && argv[++which] != NULL && argv[which][0] == '-')
    switch (argv[which][1])
     { /*!!create UL get_arg()? in setup.c? or rw.c? */
      case 'b': case 'B':
        if (isascii(argv[which][2]) && isdigit(argv[which][2]))
          bound = atol(argv[which] + 2);
        else
          if (argv[++which] != NULL && isascii(argv[which][0]) && isdigit(argv[which][0]))
            bound = atol(argv[which]);
          else
            bound = 0, terminate = 1;
        break;
      case 'c':
        if (isascii(argv[which][2]) && isdigit(argv[which][2]))
          default_curves = n = atol(argv[which] + 2);
        else
          if (argv[++which] != NULL && isascii(argv[which][0]) && isdigit(argv[which][0]))
            default_curves = n = atol(argv[which]);
          else
            default_curves = 0, terminate = 1;
        break;
      case 'C':
        pflag ^= 0x10; /* CHECK_FACTORS */
        break;
/*    case 'd': */
        /* pflag ^= 0x20; */ /* DUP_TO_STDERR */
        /* break; */
      case 'D':
        pflag ^= 0x8; /* PRINT_COFACTORS */
        break;
      case 'g': case 'G':
        if (isascii(argv[which][2]) && isdigit(argv[which][2]))
          grow = atof(argv[which] + 2);
        else
          if (argv[++which] != NULL && isascii(argv[which][0]) && isdigit(argv[which][0]))
            grow = atof(argv[which]);
          else
            grow = -1.0, terminate = 1;
        break;
      case 'i': case 'I':
        if (isascii(argv[which][2]) && isdigit(argv[which][2]))
          info = atoi(argv[which] + 2);
        else
          if (argv[++which] != NULL && isascii(argv[which][0]) && isdigit(argv[which][0]))
            info = atoi(argv[which]);
          else
            info = -1, terminate = 1;
        break;
      case 'l':
        if (isascii(argv[which][2]) && isdigit(argv[which][2]))
          limit = atol(argv[which] + 2);
        else
          if (argv[++which] != NULL && isascii(argv[which][0]) && isdigit(argv[which][0]))
            limit = atol(argv[which]);
          else
            limit = 1, terminate = 1;
        break;
      case 'L':
        pflag ^= 0x4; /* DO_LOWEST_SUMS: work on Mersenne with lowest sum of exponent and bound first */
        break;
      case 'p':
        pflag ^= 0x1; /* CHECK_INITIALLY: check primality of initial cofactors? */
        break;
      case 'P':
        pflag ^= 0x2; /* CHECK_ONLY: do nothing except check cofactors */
        break;
      case 'o': case 'O':
        if (argv[which][2] != '\0')
          outfn = argv[which] + 2;
        else
          if (argv[++which] != NULL && argv[which][0] != '\0')
            outfn = argv[which];
          else
            outfn = NULL, terminate = 1;
        break;
      case 'r': case 'R':
        if (isascii(argv[which][2]) && isdigit(argv[which][2]))
          first = atol(argv[which] + 2);
        else
          if (argv[++which] != NULL && isascii(argv[which][0]) && isdigit(argv[which][0]))
            first = atol(argv[which]);
          else
            first = 0L, terminate = 1;
        if (!terminate && argv[++which] != NULL && isascii(argv[which][0]) && isdigit(argv[which][0]))
          last = atol(argv[which]);
        else
          last = 0L, terminate = 1;
        if (first > last || first < 32L)
          terminate = 1;
        break;
      default: /* help option or unknown option or any arg */
        terminate = 1; /* get out of this loop and into the next if */
        /* fall thru */
     }

  if (terminate || (argv != NULL && argv[which] != NULL))
   {
    if (argv[which][0] != '-' ||
        (argv[which][1] != 'h' && argv[which][1] != 'H' && argv[which][1] != '?'))
      fprintf(stderr, "%s: unknown flag or argument: '%s'\n", argv[0], argv[which]);
    fprintf(stderr, "  Usage: %s [-h|-H|-?] [-b<bound>] [-c<curves>]\n", argv[0]);
    fprintf(stderr, "         [-i<info>] [-l<limit>] [-o<outputfilename>] [-g<grow>]\n");
    fprintf(stderr, "    -b<bound>: use <bound> as the initial stage one bound (default 1000)\n");
    fprintf(stderr, "    -c<curves>: try <curves> per bound per exponent (default 1)\n");
    fprintf(stderr, "    -C: toggle: check primality of input factors (default off)\n");
    fprintf(stderr, "    -D: print cofactors, in decimal, if at least pseudo-prime (default off)\n");
    fprintf(stderr, "    -g<grow>: multiply bound by <grow> after curves done (default 0.0)\n");
    fprintf(stderr, "              (less than 1.0 grow via 'work += bound; bound = exp(grow*log(work));'\n");
    fprintf(stderr, "    -i<info>: how much extra info to print; low 2 bits passed to freeLIP's zfecm()\n");
    fprintf(stderr, "    -l<limit>: max. bound; only one set of <curves> at <limit> (default none)\n");
    fprintf(stderr, "    -L: toggle: attempt in order of lowest sum of exponent and bound\n");
    fprintf(stderr, "                (if -l<limit> also given, <limit> is of this sum, not just bound)\n");
    fprintf(stderr, "    -p: toggle pseudo-prime checks of the starting cofactors (default off)\n");
    fprintf(stderr, "    -P: toggle doing only pseudo-prime checks (default off)\n");
    fprintf(stderr, "                (if -l<limit> also given, <limit> applies to digits in cofactor)\n");
    fprintf(stderr, "    -o<outputfilename>: file to write non-error output to (default stdout)\n");
    fprintf(stderr, "    -r<first> <last>: skip exponents less than <first> or more than <last>\n");
    fprintf(stderr, "     (<first> must be > 32; exponents under 599 are factored anyway)\n");
    fprintf(stderr, "    -h, -H, -?: print this usage information and exit\n");
    fprintf(stderr, "  Tries to factor Mersenne numbers using freeLIP's zfecm()\n");
    fprintf(stderr, "  Reads from stdin and writes to stdout; future versions will be more flexible\n");
    fprintf(stderr, "  Note that some input lines will override the command line values per exponent\n");
    fprintf(stderr, "  $Id: ecm3.c,v 6.50 1999/12/19 06:06:20 wedgingt Exp $\n");
    fprintf(stderr, "  Maintained by <a href=\"mailto:wedgingt@acm.org\">Will Edgington</a>\n");
    fprintf(stderr, "  <a href=\"http://www.garlic.com/~wedgingt/mersenne.html\">Mers</a>\n");
    return(1);
   }

  /* check arguments given */
  if (bound < 1)
    fprintf(stderr, "%s: starting bound (-b) must be positive\n\t(\'ecm3 -h\' will print help info)\n",
            argv[0]);
  if (default_curves < 1)
    fprintf(stderr, "%s: number of curves (-c) must be positive\n\t(\'ecm3 -h\' will print help info)\n", argv[0]);
  if (info < 0)
    fprintf(stderr, "%s: info value (-i) must be non-negative\n\t(\'ecm3 -h\' will print help info)\n", argv[0]);
  if (limit == 1 || (0 < limit && limit < bound && !CHECK_ONLY))
    fprintf(stderr, "%s: limit (-l) must be integer >= starting bound\n\t(\'ecm3 -h\' will print help info)\n", argv[0]);
  if (grow < 0.3)
    fprintf(stderr, "%s: grow value (-g) must be at least 0.3\n\t(\'ecm3 -h\' will print help info)\n", argv[0]);
  if (bound < 1 || default_curves < 1 || info < 0 || grow < 0.3 || limit == 1 ||
      (0 < limit && limit < bound && !CHECK_ONLY))
    return(2);

  /* set restrictions and constants */
  if (bound < ECM_MINBOUND)
    bound = ECM_MINBOUND;
  if (default_curves != n)
    default_curves = 1; /* no -c on cmd line */
  if (default_curves > 1 << 30)
    default_curves = 1 << 30;
  if ((info & 3) > 2)
    info--; /* freeLIP's zfecm() only accepts 0, 1, and 2 for info */
  if (0 < limit && limit < bound && !CHECK_ONLY)
    limit = bound;
  if (limit > ECM_MAXBOUND && !CHECK_ONLY)
    limit = ECM_MAXBOUND;
  if (grow >= 1.0 && grow > limit/(double)ECM_MINBOUND && limit > ECM_MINBOUND)
    grow = limit/(double)ECM_MINBOUND;

  if (outfn != NULL && freopen(outfn, "a", stdout) == NULL)
   {
    fprintf(stderr, "%s: could not open output file '%s' for appending\n\t(\'ecm3 -h\' will print help info)\n", argv[0], outfn);
    perror(outfn);
    return(errno);
   }
  /* try to prevent data from being lost in the stdio buffers */
  fflush(stdout);
  setbuf(stdout, NULL);
  fflush(stderr);
  setbuf(stderr, NULL);

  if ((info & 4) != 0)
    if (!CHECK_ONLY)
      printf("%s: using %lu curves at starting bound %lu to limit of %lu & growth %f\n", program_name,
             default_curves, bound, limit, grow);
    else
      printf("%s: checking new cofactors of up to %lu digits\n", program_name, limit);

  zintoz(1, &one);
  for (max = 0; max < ARRAY_SLOTS; max++)
    z[max] = NULL, DONE_CLEAR(max), DONE_NOW(max);

  for (y[max = 0] = 0L; !terminate && (max >= 0 || !feof(stdin)); )
   { /* if we have room, read and parse a line of input */
    if (max < MAX_EXPONENTS && !feof(stdin) && (ch = fgetc(stdin)) != EOF)
     { /* this is a mess because some scanf's _require_ whitespace if a blank starts the format string */
      while (isascii(ch) && isspace(ch))
        ch = fgetc(stdin);
      /* check for the Cunningham Project format or 'bare' exponent */
      if (isascii(ch) && isdigit(ch))
       {
        for (p = 0L; isascii(ch) && isdigit(ch); ch = fgetc(stdin))
          p = 10*p + ch - '0';
        if (p < first || p > last)
         {
          while ((ch = fgetc(stdin)) != EOF && ch != '\n')
            ;
          continue;
         }
        if (y[max] != p) /* this will likely always be true for the Cunningham format */
         { /* this if is identical to an if in the mers package format code */
          if (y[max] != 0L && !DONE_YET(max))
           {
            if (DO_LOWEST_SUM && lowest_sum > !CHECK_ONLY*(y[max] + bounds[max]) + z2log(z[max]))
              lowest_sum = !CHECK_ONLY*(y[max] + bounds[max]) + z2log(z[max]);
            max++; /* safe because arrays are at least one longer than MAX_EXPONENTS */
           }
          y[max] = p;
          DONE_CLEAR(max);
          if (CHECK_INITIALLY || CHECK_ONLY)
            DONE_DO_CHECK(max);
          zlshift(one, y[max], z + max);
          zsub(z[max], one, z + max);
          pull_factors(max);
          bounds[max] = bound;
          curves[max] = default_curves;
          if ((info & 8) != 0)
            printf("first (Cunningham) input for M%lu\n", p);
          if (y[max] % 8 == 4)
            if (split_LM(max) != 0) /* split z[max] into L and M halves as z[max] and z[max + 1] */
              max++;
            else
              printf("%s: split_LM() returned 0 for M%lu\n", program_name, y[max]);
         }
        if (ch == '\t') /* Cunningham format */
         {
          while (isascii(ch) && isspace(ch)) /*!!presumes isascii('\t') is true */
            ch = fgetc(stdin);
          /*!!Do NOT use zread() here */
          for (zzero(&b); isascii(ch) && isdigit(ch); ch = fgetc(stdin))
           {
            zsmul(b, 10, &b);
            zsadd(b, ch - '0', &b);
           }
          for (split = 0; !ziszero(b); split = 0)
           {
            if (CHECK_FACTORS)
             {
              zcopy(b, &d);
              if ((zcompos = zcomposite(&d, -2, 3)) != 0)
               {
                printf("M( %lu )C: ", y[max]);
                zwrite(d);
                if (zcompos > 0)
                  printf(" # composite factor given\n");
                else
                  printf(" # factor of given factor; cofactor not checked\n");
               }
             }
            zgcd(z[max], b, &d);
            while (zcompare(b, d) != 0 && max - split > 0 && y[max - split - 1] == y[max - split])
             {
              if (zscompare(d, 1L) > 0)
               {
                printf("M( %lu )C: ", y[max - split]);
                zwrite(d);
                printf(" # ecm3.c factor of given factor\n");
                zdiv(z[max - split], d, &answer, &f);
                if (!ziszero(f))
                  printf("%s: bug in zgcd() or zdiv() in main() for M%lu\n", program_name, y[max - split]);
                else
                  zcopy(answer, z + max - split);
               }
              zgcd(z[max - ++split], b, &d);
             }
            if (zcompare(b, d) != 0)
             {
              printf("M( %lu )C: ", y[max - split]);
              zwrite(b);
              printf(" # ecm3.c false factor or factor of smaller Mersenne\n");
              if (zscompare(d, 1L) <= 0)
                break;
              printf("M( %lu )C: ", y[max - split]);
              zwrite(d);
              printf(" # ecm3.c factor of given factor\n");
             }
            zdiv(z[max - split], d, &answer, &b);
            if (!ziszero(b))
              printf("%s: bug in zgcd() or zdiv() in main() for M%lu\n", program_name, y[max - split]);
            else
              if (zscompare(answer, 1L) == 0)
               {
                printf("M( %lu )C: ", y[max - split]);
                zwrite(d);
                printf(" # given factor is cofactor\n");
                DONE_DO_CHECK(max - split);
                break;
               }
              else
                zcopy(answer, z + max - split);
            if (ch != '.')
              break;
            zzero(&b);
            for (ch = fgetc(stdin); isascii(ch) && isdigit(ch); ch = fgetc(stdin))
             {
              zsmul(b, 10, &b);
              zsadd(b, ch - '0', &b);
             }
            if (split > splits_warned)
              fprintf(stderr, "%s: MAX_SPLITS exceeded; recompile with at least %d\n", program_name,
                      splits_warned = split);
           } /* for (split = 0; !ziszero(b); split = 0) */
         } /* if (ch == '\t') */ /* Cunningham format */
        while (ch != EOF && ch != '\n') /* Cunningham or bare exponent */
          ch = fgetc(stdin); /* skip the rest of the line */
        continue;
       } /* if (isascii(ch) && isdigit(ch)) */ /* Cunningham or bare exponent */
      /* the expected colon cannot end the scanf format string because 'D' lines do not always have one */
      if (ch != 'M' || (ch = scanf("( %lu )%c", &p, &M)) == EOF)
        break;
      if (ch != 2)
       {
        fprintf(stderr, "%s: input format not recognized\n", argv[0]);
        if (y[max] != 0L || (max > 0 && y[--max] != 0L))
          fprintf(stderr, "  (after some data for exponent %lu read correctly)\n", y[max]);
        return(3);
       }
      if (p < first || p > last)
       {
        while ((ch = fgetc(stdin)) != EOF && ch != '\n')
          ;
        continue;
       }
      if (M == 'D' || M == 'P' || (!PRINT_COFACTORS && M == 'd'))
       { /* this Mersenne is already completely factored or the cofactor is a strong pseudo-prime */
        for (ch = fgetc(stdin); ch != EOF && ch != '\n'; )
          ch = fgetc(stdin); /* skip the rest of the line */
        continue;
       }
      zintoz(1, &b);
      if ((ch = fgetc(stdin)) == ':')
       {
        zzero(&b);
        for (ch = fgetc(stdin); isascii(ch) && isspace(ch); )
          ch = fgetc(stdin);
        for ( ; isascii(ch) && isdigit(ch); ch = fgetc(stdin))
         {
          zsmul(b, 10, &b);
          zsadd(b, ch - '0', &b);
         }
        if (!ziszero(b) && ((M == 'e' || M == 'E') && fscanf(stdin, " %lu", &curve_tmp) < 1))
          zzero(&b);
       }
      if (ziszero(b) && (M != 'd' && M != 'L' && M != 'M'))
       {
        fprintf(stderr, "%s: input format not recognized\n", argv[0]);
        if (y[max] != 0L || (max > 0 && y[--max] != 0L))
          fprintf(stderr, "  (after some data for exponent %lu read correctly)\n", y[max]);
        return(4);
       }
      while (ch != EOF && ch != '\n')
        ch = fgetc(stdin); /* skip the rest of the line */
      if (y[max] != p)
       {
        if (y[max] != 0L && !DONE_YET(max))
         {
          if (DO_LOWEST_SUM && lowest_sum > !CHECK_ONLY*(y[max] + bounds[max]) + z2log(z[max]))
            lowest_sum = !CHECK_ONLY*(y[max] + bounds[max]) + z2log(z[max]);
          max++;
         }
        y[max] = p;
        DONE_CLEAR(max);
        if (CHECK_INITIALLY || CHECK_ONLY)
          DONE_DO_CHECK(max);
        zlshift(one, y[max], z + max);
        zsub(z[max], one, z + max);
        pull_factors(max);
        bounds[max] = bound;
        curves[max] = default_curves;
        if ((info & 8) != 0)
          printf("first (mers) input for M%lu\n", p);
        if (y[max] % 8 == 4)
          if (split_LM(max) != 0)
            max++;
          else
            printf("%s: split_LM() returned 0 for M%lu\n", program_name, y[max]);
       }
      split = 0;
      switch (M)
       {
        case 'l': /* digits of composite cofactor L half of a split exponent */
          if (max > 0 && y[max - 1] == y[max] && IS_SPLIT_L(max - 1))
            split = 1;
	  /* fall thru */
        case 'c': /* digits of composite cofactor of non-split exponent */
        case 'm': /* digits of composite cofactor M half of a split exponent */
          if (zscompare(b, (long)(ceil(zln(z[max - split])/LN10))) == 0 &&
	      ((!IS_SPLIT(max - split) && M == 'c') || (IS_SPLIT_L(max - split) && M == 'l') ||
	       (IS_SPLIT_M(max - split) && M == 'm')))
            DONE_DONT_CHECK(max - split);
          else
           {
            DONE_DO_CHECK(max - split);
            if ((info & 16) != 0)
              printf("new cofactor to check for M%lu%s\n", y[max - split],
                     IS_SPLIT(max - split) ? (IS_SPLIT_L(max - split) ? "L" : "M") : "");
           }
          break;
        case 'C':
          if (CHECK_FACTORS)
           {
            zcopy(b, &d);
            if ((zcompos = zcomposite(&d, -2, 3)) != 0)
             {
              printf("M( %lu )C: ", y[max]);
              zwrite(d);
              if (zcompos > 0)
                printf(" # composite factor given\n");
              else
                printf(" # factor of given factor; cofactor not checked\n");
             }
           }
          split = 0;
          zgcd(z[max], b, &d);
          while (zcompare(b, d) != 0 && max - split > 0 && y[max - split - 1] == y[max - split])
           {
            if (zscompare(d, 1L) > 0)
             {
              printf("M( %lu )C: ", y[max - split]);
              zwrite(d);
              printf(" # ecm3.c factor of given factor\n");
              zdiv(z[max - split], d, &answer, &f);
              if (!ziszero(f))
                printf("%s: bug in zgcd() or zdiv() in main() for M%lu\n", program_name, y[max - split]);
              else
                zcopy(answer, z + max - split);
             }
            zgcd(z[max - ++split], b, &d);
           }
          if (split > splits_warned)
            fprintf(stderr, "%s: MAX_SPLITS exceeded; recompile with at least %d\n", program_name,
                    splits_warned = split);
          if (zcompare(b, d) != 0)
           {
            printf("M( %lu )C: ", y[max - split]);
            zwrite(b);
            printf(" # ecm3.c false factor or factor of smaller Mersenne\n");
            if (zscompare(d, 1L) <= 0)
              break;
            printf("M( %lu )C: ", y[max - split]);
            zwrite(d);
            printf(" # ecm3.c factor of given factor\n");
           }
          zdiv(z[max - split], d, &answer, &b);
          if (!ziszero(b))
            fprintf(stderr, "%s: bug in zgcd() or zdiv() in main() for M(%lu)\n", program_name,
                    y[max - split]);
          else
            if (zscompare(answer, 1L) == 0)
             {
              printf("M( %lu )C: ", y[max - split]);
              zwrite(d);
              printf(" # given factor is cofactor\n");
              DONE_DO_CHECK(max - split);
              break;
             }
            else
              zcopy(answer, z + max - split);
          zgcd(z[max - split], d, &b);
          if (zcompare(b, d) == 0)
           {
            if (zscompare(d, 3511) != 0 && y[max - split] % 2 == 1)
             { /* two known exceptions: M1755 % (3511*3511) == 0; other is even exponent */
              printf("Possible contradiction of square-free conjecture\nM( %lu )C: ", y[max - split]);
              zwrite(d);
              printf(" # square is a factor as well\n");
             }
            zdiv(z[max - split], d, &answer, &b);
            if (!ziszero(b))
              fprintf(stderr, "%s: bug in zgcd() or zdiv() in main() for M(%lu)\n", program_name,
                      y[max - split]);
            else
              if (zscompare(answer, 1L) == 0)
               {
                printf("M( %lu )C: ", y[max - split]);
                zwrite(d);
                printf(" # given factor is cofactor\n");
                DONE_DO_CHECK(max - split);
                break;
               }
              else
                zcopy(answer, z + max - split);
           }
          break;
        case 'd': /* PRINT_COFACTORS must be true for 'd' to get here */
          DONE_NOW(max);
          printf("M( %lu )%c: ", y[max - split], M);
          zwriteln(z[max - split]);
          while (--max >= 0 && y[max] == y[max + 1])
           { /* assume 'd' applies to all parts of a split exponent */
            DONE_NOW(max);
            printf("M( %lu )d: ", y[max]);
            zwriteln(z[max]);
           }
          continue;
        case 'e': /* b has the prior bound used; curve_tmp has the number of curves at that bound */
          if (CHECK_ONLY)
            break;
          if (max > 0 && y[max - 1] == y[max]) /* assume the work was split evenly among the halves */
            ((void)z2div(b, &d), zcopy(d, &b)); /*!!bad assumption when one half is factored easier */
          if (zscompare(b, ECM_MAXBOUND) > 0)
            zintoz(ECM_MAXBOUND, &b);
          if (curve_tmp >= default_curves)
           {
            if (grow >= 1.0)
              bnd_tmp = grow*zdoub(b) + 1.0;
            else
              bnd_tmp = exp(grow*(zdoub(b) + exp(zln(b)/grow))) + 1.0;
            if (bnd_tmp <= zdoub(b))
              bnd_tmp = zdoub(b) + 1.0;
            if (bounds[max] < bnd_tmp)
              bounds[max] = (unsigned long)bnd_tmp, curves[max] = default_curves;
           }
          else
            if (bounds[max] < ztouint(b))
              bounds[max] = ztouint(b), curves[max] = default_curves - curve_tmp;
          if (max > 0 && y[max - 1] == y[max]) /* assume everything is for both halves of split */
            bounds[max - 1] = bounds[max], curves[max - 1] = curves[max];
          break;
        case 'E': /* b has the "work"; curve_tmp has the largest bound used in that work */
          if (CHECK_ONLY)
            break;
          if (max > 0 && y[max - 1] == y[max]) /* assume the work was split evenly among the halves */
            ((void)z2div(b, &d), zcopy(d, &b));
          if (limit == 0 || curve_tmp <= limit || curve_tmp < ECM_MAXBOUND)
           {
            if (grow < 1.0)
              bnd_tmp = exp(grow*zln(b));
            else /* 0.63 appears to - very roughly - match the values in lipdoc.ps */
              bnd_tmp = exp(0.63*zln(b));
            if (bounds[max] < bnd_tmp)
              bounds[max] = (unsigned long)bnd_tmp;
           }
          else
            bounds[max] = ECM_MAXBOUND + 1;
          break;
        case 'H': /* b is the trial factoring extent, from which we can estimate the "best" bound */
          if (CHECK_ONLY)
            break;
	  /*!!except that many 'H' lines are from ECM or P-1 factors and the bnd_tmp equation does */
	  /*!! not appear to be well behaved; it gives much too high an answer in many cases */
#ifdef USE_H_LINES_TO_GET_STARTING_BOUND
          if ((bnd_tmp = z2log(b)) > 64) /* 2^65: trial factoring very rarely goes higher presently */
            break;
          /*!!this looks close but a bit low for all entries in the table in lipdoc.ps */
          /*!! yes, it's complicated and contains "empirical" constants */
          bnd_tmp = exp(log(bnd_tmp - 2.0)/0.127)/1000000.0/sqrt(bnd_tmp)/0.171;
          if (bounds[max] < bnd_tmp)
            bounds[max] = bnd_tmp, curves[max] = default_curves;
          if (max > 0 && y[max - 1] == y[max])
            bounds[max - 1] = bounds[max], curves[max - 1] = curves[max];
#endif
          break;
        case 'L':
          split = (max > 0 && y[max - 1] == y[max] && IS_SPLIT_L(max - 1));
          if (!IS_SPLIT_L(max - split))
            continue; /* invalid info or already marked done */
          /* fall thru */
        case 'M':
          if (M == 'M')
            split = 0;
          if (M == 'M' && !IS_SPLIT_M(max))
            continue;
          DONE_NOW(max - split);
          if (PRINT_COFACTORS)
           {
            printf("M( %lu )%c: ", y[max - split], M);
            zwriteln(z[max - split]);
           }
          continue;
       }
      if (CHECK_ONLY)
        continue;
      if (limit > 0 && (bounds[max] > limit || (DO_LOWEST_SUM && y[max] + bounds[max] > limit)))
        bounds[max] = limit + 1; /* would just do DONE_NOW(max) except for checking initial cofactor */
      else
        if (bounds[max] >= ECM_MAXBOUND)
          bounds[max] = ECM_MAXBOUND;
        else
          if (bounds[max] < bound)
            bounds[max] = bound;
      for (split = 0; max - split > 0 && y[max - split - 1] == y[max - split]; split++)
        bounds[max - split - 1] = bounds[max - split];
      if (split > splits_warned)
        fprintf(stderr, "%s: MAX_SPLITS exceeded; recompile with at least %d\n", program_name,
                splits_warned = split);
     } /* if (max < MAX_EXPONENTS && (ch = fgetc(stdin)) != EOF) */
    if (max < 0 || (!feof(stdin) && (y[0] == y[max] || (max < MAX_EXPONENTS && DO_LOWEST_SUM))))
        /* we need to read all of the lines for the first exponent before we can do anything */
      continue; /* if max < 0, we have no data at all and will exit */

    if (CHECK_ONLY || ++which >= max || y[which] == y[max])
      which = 0;
    zzero(&b);
    /* need to check this here in case we need/had to check the starting cofactor's primality */
    if (DO_ECM(which) && (CHECK_ONLY || (limit > 0 && (DO_LOWEST_SUM*y[which]) + bounds[which] > limit)))
      DONE_NOW(which);

    for (foundfac = !CHECK_ONLY; !terminate && DO_ECM(which) && foundfac; )
     { /* if -L was given, skip this Mersenne if appropriate */
      if (DO_LOWEST_SUM && lowest_sum < y[which] + bounds[which] + z2log(z[which]))
       {
        if (lowest_sum < (y[which] + bounds[which] + z2log(z[which]))/4 && max > MAX_EXPONENTS)
          DONE_NOW(which); /* presume we're not going to more than quadruple lowest_sum in a single run */
        break;
       }
      if (DO_LOWEST_SUM && lowest_sum > y[which] + bounds[which] + z2log(z[which]))
        lowest_sum = y[which] + bounds[which] + z2log(z[which]);
      if ((n = curves[which]) < 1L)
        n = curves[which] = default_curves;
      p = bounds[which];
      if ((info & 128) != 0)
        printf(":%lu: (%d of %d) test for %lu iterations @ P1 bound %lu.\n",
               y[which], which + 1, max, n, p);
      /* finally: try to find a new factor */
      switch ((int)zfecm(z[which], &answer, 0, &n, &p, 0.0, 0, info & 03, stdout))
       {
        case 0: /* zfecm() also returns 0 for problems(!) */
          printf("M( %lu )e: %lu %lu # ecm3.c\n", y[which], bounds[which], curves[which]);
          if (limit > 0 && (bounds[which] >= limit || (DO_LOWEST_SUM && y[which]+bounds[which] >= limit)))
           {
            DONE_NOW(which);
            lowest_sum++; /* best we can do without scanning entire array */
            if ((info & 256) != 0)
              printf("now done with M%lu\n", y[which]);
            continue;
           }
          if (limit > 0 && (bounds[which] >= limit && (DO_LOWEST_SUM && y[which]+bounds[which] >= limit)))
            bounds[which] = limit;
          else
            if (bounds[which] >= ECM_MAXBOUND)
              bounds[which] = ECM_MAXBOUND;
            else
              if (grow >= 1.0)
                bounds[which] = grow*(double)bounds[which] + 1;
              else
                bounds[which] = exp(grow*log(bounds[which] + exp(log(bounds[which])/grow))) + 1;
          if (curves[which] < default_curves) /* should only happen if the curves read in were fewer */
            curves[which] = default_curves;
          foundfac = 0;
          lowest_sum++;
          continue;
        case -1:
          printf("M( %lu )%c%s", y[which], IS_SPLIT(which) ? (IS_SPLIT_L(which) ? 'L' : 'M') : 'd',
                 PRINT_COFACTORS ? ": " : " # ecm3.c\n");
          if (PRINT_COFACTORS)
            zwriteln(z[which]);
          DONE_NOW(which);
          continue;
        case 1:
          if ((info & 32) != 0)
            printf(":%lu: FACTOR:\n", y[which]);
          break;
        case 2:
          if ((info & 32) != 0)
           {
            printf(":%lu: Got 2:", y[which]);
            zwriteln(answer);
           }
          break;
       }
      zdiv(z[which], answer, &d, &f);
      if (!ziszero(f)) /* should never happen */
        printf("Warning: non-zero remainder for new factor for M%lu\n", y[which]);
      if (zcompare(answer, d) > 0) /* this will usually only matter for composite y[which] */
        zcopy(answer, z + which), /* z[which] gets new cofactor and ... */
          zcopy(d, &answer); /* answer gets new factor (which is probably always prime if y[which] is) */
      else
        zcopy(d, z + which);
      zcopy(answer, &b); /* b also gets new factor */
      DONE_DO_CHECK(which);
      if (DO_LOWEST_SUM)
        lowest_sum = y[which] + bounds[which] + z2log(z[which]);
     } /* for (foundfac = 1; !terminate && DO_ECM(which) && foundfac; ) */

    while (CHECK_PRIMALITY(which))
     { /* check primality of new factor or of new (or old iff CHECK_INITIALLY) cofactor */
      if (ziszero(b)) /* check cofactor's primality, but don't try to factor it here */
        zcopy(z[which], &b);
      /* if -P and -l<limit> were given, only check cofactor's with up to <limit> digits */
      if (CHECK_ONLY && limit > 0 && limit < zslog(z[which], 10))
       { /* we cannot be checking a factor, since we have not called zcomposite() for this exponent yet */
        DONE_NOW(which);
        break;
       }
      /* if -L was given, skip this Mersenne if appropriate, but note it's not appropriate if we're */
      /*  checking a factor's primality */
      if (DO_LOWEST_SUM)
        if (lowest_sum < z2log(z[which]) && zcompare(z[which], b) == 0)
          break;
        else
          if (lowest_sum > !CHECK_ONLY*(y[which] + bounds[which]) + z2log(z[which]))
            lowest_sum = !CHECK_ONLY*(y[which] + bounds[which]) + z2log(z[which]);
      if ((info & 16) != 0 && zcompare(z[which], b) == 0)
        printf("checking cofactor of M%lu\n", y[which]);
      if ((n = curves[which]) < 1L)
        n = default_curves;
      p = bounds[which];
      zcopy(b, &f);
      zcompos = zcomposite(&b, -5, 3);
      while (zcompos == 1 && zcompare(z[which], b) != 0)
       { /* the new factor is composite; try to factor it */
        if ((info & 128) != 0)
          printf(":%lu: (%d of %d) factoring new factor for %lu curves at %lu\n",
                 y[which], which + 1, max, n, p);
        switch ((int)zfecm(b, &d, 0, &n, &p, 0.0, 0, info & 03, stdout))
         {
          case 0:
            if ((info & 32) != 0)
             {
              printf("ECM %lu curves at %lu failed to factor new composite factor M( %lu )C: ",
                     n, p, y[which]);
              zwriteln(answer);
             }
            if (++n > 2*curves[which] && n > 2*default_curves && n > 10) /* probably overkill */
             {
              printf("Warning: giving up trying to factor composite factor after %lu curves at %lu\n",
                     --n, bounds[which]);
              zintoz(2L, &b);
             }
            else
             {
              if (grow >= 1.0)
                p = grow*(double)p + 1;
              else
                p = exp(grow*log(p + exp(log(p)/grow))) + 1;
              if (p > ECM_MAXBOUND)
                p = ECM_MAXBOUND;
             }
            continue;
          case -1:
            printf("new composite factor now reported as prime by zfecm() M( %lu )C: ", y[which]);
            zwriteln(answer);
            zcompos = 0; /* prime or pseudo-prime */
            continue;
          case 1:
            if ((info & 32) != 0)
              printf(":%lu: new FACTOR of FACTOR:\n", y[which]);
            break;
          case 2:
            printf("Warning: next line is two factors of 'two' factors(?!)\n");
            break;
         }
        zdiv(b, d, &answer, &f);
        if (!ziszero(f)) /* should never happen */
          printf("Warning: non-zero remainder for factor of factor for exponent %lu\n", y[which]);
        if (zcompare(answer, d) > 0)
          zcopy(d, &f);
        else
          zcopy(answer, &f),
            zcopy(d, &answer);
        printf("M( %lu )C: ", y[which]);
        zwrite(f); /* smaller factor of factor; almost certainly prime if y[which] is prime */
        printf(" # ecm3.c bound %lu factor of factor\n", p);
        zcopy(answer, &b);
        zcopy(b, &f);
        zcompos = zcomposite(&b, -5, 3);
       } /* while new factor, b, is composite */
      switch (zcompos)
       {
        case 1: /* composite cofactor (all composite factors get factored just above) */
          printf("M( %lu )%c: %ld%s\n", y[which], IS_SPLIT(which) ? (IS_SPLIT_L(which) ? 'l' : 'm') : 'c',
                 (long)(ceil(zln(z[which])/LN10)), (info & 3) == 0 ? "" : " # composite cofactor");
          DONE_DONT_CHECK(which);
          continue;
        case 0: /* new factor or the cofactor is prime or pseudo prime in base 3 and four other bases */
          if (zcompare(z[which], b) == 0)
           { /* factorization of M(y[which]) (this half, if split) thought to be complete */
            printf("M( %lu )%c%s", y[which], IS_SPLIT(which) ? (IS_SPLIT_L(which) ? 'L' : 'M') : 'd',
                   PRINT_COFACTORS ? ": " : "\n");
            if (PRINT_COFACTORS)
              zwriteln(z[which]);
            DONE_NOW(which);
            continue;
           }
          printf("M( %lu )C: ", y[which]);
          zwrite(answer);
          printf(" # ecm3.c bound %lu\n", p);
          zzero(&b);
          continue; /* check primality of the new cofactor */
        case -1: /* composite, and new value of b may give new factor of old b, still in f */
        case -2: /* composite, and new value of b is a factor of old b, still in f */
          zdiv(f, b, &answer, &d);
          if (zcompos == -1 && !ziszero(d))
           { /*!!skip for now, but print what we know in case it happens */
            printf("Please send this section (below) to wedgingt@acm.org; it may lead to a new factor\n");
            printf("zcomposite(&b, -5, 3) returned -1 for M%lu\n", y[which]);
            if (zcompare(z[which], f) != 0)
             {
              printf(" cofactor (z[which]) is ");
              zwriteln(z[which]);
             }
            printf(" old b is ");
            zwriteln(f);
            printf(" new b is ");
            zwriteln(b);
	    printf("%s\n", "$Id: ecm3.c,v 6.50 1999/12/19 06:06:20 wedgingt Exp $");
            printf("Please send this section (above) to wedgingt@acm.org\n");
            DONE_NOW(which); /*!!pretend we've completed it just in case */
            continue;
           }
          if (!ziszero(d))
            printf("Warning: non-zero remainder via zcomposite() return of -2 for M%lu\n", y[which]);
          if (zcompare(b, answer) > 0)
            zcopy(answer, &d),
              zcopy(b, &answer),
              zcopy(d, &b);
          if (zcompare(z[which], f) == 0) /* b is factor of (now old) cofactor */
           {
            zcopy(answer, z + which);
            zcopy(b, &answer);
            continue; /* check primality of new factor */
           }
          printf("M( %lu )C: ", y[which]);
          zwrite(b);
          printf(" # ecm3.c zcomposite() == -2\n");
          zcopy(answer, &b);
          continue; /* check primality of b, cofactor of factor */
       } /* switch (zcompos) */
     } /* while (CHECK_PRIMALITY(which) */

    /* if we're at the front of the list or this one is done, shift later ones down over any that are done */
    if (which == 0 || DONE_YET(which))
     { /* borrow ch and zcompos for subscripts */
      if (DO_LOWEST_SUM)
        lowest_sum = ~(unsigned long)0;
      for (ch = 0; ch < max && !DONE_YET(ch); ch++)
        if (DO_LOWEST_SUM && ch < max - MAX_SPLITS &&
            lowest_sum > !CHECK_ONLY*(y[ch] + bounds[ch]) + z2log(z[ch]))
          lowest_sum = !CHECK_ONLY*(y[ch] + bounds[ch]) + z2log(z[ch]);
      for (zcompos = ch + 1; zcompos <= max; zcompos++)
       {
        if (DO_LOWEST_SUM && zcompos < max - MAX_SPLITS && !DONE_YET(zcompos) &&
            lowest_sum > !CHECK_ONLY*(y[zcompos] + bounds[zcompos]) + z2log(z[zcompos]))
          lowest_sum = !CHECK_ONLY*(y[zcompos] + bounds[zcompos]) + z2log(z[zcompos]);
        if (DONE_YET(ch) && !DONE_YET(zcompos))
         {
          y[ch] = y[zcompos];
          ztmp = z[ch];
          z[ch] = z[zcompos];
          z[zcompos] = ztmp;
          done[ch] = done[zcompos];
          DONE_CLEAR(zcompos);
          DONE_NOW(zcompos);
          bounds[ch] = bounds[zcompos];
          curves[ch] = curves[zcompos];
          if (ch++ == 0 && (info & 64) != 0)
            printf("first exponent is now M%lu\n", y[0]);
         }
       }
      while (max > 0 && DONE_YET(max))
        max--;
      if (max == 0 && DONE_YET(0))
        if (feof(stdin) || ferror(stdin))
          max--;
        else
          y[0] = 0L;
     }
   } /* for (y[max = 0] = 0L; !terminate && !feof(stdin); ) */
  return(0);
 }
