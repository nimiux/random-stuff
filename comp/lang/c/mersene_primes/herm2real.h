#define DRCSherm2real_h "$Id: herm2real.h,v 6.50 1999/12/19 06:06:20 wedgingt Exp $"

#ifdef __STDC__
extern void fftinv_hermitian_to_real(BIG_DOUBLE *z, UL n);
#else
extern void fftinv_hermitian_to_real();
#endif
