static const char RCSzero_c[] = "$Id: zero.c,v 6.50 1999/12/19 06:06:20 wedgingt Exp $";

#include "setup.h"
#include "zero.h"

#ifdef __STDC__
int is_zero(BIG_DOUBLE *x, UL n, UL mask, UL shift)
#else
int is_zero(x, n, mask, shift)
BIG_DOUBLE *x;
UL n, mask, shift;
#endif
 {
  register UL j, offset;

  for (j = 0; j < n; ++j)
   {
    offset = j + ((j & mask) >> shift);
    if (rint(x[offset]))
      return(0);
   }
  return(1);
 }
