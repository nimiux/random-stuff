static const char RCSlucas_init_c[] = "$Id: lucas_init.c,v 6.50 1999/12/19 06:06:20 wedgingt Exp $";

#include "setup.h"
#include "globals.h"
#include "lucas_init.h"

#ifdef __STDC__
void init_lucas(UL q, UL n, UL *b, UL *c)
#else
void init_lucas(q, n, b, c)
UL q, n, *b, *c;
#endif
 {
  UL j;
  long qn, a;
  BIG_DOUBLE ln2 = log(2.0);

  if (two_to_phi != NULL)
    free(two_to_phi);
  two_to_phi = (BIG_DOUBLE *)calloc(n, sizeof(BIG_DOUBLE));
  if (two_to_minusphi != NULL)
    free(two_to_minusphi);
  two_to_minusphi = (BIG_DOUBLE *)calloc(n, sizeof(BIG_DOUBLE));
  low = ldexp(1.0, (int)(q/n));
  high = low + low;
  lowinv = 1.0/low;
  highinv = 1.0/high;
  (*b) = q & (n - 1);
  (*c) = n - (*b);
  two_to_phi[0] = 1.0;
  two_to_minusphi[0] = 1.0;
  qn = q & (n - 1);
  for (j = 1; j < n; ++j)
   {
    a = n - ((j*qn) & (n - 1));
    two_to_phi[j] = exp(a*ln2/n);
    two_to_minusphi[j] = 1.0/two_to_phi[j];
   }
 }
