static const char RCStunefftw_c[] = "$Id: tunefftw.c,v 6.50 1999/12/19 06:06:20 wedgingt Exp $ wedgingt@acm.org";
const char *RCSprogram_id = RCStunefftw_c;
const char *program_name = "tunefftw"; /* for perror() and similar */
const char program_revision[] = "$Revision: 6.50 $";
char version[sizeof(RCStunefftw_c) + sizeof(program_revision)]; /* overly long, but certain */

/* The following comment is by Guillermo Ballester Valor.  Feel free to send me email
   about problems as well.  --Will Edgington, wedgingt@acm.org */

/* 
   tunefftw

   Version 1.0.0 
   Guillermo Ballester Valor
   Granada (Spain), Sep 1999.

   Tune program to take the maximum performance from GNU-FFTW package.

   FFTW (Fastest Fourier Transform in the West).

   This program is to provide a complete base of plan data to be used 
   by gmpfftw routines, the interface between GNU-GMP and GNU-FFTW package 

   Requirements:

   Obviously, you have to install FFTW package before.
   See www.fftw.org 

   Input arguments:

   You can estimate the number of maximum number of FFT elements you ever 
   will need.
   Give it in powers of two. Example for 65536 maximum number:
      tunefftw 16
*/ 

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "fftw.h"
#include "rfftw.h"

extern void explain(void);

void explain(void)
{
  printf("\n Usage: tunefftw <size>");
  printf("\n        <size> = exponent for power of two max. size FFT ");
  printf("\n                 example: 16  is for 2^16=65536 max. FFT length.");
  printf("\n\n DON'T FORGET THE MEMORY REQUIREMENTS (16 bytes per FFT element.)\n");
}

int main (int argc, char *argv[])
{
  int optsize,base,i,n;
  FILE *out_file;
  const char *namefile = "fftw.pln";
  fftw_plan plan;

  if (argc == 1) 
    {
      explain();
      exit(1);
    }
  else if (argc ==2)
    optsize=atoi( argv[1]);
  else  
    {
      explain();
      exit(1);
    }
  if (optsize > 25) 
    {
      explain();
      exit(1);
    }
  else if ((optsize < 5) && (optsize >0))
    optsize=5;
  else if ( optsize <= 0) 
    {
      explain();
      exit(1);
    }
  
  printf("\n The max. size is %i ", (1<<optsize));
  printf("\n Please wait. It can take from few minutes to half an hour.");
  printf("\n Trying to find the best FFT for the following eight sizes :");
  base= 1<<(optsize-4);
  for(i=9;i<17;i++)    
    {
      n=base*i;
      printf("\n %i ...",n);
      fflush(stdout);
      plan=rfftw_create_plan(n,FFTW_FORWARD, FFTW_MEASURE | FFTW_USE_WISDOM);
      fftw_destroy_plan(plan);
      plan=rfftw_create_plan(n,FFTW_BACKWARD,FFTW_MEASURE | FFTW_USE_WISDOM);
      fftw_destroy_plan(plan);
      printf(" done.");
    }
  out_file=fopen(namefile, "w");
  fftw_export_wisdom_to_file(out_file);
  fclose(out_file);
  printf("\n Next time you use MacLucasFFTW, copy 'fftw.pln' file");
  printf("\n to work directory to get maximum performance.\n");
  return(0);
}
