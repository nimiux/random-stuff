#define DRCStrig_h "$Id: trig.h,v 6.50 1999/12/19 06:06:20 wedgingt Exp $"

#define GOOD_TRIG

#ifndef GOOD_TRIG
#define FAST_TRIG
#endif

#define TRIG_TABLE_SIZE (32)

#ifdef GOOD_TRIG

#define FHT_SWAP(a,b,t) {(t)=(a);(a)=(b);(b)=(t);}

#define TRIG_VARS int t_lam=0;

#define TRIG_INIT(k2,c2,s2)					 \
     {								 \
      int ti;							 \
      if (k2 >= TRIG_TABLE_SIZE)				 \
          {							 \
           fprintf(stderr, "%s: trig tables not large enough; contact wedgingt@acm.org\n", program_name); \
           exit(1);						 \
          }							 \
      for (ti=0 ; ti<=k2 ; ti++)				 \
          {coswrk[ti]=costab[ti];sinwrk[ti]=sintab[ti];}	 \
      t_lam = 0;					         \
      c2 = 1;							 \
      s2 = 0;							 \
     }

#define TRIG_NEXT(k,c,s)					 \
     {								 \
	 int ti,tj;	                                         \
         (t_lam)++;	  					 \
         for (ti=0 ; !((1<<ti)&t_lam) ; ti++);			 \
         ti = k-ti;						 \
         s = sinwrk[ti];					 \
         c = coswrk[ti];					 \
         if (ti>1)   						 \
            {	    						 \
             for (tj=k-ti+2 ; (1<<tj)&t_lam ; tj++);			\
             tj	        = k - tj;					\
             sinwrk[ti] = halsec[ti] * (sinwrk[ti-1] + sinwrk[tj]);	\
             coswrk[ti] = halsec[ti] * (coswrk[ti-1] + coswrk[tj]);	\
            }								\
     }

#define TRIG_RESET(k,c,s)

#endif

#ifdef FAST_TRIG

#define TRIG_VARS REAL t_c, t_s;

#define TRIG_INIT(k,c,s)				 \
    {				        		 \
     t_c  = costab[k];				         \
     t_s  = sintab[k];				         \
     c    = 1;				    		 \
     s    = 0;				    		 \
    }

#define TRIG_NEXT(k,c,s)				 \
    {                                                    \
     REAL t = c;                                         \
     c   = t*t_c - s*t_s;				 \
     s   = t*t_s + s*t_c;				 \
    }

#define TRIG_RESET(k,c,s)

#endif

extern REAL halsec[];
extern REAL costab[];
extern REAL sintab[];
extern REAL coswrk[];
extern REAL sinwrk[];
