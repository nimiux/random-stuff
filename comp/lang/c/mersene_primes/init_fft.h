#define DRCSinit_fft_h "$Id: init_fft.h,v 6.50 1999/12/19 06:06:20 wedgingt Exp $"

#include "init_real.h"

#ifdef __STDC__
extern void init_fft(UL n);
#else
extern void init_fft();
#endif
