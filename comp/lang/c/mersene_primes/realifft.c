static const char RCSrealifft_c[] = "$Id: realifft.c,v 6.50 1999/12/19 06:06:20 wedgingt Exp $";

#include "setup.h"
#include "fht.h"
#include "realifft.h"

#ifdef __STDC__
void realifft(UL n, BIG_DOUBLE *real)
#else
void realifft(n, real)
UL n;
BIG_DOUBLE *real;
#endif
 {
  BIG_DOUBLE a,b;
  UL i,j,k;

  for (i = 1, j = n - 1, k = n/2; i < k; i++, j--)
   {
    a = real[i];
    b = real[j];
    real[j] = (a - b);
    real[i] = (a + b);
   }
  fht(real, n);
  for (i = 0; i < n; i++)
    real[i] /= n;
  return;
 }
