/* $Id: facgmp.h,v 6.50 1999/12/19 06:06:20 wedgingt Exp $ */
/* This include file should be included if you want to use the */
/* freelip package to perform the Mersenne factoring.  The advantage */
/* of freelip is that it will handle arbitrary sized factors. */

#include <stdio.h>
#include "gmp.h"

/*-------------------------------------*/
/* Start of user-definable parameters  */
/*-------------------------------------*/

/* There are no user-definable parameters here */

/*-----------------------------------*/
/* End of user-definable parameters  */
/*-----------------------------------*/

/* This data type is used for handling potential factors of */
/* Mersenne numbers.  The size of these factors is limited */
/* only by the math package used for the large integer arithmetic. */
typedef mpz_t FACTOR;


/* These are the GNU MP equivalents of the macros used in factor.c */
/* Speed is not important in the first of these macros */

/* This macro returns the largest exponent of two that can be safely tested */
/*  0 means there is no maximum besides memory limits */

#define fac_max_exp_of_2	0

/* This macro performs initialization of the large integer math package. */
/* This macro should be called only once during a program. */

/* gmp itself nees no initialization, but these variables do */
#define fac_init() (fac_init_var(savef), fac_init_var(numer), fac_init_var(tmpfac), fac_init_var(tmpfac2))
extern FACTOR tmpfac, tmpfac2;

/* This macro does any required initialization per variable */
/*  sort of like a C++ constructor except zero is always assigned */

#define fac_init_var(f)		(mpz_init(f))

/* This macro covers the C++ destructor case, usually freeing any */
/*  memory that the variable had malloc'd */

#define fac_dest_var(f)		mpz_clear(f)

/* This macro sets a factor to the value of a different factor */

#define fac_set(srcf,destf)	mpz_set(*(destf), (srcf))

/* This macro initializes a factor with an integer value */

#if SIZEOF_UL <= 4
# define fac_set_int(i,f)	mpz_set_ui(*(f), (i))
#else
# define fac_set_int(i,f)	fac_set_ull((i), (f))
extern void fac_set_ull(UL i, FACTOR *f);
#endif

/* This macro compares a factor to an integer */

#if SIZEOF_UL <= 4
# define fac_less_than_int(f,i)	(mpz_cmp_ui((f), (i)) < 0)
#else
# define fac_less_than_int(f,i) (fac_less_than_ull((f), (i)))
extern int fac_less_than_ull(FACTOR f, UL i);
#endif

/* This macro compares one factor to another factor */

#define fac_less_than(f1,f2)	(mpz_cmp((f1), (f2)) < 0)

/* This macro divides a factor by an integer.  The result is */
/* the floor(factor/integer) */

#if SIZEOF_UL <= 4
# define fac_div_int(fsrc,i,fdest)	(mpz_tdiv_q_ui(*(fdest), (fsrc), (i)))
#else
# define fac_div_int(fsrc,i,fdest)	(fac_div_ull((fsrc), (i), (fdest)))
extern void fac_div_ull(FACTOR fsrc, UL i, FACTOR *fdest);
#endif

/* This macro multiplies a factor by an integer */

#if SIZEOF_UL <= 4
# define fac_mul_int(fsrc,i,fdest)	(mpz_mul_ui(*(fdest), (fsrc), (i)))
#else
# define fac_mul_int(fsrc,i,fdest)	(fac_mul_ull((fsrc), (i), (fdest)))
extern void fac_mul_ull(FACTOR fsrc, UL i, FACTOR *fdest);
#endif

/* This macro adds a factor and an integer */

#if SIZEOF_UL <= 4
# define fac_add_int(fsrc,i,fdest)	(mpz_add_ui(*(fdest), (fsrc), (i)))
#else
# define fac_add_int(fsrc,i,fdest)	(fac_add_ull((fsrc), (i), (fdest)))
extern void fac_add_ull(FACTOR fsrc, UL i, FACTOR *fdest);
#endif

/* This macro adds two factors */

#define fac_add(fsrc1, fsrc2, fdest)	(mpz_add(*(fdest), (fsrc1), (fsrc2)))

/* This macro multiplies two factors and is only used in rw.c:input() */

#define fac_mul(fsrc1, fsrc2, fdest)	(mpz_mul(*(fdest), (fsrc1), (fsrc2)))

/* This macro divides a factor by another factor and is only used by 1strs8.c */
#define fac_div(fnumer, fdenom, fquot, frem)	(mpz_tdiv_qr(*(fquot), *(frem), (fnumer), (fdenom)))

/* This macro subtracts an integer from a factor */

#if SIZEOF_UL <= 4
# define fac_sub_int(fsrc,i,fdest)	(mpz_sub_ui(*(fdest), (fsrc), (i)))
#else
# define fac_sub_int(fsrc,i,fdest)	(fac_sub_ull((fsrc), (i), (fdest)))
extern void fac_sub_ull(FACTOR fsrc, UL i, FACTOR *fdest);
#endif

/* This macro subtracts two factors and is only used in rw.c:input() */

#define fac_sub(fsrc1, fsrc2, fdest)	(mpz_sub(*(fdest), (fsrc1), (fsrc2)))

/* This macro returns factor modulo an integer */

#if SIZEOF_UL <= 4
# define fac_mod_int(f,i)		(mpz_fdiv_ui((f), (i)))
#else
# define fac_mod_int(f,i)		(fac_mod_ull((f), (i)))
extern UL fac_mod_ull(FACTOR f, UL i);
#endif

/* This macro returns the factor modulo 8 */

#define fac_mod8(f)			(mpz_fdiv_ui((f), 8))

/* This macro returns the value of the i'th bit (0 for units bit) of the factor f */

#define fac_bit(f, i)			(mpz_scan0((f), (i)) != (i))

/* This macro squares a factor */

#define fac_square(fsrc,fdest)		(mpz_mul(*(fdest), (fsrc), (fsrc)))

/* This macro doubles a factor */

#define fac_times2(fsrc,fdest)		(mpz_mul_2exp(*(fdest), (fsrc), 1))

/* This macro alerts the math package that several */
/* successive modulo operations of this numerator will take place */

#define fac_prepare_numerator(f)	(mpz_set((numer), (f)))


/* The following are the speed-critical macros */


/* This macro alerts the math package that several */
/* successive modulo operations are about to take place */

#define fac_prepare_denominator(f)	(mpz_set((savef), (f)))

/* This macro takes performs a modulo operation using the */
/* numerator sent to fac_prepare_numerator and the factor */
/* sent to fac_prepare_denominator */

#define fac_mod(fdest)			(mpz_mod(*(fdest), (numer), (savef)))

/* This macro squares a factor then performs a modulo operation */
/* using the factor sent to fac_prepare_denominator */

#define fac_square_mod(fsrc,fdest)	(mpz_mul(*(fdest),(fsrc),(fsrc)),mpz_mod(*(fdest),*(fdest),(savef)))

/* This macro doubles a factor modulo the factor sent to */
/* fac_prepare_denominator.  NOTE: The modulo operation can be skipped */
/* as long as fac_square_mod can handle the larger number. */

#define fac_times2_mod(fsrc,fdest)	fac_times2((fsrc), (fdest))

/* This macro returns TRUE if f equals one OR if fac_times2 */
/* did not perform the modulo operation this macro returns */
/* TRUE if f equals saved-denominator + 1 */

#define fac_is_winner(f)		(mpz_sub((f), (f), (savef)), mpz_cmp_ui((f), 1) == 0)
