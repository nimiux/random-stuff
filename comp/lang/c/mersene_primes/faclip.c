static const char RCS_faclip_c[] = "$Id: faclip.c,v 6.50 1999/12/19 06:06:20 wedgingt Exp $";

#define FACTOR_PACKAGE FACTOR_FREELIP

#include "setup.h"
#include "factor.h"

FACTOR tmpfac, tmpfac2;

#ifdef __STDC__
int fscan_fac(FILE *fp, FACTOR *f)
#else
int fscan_fac(fp, f)
FILE *fp;
FACTOR *f;
#endif
 {
  int ch = fgetc(fp);
  
  while (isascii(ch) && isspace(ch))
    ch = fgetc(fp);
  if (!isascii(ch))
    return(EOF);
  if (!isdigit(ch))
   {
    (void)ungetc(ch, fp);
    return(0);
   }
  for (zzero(f); isascii(ch) && isdigit(ch); ch = fgetc(fp))
   {
    zsmul(*f, 10, f);
    zsadd(*f, ch - '0', f);
   }
  (void)ungetc(ch, fp);
  return(1);
 }

#ifdef __STDC__
int sscan_fac(char *str, FACTOR *f)
#else
int sscan_fac(str, f)
char *str;
FACTOR *f;
#endif
 {
  while (*str != '\0' && isascii(*str) && isspace(*str))
    str++;
  if (*str == '\0')
    return(EOF);
  if (!isascii(*str) || !isdigit(*str))
    return(0);
  for (zzero(f); *str != '\0' && isascii(*str) && isdigit(*str); str++)
   {
    zsmul(*f, 10, f);
    zsadd(*f, *str - '0', f);
   }
  return(1);
 }

#ifdef __STDC__
int fprint_fac(FILE *fp, FACTOR f)
#else
int fprint_fac(fp, f)
FILE *fp;
FACTOR f;
#endif
 {
  return((int)zfwrite(fp, f));
 }

/* this #if covers the rest of the file */
#if SIZEOF_UL > 4

# ifdef __STDC__
void fac_set_ull(UL i, FACTOR *f)
#else
void fac_set_ull(i, f)
UL i;
FACTOR *f;
# endif
 {
  if (i < RADIX)
    zintoz((long)i, f);
  else
   {
    zintoz((long)(UL)(i >> 2*NBITS), f); /* most significant 32 bits */
    zlshift(*f, NBITS, f);
    zsadd(*f, (long)((UL)(i >> NBITS) & RADIXM), f); /* middle NBITS bits */
    zlshift(*f, NBITS, f);
    zsadd(*f, (long)(UL)(i & RADIXM), f); /* least significant NBITS bits */
   }
 }

# ifdef __STDC__
int fac_less_than_ull(FACTOR f, UL i)
# else
int fac_less_than_ull(f, i)
FACTOR f;
UL i;
# endif
 { /* zscompare() always calls zcompare() */
  fac_set_ull(i, &tmpfac);
  return(zcompare(f, tmpfac) < 0);
 }

# ifdef __STDC__
UL fac_div_ull(FACTOR fsrc, UL i, FACTOR *fdest)
# else
UL fac_div_ull(fsrc, i, fdest)
FACTOR fsrc;
UL i;
FACTOR *fdest;
# endif
 {
  if (i < RADIX)
    return(zsdiv(fsrc, (long)i, fdest));
  fac_set_ull(i, &tmpfac);
  zdiv(fsrc, tmpfac, fdest, &tmpfac);
  i = zsdiv(tmpfac, RADIX, &tmpfac2);
  i += (UL)RADIX*(UL)zsdiv(tmpfac2, RADIX, &tmpfac);
  i += (UL)RADIX*(UL)RADIX*zsdiv(tmpfac, RADIX, &tmpfac2);
  return(i);
 }

# ifdef __STDC__
void fac_mul_ull(FACTOR fsrc, UL i, FACTOR *fdest)
# else
void fac_mul_ull(fsrc, i, fdest)
FACTOR fsrc;
UL i;
FACTOR *fdest;
# endif
 {
  if (i < RADIX)
    zsmul(fsrc, (long)i, fdest);
  else
   {
    fac_set_ull(i, &tmpfac);
    zcopy(fsrc, &tmpfac2); /* in case fsrc == *fdest, which it usually does */
    zmul(tmpfac2, tmpfac, fdest);
   }
 }

# ifdef __STDC__
void fac_add_ull(FACTOR fsrc, UL i, FACTOR *fdest)
# else
void fac_add_ull(fsrc, i, fdest)
FACTOR fsrc;
UL i;
FACTOR *fdest;
# endif
 { /* zsadd() always calls zadd() */
  fac_set_ull(i, &tmpfac);
  zadd(fsrc, tmpfac, fdest);
 }

# ifdef __STDC__
void fac_sub_ull(FACTOR fsrc, UL i, FACTOR *fdest)
# else
void fac_sub_ull(fsrc, i, fdest)
FACTOR fsrc;
UL i;
FACTOR *fdest;
# endif
 { /* there is no zssub() */
  fac_set_ull(i, &tmpfac);
  zsub(fsrc, tmpfac, fdest);
 }

# ifdef __STDC__
UL fac_mod_ull(FACTOR fsrc, UL i)
# else
UL fac_mod_ull(fsrc, i)
FACTOR fsrc;
UL i;
# endif
 {
  if (i < RADIX)
    return(zsdiv(fsrc, (long)i, &tmpfac));
  fac_set_ull(i, &tmpfac);
  zdiv(fsrc, tmpfac, &tmpfac2, &tmpfac);
  i = zsdiv(tmpfac, RADIX, &tmpfac2);
  i += (UL)RADIX*(UL)zsdiv(tmpfac2, RADIX, &tmpfac);
  i += (UL)RADIX*(UL)RADIX*zsdiv(tmpfac, RADIX, &tmpfac2);
  return(i);
 }

#endif
