static const char RCSsquare_c[] = "$Id: square.c,v 6.50 1999/12/19 06:06:20 wedgingt Exp $";

#include "setup.h"
#include "herm2real.h"
#include "real2herm.h"
#include "sq_herm.h"
#include "square.h"

#ifdef __STDC__
void squareg(BIG_DOUBLE *x, UL size)
#else
void squareg(x, size)
BIG_DOUBLE *x;
UL size;
#endif
{
  fft_real_to_hermitian(x, size);
  square_hermitian(x, size);
  fftinv_hermitian_to_real(x, size);
}
