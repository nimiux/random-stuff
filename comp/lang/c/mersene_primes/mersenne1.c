static char RCSmersenne1_c[] = "$Id: mersenne1.c,v 6.50 1999/12/19 06:06:20 wedgingt Exp $ by wedgingt@acm.org";
char *RCSprogram_id = RCSmersenne1_c;

#ifdef FHT
const char *program_name = "mersenne1"; /* for perror() and similar */
#else
const char *program_name = "mersenne2"; /* for perror() and similar */
#endif

const char program_revision[] = "$Revision: 6.50 $";
char version[sizeof(RCSmersenne1_c) + sizeof(program_revision)]; /* overly long, but certain */

/* See README.mersenne1 for notes and information on this program and */
/*  the algorithm it uses. */

#ifdef macintosh
# include <console.h>
#endif

#include "setup.h"
#include "balance.h"
#include "fht.h"
#include "globals.h"
#include "rw.h"
#include "lucas_init.h"
#include "realfft.h"
#include "realifft.h"
#include "zero.h"

#ifdef __STDC__
int main(int argc, char *argv[])
#else
int main(argc, argv)
int argc;
char *argv[];
#endif
 {
  BIG_DOUBLE *x = NULL;
  long j = 0L, k = 0L;
  UL n = 8L, q = 0L, b = 0L, c = 0L, i = 0L, last = 2L;
  long bj = 0L, NminusOne = 0L, carry = 0L;
  BIG_DOUBLE hi, lo, highliminv, lowliminv;
  BIG_DOUBLE xx;
  BIG_DOUBLE highlim, lowlim, lim, inv, base;
  BIG_DOUBLE error, maxerror = 0.0, error_limit = 0.43751, error_limit_1000 = 0.35;
  BIG_DOUBLE temp14;
#ifdef FHT
  BIG_DOUBLE temp13;
  int s,t,u;
#else
  BIG_DOUBLE d, e;
#endif
  int restarting = 0; /* flag: restarting with a larger FFT run length (n)? */
  char M = '\0'; /* 'U', 'I', 'H', etc. */
  FILE *infp = NULL, *outfp = NULL, *dupfp = NULL;
  int primenet = 0; /* flag: interact with PrimeNet?  -N option; see rw.c and rw.h */

#ifdef macintosh
  argc = ccommand(&argv);
#endif
  strcpy(version, program_name);
  strcat(version, " v");
  strncat(version, program_revision + strlen("4Revision: "),
          strlen(program_revision) - strlen("4Revison:  4"));
  /*!!will need a flag for this next part after merging in Kline's FHT/FFT code */
  strcat(version, " Kline");
  setup();
  while (!terminate)
   {
    do /* while (maxerror >= error_limit) */
     {
      switch ((restarting != 0) ? 3 :
              input(argc, argv, &q, &n, &i, &error, &x, last, &M, NULL, NULL, NULL, NULL, NULL,
                    (UL)0, (UL)0, &infp, &outfp, &dupfp, &primenet))
       {
        case 0: /* no more input */
          print_time();
          return(0);
        case 1: /* something wrong; error message, if any, already printed */
          print_time();
          return(1);
        case 2: /* continuing work from a prior result */
          restarting = 1; /* not the usual sense of restarting, but ... */
          break;
        case 3: /* starting work on a new exponent or restarting with larger FFT run length */
          while (n < (2.6*(BIG_DOUBLE)q)/64.0)
            n <<= 1;
          if (x != NULL)
            free((char *)x);
          if ((x = (BIG_DOUBLE *)calloc(n, sizeof(BIG_DOUBLE))) == NULL)
           {
            perror(program_name);
            (void)fprintf(stderr, "%s: cannot get memory for FFT array\n", program_name);
            print_time();
            return(errno == 0 ? 1 : errno);
           }
          for (k = 1; k < n; k++)
            x[k] = 0.0;
          x[0] = 4.0;
          i = 1;
          break;
       }
      if (!restarting)
       {
        if (M != 'U' && M != 'I' && last != q - 1)
          (void)fprintf(stderr, "%s: exponent " PRINTF_FMT_UL " should not need Lucas-Lehmer testing\n", program_name, q);
        if ((M != 'U' && M != 'I') || last == q - 1)
          continue;
       }
      restarting = 0;
      init_lucas(q, n, &b, &c);
      NminusOne = n - 1;
      hi = high;
      lo = low;
      carry = 0;
      highlim = hi*0.5;
      lowlim = lo*0.5;
      highliminv = 1.0/highlim;
      lowliminv = 1.0/lowlim;
      maxerror = 0.0;

      for (last = q - 1; !terminate && i < last; i++)
       {		

#ifdef FHT
        fht(x, n);
        for (s = 1, t = n - 1, u = n/2; s < u; s++, t--)
         {
          temp14 = 0.5*(x[s] + x[t])*(x[s] - x[t]);
          temp13 = x[s]*x[t];
          x[t] = temp13 - temp14;
          x[s] = temp13 + temp14;
         } 
#else /* FHT */
        realfft(n, x);			
#endif /* FHT */

        x[0] *= x[0];
        x[n/2] *= x[n/2];

#ifdef FHT
        fht(x, n);		
        xx = x[0]/n;
#else /* FHT */
        for (j = 1; j < n/2; j++)
         {
          e = x[j];
          d = x[n - j];
          x[n - j] = 2.0*e*d;
          x[j] = (e + d)*(e - d);
         }		 
        realifft(n, x);			
        xx = x[0];
#endif /* FHT */

        temp14 = xx;
        if (xx < 0.0)
          xx = floor(xx + 0.5);
        else
          xx = ceil(xx - 0.5);
        error = fabs(temp14 - xx);
        if (error > maxerror)
          if ((maxerror = error) >= error_limit || (i <= 1000 && maxerror >= error_limit_1000))
            break;
        if (xx >= highlim)
          carry =((long)(xx*highliminv + 1)) >> 1;
        else
          if (xx < -highlim)
            carry = -(((long)(1 - xx*highliminv)) >> 1);
          else
            carry = 0;
        x[0] = xx - carry*hi;
        bj = b;
        for (j = 1; j < n - 1; j++)
         {

#ifdef FHT
          x[j] /= (two_to_phi[j]*n);
#else /* FHT */
          x[j] /= two_to_phi[j];
#endif /* FHT */

          temp14 = x[j];
          if (x[j] < 0.0)
            x[j] = floor(x[j] + 0.5);
          else
            x[j] = ceil(x[j] - 0.5);	
          error = fabs(temp14 - x[j]);
          if (error > maxerror)
            if ((maxerror = error) >= error_limit || (i <= 1000 && maxerror >= error_limit_1000))
              break;
          xx = x[j] + carry;
          if ((bj & NminusOne) >= c)
           {
            if (xx >= highlim)
              carry = ((long)(xx*highliminv + 1)) >> 1;
            else
              if (xx < -highlim)
                carry = -(((long)(1 - xx*highliminv)) >> 1);
              else
                carry = 0;
            x[j] = xx - carry*hi;
           }
          else
           {
            if (xx >= lowlim)
              carry = ((long)(xx*lowliminv + 1)) >> 1;
            else
              if (xx < -lowlim)
                carry = -(((long)(1 - xx*lowliminv)) >> 1);
              else
                carry = 0;
            x[j] = xx - carry*lo;
           }
          bj += b;
          x[j] *= two_to_phi[j];
         }	
        if (maxerror >= error_limit || (i <= 1000 && maxerror >= error_limit_1000))
          break;

#ifdef FHT
        xx = x[n - 1]/(two_to_phi[n - 1]*n);
#else /* FHT */
        xx = x[n - 1]/two_to_phi[n - 1];
#endif /* FHT */

        xx = xx + carry;
        temp14 = xx;		
        if (xx < 0.0)
          xx = floor(xx + 0.5);
        else
          xx = ceil(xx - 0.5);
        error = fabs(temp14 - xx);
        if (error > maxerror)
          if ((maxerror = error) >= error_limit || (i <= 1000 && maxerror >= error_limit_1000))
            break;
        if (xx >= lowlim)
          carry = ((long)(xx*lowliminv + 1)) >> 1;
        else
          if (xx < -lowlim)
            carry = -(((long)(1 - xx*lowliminv)) >> 1);
          else
            carry = 0;
        x[n - 1] = xx - carry*lo;
        x[n - 1] *= two_to_phi[n - 1];		
        if (carry)
         {
          j = 0;
          bj = 0;
          while (carry)
           {
            xx = x[j]/two_to_phi[j];	
            xx = xx + carry;		
            if (j == 0)
             {
              lim = highlim;
              inv = highliminv;
              base = hi;
             }
            else
              if (j == NminusOne)
               {
                lim = lowlim;
                inv = lowliminv;
                base = lo;
               }
              else
                if ((bj & NminusOne) >= c)
                 {
                  lim = highlim;
                  inv = highliminv;
                  base = hi;
                 }
                else
                 {
                  lim = lowlim;
                  inv = lowliminv;
                  base = lo;
                 }
            if (xx >= lim)
              carry = ((long)(xx*inv + 1)) >> 1;
            else
              if (xx < -lim)
                carry = -(((long)(1 - xx*inv)) >> 1);
              else
                carry = 0;
            x[j] = xx - carry*base;
            x[j] = x[j]*two_to_phi[j];
            j = j + 1;
            bj += b;
            if (j == n)
             {
              j = 0;
              bj = 0;
             }
           }
         }
        x[0] = x[0] - 2.0;	
        if (i + 1 < last && (terminate || (chkpnt_iterations != 0L && (i % chkpnt_iterations) == 1)) &&
            (check_point(q, n, i + 1, error, x, (UL)0, (UL)0, primenet) <= 0 || terminate))
         {
          print_time();
          return(errno);
         }
       }
      if (maxerror >= error_limit || (i <= 1000 && maxerror >= error_limit_1000))
       {
#ifdef PRINT_FFT_LENGTH_INCREASES
        printf("Error too big with FFT length " PRINTF_FMT_UL "\n", n);
        printf("  maxerror %.20f limit %.20f\n", (double)maxerror,
               (double)((i <= 1000) ? error_limit_1000 : error_limit));
#endif
        n <<= 1;
        restarting = 1;
       }
     } while (maxerror >= error_limit || (i <= 1000 && maxerror >= error_limit_1000));
    if (i < last && check_point(q, n, i, error, x, (UL)0, (UL)0, primenet) <= 0)
     {
      print_time();
      return(errno);
     }
    if (i < last)
      return(0); /* check_point() succeeded */
    restarting = 0;
    for (j = 0; j < n; j++)
     {
      x[j] /= two_to_phi[j];
      if (x[j] < 0.0)
        x[j] = floor(x[j] + 0.5);
      else
        x[j] = ceil(x[j] - 0.5);
     }			
    printbits(x, q, n, (q > 64L) ? 64L : q, b, c, high, low, (UL)0, (UL)0, version, outfp, dupfp, primenet);
    print_time();
   }
  return(0);
 }
