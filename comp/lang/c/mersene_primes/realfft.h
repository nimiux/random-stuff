#define DRCSrealfft_h "$Id: realfft.h,v 6.50 1999/12/19 06:06:20 wedgingt Exp $"

#ifdef __STDC__
extern void realfft(UL n, BIG_DOUBLE *real);
#else
extern void realfft();
#endif
