#define DRCSglobals_h "$Id: globals.h,v 6.50 1999/12/19 06:06:20 wedgingt Exp $"

extern BIG_DOUBLE *cn, *sn, *two_to_phi, *two_to_minusphi, *scrambled;
extern BIG_DOUBLE high, low, highinv, lowinv;
extern long *permute;
extern int partial; /* read a partial result? */
#ifdef DEBUG
extern BIG_DOUBLE highest_err;
#endif
