static char RCSmersfac_c[] = "$Id: mersfac.c,v 6.50 1999/12/19 06:06:20 wedgingt Exp $ by wedgingt@acm.org";
char *RCSprogram_id = RCSmersfac_c; /*!!inadequate, as it says nothing about the factoring package */
const char program_revision[] = "$Revision: 6.50 $"; /*!!same problem */

#ifdef macintosh
# include <console.h>
#endif

#include "setup.h"
#include "factor.h"
#include "rw.h"

const char *program_name = NULL;

/*ARGSUSED*/
#ifdef __STDC__
int main(int argc, char **argv)
#else
int main(argc, argv)
int argc;
char **argv;
#endif
 {
  EXPONENT p;
  FACTOR start, stop, tmp;
  int all_factors = 0; /* flag: find all factors? or just first one? */
  int small_test = 0; /* flag: don't test very many trial factors? */
  char M; /* C, D, H, I, or U */
  FILE *infp = NULL, *outfp = NULL, *dupfp = NULL;
  int task = 0; /* return value from rw.c input(); only really needed for exit status */
  int primenet = 0; /* flag: interact with PrimeNet?  -N option; see rw.c and rw.h */

#ifdef macintosh
  argc = ccommand(&argv);
#endif
  if (argv == NULL || argv[0] == NULL)
    program_name = "mersfac[unknown-type]";
  else
    for (program_name = argv[0] + strlen(argv[0]); program_name > argv[0] && *program_name != '/'; )
      --program_name;
  if (*program_name == '/')
    ++program_name;
  setup();
  /* init the math package and variables */
  fac_init();
  fac_init_var(start);
  fac_init_var(stop);
  fac_init_var(tmp);
  fac_set_int((UL)1, &stop);
  while (!terminate && 
         (task = input(argc, argv, &p, NULL, NULL, NULL, NULL, (EXPONENT)0, &M, &start, &stop, &all_factors,
                       &small_test, NULL, (UL)0, (UL)0, &infp, &outfp, &dupfp, &primenet)) > 1)
   {
    if (presence("UHIG", M) == NULL || p % 2 == 0)
      continue; /* we can only improve on trial factoring data for prime exponents */
    if (fac_mod_int(start, (UL)2) == 0) /* if it's even ... */
      fac_sub_int(start, (UL)1, &start); /* then make it odd. */
    if (fac_mod_int(start, (UL)2) == 0) /* if it's still even ... */
     { /* then it's too large to represent accurately in a FACTOR's floating point mantissa */
      fprintf(stderr, "%s: " PRINTF_FMT_UL " tested too far for this factoring program\n", argv[0], p);
      continue;
     }
    fac_set_int(p, &tmp);
    if (small_test || (!all_factors && M != 'U' && M != 'I' && M != 'G'))
     {
      fac_mul_int(tmp, (UL)2400, &tmp);
      fac_add(start, tmp, &stop);
     }
    else
     {
      fac_mul_int(tmp, (UL)4, &tmp);
      fac_add_int(tmp, (UL)2, &tmp);
      if (fac_less_than(stop, tmp))
       { /* go slightly past the next power of two */
        fac_set_int((UL)1 << 14, &tmp);
        while (fac_less_than(tmp, start))
         {
          fac_set(tmp, &stop);
          fac_times2(stop, &tmp);
         }
        fac_add_int(tmp, p, &stop);
        fac_add_int(stop, p, &tmp);
        fac_add_int(tmp, (UL)1, &stop);
       }
     }
    if (fac_mod_int(stop, (UL)2) == 0) /* must be too large to represent, ... */
     { /* so try just above start: */
      fac_add_int(start, p, &stop);
      fac_add_int(stop, p, &stop);
     }
    if (fac_mod_int(stop, (UL)2) == 0 || !fac_less_than(start, stop)) /* if still not odd or <= start ... */
     { /* then it's too large to represent accurately in a FACTOR, so ... */
      fprintf(stderr, "%s: " PRINTF_FMT_UL " tested too far for this factoring program\n", argv[0], p);
      continue;
     }
    mersenne_factor(p, start, stop, all_factors, M, outfp, dupfp);
    print_time();
   }
  return(task == 1);
 }
