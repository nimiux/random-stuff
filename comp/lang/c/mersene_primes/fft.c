static char RCSfft_c[] = "$Id: fft.c,v 6.50 1999/12/19 06:06:20 wedgingt Exp $";

#include "setup.h"

#ifdef __STDC__
void fft(long n, BIG_DOUBLE *real, BIG_DOUBLE *imag)
#else
void fft(n, real, imag)
long n;
BIG_DOUBLE *real,*imag;
#endif
 {
  BIG_DOUBLE a, b, c, d;
  BIG_DOUBLE q, r, s, t;
  long i, j, k;

  for (i = 1, j = n - 1, k = n/2; i < k; i++, j--)
   {
    a = real[i];
    b = real[j];
    q = a + b;
    r = a - b;
    c = imag[i];
    d = imag[j];
    s = c + d;
    t = c - d;
    real[i] = (q + t)*.5;
    real[j] = (q - t)*.5;
    imag[i] = (s - r)*.5;
    imag[j] = (s + r)*.5;
   }
  fht(real, n);
  fht(imag, n);
 }
