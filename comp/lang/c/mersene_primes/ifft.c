static char RCSifft_c[] = "$Id: ifft.c,v 6.50 1999/12/19 06:06:20 wedgingt Exp $";

#include "setup.h"

#ifdef __STDC__
void ifft(int n, BIG_DOUBLE *real, BIG_DOUBLE *imag)
#else
void ifft(n,real,imag)
int n;
BIG_DOUBLE *real, *imag;
#endif
 {
  BIG_DOUBLE a,b,c,d;
  BIG_DOUBLE q,r,s,t;
  int i,j,k;

  fht(real, n);
  fht(imag, n);
  for (i = 1, j = n - 1, k = n/2; i < k; i++, j--)
   {
    a = real[i];
    b = real[j];
    q = a + b;
    r = a - b;
    c = imag[i];
    d = imag[j];
    s = c + d;
    t = c - d;
    imag[i] = (s + r)*0.5;
    imag[j] = (s - r)*0.5;
    real[i] = (q - t)*0.5;
    real[j] = (q + t)*0.5;
   }
 }
