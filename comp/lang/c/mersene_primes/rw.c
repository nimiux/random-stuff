/*!!time & date stamp error messages? */
/*!!Other input formats:
  M( exponent )letter: k=value [k2=value]
  M( exponent )Z [no more input for exponent]
  M( M( exponent ) )[...]
  all factors 1
  all factors 0
  [other lines that change flags given on command line?]
 */

static const char RCSrw_c[] = "$Id: rw.c,v 6.50 1999/12/19 06:06:20 wedgingt Exp $ by wedgingt@acm.org";
static const char RCSrw_revision[] = "$Revision: 6.50 $";

static const char Options[] =
"-\t\tread stdin (default if no files or exponents given)\n"
#ifdef FACTOR_PACKAGE
"-a\t\ttry to find all factors, not just one\n"
#else
"-b\t\tuse binary Lucas-Lehmer checkpoint files\n"
"-c#\t\tcheckpoint LL test every # iterations\n"
#endif
"-d\t\tduplicate output to stderr\n"
"-d-\t\tduplicate output to stderr\n"
"-dstderr\tduplicate output to stderr\n"
"-dstdout\tduplicate output to stdout\n"
"-d<fn>\t\tappend duplicate output to file <fn>\n"
"-e#\t\tstop searching for factors at 2^#\n"
"-h\t\tuse human-readable LL checkpoint files (fftlucas only)\n"
"-l#\t\tlimit bound to # for P-1 (only meaningful to merspm1)\n"
"-m#\t\tmax # of trial factors to attempt\n"
#ifdef TCPIP
# ifndef FACTOR_PACKAGE
#  ifdef PRIMENET_SUPPORT
"-N<host>\tuse <host> as PrimeNet server\n"
"\t\t(default <host> is entropia.com)\n"
#  endif
# endif
"-n<host>\tconnect to <host> via TCP/IP for input and output\n"
"\t\t(mersarch.txt used for output during network outages)\n"
"\t\t(no later arguments will be processed)\n"
"\t\t(default <host> is localhost)\n"
#endif
"-o\t\tsend output to stderr\n"
"-o-\t\tsend output to stdout\n"
"-ostderr\tsend output to stderr\n"
"-ostdout\tsend output to stdout\n"
"-o<fn>\t\tappend output to file <fn>\n"
#ifdef TCPIP
"-p#\t\tuse port # when connecting to a server with -n\n"
"\t\t(default is 2203 with -n and 80 with -N; useful only before -n/-N)\n"
#endif
"-r# #\t\tskip exponents outside range from first # to second #\n"
"-s\t\tsmall test (mostly for the Makefile to test factorers)\n"
"-t\t\tprint CPU time, wall clock time, etc. if available\n"
"-v\t\tprint brief version information\n"
"<fn>\t\tread from file <fn> (Mersenne exponents, 'extract -2'\n"
"\t\toutput, http://www.garlic.com/~wedgingt/mersfmt.html formats)\n"
"<p>\t\ttest Mersenne exponent <p>\n"
"\t\t(<p> will be tried as a file first)\n";

#include "setup.h"
#include "balance.h"

#ifdef FACTOR_PACKAGE
# include "factor.h"
#else
# define FACTOR char
#endif

#include "rw.h"
#include <time.h>

#if defined(sun) && !defined(SOLARIS)
extern time_t time(time_t *tloc);
#endif

static time_t start_time;

#if defined(USE_RUSAGE)
# include <sys/resource.h>
int getrusage PROTO((int who, struct rusage *usage));
#endif

extern char *RCSprogram_id; /* RCS Id info for .c file with main() in it */

/* Checkpoints are the save files for the Lucas-Lehmer test programs, */
/*  mersenne1, mersenne2, and fftlucas */

UL chkpnt_iterations = 0;

/* checkpoint file formats: human readable and machine-specific binary with magic number ala file(1) */
#define CHKPNT_ASCII 0
#define CHKPNT_BINARY 1
#define MAGIC_NUMBER (0x76757473)

static int chkpnt_fmt = CHKPNT_BINARY;
static char chkpnt_cfn[CHKPNT_FILENAME_LENGTH];
static char chkpnt_tfn[CHKPNT_FILENAME_LENGTH];
static char first_time_here = 1; /* first call to input() in this program? */
static EXPONENT start_range = (UL)31, /* smallest exponent that can be tested */
  stop_range = (~(UL)0)/2; /* this is probably the largest */

#ifdef TCPIP
static const char *server_host = NULL;
static int server_port = 0;

# ifdef PROXY_SUPPORT
static const char *proxy_host = NULL;
static int proxy_port = 0;
# endif

# define MIN_PORT (23)
  /* smallest port number this will try to connect to except for next two */
# define DEFAULT_TCP_PORT (2203)
  /* 2203 is default port # to connect to server on when given -n (not PrimeNet) */
# define MAX_PORT ((short)(1 << 15))
  /* highest port this will try to connect to except for prior two */

# ifdef PRIMENET_SUPPORT
#  define HTTP_COMMAND "POST"
  /* Only other choice is "GET" */
const char default_primenet_host[] = "entropia.com";
  /* default host when given -N (PrimeNet) */
#  define DEFAULT_PRIMENET_PORT (80)
  /* default port # when given -N; many other PrimeNet servers are at 8080 */
const char default_primenet_cgi[] = "/cgi-bin/primenet4.pl";
  /* default CGI script to ask for on the PrimeNet server */
# endif

#endif

static int print_times = 0; /* print wall clock and CPU times to stderr? -t flag */

void print_time PROTO((void))
 {
  time_t end_time, diff;
#if defined(RUSAGE_SELF)
  struct rusage ru;
#endif

  if (!print_times)
    return;
#if defined(RUSAGE_SELF)
  getrusage(RUSAGE_SELF, &ru);
  fprintf(stderr, "%ld.%02ld user %ld.%02ld sys ",
    (long)ru.ru_utime.tv_sec, (long)ru.ru_utime.tv_usec/10000,
    (long)ru.ru_stime.tv_sec, (long)ru.ru_stime.tv_usec/10000);
#endif
  time(&end_time);
  diff = end_time - start_time;
  start_time = end_time;
  if (diff > 3599)
   {
    fprintf(stderr, "%ld", diff/3600);
    diff %= 3600;
    fprintf(stderr, ":%02ld", diff/60);
   }
  else
    fprintf(stderr, "%ld", diff/60);
  diff %= 60;
  fprintf(stderr, ":%02ld real\n", diff);
  return;
 }

#ifndef FACTOR_PACKAGE
#define EXPONENT UL
#endif

/*!!this is a function to make it easier to modify for differing operating systems */
const char *archive_name PROTO((void))
 {
  return("mersarch.txt");
 }

FILE *open_archive PROTO((void))
 {
  return(fopen(archive_name(), "a"));
 }

#ifdef __STDC__
static void print_warning(const char *str)
#else
static void print_warning(str)
const char *str;
#endif
 {
  static int prior_errno = 0;

  if (prior_errno == errno)
    return;
  prior_errno = errno;
  if (fprintf(stderr, "%s: %s\n", program_name, str) < 0)
    errno = prior_errno; /* so perror() prints the original complaint, not the new one, if possible */
  perror(program_name);
 }

#if defined(TCPIP)

/* For -n or -N, contact the server; failure is returned by *infp == NULL; */
/*  *outfp is left alone if possible.  Note that infp and outfp can be NULL */
/*  in the PrimeNet case, since this function will do the I/O as well */
/*!!return errno? */
# ifdef __STDC__
static void contact(const char *host, int port, int all_factors, FILE **infp, FILE **outfp, int primenet)
# else
static void contact(host, port, all_factors, infp, outfp, primenet)
const char *host;
int port, all_factors;
FILE **infp, **outfp;
int primenet;
# endif
 {
  struct sockaddr_in server;
  struct hostent *host_entry;
  int s, s2; /* sockets to server */
  int i = errno; /* index into list of addresses from gethostbyname() and to read from archfp */
  FILE *archfp;
  FILE *localinfp = NULL, *localoutfp = NULL;
  static int prior_host_error = 0; /* if h_errno matches this, presume we've already warned about it */

  /*!!if last connect was successful, don't try again unless "enough" time has passed */
  errno = 0;
  if ((archfp = fopen(archive_name(), "r")) == NULL || fgetc(archfp) == EOF)
   {
    if (errno == ENOENT || errno == 0)
      errno = i; /* restore it */
    else
      print_warning(archive_name());
    if (primenet)
      return; /* nothing to send to server, so no need to contact it */
   }
  errno = i;
  if (archfp != NULL) /* otherwise, we'd have to close it before a lot of error returns below; */
    (void)fclose(archfp); /* but if we opened it later, we couldn't skip trying to connect when its empty */
  if (primenet && infp == NULL)
    infp = &localinfp;
  if (outfp == NULL)
    outfp = &localoutfp;
  if (infp != NULL && *infp != NULL && *infp != stdin)
   {
    (void)fclose(*infp);
    *infp = NULL;
   }
  if ((host_entry = gethostbyname(host)) == NULL)
   {
# ifdef __hpux
    h_errno = 1;
# endif
    if (prior_host_error != h_errno)
     {
      (void)fprintf(stderr, "%s: ", program_name);
      herror(host);
      prior_host_error = h_errno;
     }
    return;
   }
  h_errno = 0;
  errno = 0;
  if (port < MIN_PORT || port > MAX_PORT)
# ifdef PRIMENET_SUPPORT
    port = primenet ? DEFAULT_PRIMENET_PORT : DEFAULT_TCP_PORT;
# else
    port = DEFAULT_TCP_PORT;
# endif
  server.sin_port = htons(port);
# ifndef __hpux
  for (i = 0; host_entry->h_addr_list[i] != NULL; i++)
   {
    server.sin_family = host_entry[i].h_addrtype;
    bcopy(host_entry->h_addr_list[i], (char *)&server.sin_addr.s_addr, host_entry->h_length);
    if ((s = socket(host_entry->h_addrtype, SOCK_STREAM, 0)) < 0)
     {
      print_warning("inet socket create");
      return;
     }
    if (connect(s, (struct sockaddr *)&server, sizeof(server)) >= 0)
      break;
   }
  if (host_entry->h_addr_list[i] == NULL)
   {
    print_warning("cannot connect to any of the server's IP addresses");
    return;
   }
# else
  server.sin_family = host_entry->h_addrtype;
  bcopy(host_entry->h_addr, (char *)&server.sin_addr.s_addr, host_entry->h_length);
  if ((s = socket(host_entry->h_addrtype, SOCK_STREAM, 0)) < 0)
   {
    print_warning("inet socket create");
    return;
   }
  if (connect(s, (struct sockaddr *)&server, sizeof(server)) < 0)
   {
    print_warning("cannot connect to the server");
    return;
   }
# endif
  if ((s2 = dup(s)) < 0)
   {
    print_warning("could not dup() server socket");
    return;
   }
  if (infp != NULL && (*infp = fdopen(s, "r")) == NULL)
   {
    print_warning("could not fdopen() server socket for input");
    return;
   }
  if (*outfp != NULL && *outfp != stdout && *outfp != stderr)
    (void)fclose(*outfp);
  if ((*outfp = fdopen(s2, "w")) == NULL)
   {
    print_warning("could not fdopen() server socket for output");
    if (infp != NULL)
     {
      (void)fclose(*infp);
      *infp = NULL;
     }
    return;
   }
  if (infp != NULL)
    setlinebuf(*infp);
  setlinebuf(*outfp);
  if ((archfp = fopen(archive_name(), "r")) == NULL)
   {
    if (primenet)
     {
      print_warning("archive file was OK; now not OK");
      return;
     }
    if (errno != ENOENT)
      print_warning(archive_name());
   }
# ifdef PRIMENET_SUPPORT
  if (primenet)
   {
    /*!!Add ProxyHost support, including http://host */
    fprintf(outfp, "%s", HTTP_COMMAND, default_primenet_cgi);
    /*!!send the Primenet server PING packet */
    /*!!send the data in archfp to PrimeNet */
    /*!!have to check for errors on *infp, *outfp, and archfp along the way */
    if (infp != NULL && *infp != NULL)
     {
      if (fclose(*infp) != 0)
        print_warning("closing input stream from PrimeNet");
      *infp = NULL;
     }
    if (fclose(*outfp) != 0)
     {
      print_warning("error writing to PrimeNet");
      (void)fclose(archfp);
      *outfp = NULL;
      return;
     }
    *outfp = NULL;
    if (fclose(archfp) == 0)
      (void)unlink(archive_name());
    else
      print_warning(archive_name());
    return;
   }
# endif /* PRIMENET_SUPPORT */
  /* non-PrimeNet server from here on */
  if (archfp != NULL)
   {
    while ((i = fgetc(archfp)) != EOF)
      if (putc(i, *outfp) == EOF)
        break;
    if (ferror(*outfp))
      print_warning("writing to server");
    if (ferror(archfp))
     {
      print_warning(archive_name());
      (void)fclose(archfp);
     }
    else
      if (fclose(archfp) == 0 && !ferror(*outfp))
        (void)unlink(archive_name());
   }
  /* tell server what kind of client we are with _no_ '\n' half way thru */
# ifdef FACTOR_PACKAGE
  if (all_factors != 0)
    fputs("Joining trial factorer, all factors", *outfp);
  else
    fputs("joining trial factorer", *outfp);
# else
  fputs("residue", *outfp);
# endif
  fprintf(*outfp, ", %s.c, %s sBL %ld sBD %ld RHM %ld range " PRINTF_FMT_UL " " PRINTF_FMT_UL "\n",
          program_name, program_revision, (long)sizeof(BIG_LONG), (long)sizeof(BIG_DOUBLE),
          ROUNDED_HALF_MANTISSA, start_range, stop_range);
  if (fflush(*outfp) != 0)
    print_warning("flushing output to server");
  if ((infp == NULL || !ferror(*infp)) && !ferror(*outfp))
   {
    if (*infp == localinfp && fclose(*infp) != 0)
      print_warning("closing input from server");
    if (*outfp == localoutfp && fclose(*outfp) != 0)
      print_warning("closing output to server");
    return;
   }
  if (!ferror(*outfp))
    print_warning(" reading from server"); /* probably cannot happen here (-n); haven't tried to read yet */
  (void)fclose(*outfp);
  *outfp = NULL;
  if (infp == NULL)
    return;
  (void)fclose(*infp);
  *infp = NULL;
  return;
 }

#endif /* ifdef TCPIP */

/* Get some input and/or parse the command line (since exponents can be on the command line) */
/*  return values: */
/*  0: no more input; 1: trouble; 2: resuming from a partial (RI) result; 3: other data for exponent */
/*!!read from merstodo.ini or similar in primenet case rather than directly from server? */
#ifdef __STDC__
int input(int argc, char **argv, EXPONENT *q, UL *n, UL *j, BIG_DOUBLE *err, BIG_DOUBLE **x, EXPONENT last,
          char *M, FACTOR *start, FACTOR *stop, int *all_factors, int *small_test, EXPONENT *limit,
          UL mask, UL shift, FILE **infp, FILE **outfp, FILE **dupfp, int *primenet)
#else
int input(argc, argv, q, n, j, err, x, last, M, start, stop, all_factors, small_test, limit, mask, shift,
          infp, outfp, dupfp, primenet)
int argc;
char **argv;
EXPONENT *q;
UL *n, *j, mask, shift;
BIG_DOUBLE *err, **x;
EXPONENT last;
char *M;
FACTOR *start, *stop;
int *all_factors;
int *small_test;
EXPONENT *limit;
FILE **infp;
FILE **outfp;
FILE **dupfp;
int *primenet;
#endif
 {
  static int arg = 0; /* subscript of argv; will be incremented immediately */
  int retval; /* return value from fread(): Linux man page and libc.a don't agree on correct value */
  int sBL, sBD; /* sizeof(BIG_LONG), sizeof(BIG_DOUBLE), and */
  long RHM;     /* ROUNDED_HALF_MANTISSA as read from input */
  UL tmp;
  FILE *tmpfp = NULL;
  int partial; /* are we resuming a partially computed LL test? */
  char *tmpstr = NULL;
  int i = EOF;
#ifdef FACTOR_PACKAGE
  static FACTOR trials; /* 0 means let mersfac.c choose where to stop */
#endif
  /* these last three are needed by LL tests only to skip parts of the input appropriately */
  char str[1]; /* for reading the ending 's' of 'trials' from input (usually the server) */
  static UL stop_power_of_2 = (UL)0;
  static UL start_power_of_2 = (UL)0;
  static EXPONENT other_fmt = 0; /* is the input in some other format? if no, this is zero; if yes, this */
    /* is the exponent of the Cunningham Project line we're in the middle of reading unless it is one, */
    /*  in which case we are reading a DATABASE file from George Woltman (binary, Intel order) */
  static EXPONENT DBaddition = (UL)0; /* 0xFF or 0x7F addition to exponents from a DATABASE file */
  static char first0x7F = 1; /* seen 0x7F in this DATABASE file yet? */

  if (q == NULL || M == NULL || infp == NULL || outfp == NULL || dupfp == NULL)
    return(1);
  if (first_time_here)
   {
    time(&start_time);
#ifdef FACTOR_PACKAGE
    fac_init_var(trials);
    fac_set_int((UL)0, &trials);
#endif
    if (((tmpstr = getenv("FFTLUCAS_CHECKPOINT")) != NULL) &&
        (sscanf(tmpstr, SCANF_FMT_UL, &chkpnt_iterations) != 1 || chkpnt_iterations < (UL)5000))
     {
      if (!isdigit(tmpstr[0]))
        fprintf(stderr, "%s: non-numeric FFTLUCAS_CHECKPOINT; using 5000\n", program_name);
      chkpnt_iterations = (UL)5000;
     }
    if (mask != (UL)0)
     {
      start_range = 4*(~(UL)mask);
      *n = 1 + ~(UL)mask;
     }
   }  

get_next_q: /* all but one goto to here are just before a return if (*q < start_range || *q > stop_range); */
            /*  the single exception is reading a DATABASE file that has exponents that do not fit in a UL */

  tmp = (UL)0;
  if ((*infp == NULL || feof(*infp)) && argv != NULL && argv[arg] != NULL)
    arg++;
  while (argv != NULL && argv[arg] != NULL && argv[arg][0] == '\0')
    arg++;
  if ((argv == NULL || argv[arg] == NULL) && (*infp == NULL || feof(*infp)))
    if (first_time_here)
      *infp = stdin,
        *outfp = stdout;
    else
#ifdef TCPIP
      if (server_host == NULL)
#endif
        return(0); /* nothing else to read; must be done; only return(0) when no -n is given */
  while (argv != NULL && argv[arg] != NULL &&
         ((*infp == NULL && tmp == (UL)0) || (*infp != NULL && feof(*infp)) || first_time_here))
   {
    if (*infp != NULL && *infp != stdin && feof(*infp))
      (void)fclose(*infp),
        *infp = NULL,
        other_fmt = 0;
    if (*outfp != NULL && *outfp != stdout && *outfp != stderr && ferror(*outfp))
      (void)fclose(*outfp),
        *outfp = NULL;
    switch (argv[arg][0])
     {
      case '\0': /* empty argument: pretend it's a lone dash */
        *infp = stdin;
        other_fmt = 0;
        first_time_here = 0;
        if (arg > 1)
          fprintf(stderr, "Warning: reading from stdin due to NULL argument or no file arguments\n");
        break;
      case '-': /* leading dash: some sort of option */
        /* set tmpstr to point at the option's likely argument to simplify things later */
        if (argv[arg][1] != '\0') /* could skip this except: */
          if (argv[arg][2] != '\0') /* can't check this unless prior condition is true */
            tmpstr = argv[arg] + 2;
          else
            if (arg + 1 < argc)
              tmpstr = argv[arg + 1];
            else
              tmpstr = NULL;
        switch (argv[arg][1])
         {
          case '\0': /* just a dash: read stdin */
            *infp = stdin;
            other_fmt = 0;
            first_time_here = 0;
            /*!!kludge to undo increment just after this switch so increment at EOF will be correct */
            /*!! and to match argv[arg] being the name of the open file in the named file case */
            arg--;
#ifdef TCPIP
            server_host = NULL;
#endif
            break;
          case 'a': /* find all factors, not just one */
            if (all_factors != NULL)
              *all_factors = 1;
            break;
          case 'b': /* binary checkpoint format */
            chkpnt_fmt = CHKPNT_BINARY;
            break;
          case 'c': /* check point iterations */
            if (tmpstr == NULL || sscanf(tmpstr, SCANF_FMT_UL, &chkpnt_iterations) != 1)
              chkpnt_iterations = (UL)5000;
            else
              if (tmpstr != NULL && tmpstr == argv[arg + 1])
                arg++;
            break;
       /* case 'd': is with case 'o': below because they share code */
          case 'e': /* max. exponent of 2 to search to: 'stop = (1 << stop_power_of_2)' */
            /*!!check extent of factoring against this and don't LL test if it's not factored enough? */
            if (tmpstr == NULL || sscanf(tmpstr, SCANF_FMT_UL, &stop_power_of_2) != 1)
              stop_power_of_2 = (UL)0;
            else
              if (tmpstr != NULL && tmpstr == argv[arg + 1])
                arg++;
#if fac_max_exp_of_2 > 0
            if (fac_max_exp_of_2 < stop_power_of_2)
              stop_power_of_2 = fac_max_exp_of_2;
#endif
            break;
          case 'h': /* human-readable checkpoint format */
            if (strncmp(program_name, "fftlucas", 8))
             {
              fprintf(stderr, "%s: only fftlucas supports human readable checkpoints\n%s",
                      program_name, Options);
              exit(1);
             }
            chkpnt_fmt = CHKPNT_ASCII;
            break;
          case 'l': /* limit on P-1 or P+1 (stage 1) bound for merspm1 or merspp1 */
            if (limit == NULL)
             {
              if (tmpstr != NULL && tmpstr == argv[arg + 1])
                arg++;
              break;
             }
            if (tmpstr == NULL || sscanf(tmpstr, SCANF_FMT_UL, limit) != 1)
              *limit = (UL)5000;
            else
              if (tmpstr == argv[arg + 1])
                arg++;
            if (*limit < (UL)5000)
              *limit = (UL)5000;
            break;
          case 'm': /* max. trials, defined from: 'stop = start + 2*trials*(*q)' */
#ifdef FACTOR_PACKAGE
            if (tmpstr == NULL || sscan_fac(tmpstr, &trials) != 1)
              fac_set_int((UL)0, &trials);
            else
#endif
             {
              if (tmpstr != NULL && tmpstr == argv[arg + 1])
                arg++;
              break;
             }
            break;
          case 'N': /* network: connect to PrimeNet server via HTTP */
            /* fall thru */
          case 'n': /* network: connect to server on named host using TCP/IP; always human-readable */
#if defined(vms) && !defined(MULTINET)
            (void)fprintf(stderr, "%s: VMS without Multinet does not %s\n%s", program_name,
                          "support the 'n' flag", Options);
            exit(1);
#else /* vms && !MULTINET */
# if !defined(TCPIP)
            (void)fprintf(stderr, "%s: -n: (TCP/IP) support not compiled in\n%s", program_name, Options);
            exit(1);
# else
            if (primenet != NULL)
#  ifdef PRIMENET_SUPPORT
              *primenet = (argv[arg][1] == 'N');
#  else
              *primenet = 0;
#  endif
            if (tmpstr == NULL)
             {
              tmpstr = getenv("MERS_SERVER_HOST");
#  ifdef PROXY_SUPPORT
              proxy_host = getenv("MERS_PROXY_HOST");
#  endif
             }
            if (tmpstr == NULL || tmpstr[0] == '\0')
#  ifdef PRIMENET_SUPPORT
              if (primenet != NULL && *primenet)
                server_host = default_primenet_host;
              else
#  endif
                server_host = "localhost";
            else
             {
              server_host = tmpstr;
              if (tmpstr == argv[arg + 1])
                arg++;
             }
            if (server_port == 0 && getenv("MERS_SERVER_PORT") != NULL)
              server_port = atoi(getenv("MERS_SERVER_PORT"));
#  ifdef PROXY_SUPPORT
            if (proxy_host != NULL)
              if (presence(proxy_host, ':') != NULL)
                proxy_port = atoi(1 + presence(proxy_host, ':'));
              else
                if (getenv("MERS_PROXY_PORT") != NULL)
                  proxy_port = atoi(getenv("MERS_PROXY_PORT"));
#  endif
            contact(server_host, server_port, (all_factors == NULL) ? 0 : (*all_factors != 0), infp, outfp,
                    primenet == NULL ? 0 : *primenet);
            if (*infp == NULL && first_time_here == 1 && (primenet == NULL || !*primenet))
              return(1);
            other_fmt = 0;
            first_time_here = 0;
            break;
# endif /* if !TCPIP */
#endif /* vms && !MULTINET */
          case 'o': case 'd': /* 'o'utput and 'd'uplicate output files */
            i = argv[arg][1];
            if (tmpstr == NULL)
             {
              fprintf(stderr, "%s: no file name after -%c\n%s", program_name, i, Options);
              exit(1);
             }
            if (!strcmp(tmpstr, "stdout") || /* rest of condition is "-o-", "-o -", "-o -anythingelse" */
                (i == 'o' && (!strcmp(tmpstr, "-") || (argv[arg][2] == '\0' && argv[arg + 1][0] == '-'))))
              tmpfp = stdout; /* that 'rest' of the condition avoids "-ofile -n", "-o-file", etc */
            else
              if (!strcmp(tmpstr, "stderr") ||
                  (i == 'd' && (!strcmp(tmpstr, "-") || (argv[arg][2] == '\0' && argv[arg + 1][0] == '-'))))
                tmpfp = stderr;
              else
                if ((tmpfp = fopen(tmpstr, "a")) == NULL)
                 {
                  fprintf(stderr, "%s: could not open '%s' for append\n", program_name, tmpstr);
                  perror(tmpstr);
                  exit(errno);
                 }
            if (tmpstr == argv[arg + 1] && (tmpstr[0] != '-' || tmpstr[1] == '\0'))
              arg++;
            if (i == 'o')
             { /* normal output */
              if (*outfp != NULL && *outfp != stdout && *outfp != stderr)
                (void)fclose(*outfp);
#ifdef TCPIP
              server_host = NULL;
#endif
              *outfp = tmpfp;
              if (*dupfp == *outfp)
                *dupfp = NULL; /* no point in duplicating the output to the same descriptor */
             }
            else
             { /* duplicate output, usually to get local copy when the normal output is via TCP/IP */
              if (*dupfp != NULL && *dupfp != stdout && *dupfp != stderr)
                (void)fclose(*dupfp);
              if (tmpfp == *outfp)
                tmpfp = NULL;
              *dupfp = tmpfp;
             }
            tmpfp = NULL;
            break;
          case 'p': /* TCP/IP port number */
#if defined(vms) && !defined(MULTINET)
            (void)fprintf(stderr, "%s: VMS without Multinet does not %s\n%s", program_name,
                          "support the 'p' flag", Options);
            exit(1);
#else /* vms && !MULTINET */
# if !defined(TCPIP)
            (void)fprintf(stderr, "%s: -p: (TCP/IP) support not compiled in\n%s", program_name, Options);
            exit(1);
# else
            if (tmpstr != NULL && isascii(tmpstr[0]) && isdigit(tmpstr[0]))
              if (sscanf(tmpstr, "%u", &server_port) == 1)
               {
                if (tmpstr == argv[arg + 1])
                  arg++;
                break;
               }
              else
                server_port = 0;
            else
              server_port = 0;
            break;
# endif /* if !TCPIP */
#endif /* vms && !MULTINET */
          case 'r':
            if (tmpstr == NULL || !isascii(tmpstr[0]) || !isdigit(tmpstr[0]) ||
                (tmpstr == argv[arg + 1] &&
                 (argv[arg + 2] == NULL || !isascii(argv[arg + 2][0]) || !isdigit(argv[arg + 2][0]))) ||
                (tmpstr != argv[arg + 1] &&
                 (argv[arg + 1] == NULL || !isascii(argv[arg + 1][0]) || !isdigit(argv[arg + 1][0]))))
             {
              (void)fprintf(stderr, "%s: -r requires two numeric arguments\n%s", program_name, Options);
              exit(1);
             }
            sscanf(tmpstr, SCANF_FMT_UL, &start_range);
            if (tmpstr == argv[++arg])
              sscanf(argv[++arg], SCANF_FMT_UL, &stop_range);
            else
              sscanf(argv[arg], SCANF_FMT_UL, &stop_range);
            if (start_range < (UL)31 || stop_range > (~(UL)0)/2 || start_range > stop_range ||
                (mask != (UL)0 && start_range < 2*(~(UL)mask)))
             {
              fprintf(stderr, "%s: -r start too small, stop too large, or start > stop\n", program_name);
              exit(1);
             }
            break;
          case 's':
            if (small_test != NULL)
              *small_test = 1;
            break;
          case 't':
            print_times = 1;
            break;
          case 'v':
            (void)fprintf(stderr, "%s version information:\n %s\n", program_name, RCSprogram_id);
            break;
          default:
            (void)fprintf(stderr, "%s: unknown option: '%s'\n%s", program_name, argv[arg], Options);
            exit(1);
         }
        arg++;
        break;
      default: /* anything except a leading '-' */
        if ((tmpfp = fopen(argv[arg], "r")) != NULL)
         {
          *infp = tmpfp;
          if ((strlen(argv[arg]) == 8 /* long enough to be "DATABASE" or "database" */
               && (!strcmp(argv[arg], "DATABASE") || !strcmp(argv[arg], "database"))) ||
              (strlen(argv[arg]) > 8 /* must include a directory to be "DATABASE" or "database" */
               && (!strcmp(argv[arg] + strlen(argv[arg]) - 9, "/DATABASE") ||
                   !strcmp(argv[arg] + strlen(argv[arg]) - 9, "/database"))))
            other_fmt = 1; /* Woltman's DATABASE file */
          else
            other_fmt = 0;
          first_time_here = 0;
#ifdef TCPIP
          server_host = NULL;
#endif
          tmpfp = NULL;
         }
        else
          if (sscanf(argv[arg], SCANF_FMT_UL, &tmp) == 1)
           {
            *q = tmp;
            *infp = NULL;
            other_fmt = 0;
            first_time_here = 0;
           }
          else
           {
            (void)fprintf(stderr, "%s: tried to read '%s' as a check point file or list of exponents\n%s",
                          program_name, argv[arg], Options);
            perror(program_name);
            exit(errno == 0 ? 1 : errno);
           }
        break;
     }
   }
  if (*infp == NULL && first_time_here)
    *infp = stdin;
#ifdef TCPIP
  else
    if ((*infp == NULL || feof(*infp) || ferror(*infp)) && server_host != NULL)
     {
      other_fmt = 0;
      contact(server_host, server_port, (all_factors == NULL) ? 0 : (*all_factors != 0), infp, outfp,
              primenet == NULL ? 0 : *primenet);
      if (*infp == NULL)
       {
# ifdef FACTOR_PACKAGE
        fac_set(*stop, start);
        fac_set_int(*q, stop);
        fac_times2(*stop, stop);
        if (fac_less_than_int(trials, (UL)96000))
          fac_mul_int(*stop, (UL)96000, stop);
        else
          fac_mul(*stop, trials, stop);
        fac_add(*start, *stop, stop);
        if (fac_mod8(*stop) % 2 == 0)
          fac_add_int(*stop, (UL)1, stop);
        if (fac_mod8(*stop) % 2 == 1 && fac_less_than(*start, *stop))
          if (*q < start_range || *q > stop_range)
            goto get_next_q; /* probably can't happen since we've already done work on this exponent */
          else
            return(3);
        /* gone as far as we can: *stop is not odd (mantissa too small) or *stop <= *start (int overflow) */
# endif
        return(0); /* only return(0) if -n or -N was given */
       }
     }
#endif
  if (*outfp == NULL)
    *outfp = stdout;
  first_time_here = 0;
  if (primenet != NULL && *primenet)
   { /*!!get a number or two to work on; "merstodo.txt" file? */
    /*!!Note: might have to "fall thru" to look for a checkpoint file */
    return(1); /*!!change to 3 once implemented */
   }

nextDB: /* only goto is just below after reading a special 0xFF or 0x7F tag in a DATABASE file */

  if (other_fmt == 1 && (i = fgetc(*infp)) == EOF)
    other_fmt = 0, DBaddition = (UL)0, first0x7F = 1; /* hit EOF from DATABASE */
  if (other_fmt == 1)
   { /* exponent in three bytes, low order byte first */
    *q = (UL)i + ((UL)fgetc(*infp))*(1 << 8);
    *q += ((UL)fgetc(*infp))*(1 << 16);
    /* the power-of-two extent of factoring with the high bit being the count of prior LL tests */
    tmp = (UL)fgetc(*infp);
    if (tmp == 0xFF)
      DBaddition = (*q) << 8;
    if (tmp == 0x7F)
      if (first0x7F)
        DBaddition = (*q) << 24, first0x7F = 0;
      else
        DBaddition += (*q) << 24;
    if ((tmp & 0x7F) == 0x7F)
      goto nextDB;
    if (tmp > 128)
      *M = 'H',
        tmp -= 128;
    else
      *M = 'U';
    if (DBaddition > 0)
      *q += DBaddition;
    if (*q < DBaddition || *q < start_range || *q > stop_range)
      goto get_next_q;
#ifdef FACTOR_PACKAGE
    for (fac_set_int((UL)1, start); tmp > 0; tmp--)
      fac_times2(*start, start);
    if (fac_less_than_int(trials, (UL)1) || stop_power_of_2 == (UL)1)
      if (stop_power_of_2 < (UL)1)
       {
        fac_times2(*start, stop);
        fac_add_int(*stop, *q, stop);
        fac_add_int(*stop, *q, stop);
        fac_add_int(*stop, (UL)1, stop);
       }
      else
        for (tmp = 24L, fac_set_int((UL)1 << 24, stop);
             (tmp < stop_power_of_2 ||
              (stop_power_of_2 == (UL)1 && fac_less_than(*stop, *start)))
# if fac_max_exp_of_2 > 0
               && tmp < fac_max_exp_of_2
# endif
             ; tmp++)
          fac_times2(*stop, stop);
    else
     {
      fac_mul_int(trials, *q, stop);
      fac_times2(*stop, stop);
      fac_add(*start, *stop, stop);
     }
    if (!fac_less_than(*start, *stop))
      fac_add(*start, *stop, stop);
    if (fac_mod8(*stop) % 2 == 0)
      fac_add_int(*stop, (UL)1, stop);
#endif
    return(3); /*!!putting an exponent < 1024 in DATABASE will likely crash MacLucasFFT */
   }
  if (other_fmt != 0)
    switch (i = fgetc(*infp))
     {
      case '.': /* separates factors */
      case '\t': /* precedes 'Cdigits' or 'Pdigits' */
        while ((i = fgetc(*infp)) != EOF && i != '\n' && isspace(i))
          ;
        switch (i)
         {
          case 'C': /* rest of factorization unknown */
            *M = 'H';
#ifdef FACTOR_PACKAGE
            if (*q == other_fmt && stop != NULL && !fac_less_than_int(*stop, (UL)1))
             {
              fac_set(*stop, start);
              fac_set_int(*q, stop);
              fac_mul_int(*stop, (UL)4, stop);
              fac_add(*stop, *start, stop);
             }
#endif
            /* fall thru */
          case 'P': /* complete factorization known */
          case 'p': /* complete factorization thought to be known */
            if (i == 'P')
              *M = 'D';
            else
              if (i == 'p')
                *M = 'd';
            *q = other_fmt;
            other_fmt = 0;
            while (i != EOF && i != '\n')
              i = fgetc(*infp);
            if (*q < start_range || *q > stop_range)
              goto get_next_q;
            return(3);
          case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9':
            *M = 'C';
            *q = other_fmt;
#ifdef FACTOR_PACKAGE
            (void)ungetc(i, *infp);
            switch (fscan_fac(*infp, start))
             {
              case 1:
                if (*q < start_range || *q > stop_range)
                  goto get_next_q;
                else
                  return(3);
              case 0: /*!!should never happen */
              case EOF: /*!!should never happen */
                (void)fprintf(stderr, "%s: exponent " PRINTF_FMT_UL ": unknown problem\n", program_name, *q);
                return(1);
              default: /* overflow */
# ifdef TCPIP
                if (server_host != NULL)
                  fprintf(*outfp, "value too large\n");
# endif
                while ((i = fgetc(*infp)) != EOF && i != '\n')
                  ;
                goto get_next_q;
             }
#else
            while (isascii(i) && isdigit(i)) /*!!does not handle escaped newlines, but shouldn't be any */
              i = fgetc(*infp);
            if (i != EOF)
              if (*q < start_range || *q > stop_range)
                goto get_next_q;
              else
                return(3);
#endif
            /* fall thru */
          case EOF: /* line and file ended early */
          case '\n': /* line ended early */
          default:
            break;
         }
        /* fall thru */ /* should only be the default case just above and those that fall thru to it */
      default:
        fprintf(stderr, "%s: unknown format in middle of Cunningham line for exponent " PRINTF_FMT_UL "\n",
                program_name, *q);
        other_fmt = 0;
        return(1);
     }
  if (*infp != NULL && fscanf(*infp, " trial%c", str) == 1 && str[0] == 's')
   {
#ifdef FACTOR_PACKAGE
    if (fscan_fac(*infp, &trials) != 1)
      fac_set_int((UL)0, &trials);
    while ((i = fgetc(*infp)) != EOF && i != '\n')
      ;
#else
    /* gobble the digits, including any leading whitespace and escaped linefeeds and returns */
    while (((i = fgetc(*infp)) != EOF && isascii(i) && isspace(i)) ||
           (i == '\\' && ((i = fgetc(*infp)) == '\n' || i == '\r')))
      ;
    while ((i != EOF && isascii(i) && isdigit(i)) ||
           (i == '\\' && ((i = fgetc(*infp)) == '\n' || i == '\r')))
      i = fgetc(*infp);
#endif
   }   
  if (*infp == NULL || fscanf(*infp, SCANF_FMT_UL, q) == 1)
   { /* if there's no input file, *q is from the command line */
    if (*q < (UL)31 || (mask != (UL)0 && *q < 2*(~(UL)mask)))
     {
      (void)fprintf(stderr, "%s: exponent " PRINTF_FMT_UL " is too small to test\n", program_name, *q);
      return(1);
     }
    start_power_of_2 = (UL)0;
    if (*infp != NULL)
      switch (i = fgetc(*infp))
       {
        case ',': /* Woltman's "exponent,powerOf2" format */
          (void)fscanf(*infp, SCANF_FMT_UL, &start_power_of_2);
          break;
        case '\t': /* Cunningham Project's "exponent\tfactor1.factor2.\tCdigitsOfCofactor" format */
          while ((i = fgetc(*infp)) != EOF && i != '\n' && isspace(i))
            ;
          switch (i)
           {
            case '\n': /* unknown format; presume list of exponents */
            case 'C': /* Cunningham format but no known factors */
            case 'P': /* Cunningham format for Mersenne prime */
              if (i == 'P')
                *M = 'P';
              else
                *M = 'U';
              while (i != EOF && i != '\n') /* throw away the digit count */
                i = fgetc(*infp);
#ifdef FACTOR_PACKAGE
              if (start != NULL)
                fac_set_int((UL)0, start);
#endif
              break;
            case EOF: /* unknown format; handle this next time by going to the next command line argument */
              *M = 'U';
              break;
            case '0': case '1': case '2': case '3': case '4':
            case '5': case '6': case '7': case '8': case '9':
              *M = 'C';
              other_fmt = *q;
#ifdef FACTOR_PACKAGE
              (void)ungetc(i, *infp);
              switch (fscan_fac(*infp, start))
               {
                case 1:
                  break;
                case 0: /*!!should never happen */
                case EOF: /*!!should never happen */
                  fprintf(stderr, "%s: unexpected data after exponent " PRINTF_FMT_UL " and tab\n",
                          program_name, *q);
                  other_fmt = 0;
                  return(1);
                default: /* overflow */
# ifdef TCPIP
                  if (server_host != NULL)
                    fprintf(*outfp, "value too large\n");
# endif
                  while ((i = fgetc(*infp)) != EOF && i != '\n')
                    ;
                  goto get_next_q;
               }
#else
              /*!!this doesn't handle backslash-escaped returns, but Cunningham format doesn't have them */
              while (isascii(i) && isdigit(i))
                i = fgetc(*infp);
#endif
              break;
            default:
              (void)ungetc(i, *infp);
              return(1);
           }
          break;
        default: /* unknown format; presume it's just a list of exponents */
          (void)ungetc(i, *infp);
          /* fall thru */
        case '\n': /* list of exponents */
        case ' ':
          *M = 'U';
          break;
       }
    if (n != NULL || start == NULL || stop == NULL || all_factors == NULL)
     { /* must be LL testing, not factoring */
      if (*q < start_range || *q > stop_range)
        goto get_next_q;
      *M = 'U';
      if (last >= *q) /* reset n in case it's too big for this new exponent */
        if (mask != (UL)0)
          *n = 1 + ~(UL)mask; /* smallest length this algorithm can handle */
        else
          *n = 8L;
      sprintf(chkpnt_cfn, "c" PRINTF_FMT_UL, *q);
      sprintf(chkpnt_tfn, "t" PRINTF_FMT_UL, *q);
      /*!!see if the checkpoint files already exist and continue from them? */
      /*!!can't easily use *infp to do it since that would lose track of where we are in the current input */
      /*!!would help to have new read_chkpoint() and save_chkpoint() functions */
      /*!!new good_chkpoint() function that returns flag: is checkpoint data just read consistent? */
      return(3);
     }
#ifndef FACTOR_PACKAGE
    return(1); /* incorrect call since we must be doing LL tests */
#else
    if (*q < start_range || *q > stop_range)
      goto get_next_q;
    fac_set_int(2*(*q) + 1, start);
# if fac_max_exp_of_2 > 0
    if (start_power_of_2 >= fac_max_exp_of_2)
     {
      fprintf(stderr, "%s: " PRINTF_FMT_UL " tested too far for this program\n", program_name, *q);
      return(1);
     }
# endif
    if (start_power_of_2 > (UL)0)
     { /* use stop as a temporary briefly to compare 2^start_power_of_2 with start */
      /*!!freeLIP has a zlshift() function; others? */
      for (tmp = start_power_of_2, fac_set_int((UL)1, stop); tmp > 0; tmp--)
        fac_times2(*stop, stop);
      if (fac_less_than(*start, *stop))
        fac_set(*stop, start);
     }
    if (fac_less_than_int(trials, (UL)1) || stop_power_of_2 == (UL)1)
      if (stop_power_of_2 < 1L)
       {
        fac_set_int(2*(*q) + 1, stop);
        fac_mul_int(*stop, (UL)96000, stop); /* default of 96,000 trial factors attempted */
       }
      else /* never search to less than 2^24 */
        for (tmp = 24L, fac_set_int((UL)1 << 24, stop);
             (tmp < stop_power_of_2 ||
              (stop_power_of_2 == (UL)1 && fac_less_than(*stop, *start)))
# if fac_max_exp_of_2 > 0
               && tmp < fac_max_exp_of_2
# endif
             ; tmp++)
          fac_times2(*stop, stop);
    else
     {
      fac_mul_int(trials, *q, stop);
      fac_times2(*stop, stop);
      fac_add(*start, *stop, stop);
     }
    if (!fac_less_than(*start, *stop))
      fac_add(*start, *stop, stop);
    if (fac_mod8(*stop) % 2 == 0)
      fac_add_int(*stop, (UL)1, stop);
    return(3);
#endif
   }
  if (fscanf(*infp, "M(" SCANF_FMT_UL " )%c", q, M) == 2)
   {
    if (*q < (UL)31 || presence("UHIEeoqGCDdPJcLMlm", *M) == NULL || (mask != (UL)0 && *q < 2*(~(UL)mask)))
     {
      (void)fprintf(stderr, "%s: invalid data (exponent or letter 'code')\n", program_name);
      return(1); /* different format or error */
     }
    if (n != NULL || start == NULL || stop == NULL || all_factors == NULL)
     {
      while ((i = fgetc(*infp)) != EOF && i != '\n')
        ; /* rest of line is useless to LL testers */ /*!!unless to make sure it's factored far enough */
      if (*q < start_range || *q > stop_range)
        goto get_next_q;
      if (last >= *q) /* reset n in case it's too big for this new exponent */
        if (mask != (UL)0)
          *n = 1 + ~(UL)mask; /* smallest length this algorithm can handle */
        else
          *n = 8L;
      sprintf(chkpnt_cfn, "c" PRINTF_FMT_UL, *q);
      sprintf(chkpnt_tfn, "t" PRINTF_FMT_UL, *q);
      return(3);
     }
#ifndef FACTOR_PACKAGE
    return(1); /* incorrect call */
#else
    if (presence("DdPLM", *M) != NULL)
     { /* these lines can be ignored by most client programs and nothing else on the line is needed */
      while ((i = fgetc(*infp)) != EOF && i != '\n')
        ;
      if (*q < start_range || *q > stop_range)
        goto get_next_q;
      return(3);
     }
    switch ((fgetc(*infp) == ':') ? fscan_fac(*infp, start) : 0)
     {
      case 1:
        break;
      case 0:
      case EOF:
        fprintf(stderr, "%s: exponent " PRINTF_FMT_UL " data in unknown format\n", program_name, *q);
        return(1);
      default: /* overflow */
# ifdef TCPIP
        if (server_host != NULL)
          fprintf(*outfp, "value too large\n");
# endif
        while ((i = fgetc(*infp)) != EOF && i != '\n')
          ;
        goto get_next_q;
     }
    if (*M == 'C') /* factor; can't be power of two format and nothing else on the line is needed */
     {
      while ((i = fgetc(*infp)) != EOF && i != '\n')
        ;
      if (*q < start_range || *q > stop_range)
        goto get_next_q;
      return(3);
     }
    if (fac_less_than_int(*start, (UL)3) && !fac_less_than_int(*start, (UL)2))
     { /* check for power of 2 format */
      fac_set_int(2*(*q) + 1, start);
      switch (i = fgetc(*infp))
       {
        case '^':
          start_power_of_2 = (UL)0;
          (void)fscanf(*infp, SCANF_FMT_UL, &start_power_of_2);
# if fac_max_exp_of_2 > 0
          if (start_power_of_2 >= fac_max_exp_of_2)
           {
            fprintf(stderr, "%s: " PRINTF_FMT_UL " tested too far for this factoring program\n",
                    program_name, *q);
            return(1);
           }
# endif
          if (start_power_of_2 > (UL)0)
           { /* use stop as a temporary briefly */
            for (tmp = start_power_of_2, fac_set_int((UL)1, stop); tmp > 0; tmp--)
              fac_times2(*stop, stop);
            if (fac_less_than(*start, *stop))
              fac_set(*stop, start);
           }
          break;
        default:
          (void)ungetc(i, *infp);
          break;
       }
     }
    if (presence("UIHclm", *M) != NULL)
      fac_set_int(2*(*q), stop); /* never an input value for stop */
    else /* GJEeoq, any of which could have a second number in the rest of the line */
     {
      for (i = fgetc(*infp); isascii(i) && isspace(i) && i != '\n'; i = fgetc(*infp))
        ;
      if (i == EOF || i == '\n')
       { /* no curve count (e), largest ECM bound (E), second stage bound (oq), or stop (GJ) present */
        fac_set_int((UL)1, stop); /* whichever (eEoqGJ) of one is best we can do */
        if (presence("Eeoq", *M) != NULL)
          if (*q < start_range || *q > stop_range)
            goto get_next_q;
          else
            return(3);
       }
      else
       {
        (void)ungetc(i, *infp);
        switch (fscan_fac(*infp, stop))
         {
          case 1:
            break;
          case EOF: /*!!should be impossible */
          case 0: /*!!should be impossible */
            fprintf(stderr, "%s: exponent " PRINTF_FMT_UL " data in unknown format\n", program_name, *q);
            return(1);
          default: /* overflow */
# ifdef TCPIP
            if (server_host != NULL)
              fprintf(*outfp, "second value too large\n");
# endif
            while ((i = fgetc(*infp)) != EOF && i != '\n')
              ;
            goto get_next_q;
         }
       }
      if (fac_less_than_int(*stop, (UL)3) && !fac_less_than_int(*stop, (UL)2))
       { /* check for power of 2 format */
        fac_set_int(2*(*q) + 1, stop);
        switch (i = fgetc(*infp))
         {
          case '^':
            tmp = (UL)0;
            (void)fscanf(*infp, SCANF_FMT_UL, &tmp);
# if fac_max_exp_of_2 > 0
            if (tmp >= fac_max_exp_of_2)
              tmp = fac_max_exp_of_2;
# endif
            fac_set_int((UL)1 << 24, stop);
            if (tmp > (UL)24)
              for (tmp -= 24; tmp > 0; tmp--)
                fac_times2(*stop, stop);
            if (fac_less_than(*stop, *start))
             {
              fac_set_int(2*(*q), stop);
              fac_add(*start, *stop, stop);
             }
            break;
          default:
            (void)ungetc(i, *infp);
            break;
         }
       }
     }
    while ((i = fgetc(*infp)) != EOF && i != '\n')
      ; /* skip the rest of the line */
    if (*q < start_range || *q > stop_range)
      goto get_next_q;
    if (presence("Eeoqclm", *M) != NULL) /* these use start and stop for (non-trial) factoring data */
      return(3);
    /* trial factoring data (UIHGJ); stop should be higher than start and both are 1 (mod 2*(*q)) */
    if (fac_mod_int(*start, 2*(*q)) != 1)
      if (*M != 'J')
        if (fac_less_than_int(*start, 6*(*q) + 4))
          fac_set_int(2*(*q) + 1, start);
        else
          fac_sub_int(*start, 4*(*q) + 2, start);
      else /* make sure we don't include anything that wasn't tested */
        fac_add_int(*start, 4*(*q) + 2, start);
    if (!fac_less_than_int(*stop, 2*(*q) + 1) && fac_mod_int(*stop, 2*(*q)) != 1)
      if (*M == 'J')
        fac_add_int(*stop, 4*(*q) + 2, stop);
      else /* make sure we don't include anything that wasn't tested */
        if (fac_less_than_int(*stop, 6*(*q) + 4))
          fac_set(*start, stop);
        else
          fac_sub_int(*stop, 4*(*q) + 2, stop);
    if (*M == 'J')
      return(3);
    if (*M == 'I') /* interrupted; make sure it's done again */
      if (fac_less_than_int(*start, 6*(*q) + 4))
        fac_set_int(2*(*q) + 1, start);
      else
        fac_sub_int(*start, 4*(*q) + 2, start);
    if (*M == 'G') /* gap; make sure we test wider if possible for this factoring package */
     {
      if (fac_less_than_int(*start, 6*(*q) + 4))
        fac_set_int(2*(*q) + 1, start);
      else
        fac_sub_int(*start, 4*(*q) + 2, start);
      if (fac_mod8(*start) % 2 == 0)
        fac_sub_int(*start, (UL)1, start);
      fac_add_int(*stop, 4*(*q) + 2, stop); /* it's most likely odd, so preserve that */
      if (!fac_less_than(*start, *stop))
        fac_add_int(*start, 4*(*q) + 2, stop); /* mostly to cover integer overflow case */
      if (fac_mod8(*stop) % 2 == 0) /* make certain it's odd (if it fits in a (float) FACTOR's mantissa) */
        fac_add_int(*stop, 2*(*q) + 1, stop);
      return(3);
     }
    /* UIH: no stop, so construct one */
    if (fac_less_than_int(trials, (UL)1) || stop_power_of_2 == (UL)1)
      if (stop_power_of_2 < 1L)
       {
        fac_mul_int(*stop, (UL)96000, stop);
        fac_add(*start, *stop, stop);
       }
      else
       {
        for (tmp = 24L, fac_set_int((UL)1 << 24, stop);
             (tmp < stop_power_of_2 ||
              (stop_power_of_2 == (UL)1 && fac_less_than(*stop, *start)))
# if fac_max_exp_of_2 > 0
               && tmp < fac_max_exp_of_2
# endif
               ; tmp++)
          fac_times2(*stop, stop);
        fac_add_int(*stop, (UL)1, stop);
       }
    else
     {
      fac_mul(*stop, trials, stop);
      fac_add(*start, *stop, stop);
     }
    if (!fac_less_than(*start, *stop))
      fac_add(*start, *stop, stop);
# if fac_max_exp_of_2 > 0
    if (!fac_less_than(*start, *stop)) /* overflowed an integer type */
      fac_add_int(*start, 4*(*q) + 2, stop);
# endif
    if (fac_mod8(*start) % 2 == 0)
      fac_sub_int(*start, (UL)1, start);
    if (fac_mod8(*stop) % 2 == 0)
      fac_add_int(*stop, (UL)1, stop);
    return(3); /* U, I, or H */
#endif /* else of #ifndef FACTOR_PACKAGE */
   }
  if (feof(*infp)) /* this was already checked except in the case of whitespace */
    goto get_next_q; /*  - including a solitary return or LF - in front of EOF */
  /* everything else is for Lucas-Lehmer testers */
  if (x == NULL) /* no place to store pointer to FFT array from checkpoint */
    return(1); /* then the caller goofed; let them know */
  /* check for printable/human readable checkpoint header; partial will become 7 if so */
  partial = fscanf(*infp, SCANF_FMT_ONE, q, n, j, err, &sBL, &sBD, &RHM);
  if (partial < 0)
    if (ferror(*infp))
      return(1);
    else
      goto get_next_q; /* would be return(0) except that there might be more command line to parse */
  if (partial == 0 && !ferror(*infp))
   { /* check for binary checkpoint format */
    if (((retval = fread(&tmp, sizeof(tmp), 1, *infp)) == sizeof(tmp) || retval == 1) &&
        tmp == MAGIC_NUMBER &&
        ((retval = fread(q, sizeof(*q), 1, *infp)) == sizeof(*q) || retval == 1) &&
        ((retval = fread(n, sizeof(*n), 1, *infp)) == sizeof(*n) || retval == 1) &&
        *n < ((1L << 31) + 1L) &&
        ((retval = fread(j, sizeof(*j), 1, *infp)) == sizeof(*j) || retval == 1) &&
        ((retval = fread(err, sizeof(*err), 1, *infp)) == sizeof(*err) || retval == 1) &&
        ((*x) == NULL || (free((char *)(*x)), 1),
         (((*x) = (BIG_DOUBLE *)calloc((size_t)(*n + (((*n)&mask)>>shift)), sizeof(BIG_DOUBLE))) != NULL)) &&
        ((retval = fread(*x, sizeof(x[0][0]), (size_t)*n, *infp)) == (*n)*sizeof(x[0][0]) || retval == *n) &&
        (*n) > ((BIG_DOUBLE)(*q))/ROUNDED_HALF_MANTISSA + 3 && *q > (UL)30 && *q % 2 == 1 && *j < *q &&
        *j > 0 && (*err < 0.3 || (*q > (UL)10000 && *err < 0.49)))
     {
      for (tmp = *n - 1; ((tmp & mask) >> shift) != 0; tmp--)
        x[0][tmp + ((tmp & mask) >> shift)] = x[0][tmp];
      for (tmp = *n; (tmp & 1) == 0; tmp >>= 1)
        ;
      sprintf(chkpnt_cfn, "c" PRINTF_FMT_UL, *q);
      sprintf(chkpnt_tfn, "t" PRINTF_FMT_UL, *q);
      if (tmp == 1 || tmp == 3 || tmp == 5 || tmp == 7)
        return(2);
      (void)fprintf(stderr, "%s: inconsistent binary checkpoint; bad FFT run length of " PRINTF_FMT_UL "\n",
                    program_name, *n);
      return(1);
     }
    if (tmp == MAGIC_NUMBER || ferror(*infp) || feof(*infp))
     {
      (void)fprintf(stderr, "%s: inconsistent or incomplete binary checkpoint\n", program_name);
      return(1);
     }
   }
  if (partial != 7 || *n < ((BIG_DOUBLE)(*q))/ROUNDED_HALF_MANTISSA + 4 || *q < (UL)31 || *q % 2 == 0 ||
      *j >= *q || *j < 1 || *err > 0.49 || (*err > 0.3 && *q < (UL)10000) || (sizeof(BIG_LONG) != sBL &&
      sBL != 4 && sBL != 8) || sizeof(BIG_DOUBLE) != sBD || ROUNDED_HALF_MANTISSA != RHM ||
      (mask != (UL)0 && *q < 2*(~(UL)mask)))
   {
    (void)fprintf(stderr, "%s: inconsistent RI header (initial line)\n", program_name);
    return(1);
   }
  for (tmp = *n; (tmp & 1) == 0; tmp >>= 1)
    ;
  if (tmp != 1 && tmp != 3 && tmp != 5 && tmp != 7)
   {
    (void)fprintf(stderr, "%s: inconsistent RI input; FFT run length not a power of 2\n", program_name);
    return(1);
   }
  while (*n < ((BIG_DOUBLE)(*q))/ROUNDED_HALF_MANTISSA + 4.0)
    *n <<= 1;
  if (*x != NULL)
    free((char *)*x);
  if (((*x) = (BIG_DOUBLE *)calloc((size_t)(*n + (((*n) & mask) >> shift)), sizeof(BIG_DOUBLE))) == NULL)
   {
    perror(program_name);
    (void)fprintf(stderr, "%s: cannot get memory for FFT array\n", program_name);
    return(1);
   }
  for (tmp = 0; tmp < *n; tmp++)
    if (fscanf(*infp, SCANF_FMT_TWO, x[0] + tmp + ((tmp & mask) >> shift)) != 1)
     {
      perror(program_name);
      (void)fprintf(stderr, "%s: error reading RI input (middle lines)\n", program_name);
      return(1);
     }
  if (fscanf(*infp, " RI " PRINTF_FMT_UL " done", &tmp) != 1)
   {
    perror(program_name);
    (void)fprintf(stderr, "%s: error reading RI input (last line)\n", program_name);
    return(1);
   }
  if (*q != tmp)
   {
    (void)fprintf(stderr, "%s: inconsistent RI input (last line)\n", program_name);
    return(1);
   }
  sprintf(chkpnt_cfn, "c" PRINTF_FMT_UL, *q);
  sprintf(chkpnt_tfn, "t" PRINTF_FMT_UL, *q);
  return(2); /*!!could get_next_q here also, but skip an incomplete checkpoint?? */
 }

#ifndef FACTOR_PACKAGE

/* print what we've done so far to a (local) checkpoint file */
# ifdef __STDC__
int check_point(UL q, UL n, UL j, BIG_DOUBLE err, BIG_DOUBLE *x, UL mask, UL shift, int primenet)
# else
int check_point(q, n, j, err, x, mask, shift, primenet)
UL q, n, j, mask, shift;
BIG_DOUBLE err, *x;
# endif
 {
  FILE *chkpnt_fp;
  UL k = MAGIC_NUMBER; /* magic number ala the file(1) command; note that it's byte order dependent, */
                        /* which we want so that we don't try to use a save file from a different */
                        /* architecture or byte order */
  int retval; /* return value from fwrite(); Linux man page and libc.a do not agree on correct value */

  if (q < (UL)1000) /* refuse to write checkpoint files for small exponents */
    return(1);
  if ((chkpnt_fp = fopen(chkpnt_cfn, "r")) != NULL)
   {
    (void)fclose(chkpnt_fp);
    (void)unlink(chkpnt_tfn);
    (void)rename(chkpnt_cfn, chkpnt_tfn);
# ifdef PRIMENET_SUPPORT
    if (primenet)
     {
      /*!!predict completion date based on prior checkpoint date and chkpnt_iterations */
      /*!! careful, though: the prior checkpoint file could be from an earlier run, */
      /*!! so may have to have a 'been_here_before' flag for this block */
      /*!!write data to archive file and call contact() */
     }
# endif
   }
  if ((chkpnt_fp = fopen(chkpnt_cfn, "w")) == NULL)
   {
    perror(program_name);
    fprintf(stderr, "%s: could not open '%s' for writing\n", program_name, chkpnt_cfn);
    return(0);
   }
  switch (chkpnt_fmt)
   {
    case CHKPNT_BINARY:
      /* 'tag' the file as coming from this program, computer architecture, etc. */
      /* Note that k was initialized to MAGIC_NUMBER as fwrite() can't write a #define. */
      if ((retval = fwrite(&k, sizeof(k), 1, chkpnt_fp)) != sizeof(k) && retval != 1)
       {
        perror(program_name);
        fprintf(stderr, "%s: cannot write checkpoint info (k = " PRINTF_FMT_UL ")\n", program_name, k);
        fprintf(stderr, "%s: fwrite returned %d\n", program_name, retval);
        return(0);
       }
      if ((retval = fwrite(&q, sizeof(q), 1, chkpnt_fp)) != sizeof(q) && retval != 1)
       {
        perror(program_name);
        fprintf(stderr, "%s: cannot write checkpoint info (q = " PRINTF_FMT_UL ")\n", program_name, q);
        fprintf(stderr, "%s: fwrite returned %d\n", program_name, retval);
        return(0);
       }
      if ((retval = fwrite(&n, sizeof(n), 1, chkpnt_fp)) != sizeof(n) && retval != 1)
       {
        perror(program_name);
        fprintf(stderr, "%s: cannot write checkpoint info (n = " PRINTF_FMT_UL ", q = " PRINTF_FMT_UL ")\n",
                program_name, n, q);
        fprintf(stderr, "%s: fwrite returned %d\n", program_name, retval);
        return(0);
       }
      if ((retval = fwrite(&j, sizeof(j), 1, chkpnt_fp)) != sizeof(j) && retval != 1)
       {
        perror(program_name);
        fprintf(stderr, "%s: cannot write checkpoint info (j = " PRINTF_FMT_UL ", q = " PRINTF_FMT_UL ")\n",
                program_name, j, q);
        fprintf(stderr, "%s: fwrite returned %d\n", program_name, retval);
        return(0);
       }
      if ((retval = fwrite(&err, sizeof(err), 1, chkpnt_fp)) != sizeof(err) && retval != 1)
       {
        perror(program_name);
        fprintf(stderr, "%s: cannot write checkpoint info (err, q = " PRINTF_FMT_UL ")\n", program_name, q);
        fprintf(stderr, "%s: fwrite returned %d\n", program_name, retval);
        return(0);
       }
      for (k = 0; k < n; k++)
        if ((retval = fwrite(&(x[k + ((k & mask) >> shift)]), sizeof(x[0]), 1, chkpnt_fp)) != sizeof(x[0]) &&
            retval != 1)
         {
          perror(program_name);
          fprintf(stderr, "%s: cannot write checkpoint info (x, q = " PRINTF_FMT_UL ")\n", program_name, q);
          fprintf(stderr, "%s: fwrite returned %d\n", program_name, retval);
          return(0);
         }
      break;
    case CHKPNT_ASCII:
    default: /* some compilers think sizeof() returns an int and others think it returns a long */
      if (fprintf(chkpnt_fp, PRINTF_FMT_ONE, q, n, j, err, (long)sizeof(BIG_LONG), (long)sizeof(BIG_DOUBLE),
                  ROUNDED_HALF_MANTISSA) <= 0)
       {
        perror(program_name);
        fprintf(stderr, "%s: cannot write checkpoint info (RI header, q = " PRINTF_FMT_UL ")\n",
                program_name, q);
        return(0);
       }
      for (k = 0; k < n; k++)
        if (fprintf(chkpnt_fp, PRINTF_FMT_TWO, x[k + ((k & mask) >> shift)]) <= 0)
         {
          perror(program_name);
          fprintf(stderr, "%s: cannot write checkpoint info (RI mid, q = " PRINTF_FMT_UL ")\n",
                  program_name, q);
          return(0);
         }
      if (fprintf(chkpnt_fp, "RI " PRINTF_FMT_UL " done\n", q) <= 0)
       {
        perror(program_name);
        fprintf(stderr, "%s: cannot write checkpoint info (RI tail, q = " PRINTF_FMT_UL ")\n",
                program_name, q);
        return(0);
       }
      break;
   }
  fflush(chkpnt_fp);
#if !defined(macintosh) && !defined(pccompiler)
  fsync(fileno(chkpnt_fp));
#endif
  return(!fclose(chkpnt_fp));
 }

# ifdef __STDC__
static void note_archive_failure(FILE *fp, int error)
# else
static void note_archive_failure(fp, error)
FILE *fp;
int error;
# endif
 {
  static char cnt = 0;

  if (++cnt > 3)
    return; /* already warned in all possible output files, but warn again after it overflows */
  if (fp != NULL && !ferror(fp))
   {
    fprintf(fp, "%s: previous line may not have been appended to %s correctly\n",
            program_name, archive_name());
    fprintf(fp, "\terrno %d: %s\n", error, strerror(error));
   }
 }

# ifdef __STDC__
static void close_archive(FILE *archfp, FILE *outfp, FILE *dupfp, int primenet)
# else
static void close_archive(archfp, outfp, dupfp, primenet)
FILE *archfp, *outfp, *dupfp;
int primenet;
# endif
 {
  int last_errno;

  if (fclose(archfp) != 0)
   {
    note_archive_failure(outfp, last_errno = errno);
    note_archive_failure(dupfp, last_errno);
    if (outfp != stderr && dupfp != stderr)
      note_archive_failure(stderr, last_errno);
    return;
   }
# ifdef TCPIP
  if (primenet)
    contact(server_host, server_port, 0, NULL, NULL, 1);
# endif
  (void)unlink(chkpnt_cfn);
  (void)unlink(chkpnt_tfn);
 }

# ifdef __STDC__
void printbits(BIG_DOUBLE *x, UL q, UL n, UL totalbits, UL b, UL c, BIG_DOUBLE hi, BIG_DOUBLE lo,
               UL mask, UL shift, const char *version_info, FILE *outfp, FILE *dupfp, int primenet)
# else
void printbits(x, q, n, totalbits, b, c, hi, lo, mask, shift, version_info, outfp, dupfp, primenet)
BIG_DOUBLE *x, hi, lo;
UL q, n, totalbits, b, c, mask, shift;
const char *version_info;
FILE *outfp, *dupfp;
int primenet;
# endif
 {
  static UL *hex = NULL;
  static UL prior_hex = 0;
  static char bits_fmt[16] = "\0"; /* "%%0%ulx" -> "%08lx" or "%016lx" depending on sizeof(UL) */
  long j, k, i, word;
  FILE *archfp = open_archive();

  if (version_info == NULL || version_info[0] == '\0')
   {
    fprintf(stderr, "%s: printbits() called with no version info; bug\n", program_name);
    fprintf(stderr, "\t%s\n", RCSrw_c);
    if (outfp != NULL && outfp != stderr)
     {
      fprintf(outfp, "%s: printbits() called with no version info; bug\n", program_name);
      fprintf(outfp, "\t%s\n", RCSrw_c);
     }
    if (dupfp != NULL && dupfp != stderr)
     {
      fprintf(dupfp, "%s: printbits() called with no version info; bug\n", program_name);
      fprintf(dupfp, "\t%s\n", RCSrw_c);
     }
    version_info = RCSrw_revision;
   }
  if (is_zero(x, n, mask, shift))
   {
    if (outfp != NULL && !ferror(outfp))
      fprintf(outfp, "M( " PRINTF_FMT_UL " )P, n = " PRINTF_FMT_UL ", %s\n", q, n, version_info);
    if (dupfp != NULL && !ferror(dupfp))
      fprintf(dupfp, "M( " PRINTF_FMT_UL " )P, n = " PRINTF_FMT_UL ", %s\n", q, n, version_info);
    if (archfp != NULL)
     {
      fprintf(archfp, "M( " PRINTF_FMT_UL " )P, n = " PRINTF_FMT_UL ", %s\n", q, n, version_info);
      close_archive(archfp, outfp, dupfp, primenet);
     }
    return;
   }
  if (outfp != NULL && !ferror(outfp))
    fprintf(outfp, "M( " PRINTF_FMT_UL " )C", q);
  if (dupfp != NULL && !ferror(dupfp))
    fprintf(dupfp, "M( " PRINTF_FMT_UL " )C", q);
  if (archfp != NULL)
    fprintf(archfp, "M( " PRINTF_FMT_UL " )C", q);
  if (totalbits < 1)
   {
    if (outfp != NULL && !ferror(outfp))
      fprintf(outfp, ", %s\n", version_info);
    if (dupfp != NULL && !ferror(dupfp))
      fprintf(dupfp, ", %s\n", version_info);
    if (archfp != NULL)
     {
      fprintf(archfp, ", %s\n", version_info);
      close_archive(archfp, outfp, dupfp, primenet);
     }
    return;
   }
  balancedtostdrep(x, n, b, c, hi, lo, mask, shift);
  if (hex != NULL && totalbits/BITS_IN_LONG + 1 > prior_hex)
   {
    free(hex);
    hex = NULL;
    prior_hex = totalbits/BITS_IN_LONG + 1;
   }
  if (hex == NULL && (hex = (UL *)calloc(totalbits/BITS_IN_LONG + 1, sizeof(UL))) == NULL)
   {
    perror(program_name);
    fprintf(stderr, "%s: cannot get memory for residue bits; calloc() errno %d\n", program_name, errno);
    exit(errno);
   }
  j = 0;
  i = 0;
  do
   {
    k = (long)(ceil((BIG_DOUBLE)q*(j + 1)/n) - ceil((BIG_DOUBLE)q*j/n));
    if (k > totalbits)
      k = totalbits;
    totalbits -= k;
    word = (long)x[j + ((j & mask) >> shift)];
    for (j++; k > 0; k--, i++)
     {
      if (i % BITS_IN_LONG == 0)
        hex[i/BITS_IN_LONG] = 0L;
      hex[i/BITS_IN_LONG] |= ((word & 0x1) << (i % BITS_IN_LONG));
      word >>= 1;
     }
   } while(totalbits > 0);
  if (outfp != NULL && !ferror(outfp))
    fputs(", 0x", outfp);
  if (dupfp != NULL && !ferror(dupfp))
    fputs(", 0x", dupfp);
  if (archfp != NULL)
    fputs(", 0x", archfp);
  if (bits_fmt[0] != '%')
    sprintf(bits_fmt, "%%0%lu%s", (UL)(BITS_IN_LONG/4), "lx"); /* 4 bits per hex 'digit' */
  for (j = (i - 1)/BITS_IN_LONG; j >= 0; j--)
   {
    if (outfp != NULL && !ferror(outfp))
      fprintf(outfp, bits_fmt, hex[j]);
    if (dupfp != NULL && !ferror(dupfp))
      fprintf(dupfp, bits_fmt, hex[j]);
    if (archfp != NULL)
      fprintf(archfp, bits_fmt, hex[j]);
   }
  if (outfp != NULL && !ferror(outfp))
    fprintf(outfp, ", n = " PRINTF_FMT_UL ", %s\n", n, version_info);
  if (dupfp != NULL && !ferror(dupfp))
    fprintf(dupfp, ", n = " PRINTF_FMT_UL ", %s\n", n, version_info);
  if (archfp != NULL)
   {
    fprintf(archfp, ", n = " PRINTF_FMT_UL ", %s\n", n, version_info);
    close_archive(archfp, outfp, dupfp, primenet);
   }
  return;
 }
#endif /* !defined(FACTOR_PACKAGE) */
