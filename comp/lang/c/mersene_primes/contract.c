#ifndef lint
static char RCScontract_c[] = "$Id: contract.c,v 6.50 1999/12/19 06:06:20 wedgingt Exp $";
#endif

static const char program_name[] = "contract";

/*
	Creates a file DATABASE (Woltman's format)
	  from a file lowM.txt (Edgington's format).

	Rounds the trial factoring extent down to the nearest power of 2.

	08/05/97: Jean-Charles MEYRIGNAC
	16 Aug 1997 Will Edgington
*/

#ifdef macintosh
# include <console.h>
# define ENOENT -43
# ifndef isascii
#  define isascii(x) (((x) | 0177) == 0177)
# endif
#endif

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#if defined(sun) || defined(__alpha) || defined(linux) || defined(__sgi__) || defined(__NetBSD__) || defined(macintosh)
# include <errno.h>
#endif

#include <math.h>
/* math.h is only needed for log() to take base 2 logarithms; if you don't have */
/*  log(), you'll have to find the largest power of two that's less than a given */
/*  integer in some other way */

/* the natural logarithm of two */
#define LN2 (0.693147180559945309417232121458176568075500134360255254120680009493393621969694715605863326996418687542001481020570685733)
#define log2(x) (log(x)/LN2)

const char Usage[] = "contract [-i inputfile] [-o outputfile] [-h] [-u] [-v]";

const char *infile = NULL;
const char *outfile = NULL;
FILE *in, *out;

#if defined(linux) && defined(__GNUC__) && defined(__i386__) && !defined(DO_NOT_USE_LONG_LONG_EXPONENT)
typedef unsigned long long UL;
# define FMT_UL "%Lu"
#else
typedef unsigned long UL;
# define FMT_UL "%lu"
#endif

/* Maximums inherent in the format of DATABASE files */
#define MAX_SIMPLE_EXPONENT ((UL)0xFFFFFF)
#define MAX_0xFF_EXPONENT (0xFF + 0x100*((UL)0xFFFFFF))
#define MAX_POWER_OF_2 (123)

int stats[MAX_POWER_OF_2];

int main(int argc, char **argv)
 {
  UL exponent = 0, old = 0; /* exponent from this line of input and from prior line */
  char M; /* letter of lowM.txt format: 'U', 'I', 'H', 'C', 'D', 'd', 'P', etc. */
  unsigned char p; /* power of 2 extent of trial division */
  int n = 0; /* several uses */
  int cnt = 0; /* count of distinct exponents read */
  double f; /* extent of trial factoring */
  char verbose = 0; /* how much to print to user/stdout */
  char only_U_in_db = 0; /* only put unknown primality Mersennes into DATABASE */
  UL addition = 0; /* 0xFF or 0x7F style addition to the exponent */
  char bare_exponent = 0; /* just the exponent? or with a trial factoring extent or other data? */

#ifdef macintosh
  argc = ccommand(&argv);
#endif
  /*!!-n(o known factors), etc. */
  for (n = 1; n < argc; n++)
   {
    if (argv[n][0] != '-')
     {
      fprintf(stderr, "contract: unknown argument: '%s'\n", argv[n]);
      fprintf(stderr, "  Usage: %s\n", Usage);
      exit(1);
     }
    switch (argv[n][1])
     {
      case 'h':
      case 'H':
      case '?':
        fprintf(stderr, "%s\n", Usage);
        fprintf(stderr, "  %s\n", RCScontract_c);
        return(0);
      case 'i':
      case 'I':
        if (infile != NULL)
          fprintf(stderr, "contract: warning: previous input file '%s' ignored\n", infile);
        if (argv[n][2] != '\0')
          infile = argv[n] + 2;
        else
          infile = argv[++n];
        if (n >= argc || infile == NULL || infile[0] == '\0')
         {
          fprintf(stderr, "contract: no input file given after -i\n  Usage: %s\n", Usage);
          return(1);
         }
        break;
      case 'o':
      case 'O':
        if (outfile != NULL)
          fprintf(stderr, "contract: warning: previous output file '%s' ignored\n", outfile);
        if (argv[n][2] != '\0')
          outfile = argv[n] + 2;
        else
          outfile = argv[++n];
        if (n >= argc || outfile == NULL || outfile[0] == '\0')
         {
          fprintf(stderr, "contract: no output file given after -o\n  Usage: %s\n", Usage);
          return(1);
         }
        break;
      case 'u':
      case 'U':
        only_U_in_db = !only_U_in_db;
        break;
      case 'v':
      case 'V':
        verbose++;
        if (verbose > 4)
          verbose = 0;
        break;
     }
   }
  errno = 0;
  if (infile == NULL)
    in = stdin;
  else
    if ((in = fopen(infile, "r")) == NULL)
     {
      perror(infile);
      return(errno);
     }
  if (outfile == NULL)
    fprintf(stderr, "contract: writing to %s\n", outfile = "DATABASE");
  if ((out = fopen(outfile, "rb")) != NULL)
   {
    fprintf(stderr, "contract: output file %s already exists\n", outfile);
    exit(1);
   }
  if (errno == ENOENT)
    errno = 0;
  if ((out = fopen(outfile, "wb")) == NULL)
    perror(outfile);
  if (errno != 0)
    return(errno);
  while (n < MAX_POWER_OF_2)
    stats[n++] = 0;
  while (!feof(in) && !ferror(in) && !ferror(out) && (n = fgetc(in)) != EOF)
   {
    while (n != EOF && isascii(n) && isspace(n))
      n = fgetc(in); /* some fscanf()s require white space in input if present at start of format string */
    if ((bare_exponent = !!(isascii(n) && isdigit(n))) != 0)
      ungetc(n, in);
    if ((!bare_exponent && fscanf(in, "( " FMT_UL " )%c", &exponent, &M) < 2) ||
        (bare_exponent && (M = 'U', fscanf(in, FMT_UL, &exponent)) < 1))
     {
      fprintf(stderr, "unrecognized input near or after 'M( " FMT_UL " )' in %s\n", exponent, infile);
      break;
     }
    if (exponent == old) /* 'D', 'd', 'P', known factor ('C'), or already put into new DATABASE */
     {
      while ((n = fgetc(in)) != EOF && n != '\n')
        ; /* skip rest of line */
      continue;
     }
    switch (M)
     {
      case 'D': case 'd': case 'P': /* all factors known, cofactor is pseudo-prime, or prime */
      case 'l': case 'm': case 'L': case 'M': /* # of digits or pseudo-prime halves info */
        old = exponent;
        /* fall thru */
      case 'C':
        /* factor */
        if (only_U_in_db)
          old = exponent;
        /* fall thru */
      case 'c': case 'e': case 'E': case 'o': case 'q': /* # of digits, ECM, P-1, or P+1 data */
        while ((n = fgetc(in)) != EOF && n != '\n')
          ; /* skip rest of line */
        continue;
      case 'G': case 'H': case 'I': case 'J': case 'U':
        /* gap, highest attempted, interrupted, or unknown primality w/ extent of search */
        if (only_U_in_db && M == 'H')
         {
          old = exponent;
          while ((n = fgetc(in)) != EOF && n != '\n')
            ; /* skip rest of line */
          continue;
         }
        break; /* handled outside switch() in case we need to break out of the loop */
      default:
        fprintf(stderr, "%s: unrecognized flag letter after 'M( " FMT_UL " )' in %s\n", program_name, exponent,
                infile);
        return(1);
     }
    old = exponent; /* so DATABASE gets at most one entry for any exponent */
    if (!bare_exponent && ((n = fgetc(in)) != ':' || (n = fgetc(in)) != ' '))
     {
      fprintf(stderr, "unrecognized input after 'M( " FMT_UL " )' in %s\n", exponent, infile);
      break;
     }
    if (M == 'J') /* 'J'oined gap data, so the extent of factoring is the second number, not the first */
     {
      while (n != EOF && isascii(n) && isspace(n))
        n = fgetc(in);
      if (!isdigit(n))
       {
        (void)fprintf(stderr, "%s: invalid 'J' line for exponent " FMT_UL " in %s\n", program_name, exponent,
                      infile);
        break;
       }
      while (n != EOF && isascii(n) && isdigit(n))
        n = fgetc(in);
      if (n != ' ')
       {
        (void)fprintf(stderr, "%s: invalid 'J' line for exponent " FMT_UL " in %s\n", program_name, exponent,
                      infile);
        break;
       }
     }
    if (!bare_exponent && fscanf(in, "%lf", &f) != 1)
     {
      fprintf(stderr, "%s: unrecognized input after 'M( " FMT_UL " )%c: ' in %s\n", program_name, exponent, M,
              infile);
      break;
     }
    if (bare_exponent)
      p = 0;
    else /* check for '2^56' format */
      if (fabs(f - 2.0) < 0.1 && (n = fgetc(in)) == '^' && fscanf(in, "%d", &n) == 1)
       {
        if (n >= MAX_POWER_OF_2)
          p = MAX_POWER_OF_2 - 1;
        else
          p = (unsigned char)n;
        if (p == 0)
          f = 1.5;
        else
          if (M == 'I' /* && p > 0 */ )
            f = pow(2.0, --p + 0.5);
          else
            f = pow(2.0, (double)p);
       }
      else
       {
        if (M == 'I')
          f -= 2*(double)exponent;
        if ((f - 1.0)/2.0 <= (double)exponent)
          p = 0, f = 1.5;
        else
          if (f >= ldexp(1.0, MAX_POWER_OF_2 - 1))
            p = MAX_POWER_OF_2 - 1;
          else
            p = (unsigned char)(log2(f));
       }
    /* note that certain strange input, like 'M( 11 )C: 2^\nM( ', might be accepted, harmlessly */
    while ((n = fgetc(in)) != EOF && n != '\n')
      ; /* skip rest of line */
    ++stats[p];
    ++cnt;
    if (bare_exponent || (f >= pow(2.0, (double)p) && f < pow(2.0, p + 1.0)))
     {
      if (verbose > 3)
        printf(FMT_UL ",%d\n", exponent, p);
      if (M == 'H')
        p |= 0x80; /* high bit means at least one LL test or known to be a composite Mersenne */
      if (exponent > MAX_0xFF_EXPONENT && addition < exponent >> 24)
       {
        while (addition + 0xFFFFFF < exponent >> 24)
         {
          fputc(0xFF, out);
          fputc(0xFF, out);
          fputc(0xFF, out);
          fputc(0x7F, out);
          addition += 0xFFFFFF;
         }
        if (addition < exponent >> 24)
         {
          fputc((int)(((exponent >> 24) - addition) & 0xFF), out);
          fputc((int)((((exponent >> 24) - addition) >> 8) & 0xFF), out);
          fputc((int)((((exponent >> 24) - addition) >> 16) & 0xFF), out);
          fputc(0x7F, out);
          addition = exponent >> 24;
         }
       }
      else
        if (exponent > MAX_SIMPLE_EXPONENT && addition < exponent >> 24)
         {
          addition = exponent >> 24;
          fputc(0, out);
          fputc(0, out);
          fputc((int)addition & 0xFF, out);
          fputc(0xFF, out);
         }
      fputc((int)(exponent & 0xFF), out); /*!!these three lines depend on value of MAX_SIMPLE_EXPONENT */
      fputc((int)((exponent >> 8) & 0xFF), out);
      fputc((int)((exponent >> 16) & 0xFF), out);
      fputc(((int)p) & 0xFF, out); /*!!this line depends on value of MAX_POWER_OF_2 */
     }
    else
      if (verbose > 2)
        printf("# " FMT_UL "," FMT_UL "\n", exponent, (UL)log2(f));
   }
  if (ferror(in))
    fprintf(stderr, "contract: stdio error(s) occured reading %s\n", infile);
  if (ferror(out))
    fprintf(stderr, "contract: stdio error(s) occured writing %s\n", outfile);
  if (errno == 0 && ferror(out))
    errno = ferror(out);
  if (errno == 0 && ferror(in))
    errno = ferror(in);
  if (errno == 0 && !feof(in))
    errno = feof(in);
  (void)fclose(in);
  (void)fclose(out);
  if (cnt > 0 && verbose > 0)
   {
    printf("# %d exponents/Mersenne numbers\n# %6s  Extent of factoring\n", cnt, "Count");
    for (n = 0; n < MAX_POWER_OF_2; ++n)
      if (stats[n] > 0)
        printf("# %6d  2^ %d\n", stats[n], n);
   }
  return(errno);
 }
