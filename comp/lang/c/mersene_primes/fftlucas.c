static char RCSfftlucas_c[] = "$Id: fftlucas.c,v 6.50 1999/12/19 06:06:20 wedgingt Exp $ by wedgingt@acm.org";
char *RCSprogram_id = RCSfftlucas_c;
const char *program_name = "fftlucas"; /* for perror() and similar */
const char program_revision[] = "$Revision: 6.50 $";
char version[sizeof(RCSfftlucas_c) + sizeof(program_revision)]; /* overly long, but certain */

/* See README.fftlucas for notes on the subroutines. */

#ifdef macintosh
# include <console.h>
#endif

#include "setup.h"
#include "balance.h"
#include "fftlucas.h"
#include "globals.h"
#include "init_fft.h"
#include "lucas_init.h"
#include "lucas_sq.h"
#include "rw.h"
#include "zero.h"

#ifdef __STDC__
int main(int argc, char *argv[])
#else
int main(argc, argv)
int argc;
char *argv[];
#endif
 {
  UL n = 8L; /* FFT run length */
  UL q = 0L; /* exponent of Mersenne number being tested */
  UL i = 0L; /* iteration of test we're on or index into x: 0 <= i < n */
  UL last = 0L; /* last iteration (== q - 1) */
  BIG_DOUBLE err = 0.0; /* FFT error */
  BIG_DOUBLE *x = NULL; /* FFT array */
  int restarting = 0; /* flag: was the first FFT run length tried too small? */
  UL b = 0L, c = 0L;
  char M = '\0'; /* 'U', 'I', 'H', 'C', etc. */
  FILE *infp = NULL, *outfp = NULL, *dupfp = NULL;
  int primenet = 0; /* flag: interact with PrimeNet?  -N flag; see rw.c & rw.h */

#ifdef macintosh
  argc = ccommand(&argv);
#endif
  strcpy(version, program_name);
  strcat(version, " v");
  strncat(version, program_revision + strlen("4Revision: "),
          strlen(program_revision) - strlen("4Revison:  4"));
  /*!!will need a flag for this next part after merging in Kline's FHT/FFT code */
  strcat(version, " Crandall");
  setup();
  while (!terminate)
   {
    do /* while (restarting) */
     {
      switch ((restarting != 0) ? 3 :
              input(argc, argv, &q, &n, &i, &err, &x, last, &M, NULL, NULL, NULL, NULL, NULL,
                    (UL)0, (UL)0, &infp, &outfp, &dupfp, &primenet))
       {
        case 0: /* no more input */
          print_time();
          return(0);
        case 1: /* something wrong; error message, if any, already printed */
          print_time();
          return(1);
        case 2: /* continuing work from a partial result */
          restarting = 1; /* not the usual sense of restarting, but ... */
          break;
        case 3: /* starting work on a new exponent or restarting with a larger FFT run length */
          while (n < ((BIG_DOUBLE)q)/ROUNDED_HALF_MANTISSA + 4.0)
            n <<= 1;
          if (x != NULL)
            free((char *)x);
          if ((x = (BIG_DOUBLE *)calloc(n, sizeof(BIG_DOUBLE))) == NULL)
           {
            perror(program_name);
            (void)fprintf(stderr, "%s: cannot get memory for FFT array\n", program_name);
            print_time();
            return(errno == 0 ? 1 : errno);
           }
          for (i = 1; i < n; i++)
            x[i] = 0.0;
          x[0] = 4.0;
          i = 1;
          break;
       }
      if (!restarting)
       {
        if (M != 'U' && M != 'I' && last != q - 1)
          (void)fprintf(stderr, "%s: exponent " PRINTF_FMT_UL " should not need Lucas-Lehmer testing\n",
                        program_name, q);
        if ((M != 'U' && M != 'I') || last == q - 1)
          continue;
       }
      restarting = 0;
      init_fft(n);
      init_lucas(q, n, &b, &c);
      for (last = q - 1; !terminate && !restarting && i < last; i++)
       {
        err = lucas_square(x, n, b, c);
        x[0] -= 2.0;
        if (chkpnt_iterations != 0L && i % chkpnt_iterations == 1 && i < last &&
            check_point(q, n, i + 1, err, x, (UL)0, (UL)0, primenet) <= 0)
         {
          print_time();
          return(errno);
         }
        if (err > 0.49 || (err > 0.3 && q < 10000)) /* n is not big enough; increase it and start over */
         {
          n <<= 1;
          restarting = 1;
         }
       }
     } while (restarting);
    if (i < last) /* not done, but need to quit */
     {
      chkpnt_iterations = 0L;
      return((check_point(q, n, i, err, x, (UL)0, (UL)0, primenet) <= 0) ? errno : 0);
     }
    printbits(x, q, n, (q > 64L) ? 64L : q, b, c, high, low, (UL)0, (UL)0, version, outfp, dupfp, primenet);
    print_time();
   }
  return(0);
 }
