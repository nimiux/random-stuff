static char RCSextract_c[] = "$Id: extract.c,v 6.50 1999/12/19 06:06:20 wedgingt Exp $ by wedgingt@acm.org, http://www.garlic.com/~wedgingt/mersenne.html";

#if defined(linux) && defined(__GNUC__) && defined(__i386__) && !defined(DO_NOT_USE_LONG_LONG_EXPONENT)
typedef unsigned long long UL;
# define FMT_UL "%Lu"
#else
typedef unsigned long UL;
# define FMT_UL "%lu"
#endif

#include <stdio.h>

#ifdef __STDC__
# define PROTO(x) x
#else
# define PROTO(x) (/* x */)
#endif

#ifdef macintosh
# include <types.h>
# include <stat.h>
# include <console.h>
# ifndef isascii
#  define isascii(x) (((x) | 0177) == 0177)
# endif
#else
# include <sys/types.h>
# include <sys/stat.h>
# include <ctype.h>
#endif

#include <fcntl.h>
#include <time.h>
#include <string.h>

#ifdef sun
/* SunOS 4.1.3, at least, does not have difftime(3) */
# define difftime(final, start) (final - start)
#endif

typedef void (*out_function)(UL exponent, UL powerof2, UL lltests);

extern long atol PROTO((char *string));
extern int close PROTO((int fd));
extern int fclose PROTO((FILE *stream));
extern int fgetc PROTO((FILE *stream));
extern int fputc PROTO((int c, FILE *stream));
extern int fputs PROTO((const char *s, FILE *stream));
extern size_t fread PROTO((void *ptr, size_t size, size_t nitems, FILE *stream));
extern int isdigit PROTO((int ch));
extern void perror PROTO((const char *msg));
extern void rewind PROTO((FILE *stream));

#ifdef _AIX
extern int read PROTO((int fd, void *buf, size_t count));
#else
extern int fprintf PROTO((FILE *stream, const char *fmt, ...));
extern int printf PROTO((const char *fmt, ...));
extern int read PROTO((int fd, char *buf, size_t count));
#endif

/* The next four lines are copied from the mers package's rw.c; only MAGIC_NUMBER is actually used here */
/* checkpoint file formats: human readable and machine-specific binary with magic number ala file(1) */
#define CHKPNT_ASCII 0
#define CHKPNT_BINARY 1
#define MAGIC_NUMBER (0x76757473)

#ifdef __STDC__
static UL byte_swap(UL num, size_t len)
#else
static UL byte_swap(num, len)
UL num;
size_t len;
#endif
 {
  char swap[sizeof(UL)];
  size_t pos;

  /* unpack the number */
  for (pos = len; pos-- > 0; num >>= 8)
    swap[pos] = num & 0xff;
  /* pack the new number */
  for (pos = len, num = 0; pos-- > 0; )
   {
    num <<= 8;
    num |= swap[pos] & 0xff;
   }
  return(num);
 }

/*ARGSUSED*/
#ifdef __STDC__
static void one_column(UL exponent, UL powerof2, UL lltests)
#else
static void one_column(exponent, powerof2, lltests)
UL exponent, powerof2, lltests;
#endif
 {
  printf(FMT_UL "\n", exponent);
 }

/*ARGSUSED*/
#ifdef __STDC__
static void two_column(UL exponent, UL powerof2, UL lltests)
#else
static void two_column(exponent, powerof2, lltests)
UL exponent, powerof2, lltests;
#endif
 {
  printf(FMT_UL "," FMT_UL "\n", exponent, powerof2);
 }

#ifdef __STDC__
static void three_column(UL exponent, UL powerof2, UL lltests)
#else
static void three_column(exponent, powerof2, lltests)
UL exponent, powerof2, lltests;
#endif
 {
  printf(FMT_UL "," FMT_UL "," FMT_UL "\n", exponent, powerof2, lltests);
 }

#ifdef __STDC__
static void four_column(UL exponent, UL powerof2, UL lltests)
#else
static void four_column(exponent, powerof2, lltests)
UL exponent, powerof2, lltests;
#endif
 {
  printf("M( " FMT_UL " )%c: 2^" FMT_UL "\n", exponent, lltests ? 'H' : 'U', powerof2);
 }

static out_function out_funcs[4] =
 { one_column, two_column, three_column, four_column };

/* read George Woltman's DATABASE file: four byte entries with least significant byte first (Intel order) */
#ifdef __STDC__
static int woltmanDB(FILE *rfp, UL first, UL last, int cols, int print_all)
#else
static int woltmanDB(rfp, first, last, cols, print_all)
FILE *rfp;
UL first, last;
int cols, print_all;
#endif
 {
  int exponent, extent, LL_flag, first_0x7F = 1;
  UL addition = 0;

  cols--; /* now subscript for out_funcs[]() */
  while ((exponent = fgetc(rfp)) != EOF) /* first (lowest order) byte of four */
   {
    if ((extent = fgetc(rfp)) == EOF)
      return(1);
    exponent += extent << 8;
    if ((extent = fgetc(rfp)) == EOF)
      return(1);
    exponent += extent << 16;
    if ((extent = fgetc(rfp)) == EOF)
      return(1);
    if (extent == 0xFF)
     { /* special value: amount to increase further exponents by */
      addition = ((UL)exponent) << 8;
      continue;
     }
    if (extent == 0x7F)
     {
      if (first_0x7F)
       {
        addition = 0;
        first_0x7F = 0;
       }
      if (addition > ~(UL)0 - (((UL)exponent) << 24))
        return(2);
      addition += exponent << 24;
      continue;
     }
    LL_flag = (extent & 0x80) >> 7;
    extent &= ~0xFF80;
    if (exponent > ~(UL)0 - addition)
      return(2);
    if (last < exponent + addition && first > 0L)
      return(0);
    else
      if (first <= exponent + addition && (print_all || LL_flag == 0))
        out_funcs[cols]((UL)exponent + addition, (UL)extent, (UL)LL_flag);
   }
  return(0);
 }

#ifdef __STDC__
static void print_time_left(UL q, UL iter, UL iter2, time_t date, time_t date2)
#else
static void print_time_left(q, iter, iter2, date, date2)
UL q, iter, iter2;
time_t date, date2;
#endif
 {
  double seconds; /* time_t may not be big enough */
  UL left;

  if (iter2 > iter)
   {
    seconds = difftime(date2, date);
    left = q - 1 - iter2;
    iter = iter2 - iter;
   }
  else
   {
    seconds = difftime(date, date2);
    left = q - 1 - iter;
    iter = iter - iter2;
    date2 = date;
   }
  seconds *= ((double)left)/(double)iter;
  date2 += (time_t)seconds;
  seconds = difftime(date2, time(NULL));
  if (seconds < 5.0)
   {
    printf("Exponent " FMT_UL " could complete any time now\n", q);
    return;
   }
  printf("Exponent " FMT_UL " will complete in roughly ", q);
  left = (UL)(seconds/7.0/24.0/60.0/60.0);
  if (left > 0)
    printf(FMT_UL " week%s ", left, left > 1 ? "s" : ""),
      seconds -= left*7.0*24.0*60.0*60.0;
  left = (UL)(seconds/24.0/60.0/60.0);
  if (left > 0)
    printf(FMT_UL " day%s ", left, left > 1 ? "s" : ""),
      seconds -= left*24.0*60.0*60.0;
  left = (UL)(seconds/60.0/60.0);
  if (left > 0)
    printf(FMT_UL " hour%s ", left, left > 1 ? "s" : ""),
      seconds -= left*60.0*60.0;
  left = (UL)(seconds/60.0);
  if (left > 0)
    printf(FMT_UL " minute%s ", left, left > 1 ? "s" : ""),
      seconds -= left*60.0;
  if (seconds > 2.0)
    printf("%.0f seconds", seconds);
  printf("\n\tat about %s", ctime(&date2));
 }

#ifdef __STDC__
int main(int argc, char **argv)
#else
int main(argc, argv)
int argc;
char **argv;
#endif
 {
  const int mode = O_RDONLY
#ifdef O_BINARY
    | O_BINARY
#endif
    ;
  int rfd = -1;
  int columns = 4;
  int files_so_far = 0;
  int i, retval, swapped;
  UL q = 0L, n, j, magic, first = 0L, last = 0L;
  struct stat info; /* to get modify time of checkpoint file */
  UL q2 = 0L, n2 = 0L, j2 = 0L; /* of prior checkpoint */
  struct stat info2;
  FILE *infp = NULL;
  short fftlen; /* from one of George Woltman's program's checkpoint files (e.g. prime95's) */
  int pass = 0; /* Woltman factoring checkpoint pass (out of 16) */
  int print_all = 1; /* H and U lines from DATABASE */
  int woltman_fmt = 0; /* presume George Woltman's DATABASE format, no matter what the filename is */

#ifdef macintosh
  argc = ccommand(&argv);
#endif
  /* Note unusual use of argc as index into argv */
  for (argc = 1; argv[argc] != NULL;
       (void)(argc++, infp != NULL && ((void)fclose(infp), (void)close(rfd), rfd = -1, infp = NULL)))
   {
    switch (argv[argc][0])
     {
      case '-':
        switch (argv[argc][1])
         {
          case '1': /* one column format: exponents only */
          case '2': /* two column format: exponent,power-of-2 */
          case '3': /* three column format: exponent,power-of-2,[HU] */
          case '4': /* four column format: M( exponent )[HU]: 2^power-of-2 */
            columns = (argv[argc][1]) - '0';
            continue;
          case 'd': case 'D':
            woltman_fmt = 1;
            continue;
          case 'h': case 'H': case '?': /* help */
            fprintf(stderr, "extract version %s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n", RCSextract_c,
                    "\tUsage: extract [-[hH?]] [-[1234dm]] [-r first last] [-u] [DATABASE]",
                    "\t  Prints files that are in Woltman's DATABASE format in one of four human-readable",
                    "\t    forms, using either one, two, three, or four columns.  -m is the same as -4.",
                    "\t  If -d is given, DATABASE format is assumed; otherwise, only files actually",
                    "\t    named 'DATABASE' or 'database' are treated this way.",
                    "\t  If -r is given, prints only exponents between first and last.",
                    "\t  If -u is given, only data for 'U' exponents is printed (unknown primality).\n");
            fprintf(stderr, "\tUsage: extract <mers-LL-checkpoint-files>\n%s\n",
                    "\t  Prints exponent (q), FFT run length (n), and iteration (j) from each such file.\n");
            fprintf(stderr, "\tUsage: extract <Prime95-LL-checkpoint-files>\n%s\n",
                    "\t  Prints exponent, FFT run length, iteration, and other data from each such file.\n");
            fprintf(stderr, "  For LL checkpoint files, if two files for the same exponent are given\n%s",
                    "    next to each other, extract will also print an estimate of time to completion.\n");
            fprintf(stderr, "\tUsage: extract <Prime95-factoring-save-files>\n%s\n",
                    "\t  Prints exponent and current factoring pass from each such file.\n");
            fprintf(stderr, "\tUsage: extract <Factor98-save-files>\n%s\n",
                    "\t  Prints exponent, P-1 first bound, and other data from each such file.\n");
            fprintf(stderr, "Any number of files of any or all of the above types may be given.\n\n");
            fprintf(stderr, "The intent is that this program prints some human readable info\n%s%s%s",
                    "  about any binary format file that is associated with GIMPS (www.mersenne.org),\n",
                    "  so please let me know if it fails to do so or you have ideas for improvements.\n",
                    "Will Edgington, wedgingt@acm.org\n");
            return(0);
          case 'm': case 'M': /* m format: same as four column format */
            columns = 4;
            continue;
          case 'r': case 'R':
            if (argv[++argc] != NULL && isascii(argv[argc][0]) && isdigit(argv[argc][0]))
              first = atol(argv[argc]);
            if (argv[++argc] != NULL && isascii(argv[argc][0]) && isdigit(argv[argc][0]))
              last = atol(argv[argc]);
            if (0L < first && first < last)
              continue;
            fprintf(stderr, "extract: invalid first and last exponent values after '-r'\n");
            return(1);
          case 'u': case 'U':
            print_all = 0;
            continue;
          case 'v': case 'V':
            fprintf(stderr, "extract version %s (-h for help)\n", RCSextract_c);
            continue;
          default:
            fprintf(stderr, "extract: unknown option; 'extract -h' for version and help\n");
            return(0);
         }
      default:
        if ((rfd = open(argv[argc], mode)) < 0)
         {
          fprintf(stderr, "extract: error open()ing %s:\n", argv[argc]);
          perror("extract");
          return(1);
         }
        ++files_so_far;
        if (fstat(rfd, &info) != 0)
         {
          fprintf(stderr, "extract: fstat(): ");
          perror(argv[argc]);
          q2 = 0L;
          continue;
         }
        (void)close(rfd);
        if (info.st_size <= 0)
         {
          printf("%s: empty file\n", argv[argc]);
          continue;
         }
        /* find last component of filename if UNIX separator */
        for (i = strlen(argv[argc]) - 1; i > 0 && argv[argc][i] != '/'; i--)
          ;
        if (argv[argc][i] == '/')
          i++;
        else
         { /* no UNIX separators, so check for MSDOS/MSWindows separator */
          for (i = strlen(argv[argc]) - 1; i > 0 && argv[argc][i] != '\\'; i--)
            ;
          if (argv[argc][i] == '\\')
            i++;
         }
        if ((infp = fopen(argv[argc], "r")) == NULL)
         {
          fprintf(stderr, "extract: error fopen()ing %s:\n", argv[argc]);
          perror("extract");
          return(1);
         }
        /* is it one of George Woltman's DATABASE files? */
        if (woltman_fmt != 0 || strcmp(argv[argc] + i, "DATABASE") == 0
            || strcmp(argv[argc] + i, "database") == 0)
          if ((retval = woltmanDB(infp, first, last, columns, print_all)) != 0)
            return(retval);
          else
           {
            woltman_fmt = 0;
            continue;
           }
        if (atol(argv[argc] + i + 1) > 0) /* digits after first char of (last component of) file name */
          switch (argv[argc][i])
           {
            case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9':
            case 'c': case 'C': /* mers package LL tester checkpoint file; handled after switch ... */
              /* ... because the filename is not needed to recognize the format */
              break;
            case 'g': case 'G': /* checkpoint file from Factor98; Intel byte order */
              q = atol(argv[argc] + i + 1); /* Mersenne exponent */
              pass = fgetc(infp); /* two bytes of version number */
              pass += (fgetc(infp)) << 8;
              j = fgetc(infp); /* four bytes of Mersenne exponent */
              j += (fgetc(infp)) << 8;
              j += (fgetc(infp)) << 16;
              j += (fgetc(infp)) << 24;
              printf("%s: exponent " FMT_UL " P-1 checkpoint (Factor98 v%d binary, Intel ordering)\n",
                     argv[argc], q, pass);
              if (q != j)
                printf("Warning: %s contains M " FMT_UL " data!\n", argv[argc], q = j);
              fftlen = fgetc(infp); /* two bytes of FFT word length */
              fftlen += (fgetc(infp)) << 8;
              j = fgetc(infp); /* four bytes of counter */
              j += (fgetc(infp)) << 8;
              j += (fgetc(infp)) << 16;
              j += (fgetc(infp)) << 24;
              q2 = (UL)fgetc(infp); /* eight bytes: bound when file was converted from Factor95 format */
              q2 += ((UL)fgetc(infp)) << 8;
              q2 += ((UL)fgetc(infp)) << 16;
              q2 += ((UL)fgetc(infp)) << 24;
              q2 += ((UL)fgetc(infp)) << 32;
              q2 += ((UL)fgetc(infp)) << 40;
              q2 += ((UL)fgetc(infp)) << 48;
              q2 += ((UL)fgetc(infp)) << 56;
              n2 = (UL)fgetc(infp); /* eight bytes: stage one bound completed */
              n2 += ((UL)fgetc(infp)) << 8;
              n2 += ((UL)fgetc(infp)) << 16;
              n2 += ((UL)fgetc(infp)) << 24;
              n2 += ((UL)fgetc(infp)) << 32;
              n2 += ((UL)fgetc(infp)) << 40;
              n2 += ((UL)fgetc(infp)) << 48;
              n2 += ((UL)fgetc(infp)) << 56;
              printf("M( " FMT_UL " )o: " FMT_UL " # Factor98 stage one P-1 bound\n", q, n2);
              j2 = (UL)fgetc(infp); /* eight bytes: where the checksum on the file started */
              j2 += ((UL)fgetc(infp)) << 8;
              j2 += ((UL)fgetc(infp)) << 16;
              j2 += ((UL)fgetc(infp)) << 24;
              j2 += ((UL)fgetc(infp)) << 32;
              j2 += ((UL)fgetc(infp)) << 40;
              j2 += ((UL)fgetc(infp)) << 48;
              j2 += ((UL)fgetc(infp)) << 56;
              magic = (UL)fgetc(infp); /* eight bytes: file checksum */
              magic += ((UL)fgetc(infp)) << 8;
              magic += ((UL)fgetc(infp)) << 16;
              magic += ((UL)fgetc(infp)) << 24;
              magic += ((UL)fgetc(infp)) << 32;
              magic += ((UL)fgetc(infp)) << 40;
              magic += ((UL)fgetc(infp)) << 48;
              magic += ((UL)fgetc(infp)) << 56;
              printf("\tcnt " FMT_UL "; cnvrt " FMT_UL "; chksum " FMT_UL "; chksum start " FMT_UL "\n",
                     j, q2, magic, j2);
              /*!! bytes of residue: how many bytes? */
              /*!! four bytes of checksum of residue; EOF */
              continue;
            case 'm': case 'M': /* checkpoint file for George Woltman's new P-1 program */
              /* Intel byte order, least significant first */
              /*!!Note that the exponent is only available from the filename! */
              q = atol(argv[argc] + i + 1); /* Mersenne exponent */
              magic = fgetc(infp); /* four bytes of magic number */
              magic += ((UL)fgetc(infp)) << 8;
              magic += ((UL)fgetc(infp)) << 16;
              magic += ((UL)fgetc(infp)) << 24;
              j = fgetc(infp); /* four bytes of version */
              j += ((UL)fgetc(infp)) << 8;
              j += ((UL)fgetc(infp)) << 16;
              j += ((UL)fgetc(infp)) << 24;
              pass = fgetc(infp); /* stage: -1 fail, 0 stage1, 1 stage2, 2 done */
              pass += ((UL)fgetc(infp)) << 8;
              pass += ((UL)fgetc(infp)) << 16;
              pass += ((UL)fgetc(infp)) << 24;
              n = fgetc(infp); /* four bytes: P-1 limit B (stage one bound) */
              n += ((UL)fgetc(infp)) << 8;
              n += ((UL)fgetc(infp)) << 16;
              n += ((UL)fgetc(infp)) << 24;
              n2 = fgetc(infp); /* four bytes: P-1 limit B_processed (stage one done) */
              n2 += ((UL)fgetc(infp)) << 8;
              n2 += ((UL)fgetc(infp)) << 16;
              n2 += ((UL)fgetc(infp)) << 24;
              j2 = fgetc(infp); /* four bytes: P-1 limit C_processed (stage two done) */
              j2 += ((UL)fgetc(infp)) << 8;
              j2 += ((UL)fgetc(infp)) << 16;
              j2 += ((UL)fgetc(infp)) << 24;
              fftlen = fgetc(infp); /* four bytes: length of U in longs */
              fftlen += ((UL)fgetc(infp)) << 8;
              fftlen += ((UL)fgetc(infp)) << 16;
              fftlen += ((UL)fgetc(infp)) << 24;
              printf("%s: exponent " FMT_UL " P-1 checkpoint from Prime95 v" FMT_UL " (binary, Intel ordering)\n",
                     argv[argc], q, j);
              printf("M( " FMT_UL " )o: " FMT_UL " " FMT_UL " # Prime95 P-1", q, n2, j2);
              switch (pass)
               {
                case -1:
                  printf("\n  Problems!  Stage is -1, indicating a failure!\n");
                  break;
                case 0:
                  printf("; stage 1 going to " FMT_UL "\n", n);
                  break;
                case 1:
                  printf("; stage 2 going to " FMT_UL "\n", 100*n);
                  break;
                case 2:
                  printf(" (done)\n");
                  break;
                default:
                  printf("\tWarning: unknown value of stage recorded: %d\n", pass);
                  break;
               }
              if (magic != 0x1A2B3C4D && magic != 0xCD + 0x100*(UL)0x9AABBC && magic != (UL)0xFF)
                printf("\tProblems!  Unknown magic value recorded: " FMT_UL "\n", magic);
              /* 0x1C  long number  U */
              /* 0xend checksum  (doesn't include magic number and version) */
              continue;
            case 'p': case 'P': case 'q': case 'Q': /* George Woltman's LL test checkpoint file */
              /*  note that there is no 'magic number' ala the UNIX file(1) program */
              q = atol(argv[argc] + i + 1); /* Mersenne exponent */
              fftlen = fgetc(infp); /* Intel is low order bytes first */
              fftlen += (fgetc(infp)) << 8; /* 8 is NBBY of Intel architecture, not current arch */
              if (fftlen < 4)
               { /* Woltman factoring checkpoint */
                printf("%s: exponent " FMT_UL " trial factoring checkpoint (Woltman binary, Intel ordering)\n",
                       argv[argc], q);
                for (n = 0; n < 5; n++)
                  pass = fgetc(infp);
                if (pass >= 0)
                  printf("\t(in pass %d of 16)\n", pass);
                continue;
               }
              n = 1024*fftlen;
              j = fgetc(infp);
              j += ((UL)fgetc(infp)) << 8;
              j += ((UL)fgetc(infp)) << 16;
              j += ((UL)fgetc(infp)) << 24;
              printf("%s: exponent " FMT_UL " FFT length " FMT_UL " iteration " FMT_UL " (Woltman binary, Intel ordering)\n",
                     argv[argc], q, n, j);
              if (q == q2 && n == n2 && j != j2) /* same exponent and FFT length but different iteration */
                print_time_left(q, j, j2, info.st_mtime, info2.st_mtime);
              q2 = q;
              n2 = n;
              j2 = j;
              info2 = info;
              continue;
            case 't': case 'T': /* mers package LL tester checkpoint file; handled after switch ... */
              /* ... because the filename is not needed to recognize the format */
              break;
           }
        /* This if and else are for the 'c' and 't' cases of the switch that just ended: */
        /*  mers package Lucas-Lehmer test checkpoint format. */
        if (fgetc(infp) == 'R' && fgetc(infp) == 'I' && fgetc(infp) == ' ' && fgetc(infp) == 'q')
         { /* human-readable format; only used by fftlucas */
          for (printf("%s: RI q", argv[argc]), i = fgetc(infp); i != EOF && i != '\n'; i = fgetc(infp))
            fputc(i, stdout);
          fputs(" (printable)\n", stdout);
         }
        else
         { /* check for binary checkpoint format of rw.c (mersenne1, mersenne2, fftlucas) */
          rewind(infp);
          /* on some UNIX systems, fread() returns a byte count; on others it returns an 'object' count */
          if (((retval = fread(&magic, sizeof(magic), 1, infp)) != sizeof(magic) && retval != 1) ||
              ((retval = fread(&q, sizeof(q), 1, infp)) != sizeof(q) && retval != 1) ||
              ((retval = fread(&n, sizeof(n), 1, infp)) != sizeof(n) && retval != 1) ||
              ((retval = fread(&j, sizeof(j), 1, infp)) != sizeof(j) && retval != 1))
           {
            printf("%s: unknown data\n", argv[argc]);
            continue;
           }
          if (magic == MAGIC_NUMBER)
            swapped = 0;
          else
            if (sizeof(magic) == 8 && ((magic & (0xF + 0x10*(UL)0xFFFFFFF)) == MAGIC_NUMBER))
             { /* checkpoint was written with 4 byte UL's but we have 8 byte UL's */
              j = q >> 32;
              n = q & (0xF + 0x10*(UL)0xFFFFFFF);
              q = magic >> 32;
              magic &= (0xF + 0x10*(UL)0xFFFFFFF);
              swapped = 2;
             }
            else
             {
              swapped = 1;
              magic = byte_swap(magic, sizeof(magic));
              q = byte_swap(q, sizeof(q));
              n = byte_swap(n, sizeof(n));
              j = byte_swap(j, sizeof(j));
              if (sizeof(magic) == 8 && magic != MAGIC_NUMBER && (magic & (0xF + 0x10*(UL)0xFFFFFFF)) == MAGIC_NUMBER)
               { /*!!This case probably never actually occurs */
                j = q >> 32;
                n = q & (0xF + 0x10*(UL)0xFFFFFFF);
                q = magic >> 32;
                magic &= (0xF + 0x10*(UL)0xFFFFFFF);
                swapped = 3;
               }
              else
                if (sizeof(magic) == 8 && magic != MAGIC_NUMBER && (magic >> 32) == MAGIC_NUMBER)
                 {
                  j = q & (0xF + 0x10*(UL)0xFFFFFFF);
                  n = q >> 32;
                  q = magic & (0xF + 0x10*(UL)0xFFFFFFF);
                  magic >>= 32;
                  swapped = 3;
                 }
             }
          if (magic != MAGIC_NUMBER)
           {
            printf("%s: unknown data\n", argv[argc]);
            continue;
           }
          printf("%s: exponent " FMT_UL " FFT length " FMT_UL " iteration " FMT_UL " (binary)%s%s\n",
                 argv[argc], q, n, j, (swapped & 01) == 01 ? " (byte-swapped)" : "",
                 (swapped & 02) == 02 ? " (4 byte UL)" : "");
         }
        if (q == q2 && n == n2 && j != j2) /* same exponent and same FFT length but different iteration */
          print_time_left(q, j, j2, info.st_mtime, info2.st_mtime);
        q2 = q;
        n2 = n;
        j2 = j;
        info2 = info;
        continue;
     }
   }
  if (files_so_far > 0)
    return(0);
  if ((infp = fopen("DATABASE", "r")) == NULL)
   {
    perror("extract: error opening DATABASE:\nextract");
    return(1);
   }
  return(woltmanDB(infp, first, last, columns, print_all));
 }
