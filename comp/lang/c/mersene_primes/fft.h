#define DRCSfft_h "$Id: fft.h,v 6.50 1999/12/19 06:06:20 wedgingt Exp $"

#ifdef __STDC__
extern void fft(int n, BIG_DOUBLE *real, BIG_DOUBLE *imag);
#else
extern void fft();
#endif
