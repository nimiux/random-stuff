#define DRCSrealifft_h "$Id: realifft.h,v 6.50 1999/12/19 06:06:20 wedgingt Exp $"

#ifdef __STDC__
extern void realifft(UL n, BIG_DOUBLE *real);
#else
extern void realifft();
#endif
