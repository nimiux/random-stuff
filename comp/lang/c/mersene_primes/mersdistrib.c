static const char RCSmersdistrib_c[] = "$Id: mersdistrib.c,v 6.50 1999/12/19 06:06:20 wedgingt Exp $ wedgingt@acm.org";

/*
   This version is slightly modified from PLM's original at:

   http://www2.netdoor.com/~acurry/mersenne/archive2/0032.html

   Peter's original comment, just below, is slightly dated now in that
   there are presently 38 known Mersenne primes, which are provided in
   the primeM.txt file.

   -- Will Edgington, wedgingt@acm.org, 4 Mar 1998 (updated 10 July 1999)

   This program looks at the 34 known Mersenne prime exponents
   e_1, e_2, ..., e_34. In particular, it looks at the
   values ln(e_{i+1} / e_i) for 1 <= i <= 33.
   These appear to fit an exponential distribution
   with parameter lambda ~= ln(3/2).

   Peter Montgomery
   pmontgom@cwi.nl
   September, 1996

*/

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

#ifdef macintosh
# include <console.h>
#endif

#if defined(_AIX) || defined(__sgi__) || defined(SOLARIS) || defined(linux) || defined(__NetBSD__) || defined(macintosh)
# include <errno.h>
#endif

#define MAX_MERSENNES 64

typedef unsigned long UL;

UL mexpon[MAX_MERSENNES];
double ln_expon[MAX_MERSENNES];
double diffs[MAX_MERSENNES - 1];

const char *program_name = NULL;
static const char Usage[] = "  Usage: %s [file-containing-exponents-of-Mersenne-primes]\n%s\n";
static const char List[] = "  See http://www.garlic.com/~wedgingt/primeM.txt";

#ifdef __STDC__
static int double_cmp(const void *v_one, const void *v_two)
#else
static int double_cmp(v_one, v_two)
const void *v_one, *v_two;
#endif
 {
  double d_one = *(const double *)v_one, d_two = *(const double *)v_two;

  if (d_one < d_two)
    return(-1);
  if (d_one > d_two)
    return(1);
  return(0);
 }

#ifdef __STDC__
int main(int argc, char **argv)
#else
int main(argc, argv)
int argc;
char **argv;
#endif
 {
  FILE *infp = NULL;
  size_t i, cnt;
  double lambda, sum1, sum2, x;

#ifdef macintosh
  argc = ccommand(&argv);
#endif
  if (argv == NULL || argv[0] == NULL)
    program_name = "mersdistrib";
  else
    for (program_name = argv[0] + strlen(argv[0]); program_name > argv[0] && *program_name != '/'; )
      --program_name;
  if (argc > 2)
    fprintf(stderr, "%s: too many arguments\n", program_name);
  else
    if (argc > 1 && (infp = fopen(argv[1], "r")) == NULL)
      perror(program_name);
    else
      if (infp == NULL)
        infp = stdin;
  if (infp == NULL)
   {
    fprintf(stderr, Usage, program_name, List);
    return(argc == 1 ? errno : 1);
   }
  for (cnt = 0; cnt < MAX_MERSENNES && fscanf(infp, " %lu", mexpon + cnt) == 1; cnt++)
    ;
  if (ferror(infp))
   {
    fprintf(stderr, "%s: ", program_name);
    perror(argc > 1 ? argv[1] : "stdin");
   }
  if (cnt == MAX_MERSENNES)
    fprintf(stderr, "%s: increase MAX_MERSENNES (presently %d) and re-compile\n", program_name, MAX_MERSENNES);
  for (i = 0; i < cnt; i++)
    ln_expon[i] = log((double)mexpon[i]);
  lambda = (ln_expon[cnt - 1] - ln_expon[0])/(double)(cnt - 1);
  sum1 = 0.0;
  sum2 = 0.0;
  printf("Average ratio of adjacent Mersenne exponents is %7.3f\n", exp(lambda));
  for (i = 0; i < cnt - 1; i++)
   {
    diffs[i] = x = ln_expon[i+1] - ln_expon[i];
    printf("Gap %8.3f from %8ld to %8ld is %7.3f times the average\n",
           exp(x), mexpon[i], mexpon[i+1], x/lambda);
    sum2 += (x - lambda)*(x - lambda);
   }
  sum2 = sqrt(sum2/(double)(cnt-1));
  printf("Standard deviation is %7.3f times the average\n", sum2/lambda);
  /* Sort the logarithmic differences into ascending order */
  qsort(diffs, cnt - 1, sizeof(diffs[0]), double_cmp);
  for (i = 0; i < cnt - 1; i++)
    /* Estimate the probability that a gap will be diffi or smaller */
    /* Using the exponential distribution, this is 1 - exp(-diffi/lambda) */
    printf("Gap %3lu is %7.3f, expect %8.2f gaps this size or smaller\n",
           (UL)i+1, exp(diffs[i]), (cnt - 1)*(1 - exp(-diffs[i]/lambda)));
  return(0);
 }
