static const char RCSlucas_sq_c[] = "$Id: lucas_sq.c,v 6.50 1999/12/19 06:06:20 wedgingt Exp $";

#include "setup.h"
#include "addsignal.h"
#include "balance.h"
#include "globals.h"
#include "init_fft.h"
#include "lucas_sq.h"
#include "patch.h"
#include "square.h"

#ifdef __STDC__
BIG_DOUBLE lucas_square(BIG_DOUBLE *x, UL n, UL b, UL c)
#else
BIG_DOUBLE lucas_square(x, n, b, c)
BIG_DOUBLE *x;
UL n, b, c;
#endif
 {
  register UL j;
  register long *perm;
  register BIG_DOUBLE err, *ptrx, *ptry, *ptrmphi;

  perm = permute;
  ptry = scrambled;
  for (j = 0; j < n; ++j, perm++)
    *(ptry++) = x[*perm] * two_to_phi[*perm];
  squareg(scrambled, n);
  perm = permute;
  ptrx = x;
  ptrmphi = two_to_minusphi;
  for (j = 0; j < n; ++j)
    *(ptrx++) = scrambled[*(perm++)] *  *(ptrmphi++);
  err = addsignal(x, n, b, c);
  patch(x, n, b, c);
  check_balanced(x, n, b, c, high, low, (UL)0, (UL)0);
  return(err);
 }
