#define DRCSlucas_sq_h "$Id: lucas_sq.h,v 6.50 1999/12/19 06:06:20 wedgingt Exp $"

#include "square.h"
#include "addsignal.h"
#include "patch.h"
#include "balance.h"

#ifdef __STDC__
extern BIG_DOUBLE lucas_square(BIG_DOUBLE *x, UL n, UL b, UL c);
#else
extern BIG_DOUBLE lucas_square();
#endif
