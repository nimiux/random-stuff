#define DRCSsquare_h "$Id: square.h,v 6.50 1999/12/19 06:06:20 wedgingt Exp $"

#include "real2herm.h"
#include "sq_herm.h"
#include "herm2real.h"

#ifdef __STDC__
extern void squareg(BIG_DOUBLE *x, UL size);
#else
extern void squareg();
#endif
