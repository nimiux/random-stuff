#define DRCSreal2herm_h "$Id: real2herm.h,v 6.50 1999/12/19 06:06:20 wedgingt Exp $"

#ifdef __STDC__
extern void fft_real_to_hermitian(BIG_DOUBLE *z, UL n);
#else
extern void fft_real_to_hermitian();
#endif
