static const char RCSaddsignal_c[] = "$Id: addsignal.c,v 6.50 1999/12/19 06:06:20 wedgingt Exp $";

#include "setup.h"
#include "addsignal.h"
#include "globals.h"
#include "init_fft.h"

#ifdef __STDC__
BIG_DOUBLE addsignal(BIG_DOUBLE *x, UL n, UL b, UL c)
#else
BIG_DOUBLE addsignal(x, n, b, c)
BIG_DOUBLE *x;
UL n, b, c;
#endif
 {
  register UL k, j, bj, bk;
  register long sign_flip;
  register UL NminusOne = n - 1;
  register BIG_DOUBLE zz, w, *xptr = x, *xxptr;
  register BIG_DOUBLE hi = high, lo = low, hiinv = highinv, loinv = lowinv;
  BIG_DOUBLE err, maxerr = 0.0;

  bk = 0;
  for(k = 0; k < n; ++k)
   {
    zz = floor(fabs(*xptr) + 0.5);
     sign_flip=(*xptr<0);
     if (sign_flip)
	 err = fabs(zz + *xptr);
     else
	 err = fabs(zz - *xptr);
     if (err > maxerr)
	 maxerr = err;
     *xptr = 0;
     j = k;
     bj = bk;
     xxptr = xptr++;
     do {
          if(j==n) j=0;
	  if(j==0){xxptr = x; bj = 0;  w = floor(zz*hiinv);
	  	    if(sign_flip) *xxptr -= (zz-w*hi); else *xxptr += (zz-w*hi); }
	  else if(j==NminusOne) { w = floor(zz*loinv);
	             if(sign_flip) *xxptr -= (zz-w*lo); else *xxptr += (zz-w*lo); }
	    else if (bj >= c) { w = floor(zz*hiinv);
	    	    if(sign_flip) *xxptr -= (zz-w*hi); else *xxptr += (zz-w*hi); }
	       else { w = floor(zz*loinv);
	            if(sign_flip) *xxptr -= (zz-w*lo); else *xxptr += (zz-w*lo); }
          zz = w;
          ++j;
	  ++xxptr;
          bj += b; if(bj>=n) bj -= n;
          }  while(zz!=0.0);
     bk += b; if(bk>=n) bk -= n;
     }
     return(maxerr);
}

