static const char RCSmerskfac_c[] = "$Id: merskfac.c,v 6.50 1999/12/19 06:06:20 wedgingt Exp $ by wedgingt@acm.org";
const char *RCSprogram_id = RCSmerskfac_c; /*!!inadequate, as it says nothing about the factoring package */
const char program_revision[] = "$Revision: 6.50 $"; /*!!same problem */

/* Use freeLIP's ECM factoring functions to try to factor k in 2*k*p + 1 of factors of Mersennes */

#include "setup.h"
#include "factor.h"
#include "rw.h"

#ifdef macintosh
# include <console.h>
#endif

#if !defined(FACTOR_PACKAGE) || FACTOR_PACKAGE != FACTOR_FREELIP
This program can only be compiled with the freeLIP library as it
uses the Elliptic Curve Factoring functions from that library.
#endif

FACTOR savef, numer; /* not needed by this program, but faclip.h is and refers to them, so use them */
                     /*  where we'd otherwise use temporaries */

#ifndef ECM_MINBOUND
# define ECM_MINBOUND 100
#endif

const char *program_name = NULL;

/*ARGSUSED*/
#ifdef __STDC__
int main(int argc, char **argv)
#else
int main(argc, argv)
int argc;
char **argv;
#endif
 {
  EXPONENT p;
  FACTOR factor, remaining, tmp, tmp2;
  char M; /* C, D, H, I, or U */
  int expo = 0; /* power of current factor of k; currently only used for 2 */
  FILE *infp = NULL, *outfp = NULL, *dupfp = NULL;
  int task = 0;

#ifdef macintosh
  argc = ccommand(&argv);
#endif
  if (argv == NULL || argv[0] == NULL)
    program_name = "merskfac[unknown-type]";
  else
    for (program_name = argv[0] + strlen(argv[0]); program_name > argv[0] && *program_name != '/'; )
      --program_name;
  if (*program_name == '/')
    ++program_name;
  setup();
  /* init the math package and variables */
  fac_init();
  fac_init_var(factor);
  fac_init_var(remaining);
  fac_init_var(tmp);
  fac_init_var(tmp2);
  while (!terminate && /* borrow expo for all_factors arg of input(); we don't care about all_factors */
         (task = input(argc, argv, &p, NULL, NULL, NULL, NULL, (UL)0L, &M, &factor, &remaining, &expo,
                       NULL, NULL, (UL)0, (UL)0, &infp, &outfp, &dupfp, NULL)) > 1)
   {
    if (M != 'C')
      continue; /* we are only interested in known factors of Mersenne numbers */
    if (fac_div_int(factor, p, &remaining) != 1L)
     {
      fprintf(outfp, "%s: 'factor' of M(p = " PRINTF_FMT_UL ") is not 1 mod p\n", program_name, p);
      if (dupfp != NULL)
        fprintf(dupfp, "%s: 'factor' of M(p = " PRINTF_FMT_UL ") is not 1 mod p\n", program_name, p);
      continue;
     }
    fprintf(outfp, "M( " PRINTF_FMT_UL " )C: ", p);
    fprint_fac(outfp, factor);
    fprintf(outfp, " # = 1+p 2 ");
    if (dupfp != NULL)
     {
      fprintf(dupfp, "M( " PRINTF_FMT_UL " )C: ", p);
      fprint_fac(dupfp, factor);
      fprintf(dupfp, " # = 1+p 2 ");
     }
    for (expo = 1; zsdiv(remaining, 2L, &tmp) == 0; expo++)
      zcopy(tmp, &remaining);
    if (expo > 1)
     {
      fprintf(outfp, "^%d", expo);
      if (dupfp != NULL)
        fprintf(dupfp, "^%d", expo);
     }
    if (zscompare(remaining, 1L) == 0)
     {
      putc('\n', outfp);
      if (dupfp != NULL)
        putc('\n', dupfp);
      continue;
     }
    for (zintoz(1L, &tmp); !ziszero(tmp) && zcomposite(&remaining, 5, 3) == 1; )
     {
      switch ((int)zecm(remaining, &tmp, 0L, 250, ECM_MINBOUND, 1.05, 6, 0L))
       {
        case 0: /* could not find a(nother) factor; zecm() also returns 0 for problems(!) */
          zcopy(0L, &tmp);
          continue;
        case -1: /* should not occur; it's already been checked by zcomposite() */
          zcopy(0L, &tmp);
          continue;
        case 1: /* new factor in tmp */
          break;
        case 2: /* new (composite?) factor in tmp */
          break;
        default:
          fprintf(outfp, "\n%s: unknown return value from freelip:zecm()\n", program_name);
          if (dupfp != NULL)
            fprintf(dupfp, "\n%s: unknown return value from freelip:zecm()\n", program_name);
          zcopy(0L, &tmp);
          continue;
       }
      zdiv(remaining, tmp, &tmp2, &savef);
      if (!ziszero(savef))
       {
        fprintf(outfp, "\n%s: 'factor' of k for M(" PRINTF_FMT_UL ") left non-zero remainder\n", program_name, p);
        if (dupfp != NULL)
          fprintf(dupfp, "\n%s: 'factor' of k for M(" PRINTF_FMT_UL ") left non-zero remainder\n", program_name, p);
        zcopy(0L, &tmp);
        continue;
       }
      /* put larger of the factors in remaining for next time and smaller in tmp to check primality */
      if (zcompare(tmp, tmp2) > 0)
        zcopy(tmp, &remaining),
          zcopy(tmp2, &tmp);
      else
        zcopy(tmp2, &remaining);
      for (zintoz(1L, &tmp2); !ziszero(tmp2) && zcomposite(&tmp, 5, 3) == 1; )
       {
        switch ((int)zecm(tmp, &tmp2, 0, 250, ECM_MINBOUND, 1.05, 6, 0L))
         {
          case 0: /* this should never happen, but ... */
            zcopy(0L, &tmp2);
            continue;
          case -1: /* should not occur */
            zcopy(0L, &tmp2);
            continue;
          case 1: /* new factor of tmp in tmp2 */
            break;
          case 2: /* new (composite?) factor of tmp in tmp2 */
            break;
          default:
            fprintf(outfp, "\n%s: unknown return value from freelip:zecm()\n", program_name);
            if (dupfp != NULL)
              fprintf(dupfp, "\n%s: unknown return value from freelip:zecm()\n", program_name);
            zcopy(0L, &tmp2);
            continue;
         }
        zdiv(tmp, tmp2, &savef, &numer);
        if (!ziszero(numer))
         {
          fprintf(outfp, "Warning: non-zero remainder for factor of factor of k for M(" PRINTF_FMT_UL ")\n", p);
          if (dupfp != NULL)
            fprintf(dupfp, "Warning: non-zero remainder for factor of factor of k for M(" PRINTF_FMT_UL ")\n", p);
          zcopy(0L, &tmp2);
          continue;
         }
        if (zcompare(tmp2, savef) > 0)
          zcopy(tmp2, &tmp),
            zcopy(savef, &tmp2);
        else
          zcopy(savef, &tmp);
        putc(' ', outfp);
        if (dupfp != NULL)
          putc(' ', dupfp);
        if (zcomposite(&tmp2, 5, 3))
         {
          fputs("C=", outfp); /* to mark that factoring is incomplete */
          if (dupfp != NULL)
            fputs("C=", dupfp);
         }
        fprint_fac(outfp, tmp2);
        if (dupfp != NULL)
          fprint_fac(dupfp, tmp2);
       } /* for tmp2 != 0 && tmp is composite */
      putc(' ', outfp);
      if (dupfp != NULL)
        putc(' ', dupfp);
      if (ziszero(tmp2))
       {
        fputs("C=", outfp); /* to mark that factoring is incomplete */
        if (dupfp != NULL)
          fputs("C=", dupfp);
       }
      fprint_fac(outfp, tmp);
      if (dupfp != NULL)
        fprint_fac(dupfp, tmp);
     } /* for tmp != 0 and remaining is composite */
    putc(' ', outfp);
    if (dupfp != NULL)
      putc(' ', dupfp);
    if (ziszero(tmp))
     {
      fputs("C=", outfp); /* to mark that factoring is incomplete */
      if (dupfp != NULL)
        fputs("C=", dupfp);
     }
    fprint_fac(outfp, remaining);
    if (dupfp != NULL)
      fprint_fac(dupfp, remaining);
    putc('\n', outfp);
    if (dupfp != NULL)
      putc('\n', dupfp);
    print_time();
   }
  return(task == 1);
 }
