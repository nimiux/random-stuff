#define DRCSsq_herm_h "$Id: sq_herm.h,v 6.50 1999/12/19 06:06:20 wedgingt Exp $"

#ifdef __STDC__
extern void square_hermitian(BIG_DOUBLE *x, UL n);
#else
extern void square_hermitian();
#endif
