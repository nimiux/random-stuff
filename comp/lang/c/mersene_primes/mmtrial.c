static char RCSmmtrial_c[] = "$Id: mmtrial.c,v 6.50 1999/12/19 06:06:20 wedgingt Exp $ wedgingt@acm.org";
char *RCSprogram_id = RCSmmtrial_c;

/* mmtrial.c - (C) R.W.W. Hooft, 1997. Distribute freely. */
/* Modified by Will Edgington, wedgingt@acm.org, as part of the mers package on behalf of GIMPS */
/*  See http://www.garlic.com/~wedgingt/README.html and
        http://www.garlic.com/~wedgingt/mersenne.html */

/* Lower these two numbers if you would want to do smaller MM's and
   raise SIEVE_LIMIT if you want to do larger MM's, but note that
   SIEVE_LIMIT must be less than the largest prime returned by
   freeLIP's zpnext(). */

#define BATCH_SIZE ((unsigned long)500000)
#define SIEVE_LIMIT ((unsigned long)500000)

#include "setup.h"
#include "lip.h"
#include <stdio.h>

#ifdef macintosh
# include <console.h>
#endif

const char *program_name = NULL;

#define MAX_FILTER_PRIME (7)
#define FILTER_SIZE (3*5*7)
#define FILTER_MULT (8)
#define FILTER_MOD (FILTER_MULT*FILTER_SIZE)
char filter[FILTER_SIZE]; /* NBBY*FILTER_SIZE bits */

static void mkfilter PROTO((void))
 {
  int k;

  for (k = 0; k < FILTER_SIZE; k++)
    filter[k] = (1 << 7) + (1 << 1); /* all factors are either 1 or 7 mod 8 */
  for (k = 0; k < FILTER_MOD; k += 3)
    filter[k/NBBY] &= ~(1 << (k % NBBY));
  for (k = 0; k < FILTER_MOD; k += 5)
    filter[k/NBBY] &= ~(1 << (k % NBBY));
  for (k = 0; k < FILTER_MOD; k += 7)
    filter[k/NBBY] &= ~(1 << (k % NBBY));
 }

#ifdef __STDC__
static long findk(long ifilter, long td)
#else
static long findk(ifilter, td)
long ifilter, td;
#endif
 {
  static verylong K = NULL, Ifilter = NULL, Wanted = NULL, Td = NULL, Temp = NULL;

  zintoz(ifilter, &Ifilter);
  zintoz(ifilter - 1, &Wanted);
  zintoz(td, &Td);
  zgcd(Td, Ifilter, &Temp);
  if (ztoint(Temp) != 1)
    return(-1);
  zdivmod(Wanted, Td, Ifilter, &K);
  return(ztoint(K));
 }

char sieve[BATCH_SIZE]; /* NBBY*BATCH_SIZE bits */

#ifdef __STDC__
int main(int argc, char **argv) 
#else
int main(argc, argv)
int argc;
char **argv;
#endif
 {
  long exponent, k = 1, t120, td120, inode, ifilter, count, firstzero, nextzero, i, firstk;
  verylong Exponent = 0, Trial = 0, TrialDif = 0, P = 0, Remainder = 0, 
           One = 0, Two = 0, N120 = 0;
  int verbose = 0;

#ifdef macintosh
  argc = ccommand(&argv);
#endif
  if (argv == NULL || argv[0] == NULL)
    program_name = "mmtrial";
  else
    for (program_name = argv[0] + strlen(argv[0]); program_name > argv[0] && *program_name != '/'; )
      --program_name;
  if (*program_name == '/')
    ++program_name;
  setup();
  mkfilter();
  setlinebuf(stdout);

  /* parse the command line */
  if (argc < 2)
   {
    fprintf(stderr, "Usage: %s MM initialk; MM=mersenne exponent exponent\n", argv[0]);
    exit(0);
   }
  while (argv[1][0] == '-' && argv[1][1] == 'v')
   {
    argv++; argc--;
    if (argv[0][2] == '\0' && isascii(argv[1][0]) && isdigit(argv[1][0]))
     {
      argv++; argc--;
      verbose = atoi(argv[0]);
      continue;
     }
    if (!isascii(argv[0][2]) || !isdigit(argv[0][2]))
      verbose++;
    else
      verbose = atoi(argv[0] + 2);
   }
  exponent = atol(argv[1]);
  if (exponent < 5)
   {
    fprintf(stderr, "Give Mersenne exponent exponent > 4 as argument\n");
    exit(1);
   }
  zintoz(FILTER_MOD, &N120);
  zintoz(exponent, &Exponent);
  /* Calculate the prime which is the mersenne exponent */
  zintoz(1, &One);
  zintoz(2, &Two);
  zexp(Two, Exponent, &P);
  zsub(P, One, &P);
  /* Get initial K if specified */
  if (argc > 2)
    k = atol(argv[2]) - 1;
  if (k < 1)
    k = 1;
  /* Difference between successive trials */
  z2mul(P, &TrialDif);
  while (!terminate) /* Repeat batches of NBBY*BATCH_SIZE until signal'd to stop */
   { /* First value of trial */
    zsmul(TrialDif, k, &Trial);
    zadd(Trial, One, &Trial);
    /* Filter values */
    t120 = zsmod(Trial, FILTER_MOD);
    td120 = zsmod(TrialDif, FILTER_MOD);
    firstk = k;
    bzero(sieve, BATCH_SIZE);
    for (inode = 0; inode < NBBY*BATCH_SIZE; inode++, t120 += td120, t120 %= FILTER_MOD)
      sieve[inode/NBBY] |= ((filter[t120/NBBY] >> (t120 % NBBY)) & 1) << (inode % NBBY);
    k += NBBY*BATCH_SIZE;
    if (verbose > 2)
      printf("Last of batch is k=%ld\n", k - 1);
    zpstart();
    while ((ifilter = zpnext()) <= MAX_FILTER_PRIME)
      ;
    /* Calculate the first trial */
    zsmul(TrialDif, firstk, &Trial);
    zadd(Trial, One, &Trial);
    /* Now check all numbers in the array for divisibility by ifilter */
    for ( ; ifilter < SIEVE_LIMIT && (firstzero = findk(ifilter, zsmod(TrialDif, ifilter))) >= 0;
          ifilter = zpnext())
     { /* Next k for which 2*k*p+1 is divisible by ifilter */
      nextzero = (firstk/ifilter)*ifilter + firstzero;
      while (nextzero < firstk)
        nextzero += ifilter;
      while (nextzero < k) /* Remove nodes that are divisible */
       {
	sieve[(nextzero - firstk)/NBBY] &= ~(1 << ((nextzero - firstk) % NBBY));
	nextzero += ifilter;
       } /* End of sieving with ifilter */
     } /* End of sieving process */
    count = 0;
    for (inode = 0; inode < NBBY*BATCH_SIZE; inode++)
      if (sieve[inode/NBBY] & (1 << (inode % NBBY)))
        count++;
    if (verbose > 1)
      printf("Number left after sieving is %ld (out of %lu)\n", count, NBBY*BATCH_SIZE);
    for (inode = 0; !terminate && inode < NBBY*BATCH_SIZE; inode++) /* Do trials */
     {
      if (sieve[inode/NBBY] & (1 << (inode % NBBY)))
       {
        if (verbose > 3)
          printf("M( M( %ld ) )U: k=%ld done\n", exponent, firstk + inode - 1);
	zsmul(TrialDif, firstk + inode, &Trial);
	zadd(Trial, One, &Trial);
        zcopy(Two, &Remainder);
        for (i = 0; i < exponent; i++)
          zsqinmod(&Remainder, Trial);
	if (zcompare(Remainder, Two) == 0)
         {
          printf("M( M( %ld ) )C: ", exponent);
          zwrite(Trial);
	  printf(", k=%ld\n", firstk + inode);
         }
        if (verbose > 0)
          printf("M( M( %ld ) )U: k=%ld done\n", exponent, firstk + inode);
       }
     }
    if (verbose == 0)
      printf("M( M( %ld ) )U: k=%ld done\n", exponent, firstk + inode - 1);
   } /* Next batch */
  exit(0);
 }
