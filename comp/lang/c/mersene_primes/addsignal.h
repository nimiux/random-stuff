#define DRCSaddsignal_h "$Id: addsignal.h,v 6.50 1999/12/19 06:06:20 wedgingt Exp $"

#ifdef __STDC__
extern BIG_DOUBLE addsignal(BIG_DOUBLE *x, UL n, UL b, UL c);
#else
extern BIG_DOUBLE addsignal();
#endif
