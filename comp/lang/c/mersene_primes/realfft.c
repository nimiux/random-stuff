static const char RCSrealfft_c[] = "$Id: realfft.c,v 6.50 1999/12/19 06:06:20 wedgingt Exp $";

#include "setup.h"
#include "fht.h"
#include "realfft.h"

#ifdef __STDC__
void realfft(UL n, BIG_DOUBLE *real)
#else
void realfft(n, real)
UL n;
BIG_DOUBLE *real;
#endif
 {
  BIG_DOUBLE a, b;
  int i, j, k;

  fht(real, n);
  for (i = 1, j = n - 1, k = n/2; i < k; i++, j--)
   {
    a = real[i];
    b = real[j];
    real[j] = (a - b)*0.5;
    real[i] = (a + b)*0.5;
   } 
 }
