static const char RCSinit_fft_c[] = "$Id: init_fft.c,v 6.50 1999/12/19 06:06:20 wedgingt Exp $";

#include "setup.h"
#include "globals.h"
#include "init_fft.h"
#include "init_real.h"

#ifdef __STDC__
void init_fft(UL n)
#else
void init_fft(n)
UL n;
#endif
{
	UL j;
	BIG_DOUBLE e = TWOPI/n;

	if (cn != NULL) free(cn);
	cn = (BIG_DOUBLE *)calloc(n, sizeof(BIG_DOUBLE));
	if (sn != NULL) free(sn);
	sn = (BIG_DOUBLE *)calloc(n, sizeof(BIG_DOUBLE));
	for(j=0;j<n;j++) {
		cn[j] = cos(e*j);
		sn[j] = sin(e*j);
	}
	if (permute != NULL) free(permute);
	permute = (long *)calloc(n, sizeof(long));
	if (scrambled != NULL) free(scrambled);
	scrambled = (BIG_DOUBLE *)calloc(n, sizeof(BIG_DOUBLE));
	init_scramble_real(n);
}
