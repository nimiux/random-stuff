/* 1strs8.c
 * Calculate the "ones string" length for primes.
 * arguments: [-r<minlen> <maxlen>]: smallest and largest Mersenne exponent to print.
 *            <start>: first prime to find smallest Mersenne number which it factors.
 *            [<stop>]: largest value to calculate length of ones string of.
 * stdin: list of primes, in order, to skip (they are already known to factor a particular Mersenne).
 *        This is almost always redirected from factors.only.
 * stdout: primes and the smallest Mersenne number each factors.
 */
static char RCS1strs8_c[] = "$Id: 1strs8.c,v 6.50 1999/12/19 06:06:20 wedgingt Exp $ by wedgingt@acm.org";
char *RCSprogram_id = RCS1strs8_c;
const char *program_name = "1strs8"; /* for perror() and similar */
const char program_revision[] = "$Revision: 6.50 $";

#define MERS_1STRS

#ifndef FACTOR_PACKAGE
#define FACTOR_PACKAGE FACTOR_LONGLONG
#endif

#include "setup.h"
#include "factor.h"

FACTOR savef, numer; /* globals used by some factor.h routines */

#define SIEVE_SIZE (65528)
#define NUM_SMALL_PRIMES (6540)

unsigned char sieve[SIEVE_SIZE/NBBY];
#define CLEAR_BIT(x) (sieve[x/NBBY] &= ~(1 << (x & (NBBY - 1))))
#define TEST_BIT(x) (sieve[x/NBBY] & (1 << (x & (NBBY - 1))))
#define SET_VALUE ((1 << NBBY) - 1)

/* attempt to use smaller types to save memory */
/*  subscript 0 is prime 3 gives subscript 6541 is prime 65537; 3511 is 32771; 53 is 257 */
/* we want to avoid signed types, however */
#if (NUM_SMALL_PRIMES < 53) && (SIEVE_SIZE < 256)
typedef unsigned char INTEGER;
#else
# if (NUM_SMALL_PRIMES < 6541) && (SIEVE_SIZE < 65536)
typedef unsigned short INTEGER;
# else
typedef unsigned long INTEGER;
# endif
#endif

struct small_prime_info
 {
  INTEGER prime;
  INTEGER bit_to_clear;
 } small_primes[NUM_SMALL_PRIMES];

/*!!Next is lifted from testerl3.c v1.209, but note that num2 is always 2 here, */
/*!! so it is _much_ simpler */
/* Compute x such that 2*x = -1 MOD small_primes[which].prime */
/* We can then use x to compute the first bit in the sieve array */
/*  that to be cleared */
static INTEGER euclid(FACTOR p, UL which)
 {
  INTEGER x = small_primes[which].prime/2;
  UL temp = fac_mod_int(p, (UL)small_primes[which].prime);

  /*!!note the next line's product when choosing the types (& their lengths) involved */
  return((x*temp) % small_primes[which].prime);
 }

int main(int argc, char **argv)
 {
  register UL factor, /* factor of length (of 1s string) */
    minlen = 601, /* smallest exponent we care about; all smaller than M601 are completely factored */
    maxlen = ~(UL)0; /* largest exponent we care about */
  FACTOR p, /* prime for which we are calculating the 1s string length */
    pshift, /* p, shifted; note that first shifted bit must be a 1 */
    acc, /* accumulator: power of two mod p */
    length, length2, /* length of 1s string so far */
    cofactor, /* cofactor of length after dividing out factors as they're found */
    bit, /* power of two just larger than pshift */
    stop, /* largest p to try */
    skip; /* prime to skip; read from stdin */
  UL small; /* used to read small primes and then as temporary */
  INTEGER which, next; /* subscripts for small_primes array */
  FILE *primesfp; /* for reading the small primes from /usr/games/primes */

  fac_init();
  fac_init_var(p);
  fac_init_var(pshift);
  fac_init_var(acc);
  fac_init_var(length);
  fac_init_var(length2);
  fac_init_var(cofactor);
  fac_init_var(bit);
  fac_init_var(stop);
  fac_init_var(skip);
  fac_set_int((UL)0, &skip);
  if (argc < 2 || !isascii(argv[1][0]) || (!isdigit(argv[1][0]) && (argv[1][0] != '-' || argv[1][1] != 'r')))
   {
    fprintf(stderr, "\tUsage: %s [-r<minlen> <maxlen>] <start> [<stop>]\n", program_name);
    return(1);
   }
  if (argc > 3 && argv[1][0] == '-' && argv[1][1] == 'r')
   {
    minlen = atol(*++argv + 2);
    maxlen = atol(*++argv);
    argc -= 2;
   }
  if (minlen < 601)
    minlen = 601;
  if (minlen >= maxlen)
    maxlen = ~(UL)0;
  if (maxlen < minlen)
    maxlen = 32768;
  if (argc < 2 || sscan_fac(*++argv, &p) != 1 || fac_mod8(p) % 2 != 1)
   {
    fprintf(stderr, "%s: first non-flag argument, the odd number to start at, is required\n", program_name);
    fprintf(stderr, "\tUsage: %s [-r<minlen> <maxlen>] <start> [<stop>]\n", program_name);
    return(1);
   }
  if (--argc < 2 || sscan_fac(*++argv, &stop) != 1)
    fac_set_int((UL)0, &stop);
  if (!fac_less_than_int(stop, (UL)1) && !fac_less_than(p, stop))
   {
    fprintf(stderr, "%s: start must be less than stop\n", program_name);
    fprintf(stderr, "\tUsage: %s [-r<minlen> <maxlen>] <start> [<stop>]\n", program_name);
    return(1);
   }
  /* read small primes rather than duplicating code to find them */
  primesfp = popen("/usr/games/primes 3", "r");
  for (which = 0; which < NUM_SMALL_PRIMES; which++)
    if (fscanf(primesfp, "%Lu", &small) != 1 || (small >> NBBY*sizeof(INTEGER)) != 0)
      break;
    else
      small_primes[which].prime = (INTEGER)small;
  (void)pclose(primesfp);
  if (which < NUM_SMALL_PRIMES)
   {
    fprintf(stderr, "%s: problems reading small primes\n", program_name);
    return(1);
   }
  if (fac_less_than_int(p, (UL)small_primes[NUM_SMALL_PRIMES - 1].prime))
   {
    fprintf(stderr, "%s: starting number too small; must be at least %lu\n", program_name,
            (unsigned long)small_primes[NUM_SMALL_PRIMES - 1].prime);
    return(1);
   }
  /* set bits of sieve for numbers starting with p */
  for (which = 0; which < NUM_SMALL_PRIMES; which++)
    small_primes[which].bit_to_clear = euclid(p, (UL)which);
  memset(sieve, SET_VALUE, sizeof(sieve));
  next = SIEVE_SIZE;
  for (setup(); !terminate; next++, fac_add_int(p, (UL)2, &p))
   { /* step to next number not sieved out */
    for ( ; next < SIEVE_SIZE && !TEST_BIT(next); next++)
      fac_add_int(p, (UL)2, &p);
    while (next >= SIEVE_SIZE && !terminate)
     { /* do next sieve */
      memset(sieve, SET_VALUE, sizeof(sieve));
      for (which = 0; which < NUM_SMALL_PRIMES; which++)
       {
        for (small = small_primes[which].bit_to_clear; small < SIEVE_SIZE;
             small += small_primes[which].prime)
          CLEAR_BIT(small);
        small_primes[which].bit_to_clear = small - SIEVE_SIZE;
       }
      for (next = 0; next < SIEVE_SIZE && !TEST_BIT(next); next++)
        fac_add_int(p, (UL)2, &p);
     }
    if (terminate)
      break;
    while (fac_less_than(skip, p) && fscan_fac(stdin, &skip) == 1)
      ; /* read new skip's until at least p */
    if (!fac_less_than(p, skip))
     { /* p == skip, so skip to next p */
      if (fscan_fac(stdin, &skip) != 1) /* if there's a problem reading, then ... */
        fac_set_int(~(UL)0, &skip); /* ... try to avoid further compares */
      continue;
     }
    fac_prepare_denominator(p);
    for (fac_div_int(p, (UL)2, &pshift); (fac_mod8(pshift) & 1) == 0; fac_div_int(pshift, (UL)2, &pshift))
      ;
    for (small = 0, fac_set_int((UL)1, &bit); !fac_less_than(pshift, bit); small++)
      fac_times2(bit, &bit);
    small--;
    fac_set_int((UL)1, &length);
    fac_set_int((UL)2, &acc);
    fac_set_int((UL)3, &cofactor); /* acc + 1, for comparing against p */
    while (small > 0 && (fac_less_than_int(acc,(UL)1) || !fac_less_than_int(acc,(UL)2))
           && fac_less_than(cofactor, p) && !terminate)
     {
      fac_square_mod(acc, &acc);
      fac_times2(length, &length);
      --small;
      if (fac_bit(pshift, (UL)small) != 0)
       {
        fac_times2_mod(acc, &acc);
        fac_add_int(length, (UL)1, &length);
       }
      fac_prepare_numerator(acc);
      fac_mod(&acc);
      fac_add_int(acc, (UL)1, &cofactor);
     }
    fac_set(length, &cofactor);
    fac_add_int(length, (UL)2, &bit);
    while (!fac_less_than(p, bit) && (fac_less_than_int(acc,(UL)1) || !fac_less_than_int(acc,(UL)2))
           && !terminate)
     {
      fac_square_mod(acc, &acc);
      fac_mul_int(length, (UL)2, &length);
      fac_add_int(length, (UL)2, &bit);
     }
    if (fac_less_than_int(acc, (UL)1) || !fac_less_than_int(acc, (UL)2) /* 2^length != 1 (mod p): p !prime */
        || fac_less_than_int(length, minlen) || terminate) /* too small or need to exit */
      continue;

    factor = (UL)small_primes[which = 0].prime;
    while (!fac_less_than_int(cofactor, factor) && !terminate)
     {
      if (fac_less_than_int(cofactor, (UL)1) || !fac_less_than_int(cofactor, (UL)2))
       {
        fac_div_int(cofactor, factor, &bit);
        if (fac_less_than_int(bit, factor))
          if (fac_less_than_int(cofactor, ~(UL)0))
            factor = fac_mod_int(cofactor, ~(UL)0);
          else
            factor = 0; /* should become the cofactor, but that is probably too large: fake it */
       }
      while (factor != 0 && fac_mod_int(cofactor, factor) == 0)
        (void)fac_div_int(cofactor, factor, &cofactor);
      fac_set(length, &length2);
      while ((factor == 0 || fac_mod_int(length, factor) == 0) &&
             !fac_less_than(length, length2) && !fac_less_than(length2, length) &&
             !terminate)
       {
        if (factor == 0)
          fac_div(length, cofactor, &pshift, &bit); /* remainder, in bit, must be zero */
        else
          (void)fac_div_int(length, factor, &pshift); /* remainder, returned, must be zero */
        for (small = 0, fac_set_int((UL)1, &bit); !fac_less_than(pshift, bit); small++)
          fac_times2(bit, &bit);
        small--;
        fac_set_int((UL)1, &length2);
        fac_set_int((UL)2, &acc);
        while (small > 0 && (fac_less_than_int(acc, (UL)1) || !fac_less_than_int(acc, (UL)2)) &&
               !terminate)
         {
          fac_square_mod(acc, &acc);
          fac_times2(length2, &length2);
          --small;
          if (fac_bit(pshift, (UL)small) != 0)
           {
            fac_times2_mod(acc, &acc);
            fac_add_int(length2, (UL)1, &length2);
           }
          fac_prepare_numerator(acc);
          fac_mod(&acc);
         }
        if (!fac_less_than_int(acc, (UL)1) && fac_less_than_int(acc, (UL)2)) /* 2^length2 == 1 (mod p) */
         {
          factor = small_primes[which = 0].prime;
          fac_set(length2, &length);
          fac_set(length2, &cofactor);
          while (fac_mod8(cofactor) % 2 == 0)
            fac_div_int(cofactor, (UL)2, &cofactor);
          while (fac_mod_int(cofactor, (UL)3) == 0)
            fac_div_int(cofactor, (UL)3, &cofactor);
         }
       }
      if (factor == 0)
        fac_set_int((UL)1, &cofactor);
      if (which < NUM_SMALL_PRIMES - 1)
        factor = small_primes[++which].prime;
      else
        factor += 2;
     }
    fac_prepare_numerator(p);
    fac_prepare_denominator(length);
    fac_mod(&acc);
    if (fac_less_than_int(length, maxlen) && !fac_less_than_int(acc, (UL)1) && fac_less_than_int(acc, (UL)2)
        && !fac_less_than_int(length, minlen) && !terminate)
     {
      printf("M( ");
      fprint_fac(stdout, length);
      printf(" )C: ");
      fprint_fac(stdout, p);
      printf("\n");
     }
    if (!fac_less_than_int(stop, (UL)1) && fac_less_than(stop, p))
      break;
   }
  printf("M( %Lu )X: ", maxlen);
  fac_sub_int(p, (UL)4, &p);
  fprint_fac(stdout, p);
  printf(" 1strs8 %s end\n", program_revision);
  return(0);
 }
