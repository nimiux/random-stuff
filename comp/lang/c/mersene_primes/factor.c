static const char RCS_fac_c[] = "$Id: factor.c,v 6.50 1999/12/19 06:06:20 wedgingt Exp $";

#include <memory.h>
#include <math.h>
#include "setup.h"
#include "factor.h"
#include "rw.h"

FACTOR savef;	/* Factor in modulo operations */
FACTOR numer;	/* Numerator in modulo operations */

/* The following ideas could be implemented to speed up this program */
/* even more. */
/* 1)  If using a bit sieve and SIEVE_PASSES == 1, then fill the sieve */
/* with the bit pattern that will eliminate all 3 and 5 mod 8 factors. */
/* Eliminate the code within the loop that tests for 3 or 5 mod 8 factors. */
/* 2)  If using a byte sieve and SIEVE_PASSES == 1, then clear the sieve */
/* bytes that will eliminate all 3 and 5 mod 8 factors. */
/* Eliminate the code within the loop that tests for 3 or 5 mod 8 factors. */
/* 3)  If using a bit sieve, unravel the test loop so that 8 bits are tested */
/* in each loop iteration.  Load the sieve byte just once and test each bit */
/* using simple & instructions rather than the complex TEST_BIT macro. */
/* This will require changing the CLEAR_BIT macro to something like this */
/* sieve[x>>1] &= ~(1 << (p * 2 + (x & 1))) for the SIEVE_PASSES == 4 case */
/* The SIEVE_PASSES == 2 case would need changing too.  This change could */
/* also improve the programs locality. */

/*-------------------------------------*/
/* Start of user-definable parameters  */
/*-------------------------------------*/

/* These are the #defines that let you fine-tune this program */
/* for optimal performance on your machine.  These #defines should */
/* be independent of the math factoring package you choose. */

/* This program supports both a bit sieve and a byte sieve. */
/* A bit sieve uses every bit in a byte for sieving.  A byte sieve */
/* uses only one bit in each byte for sieving. */
/* Uncomment this #define to see if a bit sieve is faster on your machine */ 
/* # define BIT_SIEVE */

/* This is the size of the sieve in bytes.  This value should be chosen */
/* carefully.  The optimum value is often less than the size of your */
/* machine's primary memory cache. */

# ifndef SIEVE_SIZE_IN_BYTES
#  define SIEVE_SIZE_IN_BYTES	 4096
# endif

/* How many small primes to sieve.  This is the number of small primes */
/* that are used in clearing bits from the sieve.  The optimal value */
/* will depend on how fast your machine can clear bits from the sieve */
/* vs. the speed which your machine can test a potential factor of a */
/* Mersenne number. */

# ifndef NUM_SMALL_PRIMES
#  define NUM_SMALL_PRIMES  5000
# endif

/* The number of sieve passes - must be 1, 2, or 4.  Using more passes */
/* increases memory consumption in the small_primes_array and increases */
/* the number of calls to euclid().  However, using more passes makes */
/* process of clearing sieve bits more efficient. */

# ifndef SIEVE_PASSES
#  define SIEVE_PASSES	4
# endif

/* If you will be testing Mersenne exponents with factors that are */
/* less than the NUM_SMALL_PRIMESth prime number, then you may uncomment */
/* this #define (for slightly slower code). */

/* # define TESTING_SMALL_EXPONENTS */

/* Since some compilers have trouble aligning stack variables on */
/* eight byte boundaries, using the first #define allows you to turn */
/* stack variables into global variables which may be better aligned. */
/* On the other hand, use of the first #define may keep your compiler */
/* from using register optimizations. */

# define FACALIGN static
# define FACSTATIC
/* #define FACALIGN */
/* #undef FACSTATIC */

/*-----------------------------------*/
/* End of user-definable parameters  */
/*-----------------------------------*/

/* These are the typedefs that are used throughout this code. */
/* You should review these to make sure they are appropriate for */
/* your machine. */

/* Small primes used in clearing the sieve.  These will never */
/* exceed 32 bits.  If NUM_SMALL_PRIMES is set below 6541, then */
/* these will not exceed 16 bits.  It is usually better to use */
/* the short data type rather than the long data type to reduce */
/* memory consumption.  This leaves more cache space available */
/* for the sieve. */
# if NUM_SMALL_PRIMES > 6541
typedef unsigned long SMALL_PRIME;
# else
typedef unsigned short SMALL_PRIME;
# endif

/* Memory and macros used in maintaining the sieve */

unsigned char sieve[SIEVE_SIZE_IN_BYTES];
#ifdef BIT_SIEVE
# define SIEVE_SIZE	(SIEVE_SIZE_IN_BYTES*8)/SIEVE_PASSES
# define CLEAR_BIT(p,x)	(sieve[p*SIEVE_SIZE/8+(x>>3)] &= ~(1 << (x & 7)))
# define TEST_BIT(p,x)	(sieve[p*SIEVE_SIZE/8+(x>>3)] & (1 << (x & 7)))
# define SET_VALUE	(0xFF)
#else
# define SIEVE_SIZE	(SIEVE_SIZE_IN_BYTES/SIEVE_PASSES)
# define CLEAR_BIT(p,x)	(sieve[p*SIEVE_SIZE+x] = 0)
# define TEST_BIT(p,x)	(sieve[p*SIEVE_SIZE+x])
# define SET_VALUE	(1)
#endif

struct small_prime_info
 {
  SMALL_PRIME prime;
  SMALL_PRIME bit_to_clear[SIEVE_PASSES];
 } small_primes[NUM_SMALL_PRIMES];

/* This routine uses Euclid's GCD algorithm to determine */
/* the first bit in the sieve that will need clearing. */

/* Let prime = an entry from our small primes array */
/* Let y = num2 mod prime */
/* Use Euclid's greatest common denominator algorithm to compute */
/*  the number x such that x * y = -1 MOD prime */
/* We can then use x to compute the first bit in the sieve array */
/*  that needs clearing. */

#ifdef __STDC__
static SMALL_PRIME euclid(FACTOR f, UL prime, EXPONENT sieve_skip)
#else
static SMALL_PRIME euclid(f, prime, sieve_skip)
FACTOR f;
UL prime;
EXPONENT sieve_skip;
#endif
/* FACTOR f: The factor that the first bit in the sieve represents.
   UL prime: The small prime which we are trying to compute
     the first bit in the sieve that needs clearing (because the
     factor corresponding to that sieve bit is divisible by this small
     prime).
   EXPONENT sieve_skip: The amount added to the factor f for each bit
     in the sieve.  That is, if x is the value of the factor
     corresponding to the second bit in the sieve, then sieve_skip is
     x-f. */
 {
  long prev = 0;
  long cur = 1;
  SMALL_PRIME bigrem = (SMALL_PRIME) prime;
  SMALL_PRIME litrem = (SMALL_PRIME) (sieve_skip % prime);
  SMALL_PRIME rem;
  SMALL_PRIME x;
  long temp;

  /* Compute the GCD */
  while (litrem != 1)
   {
    temp = bigrem / litrem;
    rem = bigrem % litrem;
    bigrem = litrem;
    litrem = rem;
    temp = prev - temp*cur;
    prev = cur;
    cur = temp;
   }
  /* Normalize cur so that it is between 0 and prime */
  if (cur < 0)
    x = (SMALL_PRIME) -cur;
  else
    x = (SMALL_PRIME) (prime - cur);
  /* Now we compute the bit to clear! */
  /* Let's be careful - overflows can occur during multiplication */
#ifndef NBBY
# define NBBY 8
#endif
  if (prime < ((UL)1 << (NBBY/2)*sizeof(UL)))
   {
    UL z;
    z = x;
    z *= fac_mod_int(f, prime);
    return((SMALL_PRIME)(z % prime));
   }
  else
   {
    double z;
    z = x;
    z *= fac_mod_int(f, prime);
    return((SMALL_PRIME)(fmod(z, (double)prime)));
   }
 }

/* This routine finds factors of Mersenne numbers. */
/* It returns 0 if no factors are found and 1 if any factors are found. */
/* 'goto done' used if a factor found and not looking for all factors to */
/*  avoid repeating fac_dest_var() calls in four other locations. */
#ifdef __STDC__
int mersenne_factor(EXPONENT exponent, FACTOR first_factor, FACTOR last_factor,
                    int find_all_factors, int M, FILE *fp, FILE *dupfp)
#else
int mersenne_factor(exponent, first_factor, last_factor, find_all_factors, M, fp, dupfp)
EXPONENT exponent;
FACTOR first_factor, last_factor;
int find_all_factors;
int M;
FILE *fp, *dupfp;
#endif
/* EXPONENT exponent: The exponent of the Mersenne number that we are
     trying to find a factor of.
   FACTOR first_factor: The first factor to test.  This need not be of
     the form 2kp+1, this code will test for factors larger than this
     number.
   FACTOR last_factor: This routine will not test factors larger than
     this number.
   int find_all_factors: TRUE if this routine should continue
     searching for factors after the first factor is found.
   int M: the mers format letter; 'U', 'I', 'H', 'G', 'J', etc.
   FILE *fp: file pointer to write to
   FILE *dupfp: duplicate output to also write to */
 {
  static int first_call = 1;	/* True if this is the first time this */
				/* routine has been called. */
  unsigned int i, j, pass;	/* Loop indexes */
  struct small_prime_info *sp; /* Pointer into the small_prime array */
  FACALIGN FACTOR factor[SIEVE_PASSES]; /* Trial factor(s) of the Mersenne number */
  FACALIGN FACTOR startacc;	/* Starting value for accumulator */
  FACALIGN FACTOR acc;		/* Accumulator */
  FACALIGN FACTOR stop;		/* Copy of last_factor unless that is too large to represent accurately */
  EXPONENT factor_incr;		/* Amount to add to factor for each bit in the sieve */
  EXPONENT bit, topbit;		/* Used in controlling the square-and-modulo loop */
  int found_one = 0;		/* found any factors yet? return value of function */
  int ok_to_print = 0;		/* OK to print the work performed? */

#ifndef FACSTATIC
  fac_init_var(startacc);
  fac_init_var(acc);
  fac_init_var(stop);
  for (pass = 0; pass < SIEVE_PASSES; pass++)
    fac_init_var(factor[pass]);
#endif

/* Fill in the small primes table.  This needs to be done only once. */
  if (first_call)
   {
    SMALL_PRIME x, y;

    first_call = 0;
#ifdef FACSTATIC
    fac_init_var(startacc);
    fac_init_var(acc);
    fac_init_var(stop);
    for (pass = 0; pass < SIEVE_PASSES; pass++)
      fac_init_var(factor[pass]);
#endif
    small_primes[0].prime = 3;
    small_primes[1].prime = 5;
    small_primes[2].prime = 7;
    for (i = 3, x = 11; i < NUM_SMALL_PRIMES; x += 2)
     {
      for (y = 3, j = 0; y*y <= x; y = small_primes[++j].prime)
        if (x % y == 0)
          break;
      if (x % y != 0)
        small_primes[i++].prime = x;
     }
   }
  /* Compute the first factor larger than first_factor.  Remember */
  /* factors of odd exponent Mersenne numbers must be of the form 2kp+1. */
  /* Factors of even exponent Mersennes are of the form kp+1. */
  fac_set_int(exponent, &stop);
  fac_mul_int(stop, (UL)2, &stop);
  if (fac_less_than(first_factor, stop))
    fac_set_int((UL)1, &acc);
  else
   {
    fac_div_int(first_factor, exponent, &stop);
    fac_div_int(stop, (UL)2, &acc);
   }
  fac_mul_int(acc, exponent, &acc);
  fac_mul_int(acc, (UL)2, &acc);
  fac_add_int(acc, (UL)1, &factor[0]);
#if fac_max_exp_of_2 > 0
  /* is the starting factor too large to represent? */
  /*  see if it appears even, which should always work */
  if (fac_mod_int(factor[0], 2) == 0)
    goto done;
#endif
  /* Figure out how much we will increment the factor for */
  /* each bit in the sieve */
#if SIEVE_PASSES == 1
# define FAC_INCR_MUL 2
#endif
#if SIEVE_PASSES == 2
# define FAC_INCR_MUL 8
#endif
#if SIEVE_PASSES == 4
# define FAC_INCR_MUL 24
#endif
  factor_incr = FAC_INCR_MUL*exponent;
  if (factor_incr/FAC_INCR_MUL != exponent) /* factor_incr exceeds an */
    /* EXPONENT; either use a bigger type for EXPONENT or fewer SIEVE_PASSES */
    goto done;
#undef FAC_INCR_MUL
  fac_div_int(last_factor, exponent, &acc);
  fac_div_int(acc, (UL)2, &acc);
  fac_add_int(acc, (UL)1, &acc);
  fac_mul_int(acc, exponent, &acc);
  fac_mul_int(acc, (UL)2, &acc);
  fac_add_int(acc, (UL)1, &stop);
#if fac_max_exp_of_2 > 0
  /* is the last factor to be attempted too large to be represented? */
  if (fac_mod_int(stop, 2) == 0)
   {
    do
     { /* divide by two, but result constrained to form of 2kp+1 */
      fac_div_int(stop, exponent, &acc);
      fac_div_int(stop, 4L, &acc);
      fac_mul_int(acc, exponent, &acc);
      fac_mul_int(acc, 2L, &acc);
      fac_add_int(acc, 1, &stop);
     } while (fac_mod_int(stop, 2) == 0);
   }
  fac_add_int(first_factor, SIEVE_SIZE*factor_incr, &acc);
  if (!fac_less_than(acc, stop))
   {
    fac_set(acc, &stop);
    if (fac_mod_int(stop, 2) == 0)
      goto done; /* can't do even a single set of passes */
   }
#endif
  /* If we're making two sieve passes, one pass will test the 1 mod 8 */
  /* factors, the other will test the 7 mod 8 factors. */
  /* If we're making four sieve passes, then we will test the */
  /* 1, 7, 17, and 23 mod 24 factors. */
  for (i = (SIEVE_PASSES < 2); i < SIEVE_PASSES; )
   {
#if SIEVE_PASSES == 2
    j = fac_mod8(factor[i]);
#endif
#if SIEVE_PASSES == 4
    j = fac_mod_int(factor[i], (UL)24);
#endif
    if (j == 1 || j == 7 || j == 17 || j == 23)
     {
      if (++i == SIEVE_PASSES)
        break;
      fac_add_int(factor[i - 1], exponent*2, factor + i);
     }
    else
      fac_add_int(factor[i], exponent*2, factor + i);
   }
  /* Initialize the small primes array with the position of the first */
  /* sieve bit to clear. */
  for (i = (SIEVE_PASSES > 2), sp = small_primes + i; i < NUM_SMALL_PRIMES; i++, sp++)
   {
#ifdef TESTING_SMALL_EXPONENTS
    /* this will also allow testing of odd composite exponents, I think */
    if (factor_incr % sp->prime == 0)
      break;
#endif
    for (pass = 0; pass < SIEVE_PASSES; pass++)
      sp->bit_to_clear[pass] = euclid(factor[pass], (UL)sp->prime, factor_incr);
   }
  /* Figure out the most significant bit in the Mersenne exponent. */
  for (topbit = 1; exponent >= topbit + topbit; topbit <<= 1)
    ;
  /* We will be computing the number 2^exponent modulo factor using */
  /* a standard square-and-modulo technique.  The modulo part of the */
  /* first few square-and-modulo operations is a no-op because the */
  /* squared number is so small.  We will compute these squarings */
  /* here - outside of the main loop. */
  fac_set_int((UL)2, &startacc);
  while ((topbit >>= 1) > 1
#if fac_max_exp_of_2 > 0
         /* overflowing here could cause factors to be missed */
         && fac_less_than_int(startacc, (1L << (fac_max_exp_of_2/2 - 2)))
#endif
    )
   {
    if (fac_less_than(factor[0], startacc))
      break;
    fac_square(startacc, &startacc);
    if (exponent & topbit)
      fac_times2(startacc, &startacc);
   }
  fac_prepare_numerator(startacc);
  /* Main loop: until we've tested all the factors less than stop */
  /*  must go thru it at least once or the ending print will be incorrect */
  do /* while (!terminate && fac_less_than(factor[0], stop)) */
   {
    /* Fill the sieve */
    memset(sieve, SET_VALUE, sizeof(sieve));
    /* Clear bits from the sieve.  Every time a bit is cleared, the factor */
    /* corresponding to the bit in the sieve is divisible by the small prime */
    for (i = (SIEVE_PASSES > 2), sp = small_primes + i; i < NUM_SMALL_PRIMES;
         i++, sp++)
     {
#ifdef TESTING_SMALL_EXPONENTS
      if (exponent == sp->prime)
        break;
#endif
      for (pass = 0; pass < SIEVE_PASSES; pass++)
       { /* Clear this pass's sieve bits and remember the first bit that */
        /*   will need to be cleared in the next pass! */
        for (j = sp->bit_to_clear[pass]; j < SIEVE_SIZE; j += sp->prime)
          CLEAR_BIT(pass, j);
        sp->bit_to_clear[pass] = (SMALL_PRIME)(j - SIEVE_SIZE);
       }
     }
    /* Scan the sieve.  We only need to test the factors that were not */
    /* divisible by a small prime */
    for (i = 0; (!terminate && i < SIEVE_SIZE) || i == 0; i++)
     { /* force one pass no matter what; otherwise, acc could be anything and ending print will be wrong */
      for (pass = 0; pass < SIEVE_PASSES; pass++)
       { /* If sieve bit is set, then test this factor.  Note that we need to */
        /*   eliminate 3 mod 8 and 5 mod 8 factors in one pass sieves. */
        if (TEST_BIT(pass, i)
#if SIEVE_PASSES == 1
            && (j = fac_mod8(factor[pass])) != 3 && j != 5
#endif
          )
         {
          fac_prepare_denominator(factor[pass]);
          fac_mod(&acc);
          /* Loop through the remaining bits in the exponent, doing squaring */
          /* modulo factor operations (and occasional multiplies by 2). */
          for (bit = topbit; bit; bit >>= 1)
           {
            fac_square_mod(acc, &acc);
            if (exponent & bit)
              fac_times2_mod(acc, &acc);
           }
          /* If the result of all this squaring modulo factor is that the */
          /* accumulator equals factor+1, then we have found a factor of */
          /* the Mersenne number. */
          if (fac_is_winner(acc))
           {
            fac_fprint_winner(fp, exponent, factor[pass], dupfp);
            found_one = 1;
            if (!find_all_factors)
             { /* put search limit in acc for later call to fac_fprint_end() */
              fac_set(factor[pass], &acc);
              ok_to_print = 1;
              goto done;
             }
           }
         } /* if (TEST_BIT(pass, i)) */
        fac_add_int(factor[pass], factor_incr, factor + pass);
       } /* for (pass < SIEVE_PASSES) */
     } /* for (!terminate && i < SIEVE_SIZE) */
   } while (!terminate && fac_less_than(factor[0], stop));
  fac_sub_int(factor[SIEVE_PASSES - 1], factor_incr, &acc);
  ok_to_print = 1;

done: /* only label; only needed where returns would be used */

  if (ok_to_print)
    fac_fprint_end(fp, exponent, first_factor, acc, M, found_one, dupfp);
  else /* best we can do is repeat back what we were asked to do */
    fac_fprint_start(fp, exponent, first_factor, last_factor, M, dupfp);
#ifndef FACSTATIC
  fac_dest_var(startacc);
  fac_dest_var(acc);
  fac_dest_var(stop);
  for (i = 0; i < SIEVE_PASSES; i++)
    fac_dest_var(factor[i]);
#endif
  return(found_one);
 }

static char warned = 0;

#ifdef __STDC__
void fac_fprint_start(FILE *fp, EXPONENT e, FACTOR f, FACTOR f2, int M, FILE *dupfp)
#else
void fac_fprint_start(fp, e, f, f2, M, dupfp)
FILE *fp;
EXPONENT e;
FACTOR f;
FACTOR f2;
int M;
FILE *dupfp;
#endif
 {
  FILE *archfp;

  if (!ferror(fp))
   {
    (void)fprintf(fp, "M( " PRINTF_FMT_UL " )%c: ", e, M);
    (void)fprint_fac(fp, f);
    (void)fputc('\n', fp);
    if (presence("EeoqGJ", M) != NULL)
     {
      (void)fputc(' ', fp);
      (void)fprint_fac(fp, f2);
      (void)fputc('\n', fp);
     }
   }
  if (dupfp == NULL && !ferror(fp))
    return;
  if (ferror(fp) && !warned)
   {
    fprintf(stderr, "\n%s: errors writing to output file\n", program_name);
    if (dupfp != NULL && dupfp != stderr)
      fprintf(stderr, "\n%s: errors writing to output file\n", program_name);
    warned = 1;
   }
  if (dupfp == NULL && ferror(fp))
    dupfp = stderr;
  if (dupfp != NULL)
   {
    (void)fprintf(dupfp, "M( " PRINTF_FMT_UL " )%c: ", e, M);
    (void)fprint_fac(dupfp, f);
    if (presence("EeoqGJ", M) != NULL)
      (void)(fputc(' ', dupfp), fprint_fac(dupfp, f2));
    (void)fputc('\n', dupfp);
   }
  if (!ferror(fp))
    return;
  if ((archfp = open_archive()) == NULL)
   {
    fprintf(stderr, "%s: could not append to archive file '%s'\n", program_name, archive_name());
    if (ferror(stderr) && dupfp == stderr)
      exit(1); /* can't write anywhere, so no point in continuing */
    return;
   }
  (void)fprintf(archfp, "M( " PRINTF_FMT_UL " )%c: ", e, M);
  (void)fprint_fac(archfp, f);
  (void)fputc('\n', archfp);
  if (ferror(archfp))
    fprintf(stderr, "%s: problems appending to archive file '%s'\n", program_name, archive_name());
  (void)fclose(archfp);
  return;
 }

#ifdef __STDC__
void fac_fprint_end(FILE *fp, EXPONENT e, FACTOR f, FACTOR f2, int M, int got_one, FILE *dupfp)
#else
void fac_fprint_end(fp, e, f, f2, M, got_one, dupfp)
FILE *fp;
EXPONENT e;
FACTOR f;
FACTOR f2;
int M, got_one;
FILE *dupfp;
#endif
 {
  FILE *archfp;

  if (got_one && (presence("UIGJ", M) != NULL))
    M = 'H';
  else
    if (M != 'H') /* yes, even if M == 'I'; mersenne_factor() always finishes checking a possible factor */
      M = 'U';
  if (!ferror(fp))
   {
    (void)fprintf(fp, "M( " PRINTF_FMT_UL " )J: ", e);
    (void)fprint_fac(fp, f);
    (void)fputc(' ', fp);
    (void)fprint_fac(fp, f2);
    (void)fputc('\n', fp);
    (void)fprintf(fp, "M( " PRINTF_FMT_UL " )%c: ", e, M);
    (void)fprint_fac(fp, f2);
    (void)fputc('\n', fp);
   }
  if (dupfp == NULL && !ferror(fp))
    return;
  if (ferror(fp) && !warned)
   {
    fprintf(stderr, "\n%s: errors writing to normal output file\n", program_name);
    if (dupfp != NULL && dupfp != stderr)
      fprintf(stderr, "\n%s: errors writing to normal output file\n", program_name);
    warned = 1;
   }
  if (dupfp == NULL && ferror(fp))
    dupfp = stderr;
  if (dupfp != NULL)
   {
    (void)fprintf(stderr, "M( " PRINTF_FMT_UL " )J: ", e);
    (void)fprint_fac(stderr, f);
    (void)fputc(' ', stderr);
    (void)fprint_fac(stderr, f2);
    (void)fputc('\n', stderr);
    (void)fprintf(stderr, "M( " PRINTF_FMT_UL " )%c: ", e, M);
    (void)fprint_fac(stderr, f2);
    (void)fputc('\n', stderr);
   }
  if (!ferror(fp))
    return;
  if ((archfp = open_archive()) == NULL)
   {
    fprintf(stderr, "%s: could not append to archive file '%s'\n", program_name, archive_name());
    if (ferror(stderr) && dupfp == stderr)
      exit(1); /* can't write anywhere, so no point in continuing */
    return;
   }
  (void)fprintf(archfp, "M( " PRINTF_FMT_UL " )J: ", e);
  (void)fprint_fac(archfp, f);
  (void)fputc(' ', archfp);
  (void)fprint_fac(archfp, f2);
  (void)fputc('\n', archfp);
  (void)fprintf(archfp, "M( " PRINTF_FMT_UL " )%c: ", e, M);
  (void)fprint_fac(archfp, f2);
  (void)fputc('\n', archfp);
  if (ferror(archfp))
    fprintf(stderr, "%s: problems appending to archive file '%s'\n", program_name, archive_name());
  (void)fclose(archfp);
  return;
 }

#ifdef __STDC__
void fac_fprint_winner(FILE *fp, EXPONENT e, FACTOR f, FILE *dupfp)
#else
void fac_fprint_winner(fp, e, f, dupfp)
FILE *fp;
EXPONENT e;
FACTOR f;
FILE *dupfp;
#endif
 { /* the repeat of f as the 'stop' number will not be used since M == 'C'; can't use anything else ... */
  fac_fprint_start(fp, e, f, f, 'C', dupfp); /* ... since we don't know what a FACTOR looks like */
 }
