static const char RCSfht_c[] = "$Id: fht.c,v 6.50 1999/12/19 06:06:20 wedgingt Exp $";

#include "setup.h"
#include "fht.h"
#include "trig.h"

char fht_version[] = "Brcwl-Hrtly-Ron-dbld";

static int lastn = 0, swaplen = 0;
static int *swap = NULL;

/* Set up an array to help with swapping things during the fht(); */
#ifdef __STDC__
static void init_fht(UL n)
#else
static void init_fht(n)
UL n;
#endif
 {
  UL k, k1, k2;

  if (swap == NULL)
    swap = (int *)malloc(2*n*sizeof(int));
  else
    swap = (int *)realloc(swap, 2*n*sizeof(int));
  if (swap == NULL)
    return;
  swaplen = 0;
  for (k1 = 1, k2 = 0; k1 < n; k1++)
   {
    for (k = n >> 1; !((k2 ^= k) & k); k >>= 1)
      ;
    if (k1 > k2)
     {
      swap[swaplen++] = k1;
      swap[swaplen++] = k2;
     }
   }
  return;
 }


#ifdef __STDC__
void fht(REAL *fz, UL n)
#else
void fht(fz, n)
REAL *fz;
UL n;
#endif
 {
  int i, k, k1, k2, k3, k4, kx;
  REAL *fi, *fn, *gi, swap_tmp;
  TRIG_VARS;

  if (n != lastn)
   {
    init_fht(n);
    lastn = n;
   }
  if (swap == NULL)
    for (k1 = 1, k2 = 0; k1 < n; k1++)
     {
      for (k = n >> 1; !((k2 ^= k) & k); k >>= 1)
        ;
      if (k1 > k2)
       {
        swap_tmp = fz[k1];
        fz[k1] = fz[k2];
        fz[k2] = swap_tmp;
       }
     }
  else
   for (i = 0; i < swaplen; i += 2)
    {
     swap_tmp = fz[swap[i]];
     fz[swap[i]] = fz[swap[i + 1]];
     fz[swap[i + 1]] = swap_tmp;
    }
 for (k = 0; (1 << k) < n; k++)
   ;
 k &= 1;
 if (k == 0)
    {
	 for (fi=fz,fn=fz+n;fi<fn;fi+=4)
	    {
	     REAL f0,f1,f2,f3;
	     f1     = fi[0 ]-fi[1 ];
	     f0     = fi[0 ]+fi[1 ];
	     f3     = fi[2 ]-fi[3 ];
	     f2     = fi[2 ]+fi[3 ];
	     fi[2 ] = (f0-f2);	
	     fi[0 ] = (f0+f2);
	     fi[3 ] = (f1-f3);	
	     fi[1 ] = (f1+f3);
	    }
    }
 else
    {
	 for (fi=fz,fn=fz+n,gi=fi+1;fi<fn;fi+=8,gi+=8)
	    {
	     REAL s1,c1,s2,c2,s3,c3,s4,c4,g0,f0,f1,g1,f2,g2,f3,g3;
	     c1     = fi[0 ] - gi[0 ];
	     s1     = fi[0 ] + gi[0 ];
	     c2     = fi[2 ] - gi[2 ];
	     s2     = fi[2 ] + gi[2 ];
	     c3     = fi[4 ] - gi[4 ];
	     s3     = fi[4 ] + gi[4 ];
	     c4     = fi[6 ] - gi[6 ];
	     s4     = fi[6 ] + gi[6 ];
	     f1     = (s1 - s2);	
	     f0     = (s1 + s2);
	     g1     = (c1 - c2);	
	     g0     = (c1 + c2);
	     f3     = (s3 - s4);	
	     f2     = (s3 + s4);
	     g3     = SQRT2*c4;		
	     g2     = SQRT2*c3;
	     fi[4 ] = f0 - f2;
	     fi[0 ] = f0 + f2;
	     fi[6 ] = f1 - f3;
	     fi[2 ] = f1 + f3;
	     gi[4 ] = g0 - g2;
	     gi[0 ] = g0 + g2;
	     gi[6 ] = g1 - g3;
	     gi[2 ] = g1 + g3;
	    }
    }
 if (n<16) return;

 do
    {
     REAL s1,c1;
     k  += 2;
     k1  = 1  << k;
     k2  = k1 << 1;
     k4  = k2 << 1;
     k3  = k2 + k1;
     kx  = k1 >> 1;
	 fi  = fz;
	 gi  = fi + kx;
	 fn  = fz + n;
	 do
	    {
	     REAL g0,f0,f1,g1,f2,g2,f3,g3;
	     f1      = fi[0 ] - fi[k1];
	     f0      = fi[0 ] + fi[k1];
	     f3      = fi[k2] - fi[k3];
	     f2      = fi[k2] + fi[k3];
	     fi[k2]  = f0	  - f2;
	     fi[0 ]  = f0	  + f2;
	     fi[k3]  = f1	  - f3;
	     fi[k1]  = f1	  + f3;
	     g1      = gi[0 ] - gi[k1];
	     g0      = gi[0 ] + gi[k1];
	     g3      = SQRT2  * gi[k3];
	     g2      = SQRT2  * gi[k2];
	     gi[k2]  = g0	  - g2;
	     gi[0 ]  = g0	  + g2;
	     gi[k3]  = g1	  - g3;
	     gi[k1]  = g1	  + g3;
	     gi     += k4;
	     fi     += k4;
	    } while (fi<fn);
     TRIG_INIT(k,c1,s1);
     for (i=1;i<kx;i++)
        {
	 REAL c2,s2;
         TRIG_NEXT(k,c1,s1);
         c2 = c1*c1 - s1*s1;
         s2 = 2*(c1*s1);
	     fn = fz + n;
	     fi = fz +i;
	     gi = fz +k1-i;
	     do
		{
		 REAL a,b,g0,f0,f1,g1,f2,g2,f3,g3;
		 b       = s2*fi[k1] - c2*gi[k1];
		 a       = c2*fi[k1] + s2*gi[k1];
		 f1      = fi[0 ]    - a;
		 f0      = fi[0 ]    + a;
		 g1      = gi[0 ]    - b;
		 g0      = gi[0 ]    + b;
		 b       = s2*fi[k3] - c2*gi[k3];
		 a       = c2*fi[k3] + s2*gi[k3];
		 f3      = fi[k2]    - a;
		 f2      = fi[k2]    + a;
		 g3      = gi[k2]    - b;
		 g2      = gi[k2]    + b;
		 b       = s1*f2     - c1*g3;
		 a       = c1*f2     + s1*g3;
		 fi[k2]  = f0        - a;
		 fi[0 ]  = f0        + a;
		 gi[k3]  = g1        - b;
		 gi[k1]  = g1        + b;
		 b       = c1*g2     - s1*f3;
		 a       = s1*g2     + c1*f3;
		 gi[k2]  = g0        - a;
		 gi[0 ]  = g0        + a;
		 fi[k3]  = f1        - b;
		 fi[k1]  = f1        + b;
		 gi     += k4;
		 fi     += k4;
		} while (fi<fn);
        }
     TRIG_RESET(k,c1,s1);
    } while (k4<n);
}
