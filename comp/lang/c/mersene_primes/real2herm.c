static const char RCSreal2herm_c[] = "$Id: real2herm.c,v 6.50 1999/12/19 06:06:20 wedgingt Exp $";

#include "setup.h"
#include "globals.h"
#include "init_fft.h"
#include "real2herm.h"

#ifdef __STDC__
void fft_real_to_hermitian(BIG_DOUBLE *z, UL n)
#else
void fft_real_to_hermitian(z, n)
BIG_DOUBLE *z;
UL n;
#endif
/* Output is {Re(z^[0]),...,Re(z^[n/2),Im(z^[n/2-1]),...,Im(z^[1]).
   This is a decimation-in-time, split-radix algorithm.
 */
{	register long n4;
	register BIG_DOUBLE *x;
	register BIG_DOUBLE cc1, ss1, cc3, ss3;
  register UL i2;
  register long i1, i3, i4, i5, i6, i7, i8,
		     a, a3, dil;
	register BIG_DOUBLE t1, t2, t3, t4, t5, t6;
	BIG_DOUBLE e;
  UL is;
	long nn = n>>1, nminus = n-1, id;
	register long n2, n8, j;
  register UL i;
	
	x = z-1;  /* FORTRAN compatibility. */
	is = 1;
	id = 4;
	do{
	   for(i2=is;i2<=n;i2+=id) {
		i1 = i2+1;
		e = x[i2];
		x[i2] = e + x[i1];
		x[i1] = e - x[i1];
	   }
	   is = (id<<1)-1;
	   id <<= 2;
	} while(is<n);
	n2 = 2;
	while(nn>>=1) {
		n2 <<= 1;
		n4 = n2>>2;
		n8 = n2>>3;
		is = 0;
		id = n2<<1;
		do {
			for(i=is;i<n;i+=id) {
				i1 = i+1;
				i2 = i1 + n4;
				i3 = i2 + n4;
				i4 = i3 + n4;
				t1 = x[i4]+x[i3];
				x[i4] -= x[i3];
				x[i3] = x[i1] - t1;
				x[i1] += t1;
				if(n4==1) continue;
				i1 += n8;
				i2 += n8;
				i3 += n8;
				i4 += n8;
				t1 = (x[i3]+x[i4])*SQRTHALF;
				t2 = (x[i3]-x[i4])*SQRTHALF;
				x[i4] = x[i2] - t1;
				x[i3] = -x[i2] - t1;
				x[i2] = x[i1] - t2;
				x[i1] += t2;
			}
			is = (id<<1) - n2;
			id <<= 2;
		} while(is<n);
		dil = n/n2;
		a = dil;
		for(j=2;j<=n8;j++) {
		    	a3 = (a+(a<<1))&(nminus);
			cc1 = cn[a];
			ss1 = sn[a];
			cc3 = cn[a3];
			ss3 = sn[a3];
			a = (a+dil)&(nminus);
			is = 0;
			id = n2<<1;
		        do {
				for(i=is;i<n;i+=id) {
					i1 = i+j;
					i2 = i1 + n4;
					i3 = i2 + n4;
					i4 = i3 + n4;
					i5 = i + n4 - j + 2;
					i6 = i5 + n4;
					i7 = i6 + n4;
					i8 = i7 + n4;
					t1 = x[i3]*cc1 + x[i7]*ss1;
					t2 = x[i7]*cc1 - x[i3]*ss1;
					t3 = x[i4]*cc3 + x[i8]*ss3;
					t4 = x[i8]*cc3 - x[i4]*ss3;
					t5 = t1 + t3;
					t6 = t2 + t4;
					t3 = t1 - t3;
					t4 = t2 - t4;
					t2 = x[i6] + t6;
					x[i3] = t6 - x[i6];
					x[i8] = t2;
					t2 = x[i2] - t3;
					x[i7] = -x[i2] - t3;
					x[i4] = t2;
					t1 = x[i1] + t5;
					x[i6] = x[i1] - t5;
					x[i1] = t1;
					t1 = x[i5] + t4;
					x[i5] -= t4;
					x[i2] = t1;
				}
			        is = (id<<1) - n2;
				id <<= 2;
			} while(is<n);
		}
	}
}
