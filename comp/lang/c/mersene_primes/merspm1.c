static char RCSmerspm1_c[] = "$Id: merspm1.c,v 6.50 1999/12/19 06:06:20 wedgingt Exp $ by wedgingt@acm.org";
char *RCSprogram_id = RCSmerspm1_c;
const char program_revision[] = "$Revision: 6.50 $";

/* p-1 program for Mersenne primes */
/* Part of the 'mers' package at http://www.garlic.com/~wedgingt/mers.tgz */
/* Maintained by Will Edgington <wedgingt@acm.org> */

/* Originally from:

Tom Womack
thomas.womack@merton.ox.ac.uk
(website at http://users.ox.ac.uk/~mert0236)

*/

#define FACTOR_PACKAGE FACTOR_LIBGMP_INT
#define DO_NOT_USE_LONG_LONG_EXPONENT

#include "setup.h"
#include "gmp.h"
#include "factor.h"
#include "rw.h"
#include <stdio.h>
#include <assert.h>
#include <math.h>

#ifdef macintosh
# include <console.h>
#endif

const char *program_name = NULL;

static int create_primes(UL **storage, EXPONENT limit)
 {
  char *sieve;
  size_t i, j, k, pi = 0, pc = 0;

  if ((sieve = (char*)calloc(limit, 1)) == NULL)
   {
    perror("merspm1: getting sieve memory");
    exit(errno);
   }
  for (i = 4; i < limit; i += 2)
    sieve[i] = 1;
  for (j = 3; j*j < limit; j++)
    if (sieve[j] == 0)
      for (k = j*j; k < limit; k += 2*j)
        sieve[k] = 1;
  for (i = 2; i < limit; i++)
    if (sieve[i] == 0)
      pi++;
  if (*storage != NULL)
    free(*storage);
  if((*storage = (UL *)malloc(4*pi + 4)) == NULL)
   {
    perror("merspm1: getting memory for primes");
    exit(errno);
   }
  for (i = 2; i < limit; i++)
    if (sieve[i] == 0)
      storage[0][pc++] = i;
  free(sieve);
  return(pi);
 }

int main(int argc, char **argv)
 {
  size_t i, j, noofp;
  EXPONENT exponent, *primes = NULL, prior_exponent = 0L, limit = 5000L, prior_limit = 5000L;
  FACTOR N, x, tr, bound, bound2;
  char M; /* 'o', 'C', 'H', etc. */
  int all_factors = 1, small_test = 0, task = 0;
  FILE *infp = NULL, *outfp = NULL, *dupfp = NULL;

#ifdef macintosh
  argc = ccommand(&argv);
#endif
  if (argv == NULL || argv[0] == NULL)
    program_name = "merspm1";
  else
    for (program_name = argv[0] + strlen(argv[0]); program_name > argv[0] && *program_name != '/'; )
      --program_name;
  if (*program_name == '/')
    ++program_name;
  setup();
  fac_init();
  fac_init_var(N);
  fac_init_var(x);
  fac_init_var(tr);
  fac_init_var(bound);
  fac_init_var(bound2);
  noofp = create_primes(&primes, limit);
  /* create the prime powers up to limit */
  for (i = 0; primes[i]*primes[i] < limit; primes[i++] = j)
    for (j = primes[i]*primes[i]; j <= limit/primes[i]; j *= primes[i])
      ;
  while (!terminate &&
         (task = input(argc, argv, &exponent, NULL, NULL, NULL, NULL, (UL)0L, &M, &bound, &bound2,
                       &all_factors, &small_test, &limit, (UL)0, (UL)0, &infp, &outfp, &dupfp, NULL)) > 1)
   {
    if (limit > prior_limit)
     {
      noofp = create_primes(&primes, limit);
      for (i = 0; primes[i]*primes[i] < limit; primes[i++] = j)
        for (j = primes[i]*primes[i]; j <= limit/primes[i]; j *= primes[i])
          ;
      prior_limit = limit;
     }
    if (exponent != prior_exponent)
     {
      mpz_ui_pow_ui(N, 2, exponent);
      mpz_sub_ui(N, N, 1);
      prior_exponent = exponent;
     }
    switch (M)
     {
      case 'C':
        mpz_tdiv_qr(N, tr, N, bound);
        if (mpz_cmp_ui(tr, 0) != 0)
          fprintf(stderr, "warning: non-zero remainder for M(" PRINTF_FMT_UL ")\n", exponent);
        continue;
      case 'o':
        if (mpz_cmp_ui(bound, limit) >= 0 || limit < exponent)
          continue; /* already checked using P-1 to at least this limit or limit not high enough for exp. */
        break;
      default: /* other lines give us no useful info or tell us not to try to factor this exponent */
        continue;
     }
    mpz_set_ui(x, 3);
    /* We require 3 to be a primitive root */
    mpz_powm_ui(x, x, 2*exponent, N);
    /* That accounts for the fact that the factors are 2kp+1 */
    /* Now for the fun part - compute E a very large number (product of the
       primes <= limit) and calculate x^E. */
    for (i = 0; i < noofp; i++)
      mpz_powm_ui(x, x, primes[i], N);
    /* We've now got (x^BIG) % N */
    mpz_sub_ui(x, x, 1);
    mpz_gcd(tr, x, N);
    if (!fac_less_than_int(tr, (UL)2) && fac_less_than(tr, N))
      fac_fprint_winner(outfp, exponent, tr, dupfp);
    fprintf(outfp, "M( " PRINTF_FMT_UL " )o: " PRINTF_FMT_UL "\n", exponent, (UL)limit);
    if (dupfp != NULL)
      fprintf(dupfp, "M( " PRINTF_FMT_UL " )o: " PRINTF_FMT_UL "\n", exponent, (UL)limit);
   }
  return(task == 1);
 }
