/* $Id: faclip.h,v 6.50 1999/12/19 06:06:20 wedgingt Exp $ */
/* This include file should be included if you want to use the */
/* freelip package to perform the Mersenne factoring.  The advantage */
/* of freelip is that it will handle arbitrary sized factors. */

#include "lip.h"
#include "stdio.h"

/*-------------------------------------*/
/* Start of user-definable parameters  */
/*-------------------------------------*/

/* There are no user-definable parameters here.  See lip.h for */
/* information on tuning the freelip package. */

/*-----------------------------------*/
/* End of user-definable parameters  */
/*-----------------------------------*/

/* This data type is used for handling potential factors of */
/* Mersenne numbers.  The size of these factors is limited */
/* only by the math package used for the large integer arithmetic. */
typedef verylong FACTOR;

/* These are the freelip equivalent of the macros used in factor.c */
/* Speed is not important in the first of these macros */

/* This macro returns the largest exponent of two that can be safely tested */
/*  0 means there is no maximum besides memory limits */

#define fac_max_exp_of_2	0

/* This macro performs initialization of the large integer math package. */
/* This macro will be called only once. */

#define fac_init()		(zstart(), savef = NULL, numer = NULL, tmpfac = NULL, tmpfac2 = NULL)
extern FACTOR tmpfac, tmpfac2;

/* This macro does any required initialization per variable */
/*  sort of like a C++ constructor except zero is always assigned */

#define fac_init_var(f)		((f) = NULL)

/* This macro covers the C++ destructor case, usually freeing any */
/*  memory that the variable had malloc'd */

#define fac_dest_var(f)         (zfree(f))

/* This macro sets a factor from another one */

#define fac_set(srcf,destf)	(zcopy((srcf), (destf)))

/* This macro initializes a factor with an integer value */

#if SIZEOF_UL <= 4
# define fac_set_int(i,f)	(zintoz((i), (f)))
#else
# define fac_set_int(i,f)	fac_set_ull((i), (f))
extern void fac_set_ull(UL i, FACTOR *f);
#endif

/* This macro compares a factor to an integer */

#if SIZEOF_UL <= 4
# define fac_less_than_int(f,i)	(zscompare((f), (i)) < 0)
#else
# define fac_less_than_int(f,i)	(fac_less_than_ull((f), (i)))
extern int fac_less_than_ull(FACTOR f, UL i);
#endif

/* This macro compares one factor to another factor */

#define fac_less_than(f1,f2)	(zcompare((f1), (f2)) < 0)

/* This macro divides a factor by an integer.  The result is */
/* the floor(factor/integer) */

#if SIZEOF_UL <= 4
# define fac_div_int(fsrc,i,fdest)	(zsdiv((fsrc), (i), (fdest)))
#else
# define fac_div_int(fsrc,i,fdest)	(fac_div_ull((fsrc), (i), (fdest)))
extern UL fac_div_ull(FACTOR fsrc, UL i, FACTOR *fdest);
#endif

/* This macro multiplies a factor by an integer */

#if SIZEOF_UL <= 4
# define fac_mul_int(fsrc,i,fdest)	(zsmul((fsrc), (i), (fdest)))
#else
# define fac_mul_int(fsrc,i,fdest)	(fac_mul_ull((fsrc), (i), (fdest)))
extern void fac_mul_ull(FACTOR fsrc, UL i, FACTOR *fdest);
#endif

/* This macro adds a factor and an integer */

#if SIZEOF_UL <= 4
# define fac_add_int(fsrc,i,fdest)	(zsadd((fsrc), (i), (fdest)))
#else
# define fac_add_int(fsrc,i,fdest)	(fac_add_ull((fsrc), (i), (fdest)))
extern void fac_add_ull(FACTOR fsrc, UL i, FACTOR *fdest);
#endif

/* This macro adds two factors */

#define fac_add(fsrc1, fsrc2, fdest)    (zadd((fsrc1), (fsrc2), (fdest)))

/* This macro multiplies two factors and is only used in rw.c:input() */
/*!!Note freeLIP's need for a different function since one of the inputs is the output */
/*!! Luckily, that's all that is needed presently */
#define fac_mul(fsrc1, fsrc2, fdest)    (zmulin((fsrc2), (fdest)))

/* This macro divides a factor by another factor and is only used by 1strs8.c */
#define fac_div(fnumer, fdenom, fquot, frem)	(zdiv((fnumer), (fdenom), (fquot), (frem)))

/* This macro subtracts an integer from a factor */

#if SIZEOF_UL <= 4
# define fac_sub_int(fsrc,i,fdest)       (zsadd((fsrc), -(i), (fdest)))
#else
# define fac_sub_int(fsrc,i,fdest)	(fac_sub_ull((fsrc), (i), (fdest)))
extern void fac_sub_ull(FACTOR fsrc, UL i, FACTOR *fdest);
#endif

/* This macro subtracts two factors and is only used in rw.c:input() */

#define fac_sub(fsrc1, fsrc2, fdest)    (zsub((fsrc1), (fsrc2), (fdest)))

/* This macro returns factor modulo an integer */

#if SIZEOF_UL <= 4
# define fac_mod_int(f,i)		(zsmod((f), (i)))
#else
# define fac_mod_int(fsrc,i)		(fac_mod_ull((fsrc), (i)))
extern UL fac_mod_ull(FACTOR fsrc, UL i);
#endif

/* This macro returns the factor modulo 8 */

#define fac_mod8(f)			((f)[0] == 0 ? 0 : (f)[1] & 7)

/* This macro returns the value of the i'th bit (0 for units bit) of the factor f */

#define fac_bit(f, i)			(zbit((f), (i)))

/* This macro squares a factor */

#define fac_square(fsrc,fdest)		\
{verylong tmp = NULL; zsq((fsrc), &tmp); zcopy(tmp, (fdest)); }

/* This macro doubles a factor */

#define fac_times2(fsrc,fdest)		(z2mul((fsrc), (fdest)))

/* This macro alerts the math package that several */
/* successive modulo operations of this numerator will take place */

#define fac_prepare_numerator(f)	(fac_set((f), &numer))


/* The following are the speed-critical macros */


/* This macro alerts the math package that several */
/* successive modulo operations are about to take place */

#define fac_prepare_denominator(f)	(fac_set((f), &savef))

/* This macro takes performs a modulo operation using the */
/* numerator sent to fac_prepare_numerator and the factor */
/* sent to fac_prepare_denominator */

#define fac_mod(fdest)			(zmod(numer, savef, (fdest)))

/* This macro squares a factor then performs a modulo operation */
/* using the factor sent to fac_prepare_denominator */

#define fac_square_mod(fsrc,fdest)	(zsqmod((fsrc), savef, (fdest)))

/* This macro doubles a factor modulo the factor sent to */
/* fac_prepare_denominator.  NOTE: The modulo operation can be skipped */
/* as long as fac_square_mod can handle the larger number. */

#define fac_times2_mod(fsrc,fdest)	(fac_times2((fsrc), (fdest)))

/* This macro returns TRUE if f equals one OR if fac_times2 */
/* did not perform the modulo operation this macro returns */
/* TRUE if f equals saved-denominator + 1 */

#define fac_is_winner(f)		(zsub((f),(savef),&(f)),zscompare((f),1)==0)
