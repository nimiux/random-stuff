#ifndef lint
static const char RCSecm3_c[] = "$Id: ecmfactor.c,v 6.50 1999/12/19 06:06:20 wedgingt Exp $ wedgingt@acm.org";
#endif

/* Try to factor arbitrary natural numbers using freeLIP's ECM routines.
   Based on the mers package's ecm3.c.
*/

#include "setup.h"
#include "lip.h"
#include <time.h>

#ifdef macintosh
# include <console.h>
#endif

#ifndef ECM_MINBOUND
#define ECM_MINBOUND 100
#endif

#ifndef ECM_MAXBOUND
#define ECM_MAXBOUND 5000000
#endif

const char *program_name = NULL;
verylong one = NULL; /* constant after first set */
verylong z = NULL; /* number to factor or remaining cofactor */
char done = 1L; /* two flags; bit 0: done, as in completely factored? */
                           /* bit 1: check (original or new) cofactor's primality? */

#ifdef __STDC__
int main(int argc, char **argv)
#else
int main(argc, argv)
int argc;
char **argv;
#endif
 {
  double grow = 0.63; /* multiply bound by this after trying curves at current bound for all exponents */
    /*  except that values less than 1.0 mean to grow it via work += bound; bound = exp(grow*log(work)); */
  verylong answer = NULL; /* new factor from zfecm() */
  verylong b = NULL; /* factor from input, other uses */
  verylong d = NULL; /* remainder of division of Mersenne by 'known' factor; curve count from old data */
  verylong f = NULL; /* lots of uses */
  unsigned long bound = 1L, bound2 = 1L, curves = 1L; /* from -b and -c on command line */
  unsigned long bound3, n = 2L; /* ECM bound given to zfecm() and ECM iterations per P1 bound per exponent */
  unsigned long limit = 0L; /* maximum bound; if given, only one set of curves at this bound */
  int which = 0; /* index of exponent that is being worked on; borrowed for argv's index before then */
  int ch = '0'; /* for reading a char at a time and scanf() return value */
  int info = 0; /* passed on to zfecm() to determine how much info it prints */
  int zcompos; /* return value from zcomposite() */
  char *outfn = NULL; /* output file name; replaces stdout */
  char print_cofactor = 0; /* flag: print cofactor in decimal when at least pseudo-prime */

#ifdef macintosh
  argc = ccommand(&argv);
#endif
  /* setup */
  if (argv == NULL || argv[0] == NULL)
    program_name = "ecmfactor";
  else
    for (program_name = argv[0] + strlen(argv[0]); program_name > argv[0] && *program_name != '/'; )
      --program_name;
  if (*program_name == '/')
    ++program_name;
  setup();
  zrstarts(time((time_t)NULL));
  setlinebuf(stdout);

  /* parse the command line */
  while (!terminate && argv != NULL && argv[++which] != NULL && argv[which][0] == '-')
    switch (argv[which][1])
     { /*!!create UL get_arg()? in setup.c? or rw.c? */
      case 'b': case 'B':
        if (isascii(argv[which][2]) && isdigit(argv[which][2]))
          bound = atol(argv[which] + 2);
        else
          if (argv[++which] != NULL && isascii(argv[which][0]) && isdigit(argv[which][0]))
            bound = atol(argv[which]);
          else
            bound = 0, terminate = 1;
        break;
      case 'c': case 'C':
        if (isascii(argv[which][2]) && isdigit(argv[which][2]))
          curves = n = atol(argv[which] + 2);
        else
          if (argv[++which] != NULL && isascii(argv[which][0]) && isdigit(argv[which][0]))
            curves = n = atol(argv[which]);
          else
            curves = 0, terminate = 1;
        break;
      case 'd': case 'D':
        if (argv[which][2] == '\0')
          print_cofactor ^= 1;
        else
          terminate = 1;
        break;
      case 'g': case 'G':
        if (isascii(argv[which][2]) && isdigit(argv[which][2]))
          grow = atof(argv[which] + 2);
        else
          if (argv[++which] != NULL && isascii(argv[which][0]) && isdigit(argv[which][0]))
            grow = atof(argv[which]);
          else
            grow = -1.0, terminate = 1;
        break;
      case 'i': case 'I':
        if (isascii(argv[which][2]) && isdigit(argv[which][2]))
          info = atoi(argv[which] + 2);
        else
          if (argv[++which] != NULL && isascii(argv[which][0]) && isdigit(argv[which][0]))
            info = atoi(argv[which]);
          else
            info = -1, terminate = 1;
        break;
      case 'l': case 'L':
        if (isascii(argv[which][2]) && isdigit(argv[which][2]))
          limit = atol(argv[which] + 2);
        else
          if (argv[++which] != NULL && isascii(argv[which][0]) && isdigit(argv[which][0]))
            limit = atol(argv[which]);
          else
            limit = 1, terminate = 1;
        break;
      case 'o': case 'O':
        if (argv[which][2] != '\0')
          outfn = argv[which] + 2;
        else
          if (argv[++which] != NULL && argv[which][0] != '\0')
            outfn = argv[which];
          else
            outfn = NULL, terminate = 1;
        break;
      default: /* help option or unknown option or any arg */
        terminate = 1; /* get out of this loop and into the next if */
        /* fall thru */
     }

  if (terminate || (argv != NULL && argv[which] != NULL))
   {
    if (argv[which][0] != '-' ||
        (argv[which][1] != 'h' && argv[which][1] != 'H' && argv[which][1] != '?'))
      fprintf(stderr, "%s: unknown flag or argument: '%s'\n", argv[0], argv[which]);
    fprintf(stderr, "  Usage: %s [-h|-H|-?] [-b<bound>] [-c<curves>]\n", argv[0]);
    fprintf(stderr, "         [-i<info>] [-l<limit>] [-o<outputfilename>] [-g<grow>]\n");
    fprintf(stderr, "    -b<bound>: use <bound> as the initial stage one bound (default 1000)\n");
    fprintf(stderr, "    -c<curves>: try <curves> per bound (default 1)\n");
    fprintf(stderr, "    -d: toggle: print cofactor in decimal when at least pseudo-prime (default off)\n");
    fprintf(stderr, "    -g<grow>: multiply bound by <grow> after curves done (default 0.0)\n");
    fprintf(stderr, "              (less than 1.0 grow via 'work += bound; bound = exp(grow*log(work));'\n");
    fprintf(stderr, "    -i<0,1,2>: no, some, or all info from freeLIP's zfecm() and main()\n");
    fprintf(stderr, "    -l<limit>: max. bound; only one set of <curves> at <limit> (default none)\n");
    fprintf(stderr, "    -o<outputfilename>: file to write non-error output to (default stdout)\n");
    fprintf(stderr, "    -h, -H, -?: print this usage information and exit\n");
    fprintf(stderr, "  Tries to factor arbitrary natural numbers using freeLIP's zfecm()\n");
    fprintf(stderr, "  Reads from stdin and writes to stdout; future versions will be more flexible\n");
    fprintf(stderr, "  $Id: ecmfactor.c,v 6.50 1999/12/19 06:06:20 wedgingt Exp $\n");
    fprintf(stderr, "  Maintained by <a href=\"mailto:wedgingt@acm.org\">Will Edgington</a>\n");
    fprintf(stderr, "  <a href=\"http://www.garlic.com/~wedgingt/mersenne.html\">Mers</a>\n");
    return(1);
   }

  /* check arguments given */
  if (bound < 1)
    fprintf(stderr, "%s: starting bound (-b) must be positive\n\t(\'ecmfactor -h\' will print help info)\n", argv[0]);
  if (curves < 1)
    fprintf(stderr, "%s: number of curves (-c) must be positive\n\t(\'ecmfactor -h\' will print help info)\n", argv[0]);
  if (info < 0 || info > 2)
    fprintf(stderr, "%s: info value (-i) must be 0, 1, or 2\n\t(\'ecmfactor -h\' will print help info)\n", argv[0]);
  if (limit == 1 || (0 < limit && limit < bound))
    fprintf(stderr, "%s: limit (-l) must be integer >= starting bound\n\t(\'ecmfactor -h\' will print help info)\n", argv[0]);
  if (grow < 0.3)
    fprintf(stderr, "%s: grow value (-g) must be at least 0.3\n\t(\'ecmfactor -h\' will print help info)\n", argv[0]);
  if (bound < 1 || curves < 1 || info < 0 || info > 2 || grow < 0.3 ||
      limit == 1 || (0 < limit && limit < bound))
    return(2);

  /* set restrictions and constants */
  if (bound < ECM_MINBOUND)
    bound = ECM_MINBOUND;
  bound2 = bound;
  if (curves != n)
    curves = 1; /* no -c on cmd line */
  if (curves > 1 << 30)
    curves = 1 << 30;
  if (0 < limit && limit < bound)
    limit = bound;
  if (limit > ECM_MAXBOUND)
    limit = ECM_MAXBOUND;
  if (grow >= 1.0 && grow > limit/(double)ECM_MINBOUND && limit > ECM_MINBOUND)
    grow = limit/(double)ECM_MINBOUND;
  zintoz(1, &one);

  if (outfn != NULL && freopen(outfn, "a", stdout) == NULL)
   {
    fprintf(stderr, "%s: could not open output file '%s' for appending\n", argv[0], outfn);
    perror(outfn);
    return(errno);
   }
  /* try to prevent data from being lost in the stdio buffers during an interrupt or machine crash */
  fflush(stdout);
  setbuf(stdout, NULL);
  fflush(stderr);
  setbuf(stderr, NULL);

  while ((!terminate && !feof(stdin)) || done != 1)
   { /* first, if we completed a factorization or just started running, read a number to try to factor */ 
    if (done == 1)
     { /* this is a mess because some scanf's _require_ whitespace if a blank starts the format string */
      for (ch = fgetc(stdin); isascii(ch) && isspace(ch); ch = fgetc(stdin))
        ;
      if (!isascii(ch) || !isdigit(ch))
       {
        if (ch != EOF)
          fprintf(stderr, "%s: only natural numbers are valid input\n", program_name);
        while (ch != EOF && ch != '\n')
          ch = fgetc(stdin);
        continue;
       }
      /* !!Do NOT use zread() */
      for (zzero(&z); (isascii(ch) && isdigit(ch)) || ch == '\\'; ch = fgetc(stdin))
       {
        if (ch == '\\')
          if ((ch = fgetc(stdin)) == '\n')
           {
            ch = fgetc(stdin);
            continue;
           }
          else
           {
            ungetc(ch, stdin);
            ch = '\\';
            break;
           }
        zsmul(z, 10, &z);
        zsadd(z, ch - '0', &z);
       }
      while (ch != EOF && ch != '\n')
        ch = fgetc(stdin); /* skip the rest of the line */
      zzero(&b);
      if (zcompare(z, b) > 0)
       {
        printf("Attempting to factor:\n");
        zwriteln(z);
        done = 2;
        if (z2mod(z) == 0)
         {
          fputs("factor: 2", stdout);
          for (bound = 0; z2mod(z) == 0; bound++)
            zrshift(z, 1, &z);
          if (bound > 1)
            printf("^%lu", bound);
          fputc('\n', stdout);
         }
        bound = bound2; /* reset to -b value */
       }
     } /* if done == 1 */

    while (!terminate && done == 0)
     { /* try to find a new factor */
      n = curves;
      bound3 = bound;
      switch ((int)zfecm(z, &answer, 0, &n, &bound3, 0.0, 0, info, stdout))
       {
        case 0: /* zfecm() also returns 0 for problems(!) */
          printf("tried %lu curves at bound %lu without success\n", curves, bound);
          if (limit > 0 && bound >= limit)
           {
            done = 1;
            continue;
           }
          if (limit > 0 && bound >= limit)
            bound = limit;
          else
            if (bound >= ECM_MAXBOUND)
              bound = ECM_MAXBOUND;
            else
              if (grow >= 1.0)
                bound = grow*(double)bound + 1;
              else
                bound = exp(grow*log(bound + exp(log((BIG_DOUBLE)bound)/grow))) + 1;
          continue;
        case -1:
          printf("zfecm() claims remaining cofactor is prime\n");
          done = 1;
          continue;
        case 1:
          if (info > 0)
            printf("new factor: ");
          break;
        case 2:
          if (info > 0)
            printf("new, probably composite, factor: ");
          break;
       }
      zdiv(z, answer, &d, &f);
      if (!ziszero(f)) /* should never happen */
        printf("Warning: non-zero remainder for new factor\n");
      if (zcompare(answer, d) > 0)
        zcopy(answer, &z), /* z gets new cofactor and ... */
          zcopy(d, &answer); /* answer gets new factor */
      else
        zcopy(d, &z);
      printf("factor: ");
      zwriteln(answer);
      zcopy(answer, &b); /* b also gets new factor */
      done = 2;
     } /* while (!terminate && done == 0) */

    while (done == 2)
     { /* check primality of new factor or of cofactor */
      n = curves;
      bound3 = bound;
      if (ziszero(b))
        zcopy(z, &b); /* check cofactor's primality, but don't try to factor it here */
      zcopy(b, &f);
      zcompos = zcomposite(&b, -5, 3);
      while (zcompos == 1 && zcompare(z, b) != 0)
       { /* the new factor is composite; try to factor it */
        if (info > 0)
          printf("factoring new factor for %lu curves at bound %lu\n", n, bound3);
        switch ((int)zfecm(b, &d, 0, &n, &bound3, 0.0, 0, info, stdout))
         {
          case 0:
            if (info > 0)
             {
              printf("ECM %lu curves at %lu failed to factor new\ncomposite factor: ", curves, bound);
              zwriteln(answer);
             }
            if (++n > 2*curves && n > 10) /* probably overkill */
             {
              printf("Warning: giving up trying to factor composite factor after %lu curves at %lu\n",
                     --n, bound3);
              zintoz(2L, &b);
             }
            else
             {
              if (grow >= 1.0)
                bound3 = grow*(double)bound3 + 1;
              else
                bound3 = exp(grow*log(bound3 + exp(log((BIG_DOUBLE)bound3)/grow))) + 1;
              if (bound3 > ECM_MAXBOUND)
                bound3 = ECM_MAXBOUND;
             }
            continue;
          case -1:
            printf("new composite factor now reported as prime by zfecm(): ");
            zwriteln(answer);
            zcompos = 0; /* prime or pseudo-prime */
            continue;
          case 1:
            if (info > 0)
              printf("new factor of factor:\n");
            break;
          case 2:
            printf("Warning: next line is two factors of 'two' factors(?!)\n");
            break;
         }
        zdiv(b, d, &answer, &f);
        if (!ziszero(f)) /* should never happen */
          printf("Warning: non-zero remainder for factor of factor\n");
        if (zcompare(answer, d) > 0)
          zcopy(d, &f);
        else
          zcopy(answer, &f),
            zcopy(d, &answer);
        printf("factor of factor: ");
        zwriteln(f); /* smaller factor of factor */
        zcopy(answer, &b);
        zcopy(b, &f);
        zcompos = zcomposite(&b, -5, 3);
       } /* while new factor, b, is composite */
      switch (zcompos)
       {
        case 1: /* composite cofactor (all composite factors get factored just above) */
          printf("new cofactor is composite\n");
          done = 0;
          continue;
        case 0: /* new factor or the cofactor is prime or pseudo prime in base 3 and four other bases */
          if (zcompare(z, b) == 0)
           {
            printf("cofactor is prime or a strong pseudo-prime\n");
            if (print_cofactor)
             {
              printf("cofactor: ");
              zwriteln(b);
             }
            done = 1; /* factorization now thought to be complete */
            continue;
           }
          printf("prime factor: ");
          zwriteln(b);
          zzero(&b);
          continue; /* check primality of the new cofactor */
        case -1: /* composite, and new value of b may give new factor of old b, still in f */
        case -2: /* composite, and new value of b is a factor of old b, still in f */
          zdiv(f, b, &answer, &d);
          if (zcompos == -1 && !ziszero(d))
           { /*!!skip for now, but print what we know in case it happens */
            printf("Please send this section (below) to wedgingt@acm.org; it may lead to a new factor\n");
            printf("in ecmfactor, zcomposite(&b, -5, 3) returned -1\n");
            printf(" z is ");
            zwriteln(z);
            printf(" old b is ");
            zwriteln(f);
            printf(" new b is ");
            zwriteln(b);
	    printf("%s\n", "$Id: ecmfactor.c,v 6.50 1999/12/19 06:06:20 wedgingt Exp $");
            printf("Please send this section (above) to wedgingt@acm.org\n");
            done = 1; /*!!pretend we've completed it just in case */
            continue;
           }
          if (!ziszero(d))
            printf("Warning: non-zero remainder via zcomposite() return of -1 or -2\n");
          if (zcompare(b, answer) > 0)
            zcopy(answer, &d),
              zcopy(b, &answer),
              zcopy(d, &b);
          if (zcompare(z, f) == 0) /* b is factor of (now old) cofactor */
           {
            zcopy(answer, &z);
            continue; /* check primality of new factor */
           }
          printf("factor of factor: ");
          zwriteln(b);
          zcopy(answer, &b);
          continue; /* check primality of b, cofactor of factor */
       } /* switch (zcompos) */
     } /* while (done == 2) */
   } /* for ((!terminate && !EOF) || done != 1) */
  return(0);
 }
