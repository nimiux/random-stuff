#define DRCSinit_real_h "$Id: init_real.h,v 6.50 1999/12/19 06:06:20 wedgingt Exp $"

#ifdef __STDC__
extern void init_scramble_real(UL n);
#else
extern void init_scramble_real();
#endif
