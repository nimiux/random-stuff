static const char RCSherm2real_c[] = "$Id: herm2real.c,v 6.50 1999/12/19 06:06:20 wedgingt Exp $";

#include "setup.h"
#include "globals.h"
#include "herm2real.h"
#include "init_fft.h"

#ifdef __STDC__
void fftinv_hermitian_to_real(BIG_DOUBLE *z, UL n)
#else
void fftinv_hermitian_to_real(z, n)
BIG_DOUBLE *z;
UL n;
#endif
/* Input is {Re(z^[0]),...,Re(z^[n/2),Im(z^[n/2-1]),...,Im(z^[1]).
   This is a decimation-in-frequency, split-radix algorithm.
 */
{	
	register long n4;
	register BIG_DOUBLE cc1, ss1, cc3, ss3;
	register BIG_DOUBLE t1, t2, t3, t4, t5;
	register BIG_DOUBLE *x;
	register long n8, i1, i3, i4, i5, i6, i7, i8,
		 a, a3, dil;
  register unsigned long i2;
	BIG_DOUBLE e;
	long nn = n>>1,id;
	long n2, j;
  unsigned long i,is,nminus = n-1;  

	x = z-1;
	n2 = n<<1;
	while ((nn >>= 1) != 0) {
		is = 0;
		id = n2;
		n2 >>= 1;
		n4 = n2>>2;
		n8 = n4>>1;
		do {
			for(i=is;i<n;i+=id) {
				i1 = i+1;
				i2 = i1 + n4;
				i3 = i2 + n4;
				i4 = i3 + n4;
				t1 = x[i1] - x[i3];
				x[i1] += x[i3];
				x[i2] += x[i2];
				x[i3] = t1 - x[i4] - x[i4];
				x[i4] = t1 + x[i4] + x[i4];
				if(n4==1) continue;
				i1 += n8;
				i2 += n8;
				i3 += n8;
				i4 += n8;
				t1 = x[i2]-x[i1];
				t2 = x[i4]+x[i3];
				x[i1] += x[i2];
				x[i2] = x[i4]-x[i3];
				x[i3] = -SQRT2*(t2+t1);
				x[i4] = SQRT2*(t1-t2);
			}
			is = (id<<1) - n2;
			id <<= 2;
		} while(is<nminus);
		dil = n/n2;
		a = dil;
		for(j=2;j<=n8;j++) {
			a3 = (a+(a<<1))&(nminus);
			cc1 = cn[a];
			ss1 = sn[a];
			cc3 = cn[a3];
			ss3 = sn[a3];
			a = (a+dil)&(nminus);
			is = 0;
			id = n2<<1;
			do {
			   for(i=is;i<n;i+=id) {
				i1 = i+j;
				i2 = i1+n4;
				i3 = i2+n4;
				i4 = i3+n4;
				i5 = i+n4-j+2;
				i6 = i5+n4;
				i7 = i6+n4;
				i8 = i7+n4;
				t1 = x[i1] - x[i6];
				x[i1] += x[i6];
				t2 = x[i5] - x[i2];
				x[i5] += x[i2];
				t3 = x[i8] + x[i3];
				x[i6] = x[i8] - x[i3];
				t4 = x[i4] + x[i7];
				x[i2] = x[i4] - x[i7];
				t5 = t1 - t4;
				t1 += t4;
				t4 = t2 - t3;
				t2 += t3;
				x[i3] = t5*cc1 + t4*ss1;
				x[i7] = -t4*cc1 + t5*ss1;
				x[i4] = t1*cc3 - t2*ss3;
				x[i8] = t2*cc3 + t1*ss3;
			   }
			   is = (id<<1) - n2;
			   id <<= 2;
			} while(is<nminus);
		}
	}
	is = 1;
	id = 4;
	do {
	  for(i2=is;i2<=n;i2+=id){
		i1 = i2+1;
		e = x[i2];
		x[i2] = e + x[i1];
		x[i1] = e - x[i1];
	  }
	  is = (id<<1) - 1;
	  id <<= 2;
	} while(is<n);
	e = 1/(BIG_DOUBLE)n;
	for(i=0;i<n;i++) z[i] *= e;				
}
