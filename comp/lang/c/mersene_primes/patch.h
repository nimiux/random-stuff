#define DRCSpatch_h "$Id: patch.h,v 6.50 1999/12/19 06:06:20 wedgingt Exp $"

#ifdef __STDC__
extern void patch(BIG_DOUBLE *x, UL n, UL b, UL c);
#else
extern void patch();
#endif
