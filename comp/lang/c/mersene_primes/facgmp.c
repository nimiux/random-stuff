static const char RCS_facgmp_c[] = "$Id: facgmp.c,v 6.50 1999/12/19 06:06:20 wedgingt Exp $";

#define FACTOR_PACKAGE FACTOR_LIBGMP_INT

/* too bad libgmp.a wimped out and had all these return void */

#include "setup.h"
#include "factor.h"

FACTOR tmpfac, tmpfac2; /* temporaries for use in several functions here */

#ifdef __STDC__
int fscan_fac(FILE *fp, FACTOR *f)
#else
int fscan_fac(fp, f)
FILE *fp;
FACTOR *f;
#endif
 { /* note the dereference of f; libgmp's type mpz_t is already a pointer */
  mpz_inp_str(*f, fp, 10);
  return(!ferror(fp) && !feof(fp));
 }

#ifdef __STDC__
int sscan_fac(char *str, FACTOR *f)
#else
int sscan_fac(str, f)
char *str;
FACTOR *f;
#endif
 { /* note the dereference of f; libgmp's type mpz_t is already a pointer */
  while (*str != '\0' && isascii(*str) && isspace(*str))
    str++;
  if (*str == '\0')
    return(EOF);
  if (!isascii(*str) || !isdigit(*str))
    return(0);
  mpz_set_str(*f, str, 0);
  return(1);
 }

#ifdef __STDC__
int fprint_fac(FILE *fp, FACTOR f)
#else
int fprint_fac(fp, f)
FILE *fp;
FACTOR f;
#endif
 {
  mpz_out_str(fp, 10, f); /* ten is base ten; i.e., decimal */
  return(!ferror(fp)); /*!!should be count of chars to match fprintf(3) */
 }

/* this #if covers the rest of the file, which often presumes BITS_IN_LONG == 64 */
#if SIZEOF_UL > 4

# ifdef __STDC__
void fac_set_ull(UL i, FACTOR *f)
# else
void fac_set_ull(i, f)
UL i;
FACTOR *f;
# endif
 { /* 0x7FFFFFFF is used instead of 0xFFFFFFFF to avoid possible problems with sign extension */
  if ((i & (UL)0x7FFFFFFF) == i)
   {
    mpz_set_ui(*f, (unsigned long)i);
    return;
   }
  /*!!this will work for up to 92 bits */
  mpz_set_ui(*f, (unsigned long)(i >> 60));
  mpz_mul_ui(*f, *f, 1L << 30);
  mpz_add_ui(*f, *f, (unsigned long)((i >> 30) & 0x3FFFFFFF));
  mpz_mul_ui(*f, *f, 1L << 30);
  mpz_add_ui(*f, *f, (unsigned long)(i & 0x3FFFFFFF));
 }

# ifdef __STDC__
int fac_less_than_ull(FACTOR f, UL i)
# else
int fac_less_than_ull(f, i)
FACTOR f;
UL i;
# endif
 {
  if ((i & (UL)0x7FFFFFFF) == i)
    return(mpz_cmp_ui(f, (unsigned long)i) < 0);
  fac_set_ull(i, &tmpfac);
  return(mpz_cmp(f, tmpfac) < 0);
 }

# ifdef __STDC__
void fac_div_ull(FACTOR fsrc, UL i, FACTOR *fdest)
# else
void fac_div_ull(fsrc, i, fdest)
FACTOR *fdest, fsrc;
UL i;
# endif
 {
  if ((i & (UL)0x7FFFFFFF) == i)
   {
    mpz_tdiv_q_ui(*fdest, fsrc, (unsigned long)i);
    return;
   }
  fac_set_ull(i, &tmpfac);
  mpz_tdiv_q(*fdest, fsrc, tmpfac);
 }

# ifdef __STDC__
void fac_mul_ull(FACTOR fsrc, UL i, FACTOR *fdest)
# else
void fac_mul_ull(fsrc, i, fdest)
FACTOR *fdest, fsrc;
UL i;
# endif
 {
  if ((i & (UL)0x7FFFFFFF) == i)
   {
    mpz_mul_ui(*fdest, fsrc, (unsigned long)i);
    return;
   }
  fac_set_ull(i, &tmpfac);
  mpz_mul(*fdest, fsrc, tmpfac);
 }

# ifdef __STDC__
void fac_add_ull(FACTOR fsrc, UL i, FACTOR *fdest)
# else
void fac_add_ull(fsrc, i, fdest)
FACTOR *fdest, fsrc;
UL i;
# endif
 {
  if ((i & (UL)0x7FFFFFFF) == i)
   {
    mpz_add_ui(*fdest, fsrc, (unsigned long)i);
    return;
   }
  fac_set_ull(i, &tmpfac);
  mpz_add(*fdest, fsrc, tmpfac);
 }

# ifdef __STDC__
void fac_sub_ull(FACTOR fsrc, UL i, FACTOR *fdest)
# else
void fac_sub_ull(fsrc, i, fdest)
FACTOR *fdest, fsrc;
UL i;
# endif
 {
  if ((i & (UL)0x7FFFFFFF) == i)
   {
    mpz_sub_ui(*fdest, fsrc, (unsigned long)i);
    return;
   }
  fac_set_ull(i, &tmpfac);
  mpz_sub(*fdest, fsrc, tmpfac);
 }

# ifdef __STDC__
UL fac_mod_ull(FACTOR f, UL i)
# else
UL fac_mod_ull(f, i)
FACTOR f;
UL i;
# endif
 {
  if ((i & (UL)0x7FFFFFFF) == i)
    return(mpz_fdiv_ui(f, (unsigned long)i));
  fac_set_ull(i, &tmpfac);
  mpz_fdiv_r(tmpfac, f, tmpfac);
  i = mpz_fdiv_qr_ui(tmpfac2, tmpfac, tmpfac, 1L << 30);
  i += ((UL)(mpz_fdiv_qr_ui(tmpfac2, tmpfac, tmpfac2, 1L << 30))) << 30;
  i += ((UL)(mpz_fdiv_qr_ui(tmpfac2, tmpfac, tmpfac2, 1L << 30))) << 60;
  return(i);
 }

#endif /* #if BITS_IN_LONG > 32 */
