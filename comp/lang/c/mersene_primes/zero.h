#define DRCSzero_h "$Id: zero.h,v 6.50 1999/12/19 06:06:20 wedgingt Exp $"

#ifdef __STDC__
extern int is_zero(BIG_DOUBLE *x, UL n, UL mask, UL shift);
#else
extern int is_zero();
#endif
