#define DRCSbalance_h "$Id: balance.h,v 6.50 1999/12/19 06:06:20 wedgingt Exp $"

extern void balancedtostdrep PROTO((BIG_DOUBLE *x, UL n, UL b, UL c, BIG_DOUBLE hi, BIG_DOUBLE lo,
                                    UL mask, UL shift));
extern void check_balanced PROTO((BIG_DOUBLE *x, UL n, UL b, UL c, BIG_DOUBLE hi, BIG_DOUBLE lo,
                                  UL mask, UL shift));
extern UL is_big PROTO((UL j, UL big, UL small, UL n));
