static const char RCSMacLucasUNIX_c[] = "$Id: MacLucasUNIX.c,v 6.50 1999/12/19 06:06:20 wedgingt Exp $ wedgingt@acm.org";
const char *RCSprogram_id = RCSMacLucasUNIX_c;
#ifdef macintosh
const char *program_name = "MacLucasFFT"; /* for perror() and similar */
#else
const char *program_name = "MacLucasUNIX"; /* for perror() and similar */
#endif
const char program_revision[] = "$Revision: 6.50 $";
char version[sizeof(RCSMacLucasUNIX_c) + sizeof(program_revision)]; /* overly long, but certain */

/*

The following comments are John Sweeney's original ones, as is the
rest of the program except for my small additions to print the version
with every line of output, Eric Prestemon's changes to UNIX methods
for the timings, and my calls to other *.c files of the mers package.
The #ifdef's and #ifndef's on macintosh are mine as well.  'macintosh'
was chosen because it is pre-#define'd by at least the Metrowerks
C/C++ compilers (according to private email to me).

			Will Edgington, wedgingt@acm.org
			http://www.garlic.com/~wedgingt/mersenne.html

This file contains the basic elements of a Crandall/Fagin Lucas Lehmer
test.  It was written and optimized for a PowerPC 604 prcessor.  All of
the code is written in standard C (or someting pretty close to it).

Some of the architectural features that the code was written to take
advantage of are :

o	A large number of General Purpose (Integer) Registers - PowerPC has 32

o	A large number of Floating Point Registers - PowerPC has 32

o	A superscalar architecture that has multiple independent execution units
	(the PowerPC 604 has 5 - 3 integer, 1 Load/Store, and 1 Floating Point)

o	A large Level 1 (on chip) Cache - The PowerPC 604 has seperate 16Kbtye
	Data and Instruction Caches

Most other RISC microprocessors have similar Architectural features.

One final needed feature is a good Optimizing Compiler that can handle the
instruction scheduling tasks to take maximum advantage of the superscalar
features of your favorite processor.

Very little of the Algorithm contained by this code is my own.  I have tried
to capture and implement the ideas developed by George Woltman in his Prime95
program.  I have made implementation changes here and there, but the heart of
the work clearly belongs to George.

George has, in turn, designed an extremely hardware efficient implementation
of a Lucas Lehmer test algorithm developed by Richard Crandall.

As you examine this code, you will find many similarities to Richard's
original program "Lucas.c"  I began with that code as my starting point and
tried to modify it to take advantage of George's ideas.

For additional insight into George's Algorithm/methods see the file titled
"Wisdom"  It describes much of the "Why" for the "How" shown here!!

Excerpt from a message from George Woltman.....

"A Crandall/Fagin LL tester consists of these tests:

1. Multiply every FFT element by a "magic number".
2. Perform an FFT.
3. Do a complex squaring of each FFT element.
4. Perform an inverse FFT.
5. Divide every FFT element by the "magic number" and divide by N.
6. Round every FFT element to an integer.
7. Propagate carries.
8. Subtract 2 from first FFT element. [this is now done in (last_)normalize() -wedgingt, Feb. 1998]
9. Loop.

If you're clever enough - you can do steps 5,6,7,8 in the same loop. You can
even do step 1 of the next iteration in the same loop. My code spends about
15-20% of its time in steps 1,5,6,7,8.

Also note that step 6 is where you can optionally check for maximum round-off
error. If you're seeing fractional parts of .001 and .999 you're in good shape.
If you're seeing fractional parts of .400 or .600, then you are dangerously close
to having a round-off error (that's where the correct result is 3.0, the FFT
returns 3.6 and you round it to the incorrect 4.0)."

End of excerpt.......

Here is a list of the different routines used (roughly in the order used),
and what they do :

--- Initialization routines ---

These routines are not speed critical,
since they are called once for each exponent tested.

void init_lucas() -	Calculates the Crandall/Fagin "magic numbers" and
			other constants used in the "normalize" routine

void init_fft() -	Calculates the "Twiddle" factors used in the FFT

	UL Reverse() -	Used in init_fft() to appropriately scramble the order
				of the "Twiddle" factors calculated.

--- Control Routines ---

These routines are rather stubby & uninteresting, but then again
they do control the heart of the work

BIG_DOUBLE lucas_square() -	This routine does two simple things.  It calls squareg()
			which performs steps 2,3,4 from the description above, and
			then calls one of the normalize() routines to perform
			steps 5,6,7,and 1 (step 8 is done in Main()).

	void squareg() -	This routine calls the routines that make up the majority
				of the work performed.  It calls the routines to take the
				input data perform a two pass FFT square the result and then
				perform the two pass inverse FFT

--- FFT Routines ---

This is where most of the fun is!  Here are the routines to take an input
of an array of real data - perform an in place FFT-Square-IFFT - to produce
an output of a real array.  It is mind bending & boggling, and I can't tell
you how many times I got lost trying to follow the data through this
process.

Pass 1

void RealFFTR4() - 	This routine takes an all real input array and performs
			the first 'n' levels of the FFT (first 7 levels for
			MacLucas)  The number of levels performed can be
			customized to take advantage of the particular size and
			structure of the target processor's L1 data cache. In
			general 2^n floating point values should fit into 1/2 of
			the L1 data cache.  This is a Radix-4 based FFT and
			needs at least 16 floating point registers to work best
			(RealFFT() is a Radix-2 based routine that only needs
			8 floating point registers).  The output of this routine
			is a mixed real/complex array that occupies the same
			space as the original input data.  Even though most of
			the data has been converted from real to complex no
			additional storage space is used.  no information is lost,
			however, thanks to the inherent symmetry of the FFT.

Pass 2

void RealFFT()/void RealFFTInv() -	In Pass 2 these routines are used to
					complete the last 'm' levels of the FFT on the remaining
					real data elements from Pass 1.  After RealFFT() is called
					in this pass there are 2 real values left and the rest are
					complex.  All of the values (real and complex) are
					squared.  Then RealFFTInv() is called to perform the
					first 'm' levels of the Inverse FFT.

void FFT_IFFT()
void FFT_IFFTR4()
void FFT_IFFTR8() -	All of these three routines perform the last 'm' levels of
			the FFT on the complex data output in Pass 1.  After
			completeing the FFT, the complex elements are squared and
			the first 'm' levels of the Inverese FFT are performed.
			FFT_IFFT() is a Radix-2 based routine that needs 8 floating
			point registers. FFT_IFFTR4() is a Radix-4 based routine that
			needs 16 floating point registers. FFT_IFFTR8() is a Radix-8
			based routine that needs 32 floating point registers. Given
			enough registers Radix-8 routine is significantly faster than
			the Radix-4 or Radix-2 based routines.

Pass 3

void RealFFTInvR4() -	This routine takes the complex data output from pass 2 and
			performs the last 'n' levels of the Inverse FFT to product
			an all real finished array of data.


--- Rounding, Propagating, and Error Checking ---

These routines perform the normalization operations described as steps 5,6,7,
and 1 in George's LL test description above.  They can also perform error
checking to look for round-off errors to determine if the array size used
is appropriate for the exponent being tested (or to look for other misc.
hardware problems corrupting data).  The difference between  normalize() and
last_normalize() is simply that last_normalize() does not perform step 1
for the next iteration.  As might be expected, last_normalize() is called
only on the last iteration in a Lucas-Lehmer test.  These routines make
excellent use of the superscalar nature of modern processors.  There are
almost always at least 3 things happening at any given time.  These routines
also take advantage of an old "Floating Point Trick" to round fractional
values to whole values much more quickly than can be done using a call
to a floor() routine.  All in all, I think that these are the most "clever"
of the routines.


BIG_DOUBLE normalize()
BIG_DOUBLE last_normalize()

*/

/*  Header Information  */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>

/* the natural logarithm of two */
#define LN2 (0.693147180559945309417232121458176568075500134360255254120680009493393621969694715605863326996418687542001481020570685733)

#ifdef macintosh
# include <console.h>
# include <Types.h>
# include <Memory.h>
#endif

#include "setup.h"
#include "balance.h"
#include "rw.h"
#include "zero.h"

#define CACHE_LEVELS 7
#define CACHE_DEPTH 4

#define SHIFT 7
#define BLOCKSIZE 512
#define UPDATE 64
#define MASK (~(UL)(BLOCKSIZE - 1))

#define kErrLimit 0.35
#define kErrChkFreq 100
#define kErrChk 1
#define kLast 2

typedef struct
 {
  BIG_DOUBLE re, im;
 } complex;

BIG_DOUBLE Gbig = 0.0, Gsmall = 0.0;
BIG_DOUBLE Hbig = 0.0, Hsmall = 0.0;

UL Reverse PROTO((UL i, UL n));

void RealFFT PROTO((BIG_DOUBLE *x, long n, long nn, long currentLevel, long levelsToDo,
                    long startElement, long skip, long depth, complex *expn));

void RealFFTInv PROTO((BIG_DOUBLE *x, long n, long nn, long currentLevel, long levelsToDo,
                       long startElement, long skip, long depth, complex *expn));

void RealFFTR4 PROTO((BIG_DOUBLE *x, long n, long nn, long currentLevel, long levelsToDo,
                      long startElement, long skip, long depth, complex *expn));

void RealFFTInvR4 PROTO((BIG_DOUBLE *x, long n, long nn, long currentLevel, long levelsToDo,
                         long startElement, long skip, long depth, complex *expn));

void FFT_IFFT PROTO((BIG_DOUBLE *x, long n, long nn, long currentLevel, long levelsToDo,
                     long startElement, long skip, complex *expn));

void FFT_IFFTR4 PROTO((BIG_DOUBLE *x, long n, long nn, long currentLevel, long levelsToDo,
                       long startElement, long skip, complex *expn));

void FFT_IFFTR8 PROTO((BIG_DOUBLE *x, long n, long nn, long currentLevel, long levelsToDo,
                       long startElement, long skip, complex *expn));

void init_fft PROTO((UL n, complex **expn));

void squareg PROTO((BIG_DOUBLE *x, UL size, UL nn, complex *expn));

void init_lucas PROTO((UL q, UL N, UL *b, UL *c, BIG_DOUBLE *two_to_phi, BIG_DOUBLE *two_to_minusphi,
                       BIG_DOUBLE *low, BIG_DOUBLE *high, BIG_DOUBLE *lowinv, BIG_DOUBLE *highinv));

BIG_DOUBLE lucas_square PROTO((BIG_DOUBLE *x, UL N, UL nn, UL b, UL c, BIG_DOUBLE high,
                               BIG_DOUBLE highinv, BIG_DOUBLE low, BIG_DOUBLE lowinv,
                               BIG_DOUBLE TheBigA, BIG_DOUBLE TheBigB, BIG_DOUBLE *two_to_phi,
                               BIG_DOUBLE *two_to_minusphi, complex *expn, UL last));

BIG_DOUBLE normalize PROTO((BIG_DOUBLE *x, UL N, UL b, UL c, BIG_DOUBLE hi, BIG_DOUBLE hiinv,
                            BIG_DOUBLE lo, BIG_DOUBLE loinv, BIG_DOUBLE TheBigA, BIG_DOUBLE TheBigB,
                            BIG_DOUBLE *two_to_minusphi, BIG_DOUBLE *two_to_phi, UL err_flag));

BIG_DOUBLE last_normalize PROTO((BIG_DOUBLE *x, UL N, UL b, UL c, BIG_DOUBLE hi, BIG_DOUBLE hiinv,
                                 BIG_DOUBLE lo, BIG_DOUBLE loinv, BIG_DOUBLE TheBigA, BIG_DOUBLE TheBigB,
                                 BIG_DOUBLE *two_to_minusphi, BIG_DOUBLE *two_to_phi, UL err_flag));

/*--- Initialization routines ---*/

#ifdef __STDC__
void init_lucas(UL q, UL N, UL *b, UL *c, BIG_DOUBLE *two_to_phi, BIG_DOUBLE *two_to_minusphi,
                BIG_DOUBLE *low, BIG_DOUBLE *high, BIG_DOUBLE *lowinv, BIG_DOUBLE *highinv)
#else
void init_lucas(q, N, b, c, two_to_phi, two_to_minusphi, low, high, lowinv, highinv)
UL q, N, *b, *c;
BIG_DOUBLE *two_to_phi, *two_to_minusphi, *low, *high, *lowinv, *highinv;
#endif
 {
  UL a, j, k, done;
  UL qn = q & (N - 1);
  UL size = N;

  size += (size & MASK) >> SHIFT;
  *low = ldexp(1.0, (int)(q/N));
  *high = *low + *low;
  *lowinv = 1.0/(*low);
  *highinv = 1.0/(*high);
  *b = q & (N - 1);
  *c = N - *b;
  two_to_phi[0] = 1.0;
  two_to_minusphi[0] = 1.0/(BIG_DOUBLE)N;
  for(j = 1; j < N; ++j)
   {
    k = j;
    k += (k & MASK) >> SHIFT;
    a = N - ((j*qn) & (N - 1));
    two_to_phi[k] = exp(a*LN2/N);
    two_to_minusphi[k] = 1/(BIG_DOUBLE)N * 1.0/two_to_phi[k];
   }
  Gbig = two_to_minusphi[1]/two_to_minusphi[0];
  Hbig = two_to_phi[1];
  done = 0;
  j = 0;
  while (!done)
   {
    if (!is_big(j, *b, *c, N))
     {
      k = j + ((j & MASK) >> SHIFT);
      a = j + 1 + (((j + 1) & MASK) >> SHIFT);
      Gsmall = two_to_minusphi[a]/two_to_minusphi[k];
      Hsmall = two_to_phi[a]/two_to_phi[k];
      done = 1;
     }
    j++;
   }
 }

#ifdef __STDC__
UL Reverse(UL i, UL n)
#else
UL Reverse(i, n)
UL i, n;
#endif
 {
  UL tmp = 0;

  while (n >>= 1)
   {
    tmp <<= 1;
    if (i & 1)
      tmp += 1;
    i >>= 1;
   }
  return(tmp);
 }

#ifdef __STDC__
void init_fft(UL n, complex **expn)
#else
void init_fft(n, expn)
UL n;
complex **expn;
#endif
 {
  UL j, k, l, size;
  register BIG_DOUBLE a = 0, inc = TWOPI/n;
  complex *ex;

  size = n;
  size += (size & MASK) >> SHIFT;
  ex = *expn;
  for (j = 0; j < n; j++)
   {
    k = Reverse(j, n);
    a = k*inc;
    l = j;
    l += (l & MASK) >> SHIFT;
    ex[l].re = cos(a);
    ex[l].im = -sin(a);
   }
 }

/*--- Control Routines ---*/

#ifdef __STDC__
BIG_DOUBLE lucas_square(BIG_DOUBLE *x, UL N, UL nn, UL b, UL c, BIG_DOUBLE high, BIG_DOUBLE highinv,
                        BIG_DOUBLE low, BIG_DOUBLE lowinv, BIG_DOUBLE TheBigA, BIG_DOUBLE TheBigB,
                        BIG_DOUBLE *two_to_phi, BIG_DOUBLE *two_to_minusphi, complex *expn, UL flag)
#else
BIG_DOUBLE lucas_square(x, N, nn, b, c, high, highinv, low, lowinv, TheBigA, TheBigB, two_to_phi,
                        two_to_minusphi, expn, flag)
BIG_DOUBLE *x;
UL N, nn, b, c;
BIG_DOUBLE high, highinv, low, lowinv, TheBigA, TheBigB, *two_to_phi, *two_to_minusphi;
complex *expn;
UL flag;
#endif
 {
  register BIG_DOUBLE err = 0.0;

  squareg(x, N, nn, expn);
  if (flag != kLast)
    err = normalize(x, N, b, c, high, highinv, low, lowinv, TheBigA, TheBigB, two_to_minusphi,
                    two_to_phi, flag);
  else
    err = last_normalize(x, N, b, c, high, highinv, low, lowinv, TheBigA, TheBigB, two_to_minusphi,
                         two_to_phi, (UL)1);
  return(err);
 }

#ifdef __STDC__
void squareg(BIG_DOUBLE *x, UL size, UL nn, complex *expn)
#else
void squareg(x, size, nn, expn)
BIG_DOUBLE *x;
UL size, nn;
complex *expn;
#endif
 {
  register long i, j, skip, currentLevel, levelsToDo;
  register long k, l, interval, interval2;
  register BIG_DOUBLE rtmp, itmp;

  currentLevel = 1;
  skip = size;
  levelsToDo = CACHE_LEVELS;
  skip >>= levelsToDo;

/* Pass 1 */
  for (i = 0; i < skip; i += CACHE_DEPTH)
    RealFFTR4(x, size, nn, currentLevel, levelsToDo, i, skip, CACHE_DEPTH, expn);
  currentLevel += levelsToDo;
  levelsToDo = nn - (currentLevel - 1);
  interval = 1 << (levelsToDo + 1);
  interval2 = interval >> 1;

/* Start Pass 2 */
  /* Perform FFT-Square-IFFT on Real Data */
  RealFFT(x, interval, (levelsToDo + 1), 2, levelsToDo, 0, 1, 1, expn);
  /* Square 'em */
  x[0] *= x[0];
  x[1] *= x[1];
  if (interval <= BLOCKSIZE)
    for (j = 2; j < interval; j += 2)
     {
      k = j + 1;
      rtmp = x[j]*x[j] - x[k]*x[k];
      itmp = 2*x[j]*x[k];
      x[j] = rtmp;
      x[k] = itmp;
     }
  else
   {
    for (j = 2; j < BLOCKSIZE; j += 2)
     {
      k = j + 1;
      rtmp = x[j]*x[j] - x[k]*x[k];
      itmp = 2*x[j]*x[k];
      x[j] = rtmp;
      x[k] = itmp;
     }
    k = BLOCKSIZE + 4;
    for (i = 1; i < (interval >> 9); i++)
     {
      for (j = 0; j < BLOCKSIZE; j += 2)
       {
        l = k + 1;
        rtmp = x[k]*x[k] - x[l]*x[l];
        itmp = 2*x[k]*x[l];
        x[k] = rtmp;
        x[l] = itmp;
        k += 2;
       }
      k += 4;
     }
   }
  RealFFTInv(x, interval, (levelsToDo + 1), (levelsToDo + 1), levelsToDo, 0, 1, 1, expn);
  /* Perform FFT-Square-IFFT on Complex Data */
  if (levelsToDo > 3)
    for (i = interval; i < size; i += interval)
      FFT_IFFTR8(x, size, nn, currentLevel, levelsToDo, i, 1, expn);
  else
    /* Can't use a Radix-8 algorithm to perform less than 3 levels!!! */
    for (i = interval; i < size; i += interval)
      FFT_IFFT(x, size, nn, currentLevel, levelsToDo, i, 1, expn);
/* End Pass 2 */

     currentLevel= nn - levelsToDo;
     levelsToDo = nn - levelsToDo;

/* Pass 3 */
     for (i = 0; i < skip; i += CACHE_DEPTH)
       RealFFTInvR4(x, size, nn, currentLevel, levelsToDo, i, skip, CACHE_DEPTH, expn);
 }

/*--- FFT Routines ---*/

/*
		void RealFFT(
             BIG_DOUBLE *x <== data array,
             long n    <== size of data array (includes "padding" elements)
             long nn   <== log2 of data array size (not including
                           "padding" elements)
             long currentLevel <== Starting FFT level
             long levelsToDo   <== number of FFT levels to be performed
             long startElement <== offset from the start of the data array
                                   for the 1st element in this FFT
             long skip     <== number of data elements between data elements in
                               this FFT's dataset
             long depth    <== number of FFT's done in parallel (see below)
             complex *expn <== sine/cosine data array
             )

	This routine takes an array of real values and performs "levelsToDo"
	levels of an FFT on them.  This is a radix-2 based FFT.  The data to be
	operated on is a subset of the full data array *x.

	This routine is used primarily in Pass 1 of the LL test.

	There are 4 - types of FFT operations performed in this routine.

	The first type of operation takes N "Pure Real" values (2 at a time)
	as input and outputs N "Pure Real" values (2 at a time). This corresponds
	to FFT "Twiddle" factor W0 (cos=1, sin = 0).

  The second type of operation takes N "Pure Real" inputs (2 at a time) and
  outputs N/2 complex outputs (1 at a time).  No data is lost, since the N/2
  complex values not calculated are complex conjugates of those that are
  calculated.  This corresponds to FFT "Twiddle" factor W1
  (cos = 0, sin = -1).

  The third type of operation takes N complex inputs (2 at a time) and outputs
  N complex values (2 at a time).  There is data swapping.

  Input order is  : A_real, B_real, A_imag, B_imag
  Output order is : A_real, A_imag, B_real, B_imag

  The fourth type of operation takes N complex inputs (4 at a time) and outputs
  N complex values (4 at a time).  This type performs butterfly operations
  on (A,B) and (C,D).  There is data swapping.

  Input order is  : A_real, B_real, A_imag, B_imag,
                    C_real, D_real, C_imag, D_imag
  Output order is : A_real, A_imag, C_real, C_imag,
                    D_real, D_imag, B_real, B_imag

  The data padding characterized by

		j4 = j+((j&MASK)>>SHIFT);

	is designed to prevent data elements in a given FFT subset of *x from landing
	in the same data cache line.  This is accomplished by having 512 data elements
	(4 Kbytes - 1 cache page) followed by 4 padding elements
	(32 bytes - 1 cache line).

	A typical use of this routine will perform 7 levels of FFT.  This uses 128
	data elements.  This data is loaded into the cache one element per line
	in a page of the data cache.  The parameter "depth" is typically set to 4
	to take advantage of the fact that entire lines (4 elements) are loaded into
	the cache at a time from main memory.  This arrangement would look like this
	for the first 7 levels of a 16 level FFT...

	Line 0        0     1     2     3
	Line 1      512   513   514   515
	Line 2     1024  1025  1026  1027
	.           .     .     .     .
	.           .     .     .     .
	.           .     .     .     .
	Line 126  64512 64513 64514 64515
	Line 127  65024 65025 65026 65027

	This routine with depth = 4 would perform a 7 level FFT on the data set
	0,512,1024,...64512,65024, in parallel with FFT's on the datasets
	1,513,...
	2,514,...
	3,515,...

	For an input of 128 "Pure Real" data values (for levelsToDo = 7) this
	routine will output 2 "Pure Real" values and 63 complex values.  No
	information is lost in this process, however, since the 63 complex values
	not calculated are complex conjugates of those that are calculated.

	There is considerable data scrambling that occurs (in order to make the
	Pass 2 FFT easier to fit in the data cache)

	Input          Output
	0 (Pure Real)    0' (Pure Real)
	1 (Pure Real)    1' (Pure Real)
	2 (Pure Real)    2' (Real Part)
	3 (Pure Real)    2' (Imag Part)
	4 (Pure Real)    3' (Real Part)
	5 (Pure Real)    3' (Imag Part)
	6 (Pure Real)    4' (Real Part)
	7 (Pure Real)    4' (Imag Part)
	...
	...
	...
	124 (Pure Real)    62' (Real Part)
	125 (Pure Real)    62' (Imag Part)
	126 (Pure Real)    63' (Real Part)
	127 (Pure Real)    63' (Imag Part)

*/

#ifdef __STDC__
void RealFFT(BIG_DOUBLE *x, long n, long nn, long currentLevel, long levelsToDo, long startElement,
             long skip, long depth, complex *expn)
#else
void RealFFT(x, n, nn, currentLevel, levelsToDo, startElement, skip, depth, expn)
BIG_DOUBLE *x;
long n, nn, currentLevel, levelsToDo, startElement, skip, depth;
complex *expn;
#endif
 {
  long a, j, j4, j5, j2, j3, b, i, k, n2, loops, m, o0, o2, offset1, offset0j, offset1j, offset2j, offset3j;
  register BIG_DOUBLE c, s, c1, s1, rtmp, itmp, rtmp0, itmp0, rtmp1, itmp1, rtmp2, itmp2, rtmp3, itmp3;

  loops = 1 << (currentLevel - 1);
  n2 = n >> currentLevel;
  for (b = n2 << 1; levelsToDo--; b = n2, n2 >>= 1, currentLevel++, loops <<= 1)
   { /* First Loop - Real to Real xform */
    a = startElement;
    offset1 = n2 + ((n2 & MASK) >> SHIFT);
    for (j = a; j < a + n2; j += skip)
     {
      j4 = j;
      j4 += (j4 & MASK) >> SHIFT;
      for (m = 0; m < depth; m++)
       {
        rtmp = x[j4 + offset1];
        x[j4 + offset1] = x[j4] - rtmp;
        x[j4] = x[j4] + rtmp;
        j4++;
       }
     }
    /* Second Loop - Real to Complex xform */
    if (loops > 1)
     {
      a = b + startElement;
      for (j = a; j < a + n2; j += skip)
       {
        j4 = j + n2;
        j4 += (j4 & MASK) >> SHIFT;
        for (m = 0; m < depth; m++)
          x[j4++] *= -1;
       }
     }
    /* Third Loop - Complex to Complex xform */
    if (loops > 2)
     {
      a = 2*b + startElement;
      j4 = a >> (nn - currentLevel);
      j4 += (j4 & MASK) >> SHIFT;
      c = expn[j4].re;
      s = expn[j4].im;
      offset1 = n2 + ((n2 & MASK) >> SHIFT);
      offset0j = 2*n2 + (((2*n2) & MASK) >> SHIFT);
      for (j = a; j < a + n2; j += skip)
       {
        j4 = j + ((j & MASK) >> SHIFT);
        j5 = j4 + offset0j;
        for (m = 0; m < depth; m++, j4++, j5++)
         {
          rtmp = x[j4];
          itmp = x[j5];
          rtmp1 = x[j4 + offset1];
          itmp1 = x[j5 + offset1];
          x[j4] = rtmp + c*rtmp1 - s*itmp1;
          x[j4 + offset1] = itmp + s*rtmp1 + c*itmp1;
          x[j5] = rtmp - c*rtmp1 + s*itmp1;
          x[j5 + offset1] = -itmp + s*rtmp1 + c*itmp1;
         }
       }
     }
    /* Last Loop - Complex to Complex xform */
    for (i = 4; i < loops; i <<= 1)
     {
      a = i*b + startElement;
      o0 = 0;
      o2 = 2*i - 4;
      for (k = 0; k < i >> 2; k++)
       {
        j4 = (a + o0*n2) >> (nn - currentLevel);
        j5 = (a + (o0+2)*n2) >> (nn - currentLevel);
        j4 += (j4 & MASK) >> SHIFT;
        j5 += (j5 & MASK) >> SHIFT;
        c = expn[j4].re;
        s = expn[j4].im;
        c1 = expn[j5].re;
        s1 = expn[j5].im;
        offset0j = o0*n2 + (((o0*n2) & MASK) >> SHIFT);
        offset1j = (o0 + 2)*n2 + ((((o0 + 2)*n2) & MASK) >> SHIFT);
        offset2j = o2*n2 + (((o2*n2) & MASK) >> SHIFT);
        offset3j = (o2 + 2)*n2 + ((((o2 + 2)*n2) & MASK) >> SHIFT);
        for (j = a; j < a + n2; j += skip)
         {
          j4 = j + ((j & MASK) >> SHIFT);
          j5 = j4 + offset1j;
          j2 = j4 + offset2j;
          j3 = j4 + offset3j;
          j4 += offset0j;
          for (m = 0; m < depth; m++, j4++, j5++, j2++, j3++)
           {
            rtmp0 = x[j4];
            itmp0 = x[j5];
            rtmp1 = x[j4 + offset1];
            itmp1 = x[j5 + offset1];
            rtmp2 = x[j2];
            itmp2 = x[j3];
            rtmp3 = x[j2 + offset1];
            itmp3 = x[j3 + offset1];
            x[j4 + offset1] = itmp0 + s*rtmp1 + c*itmp1;
            x[j4] = rtmp0 + c*rtmp1 - s*itmp1;
            x[j3 + offset1] = -itmp0 + s*rtmp1 + c*itmp1;
            x[j3] = rtmp0 - c*rtmp1 + s*itmp1;
            x[j5 + offset1] = -itmp2 + s1*rtmp3 - c1*itmp3;
            x[j5] = rtmp2 + c1*rtmp3 + s1*itmp3;
            x[j2 + offset1] = itmp2 + s1*rtmp3 - c1*itmp3;
            x[j2] = rtmp2 - c1*rtmp3 - s1*itmp3;
           }
         }
        o0+=4;
        o2-=4;
       }
     }
   }
 }

/*
		void RealFFTR4(
             BIG_DOUBLE *x <== data array,
             long n    <== size of data array (includes "padding" elements)
             long nn   <== log2 of data array size (not including
                           "padding" elements)
             long currentLevel <== Starting FFT level
             long levelsToDo   <== number of FFT levels to be performed
             long startElement <== offset from the start of the data array
                                   for the 1st element in this FFT
             long skip     <== number of data elements between data elements in
                               this FFT's dataset
             long depth    <== number of FFT's done in parallel (see below)
             complex *expn <== sine/cosine data array
             )

	This routine is equivalent to the RealFFT() routine, except it has been
	customized to take advantage of specific PowerPC architactural features
	(the 32 floating point registers).

	This routine is "hard wired" to perform 7 levels of an FFT, and assumes
	that the input is an array of 128 "Pure Real" values.

	The output of 2 "Pure Real" values and 63 complex values is identical to
	that generated by the RealFFT() routine.

	The 7 levels of the FFT are performed in 3 passes.

	The first pass performs 3-levels in a single operation as a radix-8 based
	FFT that takes 8 "Pure Real" values and outputs 2 "Pure Real" values and 3
	complex values.

	The second pass performs 2-levels in a single operation as a radix-4 based
	FFT that takes 4 complex values as input and generates 4 complex values
	as output.

	The third pass performs 2-levels in a single operation as a radix-4 based
	FFT that takes 4 complex values as input and generates 4 complex values
	as output.

	For details of the data scrambling involved in this routine see the comments
	in the RealFFT() routine.

	Considerable optimization has been performed to take advantage of the natural
	symmetry of the FFT "Twiddle" factors (sin/cos values).  The main point
	to this is that it is faster to calculate these values than it is to read
	them from memory.  I discovered these symmetries by observation, but I'm
	sure that they would be obvious from an examination of the mathematical
	representation of the FFT.

	This routine trys to take advantage of the microprocessor's the ability
	to do multiple things at one time.  It assumes that integer, floating point,
	and load/store operations can all be performed simultaneously and
	independently.  I have also assumed that whatever compiler is used can
	perform the scheduling of these three sets of operations efficiently

	This routine is ~43% faster than the RealFFT() routine on a PowerPC 604e
	processor.

*/


#ifdef __STDC__
void RealFFTR4(
             BIG_DOUBLE *x,
             long n, long nn,
             long currentLevel, long levelsToDo,
             long startElement, long skip, long depth,
             complex *expn)
#else
void RealFFTR4(x, n, nn, currentLevel, levelsToDo, startElement, skip, depth, expn)
BIG_DOUBLE *x;
long n, nn, currentLevel, levelsToDo, startElement, skip, depth;
complex *expn;
#endif
{
    long a,j,j4,j5,j2,j3,b,i,k,n2,loops,m,
    			 o0,o2,offset1,offset2, offset0j,offset1j,offset2j,offset3j;
    long n4,n8,offset18,offset28,offset38,offset48,offset58,offset68,offset78;
    register BIG_DOUBLE c1,s1,c2, s2, c4, s4, sqrtHalf,*xoffset;
	register BIG_DOUBLE t0,t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11,t12,t13,t14,t15,t16,
					t17,t18,t19,t20,t21,t22,t23,t24,t25,t26,t27,t28,t29,t30,t31;

/* */
/* */
/* Pass 1 - Performs the first 3 levels of the FFT */
/* */
/*     */
    sqrtHalf = expn[4].re;

    n2 = n>>currentLevel;
	n4 = n2>>1;
	n8 = n2>>2;

	a = startElement;
	offset18 = n8 + ((n8&MASK)>>SHIFT);
	offset28 = n4 + ((n4&MASK)>>SHIFT);
	offset38 = (n4+n8) + (((n4+n8)&MASK)>>SHIFT);
	offset48 = n2 + ((n2&MASK)>>SHIFT);
	offset58 = n2+n8 + (((n2+n8)&MASK)>>SHIFT);
	offset68 = n2+n4 + (((n2+n4)&MASK)>>SHIFT);
	offset78 = n2+n4+n8 + (((n2+n4+n8)&MASK)>>SHIFT);
	for (j=a; j<a+n8; j+=skip) {
		j4 = j + ((j&MASK)>>SHIFT);
		for (m=0; m<depth; m++) {
			t0 = x[j4];
			xoffset = &x[j4];
			t4 = xoffset[offset48];
			t8 = t0 + t4;
			t12 = t0 - t4;
			t1 = xoffset[offset18];
			t5 = xoffset[offset58];
			t9 = t1 + t5;
			t13 = t1 - t5;
			t2 = xoffset[offset28];
			t6 = xoffset[offset68];
			t10 = t2 + t6;
			t14 = t2 - t6;
			t16 = t8 + t10;
			t3 = xoffset[offset38];
			t7 = xoffset[offset78];
			t11 = t3 + t7;
			xoffset[offset28] = t8 - t10;
			xoffset[offset38] = t11 - t9;
			t15 = t3 - t7;
			t17 = t9 + t11;
			x[j4] = t16 + t17;
			xoffset[offset18] = t16 - t17;
			t20 = t13 - t15;
			t21 = t13 + t15;
			xoffset[offset48] = t12 + sqrtHalf*t20;
			xoffset[offset58] = -t14 - sqrtHalf*t21;
			xoffset[offset68] = t12 - sqrtHalf*t20;
			xoffset[offset78] = t14 - sqrtHalf*t21;
			j4++;
		}
	}

	currentLevel +=3;
	levelsToDo -=3;

    loops = 1<<(currentLevel-1);
    n2 = n>>currentLevel;
    n4 = n2>>1;
    b = n2<<1;
/* */
/* */
/* Pass 2 - Performs the next 2 levels of the FFT */
/* */
/*     */

	/* First Loop - Real to Real xform */
			a = startElement;
			offset1 = n2 + ((n2&MASK)>>SHIFT);
			xoffset = &x[offset1];
			offset2 = n4 + ((n4&MASK)>>SHIFT);
			for (j=a; j< a+n4; j+=skip) {
			    j4 = j;
			    j5 = j+n4;
				j4+=(j4&MASK)>>SHIFT;
				j5+=(j5&MASK)>>SHIFT;
				for (m=0; m<depth; m++) {
				    t0 = x[j4];
				    t1 = x[j5];
				    t2 = xoffset[j4];
				    t3 = xoffset[j5];
				    t4 = t0 + t2;
				    t5 = t1 + t3;
				    xoffset[j5] = t3 - t1;
				    xoffset[j4] = t0 - t2;
				    x[j4] = t4 + t5;
				    x[j5] = t4 - t5;
					j4++;
					j5++;
				}

			}

	/* Second Loop - Real to Complex xform */
			a = b+startElement;
			for (j=a; j< a+n4; j+=skip) {
			    j4 = j;
			    j5 = j+n4;
				j4+=(j4&MASK)>>SHIFT;
				j5+=(j5&MASK)>>SHIFT;
				for (m=0; m<depth; m++) {
				    t0 = x[j4];						/* Ar */
				    t1 = x[j5];						/* Br */
				    t2 = xoffset[j4];				/* -Ai */
				    t3 = xoffset[j5];				/* -Bi */
				    t4 = t1 - t3;
				    t5 = t1 + t3;
				    x[j4] = t0 + sqrtHalf*t4;			/* Ar' */
				    x[j5] = -t2 - sqrtHalf*t5;			/* Ai' */
				    xoffset[j4] = t0 - sqrtHalf*t4;	/* Br' */
				    xoffset[j5] = t2 - sqrtHalf*t5;	/* Bi' */
					j4++;
					j5++;
				}

			}

                        /* Third Loop - Complex to Complex xform */
                        a = 2*b+startElement;
			c2 = expn[10].re;
			s2 = expn[10].im;
			c1 = -s2;
			s1 = c2;
			offset1 = n2 + ((n2&MASK)>>SHIFT);
			xoffset = &x[offset1];
			offset2 = n4 + ((n4&MASK)>>SHIFT);
			offset0j = 2*n2 + (((2*n2)&MASK)>>SHIFT);
			for (j=a; j< a+n4; j+=skip) {
			    j4 = j + ((j&MASK)>>SHIFT);
				j5 = j4 + offset0j;
				j2 = j4 + offset2;
				j3 = j2 + offset0j;
				for (m=0; m<depth; m++) {
					t0 = x[j4];					/* R1 */
					t1 = x[j2];					/* R2 */
					t2 = xoffset[j4];			/* R3 */
					t3 = xoffset[j2];			/* R4 */
					t4 = x[j5];					/* I1 */
					t5 = x[j3];					/* I2 */
					t6 = xoffset[j5];			/* I3 */
					t7 = xoffset[j3];			/* I4 */
					t16 = t2 + t6;
					t17 = t3 + t7;
					t18 = t6 - t2;
					t19 = t7 - t3;
					t8 = t0 + sqrtHalf*t16;		/* R1' */
					t9 = t1 + sqrtHalf*t17;		/* R2' */
					t10 = t0 - sqrtHalf*t16;		/* R3' */
					t11 = t1 - sqrtHalf*t17;		/* R4' */
					t12 = t4 + sqrtHalf*t18;		/* I1' */
					t13 = t5 + sqrtHalf*t19;		/* I2' */
					t14 = -t4 + sqrtHalf*t18;		/* I3' */
					t15 = -t5 + sqrtHalf*t19;		/* I4' */

					x[j4] = t8 + c1*t9 - s1*t13;						/* R1'' */
					x[j2] = t12 + s1*t9 + c1*t13;						/* I1'' */
					xoffset[j4] = t10 + c2*t11 + s2*t15;				/* R3'' */
					xoffset[j2] = -t14 + s2*t11 - c2*t15;				/* I3'' */
					j4++; j2++;
					x[j5] = t10 - c2*t11 - s2*t15;						/* R4'' */
					x[j3] = t14 + s2*t11 - c2*t15;				  		/* I4'' */
					xoffset[j5] = t8 - c1*t9 + s1*t13;				/* R2'' */
					xoffset[j3] = -t12 + s1*t9 + c1*t13;				/* I2'' */
					j5++; j3++;
				}

			}


	/* Last Loop - Complex to Complex xform */
			a = 4*b+startElement;
			o0 = 0;  o2 = 4;
			c2 = expn[16].re;
			s2 = expn[16].im;
			c1 = c2*c2 - s2*s2;
			s1 = c2*s2;
			s1+= s1;
			c4 = sqrtHalf*c2 + sqrtHalf*s2;
			s4 = sqrtHalf*s2 - sqrtHalf*c2;
			offset1j = 2*n2 + (((2*n2)&MASK)>>SHIFT);
			offset2j = o2*n2 + (((o2*n2)&MASK)>>SHIFT);
			offset3j = (o2+2)*n2 + ((((o2+2)*n2)&MASK)>>SHIFT);
			for (j=a; j< a+n4; j+=skip) {
				j4 = j+((j&MASK)>>SHIFT);
				j5 = j4+offset1j;
				j2 = j4+offset2j;
				j3 = j4+offset3j;
				for (m=0; m<depth; m++) {
					t0 = x[j4];			/* Ar */
					t1 = x[j5];			/* Ai */
					t2 = xoffset[j4];	/* Cr */
					t3 = xoffset[j5];	/* Ci */
					j4 += offset2; /* j4 */
					j5 += offset2; /* j5 */
					t16 = t0 + c1*t2 - s1*t3;		/* Ar' */
					t17 = t1 + s1*t2 + c1*t3;		/* Ai' */
					t18 = t0 - c1*t2 + s1*t3;		/* Cr' */
					t19 = -t1 + s1*t2 + c1*t3;	/* Ci' */

					t4 = x[j2];			/* Er */
					t5 = x[j3];			/* Ei */
					t6 = xoffset[j2];	/* Gr */
					t7 = xoffset[j3];	/* Gi */
					j2 += offset2; /* j6 */
					j3 += offset2; /* j7 */
					t20 = t4 + s1*t6 - c1*t7;	/* Er' */
					t21 = -t5 - c1*t6 - s1*t7;	/* Ei' */
					t22 = t4 - s1*t6 + c1*t7;	/* Gr' */
					t23 = t5 - c1*t6 - s1*t7;	/* Gi' */

					t8 = x[j4]; /* j4			// Br */
					t9 = x[j5];	/* j5		// Bi */
					t10 = xoffset[j4];/* Dr */
					t11 = xoffset[j5];/* Di */
					t24 = t8 + c1*t10 - s1*t11;	/* Br' */
					t25 = t9 + s1*t10 + c1*t11;	/* Bi' */
					t26 = t8 - c1*t10 + s1*t11;	/* Dr' */
					t27 = -t9 + s1*t10 + c1*t11;	/* Di' */

					t12 = x[j2]; /* j6		// Fr */
					t13 = x[j3]; /* j7		// Fi */
					t14 = xoffset[j2];/* Hr */
					t15 = xoffset[j3];/* Hi */
					t28 = t12 + s1*t14 - c1*t15;	/* Fr' */
					t29 = -t13 - c1*t14 - s1*t15;	/* Fi' */
					t30 = t12 - s1*t14 + c1*t15;	/* Hr' */
					t31 = t13 - c1*t14 - s1*t15;	/* Hi' */

					x[j4] = t17 + s2*t24 + c2*t25; /* j4			// Ai'' */
					xoffset[j4] = -t19 - c2*t26 - s2*t27;	/* Ci'' */
					j4 -= offset2; /* j4 */
					x[j5] = t21 + s4*t28 + c4*t29; /* j5			// Ei'' */
					xoffset[j5] = -t23 - c4*t30 - s4*t31;	/* Gi'' */
					j5 -= offset2; /* j5 */

					x[j4] = t16 + c2*t24 - s2*t25;			/* Ar'' */
					xoffset[j4] = t18 + s2*t26 - c2*t27;	/* Cr'' */
					x[j5] = t20 + c4*t28 - s4*t29;			/* Er'' */
					xoffset[j5] = t22 + s4*t30 - c4*t31;	/* Gr'' */
					j4++; j5++;

					x[j2] = t23 - c4*t30 - s4*t31; /* j6			// Hi'' */
					xoffset[j2] = -t21 + s4*t28 + c4*t29;	/* Fi'' */
					j2 -= offset2; /* j2 */
					x[j3] = t19 - c2*t26 - s2*t27;  /* j7			// Di'' */
					xoffset[j3] = -t17 + s2*t24 + c2*t25;	/* Bi'' */
					j3 -= offset2; /* j3 */

					x[j2] = t22 - s4*t30 + c4*t31;			/* Hr'' */
					xoffset[j2] = t20 - c4*t28 + s4*t29;	/* Fr'' */
					x[j3] = t18 - s2*t26 + c2*t27;			/* Dr'' */
					xoffset[j3] = t16 - c2*t24 + s2*t25;	/* Br'' */
					j2++; j3++;
				}

			}

	currentLevel +=2;
	levelsToDo -=2;

    loops = 1<<(currentLevel-1);
    n2 = n>>currentLevel;
    n4 = n2>>1;
    b = n2<<1;

/* */
/* */
/* Pass 3 - Performs the last 2 levels of the FFT */
/* */
/*     */

	/* First Loop - Real to Real xform */
			a = startElement;
			offset1 = n2 + ((n2&MASK)>>SHIFT);
			xoffset = &x[offset1];
			offset2 = n4 + ((n4&MASK)>>SHIFT);
			for (j=a; j< a+n4; j+=skip) {
			    j4 = j;
			    j5 = j+n4;
				j4+=(j4&MASK)>>SHIFT;
				j5+=(j5&MASK)>>SHIFT;
				for (m=0; m<depth; m++) {
				    t0 = x[j4];
				    t2 = xoffset[j4];
				    t4 = t0 + t2;
				    t6 = t0 - t2;
				    t1 = x[j5];
				    t3 = xoffset[j5];
				    t5 = t1 + t3;
				    t7 = t3 - t1;
				    xoffset[j5] = t7;
				    xoffset[j4] = t6;
				    t8 = t4 + t5;
				    t9 = t4 - t5;
				    x[j4] = t8;
				    x[j5] = t9;
					j4++;
					j5++;
				}

			}

	/* Second Loop - Real to Complex xform */
			a = b+startElement;
			for (j=a; j< a+n4; j+=skip) {
			    j4 = j;
			    j5 = j+n4;
				j4+=(j4&MASK)>>SHIFT;
				j5+=(j5&MASK)>>SHIFT;
				for (m=0; m<depth; m++) {
				    t1 = x[j5];						/* Br */
				    t3 = xoffset[j5];				/* -Bi */
				    t4 = t1 - t3;
				    t5 = t1 + t3;
				    t0 = x[j4];						/* Ar */
				    t2 = xoffset[j4];				/* -Ai */

				    x[j4] = t0 + sqrtHalf*t4;			/* Ar' */
				    x[j5] = -t2 - sqrtHalf*t5;			/* Ai' */
				    xoffset[j4] = t0 - sqrtHalf*t4;	/* Br' */
				    xoffset[j5] = t2 - sqrtHalf*t5;	/* Bi' */
					j4++;
					j5++;
				}

			}


	/* Third Loop - Complex to Complex xform */
			a = 2*b+startElement;
			c2 = expn[10].re;
			s2 = expn[10].im;
			c1 = -s2;
			s1 = c2;
			offset1 = n2 + ((n2&MASK)>>SHIFT);
			offset2 = n4 + ((n4&MASK)>>SHIFT);
			offset0j = 2*n2 + (((2*n2)&MASK)>>SHIFT);
			for (j=a; j< a+n4; j+=skip) {
			    j4 = j + ((j&MASK)>>SHIFT);
				j5 = j4 + offset0j;
				j2 = j4 + offset2;
				j3 = j2 + offset0j;
				for (m=0; m<depth; m++) {
					t2 = xoffset[j4];			/* R3 */
					t6 = xoffset[j5];			/* I3 */
					t16 = t2 + t6;
					t18 = t6 - t2;
					t3 = xoffset[j2];			/* R4 */
					t7 = xoffset[j3];			/* I4 */
					t17 = t3 + t7;
					t19 = t7 - t3;
					t0 = x[j4];					/* R1 */
					t8 = t0 + sqrtHalf*t16;		/* R1' */
					t10 = t0 - sqrtHalf*t16;		/* R3' */
					t1 = x[j2];					/* R2 */
					t9 = t1 + sqrtHalf*t17;		/* R2' */
					t11 = t1 - sqrtHalf*t17;		/* R4' */
					t4 = x[j5];					/* I1 */
					t12 = t4 + sqrtHalf*t18;		/* I1' */
					t14 = -t4 + sqrtHalf*t18;		/* I3' */
					t5 = x[j3];					/* I2 */
					t13 = t5 + sqrtHalf*t19;		/* I2' */
					t15 = -t5 + sqrtHalf*t19;		/* I4' */

					x[j4] = t8 + c1*t9 - s1*t13;						/* R1'' */
					x[j2] = t12 + s1*t9 + c1*t13;						/* I1'' */
					xoffset[j4] = t10 + c2*t11 + s2*t15;				/* R3'' */
					xoffset[j2] = -t14 + s2*t11 - c2*t15;				/* I3'' */
					j4++; j2++;
					x[j5] = t10 - c2*t11 - s2*t15;						/* R4'' */
					x[j3] = t14 + s2*t11 - c2*t15;				  		/* I4'' */
					xoffset[j5] = t8 - c1*t9 + s1*t13;				/* R2'' */
					xoffset[j3] = -t12 + s1*t9 + c1*t13;				/* I2'' */
					j5++; j3++;
				}

			}


	/* Last Loop - Complex to Complex xform */
		for (i=4; i<loops; i<<=1) {
			a = i*b+startElement;
			o0 = 0;  o2 = 2*i-4;
			for (k=0;k<i>>2;k++){
			j2 = (a+o0*n2) >> (nn-currentLevel-1);
			c2 = expn[j2].re;
			s2 = expn[j2].im;
			c1 = c2*c2 - s2*s2;
			s1 = c2*s2;
			s1+= s1;
			c4 = sqrtHalf*c2 + sqrtHalf*s2;
			s4 = sqrtHalf*s2 - sqrtHalf*c2;
			offset0j = o0*n2 + (((o0*n2)&MASK)>>SHIFT);
			offset1j = (o0+2)*n2 + ((((o0+2)*n2)&MASK)>>SHIFT);
			offset2j = o2*n2 + (((o2*n2)&MASK)>>SHIFT);
			offset3j = (o2+2)*n2 + ((((o2+2)*n2)&MASK)>>SHIFT);
			for (j=a; j< a+n4; j+=skip) {
				j4 = j+((j&MASK)>>SHIFT);
				j5 = j4+offset1j;
				j2 = j4+offset2j;
				j3 = j4+offset3j;
				j4 += offset0j;
				for (m=0; m<depth; m++) {
					t0 = x[j4];			/* Ar */
					t1 = x[j5];			/* Ai */
					t2 = xoffset[j4];	/* Cr */
					t3 = xoffset[j5];	/* Ci */
					j4 += offset2; /* j4 */
					j5 += offset2; /* j5 */
					t16 = t0 + c1*t2 - s1*t3;		/* Ar' */
					t17 = t1 + s1*t2 + c1*t3;		/* Ai' */
					t18 = t0 - c1*t2 + s1*t3;		/* Cr' */
					t19 = -t1 + s1*t2 + c1*t3;	/* Ci' */

					t4 = x[j2];			/* Er */
					t5 = x[j3];			/* Ei */
					t6 = xoffset[j2];	/* Gr */
					t7 = xoffset[j3];	/* Gi */
					j2 += offset2; /* j6 */
					j3 += offset2; /* j7 */
					t20 = t4 + s1*t6 - c1*t7;	/* Er' */
					t21 = -t5 - c1*t6 - s1*t7;	/* Ei' */
					t22 = t4 - s1*t6 + c1*t7;	/* Gr' */
					t23 = t5 - c1*t6 - s1*t7;	/* Gi' */

					t8 = x[j4];	/* j4		// Br */
					t9 = x[j5];	/* j5		// Bi */
					t10 = xoffset[j4];/* Dr */
					t11 = xoffset[j5];/* Di */
					t24 = t8 + c1*t10 - s1*t11;	/* Br' */
					t25 = t9 + s1*t10 + c1*t11;	/* Bi' */
					t26 = t8 - c1*t10 + s1*t11;	/* Dr' */
					t27 = -t9 + s1*t10 + c1*t11;	/* Di' */

					t12 = x[j2];  /* j6		// Fr */
					t13 = x[j3];  /* j7		// Fi */
					t14 = xoffset[j2];/* Hr */
					t15 = xoffset[j3];/* Hi */
					t28 = t12 + s1*t14 - c1*t15;	/* Fr' */
					t29 = -t13 - c1*t14 - s1*t15;	/* Fi' */
					t30 = t12 - s1*t14 + c1*t15;	/* Hr' */
					t31 = t13 - c1*t14 - s1*t15;	/* Hi' */

					x[j4] = t17 + s2*t24 + c2*t25;	/* j4		// Ai'' */
					xoffset[j4] = -t19 - c2*t26 - s2*t27;	/* Ci'' */
					j4 -= offset2; /* j4 */
					x[j5] = t21 + s4*t28 + c4*t29;	/* j5		// Ei'' */
					xoffset[j5] = -t23 - c4*t30 - s4*t31;	/* Gi'' */
					j5 -= offset2; /* j5 */

					x[j4] = t16 + c2*t24 - s2*t25;			/* Ar'' */
					xoffset[j4] = t18 + s2*t26 - c2*t27;	/* Cr'' */
					x[j5] = t20 + c4*t28 - s4*t29;			/* Er'' */
					xoffset[j5] = t22 + s4*t30 - c4*t31;	/* Gr'' */
					j4++; j5++;

					x[j2] = t23 - c4*t30 - s4*t31;	/* j6		// Hi'' */
					xoffset[j2] = -t21 + s4*t28 + c4*t29;	/* Fi'' */
					j2 -= offset2; /* j2 */
					x[j3] = t19 - c2*t26 - s2*t27;	/* j7		// Di'' */
					xoffset[j3] = -t17 + s2*t24 + c2*t25;	/* Bi'' */
					j3 -= offset2; /* j3 */


					x[j2] = t22 - s4*t30 + c4*t31;			/* Hr'' */
					xoffset[j2] = t20 - c4*t28 + s4*t29;	/* Fr'' */
					x[j3] = t18 - s2*t26 + c2*t27;			/* Dr'' */
					xoffset[j3] = t16 - c2*t24 + s2*t25;	/* Br'' */
					j2++; j3++;
				}

			}
			o0+=4; o2-=4;
		  }
		}
}



/*		void RealFFTInv(
             BIG_DOUBLE *x <== data array,
             long n    <== size of data array (includes "padding" elements)
             long nn   <== log2 of data array size (not including
                           "padding" elements)
             long currentLevel <== Starting FFT level
             long levelsToDo   <== number of FFT levels to be performed
             long startElement <== offset from the start of the data array
                                   for the 1st element in this FFT
             long skip     <== number of data elements between data elements in
                               this FFT's dataset
             long depth    <== number of FFT's done in parallel (see below)
             complex *expn <== sine/cosine data array
             )

	This routine takes an array of complex values and performs the last "levelsToDo"
	levels of an inverse FFT on them.  This is a radix-2 based inverse FFT.
	The data to be operated on is a subset of the full data array *x.

	This routine is used primarily in Pass 3 of the LL test.

	It is an un-doing of what was done in the RealFFT() routine.

	The output of this routine is an unscrambled array of "Pure Real" values

	See The comments in the RealFFT() routine for more details
*/


#ifdef __STDC__
void RealFFTInv(
     BIG_DOUBLE *x,
     long n, long nn,
     long currentLevel, long levelsToDo,
     long startElement, long skip, long depth,
     complex *expn)
#else
void RealFFTInv(x, n, nn, currentLevel, levelsToDo, startElement, skip, depth, expn)
BIG_DOUBLE *x;
long n, nn, currentLevel, levelsToDo, startElement, skip, depth;
complex *expn;
#endif
{
     long a,j,j4,j5,j2,j3,b,i,k,n2,loops,m,o0,o2,offset1,offset0j,offset1j,offset2j,offset3j;
    register BIG_DOUBLE c,s,c1,s1,
                    rtmp,itmp,rtmp0,itmp0,rtmp1,itmp1,rtmp2,itmp2,rtmp3,itmp3,
                    itmp4, rtmp4,itmp5,rtmp5,temp0,temp1,temp2,temp3,temp4,temp5,temp6,temp7;

    loops = 1<<(currentLevel-1);
    n2 = n>>currentLevel;
    b = n2<<1;
    while (levelsToDo--) {

	/* First Loop - Real to Real xform */
			a = startElement;
			offset1 = n2 + ((n2&MASK)>>SHIFT);
			for (j=a; j< a+n2; j+=skip) {
				j4 = j;
				j4 += (j4&MASK)>>SHIFT;
				for (m = 0; m<depth; m++) {
					rtmp = x[j4+offset1];
					x[j4+offset1] = (x[j4] - rtmp);
					x[j4] = (x[j4] + rtmp);
					j4++;
				}

			}

	/* Second Loop - Complex to Real xform */
		if (loops>1) {
			a = b+startElement;
			offset1 = n2 + ((n2&MASK)>>SHIFT);
			for (j=a; j< a+n2; j+=skip) {
				j4 = j;
				j4 += (j4&MASK)>>SHIFT;
				for (m=0; m<depth; m++) {
					x[j4] *= 2;
					x[j4+offset1] *= -2;
					j4++;
				}

			}
		}


	/* Third Loop - Complex to Complex xform */
		if (loops>2) {
			a = 2*b+startElement;
			j4 = a >> (nn-currentLevel);
			j4 += (j4&MASK)>>SHIFT;
			c = expn[j4].re;
			s = expn[j4].im;
			offset1 = n2 + ((n2&MASK)>>SHIFT);
			offset0j = 2*n2 + (((2*n2)&MASK)>>SHIFT);
			for (j=a; j< a+n2; j+=skip) {
				j4 = j + ((j&MASK)>>SHIFT);
				j5 = j4 + offset0j;
				for (m=0; m<depth; m++) {
					itmp = x[j4]-x[j5];
					rtmp = x[j4+offset1]+x[j5+offset1];
					itmp1 = (c*rtmp-s*itmp);
					rtmp1 = (s*rtmp + c*itmp);
					itmp = (x[j4+offset1]-x[j5+offset1]);
					rtmp = (x[j4]+x[j5]);
					x[j4] = rtmp;
					x[j5] = itmp;
					x[j4+offset1] = rtmp1;
					x[j5+offset1] = itmp1;
					j4++;
					j5++;
				}

			}
		}

	/* Last Loop - Complex to Complex xform */
		for (i=4; i<loops; i<<=1) {
			a = i*b+startElement;
			o0 = 0;  o2 = 2*i-4;
			for (k=0;k<i>>2;k++){
			j4 = (a+o0*n2) >> (nn-currentLevel);
			j5 = (a+(o0+2)*n2) >> (nn-currentLevel);
			j4 += (j4&MASK)>>SHIFT;
			j5 += (j5&MASK)>>SHIFT;
			c = expn[j4].re;
			s = expn[j4].im;
			c1 = expn[j5].re;
			s1 = expn[j5].im;
			offset0j = o0*n2 + (((o0*n2)&MASK)>>SHIFT);
			offset1j = (o0+2)*n2 + ((((o0+2)*n2)&MASK)>>SHIFT);
			offset2j = o2*n2 + (((o2*n2)&MASK)>>SHIFT);
			offset3j = (o2+2)*n2 + ((((o2+2)*n2)&MASK)>>SHIFT);
			for (j=a; j< a+n2; j+=skip) {
				j4 = j+((j&MASK)>>SHIFT);
				j5 = j4+offset1j;
				j2 = j4+offset2j;
				j3 = j4+offset3j;
				j4 += offset0j;
				for (m=0; m<depth; m++) {
				    temp0 = x[j4+offset1];
				    temp1 = x[j3+offset1];
					itmp0 = temp0+temp1;
					itmp2 = (temp0 - temp1);
				    temp2 = x[j4];
				    temp3 = x[j3];
					rtmp0 = temp2-temp3;
					rtmp2 = (temp2 + temp3);
				    temp4 = x[j5+offset1];
				    temp5 = x[j2+offset1];
					itmp3 = temp4+temp5;
					itmp5 = (-temp4+temp5);
				    temp6 = x[j5];
				    temp7 = x[j2];
					rtmp3 = temp6-temp7;
					rtmp5 = (temp6+temp7);

					itmp1 = (-s*rtmp0 + c*itmp0);
					rtmp1 =  (c*rtmp0 + s*itmp0);
					itmp4 = (s1*rtmp3 - c1*itmp3);
					rtmp4 =(c1*rtmp3 + s1*itmp3);

					x[j5] = itmp2;
					x[j4] = rtmp2;
					x[j4+offset1] = rtmp1;
					x[j5+offset1] = itmp1;
					x[j2] = rtmp5;
					x[j3] = itmp5;
					x[j2+offset1] = rtmp4;
					x[j3+offset1] = itmp4;
					j4++; j5++; j2++; j3++;

				}
			}
			o0+=4;/* o1+=4;*/ o2-=4; /*o3-=4;*/
		  }
		}
		n2<<=1;
		b = n2<<1;
		currentLevel--;
		loops>>=1;
	}
}



/*
		void RealFFTInvR4(
             BIG_DOUBLE *x <== data array,
             long n    <== size of data array (includes "padding" elements)
             long nn   <== log2 of data array size (not including
                           "padding" elements)
             long currentLevel <== Starting FFT level
             long levelsToDo   <== number of FFT levels to be performed
             long startElement <== offset from the start of the data array
                                   for the 1st element in this FFT
             long skip     <== number of data elements between data elements in
                               this FFT's dataset
             long depth    <== number of FFT's done in parallel (see below)
             complex *expn <== sine/cosine data array
             )

	This routine is equivalent to the RealFFTInv() routine, except it has been
	customized to take advantage of specific PowerPC architactural features
	(the 32 floating point registers).

	This routine is "hard wired" to perform 7 levels of an inverse FFT, and assumes
	that the input is an array of 2 "Pure Real" values and 63 complex values.

	The output of 128 "Pure Real" values is identical to that generated by the
	RealFFTInv() routine.

	The 7 levels of the inverse FFT are performed in 3 passes.

	The first pass performs 2-levels in a single operation as a radix-4 based
	inverse FFT that takes 4 complex values as input and generates 4 complex
	values as output.

	The second pass performs 2-levels in a single operation as a radix-4 based
	inverse FFT that takes 4 complex values as input and generates 4 complex
	values as output.

	The third pass performs 3-levels in a single operation as a radix-8 based
	inverse FFT that takes 2 "Pure Real" values and 3 complex values as input
	and outputs 8 "Pure Real" values.
	.
	For details of the data scrambling involved in this routine see the comments
	in the RealFFT()/RealFFTInv() routines.

	Considerable optimization has been performed to take advantage of the natural
	symmetry of the FFT "Twiddle" factors (sin/cos values).  The main point
	to this is that it is faster to calculate these values than it is to read
	them from memory.  I discovered these symmetries by observation, but I'm
	sure that they would be obvious from an examination of the mathematical
	representation of the FFT.

	This routine trys to take advantage of the microprocessor's the ability
	to do multiple things at one time.  It assumes that integer, floating point,
	and load/store operations can all be performed simultaneously and
	independently.  I have also assumed that whatever compiler is used can
	perform the scheduling of these three sets of operations efficiently

	This routine is ~43% faster than the RealFFTInv() routine on a PowerPC 604e
	processor.

*/

#ifdef __STDC__
void RealFFTInvR4(
     BIG_DOUBLE *x,
     long n, long nn,
     long currentLevel, long levelsToDo,
     long startElement, long skip, long depth,
     complex *expn)
#else
void RealFFTInvR4(x, n, nn, currentLevel, levelsToDo, startElement, skip, depth, expn)
BIG_DOUBLE *x;
long n, nn, currentLevel, levelsToDo, startElement, skip, depth;
complex *expn;
#endif
{
    long a,j,j4,j5,j2,j3,b,i,k,n2,loops,m,o0,o2;
    long n4,offset1,offset2,offset3,offset4,offset5,offset6,offset7;
    register BIG_DOUBLE c1,s1,c2, s2, c4, s4, sqrtHalf,*xoffset;
    register BIG_DOUBLE t0,t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11,t12,t13,t14,t15,t16,
					t17,t18,t19,t20,t21;

    sqrtHalf = expn[4].re;

/* */
/* */
/* Pass 1 & 2 - Performs the first 4 (2 per loop) levels of the IFFT */
/* */
/*     */
    while (levelsToDo>3) {

	    currentLevel--;
	    loops = 1<<(currentLevel-1);
   	 	n2 = n>>currentLevel;
    	n4 = n2>>1;
    	b = n2<<1;

	/* First Loop - Real to Real xform */
			a = startElement;
			offset1 = n2 + ((n2&MASK)>>SHIFT);
			xoffset = &x[offset1];
			offset2 = n4 + ((n4&MASK)>>SHIFT);
			for (j=a; j< a+n4; j+=skip) {
			    j4 = j;
			    j5 = j+n4;
				j4+=(j4&MASK)>>SHIFT;
				j5+=(j5&MASK)>>SHIFT;
				for (m = 0; m<depth; m++) {
				    t0 = x[j4];
				    t1 = x[j5];
				    t4 = t0 + t1;
				    t5 = t0 - t1;
				    t2 = xoffset[j4];
				    t2 += t2;
				    x[j4] = t4 + t2;
				    xoffset[j4] = t4 - t2;
				    t3 = xoffset[j5];
				    t3 += t3;
				    x[j5] = t5 - t3;
				    xoffset[j5] = t5 + t3;
					j4++;
					j5++;
				}

			}

	/* Second Loop - Real to Complex xform */
			a = b+startElement;
			for (j=a; j< a+n4; j+=skip) {
			    j4 = j;
			    j5 = j+n4;
				j4+=(j4&MASK)>>SHIFT;
				j5+=(j5&MASK)>>SHIFT;
				for (m=0; m<depth; m++) {
				    t0 = x[j4];						/* Ar' */
				    t0 += t0;
				    t1 = x[j5];						/* Ai' */
				    t1 += t1;
				    t2 = xoffset[j4];				/* Br' */
				    t2 += t2;
				    t3 = xoffset[j5];				/* Bi' */
				    t3 += t3;
				    t4 = sqrtHalf*(t0 - t2);
				    t5 = -sqrtHalf*(t1 + t3);

					x[j4] = t0 + t2;
					x[j5] = t4 + t5;
					xoffset[j4] = t3 - t1;
					xoffset[j5] = t5 - t4;
					j4++;
					j5++;
				}

			}

	/* Third Loop - Complex to Complex xform */
			a = 2*b +startElement;
			c2 = expn[10].re;
			s2 = expn[10].im;
			c1 = -s2;
			s1 = c2;
			offset1 = n2 + ((n2&MASK)>>SHIFT);
			xoffset = &x[offset1];
			offset2 = n4 + ((n4&MASK)>>SHIFT);
			offset3 = 2*n2 + (((2*n2)&MASK)>>SHIFT);
			for (j=a; j< a+n4; j+=skip) {
			    j4 = j + ((j&MASK)>>SHIFT);
				j5 = j4 + offset3;
				j2 = j4 + offset2;
				j3 = j2 + offset3;
				for (m=0; m<depth; m++) {
					t0 = x[j4];					/* R1'' */
					t6 = xoffset[j5];			/* R2'' */
					t8 = t0 + t6;
					t2 = xoffset[j4];			/* R3'' */
					t4 = x[j5];					/* R4'' */
					t10 = t2 + t4;
					t1 = x[j2];					/* I1'' */
					t7 = xoffset[j3];			/* I2'' */
					t0 = t0 - t6;
					t12 = t1 - t7;
					t1 = t1 + t7;
					t9 = c1*t0 + s1*t1;
					t13 = -s1*t0 + c1*t1;
					t3 = xoffset[j2];			/* I3'' */
					t5 = x[j3];					/* I4'' */
					t14 = t5 - t3;
					t2 = t2 - t4;
					t3 = t3 + t5;
					t11 = c2*t2 + s2*t3;
					t15 = s2*t2 - c2*t3;
					x[j4] = t8 + t10;
					x[j2] = t9 + t11;
					t18 = sqrtHalf*(t12 + t14);
					t16 = sqrtHalf*(t8 - t10);
					xoffset[j4] = t16 - t18;
					t19 = sqrtHalf*(t13 + t15);
					t17 = sqrtHalf*(t9 - t11);
					xoffset[j2] = t17 - t19;
					j4++; j2++;
					x[j5] = t12 - t14;
					x[j3] = t13 - t15;
					xoffset[j5] = t16 + t18;
					xoffset[j3] = t17 + t19;
					j5++;  j3++;
				}

			}

	/* Last Loop - Complex to Complex xform */
		for (i=4; i<loops; i<<=1) {
			a = i*b+startElement;
			o0 = 0;  o2 = 2*i-4;
			for (k=0;k<i>>2;k++){
			j2 = (a+o0*n2) >> (nn-currentLevel-1);
			c2 = expn[j2].re;
			s2 = expn[j2].im;
			c1 = c2*c2 - s2*s2;
			s1 = c2*s2;
			s1 += s1;
			c4 = sqrtHalf*c2 + sqrtHalf*s2;
			s4 = sqrtHalf*s2 - sqrtHalf*c2;
			offset3 = o0*n2 + (((o0*n2)&MASK)>>SHIFT);
			offset4 = (o0+2)*n2 + ((((o0+2)*n2)&MASK)>>SHIFT);
			offset5 = o2*n2 + (((o2*n2)&MASK)>>SHIFT);
			offset6 = (o2+2)*n2 + ((((o2+2)*n2)&MASK)>>SHIFT);
			for (j=a; j< a+n4; j+=skip) {
				j4 = j+((j&MASK)>>SHIFT);
				j5 = j4+offset4;
				j2 = j4+offset5;
				j3 = j4+offset6;
				j4 += offset3;
				for (m=0; m<depth; m++) {
					t0 = x[j4];			/* Ar */
					t1 = x[j5];			/* Ai */
					t2 = xoffset[j4];	/* Cr */
					j4 += offset2; /* j4 */
					t3 = xoffset[j5];	/* Ci */
					j5 += offset2; /* j5 */
					t4 = x[j2];			/* Er */
					t5 = x[j3];			/* Ei */
					t6 = xoffset[j2];	/* Gr */
					j2 += offset2; /* j6 */
					t7 = xoffset[j3];	/* Gi */
					j3 += offset2; /* j7 */
					t8 = x[j4];	/* j4		// Br */
					t9 = x[j5];	/* j5		// Bi */
					t10 = xoffset[j4];/* Dr */
					t11 = xoffset[j5];/* Di */
					t12 = x[j2];/* j6		// Fr */
					t13 = x[j3];/* j7		// Fi */
					t14 = xoffset[j2];/* Hr */
					t15 = xoffset[j3];/* Hi */

					t20 = (t8 + t15);
					t21 = (t0 - t7);
					t16 = s2*t20 + c2*t21;
					t17 = c2*t20 - s2*t21;
					t20 = (t2 - t5);
					t21 = (t10 + t13);
					t18 = s2*t20 - c2*t21;
					t19 = c2*t20 + s2*t21;
					t20 = t16 + t18;  /* j4 */
					x[j4] = t20;  /* j4 */

					t20  =t17 + t19; /* j5 */
					x[j5]  =t20; /* j5 */

					t16 = t16 - t18;
					t17 = t17 - t19;
					t20 = s1*t17 + c1*t16;
					xoffset[j4] = t20;
					t20 = c1*t17 - s1*t16;
					xoffset[j5] = t20;
					j4 -= offset2; /* j4 */
					j5 -= offset2; /* j5 */

					t20 = (t9 + t14);
					t21 = (t1 - t6);
					t16 = s4*t20 + c4*t21;
					t17 = c4*t20 - s4*t21;
					t20 = (t3 - t4);
					t21 = (t11 + t12);
					t18 = s4*t20 - c4*t21;
					t19 = -s4*t21 - c4*t20;

					t20 = t16 + t18; /* j6 */
					x[j2] = t20; /* j6 */

					t20 = t19 - t17; /* j7 */
					x[j3] = t20; /* j7 */

					t16 = t16 - t18;
					t17 = t17 + t19;
					t20 = -c1*t17 + s1*t16;
					xoffset[j2] = t20;
					t20 = -c1*t16 - s1*t17;
					xoffset[j3] = t20;
					j2 -= offset2; /* j2 */
					j3 -= offset2; /* j3 */

					t16 = t0 + t7;
					t18 = t2 + t5;
					t20 = t16 + t18;
					x[j4] = t20;

					t17 = t8 - t15;
					t19 = t13 - t10;
					t20 = t17 - t19;
					x[j5] = t20;

					t16 = t16 - t18;
					t17 = t17 + t19;
					t20 = s1*t17 + c1*t16;
					xoffset[j4] = t20;
					t20 = c1*t17 - s1*t16;
					xoffset[j5] = t20;
					j4++; j5++;

					t16 = t1 + t6;
					t18 = t3 + t4;
					t20 = t16 + t18;
					x[j2] = t20;

					t17 = t9 - t14;
					t19 = t12 - t11;
					t20 = t19 - t17;
					x[j3] = t20;

					t16 = t16 - t18;
					t17 = t17 + t19;
					t20 = -c1*t17 + s1*t16;
					xoffset[j2] = t20;
					t20 = -c1*t16 - s1*t17;
					xoffset[j3] = t20;
					j2++; j3++;
				}
			}
			o0+=4;
			o2-=4;
		  }
		}
		currentLevel-=1;
		levelsToDo-=2;
	}
/* */
/* */
/* Pass 3 - Performs the last 3 levels of the IFFT */
/* */
/*     */

    sqrtHalf = expn[4].re;

    n2 = n>>1;
	n4 = n2>>1;
	b = n2>>2;

	a = startElement;
	offset1 = b + ((b&MASK)>>SHIFT);
	offset2 = n4 + ((n4&MASK)>>SHIFT);
	offset3 = (n4+b) + (((n4+b)&MASK)>>SHIFT);
	offset4 = n2 + ((n2&MASK)>>SHIFT);
	offset5 = n2+b + (((n2+b)&MASK)>>SHIFT);
	offset6 = n2+n4 + (((n2+n4)&MASK)>>SHIFT);
	offset7 = n2+n4+b + (((n2+n4+b)&MASK)>>SHIFT);
	for (j=a; j<a+b; j+=skip) {
		j4 = j + ((j&MASK)>>SHIFT);
		for (m=0; m<depth; m++) {
			t0 = x[j4];
			xoffset = &x[j4];
			t1 = xoffset[offset1];
			t2 = xoffset[offset2];
			t3 = xoffset[offset3];
			t16 = t0 + t1;
			t4 = xoffset[offset4];
			t17 = t0 - t1;
			t6 = xoffset[offset6];
			t2 += t2;
			t8 = t16 + t2;
			t5 = xoffset[offset5];
			t3 += t3;
			t10 = t16 - t2;
			t7 = xoffset[offset7];
			t4 += t4;
			t6 += t6;
			t9 = t17 - t3;
			t11 = t17 + t3;
			t5 += t5;
			t7 += t7;
			t20 = sqrtHalf*(t4 - t6);
			t21 = sqrtHalf*(t5 + t7);
			t12 = t4 + t6;
			x[j4] = t8 + t12;
			xoffset[offset4] = t8 - t12;
			t13 = t20 - t21;
			xoffset[offset1] = t9 + t13;
			xoffset[offset5] = t9 - t13;
			t14 = t7 - t5;
			xoffset[offset2] = t10 + t14;
			xoffset[offset6] = t10 - t14;
			t15 = t21 + t20;
			xoffset[offset3] = t11 - t15;
			xoffset[offset7] = t11 + t15;
			j4++;
		}
	}
}



/*
		void FFT_IFFT(
             BIG_DOUBLE *x <== data array,
             long n    <== size of data array (includes "padding" elements)
             long nn   <== log2 of data array size (not including
                           "padding" elements)
             long currentlevelsToDo   <== number of FFT levels to be performed
             long startElement <== offset from the start of the data array
                                   for the 1st element in this FFT
             long skip     <== number of data elements between data elements in
                               this FFT's dataset
             complex *expn <== sine/cosine data array
             )

	This routine is utilized in Pass 2 of the LL test.  It takes N complex
	values, performs the last "levelsToDo" levels of the FFT, squares these new
	complex values, and then performs the first "levelsToDo" levels of the
	inverse FFT.

	This implementation uses a radix-2 based FFT/IFFT and performs 1-level at
	a time.

	There is no data scrambling involved in this routine.  The data to be
	operated upon is located in consecutive locations of the data array *x.
	The arrangement is N real values followed by N imaginary values.  In this
	arrangement 512 data values can fit into a single data cache page.


*/


#ifdef __STDC__
void FFT_IFFT(BIG_DOUBLE *x, long n, long nn, long currentLevel, long levelsToDo, long startElement, long skip, complex *expn)
#else
void FFT_IFFT(x, n, nn, currentLevel, levelsToDo, startElement, skip, expn)
BIG_DOUBLE *x;
long n, nn, currentLevel, levelsToDo, startElement, skip;
complex *expn;
#endif
{
    register long a,e0,p0,j,j4,b,i,n2,loops,offset,offset1,offset2,startingLevel;
    register BIG_DOUBLE c, s, rtmp, itmp, rtmp1, itmp1, temp0,temp1,temp2,temp3;

    startingLevel = currentLevel;
    loops = 1;
    offset = 1<<levelsToDo;
    offset1 = offset + ((offset&MASK)>>SHIFT);
    n2 = n>>currentLevel;
    b = n2<<1;
    while (levelsToDo-->1) {
		for (i=0; i<loops; i++) {
			a = i*b+startElement;
			e0 = a >> (nn-currentLevel);
			e0 += (e0&MASK)>>SHIFT;
			c = expn[e0].re;
			s = expn[e0].im;
			offset2 = n2 + ((n2&MASK)>>SHIFT);
			for (j=a; j< a+n2; j+=skip) {
				j4 = j + ((j&MASK)>>SHIFT);
				p0 = j4 + offset2;
				temp0 = x[p0];
				temp1 = x[p0 + offset1];
				temp2 = x[j4];
				temp3 = x[j4 + offset1];
				rtmp = c*temp0 - s*temp1;
				itmp = s*temp0 + c*temp1;
				x[p0] = temp2 - rtmp;
				x[p0 + offset1] = temp3 - itmp;
				x[j4] = temp2 + rtmp;
				x[j4 + offset1] = temp3 + itmp;

			}
		}
		b = n2;
		n2>>=1;
		currentLevel++;
		loops<<=1;
	}

		for (i=0; i<loops; i++) {
			a = i*b+startElement;
			e0 = a >> (nn-currentLevel);
			e0 += (e0&MASK)>>SHIFT;
			c = expn[e0].re;
			s = expn[e0].im;
			offset2 = n2 + ((n2&MASK)>>SHIFT);
			for (j=a; j< a+n2; j+=skip) {
				j4 = j + ((j&MASK)>>SHIFT);
				p0 = j4 + offset2;

				/* FFT */

				temp0 = x[p0];
				temp1 = x[p0 + offset1];
				temp2 = x[j4];
				temp3 = x[j4 + offset1];
				rtmp = c*temp0 - s*temp1;
				itmp = s*temp0 + c*temp1;
				temp0 = temp2 + rtmp;
				temp1 = temp3 + itmp;
				temp2 = temp2 - rtmp;
				temp3 = temp3 - itmp;

				/* Square 'Em */

				rtmp = temp0*temp0 - temp1*temp1;
				temp1 = temp1*temp0;
				temp1 += temp1;
				temp0 = rtmp;

				rtmp = temp2*temp2 - temp3*temp3;
				temp3 = temp3*temp2;
				temp3 += temp3;
				temp2 = rtmp;


				/* IFFT */

				rtmp1 = temp0-temp2;
				itmp1 = temp1-temp3;
				rtmp = (c*rtmp1 + s*itmp1);
				itmp = (c*itmp1 - s*rtmp1);
				rtmp1 = temp0+temp2;
				itmp1 = temp1+temp3;
				x[j4] = rtmp1;
				x[j4+offset1] = itmp1;
				x[p0] = rtmp;
				x[p0+offset1] = itmp;


			}
		}




	b<<=1;
	n2<<=1;
	currentLevel--;
	loops>>=1;

    while (currentLevel>=startingLevel) {
		for (i=0; i<loops; i++) {
			a = i*b+startElement;
                        e0 = a >> (nn-currentLevel);
                        e0 += (e0&MASK)>>SHIFT;
                        c = expn[e0].re;
			s = expn[e0].im;
			offset2 = n2 + ((n2&MASK)>>SHIFT);
			for (j=a; j< a+n2; j+=skip) {
				j4 = j + ((j&MASK)>>SHIFT);
				p0 = j4+offset2;
				temp0 = x[j4];
				temp1 = x[p0];
				temp2 = x[j4+offset1];
				temp3 = x[p0+offset1];
				rtmp1 = temp0-temp1;
				itmp1 = temp2-temp3;
				rtmp = (c*rtmp1 + s*itmp1);
				itmp = (c*itmp1 - s*rtmp1);
				rtmp1 = temp0+temp1;
				itmp1 = temp2+temp3;
				x[j4] = rtmp1;
				x[j4+offset1] = itmp1;
				x[p0] = rtmp;
				x[p0+offset1] = itmp;
			}
		}
		b<<=1;
		n2<<=1;
		currentLevel--;
		loops>>=1;
	}
}



/*
		void FFT_IFFTR4(
             BIG_DOUBLE *x <== data array,
             long n    <== size of data array (includes "padding" elements)
             long nn   <== log2 of data array size (not including
                           "padding" elements)
             long currentLevel <== Starting FFT level
             long levelsToDo   <== number of FFT levels to be performed
             long startElement <== offset from the start of the data array
                                   for the 1st element in this FFT
             long skip     <== number of data elements between data elements in
                               this FFT's dataset
             complex *expn <== sine/cosine data array
             )

	This routine is utilized in Pass 2 of the LL test.  It takes N complex
	values, performs the last "levelsToDo" levels of the FFT, squares these new
	complex values, and then performs the first "levelsToDo" levels of the
	inverse FFT.

	This implementation uses a radix-4 based FFT/IFFT and performs 2-levels at
	a time - except for the last (first) level of the FFT (IFFT) if
	"levelsToDo" is odd.

	There is no data scrambling involved in this routine.  The data to be
	operated upon is located in consecutive locations of the data array *x.
	The arrangement is N real values followed by N imaginary values.  In this
	arrangement 512 data values can fit into a single data cache page.

	This routine trys to take advantage of the microprocessor's the ability
	to do multiple things at one time.  It assumes that integer, floating point,
	and load/store operations can all be performed simultaneously and
	independently.  I have also assumed that whatever compiler is used can
	perform the scheduling of these three sets of operations efficiently

	This routine is ~29% faster than the FFT_IFFT() routine on a PowerPC 604e
	processor.



*/


#ifdef __STDC__
void FFT_IFFTR4(BIG_DOUBLE *x, long n, long nn, long currentLevel, long levelsToDo, long startElement, long skip, complex *expn)
#else
void FFT_IFFTR4(x, n, nn, currentLevel, levelsToDo, startElement, skip, expn)
BIG_DOUBLE *x;
long n, nn, currentLevel, levelsToDo, startElement, skip;
complex *expn;
#endif
{
    register long 	a,e0,e1,j,j4,j5,p0,p1,j4New,j5New,p0New,p1New,b,i,n2,n4,
    				loops,offset,offset1,startingLevel;
    register BIG_DOUBLE c0, s0, c0New, s0New, c1, s1, c2, s2, c3, s3, ar, ai, br, bi, cr, ci, dr, di;
    register BIG_DOUBLE tt1,tt2,t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14;
    register BIG_DOUBLE t15, t16, t17, t18, t19, t20, t21, t22, t23, t24, t25, t26, t27, t28;

    startingLevel = currentLevel;
    loops = 1;
    offset = 1<<levelsToDo;
    offset1 = offset + ((offset&MASK)>>SHIFT);
    n2 = n>>currentLevel;
    n4 = n2>>1;
    b = n2<<1;
    while (levelsToDo>2) {
		for (i=0; i<loops; i++) {
			a = i*b+startElement;
			e0 = a >> (nn-currentLevel);
			e0 += (e0&MASK)>>SHIFT;
			c0 = expn[e0].re;
			s0 = expn[e0].im;
			e1 = a >> (nn-currentLevel-1);
			e1 += (e1&MASK)>>SHIFT;
			c2 = expn[e1].re;
			s2 = expn[e1].im;
			tt1 = c2*c0 - s2*s0;
			tt2 = c2*s0 + s2*c0;

			j4 = a + ((a&MASK)>>SHIFT);
			j5 = a + n4;
			j5 = j5 + ((j5&MASK)>>SHIFT);
			p0 = a + n2;
			p0 = p0 + ((p0&MASK)>>SHIFT);
			p1 = a + n2 + n4;
			p1 = p1 + ((p1&MASK)>>SHIFT);

			for (j=a; j< a+n4; ) {
				cr = x[j5];
				ci = x[j5 + offset1];
				j+=skip;
				j4New = j + ((j&MASK)>>SHIFT);
				j5New = j+n4;
				j5New = j5New + ((j5New&MASK)>>SHIFT);
				p0New = j+n2;
				p0New = p0New + ((p0New&MASK)>>SHIFT);
				p1New = j+n4+n2;
				p1New = p1New + ((p1New&MASK)>>SHIFT);
				t5 = c2*cr - s2*ci;
				t6 = s2*cr + c2*ci;
				br = x[p0];
				bi = x[p0 + offset1];
				t3 = c0*br - s0*bi;
				t4 = s0*br + c0*bi;
				dr = x[p1];
				di = x[p1 + offset1];
				t9 = tt1*dr - tt2*di;
				t10 = tt2*dr + tt1*di;
				t1 = x[j4]; /*ar */
				t2 = x[j4 + offset1];  /*ai */
				t13 = t1 + t3;
				t15 = t5 + t9;
				x[j4] = t13 + t15;
				x[j5] = t13 - t15;
				t14 = t2 + t4;
				t16 = t6 + t10;
				x[j4 + offset1] = t14 + t16;
				j4 = j4New;
				x[j5 + offset1] = t14 - t16;
				j5 = j5New;
				t17 = t1 - t3;
				t19 = t6 - t10;
				x[p0] = t17 + t19;
				x[p1] = t17 - t19;
				t18 = t2 - t4;
				t20 = t5 - t9; /* -(t8 - t12) */
				x[p0 + offset1] = t18 - t20;
				p0 = p0New;
				x[p1 + offset1] = t18 + t20;
				p1 = p1New;
			}

		}
		n2>>=2;
		b = n2<<1;
		currentLevel+=2;
		loops<<=2;
		levelsToDo-=2;
		n4>>=2;
	}

	if (levelsToDo==2) {
		for (i=0; i<loops; i++) {
			a = i*b+startElement;
			e0 = a >> (nn-currentLevel);
			e0 += (e0&MASK)>>SHIFT;
			c0 = expn[e0].re;
			s0 = expn[e0].im;
			e1 = a >> (nn-currentLevel-1);
			e1 += (e1&MASK)>>SHIFT;
			c2 = expn[e1].re;
			s2 = expn[e1].im;
			tt1 = c2*c0 - s2*s0;
			tt2 = c2*s0 + s2*c0;

			j4 = a + ((a&MASK)>>SHIFT);
			j5 = a + n4;
			j5 = j5 + ((j5&MASK)>>SHIFT);
			p0 = a + n2;
			p0 = p0 + ((p0&MASK)>>SHIFT);
			p1 = a + n2 + n4;
			p1 = p1 + ((p1&MASK)>>SHIFT);

			for (j=a; j< a+n4; ) {
				cr = x[j5];
				ci = x[j5 + offset1];
				j+=skip;
				j4New = j + ((j&MASK)>>SHIFT);
				j5New = j+n4;
				j5New = j5New + ((j5New&MASK)>>SHIFT);
				p0New = j+n2;
				p0New = p0New + ((p0New&MASK)>>SHIFT);
				p1New = j+n4+n2;
				p1New = p1New + ((p1New&MASK)>>SHIFT);
				t5 = c2*cr - s2*ci;
				t6 = s2*cr + c2*ci;
				br = x[p0];
				bi = x[p0 + offset1];
				t3 = c0*br - s0*bi;
				t4 = s0*br + c0*bi;
				dr = x[p1];
				di = x[p1 + offset1];
				t9 = tt1*dr - tt2*di;
				t10 = tt2*dr + tt1*di;
				t1 = x[j4]; /*ar */
				t2 = x[j4 + offset1];  /*ai */
				t13 = t1 + t3;
				t15 = t5 + t9;
				t21 = t13 + t15; /* ar'' */
				t22 = t13 - t15; /* cr'' */
				t14 = t2 + t4;
				t16 = t6 + t10;
				t23 = t14 + t16; /* ai'' */
				t24 = t14 - t16; /* ci'' */
				t17 = t1 - t3;
				t19 = t6 - t10;
				t25 = t17 + t19; /* br'' */
				t26 = t17 - t19; /* dr'' */
				t18 = t2 - t4;
				t20 = t5 - t9; /* -(t8 - t12) */
				t27 = t18 - t20; /* bi'' */
				t28 = t18 + t20; /* di'' */

				/* square 'em */

				ar = t21*t21 - t23*t23;
				ai = 2*t21*t23;

				br = t25*t25 - t27*t27;
				bi = 2*t25*t27;

				cr = t22*t22 - t24*t24;
				ci = 2*t22*t24;

				dr = t26*t26 - t28*t28;
				di = 2*t26*t28;

				/* ifft */

				t1 = ar + cr;
				t7 = ar - cr;
				t17 = c2*t7;
				t2 = br+dr;
				t9 = br - dr;
				t19 = s2*t9;
				t3 = ai + ci;
				t8 = ai - ci;
				t18 = c2*t8;
				t4 = bi + di;
				t10 = bi - di;
				t20 = s2*t10;
				x[j4] = t1 + t2;
				x[j4+offset1] = t3 + t4;
				j4 = j4New;
				t5 = t1 - t2;
				t6 = t3 - t4;
				x[p0] = c0*t5 + s0*t6;
				x[p0+offset1] = c0*t6 - s0*t5;
				p0 = p0New;
				t11 = t17 + s2*t8;
				t13 = t19 - c2*t10;
				x[j5] = t11 + t13;
				t14 = t20 + c2*t9;
				t12 = t18 - s2*t7;
				x[j5+offset1] = t12 + t14;
				j5 = j5New;
				t15 = t11 - t13;
				t16 = t12 - t14;
				x[p1] = c0*t15 + s0*t16;
				x[p1+offset1] = c0*t16 - s0*t15;
				p1 = p1New;
			}
		}
		b<<=2;
		n2<<=2;
		n4=n2>>1;
		currentLevel-=2;
		loops>>=2;
	}
	else { /* levelsToDo = 1 */
		a = startElement;
		e0 = a + ((a&MASK)>>SHIFT);
		c0 = expn[e0].re;
		s0 = expn[e0].im;
		j4 = e0;
		p0 = e0 + 1;
		for (i=0; i<loops; i++) {
			/* fft */

			t1 = x[p0]; /*br */
			t2 = x[p0 + offset1]; /*bi */
			t3 = x[j4]; /*ar */
			t4 = x[j4 + offset1]; /*ai */

			a += 2;
			e0 = a + ((a&MASK)>>SHIFT);
			j4New = e0;
			p0New = e0 + 1;
			c0New = expn[e0].re;
			s0New = expn[e0].im;

			t5 = c0*t1 - s0*t2;
			t6 = s0*t1 + c0*t2;

			t7 = t3 + t5; /* ar' */
			t8 = t3 - t5; /* br' */
			t9 = t4 + t6; /* ai' */
			t10 = t4 - t6; /* bi' */

			/* square 'em */

			ar = t7*t7 - t9*t9;
			ai = 2*t7*t9;

			br = t8*t8 - t10*t10;
			bi = 2*t8*t10;

			/* ifft */

			t1 = ar-br;
			t2 = ai-bi;
			x[p0] = (c0*t1 + s0*t2);
			x[p0+offset1] = (c0*t2 - s0*t1);

			c0 = c0New;
			s0 = s0New;
			p0 = p0New;

			x[j4] = ar + br;
			x[j4+offset1] = ai + bi;

			j4 = j4New;
		}
		b<<=2;
		n2<<=2;
		n4=n2>>1;
		currentLevel-=2;
		loops>>=2;
	}


    while (currentLevel>=startingLevel) {
		for (i=0; i<loops; i++) {
			a = i*b+startElement;
			e0 = a >> (nn-currentLevel-1);
			e0 += (e0&MASK)>>SHIFT;
			c2 = expn[e0].re;
			s2 = expn[e0].im;

			c3 = s2;
			s3 = -c2;

			e0 = a >> (nn-currentLevel);
			e0 += (e0&MASK)>>SHIFT;
			c0 = expn[e0].re;
			s0 = expn[e0].im;

			c1 = c0;
			s1 = s0;

			j4 = a + ((a&MASK)>>SHIFT);
			j5 = a + n4;
			j5 = j5 + ((j5&MASK)>>SHIFT);
			p0 = a + n2;
			p0 = p0 + ((p0&MASK)>>SHIFT);
			p1 = a + n2 + n4;
			p1 = p1 + ((p1&MASK)>>SHIFT);

			for (j=a; j< a+n4; ) {
				j+=skip;
				ar = x[j4];
				cr = x[j5];
				j4New = j + ((j&MASK)>>SHIFT);
				j5New = j+n4;
				j5New = j5New + ((j5New&MASK)>>SHIFT);
				p0New = j+n2;
				p0New = p0New + ((p0New&MASK)>>SHIFT);
				p1New = j+n4+n2;
				p1New = p1New + ((p1New&MASK)>>SHIFT);
				t1 = ar + cr;
				t7 = ar - cr;
				t17 = c2*t7;
				br = x[p0];
				dr = x[p1];
				t2 = br+dr;
				t9 = br - dr;
				t19 = c3*t9;
				ai = x[j4+offset1];
				ci = x[j5 + offset1];
				t3 = ai + ci;
				t8 = ai - ci;
				t18 = c2*t8;
				bi = x[p0+offset1];
				di = x[p1 + offset1];
				t4 = bi + di;
				t10 = bi - di;
				t20 = c3*t10;
				x[j4] = t1 + t2;
				x[j4+offset1] = t3 + t4;
				j4 = j4New;
				t5 = t1 - t2;
				t6 = t3 - t4;
				x[p0] = c0*t5 + s0*t6;
				x[p0+offset1] = c0*t6 - s0*t5;
				p0 = p0New;
				t11 = t17 + s2*t8;
				t13 = t19 + s3*t10;
				x[j5] = t11 + t13;
				t14 = t20 - s3*t9;
				t12 = t18 - s2*t7;
				x[j5+offset1] = t12 + t14;
				j5 = j5New;
				t15 = t11 - t13;
				t16 = t12 - t14;
				x[p1] = c1*t15 + s1*t16;
				x[p1+offset1] = c1*t16 - s1*t15;
				p1 = p1New;
			}
		}
		b<<=2;
		n2<<=2;
		n4<<=2;
		currentLevel-=2;
		loops>>=2;
	}



}



/*
		void FFT_IFFTR8(
             BIG_DOUBLE *x <== data array,
             long n    <== size of data array (includes "padding" elements)
             long nn   <== log2 of data array size (not including
                           "padding" elements)
             long currentLevel <== Starting FFT level
             long levelsToDo   <== number of FFT levels to be performed
             long startElement <== offset from the start of the data array
                                   for the 1st element in this FFT
             long skip     <== number of data elements between data elements in
                               this FFT's dataset
             complex *expn <== sine/cosine data array
             )

	This routine is utilized in Pass 2 of the LL test.  It takes N complex
	values, performs the last "levelsToDo" levels of the FFT, squares these new
	complex values, and then performs the first "levelsToDo" levels of the
	inverse FFT.

	This implementation uses a radix-8 based FFT/IFFT and performs 3-levels at
	a time - except for the last (first) level(s) of the FFT (IFFT) if
	"levelsToDo" is not a multiple of 3.

	There is no data scrambling involved in this routine.  The data to be
	operated upon is located in consecutive locations of the data array *x.
	The arrangement is N real values followed by N imaginary values.  In this
	arrangement 512 data values can fit into a single data cache page.

	Considerable optimization has been performed to take advantage of the natural
	symmetry of the FFT "Twiddle" factors (sin/cos values).  The main point
	to this is that it is faster to calculate these values than it is to read
	them from memory.  I discovered these symmetries by observation, but I'm
	sure that they would be obvious from an examination of the mathematical
	representation of the FFT.

	This routine trys to take advantage of the microprocessor's the ability
	to do multiple things at one time.  It assumes that integer, floating point,
	and load/store operations can all be performed simultaneously and
	independently.  I have also assumed that whatever compiler is used can
	perform the scheduling of these three sets of operations efficiently

	This routine is ~56% faster than the FFT_IFFT() routine on a PowerPC 604e
	processor.


*/

#ifdef __STDC__
void FFT_IFFTR8(BIG_DOUBLE *x, long n, long nn, long currentLevel, long levelsToDo, long startElement, long skip, complex *expn)
#else
void FFT_IFFTR8(x, n, nn, currentLevel, levelsToDo, startElement, skip, expn)
BIG_DOUBLE *x;
long n, nn, currentLevel, levelsToDo, startElement, skip;
complex *expn;
#endif
{
    register long 	a,p0,p2,j,j4,j2,b,i,n2,n4,n8,jshift,
    								loops,offset,startingLevel;
    register long	j5,j3,p1,p3,flag;
    register BIG_DOUBLE c0, s0, c1, s1, c3, s3, c5, s5,tt1,tt2;
    BIG_DOUBLE sqrtHalf,c8,s8;
    register BIG_DOUBLE ar, ai, br, bi, cr, ci, dr, di, er, ei, fr, fi, gr, gi, hr, hi;
    register BIG_DOUBLE t1r,t1i,t2r, t2i, t3r, t3i,*xoffset;

    sqrtHalf = expn[4].re;
    c8 = expn[8].re;
    s8 = expn[8].im;
    flag = 0;
    startingLevel = currentLevel;
    loops = 1;
    offset = 1<<levelsToDo;
    offset = offset + ((offset&MASK)>>SHIFT);
    xoffset = &x[offset];
    n2 = n>>currentLevel;
    n4 = n2>>1;
    n8 = n2>>2;
    b = n2<<1;
    while (levelsToDo>3) {
		flag = 0;
		for (i=0; i<loops; i++) {
			a = i*b+startElement;
			nn -= currentLevel;
			nn -=2;
			if (flag==0) {
				p0 = a >> nn;
				p0 += (p0&MASK)>>SHIFT;
				c3 = expn[p0].re;
				s3 = expn[p0].im;
				flag = 1;
			}
			else {
				flag = 0;
				c1 = c3*c8;
				s1 = c3*s8;
				c3 = c1 - s3*s8;
				s3 = s1 + s3*c8;
			}
			s5 = sqrtHalf*s3;
			c1 = c3*c3;
			s1 = c3*s3;
			c5 = sqrtHalf*c3 + s5;
			s5 -= sqrtHalf*c3;
			c1 = c1 - s3*s3;
			s1 += s1;
			c0 = c1*c1;
			s0 = c1*s1;
			c0 -= s1*s1;
			s0 += s0;
			tt1 = c1*c0;
			tt2 = c1*s0;
			tt1 -= s1*s0;
			tt2 += s1*c0;
			nn += 2;
			nn += currentLevel;
			jshift = ((a&MASK)>>SHIFT);
			j4 = a + jshift;
			j2 = a + n4;
			jshift = ((j2&MASK)>>SHIFT);
			j2 += jshift;
			p0 = a + n2;
			jshift = (p0&MASK)>>SHIFT;
			p0 += jshift;
			p2 = a + n2 + n4;
			jshift = (p2&MASK)>>SHIFT;
			p2 += jshift;

			for (j=a; j< a+n8;) {
				cr = x[j2];
				ci = xoffset[j2];
				j2 = j + n4 + n8;
				jshift = ((j2&MASK)>>SHIFT); /* j3 */
				j2 += jshift; /* j3 */
				gr = x[p2];
				gi = xoffset[p2];
				p2 = j + n2 + n4 + n8;
				jshift = (p2&MASK)>>SHIFT; /* p3 */
				p2 += jshift; /* p3 */
				er = x[p0];
				ei = xoffset[p0];
				p0 = j + n2 + n8;
				jshift = (p0&MASK)>>SHIFT; /* p1 */
				p0 += jshift; /* p1 */
				ar = x[j4];
				ai = xoffset[j4];
				j4 = j + n8;
				jshift = ((j4&MASK)>>SHIFT); /* j5 */
				j4 += jshift; /* j5 */
				t1r = c1*cr;
				t3r = tt1*gr;
				t1i = s1*cr;
				t3i = tt2*gr;
				t1r -= s1*ci;
				t3r -= tt2*gi;
				t1i += c1*ci;
				t3i += tt1*gi;
				t2i = t3r - t1r;
				t2r = t1i - t3i;
				t1r += t3r;
				t1i += t3i;
				t3r = c0*er;
				t3i = s0*er;
				t3r -= s0*ei;
				t3i += c0*ei;
				t3r = ar-t3r;
				t3i = ai-t3i;

				er = t3r + t2r;
				ei = t3i + t2i;
				gr = t3r - t2r;
				gi = t3i - t2i;

				t3r = ar-t3r; /* 2*ar - t3r */
				t3i = ai-t3i; /* 2*ai-t3i; */
				t3r += ar;
				t3i += ai;

				ar = t3r + t1r;
				cr = t3r - t1r;
				ai = t3i + t1i;
				ci = t3i - t1i;

				hr = x[p2]; /* p3 */
				hi = xoffset[p2];
				dr = x[j2]; /* j3 */
				di = xoffset[j2];
				fr = x[p0]; /* p1 */
				fi = xoffset[p0];
				br = x[j4]; /* j5 */
				bi = xoffset[j4];
				t3r = tt1*hr;
				t1r = c1*dr;
				t3i = tt2*hr;
				t1i = s1*dr;
				t3r -= tt2*hi;
				t1r -= s1*di;
				t3i += tt1*hi;
				t1i += c1*di;
				t2i = t3r - t1r;
				t2r = t1i - t3i;
				t1r += t3r;
				t1i += t3i;
				t3r = c0*fr;
				t3i = s0*fr;
				t3r -= s0*fi;
				t3i += c0*fi;
				t3r = br-t3r;
				t3i = bi-t3i;

				fr = t3r + t2r;
				fi = t3i + t2i;
				hr = t3r - t2r;
				hi = t3i - t2i;

				t3r = br-t3r;
				t3i = bi-t3i;
				t3r += br;
				t3i += bi;

				br = t3r + t1r;
				bi = t3i + t1i;
				dr = t3r - t1r;
				di = t3i - t1i;

				t3r = c3*br;
				t3i = s3*br;
				t3r -= s3*bi;
				t3i += c3*bi;

				br = ar - t3r;
				bi = ai - t3i;

				x[j4] = br; /* j5 */
				xoffset[j4] = bi;
				jshift = ((j&MASK)>>SHIFT);
				j4 = j + jshift;
				t2r = s3*dr;
				t2i = c3*dr;
				t2r += c3*di;
				t2i -= s3*di;
				dr = cr - t2r;
				di = ci + t2i;
				x[j2] = dr; /* j3 */
				xoffset[j2] = di;
				j2 = j + n4;
				jshift = ((j2&MASK)>>SHIFT);
				j2 += jshift;
				ar += t3r;
				ai += t3i;
				x[j4] = ar; /* j4 */
				xoffset[j4] = ai;
				j4 = j + skip;
				jshift = ((j4&MASK)>>SHIFT);
				j4 += jshift;
				cr += t2r;
				ci -= t2i;
				x[j2] = cr; /* j2 */
				xoffset[j2] = ci;
				j2 = j + skip + n4;
				jshift = ((j2&MASK)>>SHIFT);
				j2 += jshift;
				t1r = c5*fr;
				t1i = s5*fr;
				t1r -= s5*fi;
				t1i += c5*fi;
				fr = er - t1r;
				fi = ei - t1i;
				x[p0] = fr; /* p1 */
				xoffset[p0] = fi;
				p0 = j + n2;
				jshift = ((p0&MASK)>>SHIFT);
				p0 += jshift;
				t3r = s5*hr;
				t3i = c5*hr;
				t3r += c5*hi;
				t3i -= s5*hi;
				hr = gr - t3r;
				hi = gi + t3i;
				x[p2] = hr; /* p3 */
				xoffset[p2] = hi;
				p2 = j + n2 + n4;
				jshift = ((p2&MASK)>>SHIFT);
				p2 += jshift;
				j += skip;
				er += t1r;
				ei += t1i;
				x[p0] = er; /* p0 */
				xoffset[p0] = ei;
				p0 = j + n2;
				jshift = ((p0&MASK)>>SHIFT);
				p0 += jshift;
				gr += t3r;
				gi -= t3i;
				x[p2] = gr; /* p2 */
				xoffset[p2] = gi;
				p2 = j + n2 + n4;
				jshift = ((p2&MASK)>>SHIFT);
				p2 += jshift;
			}
		}
		n2>>=3;
		b = n2<<1;
		currentLevel+=3;
		loops<<=3;
		levelsToDo-=3;
		n4 = n2>>1;
		n8 = n2>>2;
	}
	if (levelsToDo==3) {
		flag = 0;
		for (i=0; i<loops; i++) {
			a = i*b+startElement;
			nn -= currentLevel;
			nn -=2;
			if (flag==0) {
				p0 = a >> nn;
				p0 += (p0&MASK)>>SHIFT;
				c3 = expn[p0].re;
				s3 = expn[p0].im;
				flag = 1;
			}
			else {
				flag = 0;
				c1 = c3*c8 - s3*s8;
				s3 = s3*c8 + c3*s8;
				c3 = c1;
			}
			s5 = sqrtHalf*s3;
			c5 = sqrtHalf*c3 + s5;
			s5 -= sqrtHalf*c3;
			c1 = c3*c3 - s3*s3;
			s1 = c3*s3;
			s1 += s1;
			c0 = c1*c1 - s1*s1;
			s0 = c1*s1;
			s0 += s0;
			tt1 = c1*c0 - s1*s0;
			tt2 = c1*s0 + s1*c0;
			nn += 2;
			nn += currentLevel;
			jshift = ((a&MASK)>>SHIFT);
			j4 = a + jshift;
			j2 = a + n4;
			jshift = ((j2&MASK)>>SHIFT);
			j2 = j2 + jshift;
			p0 = a + n2;
			jshift = (p0&MASK)>>SHIFT;
			p0 += jshift;
			p2 = a + n2 + n4;
			jshift= (p2&MASK)>>SHIFT;
			p2 += jshift;

			for (j=a; j< a+n8; ) {
				cr = x[j2];
				ci = xoffset[j2];
				j2 = j + n4 + n8;
				jshift = ((j2&MASK)>>SHIFT); /* j3 */
				j2 += jshift; /* j3 */
				gr = x[p2];
				gi = xoffset[p2];
				p2 = j + n2 + n4 + n8;
				jshift = (p2&MASK)>>SHIFT; /* p3 */
				p2 += jshift; /* p3 */
				t1r = c1*cr;
				t1i = s1*cr;
				t1r -= s1*ci;
				t1i += c1*ci;
				er = x[p0];
				ei = xoffset[p0];
				p0 = j + n2 + n8;
				jshift = (p0&MASK)>>SHIFT; /* p1 */
				p0 += jshift; /* p1 */
				t3r = tt1*gr;
				t3i = tt2*gr;
				t3r -= tt2*gi;
				t3i += tt1*gi;
				t2r = t1i - t3i;
				t2i = t3r - t1r;
				ar = x[j4];
				ai = xoffset[j4];
				j4 = j + n8;
				jshift = ((j4&MASK)>>SHIFT); /* j5 */
				j4 += jshift; /* j5 */
				t1r += t3r;
				t1i += t3i;
				t3r = c0*er;
				t3i = s0*er;
				t3r -= s0*ei;
				t3i += c0*ei;
				t3r = ar-t3r;
				t3i = ai-t3i;
				dr = x[j2]; /* j3 */
				di = xoffset[j2];
				er = t3r + t2r;
				ei = t3i + t2i;
				gr = t3r - t2r;
				gi = t3i - t2i;


				t3r = ar-t3r;
				t3r += ar;
				t3i = ai-t3i;
				t3i += ai;
				hr = x[p2]; /* p3 */
				hi = xoffset[p2];
				ar = t3r + t1r;
				ai = t3i + t1i;
				cr = t3r - t1r;
				ci = t3i - t1i;
				fr = x[p0]; /* p1 */
				fi = xoffset[p0];
				t1r = c1*dr;
				t3r = tt1*hr;
				t1i = s1*dr;
				t3i = tt2*hr;
				t1r -= s1*di;
				t3r -= tt2*hi;
				t1i += c1*di;
				t3i += tt1*hi;
				t2i = t3r - t1r;
				t2r = t1i - t3i;

				br = x[j4]; /* j5 */
				bi = xoffset[j4];

				t1r += t3r;
				t1i += t3i;
				t3r = c0*fr;
				t3i = s0*fr;
				t3r -= s0*fi;
				t3i += c0*fi;
				t3r = br-t3r;
				t3i = bi-t3i;

				fr = t3r + t2r;
				fi = t3i + t2i;
				hr = t3r - t2r;
				hi = t3i - t2i;

				t3r = br-t3r;
				t3r += br;
				t3i = bi-t3i;
				t3i += bi;
				br = t3r + t1r;
				bi = t3i + t1i;
				dr = t3r - t1r;
				di = t3i - t1i;
				t3r = c3*br;
				t3i = s3*br;
				t3r -= s3*bi;
				t3i += c3*bi;
				br = ar - t3r;
				bi = ai - t3i;
				ar += t3r;
				ai += t3i;
				t3r = s3*dr;
				t3i = c3*dr;
				t3r += c3*di;
				t3i -= s3*di;
				dr = cr + t3r;
				di = ci - t3i;
				cr -= t3r;
				ci += t3i;
				t3r = c5*fr;
				t3i = s5*fr;
				t3r -= s5*fi;
				t3i += c5*fi;
				fr = er - t3r;
				fi = ei - t3i;
				er += t3r;
				ei += t3i;
				t3r = s5*hr;
				t3i = c5*hr;
				t3r += c5*hi;
				t3i -= s5*hi;
				hr = gr + t3r;
				hi = gi - t3i;
				gr -= t3r;
				gi += t3i;

				/* square 'em */

				t3i = ar*ai;
				t3i += t3i;
				ar = ar*ar - ai*ai;
				ai = t3i;

				t3i = br*bi;
				t3i += t3i;
				br = br*br - bi*bi;
				bi = t3i;

				t3i = cr*ci;
				t3i += t3i;
				cr = cr*cr - ci*ci;
				ci = t3i;

				t3i = dr*di;
				t3i += t3i;
				dr = dr*dr - di*di;
				di = t3i;

				t3i = er*ei;
				t3i += t3i;
				er = er*er - ei*ei;
				ei = t3i;

				t3i = fr*fi;
				t3i += t3i;
				fr = fr*fr - fi*fi;
				fi = t3i;

				t3i = gr*gi;
				t3i += t3i;
				gr = gr*gr - gi*gi;
				gi = t3i;

				t3i = hr*hi;
				t3i += t3i;
				hr = hr*hr - hi*hi;
				hi = t3i;

				/* ifft */

				t3r = ar - br;
				t3i = ai - bi;
				ar += br;
				ai += bi;
				br = c3*t3r;
				bi = c3*t3i;
				t2r = dr - cr;
				t2i = ci - di;
				br += s3*t3i;
				bi -= s3*t3r;


				cr += dr;
				ci += di;
				dr = s3*t2r;
				di = c3*t2r;
				t3r = er - fr;
				t3i = ei - fi;
				dr += c3*t2i;
				di -= s3*t2i;

				er += fr;
				ei += fi;
				fr = c5*t3r;
				fi = c5*t3i;
				t2r = hr - gr;
				t2i = gi - hi;
				fr += s5*t3i;
				fi -= s5*t3r;


				gr += hr;
				gi += hi;
				hr = s5*t2r;
				hi = c5*t2r;
				t1r = br + dr;
				t1i = bi + di;
				hr += c5*t2i;
				hi -= s5*t2i;


				t3r = br - dr;
				t3i = bi - di;

				t2r = fr + hr;
				t2i = fi + hi;


				br = t1r + t2r; /*br */
				bi = t1i + t2i; /*bi */
				x[j4] = br; /* j5 */
				xoffset[j4] = bi;
				j4 = j;
				jshift = ((j4&MASK)>>SHIFT);
				j4 += jshift;
				t1r = t1r - t2r;
				t1i = t1i - t2i;
				t2r = c0*t1r; /*fr */
				t2i = c0*t1i; /*fi */
				t2r += s0*t1i; /*fr */
				t2i -= s0*t1r; /*fi */
				x[p0] = t2r; /* p1 */
				xoffset[p0] = t2i;
				p0 = j + n2;
				jshift = ((p0&MASK)>>SHIFT);
				p0 += jshift;
				t1r = fr - hr;
				t1i = fi - hi;
				t2r = t3r - t1i;
				t2i = t3i + t1r;
				dr = c1*t2r; /*dr */
				di = c1*t2i; /*di */
				dr += s1*t2i; /*dr */
				di -= s1*t2r; /*di */
				x[j2] = dr; /* j3 */
				xoffset[j2] = di;
				j2 = j + n4;
				jshift = ((j2&MASK)>>SHIFT);
				j2 += jshift;
				t2r = t3r + t1i;
				t2i = t3i - t1r;
				hr = tt1*t2r; /*hr */
				hi = tt1*t2i; /*hi */
				hr += tt2*t2i; /*hr */
				hi -= tt2*t2r; /*hi */
				x[p2] = hr; /* p3 */
				xoffset[p2] = hi;
				p2 = j + n2 + n4;
				jshift = ((p2&MASK)>>SHIFT);
				p2 += jshift;
				j += skip;

				t1r = ar + cr;
				t1i = ai + ci;

				t3r = ar - cr;
				t3i = ai - ci;

				t2r = er + gr;
				t2i = ei + gi;

				ar = t1r + t2r; /*ar */
				ai = t1i + t2i; /*ai */
				x[j4] = ar; /* j4 */
				xoffset[j4] = ai;
				j4 = j;
				jshift = ((j4&MASK)>>SHIFT);
				j4 += jshift;
				t1r = t1r - t2r;
				t1i = t1i - t2i;
				t2r = c0*t1r; /*er */
				t2i = c0*t1i; /*ei */
				t2r += s0*t1i; /*er */
				t2i -= s0*t1r; /*ei */
				x[p0] = t2r; /*er // p0 */
				xoffset[p0] = t2i; /*ei */
				t1r = er - gr;
				t1i = ei - gi;

				p0 = j + n2;
				jshift = ((p0&MASK)>>SHIFT);
				p0 += jshift;
				t2r = t3r - t1i;
				t2i = t3i + t1r;
				cr = c1*t2r; /*cr */
				ci = c1*t2i; /*ci */
				cr += s1*t2i; /*cr */
				ci -= s1*t2r; /*ci */
				x[j2] = cr; /* j2 */
				xoffset[j2] = ci;
				j2 = j + n4;
				jshift = ((j2&MASK)>>SHIFT);
				j2 += jshift;
				t2r = t3r + t1i;
				t2i = t3i - t1r;
				gr = tt1*t2r; /*gr */
				gi = tt1*t2i; /*gi */
				gr += tt2*t2i; /*gr */
				gi -= tt2*t2r; /*gi */
				x[p2] = gr;  /* p2 */
				xoffset[p2] = gi;
				p2 = j + n2 + n4;
				jshift = ((p2&MASK)>>SHIFT);
				p2 += jshift;

			}

		}
		n2<<=3;
		b = n2<<1;
		currentLevel-=3;
		loops>>=3;
		n4 = n2>>1;
		n8 = n2>>2;
	}
	else if (levelsToDo==2) {

	/* gr,gi,hr,hi are used as temporary variables in this section */

		flag = 0;
		for (i=0; i<loops; i++) {
			a = i*b+startElement;
			if (flag==0) {
				nn -= currentLevel;
				nn--;
				p0 = a >> nn;
				p0 += (p0&MASK)>>SHIFT;
				c1 = expn[p0].re;
				nn++;
				nn += currentLevel;
				s1 = expn[p0].im;
				flag = 1;
			}
			else {
				flag = 0;
				c0 = sqrtHalf*c1 + sqrtHalf*s1;
				s1 = sqrtHalf*s1 - sqrtHalf*c1;
				c1 = c0;
			}
			c0 = c1*c1;
			s0 = c1*s1;
			c0 -= s1*s1;
			s0 += s0;
			tt1 = c1*c0;
			tt2 = c1*s0;
			tt1 -= s1*s0;
			tt2 += s1*c0;
			j4 = a + ((a&MASK)>>SHIFT);
			j2 = a + n4;
			j2 += ((j2&MASK)>>SHIFT);
			p0 = a + n2;
			p0 += (p0&MASK)>>SHIFT;
			p2 = a + n2 + n4;
			p2 += (p2&MASK)>>SHIFT;

			for (j=a; j< a+n4; ) {
				cr = x[j2];
				ci = xoffset[j2];
				j+=skip;
				t1r = c1*cr;
				t1i = s1*cr;
				t1r -= s1*ci;
				t1i += c1*ci;
				br = x[p0];
				bi = xoffset[p0];
				t2r = c0*br;
				t2i = s0*br;
				t2r -= s0*bi;
				t2i += c0*bi;
				dr = x[p2];
				di = xoffset[p2];
				t3r = tt1*dr;
				t3i = tt2*dr;
				t3r -= tt2*di;
				t3i += tt1*di;
				ar = x[j4]; /*ar */
				ai = xoffset[j4];  /*ai */
				hr = ar - t2r;
				hi = t1i - t3i;
				br = hr + hi; /* br'' */
				dr = hr - hi; /* dr'' */
				hi = ai - t2i;
				hr = t1r - t3r; /* -(t8 - t12) */
				bi = hi - hr; /* bi'' */
				di = hi + hr; /* di'' */
				gr = ar + t2r;
				hr = t1r + t3r;
				ar = gr + hr; /* ar'' */
				cr = gr - hr; /* cr'' */
				gi = ai + t2i;
				hi = t1i + t3i;
				ai = gi + hi; /* ai'' */
				ci = gi - hi; /* ci'' */

				/* square 'em */

				hr = ar*ar - ai*ai;
				ai = ar*ai;
				ai += ai;
				ar = hr;

				hr = br*br - bi*bi;
				bi = br*bi;
				bi += bi;
				br = hr;

				hr = cr*cr - ci*ci;
				ci = cr*ci;
				ci += ci;
				cr = hr;

				hr = dr*dr - di*di;
				di = dr*di;
				di += di;
				dr = hr;

				/* ifft */

				t1r = ar + cr;
				t2r = ar - cr;
				t3r = br + dr;
				gr = br - dr;
				t1i = ai + ci;
				t2i = ai - ci;
				t3i = bi + di;
				gi = bi - di;
				x[j4] = t1r + t3r;
				x[j4+offset] = t1i + t3i;
				j4 = j + ((j&MASK)>>SHIFT);
				hr = t1r - t3r;
				hi = t1i - t3i;
				x[p0] = c0*hr + s0*hi;
				x[p0+offset] = c0*hi - s0*hr;
				p0 = j + n2;
				p0 += (p0&MASK)>>SHIFT;
				t1r = c1*t2r + s1*t2i;
				t3r = s1*gr - c1*gi;
				x[j2] = t1r + t3r;
				t1i = s1*gi + c1*gr;
				t3i = c1*t2i - s1*t2r;
				x[j2+offset] = t3i + t1i;
				j2 = j + n4;
				j2 += ((j2&MASK)>>SHIFT);
				hr = t1r - t3r;
				hi = t3i - t1i;
				x[p2] = c0*hr + s0*hi;
				x[p2+offset] = c0*hi - s0*hr;
				p2 = j + n2 + n4;
				p2 += (p2&MASK)>>SHIFT;
			}
		}
		n2<<=3;
		b = n2<<1;
		n4=n2>>1;
		n8 = n2>>2;
		currentLevel-=3;
		loops>>=3;
	}
	else { /* levelsToDo = 1 */

	/* hr,hi are used as temporary variables in this section */

		tt1 = expn[8].re;
		tt2 = expn[8].im;
		flag = 0;
		for (i=0; i<loops; i+=4) {
			a = i*2+startElement;
			j4 = a + ((a&MASK)>>SHIFT);
			if (flag==0) {
				c0 = expn[j4].re;
				s0 = expn[j4].im;
				c1 = expn[j4+2].re;
				s1 = expn[j4+2].im;
				flag = 1;
			}
			else {
				flag = 0;
				c3 = c0*tt1 - s0*tt2;
				s0 = s0*tt1 + c0*tt2;
				c0 = c3;
				c3 = c1*tt1 - s1*tt2;
				s1 = s1*tt1 + c1*tt2;
				c1 = c3;
			}
			s3 = sqrtHalf*s0;
			c3 = sqrtHalf*c0 + s3;
			s3 -= sqrtHalf*c0;
			s5 = sqrtHalf*s1;
			c5 = sqrtHalf*c1 + s5;
			s5 -= sqrtHalf*c1;
			p0 = j4 + 1;
			j5 = j4 + 2;
			p1 = j4 + 3;
			j2 = j4 + 4;
			p2 = j4 + 5;
			j3 = j4 + 6;
			p3 = j4 + 7;


			/* fft */

			br = x[p0]; /*br */
			bi = xoffset[p0]; /*bi */
			ar = x[j4]; /*ar */
			ai = xoffset[j4]; /*ai */

			dr = x[p1]; /*dr */
			di = xoffset[p1]; /*di */
			cr = x[j5]; /*cr */
			ci = xoffset[j5]; /*ci */
			t1r = c0*br - s0*bi;
			t1i = s0*br + c0*bi;

			br = ar - t1r; /* br' */
			ar = ar + t1r; /* ar' */
			bi = ai - t1i; /* bi' */
			ai = ai + t1i; /* ai' */

			/* square 'em */

			t1r = ar*ar - ai*ai;
			ai = ar*ai;
			ai += ai;
			ar = t1r;

			t1i = br*br - bi*bi;
			bi = br*bi;
			bi += bi;
			br = t1i;

			fr = x[p2]; /*br */
			fi = xoffset[p2]; /*bi */
			er = x[j2]; /*ar */
			ei = xoffset[j2]; /*ai */
			t1r = c1*dr - s1*di;
			t1i = s1*dr + c1*di;

			dr = cr - t1r; /* dr' */
			cr = cr + t1r; /* cr' */
			di = ci - t1i; /* di' */
			ci = ci + t1i; /* ci' */

			/* square 'em */

			t1r = cr*cr - ci*ci;
			ci = cr*ci;
			ci += ci;
			cr = t1r;

			t1i = dr*dr - di*di;
			di = dr*di;
			di += di;
			dr = t1i;

			hr = x[p3]; /*fr */
			hi = xoffset[p3]; /*fi */
			gr = x[j3]; /*er */
			gi = xoffset[j3]; /*ei */
			t1r = c3*fr - s3*fi;
			t1i = s3*fr + c3*fi;

			fr = er - t1r; /* fr' */
			er = er + t1r; /* er' */
			fi = ei - t1i; /* fi' */
			ei = ei + t1i; /* ei' */

			/* square 'em */

			t1r = er*er - ei*ei;
			ei = er*ei;
			ei += ei;
			er = t1r;

			t1i = fr*fr - fi*fi;
			fi = fr*fi;
			fi += fi;
			fr = t1i;

			t1r = c5*hr - s5*hi;
			t1i = s5*hr + c5*hi;

			hr = gr - t1r; /* hr' */
			gr = gr + t1r; /* gr' */
			hi = gi - t1i; /* hi' */
			gi = gi + t1i; /* gi' */

			/* square 'em */

			t1r = gr*gr - gi*gi;
			gi = gr*gi;
			gi += gi;
			gr = t1r;

			t1i = hr*hr - hi*hi;
			hi = hr*hi;
			hi += hi;
			hr = t1i;

			/* ifft */

			t1r = ar-br;
			t1i = ai-bi;
			x[j4] = ar + br;
			xoffset[j4] = ai + bi;
			x[p0] = (c0*t1r + s0*t1i);
			xoffset[p0] = (c0*t1i - s0*t1r);
			t1r = cr-dr;
			t1i = ci-di;
			x[j5] = cr + dr;
			xoffset[j5] = ci + di;
			x[p1] = (c1*t1r + s1*t1i);
			xoffset[p1] = (c1*t1i - s1*t1r);
			t1r = er-fr;
			t1i = ei-fi;
			x[j2] = er + fr;
			xoffset[j2] = ei + fi;
			x[p2] = (c3*t1r + s3*t1i);
			xoffset[p2] = (c3*t1i - s3*t1r);
			t1r = gr-hr;
			t1i = gi-hi;
			x[j3] = gr + hr;
			xoffset[j3] = gi + hi;
			x[p3] = (c5*t1r + s5*t1i);
			xoffset[p3] = (c5*t1i - s5*t1r);
		}
		n2<<=3;
		b = n2<<1;
		n4=n2>>1;
		n8 = n2>>2;
		currentLevel-=3;
		loops>>=3;
	}


    while (currentLevel>=startingLevel) {
		flag = 0;
		for (i=0; i<loops; i++) {
			a = i*b+startElement;
			nn -= currentLevel;
			nn -=2;
			if (flag==0) {
				p0 = a >> nn;
				p0 += (p0&MASK)>>SHIFT;
				c3 = expn[p0].re;
				s3 = expn[p0].im;
				flag = 1;
			}
			else {
				flag = 0;
				c1 = c3*c8 - s3*s8;
				s3 = s3*c8 + c3*s8;
				c3 = c1;
			}
			s5 = sqrtHalf*s3;
			c5 = sqrtHalf*c3 + s5;
			s5 -= sqrtHalf*c3;
			c1 = c3*c3 - s3*s3;
			s1 = c3*s3;
			s1 += s1;
			c0 = c1*c1 - s1*s1;
			s0 = c1*s1;
			s0 += s0;
			tt1 = c1*c0 - s1*s0;
			tt2 = c1*s0 + s1*c0;
			nn += 2;
			nn += currentLevel;
			j4 = a + ((a&MASK)>>SHIFT);
			j2 = a + n4;
			j2 = j2 + ((j2&MASK)>>SHIFT);
			p0 = a + n2;
			p0 += (p0&MASK)>>SHIFT;
			p2 = a + n2 + n4;
			p2 += (p2&MASK)>>SHIFT;

			for (j=a; j< a+n8; ) {

				/* ifft */

				ar = x[j4];
				ai = xoffset[j4];
				j4 = j + n8; /* j5 */
				j4 += ((j4&MASK)>>SHIFT);

				cr = x[j2];
				ci = xoffset[j2];
				j2 = j + n4 + n8; /* j3 */
				j2 += ((j2&MASK)>>SHIFT);


				er = x[p0];
				ei = xoffset[p0];
				p0 = j + n2 + n8; /* p1 */
				p0 += (p0&MASK)>>SHIFT;

				gr = x[p2];
				gi = xoffset[p2];
				p2 = j + n2 + n4 + n8; /* p3 */
				p2 += (p2&MASK)>>SHIFT;

				br = x[j4]; /* j5 */
				bi = xoffset[j4];

				dr = x[j2]; /* j3 */
				di = xoffset[j2];

				fr = x[p0]; /* p1 */
				fi = xoffset[p0];

				hr = x[p2]; /*p3 */
				hi = xoffset[p2];

				t3r = ar - br;
				t3i = ai - bi;
				ar += br;
				ai += bi;
				br = c3*t3r + s3*t3i;
				bi = c3*t3i - s3*t3r;


				t3r = cr - dr;
				t3i = di - ci;
				cr += dr;
				ci += di;
				dr = s3*t3r + c3*t3i;
				di = c3*t3r - s3*t3i;

				t3r = er - fr;
				t3i = ei - fi;
				er += fr;
				ei += fi;
				fr = c5*t3r + s5*t3i;
				fi = c5*t3i - s5*t3r;


				t3r = gr - hr;
				t3i = hi - gi;
				gr += hr;
				gi += hi;
				hr = s5*t3r + c5*t3i;
				hi = c5*t3r - s5*t3i;


				t1r = br + dr;
				t1i = bi + di;

				t3r = br - dr;
				t3i = bi - di;

				t2r = fr + hr;
				t2i = fi + hi;

/*				t4r = fr - hr; */
/*				t4i = fi - hi; */

				br = t1r + t2r; /*br */
				bi = t1i + t2i; /*bi */
				x[j4] = br; /* j5 */
				xoffset[j4] = bi;
				j4 = j;
				j4 += ((j4&MASK)>>SHIFT);
				t1r = t1r - t2r;
				t1i = t1i - t2i;
				t2r = c0*t1r + s0*t1i; /*fr */
				t2i = c0*t1i - s0*t1r; /*fi */
				x[p0] = t2r; /*fr // p1 */
				xoffset[p0] = t2i; /*fi */
				p0 = j + n2;
				p0 += ((p0&MASK)>>SHIFT);
				t1r = fr - hr;
				t1i = fi - hi;
				t2r = t3r - t1i;
				t2i = t3i + t1r;
				dr = c1*t2r + s1*t2i; /*dr */
				di = c1*t2i - s1*t2r; /*di */
				x[j2] = dr; /* j3 */
				xoffset[j2] = di;
				j2 = j + n4;
				j2 += ((j2&MASK)>>SHIFT);
				t2r = t3r + t1i;
				t2i = t3i - t1r;
				hr = tt1*t2r + tt2*t2i; /*hr */
				hi = tt1*t2i - tt2*t2r; /*hi */
				x[p2] = hr; /* p3 */
				xoffset[p2] = hi;
				p2 = j + n2 + n4;
				p2 += ((p2&MASK)>>SHIFT);
				j += skip;

				t1r = ar + cr;
				t1i = ai + ci;

				t3r = ar - cr;
				t3i = ai - ci;

				t2r = er + gr;
				t2i = ei + gi;

/*				t4r = er - gr; */
/*				t4i = ei - gi; */

				ar = t1r + t2r; /*ar */
				ai = t1i + t2i; /*ai */
				x[j4] = ar;
				xoffset[j4] = ai;
				j4 = j + ((j&MASK)>>SHIFT);
				t1r = t1r - t2r;
				t1i = t1i - t2i;
				t2r = c0*t1r + s0*t1i; /*er */
				t2i = c0*t1i - s0*t1r; /*ei */
				x[p0] = t2r;
				xoffset[p0] = t2i;
				p0 = j + n2;
				p0 += ((p0&MASK)>>SHIFT);
				t1r = er - gr;
				t1i = ei - gi;
				t2r = t3r - t1i;
				t2i = t3i + t1r;
				cr = c1*t2r + s1*t2i; /*cr */
				ci = c1*t2i - s1*t2r; /*ci */
				x[j2] = cr;
				xoffset[j2] = ci;
				j2 = j + n4;
				j2 = j2 + ((j2&MASK)>>SHIFT);
				t2r = t3r + t1i;
				t2i = t3i - t1r;
				gr = tt1*t2r + tt2*t2i; /*gr */
				gi = tt1*t2i - tt2*t2r; /*gi */
				x[p2] = gr;
				xoffset[p2] = gi;
				p2 = j + n2 + n4;
				p2 += ((p2&MASK)>>SHIFT);

			}
		}
		n2<<=3;
		b = n2<<1;
		currentLevel-=3;
		loops>>=3;
		n4 = n2>>1;
		n8 = n2>>2;

	}



}


/*--- Rounding, Propagating, and Error Checking ---*/

/*

Excerpt from a message from George Woltman.....

"A Crandall/Fagin LL tester consists of these tests:

1. Multiply every FFT element by a "magic number".
2. Perform an FFT.
3. Do a complex squaring of each FFT element.
4. Perform an inverse FFT.
5. Divide every FFT element by the "magic number" and divide by N.
6. Round every FFT element to an integer.
7. Propagate carries.
8. Subtract 2 from first FFT element.
9. Loop.

If you're clever enough - you can do steps 5,6,7,8 in the same loop. You can
even do step 1 of the next iteration in the same loop. My code spends about
15-20% of its time in steps 1,5,6,7,8.

Also note that step 6 is where you can optionally check for maximum round-off
error. If you're seeing fractional parts of .001 and .999 you're in good shape.
If you're seeing fractional parts of .400 or .600, then you are dangerously close
to having a round-off error (that's where the correct result is 3.0, the FFT
returns 3.6 and you round it to the incorrect 4.0)."

End of excerpt.......

These two routines (normalize and last_normalize) perform steps 5,6,7 from the
description above.  Step 1 (for the next loop) is performed by normalize() but
not by last_normalize(), since - as you might guess from the name - it is only
used for the last loop.  Step 8 "Subtract 2 from first FFT element" is not
performed by these routines.

There are two primary optimizing "tricks" in these routines.

First is a  "well known" floating point trick. To round the FFT elements to the
nearest whole number Lucas.c adds 0.5 and then performs a floor() operation.
Calling this system level routine can be very slow and costly.  Instead this
routine adds a large number to the FFT element - in fact the number added is
large enough to force the processor to re-normalize the number to the precision
that effectively rounds to the nearest unit!!  This is handled in hardware MUCH
FASTER than floor() can be called.  The same big number is later subtraced from
the FFT element -- leaving the desired rounded value.  For a 64-bit IEEE double
precision number this magical "Big Number" is 3*2^51.  It is represented as
TheBigA and TheBigB in this routine to keep a clever compiler from optimizing the
operation away.

Second, the "magic" numbers in the *twotophi and *twotominusphi
(twotominusphi[a] = 1/N * 1/twotophi[a]) are only read for every UPDATE-th value.
In- between the values are "propagated" by a fixed set of multipliers.  UPDATE is
typically set to 64. Values much larger than this can generate errors due to loss
of precision with every propagation.

This routine makes tremendous use of the superscalar nature of RISC
microprocessors. The calculation of the address of the next element to be
normalized, as well as the determination of whether it is a "big" or "small"
number (characterized by the value of size0), all happens inter-mixed with the
floating point calculations for the normalization / rounding / carry propagation.
As a result, these integer based calculation are "free" in terms of execution
time.




*/

#ifdef __STDC__
BIG_DOUBLE normalize(
	BIG_DOUBLE *x, 			/* the primary data array of FFT data */
	UL N,					/* number of elements in *x */
       UL b,UL c,		/* same as b and c in the original Lucas.c  */
				/* (used to tell "big" from "small" data elements) */
       BIG_DOUBLE hi,				/* max value for a "big" data element */
       BIG_DOUBLE hiinv,		/* 1/hi... */
       BIG_DOUBLE lo,				/* max value for a "small" data element */
       BIG_DOUBLE loinv,		/* 1/lo */
       BIG_DOUBLE TheBigA,	/* Trick number 3*2^51 for 64 bit machines */
       BIG_DOUBLE TheBigB,	/* Trick number 3*2^51 for 64 bit machines */
       BIG_DOUBLE *two_to_minusphi, /* array of magic Crandall/Fagin multipliers */
       BIG_DOUBLE *two_to_phi,			/* array of magic Crandall/Fagin multipliers */
       UL err_flag		/* flag to tell whether of not to calculate error on */
       )								/* this iteration */
#else
BIG_DOUBLE normalize(x, N, b, c, hi, hiinv, lo, loinv, TheBigA, TheBigB, two_to_minusphi, two_to_phi,
                     err_flag)
BIG_DOUBLE *x;
UL N, b, c;
BIG_DOUBLE hi, hiinv, lo, loinv, TheBigA, TheBigB, *two_to_minusphi, *two_to_phi;
UL err_flag;
#endif
{
    register long j,k,lastloop,bj,offset,newOffset,newOffsetTemp;
    register long size0,Nminus1;
    register BIG_DOUBLE temp0,temp1,temp2,tempErr;
    register BIG_DOUBLE maxerr=0.0,err=0.0,ttmpSmall=Gsmall,ttmpBig=Gbig,ttmp,ttp,ttmpTemp;
    register BIG_DOUBLE carry,ttpSmall=Hsmall,ttpBig=Hbig;
    register BIG_DOUBLE hiMaxBigValue = hi*TheBigA - TheBigB, loMaxBigValue = lo*TheBigA - TheBigB;

    Nminus1 = N-1;
    carry = TheBigA - 2.0; /* this is the -2 of the LL x*x - 2 */
    lastloop = N-UPDATE;

    bj=N;
    size0 = 1;
    offset=0;

    for (j=0; j<N; j+=UPDATE) {

       ttmp = two_to_minusphi[offset];
       ttp = two_to_phi[offset];

       for (k=1; k<UPDATE; k++) {
          temp0 = x[offset];
	      newOffset = j + k;
	      if (err_flag) {
	          tempErr = temp0*ttmp - TheBigA;
	          tempErr += TheBigB;
		      err = fabs(temp0*ttmp-tempErr);
		      if (err>maxerr)
		         maxerr=err;
	      }
          temp0 = temp0 * ttmp + carry;
          newOffsetTemp = newOffset&MASK;
          if (size0) {
             temp1 = temp0 + hiMaxBigValue;
             bj+=b;
             ttmp *=ttmpBig;
             bj &= (Nminus1);
             carry = temp1 * hiinv;
             size0 = (bj>=c);
             temp2 = temp1 - hiMaxBigValue;
             newOffsetTemp = newOffsetTemp>>SHIFT;
             temp0 = temp0 - temp2;
             x[offset] = temp0 * ttp;
             ttp *= ttpBig;
          }
          else {
             temp1 = temp0 + loMaxBigValue;
             bj+=b;
             ttmp *=ttmpSmall;
             bj &= (Nminus1);
             carry = temp1 * loinv;
             size0 = (bj>=c);
             temp2 = temp1 - loMaxBigValue;
             newOffsetTemp = newOffsetTemp>>SHIFT;
             temp0 = temp0 - temp2;
             x[offset] = temp0 * ttp;
             ttp *= ttpSmall;
          }
          offset=newOffset+newOffsetTemp;

       }
          temp0 = x[offset];
	      newOffset = j + UPDATE;
          if (j==lastloop) size0 = 0;
	      if (err_flag) {
	          tempErr = temp0*ttmp - TheBigA;
	          tempErr += TheBigB;
		      err = fabs(temp0*ttmp-tempErr);
		      if (err>maxerr){
		         maxerr=err;}
	      }
          temp0 = temp0 * ttmp + carry;
          newOffsetTemp = newOffset&MASK;
          if (size0) {
             temp1 = temp0 + hiMaxBigValue;
             bj+=b;
             ttmp *=ttmpBig;
             bj &= (Nminus1);
             carry = temp1 * hiinv;
             size0 = (bj>=c);
             temp2 = temp1 - hiMaxBigValue;
             newOffsetTemp = newOffsetTemp>>SHIFT;
             temp0 = temp0 - temp2;
             x[offset] = temp0 * ttp;
             ttp *= ttpBig;
          }
          else {
             temp1 = temp0 + loMaxBigValue;
             bj+=b;
             ttmp *=ttmpSmall;
             bj &= (Nminus1);
             carry = temp1 * loinv;
             size0 = (bj>=c);
             temp2 = temp1 - loMaxBigValue;
             newOffsetTemp = newOffsetTemp>>SHIFT;
             temp0 = temp0 - temp2;
             x[offset] = temp0 * ttp;
             ttp *= ttpSmall;
          }
          offset=newOffset+newOffsetTemp;

    }

    bj = N;
    ttmp = two_to_minusphi[0];
    ttp = two_to_phi[0];

  k=0;
  while(carry != TheBigA)
  {
    temp0 = x[k+((k&MASK)>>SHIFT)];
    ttmpTemp = ttmp*N;
    size0 = (bj>=c);
    bj += b;
    bj &= (Nminus1);
    temp0 = temp0 * ttmpTemp + carry;
    if (size0)
    {
      temp1 = temp0 + hiMaxBigValue;
      ttmp *=ttmpBig;
      carry = temp1 * hiinv;
      temp2 = temp1 - hiMaxBigValue;
      temp0 = temp0 - temp2;
      x[k+((k&MASK)>>SHIFT)] = temp0 * ttp;
      ttp *= ttpBig;
    }
    else
    {
      temp1 = temp0 + loMaxBigValue;
      ttmp *=ttmpSmall;
      carry = temp1 * loinv;
      temp2 = temp1 - loMaxBigValue;
      temp0 = temp0 - temp2;
      x[k+((k&MASK)>>SHIFT)] = temp0 * ttp;
      ttp *= ttpSmall;
    }
    k++;
  }
  return(maxerr);
}

#ifdef __STDC__
BIG_DOUBLE last_normalize(BIG_DOUBLE *x, UL N,
       UL b,UL c,
       BIG_DOUBLE hi,
       BIG_DOUBLE hiinv,
       BIG_DOUBLE lo,
       BIG_DOUBLE loinv,
       BIG_DOUBLE TheBigA,
       BIG_DOUBLE TheBigB,
       BIG_DOUBLE *two_to_minusphi,
       BIG_DOUBLE *two_to_phi,
       UL err_flag)
#else
BIG_DOUBLE last_normalize(x, N, b, c, hi, hiinv, lo, loinv, TheBigA, TheBigB, two_to_minusphi, two_to_phi,
                          err_flag)
BIG_DOUBLE *x;
UL N, b, c;
BIG_DOUBLE hi, hiinv, lo, loinv, TheBigA, TheBigB, *two_to_minusphi, *two_to_phi;
UL err_flag;
#endif
{
    register long j,k,lastloop,bj,offset,newOffset,newOffsetTemp;
    register long size0,Nminus1;
    register BIG_DOUBLE temp0,temp1,temp2,tempErr;
    register BIG_DOUBLE maxerr=0.0,err=0.0,ttmpSmall=Gsmall,ttmpBig=Gbig,ttmp,ttp;
    register BIG_DOUBLE carry;
    register BIG_DOUBLE hiMaxBigValue = hi*TheBigA - TheBigB, loMaxBigValue = lo*TheBigA - TheBigB;

    Nminus1 = N-1;
    carry = TheBigA - 2.0; /* this is the -2 of the LL x*x - 2 */
    lastloop = N-UPDATE;

    bj=N;
    size0 = 1;
    offset=0;


    for (j=0; j<N; j+=UPDATE) {

       ttmp = two_to_minusphi[offset];
       ttp = two_to_phi[offset];

       for (k=1; k<UPDATE; k++) {
          temp0 = x[offset];
	      newOffset = j + k;
	      if (err_flag) {
	          tempErr = temp0*ttmp - TheBigA;
	          tempErr += TheBigB;
		      err = fabs(temp0*ttmp-tempErr);
		      if (err>maxerr)
		         maxerr=err;
	      }
          temp0 = temp0 * ttmp + carry;
          newOffsetTemp = newOffset&MASK;
          if (size0) {
             temp1 = temp0 + hiMaxBigValue;
             bj+=b;
             ttmp *=ttmpBig;
             bj &= (Nminus1);
             carry = temp1 * hiinv;
             size0 = (bj>=c);
             temp2 = temp1 - hiMaxBigValue;
             newOffsetTemp = newOffsetTemp>>SHIFT;
             temp0 = temp0 - temp2;
             x[offset] = temp0;
          }
          else {
             temp1 = temp0 + loMaxBigValue;
             bj+=b;
             ttmp *=ttmpSmall;
             bj &= (Nminus1);
             carry = temp1 * loinv;
             size0 = (bj>=c);
             temp2 = temp1 - loMaxBigValue;
             newOffsetTemp = newOffsetTemp>>SHIFT;
             temp0 = temp0 - temp2;
             x[offset] = temp0;
          }
          offset=newOffset+newOffsetTemp;

       }
          temp0 = x[offset];
	      newOffset = j + UPDATE;
          if (j==lastloop) size0 = 0;
	      if (err_flag) {
	          tempErr = temp0*ttmp - TheBigA;
	          tempErr += TheBigB;
		      err = fabs(temp0*ttmp-tempErr);
		      if (err>maxerr){
		         maxerr=err;}
	      }
          temp0 = temp0 * ttmp + carry;
          newOffsetTemp = newOffset&MASK;
          if (size0) {
             temp1 = temp0 + hiMaxBigValue;
             bj+=b;
             ttmp *=ttmpBig;
             bj &= (Nminus1);
             carry = temp1 * hiinv;
             size0 = (bj>=c);
             temp2 = temp1 - hiMaxBigValue;
             newOffsetTemp = newOffsetTemp>>SHIFT;
             temp0 = temp0 - temp2;
             x[offset] = temp0;
          }
          else {
             temp1 = temp0 + loMaxBigValue;
             bj+=b;
             ttmp *=ttmpSmall;
             bj &= (Nminus1);
             carry = temp1 * loinv;
             size0 = (bj>=c);
             temp2 = temp1 - loMaxBigValue;
             newOffsetTemp = newOffsetTemp>>SHIFT;
             temp0 = temp0 - temp2;
             x[offset] = temp0;
          }
          offset=newOffset+newOffsetTemp;

    }

    bj = N;
    ttmp = two_to_minusphi[0];
    ttp = two_to_phi[0];

  k=0;
  while(carry != TheBigA)
  {
    temp0 = x[k+((k&MASK)>>SHIFT)];
    size0 = (bj>=c);
    bj += b;
    bj &= (Nminus1);
    temp0 = temp0 + carry;
    if (size0)
    {
      temp1 = temp0 + hiMaxBigValue;
      carry = temp1 * hiinv;
      temp2 = temp1 - hiMaxBigValue;
      temp0 = temp0 - temp2;
      x[k+((k&MASK)>>SHIFT)] = temp0;
    }
    else
    {
      temp1 = temp0 + loMaxBigValue;
      carry = temp1 * loinv;
      temp2 = temp1 - loMaxBigValue;
      temp0 = temp0 - temp2;
      x[k+((k&MASK)>>SHIFT)] = temp0;
    }
    k++;
  }
  return(maxerr);
}

int main(int argc, char *argv[])
 {
  BIG_DOUBLE *x = NULL, *two_to_phi = NULL, *two_to_minusphi = NULL;
  complex *expn = NULL;
  BIG_DOUBLE TheBigA = TRUNC_A, TheBigB = TRUNC_B;
  BIG_DOUBLE low, high, lowinv, highinv;
  UL b, c, nn;
  UL q = 0L, n = 2L, j = 1L, last = 2L, flag;
  size_t size;
  BIG_DOUBLE err;
  int restarting = 0;
  char M = '\0';
  FILE *infp = NULL, *outfp = NULL, *dupfp = NULL;
  int primenet = 0; /* flag: interact with PrimeNet?  -N option; see rw.c and rw.h */

#ifdef macintosh
  argc = ccommand(&argv);
#endif
  strcpy(version, program_name);
  strcat(version, " v");
  strncat(version, program_revision + strlen("4Revision: "),
          strlen(program_revision) - strlen("4Revison:  4"));
  strcat(version, " Sweeney");
  setup();
  while (!terminate)
   {
    do /* while (restarting) */
     {
      switch ((restarting != 0) ? 3 :
              input(argc, argv, &q, &n, &j, &err, &x, last, &M, NULL, NULL, NULL, NULL, NULL, MASK, SHIFT,
                    &infp, &outfp, &dupfp, &primenet))
       {
        case 0: /* no more input */
          print_time();
          return(0);
        case 1: /* something wrong; error message, if any, already printed */
        default:
          print_time();
          return(1);
        case 2: /* continuing work from a partial result */
          restarting = 1; /* not the usual sense of restarting (FFT errors too high), but ... */
          size = n + ((n & MASK) >> SHIFT);
          break;
        case 3:
          while (n < ((BIG_DOUBLE)q)/ROUNDED_HALF_MANTISSA + 4.0)
            n <<= 1;
          if (x != NULL)
            free((char *)x);
          size = n + ((n & MASK) >> SHIFT);
          x = (BIG_DOUBLE *)calloc(size, sizeof(BIG_DOUBLE));
          x[0] = 4.0;
          j = 1;
          break;
       }
      if (two_to_phi != NULL)
        free((char *)two_to_phi);
      two_to_phi = (BIG_DOUBLE *)calloc(4*size, sizeof(BIG_DOUBLE));
      expn = (complex *)(two_to_phi + size);
      two_to_minusphi = two_to_phi + 3*size;
      if (!restarting)
       {
        if (M != 'U' && M != 'I' && last != q - 2)
          (void)fprintf(stderr, "%s: exponent " PRINTF_FMT_UL " should not need Lucas-Lehmer testing\n",
                        program_name, q);
        if ((M != 'U' && M != 'I') || last == q - 2)
          continue;
       }
      restarting = 0;
      init_lucas(q, n, &b, &c, two_to_phi, two_to_minusphi, &low, &high, &lowinv, &highinv);
      init_fft(n, &expn);
      nn = (UL) (log((BIG_DOUBLE)n)/LN2 + 0.5);
      last = q - 2; /* the last iteration done in the primary loop */
      for ( ; !terminate && !restarting && j < last; j++)
       {
#if (kErrChkFreq > 1)
        if ((j % kErrChkFreq) == 1 || j < 1000)
#endif
          flag = kErrChk;
#if (kErrChkFreq > 1)
        else
          flag = 0;
#endif
        err = lucas_square(x, n, nn, b, c, high, highinv, low, lowinv, TheBigA, TheBigB,
                           two_to_phi, two_to_minusphi, expn, flag);
        if (chkpnt_iterations != 0 && j % chkpnt_iterations == 2 && j < last &&
            check_point(q, n, j + 1, err, x, MASK, SHIFT, primenet) < 0)
         {
          print_time();
          return(errno);
         }
        if (err > kErrLimit) /* n is not big enough; increase it and start over */
         {
          n <<= 1;
          restarting = 1;
         }
       }
      if (restarting)
        continue;
      if (j < last) /* not done, but need to quit */
       {
        chkpnt_iterations = 0L;
        return((check_point(q, n, j, err, x, MASK, SHIFT, primenet) <= 0) ? errno : 0);
       }
      err = lucas_square(x, n, nn, b, c, high, highinv, low, lowinv, TheBigA, TheBigB, two_to_phi,
                         two_to_minusphi, expn, kLast);
      if (err > kErrLimit) /* n is too small on last iteration only (!) */
       {
        n <<= 1;
        restarting = 1;
       }
     } while (restarting);
    printbits(x, q, n, (q > 64L) ? 64L : q, b, c, high, low, MASK, SHIFT, version, outfp, dupfp, primenet);
    print_time();
   }
  return(0);
 }
