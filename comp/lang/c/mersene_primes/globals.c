static const char RCSglobals_c[] = "$Id: globals.c,v 6.50 1999/12/19 06:06:20 wedgingt Exp $";

#include "setup.h"

BIG_DOUBLE *cn = NULL, *sn = NULL, *two_to_phi = NULL, *two_to_minusphi = NULL, *scrambled = NULL;
BIG_DOUBLE high, low, highinv, lowinv;
long *permute = NULL;
int partial = 0; /* read a partial result? */
#ifdef DEBUG
BIG_DOUBLE highest_err;
#endif
