#ifndef DRCSsetup_h
#define DRCSsetup_h "$Id: setup.h,v 6.50 1999/12/19 06:06:20 wedgingt Exp $"

extern const char *program_name;
extern const char program_revision[];

#if defined(_MSC_VER) || defined(__WATCOMC__)
#define pccompiler 1
#endif

#if defined(vms) && !defined(MULTINET)
# undef TCPIP
#endif

#ifdef __STDC__
# define PROTO(x) x
#else
# define PROTO(x) (/* x */)
#endif

#define BIG_LONG long int
#define SIZEOF_UL 4
  /* the previous would not be needed if the preprocessor understood sizeof() */
#define BIG_DOUBLE double
#define ROUNDED_HALF_MANTISSA 32L
/* truncate an IEEE double by adding and then subtracting 3.0*2^51 to it */
#define TRUNC_A (3.0*0x4000000*0x2000000)
#define TRUNC_B (12.0*0x2000000*0x1000000)
#define PRINTF_FMT_ONE "RI q %lu n %lu j %lu err %f sBL %ld sBD %ld RHM %ld\n"
#define PRINTF_FMT_TWO "RI %.1f\n"
#define PRINTF_FMT_UL "%lu"
#define SCANF_FMT_UL " %lu"
#define SCANF_FMT_ONE " RI q %lu n %lu j %lu err %lf sBL %d sBD %d RHM %ld\n"
#define SCANF_FMT_TWO " RI %lf\n"

#if !defined(vms)

# if defined(__sgi) && !defined(__sgi__)
/* SGI's cc uses __sgi; gcc on SGI's uses __sgi__ */
#  define __sgi__ (__sgi)
/* avoid -xansi's inlining that fails to set errno correctly in some cases */
#  undef __INLINE_INTRINSICS
# endif

# ifdef __sgi__
   /* SGI's Irix has differing prototypes for initstate() in math.h and stdlib.h */
#  define initstate initstate_stdlib
# endif

# include <stdlib.h>

# ifdef __sgi__
#  undef initstate
# endif

/* This has to come before the #define's of exp() et al on some Linux machines */
# include <math.h>

# if defined(__GNUC__) && !defined(NeXT)
#  if (__GNUC__ == 2 && __GNUC_MINOR__ >= 6) || __GNUC__ > 2
#   if defined(__i386__)
#    ifndef DO_NOT_USE_LONG_LONG_EXPONENT
#     undef BIG_LONG
#     define BIG_LONG long long int
#     undef SIZEOF_UL
#     define SIZEOF_UL 8
#     if defined(linux) && defined(FACTOR_PACKAGE)
/* Linux wants 'L' instead of 'l' for 'long long', but only factorers care about exponents > 2^30 or so */
#      undef PRINTF_FMT_UL
#      undef SCANF_FMT_UL
#      define PRINTF_FMT_UL "%Lu"
#      define SCANF_FMT_UL " %Lu"
#     endif
#    endif
#    ifndef DO_NOT_USE_LONG_DOUBLE
#     undef BIG_DOUBLE
#     define BIG_DOUBLE long double
#     undef ROUNDED_HALF_MANTISSA
#     define ROUNDED_HALF_MANTISSA 64L
/* truncate an Intel long double by adding and then subtracting 3.0*2^62 to it */
#     undef TRUNC_A
#     define TRUNC_A (3.0*0x4000000*0x2000000*0x800)
#     undef TRUNC_B
#     define TRUNC_B (12.0*0x2000000*0x1000000*0x800)
#     undef PRINTF_FMT_ONE
#     define PRINTF_FMT_ONE "RI q " PRINTF_FMT_UL " n " PRINTF_FMT_UL " j " PRINTF_FMT_UL " err %Lf sBL %ld sBD %ld RHM %ld\n"
#     undef PRINTF_FMT_TWO
#     define PRINTF_FMT_TWO "RI %.1Lf\n"
#     undef SCANF_FMT_ONE
#     define SCANF_FMT_ONE " RI q" SCANF_FMT_UL " n " SCANF_FMT_UL " j " SCANF_FMT_UL " err %Lf sBL %d sBD %d RHM %ld\n"
#     undef SCANF_FMT_TWO
#     define SCANF_FMT_TWO " RI %Lf\n"
#     ifndef __FreeBSD__
#      ifdef ceil
#       undef ceil
#      endif
#      define ceil(x) ceill(x)
#      ifdef cos
#       undef cos
#      endif
#      define cos(x) cosl(x)
#      ifdef exp
#       undef exp
#      endif
#      define exp(x) expl(x)
#      ifdef fabs
#       undef fabs
#      endif
#      define fabs(x) fabsl(x)
#      ifdef floor
#       undef floor
#      endif
#      define floor(x) floorl(x)
#      ifdef log
#       undef log
#      endif
#      define log(x) logl(x)
#      ifdef sin
#       undef sin
#      endif
#      define sin(x) sinl(x)
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef sun
#  include "prof.h"
# endif

# ifdef hpux
  /* some HPUX compilers in ANSI mode only #define __hpux, so use it */
#  ifndef __hpux
#   define __hpux (hpux)
#  endif
# endif

# if defined(__hpux) && defined(__STDC__)
  /* HPUX has long double, but not cosl() etc. */
  /*  Further, some HP compilers are not ANSI compliant. */
#  ifndef DO_NOT_USE_LONG_LONG_EXPONENT
#   undef BIG_LONG
#   define BIG_LONG long long int
#  endif
#  undef BIG_DOUBLE
#  define BIG_DOUBLE long double
#  undef ROUNDED_HALF_MANTISSA
#  define ROUNDED_HALF_MANTISSA 64L
#  undef PRINTF_FMT_ONE
#  define PRINTF_FMT_ONE "RI q %lu n %lu j %lu err %Lf sBL %ld sBD %ld RHM %ld\n"
#  undef PRINTF_FMT_TWO
#  define PRINTF_FMT_TWO "RI %.1Lf\n"
#  undef SCANF_FMT_ONE
#  define SCANF_FMT_ONE " RI q %lu n %lu j %lu err %Lf sBL %d sBD %d RHM %ld\n"
#  undef SCANF_FMT_TWO
#  define SCANF_FMT_TWO " RI %Lf\n"
# endif

# include <stdio.h>
# include <ctype.h>

# if defined(macintosh)
#  if !defined(unlink)
#   define unlink(filename) (remove(filename))
#  endif
#  if !defined(isascii)
#   define isascii(x) (((x) | 0177) == 0177)
#  endif
# endif

/* Unfortunately, some systems, notably SunOS 4.x, do not provide */
/*  prototypes for many stdio functions, so ... */
extern int fclose PROTO((FILE *stream));
extern int fflush PROTO((FILE *stream));
extern int fgetc PROTO((FILE *stream));
extern int fputc PROTO((int ch, FILE *stream));
extern int fputs PROTO((const char *s, FILE *stream));
extern size_t fread PROTO((void *ptr, size_t size, size_t nitems, FILE *stream));
extern size_t fwrite PROTO((const void *ptr, size_t size, size_t nitems, FILE *stream));
extern void perror PROTO((const char *msg));
extern int rename PROTO((const char *oldpath, const char *newpath));
/* setlinebuf() can't be done yet due to #define's of it later on systems that don't have it */
extern int ungetc PROTO((int ch, FILE *stream));

# if !defined(_AIX) && !defined(pccompiler)
/* AIX 4.1's cc doesn't like these because its <stdio.h> has no argument types for them */
extern int fprintf PROTO((FILE *stream, const char *fmt, ...));
extern int fscanf PROTO((FILE *stream, const char *fmt, ...));
extern int printf PROTO((const char *fmt, ...));
extern int sscanf PROTO((const char *src, const char *fmt, ...));
# endif

# include <assert.h>

# if !defined(__amiga__) && !defined(macintosh) && !defined(__FreeBSD__)
#  include <malloc.h>
# endif

# ifdef rint
#  undef rint
# endif
# define rint(x) ((BIG_DOUBLE)((BIG_LONG)(x + 0.5)))

# ifndef pc7300
#  ifdef TCPIP
#   if defined(SOLARIS) && defined(__GNUC__)
#    include <sys/types.h>
#   endif
#   include <sys/socket.h>
#   include <netinet/in.h>
#   include <netdb.h>
int socket PROTO((int domain, int type, int protocol));
#   if !defined(linux) && !defined(_AIX) && !defined(SOLARIS) && !defined(__NetBSD__)
int connect PROTO((int sockfd, struct sockaddr *serv_addr, int addrlen));
#   endif
#  endif

#  if defined(TCPIP) && !defined(__hpux)
#   include <arpa/inet.h>
#  endif

#  if defined(SOLARIS) || defined(__hpux) || defined(macintosh) || defined(pccompiler)
#   include <string.h>
#  else
#   include <strings.h>
#  endif

#  ifdef pccompiler
#   define bzero(str, len) (void)memset(str, '\0', len)
#  endif

# else
#  include <string.h>
#  include <memory.h>

#  define bzero(str, len) (void)memset(str, '\0', len)

/* CAREFUL: this macro only works when newfd < smallest fd not open */
/*  We only need it to replace 0 (stdin) and 1 (stdout) presently */
/*  It could be done properly with pair of while loops and an array to */
/*   remember the fd's we opened that we didn't want */
#  define dup2(fd, newfd) ((void)close(newfd), (void)dup(fd))

#  define setlinebuf(fp) setbuf(fp, NULL)

extern void _exit();
extern void exit();
extern void free();
extern void perror();
extern unsigned sleep();
# endif

# include <signal.h>
# include <errno.h>

#else /* vms */

# include stdlib
# include stdio
# include string

# define bzero(str, len) memset(str, '\0', len)
# define setlinebuf(fp) setbuf(fp, NULL)
#endif /* vms */

#ifdef _IOLBF
/* Note that this includes at least SunOS 4.1.3+, SunOS 5.x, Linux, and HPUX despite the next #ifndef */
# define setlinebuf(stream) setvbuf(stream, NULL, _IOLBF, BUFSIZ)
extern int setvbuf PROTO((FILE *stream, char *buffer, int type, size_t size));
#endif

#ifndef setlinebuf
# ifdef sun
 /* SunOS 4.1.3 at least, but probably not SunOS 5.x since that's at least partially POSIX */
extern int setlinebuf PROTO((FILE *stream));
# else
#  ifdef linux
 /* on Linux, at least, which may mean on all POSIX flavored UNIXs */
extern void setlinebuf PROTO((FILE *stream));
#  endif
# endif
#endif

typedef unsigned short US;

/* these next two typedefs will be the same 'text' - but not always the same size - except under Linux */
#ifdef FACTOR_PACKAGE
typedef unsigned BIG_LONG UL;
#else
typedef unsigned long int UL;
#endif

#ifndef vms
# if !defined(pc7300) && !defined(mips) && !defined(linux) && !defined(_AIX)
#  if !defined(__ultrix) && !defined(macintosh) && !defined(__hpux) && !defined(pccompiler)
typedef int handler;
#   define return_handler return(0)
#  endif
# endif
# ifndef return_handler
typedef void handler;
#  define return_handler return
# endif
#endif

#ifndef __STDC__
# define volatile /* */
#endif

#ifndef vms
extern volatile char terminate;	/* Flag: have we gotten a SIGTERM ? */
#else /* !vms */
# define terminate 0
#endif /* !vms */

/* NBBY is Number of Bits per BYte, usually 8 */
#ifndef NBBY
# define NBBY 8
#endif

#define BITS_IN_LONG (NBBY*sizeof(UL))

#define CHKPNT_FILENAME_LENGTH ((BITS_IN_LONG + 1)*10/31 + 2)
/* 32 bits fit in 9.6+ digits, + 2 for c (or t or ...) and \0 */

#define TWOPI (BIG_DOUBLE)(6.2831853071795864769252867665590057683943387987502116419498891846156)
#define SQRTHALF (BIG_DOUBLE)(0.70710678118654752440084436210484903928483593768847403658833986899536)
#define SQRT2 (BIG_DOUBLE)(1.4142135623730950488016887242096980785696718753769480731766797379907)

#ifndef errno
extern int errno;
#endif

#if !defined(vms) && !defined(pc7300) && !defined(mips) && defined(TCPIP) && !defined(linux)
extern int h_errno;
#endif

#if defined(IBMRTAIX) || defined(pc7300)
# define SIGCHLD SIGCLD
#endif

#if (defined(IBMRTBSD) || defined(__hpux) || defined(mips) || defined(sun)) && defined(TCPIP)
# define NO_HERROR
#endif

extern const char *presence PROTO((const char *string, int ch)); /* same as BSD index() and SysV strchr() */
extern void setup PROTO((void));

#ifndef vms

# if defined(linux) || defined(__ultrix) || defined(_AIX) || defined(__hpux) || defined(macintosh) || defined(_MSC_VER)
extern handler term_handler PROTO((int huh));
# else
extern handler term_handler PROTO((void));
# endif

# ifndef pc7300
extern void clientexit PROTO((const char *msg));

# endif
#endif

#ifdef NO_HERROR
extern void herror PROTO((const char *msg));
#endif

#ifndef REAL
#define REAL BIG_DOUBLE
#endif

#endif /* ifndef DRCSsetup_h */
