static const char RCSinit_real_c[] = "$Id: init_real.c,v 6.50 1999/12/19 06:06:20 wedgingt Exp $";

#include "setup.h"
#include "globals.h"
#include "init_fft.h"

#ifdef __STDC__
void  init_scramble_real(UL n)
#else
void  init_scramble_real(n)
UL n;
#endif
{
  register UL i,j,k,halfn = n>>1;
	long tmp;
	
	for(i=0; i<n; ++i) permute[i] = i;
	for(i=0,j=0;i<n-1;i++) {
		if(i<j) {
	  	  tmp = permute[i];
		  permute[i] = permute[j];
		  permute[j] = tmp;
	  	}
		k = halfn;
		while(k<=j) {
			j -= k;
			k>>=1;
		}
  		j += k;
 	}
}
