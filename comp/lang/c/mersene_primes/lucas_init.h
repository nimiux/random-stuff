#define DRCSlucas_init_h "$Id: lucas_init.h,v 6.50 1999/12/19 06:06:20 wedgingt Exp $"

#ifdef __STDC__
extern void init_lucas(UL q, UL n, UL *b, UL *c);
#else
extern void init_lucas();
#endif
