static const char RCSpatch_c[] = "$Id: patch.c,v 6.50 1999/12/19 06:06:20 wedgingt Exp $";

#include "setup.h"
#include "globals.h"
#include "init_fft.h"
#include "patch.h"

#ifdef __STDC__
void patch(BIG_DOUBLE *x, UL n, UL b, UL c)
#else
void patch(x, n, b, c)
BIG_DOUBLE *x;
UL n, b, c;
#endif
 {
  register long carry;
  register UL j, bj, NminusOne = n - 1;
  register BIG_DOUBLE hi = high, lo = low, highliminv, lowliminv;
  register BIG_DOUBLE *px = x, xx;
  BIG_DOUBLE highlim, lowlim, lim, inv, base;

  carry = 0;
  highlim = hi*0.5;
  lowlim = lo*0.5;
  highliminv = 1.0/highlim;
  lowliminv = 1.0/lowlim;
  xx = *px + carry;
  if (xx >= highlim)
    carry = ((long)(xx*highliminv + 1)) >> 1;
  else
    if (xx < -highlim)
      carry = -(((long)(1 - xx*highliminv)) >> 1);
    else
      carry = 0;
  *(px++) = xx - carry*hi;

  bj = b;
  for(j = 1; j < NminusOne; ++j)
   {
    xx = *px + carry;
    if ((bj & NminusOne) >= c)
     {
      if (xx >= highlim)
        carry = ((long)(xx*highliminv + 1)) >> 1;
      else
        if (xx < -highlim)
          carry = -(((long)(1 - xx*highliminv)) >> 1);
        else
          carry = 0;
       *px = xx - carry*hi;
     }
    else
     {
      if (xx >= lowlim)
        carry = ((long)(xx*lowliminv + 1)) >> 1;
      else
        if (xx < -lowlim)
          carry = -(((long)(1 - xx*lowliminv)) >> 1);
        else
          carry = 0;
      *px = xx - carry*lo;
     }
    ++px;
    bj += b;
   }
  xx = *px + carry;
  if (xx >= lowlim)
    carry = ((long)(xx*lowliminv + 1)) >> 1;
  else
    if (xx<-lowlim)
      carry = -(((long)(1 - xx*lowliminv)) >> 1);
    else
      carry = 0;
  *px = xx - carry*lo;
  if (carry)
   {
    j = 0;
    bj = 0;
    px = x;
    while(carry)
     {
      xx = *px + carry;
      if (j == 0)
       {
        lim = highlim;
        inv = highliminv;
        base = hi;
       }
      else
        if (j == NminusOne)
         {
          lim = lowlim;
          inv = lowliminv;
          base = lo;
         }
        else
          if ((bj & NminusOne) >= c)
           {
            lim = highlim;
            inv = highliminv;
            base = hi;
           }
          else
           {
            lim = lowlim;
            inv = lowliminv;
            base = lo;
           }
      if (xx >= lim)
        carry = ((long)(xx*inv + 1)) >> 1;
      else
        if (xx<-lim)
          carry = -(((long)(1 - xx*inv)) >> 1);
        else
          carry = 0;
      *(px++) = xx - carry*base;
      bj += b;
      if (++j == n)
       {
        j = 0;
        bj = 0;
        px = x;
       }
     }
   }
 }
