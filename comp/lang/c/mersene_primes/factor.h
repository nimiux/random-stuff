/* $Id: factor.h,v 6.50 1999/12/19 06:06:20 wedgingt Exp $ */

#define FACTOR_FREELIP 0
#define FACTOR_DOUBLE 1
#define FACTOR_LONG_DOUBLE 2
#define FACTOR_LIBGMP_INT 3
#define FACTOR_LONGLONG 4

#include <stdio.h>

/*--------------------------------------*/
/* Choose a math package for factoring  */
/*--------------------------------------*/
#if FACTOR_PACKAGE == FACTOR_FREELIP
/* Freelip arbitrary precision package */
# include "faclip.h"
#else
# if FACTOR_PACKAGE == FACTOR_DOUBLE
/* Use native floating point - 53 bit accuracy on most machines */
/* This lets us find 51 or 52 bit factors */
#  include "facflt.h"
# else
#  if FACTOR_PACKAGE == FACTOR_LONG_DOUBLE
/* Use native long floating point - 64 bit accuracy on */
/* those machines and compilers supporting this data type */
/* This lets us find 62 bit factors! */
#   include "facfltl.h"
#  else
#   if FACTOR_PACKAGE == FACTOR_LIBGMP_INT
/* FSF's libgmp.a arbitrary precision package */
#    include "facgmp.h"
#   else
#    if FACTOR_PACKAGE == FACTOR_LONGLONG
/* 64 bit longs available on some machines using some compilers */
#     include "facint.h"
#    else
FACTOR_PACKAGE is not defined.
#    endif
#   endif
#  endif
# endif
#endif

/* This data type is used for handling Mersenne exponents. */
/* For various reasons, the mersfac* programs can only handle Mersenne */
/*  exponents up to 2^bits/24 where bits is (NBBY = 8)*sizeof(EXPONENT). */
#if FACTOR_PACKAGE == FACTOR_LONGLONG
typedef FACTOR EXPONENT;
#else
typedef UL EXPONENT;
#endif

/* Global variables */

extern FACTOR savef;	/* Factor in modulo operations */
extern FACTOR numer;	/* Numerator in modulo operations */

/* Routine definitions */

#ifdef __STDC__
# ifndef MERS_1STRS
int mersenne_factor (
	EXPONENT exponent,	/* The exponent of the Mersenne number */
				/* that we are trying to find a factor of */
	FACTOR	first_factor,	/* The first factor to test.  This need not */
				/* be of the form 2kp+1, this code will */
				/* test for factors larger than this number */
	FACTOR	last_factor,	/* This routine will not test factors larger */
				/* than this number. */
	int	find_all_factors,/* TRUE if this routine should continue */
				/* searching for factors after the first */
				/* factor is found. */
	int	M,		/* The mers format letter read: 'U', 'I', 'H', etc. */
	FILE	*fp,		/* file pointer to write to */
	FILE	*dupfp);	/* duplicate output to this file pointer */
extern void fac_fprint_winner(FILE *fp, EXPONENT e, FACTOR f, FILE *dupfp);
extern void fac_fprint_start(FILE *fp, EXPONENT e, FACTOR f, FACTOR f2, int M, FILE *dupfp);
extern void fac_fprint_end(FILE *fp, EXPONENT e, FACTOR f, FACTOR f2, int M, int got_one, FILE *dupfp);
# endif
extern int fscan_fac(FILE *fp, FACTOR *f);
extern int sscan_fac(char *str, FACTOR *f);
extern int fprint_fac(FILE *fp, FACTOR f);
#else
# ifndef MERS_1STRS
extern int mersenne_factor();
extern void fac_fprint_winner();
extern void fac_fprint_start();
extern void fac_fprint_end();
# endif
extern int fscan_fac();
extern int sscan_fac();
extern int fprint_fac();
#endif
