static const char RCSmers3p_c[] = "$Id: mers3p.c,v 6.50 1999/12/19 06:06:20 wedgingt Exp $ wedgingt@acm.org";
const char *RCSprogram_id = RCSmers3p_c; /*!!as for merskfac, this says nothing about the factoring package*/
const char program_revision[] = "$Revision: 6.50 $";

/* Perform the 3^(M(p) + 1) == 3^(2^p) == 9 (mod M(p)) pseudo-prime test.
   Since 3 is not a quadratic residue mod M(p) as both are 3 (mod 4), this
   may be equivalent to the stricter 3^(M(p) - 1) == 1 (mod M(p)) test.

Output is:
   M(prime)C		if proven composite
   M(prime)p		if M(prime) is probably prime
*/

#include "setup.h"
#include "factor.h"
#include "rw.h"

#ifdef macintosh
# include <console.h>
#endif

#if !defined(FACTOR_PACKAGE) || FACTOR_PACKAGE != FACTOR_FREELIP
This program can only be compiled with the freeLIP library as it
uses the Elliptic Curve Factoring functions from that library.
#endif

FACTOR savef, numer; /* not needed by this program, but faclip.h is and refers to them, so use them */
                     /*  where we'd otherwise use temporaries */

const char *program_name = NULL;

/* rr = a % b, b can only be rr if a and b positive */
/* b = 2^p - 1, p prime > 2 */
static void Mzmod(FACTOR a, long p, FACTOR b, FACTOR *rr)
 {
  if (z2log(a) < p)
   {
    zcopy(a, rr);
    return;
   }
  zrshift(a, p, &savef);
  zlowbits(a, p, &numer);
  zadd(savef, numer, rr);
  while (z2log(*rr) > p)
   {
    zrshift(*rr, p, &savef);
    zlowbits(*rr, p, &numer);
    zadd(savef, numer, rr);
   }
 }

/* c = a*a % n where n is 2^p - 1 */
static void Mzsqmod(FACTOR a, long p, FACTOR n, FACTOR *c)
 {
  static FACTOR mem = NULL;

  if (ziszero(n))
   {
    fprintf(stderr, "division by zero in Mzsqmod\n");
    exit(0);
   }
  if (ziszero(a))
   {
    zzero(c);
    return;
   }
  zsetlength(&mem, 2*p + 2, "in mers3p.c:Mzsqmod(), local");
  zsq(a, &mem);
  Mzmod(mem, p, n, c);
 }

/*ARGSUSED*/
#ifdef __STDC__
int main(int argc, char **argv)
#else
int main(argc, argv)
int argc;
char **argv;
#endif
 {
  EXPONENT p = 1L; /* input() wants this type; we can only handle long due to freeLIP */
  long i = 0L;
  FACTOR Mp, acc, startacc, temp, zero, one, nine, neg_one;
  char M = 'C';
  int allfactors; /* only needed for call to input(); ignored otherwise */
  FILE *infp = NULL, *outfp = NULL, *dupfp = NULL;

#ifdef macintosh
  argc = ccommand(&argv);
#endif
  if (argv == NULL || argv[0] == NULL)
    program_name = "mers3p[unknown-type]";
  else
    for (program_name = argv[0] + strlen(argv[0]); program_name > argv[0] && *program_name != '/'; )
      --program_name;
  if (*program_name == '/')
    ++program_name;
  setup();
  fac_init();
  fac_init_var(Mp);
  fac_init_var(zero);
  fac_init_var(one);
  fac_init_var(nine);
  fac_init_var(neg_one);
  fac_init_var(acc);
  fac_init_var(startacc);
  zzero(&zero);
  zintoz(1L, &one);
  zintoz(9L, &nine);
  zintoz(-1L, &neg_one);
  while (!terminate && input(argc, argv, &p, NULL, NULL, NULL, NULL, (UL)0L, &M, &startacc, &acc,
                             &allfactors, NULL, NULL, (UL)0, (UL)0, &infp, &outfp, &dupfp, NULL) > 1)
   {
    if (M != 'U' && M != 'I')
      continue;
    zlshift(one, i = (long)p, &Mp);
    zadd(Mp, neg_one, &Mp); /* Mp is now 2^p - 1 */
    for (zcopy(nine, &startacc); !terminate && --i > 0; startacc = temp)
     {
      Mzsqmod(startacc, (long)p, Mp, &acc);
      temp = acc;
      acc = startacc;
     }
    if (terminate)
      break;
    if (zcompare(startacc, nine) == 0)
      fprintf(outfp, "M( " PRINTF_FMT_UL " )p # mers3p.c 3-PRP\n", p);
    else
      fprintf(outfp, "M( " PRINTF_FMT_UL " )C # mers3p.c\n", p);
    if (dupfp == NULL)
      continue;
    if (zcompare(startacc, nine) == 0)
      fprintf(dupfp, "M( " PRINTF_FMT_UL " )p # mers3p.c 3-PRP\n", p);
    else
      fprintf(dupfp, "M( " PRINTF_FMT_UL " )C # mers3p.c\n", p);
   }
  return(0);
 }
