static const char RCSbalance_c[] = "$Id: balance.c,v 6.50 1999/12/19 06:06:20 wedgingt Exp $";

#include "setup.h"
#include "balance.h"
#include "globals.h"
#include "init_fft.h"

#ifdef __STDC__
UL is_big(UL j, UL big, UL small, UL n)
#else
UL is_big(j, big, small, n)
UL j, big, small, n;
#endif
 {
  return((((big*j) % n) >= small) || j == 0);
 }

#ifdef __STDC__
void balancedtostdrep(BIG_DOUBLE *x, UL n, UL b, UL c, BIG_DOUBLE hi, BIG_DOUBLE lo, UL mask, UL shift)
#else
void balancedtostdrep(x, n, b, c, hi, lo, mask, shift)
BIG_DOUBLE *x, hi, lo;
UL n, b, c, mask, shift;
#endif
{
  UL sudden_death = 0, j = 0, NminusOne = n - 1, k, k1;

  while (1)
   {
    k = j + ((j & mask) >> shift);
    if (x[k] < 0.0)
     {
      k1 = (j + 1) % n;
      k1 += (k1 & mask) >> shift;
      --x[k1];
      if (j == 0 || (j != NminusOne && is_big(j, b, c, n)))
        x[k] += hi;
      else
        x[k] += lo;
     }
    else
      if (sudden_death)
        break;
    if (++j == n)
     {
      sudden_death = 1;
      j = 0;
     }
   }
 }

#ifdef __STDC__
void check_balanced(BIG_DOUBLE *x, UL n, UL b, UL c, BIG_DOUBLE hi, BIG_DOUBLE lo, UL mask, UL shift)
#else
void check_balanced(x, n, b, c, hi, lo, mask, shift)
BIG_DOUBLE *x, hi, lo;
UL n, b, c, mask, shift;
#endif
 {
  UL j, k;
  BIG_DOUBLE limit;

  hi *= 0.5;
  lo *= 0.5;
  for (j = 0; j < n; ++j)
   {
    if (j == 0 || (j != n - 1 && is_big(j, b, c, n)))
      limit = hi;
    else
      limit = lo;
    k = j + ((j & mask) >> shift);
    assert((x[k] <= limit) && (x[k] >= -limit));
   }
 }
