/* The following C language program emulates the execution of a
 * subleq based OISC: */

int program_counter = 0;
int memory[384];
while (program_counter >= 0)
{
    a = memory[program_counter];
    b = memory[program_counter + 1];
    c = memory[program_counter + 2];
    memory[b] -= memory[a];
    if (memory[b] > 0)
        program_counter += 3;
    else
        program_counter = c;
}
