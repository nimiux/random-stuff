#ifndef __ACTION_H__
#define __ACTION_H__

/* X Events */

#include <X11/Xlib.h>

#include "displaywrapper.h" 
#define MAXEVENTS  1000

int (*eventarr[MAXEVENTS])();

int getaction (MyDisplay *disp);

Action processKeyPress (MyDisplay *disp, XEvent *e);

int processKeyRelease (MyDisplay *disp, XEvent *e);
int processButtonPress (MyDisplay *disp, XEvent *e);
int processButtonRelease (MyDisplay *disp, XEvent *e);
int processMotionNotify(MyDisplay *disp, XEvent *e);
int processEnterNotify (MyDisplay *disp, XEvent *e);
int processLeaveNotify(MyDisplay *disp, XEvent *e);
int processFocusIn (MyDisplay *disp, XEvent *e);
int processFocusOut(MyDisplay *disp, XEvent *e);
int processKeymapNotify(MyDisplay *disp, XEvent *e);
int processExpose (MyDisplay *disp, XEvent *e);
int processGraphicsExpose (MyDisplay *disp, XEvent *e);
int processNoExpose(MyDisplay *disp, XEvent *e);
int processCirculateRequest (MyDisplay *disp, XEvent *e);
int processConfigureRequest (MyDisplay *disp, XEvent *e);
int processMapRequest (MyDisplay *disp, XEvent *e);
int processResizeRequest(MyDisplay *disp, XEvent *e);
int processCirculateNotify (MyDisplay *disp, XEvent *e);
int processConfigureNotify (MyDisplay *disp, XEvent *e);
int processCreateNotify (MyDisplay *disp, XEvent *e);
int processDestroyNotify (MyDisplay *disp, XEvent *e);
int processGravityNotify (MyDisplay *disp, XEvent *e);
int processMapNotify (MyDisplay *disp, XEvent *e);
int processMappingNotify (MyDisplay *disp, XEvent *e);
int processReparentNotify (MyDisplay *disp, XEvent *e);
int processUnmapNotify (MyDisplay *disp, XEvent *e);
int processVisibilityNotify(MyDisplay *disp, XEvent *e);
int processColormapNotify(MyDisplay *disp, XEvent *e);
int processClientMessage (MyDisplay *disp, XEvent *e);
int processPropertyNotify (MyDisplay *disp, XEvent *e);
int processSelectionClear (MyDisplay *disp, XEvent *e);
int processSelectionNotify (MyDisplay *disp, XEvent *e);
int processSelectionRequest(MyDisplay *disp, XEvent *e);

void initevents();

#endif /* __ACTION_H__ */
