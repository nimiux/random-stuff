#include <syslog.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include "logger.h"
#include "matrix.h"

#define LOG_COORDS(p,c) logthis(glogchannel, LOG_WARNING, \
                     "%s [%6.20lf,%6.20lf,%6.20lf]", p, c.x, c.y, c.z);

extern channel glogchannel;
/* Some Tools */

/* TODO: Points to be passed as reference */

Vector vector(Coords pa, Coords pb) {
   Vector v;
   v.x = pb.x-pa.x;
   v.y = pb.y-pa.y;
   v.z = pb.z-pa.z;
   return v;
}

double modulus (Vector v) {
   return sqrt(v.x*v.x+v.y*v.y+v.z*v.z);
}   

Vector unitvector(Coords pa, Coords pb) {
   Vector v = vector(pa,pb);
   double mod = modulus(v); 
   v.x /= mod;
   v.y /= mod;
   v.z /= mod;
   return v;
}

void printview(Matrix *matrix) {
   LOG_COORDS("V", matrix->V.coords);
   /* TODO: C should not be used */
   LOG_COORDS("C", matrix->C.coords);
   LOG_COORDS("d", matrix->d);
   logthis(glogchannel, LOG_WARNING, "Focal Distance: %f", matrix->F);
}

/* No more Tools */

Matrix *GetMatrix (char *matrixfilename) {
   FILE *f;
   logthis(glogchannel, LOG_DEBUG, "Reading Matrix  from %s.", matrixfilename);
   f = fopen(matrixfilename,"r");
   if (!f) {
      logthis(glogchannel, LOG_ERR, "Could not read: %s.", matrixfilename);
      return (Matrix *)0; 
   } else {
      Matrix *matrix = malloc(sizeof(Matrix));
      if (matrix != (Matrix *) 0) {
         unsigned long npoints = 0;
         Coords *pV = &matrix->V.coords;
         /* TODO: C should not be used */
         Coords *pC = &matrix->C.coords;
         Grid *pg = &matrix->grid;
         fscanf(f, "%lf%lf%lf", &pV->x, &pV->y, &pV->z);
         fscanf(f, "%lf%lf%lf", &pC->x, &pC->y, &pC->z);
         matrix->d = unitvector(*pV, *pC);
         matrix->F = modulus(vector(*pV, *pC));
         for (; fscanf(f, "%lf%lf%lf", &(*pg)[npoints].coords.x, &(*pg)[npoints].coords.y, &(*pg)[npoints].coords.z)+1;npoints++) {
	    LOG_COORDS("Point scanned", (*pg)[npoints].coords); 
         }
         matrix->npoints = npoints;
         logthis(glogchannel, LOG_WARNING, "Loaded: %d points.", matrix->npoints);
         fclose(f);
      }
      return matrix;
   }
}

Point PlaneTranslation(Matrix *matrix, Point P) {
   /* C must not be used, use d and F instead */

   Point PT;
   PT.traceable = 1;
   Coords *PTC = &PT.coords;
   Coords PC = P.coords;

   Coords VC = matrix->V.coords;
   /* TODO: C should not be used */
   Coords CC = matrix->C.coords;
   Vector d = matrix->d;
   double u = d.x,v = d.y,w = d.z;

   double K3,F,dx,dy,dz;
   Coords XC;

   dx = PC.x - VC.x;
   dy = PC.y - VC.y;
   dz = PC.z - VC.z;
   K3 = u * dx + v * dy + w * dz;
   F = modulus(vector(VC, CC));

   /* Different positions relative to camera. */
   if (K3>0) {
      if (K3 < F) {
	 logthis(glogchannel, LOG_DEBUG, "Point in camera zone");
	 double K2 = u * VC.x + v * VC.y + w * VC.z;
	 double K4 = u * PC.x + v * PC.y + w * PC.z;
	 double theta = K2 + F - K4;
	 XC.x = PC.x + theta * u;
	 XC.y = PC.y + theta * v;
	 XC.z = PC.z + theta * w;
      } else {
	 double lambda;
	 logthis(glogchannel, LOG_DEBUG, "Point in view zone.");
	 lambda = F / K3;
	 XC.x = VC.x + lambda * dx;
	 XC.y = VC.y + lambda * dy;
	 XC.z = VC.z + lambda * dz;
      }
   } else {
      PT.traceable = 0;
      if (K3<0) {
	 logthis(glogchannel, LOG_DEBUG, "Point backwards");
      } else {
	 logthis(glogchannel, LOG_DEBUG, "Point in view plane");
      }
      /*double K2 = u * V.x + v * V.y + w * V.z; */
      /*double K4 = u * P.x + v * P.y + w * P.z; */
      /*double theta = K2 + F - K4;*/
      /*XC.x = P.x + theta * u;*/
      /*XC.y = P.y + theta * v;*/
      /*XC.z = P.z + theta * w;*/
      double K5=u*CC.x+v*CC.y+w*CC.z;
      double K4=u*PC.x+v*PC.y+w*PC.z; 
      double lambda=K5-K4;
      XC.x=PC.x+lambda*u;
      XC.y=PC.y+lambda*v;
      XC.z=PC.z+lambda*w;
   }
   double K1=u*(XC.x-VC.x)+v*(XC.y-VC.y)+w*(XC.z-VC.z);
   double K=1/(1-w*w);
   PTC->x=K*(v*(XC.x-VC.x)+u*(VC.y-XC.y));
   PTC->y=K*(XC.z-w*K1-VC.z);
   PTC->z=F-K1;
   logthis(glogchannel, LOG_WARNING, "Point Transformed");
   LOG_COORDS("Original Coords:\t ", PC);
   LOG_COORDS("Intersection Coords:\t ", XC);
   LOG_COORDS("Transformate Coords:\t", PT.coords);
   logthis(glogchannel, LOG_WARNING, "Traceable: %d", PT.traceable);
   return PT;   
}

void GTransform(Matrix *matrix, double angle) {
   /* TODO: C should not be used */
   Coords *pC=&matrix->C.coords;
   Coords CC=*pC;
   Coords VC=matrix->V.coords;
   pC->x=VC.x+(CC.x-VC.x)*cos(angle)+(CC.y-VC.y)*sin(angle);
   pC->y=VC.y+(CC.y-VC.y)*cos(angle)-(CC.x-VC.x)*sin(angle);
   pC->z=CC.z;
   matrix->d=unitvector(VC, (*pC));
}

void OldGTransform(Matrix *matrix, double angle) {
   /* TODO: C should not be used */
   Coords *pC=&matrix->C.coords;
   Coords CC=*pC;
   Coords VC=matrix->V.coords;
   pC->x=VC.x+(CC.x-VC.x)*cos(angle)+(CC.y-VC.y)*sin(angle);
   pC->y=VC.y+(CC.y-VC.y)*cos(angle)-(CC.x-VC.x)*sin(angle);
   pC->z=CC.z;
   matrix->d=unitvector(VC, (*pC));
}

void PTransform(Matrix *matrix, double angle) {
   /* TODO: C should not be used */
   Coords *pC=&matrix->C.coords;
   double cosine=cos(angle);
   double sine=sin(angle);
   Coords *pV=&matrix->V.coords;
   Vector *pd=&matrix->d;
   double u = pd->x * pd->x;
   double v = pd->y * pd->y;
   //double w = pd->z * pd->z;
   double f = modulus(vector(*pV, *pC));
   /* TODO: if ( u d.x < 0.0001L  && d.y < 0.0001L) { */
   double K = sqrt(u + v);
   pd->x = pd->x * (cosine+(pd->z/K)*sine);
   pd->y = pd->y * (cosine+(pd->z/K)*sine);
   pd->z = pd->z * cosine-K*sine;
   pC->x = pV->x + pd->x*f;
   pC->y = pV->y + pd->y*f;
   pC->z = pV->z + pd->z*f;
}

void OldPTransform(Matrix *matrix, double angle) {
   Coords *pC=&matrix->C.coords;
   Coords CC=*pC;
   Coords VC=matrix->V.coords;
   /* TODO: What happens when d.x and d.y == 0 */
   Vector d=matrix->d;
   if ( d.x < 0.0001L  && d.y < 0.0001L) {
printf ("############################\n");
printf ("############################\n");
	printf ("Time to quit\n");
printf ("############################\n");
printf ("############################\n");
   }
   double K=sqrt(CC.x*CC.x+CC.y*CC.y);
   pC->x=CC.x*(cos(angle)+(CC.z/K)*sin(angle));
   pC->y=CC.y*(cos(angle)+(CC.z/K)*sin(angle));
   pC->z=CC.z*cos(angle)-K*sin(angle);
   matrix->d=unitvector(VC, (*pC));
}

void ZoomTransform(Matrix *matrix, double zoom) {
   /* TODO: C should not be used */
   matrix->C.coords.x=matrix->C.coords.x+zoom*matrix->d.x;
   matrix->C.coords.y=matrix->C.coords.y+zoom*matrix->d.y;
   matrix->C.coords.z=matrix->C.coords.z+zoom*matrix->d.z;
   matrix->F+=zoom;
}

void MoveTransform(Matrix *matrix, int dir, int step) {
   /*double mod=modulus(vector(matrix->V,matrix->C));*/
   Coords *pV=&matrix->V.coords;
   Coords VC=(*pV);
   Coords *pC=&matrix->C.coords;
   Coords CC=(*pC);
   switch (dir) {
      case DIRHORIZON: 
         pC->x=CC.x+step*matrix->d.x;
	 pC->y=CC.y+step*matrix->d.y;
         pC->z=CC.z+step*matrix->d.z;
         pV->x=VC.x+step*matrix->d.x;
         pV->y=VC.y+step*matrix->d.y;
	 pV->z=VC.z+step*matrix->d.z;
         break;
      case DIRZENITH:
         pC->z=CC.z+step;
	 pV->z=VC.z+step;
         break;
      case DIRSIDE:
         pC->x=CC.x+step*matrix->d.y;
	 pC->y=CC.y-step*matrix->d.x;
         pV->x=VC.x+step*matrix->d.y;
         pV->y=VC.y-step*matrix->d.x;
         break;
      case DIRDOOM:
	 break;
      default: logthis(glogchannel, LOG_DEBUG, "I don't understand direction %i.", dir);
   }
   matrix->d=unitvector(*(pV), (*pC));
}

void WipeMatrix(Matrix *matrix) {
   assert(matrix);
   free(matrix);   
}

