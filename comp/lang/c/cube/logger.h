#ifndef __LOGGER_H__
#define __LOGGER_H__

/* Thread Unsafe Small Logger */

enum channel_list { console, systemlog, file, sock };
typedef enum channel_list channel; 

# define MAXLOGBUFFER 1000
void logthis(channel ch, int priority, const char *msg, ...);

#endif /* __LOGGER_H__ */

