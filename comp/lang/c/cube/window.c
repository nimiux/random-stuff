


Window NewWindow(Display *xd, , char *windowname, int y, int width, int height, int flag) {
   
   XSetWindowAttributes theWindowAttributes;
   unsigned long theWindowMask;
   XSizeHints theSizeHints;
   Window theNewWindow;
   /*Setting the attributes*/
   theWindowAttributes.border_pixel = BlackPixel(xd,DefaultWindow(xd));
   theWindowAttributes.background_pixel = WhitePixel(xd,DefaultWindow(xd));
   theWindowAttributes.override_redirect = True;
   theWindowMask = CWBackPixel|CWBorderPixel|CWOverrideRedirect;
   theNewWindow = XCreateWindow( xd, RootWindow(xd,DefaultWindow(xd)), x,y,width,height, BORDER_WIDTH,theDepth, InputOutput, CopyFromParent, theWindowMask, &theWindowAttributes);

   theSizeHints.flags = PPosition | PSize;
   theSizeHints.x = x;
   theSizeHints.y = y;
   theSizeHints.width = width;
   theSizeHints.height = height;
   XSetNormalHints(xd,theNewWindow,&theSizeHints);
              
	      
   size_hints.flags = PSize | PMinSize | PMaxSize;
   size_hints.min_width = width;
   size_hints.max_width = width;
   size_hints.min_height = height;
   size_hints.max_height = height;
   XSetStandardProperties(xd, window, window_name, icon_name, None, 0, 0, &size_hints);
   XSelectInput(xd, window, eventmask);
   return theNewWindow;
}

void DestroyWindow(Display *xd, Window w) {
   XDestroyWindow(xd,w);   
}

char *window_name = "Simple Window";
char *icon_name = "window";
static XSizeHints size_hints;
Window rootwin;

width=WINDOW_WIDTH;
height=WINDOW_HEIGHT;
display = XOpenDisplay(NULL);

if (display == NULL) {
   logthis(glogchannel, LOG_ERROR, "Failed to open display");
   return 0;
}

screen = DefaultScreen(display);
depth = DefaultDepth(display, screen);
rootwin = RootWindow(display, screen);
win = XCreateSimpleWindow(display, rootwin, 10, 10, width, height, 5,
BlackPixel(display, screen), BlackPixel(display, screen));
XMapWindow(display, win);

