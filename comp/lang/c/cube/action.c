/* X Events */

#include <stdio.h>
#include <stdlib.h>
#include <syslog.h> 

#include "action.h" 
#include "logger.h" 

extern  channel glogchannel;

#define TYPE(e) (e->type)
#define DISPLAY(e) (((XAnyEvent *)e)->display)
#define WINDOW(e) (((XAnyEvent *)e)->window)

int getaction (MyDisplay *disp) {
   Display *xdisp = disp->xdisp;
   XEvent e;
   XEvent *pe = &e;
   XNextEvent(xdisp, pe);
   int register type = TYPE(pe);
   if (eventarr[type]) {
      return eventarr[type](disp, pe);
   } else {
      logthis(glogchannel, LOG_DEBUG, "No event for: %i.", type);
      return noaction;
   }
}

Action processKeyPress (MyDisplay *disp, XEvent *e) {
   logthis(glogchannel, LOG_DEBUG, "KeyPress (Type: %i).....", TYPE(e));
   XKeyPressedEvent *kpe = (XKeyPressedEvent *) e;
   logthis(glogchannel, LOG_DEBUG, "Key with code %d was pressed",kpe->keycode);
   switch (kpe->keycode) {
      /* 1 key */
      case 10: return mup;
      /* ' key */
      case 20: return zoomin;
      /* ¡ key */
      case 21: return zoomout;
      /* q key */
      case 24: return mdown;
      /* e key */
      case 26: return mforward;
      /* s key */
      case 39: return mright;
      /* d key */
      case 40: return mbackward;
      /* f key */
      case 41: return mleft;
      /* x key */
      case 53: return quit;
      /* up key */
      case 98: return pdown;
      /* left key */
      case 100: return gleft;
      /* right key */
      case 102: return gright;
      /* down key*/
      case 104: return pup;
      default: return noaction;
   }
}
int processKeyRelease (MyDisplay *disp, XEvent *e) {
   logthis(glogchannel, LOG_DEBUG, "KeyRelease (Type: %i).....", TYPE(e));
   XKeyPressedEvent *kre = (XKeyReleasedEvent *) e;
   logthis(glogchannel, LOG_DEBUG, "Key with code %d was released",kre->keycode);
   return noaction;
}
int processButtonPress (MyDisplay *disp, XEvent *e) {
   logthis(glogchannel, LOG_DEBUG, "ButtonPress (Type: %i).....", TYPE(e));
   return noaction;
}
int processButtonRelease (MyDisplay *disp, XEvent *e) {
   logthis(glogchannel, LOG_DEBUG, "ButtonRelease (Type: %i).....", TYPE(e));
   return noaction;
}
int processMotionNotify(MyDisplay *disp, XEvent *e) {
   logthis(glogchannel, LOG_DEBUG, "MotionNotify (Type: %i).....", TYPE(e));
   return noaction;
}
int processEnterNotify (MyDisplay *disp, XEvent *e) {
   logthis(glogchannel, LOG_DEBUG, "EnterNotify (Type: %i).....", TYPE(e));
   return noaction;
}
int processLeaveNotify(MyDisplay *disp, XEvent *e) {
   logthis(glogchannel, LOG_DEBUG, "LeaveNotify (Type: %i).....", TYPE(e));
   return noaction;
}
int processKeymapNotify(MyDisplay *disp, XEvent *e) {
   logthis(glogchannel, LOG_DEBUG, "KeymapNotify (Type: %i).....", TYPE(e));
   return noaction;
}
int processExpose (MyDisplay *disp, XEvent *e) {
   logthis(glogchannel, LOG_DEBUG, "Expose (Type: %i).....", TYPE(e));
   return redraw;
}
int processGraphicsExpose (MyDisplay *disp, XEvent *e) {
   logthis(glogchannel, LOG_DEBUG, "GraphicsExpose (Type: %i).....", TYPE(e));
   return noaction;
}
int processNoExpose(MyDisplay *disp, XEvent *e) {
   logthis(glogchannel, LOG_DEBUG, "NoExpose (Type: %i).....", TYPE(e));
   return noaction;
}
int processCirculateRequest (MyDisplay *disp, XEvent *e) {
   logthis(glogchannel, LOG_DEBUG, "CirculateRequest (Type: %i).....", TYPE(e));
   return noaction;
}
int processConfigureRequest (MyDisplay *disp, XEvent *e) {
   logthis(glogchannel, LOG_DEBUG, "ConfigureRequest (Type: %i).....", TYPE(e));
   return noaction;
}
int processMapRequest (MyDisplay *disp, XEvent *e) {
   logthis(glogchannel, LOG_DEBUG, "MapRequest (Type: %i).....", TYPE(e));
   return noaction;
}
int processResizeRequest(MyDisplay *disp, XEvent *e) {
   logthis(glogchannel, LOG_DEBUG, "ResizeRequest (Type: %i).....", TYPE(e));
   return noaction;
}
int processCirculateNotify (MyDisplay *disp, XEvent *e) {
   logthis(glogchannel, LOG_DEBUG, "CirculateNotify (Type: %i).....", TYPE(e));
   return noaction;
}
int processConfigureNotify (MyDisplay *disp, XEvent *e) {
   logthis(glogchannel, LOG_DEBUG, "ConfigureNotify (Type: %i).....", TYPE(e));
   return noaction;
}
int processCreateNotify (MyDisplay *disp, XEvent *e) {
   logthis(glogchannel, LOG_DEBUG, "CreateNotify (Type: %i).....", TYPE(e));
   return noaction;
}
int processDestroyNotify (MyDisplay *disp, XEvent *e) {
   logthis(glogchannel, LOG_DEBUG, "DestroyNotify (Type: %i).....", TYPE(e));
   return noaction;
}
int processGravityNotify (MyDisplay *disp, XEvent *e) {
   logthis(glogchannel, LOG_DEBUG, "GravityNotify (Type: %i).....", TYPE(e));
   return noaction;
}
int processMapNotify (MyDisplay *disp, XEvent *e) {
   logthis(glogchannel, LOG_DEBUG, "MapNotify (Type: %i).....", TYPE(e));
   return noaction;
}
int processMappingNotify (MyDisplay *disp, XEvent *e) {
   logthis(glogchannel, LOG_DEBUG, "MappingNotify (Type: %i).....", TYPE(e));
   return noaction;
}
int processReparentNotify (MyDisplay *disp, XEvent *e) {
   logthis(glogchannel, LOG_DEBUG, "ReparentNotify (Type: %i).....", TYPE(e));
   return noaction;
}
int processUnmapNotify (MyDisplay *disp, XEvent *e) {
   logthis(glogchannel, LOG_DEBUG, "UnmapNotify (Type: %i).....", TYPE(e));
   return noaction;
}
int processVisibilityNotify(MyDisplay *disp, XEvent *e) {
   logthis(glogchannel, LOG_DEBUG, "VisibilityNotify (Type: %i).....", TYPE(e));
   return redraw;
}
int processColormapNotify(MyDisplay *disp, XEvent *e) {
   logthis(glogchannel, LOG_DEBUG, "ColormapNotify (Type: %i).....", TYPE(e));
   return noaction;
}
int processClientMessage (MyDisplay *disp, XEvent *e) {
   logthis(glogchannel, LOG_DEBUG, "ClientMessage (Type: %i).....", TYPE(e));
   return noaction;
}
int processPropertyNotify (MyDisplay *disp, XEvent *e) {
   logthis(glogchannel, LOG_DEBUG, "PropertyNotify (Type: %i).....", TYPE(e));
   return noaction;
}
int processSelectionClear (MyDisplay *disp, XEvent *e) {
   logthis(glogchannel, LOG_DEBUG, "SelectionClear (Type: %i).....", TYPE(e));
   return noaction;
}
int processSelectionNotify (MyDisplay *disp, XEvent *e) {
   logthis(glogchannel, LOG_DEBUG, "SelectionNotify (Type: %i).....", TYPE(e));
   return noaction;
}
int processSelectionRequest(MyDisplay *disp, XEvent *e) {
   logthis(glogchannel, LOG_DEBUG, "SelectionRequest (Type: %i).....", TYPE(e));
   return noaction;
}
int processFocusIn (MyDisplay *disp, XEvent *e) {
   logthis(glogchannel, LOG_DEBUG, "FocusIn (Type: %i).....", TYPE(e));
   return noaction;
}
int processFocusOut(MyDisplay *disp, XEvent *e) {
   logthis(glogchannel, LOG_DEBUG, "FocusOut (Type: %i).....", TYPE(e));
   return noaction;
}

void initevents () {
   eventarr[MapNotify] = (void *) processMapNotify;   
   eventarr[KeyPress] = (void *) processKeyPress;
   eventarr[KeyRelease] = (void *) processKeyRelease;
   eventarr[ButtonPress] = (void *) processButtonPress;
   eventarr[ButtonRelease] = (void *) processButtonRelease;
   eventarr[MotionNotify] = (void *) processMotionNotify;
   eventarr[EnterNotify] = (void *) processEnterNotify;
   eventarr[LeaveNotify] = (void *) processLeaveNotify;
   eventarr[KeymapNotify] = (void *) processKeymapNotify;
   eventarr[Expose] = (void *) processExpose;
   eventarr[GraphicsExpose] = (void *) processGraphicsExpose;
   eventarr[NoExpose] = (void *) processNoExpose;
   eventarr[CirculateRequest] = (void *) processCirculateRequest;
   eventarr[ConfigureRequest] = (void *) processConfigureRequest;
   eventarr[MapRequest] = (void *) processMapRequest;
   eventarr[ResizeRequest] = (void *) processResizeRequest;
   eventarr[CirculateNotify] = (void *) processCirculateNotify;
   eventarr[ConfigureNotify] = (void *) processConfigureNotify;
   eventarr[CreateNotify] = (void *) processCreateNotify;
   eventarr[DestroyNotify] = (void *) processDestroyNotify;
   eventarr[GravityNotify] = (void *) processGravityNotify;
   eventarr[MapNotify] = (void *) processMapNotify;
   eventarr[MappingNotify] = (void *) processMappingNotify;
   eventarr[ReparentNotify] = (void *) processReparentNotify;
   eventarr[UnmapNotify] = (void *) processUnmapNotify;
   eventarr[VisibilityNotify] = (void *) processVisibilityNotify;
   eventarr[ColormapNotify] = (void *) processColormapNotify;
   eventarr[ClientMessage] = (void *) processClientMessage;
   eventarr[PropertyNotify] = (void *) processPropertyNotify;
   eventarr[SelectionClear] = (void *) processSelectionClear;
   eventarr[SelectionNotify] = (void *) processSelectionNotify;
   eventarr[SelectionRequest] = (void *) processSelectionRequest;
   eventarr[FocusIn] = (void *) processFocusIn;
   eventarr[FocusOut] = (void *) processFocusOut;
}

