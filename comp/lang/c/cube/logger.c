/* Small Thread Unsafe Logger */

#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <syslog.h>

#include "logger.h"

char buffer [MAXLOGBUFFER];
channel glogchannel = console;
int glogpriority = LOG_WARNING;

void logthis(channel ch, int priority, const char *msg, ...) {

  if (priority <= glogpriority) {
    va_list args;
    va_start(args, msg);
 
     /* Insecure code beware of params */

    /* Formating final message to be printed */
    //if (args != (va_list) 0) 
    if (args != 0) 
      vsprintf(buffer, msg, args);
    else
      strcpy(buffer, msg);
   
    printf(strcat(buffer, "\n"));
    va_end(args);
  }
}
  /* file */
 /* if ( -1 == ( fdLog = open( pszLogFile, O_WRONLY | O_APPEND | O_CREAT | O_SYNC, 0644 ) ) ){
    perror( "Opening log file" );
    return;
  }
  lockf( fdLog, F_LOCK, 0 );
  write( fdLog, pszFinalMessage, strlen( pszFinalMessage ) );
  lockf( fdLog, F_ULOCK, 0 );

  if ( -1 == close( fdLog ) ){
    perror( "Closing log file" );
    return;
  } */
