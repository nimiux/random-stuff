/*#include <X11/X.h>*/
#include <syslog.h>
#include <stdlib.h>
#include <assert.h>

#include "logger.h" 
#include "xdisplay.h" 
//#include "events.h" 

extern channel glogchannel;

Display *getdisplay (char *displayname) {
   Display *xd;

   assert(displayname);
   XSetErrorHandler(errorhandler);
   xd = XOpenDisplay(displayname);
   if (xd != (Display *) 0) {
      printdisplayinfo(xd, DefaultScreen(xd));      
   }   
   return xd;
}

int getscreennumber(Display *xd) {
   assert(xd);
   return DefaultScreen(xd);
}

int printdisplayinfo (Display *xd, int scn) {
   
   unsigned long allpanes = 0; /*AllPanes; */
   int connectionnumber = ConnectionNumber(xd);
   int defdepth = DefaultDepth(xd,scn);
   /* todo */
   /*Window defrootwindow;*/
   /*Window rootwindow;*/
   /*Screen defscreeen;*/
   int screencount = ScreenCount(xd);
   int defscreenum = DefaultScreen(xd);
   int protoversion = ProtocolVersion (xd);
   int protorevision = ProtocolRevision (xd);;
   unsigned int displaywidth = DisplayWidth(xd, scn);
   unsigned int displayheight = DisplayHeight(xd, scn);
   
   assert(xd);
   logthis(glogchannel, LOG_WARNING,
	"Display info for: %s.\nAllPanes: %lu.\nConn Number. %i.\nDef. "
 	"Depth. %i.\nScreen count. %i.\n"
	"Default screen. %i.\nProtocol Version/Revision. %i/%i.\n."
	"Screen number %i.\nDisplay (W/H). (%u/%u).",
	 XDisplayName((char*)xd), allpanes, connectionnumber, 
	defdepth, screencount, defscreenum, protoversion, protorevision, 
	scn, displaywidth, displayheight);
   return 0;
}

	    
void closedisplay(Display *xd) {
   XFlush(xd);
   XCloseDisplay(xd);
   /*XFree(xd);*/
}

int errorhandler(Display *xdisp, XErrorEvent *xerr) {
   unsigned long serial;	/* serial number of failed request */
   unsigned char error_code;	/* error code of failed request */
   unsigned char request_code;	/* Major op-code of failed request */
   unsigned char minor_code;	/* Minor op-code of failed request */
   serial = xerr->serial;
   error_code= xerr->error_code;
   request_code= xerr->request_code;
   minor_code= xerr->minor_code;
   logthis(glogchannel, LOG_ERR, "Error at Display - Serial %lu \n- "
      "Error code was %i.\nRequest code was %i.\nMinor code: %i.\n", 
      serial, error_code, request_code, minor_code);
   exit(1);
}
