#ifndef __XDISPLAY_H__
#define __XDISPLAY_H__

#include <X11/Xlib.h>


Display *getdisplay (char  *displayname); 
int getscreennumber(Display *xd);
int printdisplayinfo (Display *xd, int scn);
void closedisplay(Display *xd);

int errorhandler(Display *xdisp, XErrorEvent *err);

#endif /* __XDISPLAY_H__ */
