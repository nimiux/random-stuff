#ifndef __WINDOW_H__
#define __WINDOW_H__

#include <X11/Xlib.h>

struct MyWindow {
   int screennumber; 
   Window xw;
   unsigned int height;
   unsigned int width;
};

typedef struct MyWindow MyWindow;

MyWindow CreateWindow(Display *xd, int screennumber, int width, int height);
GC CreateGC(Display *xd, MyWindow window, unsigned long valuemask, XGCValues *values);
void DestroyWindow(Display *xd, MyWindow window);


#endif /* __WINDOW_H__ */
