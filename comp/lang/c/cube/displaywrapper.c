#include <syslog.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include "logger.h"
#include "xdisplay.h"
#include "matrix.h"

#include "displaywrapper.h"

extern channel glogchannel;
extern void initevents();

MyDisplay *InitDisplay(char *dispname, int width, int height, Matrix *matrix) {

   char name[MAXDISPNAME+1];
   assert (dispname);
   assert (matrix);

   if (strlen(dispname) > MAXDISPNAME) {
      memcpy (name, dispname, MAXDISPNAME);
   } else {
      strcpy (name, dispname);
   }

   MyDisplay *disp=(MyDisplay *) malloc(sizeof(MyDisplay));
   if (disp != (MyDisplay *)0) {
      disp->xdisp=getdisplay(dispname);
      if (disp->xdisp == (Display *) 0) {
	 free(disp);
	 disp=(MyDisplay *) 0;
     } else {
	 /*int screen_num = DefaultScreen(disp->xdisp);*/
	 strcpy(disp->name, dispname);	 
	 disp->screennumber=getscreennumber(disp->xdisp);
	 disp->matrix=matrix;
	 disp->width=width;
	 disp->height=height;

	 /* move it to window */
	 int blackColor = BlackPixel(disp->xdisp, disp->screennumber);
	 int whiteColor = WhitePixel(disp->xdisp, disp->screennumber);

	 /*Window xwindow= XCreateSimpleWindow(xdisp,RootWindow(xdisp,screen_num),0,0,width,height, 0, blackColor, blackColor );*/
	 /* 20, 20, 10, CopyFromParent, CopyFromParent);*/

	 disp->window= XCreateSimpleWindow(disp->xdisp,DefaultRootWindow(disp->xdisp),0,0,width, height, 0, blackColor, whiteColor );
	 XMapWindow(disp->xdisp,disp->window);
	 XSelectInput(disp->xdisp, disp->window, ALLMASKS);

	 disp->gcontext=XCreateGC (disp->xdisp,disp->window,0,(0));
	 XSetForeground(disp->xdisp,disp->gcontext,blackColor);
	 /*GC defgc=DefaultGC(disp->xdisp, disp->screennumber);*/
	 disp->drawcallback=DrawScreen;
      }
   }
   if (disp == (MyDisplay *) 0) {
      logthis(glogchannel, LOG_ERR, "No pude abrir el display %s.", dispname);
   }
   return disp;
}

void InitEvents(MyDisplay *disp) {
   initevents(disp->xdisp); 
}

Action GetAction(MyDisplay *disp) {
   return getaction(disp);   
}

int ProcessAction(MyDisplay *disp, Action action) {
   int actiondone=1;
    switch (action) {
       case quit:  logthis(glogchannel, LOG_DEBUG, "Quitting...."); 
		  break;
       case redraw: logthis(glogchannel, LOG_DEBUG, "Redrawing...."); 
                    DrawScreen(disp);
                    break;
       case gleft:  logthis(glogchannel, LOG_DEBUG, "G-LEFT!!....");
                     DoGLeft(disp);
                   break;
       case gright:  logthis(glogchannel, LOG_DEBUG, "G-RIGHT!!....");
                      DoGRight(disp);
                    break;
       case pup:  logthis(glogchannel, LOG_DEBUG, "P-UP!!....");
                     DoPUp(disp);
                   break;
       case pdown:  logthis(glogchannel, LOG_DEBUG, "G-DOWN!!....");
                      DoPDown(disp);
                    break;
       case zoomin:  logthis(glogchannel, LOG_DEBUG, "ZOOM IN!!....");
                      DoZoomIn(disp);
                   break;
       case zoomout:  logthis(glogchannel, LOG_DEBUG, "ZOOM OUT!!....");
                       DoZoomOut(disp);
                   break;
       case mforward: logthis(glogchannel, LOG_DEBUG, "FORWARD!!....");
                        DoMoveForward(disp);
                        break;
       case mbackward:  logthis(glogchannel, LOG_DEBUG, "BACKWARD!!....");
                        DoMoveBackward(disp);
                        break;
       case mleft: logthis(glogchannel, LOG_DEBUG, "LEFT!!....");
                        DoMoveLeft(disp);
                        break;
       case mright:  logthis(glogchannel, LOG_DEBUG, "RIGHT!!....");
                        DoMoveRight(disp);
                        break;
       case mup:  logthis(glogchannel, LOG_DEBUG, "UP!!....");
                        DoMoveUp(disp);
			break;
       case mdown:  logthis(glogchannel, LOG_DEBUG, "DOWN!!....");
                        DoMoveDown(disp);
                        break;
       case noaction:  logthis(glogchannel, LOG_DEBUG, "Lazy dude!."); 
		     actiondone=0; 
		     break;
       default: logthis(glogchannel, LOG_DEBUG, "Action not supported: %d.", action);
	       actiondone=0; 
    }
    return actiondone;
}

void DrawLine(MyDisplay *disp, Coords *pa, Coords *pb) {
   double deltax=disp->width/2,deltay=disp->height/2;
   logthis(glogchannel, LOG_DEBUG,  "Drawing line from (%lf,%lf) to  (%lf,%lf).", 
	pa->x+deltax, deltay-pa->y, pb->x+deltax, deltay-pb->y);
   XDrawLine(disp->xdisp ,disp->window,disp->gcontext,pa->x+deltax,
	deltay-pa->y,pb->x+deltax,deltay-pb->y);

}

void DrawScreen(MyDisplay *disp) {
   /* TODO: Draw only changed region */
   /* TODO: Must be heavily factored */

   assert(disp);
   XClearWindow(disp->xdisp, disp->window);

   unsigned long i=0,npoints=disp->matrix->npoints;
   Grid *pgrid=&disp->matrix->grid; 

   Point p,op,dp;
   while (i < npoints ) {
      p=PlaneTranslation(disp->matrix, (*pgrid)[i++]);
      if (i == 1) {
         op=p;
      } else {
         dp=p;
         if (dp.traceable || op.traceable) {
            DrawLine(disp, &(op.coords), &(dp.coords));
         }
         op=dp;
      }
   }
   printview(disp->matrix);
}

void DoGLeft(MyDisplay *disp) {
   GTransform(disp->matrix, -G_STEP);
   DrawScreen(disp);
}

void DoGRight(MyDisplay *disp) {
   GTransform(disp->matrix, G_STEP);
   DrawScreen(disp);
}

void DoPUp(MyDisplay *disp) {
   PTransform(disp->matrix, -P_STEP);
   DrawScreen(disp);
}

void DoPDown(MyDisplay *disp) {
   PTransform(disp->matrix, P_STEP);
   DrawScreen(disp);
}

void DoZoomIn(MyDisplay *disp) {
   ZoomTransform(disp->matrix, -ZOOM_STEP);
   DrawScreen(disp);
}

void DoZoomOut(MyDisplay *disp) {
   ZoomTransform(disp->matrix, ZOOM_STEP);
   DrawScreen(disp);
}

void DoMoveForward(MyDisplay *disp) {
   MoveTransform(disp->matrix, DIRHORIZON, Y_STEP);
   DrawScreen(disp);
}

void DoMoveBackward(MyDisplay *disp) {
   MoveTransform(disp->matrix, DIRHORIZON, -Y_STEP);
   DrawScreen(disp);
}

void DoMoveLeft(MyDisplay *disp) {
   MoveTransform(disp->matrix, DIRSIDE, X_STEP);
   DrawScreen(disp);
}

void DoMoveRight(MyDisplay *disp) {
   MoveTransform(disp->matrix, DIRSIDE, -X_STEP);
   DrawScreen(disp);
}

void DoMoveUp(MyDisplay *disp) {
   MoveTransform(disp->matrix, DIRZENITH, Z_STEP);
   DrawScreen(disp);
}

void DoMoveDown(MyDisplay *disp) {
   MoveTransform(disp->matrix, DIRZENITH, -Z_STEP);
   DrawScreen(disp);
}

void EndDisplay(MyDisplay *disp) {
   closedisplay(disp->xdisp);
   free(disp);
}


