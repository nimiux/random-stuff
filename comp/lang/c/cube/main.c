
/* A 3D server
   a0w 2007 */

#include <stdio.h>
#include <stdlib.h>
#include <syslog.h>

#include "displaywrapper.h"
#include "logger.h"

extern channel glogchannel;

#define WIDTH 200
#define HEIGHT 100 

int main(int argc, char **argv) {

   if (argc == 1) {
      printf("Usage %s <world>\n", argv[0]);
      return 1;
   } else {
      MyDisplay *disp;
      Matrix *matrix = GetMatrix(argv[1]);
      if (matrix == (Matrix *) 0) exit(1);
   
      disp = InitDisplay(argc>2?argv[2]:":0", WIDTH, HEIGHT, matrix);
      if (disp == (MyDisplay *) 0) exit(2);
   
      InitEvents(disp);
      Action action = noaction;
      int result;
      while (action != quit) {
	 action = GetAction(disp);
	 logthis(glogchannel, LOG_DEBUG, "Got action: %d.", action);
	 result = ProcessAction(disp, action);
      }
      EndDisplay(disp);
      return result;
   }
}
