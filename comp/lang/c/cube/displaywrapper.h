#ifndef __DISPLAYWRAPPER_H__
#define __DISPLAYWRAPPER_H__

#include <X11/Xlib.h>

#include "matrix.h" 

/* Actions */
enum action { noaction, quit, redraw, gleft, gright, pup, pdown, zoomin, \
              zoomout, mforward, mbackward, mleft, mright, mup, mdown };

typedef enum action Action;

#define MAXDISPNAME 10

struct MyDisplay {
   char name[MAXDISPNAME+1];
   Display *xdisp; 
   int screennumber;
   Matrix *matrix;
   int width,height;
   Window window;
   GC gcontext;
   void (*drawcallback)(struct MyDisplay *);
};

typedef struct MyDisplay MyDisplay;

extern int getaction (MyDisplay *disp);

MyDisplay *InitDisplay(char *dispname, int width, int height, Matrix *matrix);
void InitEvents(MyDisplay *disp);
Action GetAction(MyDisplay *disp);
int ProcessAction(MyDisplay *disp, Action action);

void DrawScreen(MyDisplay *disp);
void DoGLeft (MyDisplay* disp);
void DoGRight (MyDisplay* disp);
void DoPUp (MyDisplay* disp);
void DoPDown (MyDisplay* disp);
void DoZoomIn(MyDisplay *disp);
void DoZoomOut(MyDisplay *disp);
void DoMoveForward(MyDisplay *disp);
void DoMoveBackward(MyDisplay *disp);
void DoMoveLeft(MyDisplay *disp);
void DoMoveRight(MyDisplay *disp);
void DoMoveUp(MyDisplay *disp);
void DoMoveDown(MyDisplay *disp);
void EndDisplay(MyDisplay *disp);



/* Event Masks */

#define ALLMASKS KeyPressMask | KeyReleaseMask | ButtonPressMask | ButtonReleaseMask | EnterWindowMask | LeaveWindowMask | PointerMotionMask 	| PointerMotionHintMask 	| Button1MotionMask | Button2MotionMask | Button3MotionMask | Button4MotionMask | Button5MotionMask | ButtonMotionMask | KeymapStateMask | ExposureMask 	| VisibilityChangeMask | StructureNotifyMask | ResizeRedirectMask | SubstructureNotifyMask | SubstructureRedirectMask | FocusChangeMask | PropertyChangeMask | ColormapChangeMask | OwnerGrabButtonMask


#endif /* __DISPLAYWRAPPER_H__ */
