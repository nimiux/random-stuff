#ifndef __MATRIX_H__
#define __MATRIX_H__

#include <math.h>

/* Angles */
#define PI  (3.1415926535897932384626433832795029L)
#define G_STEP (PI/16)  /* Gui�ada */
#define P_STEP (PI/16) /* Picado  */
#define A_STEP (PI/16) /* Alabeo  */

/* Direction */
enum direction { DIRHORIZON, DIRZENITH, DIRSIDE, DIRDOOM };

/* Distances */
#define ZOOM_STEP 10 /* Zoom */
#define Y_STEP 10 /* Y direction */
#define X_STEP 10 /* X direction */
#define Z_STEP 10 /* Z direction */

struct coords {
   double x;
   double y;
   double z;
};

typedef struct coords Coords;
typedef struct coords Vector;

struct point {
   int traceable;
   Coords coords;
};

typedef struct point Point; 

typedef Point Grid[9999];

struct matrix {
   Grid grid;
   Point V,C;
   Vector d;
   double F;
   unsigned long npoints;    
};

typedef struct matrix Matrix;

void printview(Matrix *matrix);

Matrix *GetMatrix(char *matrixfilename);
Point PlaneTranslation(Matrix *matrix, Point P);
void GTransform(Matrix *matrix, double angle);
void PTransform(Matrix *matrix, double angle);
void ZoomTransform(Matrix *matrix, double zoom);
void MoveTransform(Matrix *matrix, int dir, int step);
void WipeMatrix(Matrix *matrix);

#endif /* __MATRIX_H__ */
