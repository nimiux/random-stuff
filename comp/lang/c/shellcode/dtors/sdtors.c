#include <stdlib.h>
#include <stdio.h>

static void cleanup(void) __attribute__ ((destructor));

main () {
   printf("Main\n");   
}

void cleanup(void)  {
   printf("Cleanup\n");   
}
