#include <string.h>
#include <stdio.h>

unsigned long sp(void) {
   __asm__ ( "mov %esp, %eax" );
}

unsigned long bp(void) {
   __asm__ ( "mov %ebp, %eax" );
}

int main(int argc, char **argv) {
   char buffer[500];
   printf("ESP: 0x%x\n", sp());
   printf("EBP: 0x%x\n", bp());
   printf("UID:  %d\n", getuid());
   printf("EUID:  %d\n", geteuid());
   strcpy(buffer, argv[1]);
   return 0;
}
	 

