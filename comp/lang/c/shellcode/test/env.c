#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>

int main (int argc, char **argv) {

   if (argc > 1) {

      char *addr = getenv(argv[1]);

      if (addr)  { 
	 printf("DIR: %x\n", addr); 
	 printf("ST: %s\n", addr); 
      } else printf("NO WAY\n");
   } else printf ("Uso: %s VAR\n", argv[0]); 
}
