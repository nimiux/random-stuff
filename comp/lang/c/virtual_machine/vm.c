#include <stdio.h>
inline int idtCheck () {
   unsigned char m[6];
   __asm sidt m;
   printf("IDTR: %2.2x %2.2x %2.2x %2.2x %2.2x %2.2x\n", m[0], m[1],
   m[2], m[3], m[4], m[5]);
   return (m[5]>0xd0) ? 1 : 0;
}

int gdtCheck() {
   unsigned char m[6];
   __asm sgdt m;
   printf("GDTR: %2.2x %2.2x %2.2x %2.2x %2.2x %2.2x\n", m[0], m[1],
   m[2], m[3], m[4], m[5]);
   return (m[5]>0xd0) ? 1 : 0;
}

int ldtCheck() {
   unsigned char m[6];
   __asm sldt m;
   printf("LDTR: %2.2x %2.2x %2.2x %2.2x %2.2x %2.2x\n", m[0], m[1],
   m[2], m[3], m[4], m[5]);
   return (m[0] != 0x00 && m[1] != 0x00) ? 1 : 0;
}

int main( int argc, char * argv[]) {
   idtCheck();
   gdtCheck();
   if (ldtCheck()) {
      printf("Virtual Machine detected.\n");
   } else {
      printf("Native machine detected.\n");
   }
   return 0;
} 
