extern "C" {
   #include <sys/types.h>
   #include <sys/stat.h>
   #include <stdio.h>
   #include <errno.h>
   #include <stdlib.h>
   #include <string.h>
   #include <fcntl.h>
   #include <signal.h>
   #include <syslog.h>
   #include <unistd.h>
}

#include <iostream.h>

#include <CAplicacion.h>

sig_atomic_t gbkeeprunning                                = true;
sig_atomic_t gbrestart                                    = false;

CAplicacion * gpxapp                                      = NULL; 
extern char gszAppName[];
char gszstatusfile[128]                                   = "";

#define DAEMON_DEFAULT_POLLING_TIME                         5

static void reportstatus() {
   int fd;
   gpxapp->GetLogger()->WriteLine(CLogger::FULL, _FILE_, _LINE_
                   , "Se genera el fichero de estado: %s."
                   , gszstatusfile);
   if ((fd = open(gszstatusfile,O_RDWR|O_CREAT,0640)) < 0) { 
      gpxapp->GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                   , "No se puede generar el fichero de estado: %s."
                   , gszstatusfile);
   } else {
      close(fd);
   } 
}

static void sendmailwritesyslog(char *szmessage) {
   assert(szmessage);
   // Send mail
   char szemaillist[512];
   char szsubject[512];
   char szmailmessage[1024];
   CUtils::GetString("GATEWAYGOLS_ALERT_EMAIL_LIST", szemaillist);
   if (!szemaillist[0]) {
      gpxapp->GetLogger()->WriteLine(CLogger::WARNING, _FILE_, _LINE_
		                  , "No se puede obtener la lista de correo para "
                          "la notificación del error. Se envia a root.");
      strcpy(szemaillist, "root");
   } 
   sprintf(szsubject, "Error en aplicación %s. Código de Banco: %s"
                    , gszAppName
                    , gpxapp->GetCodigoBanco());
   sprintf(szmailmessage, "%s%s.%s%s\n\n%s. %s\n"
                        , "Este mensaje ha sido generado "
                          "automáticamente por la aplicación: "
                        , gszAppName    
                        , " Para la entidad bancaria: "
                        , gpxapp->GetCodigoBanco()
                        , GETDATETIME
                        , szmessage);

   if (!CUtils::SendMail(gpxapp->GetLogger(), szemaillist, szsubject
                        , szmailmessage)) {
      gpxapp->GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
		                  , "Se ha producido un error al notificar el fallo de "
                          "la aplicación a la lista de correo.");
   }
   // Notify SYSLOG
   char szappid[50] = "";
   sprintf(szappid, "%s[%ld]", gszAppName, getpid());
   closelog();
   openlog(szappid, LOG_CONS, LOG_DAEMON);
   syslog(LOG_ALERT, szmessage);
   closelog();
}

static void reportgolstimeout(bool btimeoutgols) {
   if (btimeoutgols) {
      char szmessage[]          = "Se ha producido un error irrecuperable en "
                                  "el acceso a GOLS. La aplicación ha sido "
                                  "abortada. Consultar el log de la "
                                  "aplicación: ";
      char szfinalmessage[1024] = "";
      sprintf(szfinalmessage, "%s\"%s\".", szmessage
                                    , gpxapp->GetLogger()->GetLogFileName());
      gpxapp->GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
            , "Se ha producido un timeout en GOLS. Se notifica SYSLOG. "
              "Se aborta la aplicación.");
      sendmailwritesyslog(szfinalmessage);
      exit(1);
   }
}
static void reporterrorlogpayment(bool bpaymentlogged) {
   if (!bpaymentlogged) {
      char szmessage[]          = "Se ha producido un error irrecuperable al "
                                  "escribir el log de pagos. La aplicación "
                                  "ha sido abortada. Consultar el log de la "
                                  "aplicación: ";
      char szfinalmessage[1024] = "";
      sprintf(szfinalmessage, "%s\"%s\".", szmessage
                            , gpxapp->GetLogger()->GetLogFileName());
      gpxapp->GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                     , "Se ha producido un error al escribir el log de pagos. "
                       "Se notifica SYSLOG. Se aborta la aplicación.");
      sendmailwritesyslog(szfinalmessage);
      exit(1);
   }
}

static void reportmqserieserror() {
   char szmessage[]            = "Se ha producido un error irrecuperable en "
                                 "el acceso a MQSeries. Consultar el log de "
                                 "la aplicación: ";
   char szfinalmessage[1024] = "";
   sprintf(szfinalmessage, "%s\"%s\".", szmessage
                                 , gpxapp->GetLogger()->GetLogFileName());
   gpxapp->GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
         , "Se ha producido un error irrecuperable en el acceso a MQSeries. "
           "Se notifica SYSLOG. Se aborta la aplicación.");
   sendmailwritesyslog(szfinalmessage);
}


extern "C" {
   static void signalHandler(int wSignal);
}

static void signalHandler( int wSignal ) {
   bool bteststatus          = false;
   bool bdisconnectgols      = false;
   bool bkeeprunning         = true;
   bool brestart             = false;
   bool berror               = false;
   char szlogmessage[128]    = "";
   char szsignalname[12]     = "";
   
	switch (wSignal) {
      case SIGHUP:/* hangup */
         /* Usada para rearrancar la aplicación */
         brestart = true;
         strcpy(szsignalname, "SIGHUP");
         signal(SIGHUP,    signalHandler);  // Se reactiva la señal
         break;
      case SIGTERM:/* software termination signal from kill */
         strcpy(szsignalname, "SIGTERM");
         bkeeprunning=false;
         signal(SIGTERM,    signalHandler);  // Se reactiva la señal
         break;
      case SIGSEGV:/* segmentation violation */
         strcpy(szsignalname, "SIGSEGV");
         break;
      case SIGBUS:/* bus error */
         strcpy(szsignalname, "SIGBUS");
         break;
      case SIGINT:/* interrupt (rubout) */
         strcpy(szsignalname, "SIGINT");
         break;
      case SIGQUIT:/* quit (ASCII FS) */
         strcpy(szsignalname, "SIGQUIT");
         break;
      case SIGTRAP:/* trace trap (not reset when caught) */
         strcpy(szsignalname, "SIGTRAP");
         break;
      case SIGABRT:/* used by abort, replace SIGIOT in the future */
         strcpy(szsignalname, "SIGABRT");
         break;
      case SIGSYS:/* bad argument to system call */
         strcpy(szsignalname, "SIGSYS");
         break;
      case SIGUSR1:/* user defined signal 1 */
         /* Usada para comprobar el estado de la aplicación */
         bteststatus = true;
         strcpy(szsignalname, "SIGUSR1");
         signal(SIGUSR1,    signalHandler);  // Se reactiva la señal
         break;
      case SIGUSR2:/* user defined signal 2 */
         /* Usada para desconectarse de GOLS */
         bdisconnectgols = true;
         strcpy(szsignalname, "SIGUSR2");
         signal(SIGUSR2,    signalHandler);  // Se reactiva la señal
         break;
      case SIGLWP:/* special signal used by thread library */
         strcpy(szsignalname, "SIGLWP");
         break;
      case SIGCANCEL: /* thread cancellation signal used by libthread */
         strcpy(szsignalname, "SIGCANCEL");
         break;
      case SIGXCPU:/* exceeded cpu limit */
         strcpy(szsignalname, "SIGXCPU");
         break;
      case SIGXFSZ:/* exceeded file size limit */
         strcpy(szsignalname, "SIGXFSZ");
         break;
      case SIGILL:/* illegal instruction (not reset when caught) */
         strcpy(szsignalname, "SIGILL");
         break;
      default: sprintf(szsignalname, "%ld", wSignal);
   }
   sprintf(szlogmessage, "La aplicación ha recibido la señal: %s"
                       , szsignalname);
	berror = (!bkeeprunning || brestart || bteststatus || bdisconnectgols);
   if (gpxapp->GetLogger() != NULL) {
      gpxapp->GetLogger()->WriteLine((berror?CLogger::MANDATORY:CLogger::ERROR)
                                    , _FILE_, _LINE_
                                    , szlogmessage);
   }
   if (bdisconnectgols) {
      gpxapp->GetLogger()->WriteLine(CLogger::MANDATORY, _FILE_, _LINE_
                       , "Se ha solicitado la desconexión a la pasarela GOLS.");
      gpxapp->DesconectarGOLS();
   } else {
      if (bteststatus) {
         gpxapp->GetLogger()->WriteLine(CLogger::MANDATORY, _FILE_, _LINE_
                              , "Se ha solicitado el estado de la aplicación.");
         reportstatus();
      } else {
	      if (!bkeeprunning) {
             gbkeeprunning = bkeeprunning;
             gbrestart = false;
             gpxapp->GetLogger()->WriteLine(CLogger::MANDATORY, _FILE_, _LINE_
                        , "Se ha solicitado la terminación de la aplicación.");
         } else { 
            if (brestart) {
               gbrestart = brestart;
               gbkeeprunning = true;
               gpxapp->GetLogger()->WriteLine(CLogger::MANDATORY, _FILE_, _LINE_
                         , "Se ha solicitado el rearranque de la aplicación.");
            } else {
               gpxapp->GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                                   , "La aplicación ha terminado con error." );
               sendmailwritesyslog(szlogmessage);
               cout << szlogmessage << endl;
               SAFE_DELETE(gpxapp);
               exit(1);
            }
         }
      }
   }
}

void daemonize(char*szpathpidfile, char *szpidfile) {
   int i,lfp;
   char szlockfile[64];
   char str[10];
   if(getppid()==1) {
      return; /* already a daemon */
   }
   i=fork();
   if (i<0) {
      exit(1); /* fork error */
   }
   if (i>0) {
      _exit(0); /* parent exits */
   }
   /* child (daemon) continues */
   if (setsid() < 0) { /* obtain a new process group */
      exit(-1);
   }      
   i=fork();
   if (i<0) {
      exit(1); /* fork error */
   }
   if (i>0) {
      _exit(0); /* parent exits */
   }
   /* child (daemon) continues */
   /* close all descriptors */
   for (i=getdtablesize();i>=0;--i)  {
      close(i);
   }
   i=open("/dev/null",O_RDWR);
   dup(i);
   dup(i); /* handle standard I/O */
   umask(027); /* set newly created file permissions */
   if (chdir(szpathpidfile)) { /* change running directory */
      exit(1);
   }
   lfp=open(szpidfile,O_RDWR|O_CREAT,0640);
   if (lfp<0) {
		exit(1); /* can not open */
   }		 
   if (lockf(lfp,F_TLOCK,0)<0) {
      exit(0); /* can not lock */
   }

   sprintf(str,"%d\n",getpid());
   write(lfp,str,strlen(str)); /* record pid to lockfile */

   signal(SIGHUP,    signalHandler); /* catch hangup signal */
   signal(SIGINT,    signalHandler);
   signal(SIGQUIT,   signalHandler);
   signal(SIGILL,    signalHandler);
   signal(SIGTRAP,   signalHandler);
   signal(SIGABRT,   signalHandler);
   signal(SIGEMT,    signalHandler);
   signal(SIGFPE,    signalHandler);
   signal(SIGBUS,    signalHandler);
   signal(SIGSEGV,   signalHandler);
   signal(SIGSYS,    signalHandler); 
   signal(SIGPIPE,   SIG_DFL); /* write on a pipe with no one to read it */ 
   signal(SIGALRM,   SIG_DFL); /* alarm clock */ 
   signal(SIGTERM,   signalHandler); /* catch kill signal */
   signal(SIGUSR1,   signalHandler);
   signal(SIGUSR2,   signalHandler);
   signal(SIGCHLD,   SIG_IGN); /* ignore child */
   signal(SIGPWR,    SIG_DFL); /* power-fail restart */
   signal(SIGWINCH,  SIG_DFL); /* window size change */
   signal(SIGTSTP,   SIG_IGN); /* ignore tty signals */
	signal(SIGCONT,   SIG_DFL); /* stopped process has been continued */
   signal(SIGTTOU,   SIG_IGN);
   signal(SIGTTIN,   SIG_IGN);
   signal(SIGXCPU,   signalHandler); 
   signal(SIGXFSZ,   signalHandler);
   signal(SIGLWP,    signalHandler);
}

int run(char *szbankcode, char *szinputqueue) {
   // Ejecuta la aplicación de pagos.
   // Los valores de salida son:
   //   0  Ejecución correcta
   //   1  No se puede crear el fichero de log
   //   2  MQSeries no disponible 
   //   3  Error en acceso a MQSeries
   int nreturn         = 0;
   bool btimeoutgols   = false;
   bool bpaymentlogged = true;
   gbkeeprunning       = true;

   do {
      gbrestart = false;
      gpxapp = new CAplicacion();
      int nappinit = gpxapp->Inicializar(CAplicacion::ID_DAEMON_APP_TYPE
                                        , szbankcode
                                        , szinputqueue
                                        , &btimeoutgols);
      reportgolstimeout(btimeoutgols);
      if ((nappinit == CAplicacion::ID_RETURN_ERRORLOGINIT) ||
          (nappinit == CAplicacion::ID_RETURN_INVALIDBANKCODE)) {
         nreturn = 1; 
      } else {
         if ((nappinit == CAplicacion::ID_RETURN_INITOK) ||
             (nappinit == CAplicacion::ID_RETURN_ERRORGOLSINIT)) {    
            nreturn=gpxapp->ProcesaColaLog(&btimeoutgols, &bpaymentlogged);
            nreturn=(nreturn?0:3);
            reportgolstimeout(btimeoutgols);
            reporterrorlogpayment(bpaymentlogged);
            // Intenta obtener el tiempo de espera entre llamadas a 
            // CAplicacion::Run
            char szdaemonpollingtime[128];
            unsigned int ndaemonpollingtime = 0;
            CUtils::GetString((char *)"CPGDAEMON_POLLINGTIME"
                             , szdaemonpollingtime);
            if (szdaemonpollingtime[0]) {
               ndaemonpollingtime = atoi(szdaemonpollingtime);
            } else {
               gpxapp->GetLogger()->WriteLine(CLogger::INFO, _FILE_, _LINE_
                           , "No se puede obtener el tiempo de polling de la "
                       "cola de entrada. Se toma el valor por defecto: %ld."
                     , DAEMON_DEFAULT_POLLING_TIME);
            }
            ndaemonpollingtime = (ndaemonpollingtime?ndaemonpollingtime
                                 :DAEMON_DEFAULT_POLLING_TIME);
            while (!nreturn && gbkeeprunning && !gbrestart) {
               nreturn = (gpxapp->Run(&btimeoutgols, &bpaymentlogged)?0:3);
               reporterrorlogpayment(bpaymentlogged);
               reportgolstimeout(btimeoutgols);
               if (!nreturn) {
                  gpxapp->GetLogger()->WriteLine(CLogger::INFO, _FILE_, _LINE_
                            , "Esperando %ld segundos.", ndaemonpollingtime);
                  sleep(ndaemonpollingtime);
               }
            }
         } else {
            if (nappinit == CAplicacion::ID_RETURN_ERRORMQSERIESINIT) {    
               gpxapp->GetLogger()->WriteLine(CLogger::FULL, _FILE_, _LINE_
                       , "MQSeries no disponible. No se realizan acciones.");
               nreturn = 2;
            } else {
               gpxapp->GetLogger()->WriteLine(CLogger::FULL, _FILE_, _LINE_
               , "Ni GOLS ni MQSeries disponibles. No se realizan acciones.");
               nreturn = 2;
            }
         }
      }
      gpxapp->GetLogger()->WriteLine(CLogger::MANDATORY, _FILE_, _LINE_
                                  , "Fin de la ejecución de la aplicación.");
      if (nreturn == 3) {
         // Se ha producido un error serio en el acceso a MQSeries
         reportmqserieserror();
      }
      SAFE_DELETE(gpxapp);
   } while (!nreturn && gbrestart);
   return nreturn;
}

int main(int argc, char *argv[]) {
   char szcwd[128];
   char szbankcode[128];
   char szinputqueue[128];
   char szpidpath[128];
   char szpidfile[128];
   strcpy(gszAppName, CUtils::GetFileName(argv[0]));
   if (argc < 3) {
      cout << "Número incorrecto de parámetros" << endl;
      cout << " Uso: " << gszAppName << " <codigo_banco> <cola_de_entrada>" 
           << endl;
      return -1;
   }
   strcpy(szbankcode, argv[1]);
   strcpy(szinputqueue, argv[2]);
   if ((strlen(szbankcode) != 4) || (!CUtils::IsANumber(szbankcode))) {
      cout << "Código de banco inválido: " << szbankcode <<
              ". No se puede arrancar el demonio de pagos." << endl;
      return -1;
   }
   sprintf(szpidfile, "%s.%s.pid", gszAppName, szbankcode);
   sprintf(gszstatusfile, "%s.%s.status", gszAppName, szbankcode);
   getcwd(szcwd, 128);
   CUtils::GetString("CPGDAEMON_PIDDIR", szpidpath);
   if (!szpidpath[0]) {
      cout << "No se puede obtener el valor de la variable "
              "CPGDAEMON_PIDDIR. No se puede arrancar el demonio de "
              "pagos." 
           << endl;
      return -1;
   } else {
      if (chdir(szpidpath)) {
         cout << "No se puede usar el directorio: " << szpidpath << 
                 "como directorio de trabajo." << endl;
         perror("Error: ");
         return -1;
      } else {
         int fd;
         if ((fd = open(szpidfile,O_RDWR|O_CREAT,0640)) < 0) { 
            cout << "No se puede escribir en el directorio: " << szpidpath 
                 << endl;
            perror("Error: ");
            return -1;
         } else {
            close(fd);
            remove(szpidfile);
            chdir(szcwd);
            daemonize(szpidpath, szpidfile);
            /* run */
            return run(szbankcode, szinputqueue);
         }
      }
   }
}
