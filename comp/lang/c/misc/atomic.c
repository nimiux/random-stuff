

#ifndef _BITS_LIBC_ATOMIC_H
#define _BITS_LIBC_ATOMIC_H1

#include <spin-lock.h>

/* According to the Intel documentation the following operations are
   atomic on the Intel Architecture processors:

   - Reading or writing a byte.
   - Reading or writing a word aligned on a 16-bit boundary.
   - Reading or writing a doubleword aligned on a 32-bit boundary.

   The compiler takes care of proper alignment so we don't have to
   worry about unaligned access.

   Bus-locking may be necessary for certain operations to be atomic on
   multiprocessor machines.  According to the Intel documentation this
   is necessary for the following operations:

   - The bit test and modify instructions (BTS, BTR and BTC).
   - The exchange instructions (XADD, CMPXCHG, CMPXCHG8B).
   - The following single-operand arithmetic and logical instructions:
   INC, DEC, NOT, and NEG.
   - The following two-operand arithmetic and logical instructions:
   ADD, ADC, SUB, SBB, AND, OR, and XOR.

   The XCHG instruction is automatically locked.  */

/* Define an atomic variable NAME of type TYPE with storage class
   CLASS.  The operations that must be atomic must be specified with
   __libc_atomic_operations.  Use `extern' for CLASS to declare an
   atomic variable defined in another module.  */
#define __libc_atomic_define(CLASS, TYPE, NAME) \
  CLASS __spin_lock_t NAME##_lock; \
  CLASS volatile TYPE NAME

/* Define that the atomic variable NAME will be operated upon by the
   operations specified in OPERATIONS.  */
#define __libc_atomic_operations(NAME, OPERATIONS) \
  enum { NAME##_operations = OPERATIONS };

/* The operations that can be specified in the last argument to
   __libc_atomic_operations.  These should be OR'd together.  */
#define __LIBC_ATOMIC_INC0
#define __LIBC_ATOMIC_DEC0
#define __LIBC_ATOMIC_ADD       0
#define __LIBC_ATOMIC_XADD1 /* The i386 lacks XADD and CMPXCHG.  */
#define __LIBC_ATOMIC_CMPXCHG1

/* Increment NAME.  */
#define __libc_atomic_inc(NAME) \
  if (NAME##_operations || sizeof (NAME) != 4)                                \
    {                                                                         \
      __spin_lock (&(NAME##_lock));                                           \
      ++NAME;                                                                 \
      __spin_unlock (&(NAME##_lock));                                         \
    }                                                                         \
  else                                                                        \
    __asm__ __volatile__ ("lock; incl %0" : : "m" (NAME) : "memory")

/* Decrement NAME.  */
#define __libc_atomic_dec(NAME) \
  if (NAME##_operations || sizeof (NAME) != 4)                                \
    {                                                                         \
      __spin_lock (&(NAME##_lock));                                           \
      --NAME;                                                                 \
      __spin_unlock (&(NAME##_lock));                                         \
    }                                                                         \
  else                                                                        \
    __asm__ __volatile__ ("lock; incl %0" : : "m" (NAME) : "memory")

/* Add VAL to NAME.  */
#define __libc_atomic_add(NAME, VAL) \
  if (NAME##_operations || sizeof (NAME) != 4)                                \
    {                                                                         \
      __spin_lock (&(NAME##_lock));                                           \
      NAME += VAL;                                                            \
      __spin_unlock (&(NAME##_lock));                                         \
    }                                                                         \
  else                                                                        \
    __asm__ __volatile__ ("lock; addl %0,%1"                                  \
  : : "ir" (VAL) "m" (NAME) : "memory")

/* Add VAL to NAME, and return the previous value of NAME.  */
#define __libc_atomic_xadd(NAME, VAL) \
  ({ __typeof (NAME) __result;                                                \
     __spin_lock (&(NAME##_lock));                                            \
     __result = NAME;                                                         \
     NAME += VAL;                                                             \
     __spin_unlock (&(NAME##_lock));                                          \
     result; })

/* Assign NEWVAL to NAME iff the current value of NAME is OLDVAL.
   Return nonzero if the operation succeeded.  */
#define __libc_atomic_cmpxchg(NAME, OLDVAL, NEWVAL) \
  ({ int __result = 0;                                                        \
     __spin_lock (&(NAME##_lock));                                            \
     if (NAME == OLDVAL)                                                      \
       NAME = NEWVAL, __result = 1;                                           \
     __spin_unlock (&(NAME##_lock));                                          \
     __result; })

#endif /* bits/libc-atomic.h */