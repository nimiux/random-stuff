/******************************************************************************
 * ++
 * Author       : Jose Maria Alonso 
 *
 * Module Name  : multiproc.c
 *
 * Description  :
 *
 * Status Diagrams :
 *
 * Mod. History :
 *
 * AUTHOR               DATE            DESCRIPTION
 * ----------------     --------------  ------------------------------------
 * Jose Maria Alonso 
 *
 * --
 ******************************************************************************/
#ifndef _MULTIPROC_C
#define _MULTIPROC_C

/******************************************************************************
 * Standards and Complance                                                    *
 ******************************************************************************/

/******************************************************************************
 * System Include Files                                                       *
 ******************************************************************************/
/*#include <stdlib.h>*/
/*#include <stdio.h>*/

/******************************************************************************
 * Application Include Files                                                  *
 ******************************************************************************/
#include "multiproc.h" 

/******************************************************************************
 * Debug and Trace Options                                                    *
 ******************************************************************************/
#ifdef TRACE_MAIN
#define TRACE(x) x
#else
#define TRACE(x)
#endif

/******************************************************************************
 * Macro Definitions                                                          *
 ******************************************************************************/
DEFINIR_TEMPORIZADOR( EJECUTAR_PROCESO )
DEFINIR_TEMPORIZADOR( EJECUTAR_SUBPROCESO )

/******************************************************************************
 * Type Definitions                                                           *
 ******************************************************************************/

/******************************************************************************
 * Global Variables                                                           *
 ******************************************************************************/

/******************************************************************************
 * Local Function Prototypes                                                  *
 ******************************************************************************/
static int ejecutarPadre     ( const char *nombProceso,
                               long opciones,
                               long numProcesos,
                               long numFicheros,
                               const char *nombFichero,
                               const char *conexionData);

static int ejecutarHijo      ( const char *nombProceso,
                                long idProceso,
                                long opciones,
                                long minCodigo,
                                long maxCodigo,
                                long numFicheros,
                                const char *nombFichero,
                                const char *conexionData);

static int imprimirLogo  ( const char *nombProceso );
static int imprimirAyuda ( const char *nombProceso );

/******************************************************************************
 * Local Function                                                             *
 ******************************************************************************/

/******************************************************************************
 * Main                                                                       *
 ******************************************************************************/
/*
 * main:
 *
 * Punto de entrada del proceso.
 *
 * [in] argc:      Numero de argumentos de la linea de comandos.
 * [in] argv:      Puntero al arreglo de argumentos.
 *
 * [retorno]:      Devuelve el codigo de error resultante de la
 *                 ejecucion del proceso.
 */
int main( int argc, char *argv[] )
{
	long opciones;
	long numProcesos;
	long numFicheros;
	long idProceso;
	long minCodigo;
	long maxCodigo;

	int i;

	/*
	 * Analiza la linea de comandos
	 */

	if (argc < PPAL_NUMERO_MINNUMARGS)
	{
		return ERROR( ERR_ARGINV );
	}

	*nombFichero = '\0';

	*conexionPlla = '\0';
	*conexionData = '\0';
	*conexionTtes = '\0';

	opciones = PPAL_OPCION_NING;

	numProcesos = 0;
	numFicheros = 0;

	idProvincia = 0;
	idProceso = 0;

	minCodigo = 0;
	maxCodigo = 0;

	for (i = 1; i < argc; i++)
	{
		if ((argv[i][0] == '-') || (argv[i][0] == '/'))
		{
			switch (tolower( argv[i][1] ))
			{
			case '?':       /* Muestra el texto de ayuda */
			case 'h':

				imprimirAyuda( argv[0] );
				return ERROR( ERR_NING );

			case 's':       /* Indica ejecucion en modo silencioso */

				opciones |= PPAL_OPCION_MODO_SILEN;
				break;

			case 'f':       /* Indica el nombre del fichero a generar */

				if ((++i == argc) || *nombFichero)
				{
					imprimirAyuda( argv[0] );
					return ERROR( ERR_ARGINV );
				}

				strncpy( nombFichero, argv[i], PPAL_LGO_NOMBFICHERO );
				break;

			case 'c':       /* Cadenas de conexion */

				if ((++i == argc) || *conexionData)
				{
					imprimirAyuda( argv[0] );
					return ERROR( ERR_ARGINV );
				}

				strncpy( conexionData, argv[i], PPAL_LGO_CADENACONEXION );
				break;

			case 'n':          /* Numeros */

				switch (tolower( argv[i][2] ))
				{
				case 'p':      /* Numero de procesos */

					if ((++i == argc) || numProcesos)
					{
						imprimirAyuda( argv[0] );
						return ERROR( ERR_ARGINV );
					}

					numProcesos = atol( argv[i] );
					break;

				case 'f':      /* Numero de ficheros */

					if ((++i == argc) || numFicheros)
					{
						imprimirAyuda( argv[0] );
						return ERROR( ERR_ARGINV );
					}

					numFicheros = atol( argv[i] );
					break;

				default:       /* Opcion invalida */

					imprimirAyuda( argv[0] );
					return ERROR( ERR_ARGINV );
				}
				break;

			case 'i':       /* Identificador del proceso */

				if ((++i == argc) || idProceso)
				{
					imprimirAyuda( argv[0] );
					return ERROR( ERR_ARGINV );
				}

				idProceso = atol( argv[i] );
				break;

			default:        /* Opcion invalida */

				imprimirAyuda( argv[0] );
				return ERROR( ERR_ARGINV );
			}
		}
		else /* Opcion invalida */
		{
			imprimirAyuda( argv[0] );
			return ERROR( ERR_ARGINV );
		}
	}

	if (!*conexionData ||
	    !*nombFichero ) 
	{
		imprimirAyuda( argv[0] );
		return ERROR( ERR_ARGINV );
	}

	/*
	 * Ejecuta el proceso.
	 */

	if (!minCodigo && !maxCodigo)
	{
		numProcesos = MAX( MIN( numProcesos, PPAL_NUMERO_MAXNUMPROCS ),
		                                     PPAL_NUMERO_MINNUMPROCS );
		numFicheros = 1;

		return ejecutarPrincipal( argv[0],
		                          opciones, 
				          numProcesos,
		                          numFicheros,
		                          nombFichero,
		                          conexionData);
	}
	else
	{
		if (!numFicheros)
		{
			imprimirAyuda( argv[0] );
			return ERROR( ERR_ARGINV );
		}

		return ejecutarSecundario( argv[0],
		                           idProceso,
		                           opciones,
		                           minCodigo,
		                           maxCodigo,
		                           numFicheros,
		                           nombFichero,
		                           conexionPlla,
		                           conexionData,
		                           conexionTtes );
	}
}



/* ----------------------------------------------------------------------------
 * Definicion de las funciones privadas del modulo
 * ------------------------------------------------------------------------- */

/*
 * ejecutarPadre:
 *
 * Ejecuta 1 o mas instancias del proceso, con los parametros dados.
 *
 * [in] nombProceso:   Puntero al nombre del proceso.
 * [in] opciones:      Opciones generales del proceso.
 * [in] numProcesos:   Numero de procesos a ejecutar.
 * [in] numFicheros:   Numero de ficheros a generar.
 * [in] nombFichero:   Puntero al nombre del fichero de salida.
 * [in] conexionData:  Puntero a la cadena de conexion con los
 *                     datos de entrada.
 *
 * [retorno]:   Devuelve 0 si la ejecucion de todos los subprocesos
 *              fue exitosa.  Si no se pueden levantar los subprocesos,
 *              devuelve el codigo asociado a la causa del error. Si
 *              Si se ejecutan los subprocesos pero alguno de estos
 *              finaliza con errores, devuelve un valor negativo que
 *              indica el numero de subprocesos que han fallado.
 */
static int ejecutarPadre ( const char  *nombProceso,
                    long            opciones,
                    long         numProcesos,
                    long         numFicheros,
                    const char  *nombFichero,
                    const char *conexionData) 
{
	FILE *fichSalida;

	pid_t pids[PPAL_NUMERO_MAXNUMPROCS];
	char nombsFichero[PPAL_LGO_NOMBFICHERO][PPAL_NUMERO_MAXNUMPROCS];

	int i, j, retorno;

	INICIAR_TEMPORIZADOR( EJECUTAR_PROCESO )
	INICIO_MEDIDA_TEMPORIZADOR( EJECUTAR_PROCESO )

	if (!(opciones & PPAL_OPCION_MODO_SILEN))
		imprimirLogo( nombProceso );

	/*
	 * Ajusta el manipulador de la senal de  terminacion.
	 */

	if (signal( SIGUSR1, manipularSenalTerminar ))
	{
		return ERROR( ERR_SO_NOSIGNAL );
	}

	/*
	 * Calcula la carga de cada instancia del proceso
	 */

	if (numProcesos > 1)
	{
		if (!(opciones & PPAL_OPCION_MODO_SILEN))
		{
			fprintf( stderr, "Calculando la carga de cada proceso...\n" );
		}

		retorno = iface_calcularCarga( opciones,
		                               numProcesos,
		                               conexionData,
		                               minCodigo,
		                               maxCodigo );
		if (retorno != ERR_NING)
		{
			FIN_MEDIDA_TEMPORIZADOR( EJECUTAR_PROCESO )
			FINALIZAR_TEMPORIZADOR( EJECUTAR_PROCESO )

			return retorno;
		}

		if (!(opciones & PPAL_OPCION_MODO_SILEN))
		{
			fprintf( stderr, "-> Nro. procesos: %ld.\n", numProcesos );
			fprintf( stderr, "-> Intervalos de codigos:\n" );

			for (i = 0; i < numProcesos; i++)
			{
				fprintf( stderr,
				         "\t[%d] De %ld a %ld.\n",
				         i,
				         minCodigo[i],
				         maxCodigo[i] );
			}
		}
	}
	else
	{
		if (!(opciones & PPAL_OPCION_MODO_SILEN))
		{
			fprintf( stderr, "Calculando la carga del proceso...\n" );
			fprintf( stderr, "-> Nro. procesos: 1.\n" );
			fprintf( stderr, "-> Intervalo de codigos:\n" );
			fprintf( stderr, "\t[0] De %ld a %ld.\n",
			                 minCodigo[0],
			                 maxCodigo[0] );
		}
	}

	/*
	 * Ejecuta las instancias secundarias del proceso
	 */

	if (!(opciones & PPAL_OPCION_MODO_SILEN))
	{
		fprintf( stderr, "Ejecutando los sub-procesos...\n" );
	}

	for (i = 0; i < numProcesos; i++)
	{
		sprintf( nombsFichero[i], "%s.t%02d", nombFichero, i );

		pids[i] = fork();

		if (!pids[i])
		{
			char flagTipoFichero[PPAL_LGO_FTIPOFICHERO];
			char flagNumFicheros[PPAL_LGO_FNUMFICHEROS];
			char flagIdProceso[PPAL_LGO_FIDPROCESO];
			char flagMinCodigo[PPAL_LGO_FMINCODIGO];
			char flagMaxCodigo[PPAL_LGO_FMAXCODIGO];
			char indicadorTipo;

			sprintf( flagTipoFichero, "-t%c",
			         (opciones & PPAL_OPCION_TIPO_PA) ? 'a' :
			         (opciones & PPAL_OPCION_TIPO_AL) ? 'h' :
			         (opciones & PPAL_OPCION_TIPO_RS) ? 'r' :
			         (opciones & PPAL_OPCION_TIPO_PB) ? 'b' : '?' );

			sprintf( flagNumFicheros, "%ld", numFicheros );
			sprintf( flagIdProceso,   "%ld", i );
			sprintf( flagMinCodigo,   "%ld", minCodigo[i] );
			sprintf( flagMaxCodigo,   "%ld", maxCodigo[i] );

			execl( nombProceso,
			       nombProceso,
			       flagTipoFichero,
			       PPAL_CADENA_FNUMFICHEROS,
			       flagNumFicheros,
			       PPAL_CADENA_FNOMBFICHERO,
			       nombsFichero[i],
			       PPAL_CADENA_FCONEXDATA,
			       conexionData,
			       PPAL_CADENA_FCONEXPLLA,
			       conexionPlla,
			       PPAL_CADENA_FCONEXTTES,
			       conexionTtes,
			       PPAL_CADENA_FIDPROCESO,
			       flagIdProceso,
			       PPAL_CADENA_FRGOINICIO,
			       flagMinCodigo,
			       PPAL_CADENA_FRGOFIN,
			       flagMaxCodigo,
			       NULL );

			/*
			 * Atencion: Si se retorna desde la llamada a 'execl'
			 * --------  entonces no se pudo crear la instancia
			 *           del subproceso.
			 */

			return ERROR( ERR_SO_NOEXEC );
		}

		sleep( PPAL_NUMERO_TPOESPERA );
	}

	/*
	 * Concatena los ficheros de salida de cada instancia
	 * del proceso de generacion de BRS
	 */

	if (!(opciones & PPAL_OPCION_MODO_SILEN))
		fprintf( stderr, "Esperando para la recoleccion de datos...\n" );

	retorno = 0;

	for (i = 0; i < numProcesos; i++)
	{
		int rs, estado, resumen;

		/*
		 * Espera a la finalizacion del i-esimo hijo
		 */
		
		estado = 0;
		resumen = 0;

		while ((rs = waitpid( pids[i], &estado, WNOHANG | WUNTRACED )) != pids[i])
		{
			if (rs != 0)
			{
				resumen = 1;
				break;
			}

			sleep( PPAL_NUMERO_TPOESPERA );
		}

		if (WIFEXITED( estado ))
		{
			resumen = 2;

			if (WEXITSTATUS( estado ) != ERR_NING)
			{
				resumen = 3;
				--retorno;
			}
		}
		else if (WIFSIGNALED( estado ))
		{
			resumen = 4;

			if (WTERMSIG( estado ) != SIGUSR1)
				--retorno;
		}
		else if (WIFSTOPPED( estado ))
		{
			resumen = 5;

			if (WSTOPSIG( estado ) != SIGUSR1)
				--retorno;
		}
		else
		{
			resumen = 6;
		}
		
		/* Muestra el resultado de la ejecucion del proceso hijo */

		switch (resumen)
		{
		case 2:

			fprintf( stderr,
			         "[00] Ha terminado correctamente el proceso "
			         "hijo [%02d]: pid = %d, rs = %d y estado = %d.\n\n",
			         i, pids[i], rs, estado ); 
			break;

		case 3:

			fprintf( stderr,
			         "[00] Ha terminado con errores el proceso "
			         "hijo [%02d]: pid = %d, rs = %d, estado = %d y "
			         "codigo de error = %d.\n\n",
			         i, pids[i], rs, estado, WEXITSTATUS( estado ) ); 
			break;

		case 4:

			fprintf( stderr,
			         "[00] Ha recibido una senal de terminacion el proceso "
			         "hijo [%02d]: pid = %d, rs = %d y estado = %d.\n\n",
			         i, pids[i], rs, estado ); 
			break;

		case 5:

			fprintf( stderr,
			         "[00] Ha recibido una senal de parada el proceso "
			         "hijo [%02d]: pid = %d, rs = %d y estado = %d.\n\n",
			         i, pids[i], rs, estado ); 
			break;

		default: /* Incluye tambien los casos resume = (0, 1, 6) */

			fprintf( stderr,
			         "[00] No ha podido determinar estado del proceso "
			         "hijo [%02d]: pid = %d, rs = %d, estado = %d, "
			         "resumen = %d.\n\n",
			         i, pids[i], rs, estado, resumen ); 
		}

	}

	for (i = 0; i < numFicheros; i++)
	{
		char nueNombFichero[PPAL_LGO_NOMBFICHERO];
		char tmpNombFichero[PPAL_LGO_NOMBFICHERO];
		long encontradosNr;

		if (!(opciones & PPAL_OPCION_MODO_SILEN))
		{
			fprintf( stderr, "Genenerando el fichero de salida Nro. %ld.\n", i );
		}

		sprintf( nueNombFichero, nombFichero, i );

		if (!(fichSalida = fopen( nueNombFichero, "a" )))
		{
			ERROR( ERR_SO_ESINVALIDA );

			if (!(opciones & PPAL_OPCION_MODO_SILEN))
			{
				fprintf( stderr, "Recoleccion de datos omitida por errores.\n" );
			}

			continue;
		}

		encontradosNr = 0;

		for (j = 0; j < numProcesos; j++)
		{
			char bufferES[1024];

			const char *ptrPatron;
			char       *ptrBuffer;
			size_t       leidosNr;
			int cc;

			/*
			 * Concatena el fichero de salida resultante
			 */

			sprintf( tmpNombFichero, nombsFichero[j], i );

			if (!(fichEntrada = fopen( tmpNombFichero, "r" )))
			{
				ERROR( ERR_SO_ESINVALIDA );

				fclose( fichSalida );
				continue;
			}

			ptrPatron = PPAL_CADENA_PATRON_CNT;

			while ((leidosNr = fread( bufferES,
									  1,
									  PPAL_LGO_BUFFERCOPIAFICH,
									  fichEntrada )) > 0)
			{
				for (ptrBuffer = bufferES;
				       (size_t)(ptrBuffer - bufferES) < leidosNr;
					       ptrBuffer++)
				{
					while (*ptrPatron)
					{
						if (*ptrBuffer != *ptrPatron)
							break;

						ptrBuffer++, ptrPatron++;

						if ((size_t)(ptrBuffer - bufferES) == leidosNr)
							break;
					}

					if (!*ptrPatron)
						encontradosNr++;

					if ((size_t)(ptrBuffer - bufferES) == leidosNr)
						continue;

					ptrPatron = PPAL_CADENA_PATRON_CNT;
				}

				fwrite( bufferES, 1, leidosNr, fichSalida );
			}

			fclose( fichEntrada );
			unlink( tmpNombFichero );
		}

		if (numFicheros > 0)
			fprintf( fichConteo, "%s %ld\n", nueNombFichero, encontradosNr );

		fflush( fichConteo );

		fclose( fichSalida );
	}

	fclose( fichConteo );

	if (!(opciones & PPAL_OPCION_MODO_SILEN))
	{
		fprintf( stderr, "Recoleccion de datos finalizada.\n" );
	}

	FIN_MEDIDA_TEMPORIZADOR( EJECUTAR_PROCESO )
	FINALIZAR_TEMPORIZADOR( EJECUTAR_PROCESO )

	return retorno;
}


/*
 * ejecutarHijo:
 *
 * Ejecuta una instancia del proceso, con los parametros dados.
 *
 * [in] nombProceso:   Puntero al nombre del proceso.
 * [in] idProceso:     Identificador del proceso.
 * [in] opciones:      Opciones generales del proceso.
 * [in] minCodigo:     Numero minimo de codigo a generar.
 * [in] maxCodigo:     Numero maximo de codigo a generar.
 * [in] numFicheros:   Numero de ficheros a generar.
 * [in] nombFichero:   Puntero al nombre del fichero de salida.
 * [in] conexionData:  Puntero a la cadena de conexion con los
 *
 * [retorno]:   Devuelve el codigo de error asociado a la ejecucion de
 *              todas las tareas del proceso.
 */
static int ejecutarHijo ( const char *nombProceso,
                     long idProceso,
                     long opciones,
                     long minCodigo,
                     long maxCodigo,
                     long numFicheros,
                     const char *nombFichero,
                     const char *conexionData)
{
	PGtorSalida gtorsal;
	PGtorEntrada gtorent;
	PPlantilla plla;
	PContenedor cdor;

	int retorno;

	/*
	 * Ajusta el manipulador de la senal de  terminacion.
	 */

	if (signal( SIGUSR1, manipularSenalTerminar ))
	{
		return ERROR( ERR_SO_NOSIGNAL );
	}

	ajustarIdProceso( idProceso );

	INICIAR_TEMPORIZADOR( EJECUTAR_SUBPROCESO )
	INICIAR_TEMPORIZADOR( CARGAR_PLANTILLA )
	INICIAR_TEMPORIZADOR( GENERAR_SALIDA )
	INICIAR_TEMPORIZADOR( CARGAR_DATOS )
	INICIAR_TEMPORIZADOR( P_CO_TEL )
	INICIAR_TEMPORIZADOR( P_CO_WEB )
	INICIAR_TEMPORIZADOR( P_PA_EMP )
	INICIAR_TEMPORIZADOR( P_PA_DIR )
	INICIAR_TEMPORIZADOR( P_RS_EMP )
	INICIAR_TEMPORIZADOR( P_RS_DIR )
	INICIAR_TEMPORIZADOR( P_AL_EMP )
	INICIAR_TEMPORIZADOR( P_AL_DIR )
	INICIAR_TEMPORIZADOR( P_AL_ADI )
	INICIAR_TEMPORIZADOR( P_PB_ABO )
	INICIAR_TEMPORIZADOR( P_PB_TFSEC )
	INICIAR_TEMPORIZADOR( P_GE_EXP )
	INICIAR_TEMPORIZADOR( P_GE_ZON )
	INICIAR_TEMPORIZADOR( P_PR_EXP )
	INICIAR_TEMPORIZADOR( P_PR_UNO )
	INICIAR_TEMPORIZADOR( P_PR_DOS )
	INICIAR_TEMPORIZADOR( EXPANDIR_CONTENEDOR )
	INICIAR_TEMPORIZADOR( GENERAR_LOTE )
	INICIAR_TEMPORIZADOR( GENERAR_REGISTRO )

	INICIO_MEDIDA_TEMPORIZADOR( EJECUTAR_SUBPROCESO )

	do
	{
		/*
		 * Crea las instancias de los tipos de dato abstractos
		 */

		retorno = ERR_NOMEMORIA;

		if (!(opciones & PPAL_OPCION_MODO_SILEN))
			fprintf( stderr,
			         "[%02ld] Creando el contenedor...\n", idProceso );

		if (!(cdor = crearContenedor()))
			break;

		if (!(opciones & PPAL_OPCION_MODO_SILEN))
		{
			fprintf( stderr,
			         "[%02ld] Creando la plantilla:\n", idProceso );

			fprintf( stderr, "\topciones = %08X\n", opciones );
			fprintf( stderr, "\tconexionPlla = %s\n", conexionPlla );
		}

		if (!(plla = crearPlantilla( opciones,
		                             conexionPlla )))
			break;

		if (!(opciones & PPAL_OPCION_MODO_SILEN))
		{
			fprintf( stderr,
			         "[%02ld] Creando el gestor de entrada:\n", idProceso );

			fprintf( stderr, "\topciones = %08X\n", opciones );
			fprintf( stderr, "\tminCodigo = %ld\n", minCodigo );
			fprintf( stderr, "\tmaxCodigo = %ld\n", maxCodigo );
			fprintf( stderr, "\tconexionData = %s\n", conexionData );
			fprintf( stderr, "\tconexionTtes = %s\n", conexionTtes );
		}

		if (!(gtorent = crearGtorEntrada( opciones,
		                                  minCodigo,
		                                  maxCodigo,
		                                  plla,
		                                  cdor,
		                                  conexionData,
		                                  conexionTtes )))
			break;

		if (!(opciones & PPAL_OPCION_MODO_SILEN))
		{
			fprintf( stderr,
			         "[%02ld] Creando el gestor de salida:\n", idProceso );

			fprintf( stderr, "\topciones = %08X\n", opciones );
			fprintf( stderr, "\tnumFicheros = %ld.\n", numFicheros );
			fprintf( stderr, "\tnombFichero = %s\n", nombFichero );
		}

		if (!(gtorsal = crearGtorSalida( opciones,
		                                 numFicheros,
		                                 nombFichero,
		                                 gtorent,
		                                 plla,
		                                 cdor )))
			break;
	
		/*
		 * Ejecuta el proceso de generacion de fichero
		 */

		if (!(opciones & PPAL_OPCION_MODO_SILEN))
			fprintf( stderr, "[%02ld] Cargando plantilla...\n", idProceso );

		if ((retorno = cargarPlantilla( plla ))
		      != ERR_NING)
			break;

		if (!(opciones & PPAL_OPCION_MODO_SILEN))
			fprintf( stderr,
			         "[%02ld] Conectando el gestor de entrada...\n",
			         idProceso );

		if ((retorno = conectarGtorEntrada( gtorent )) != ERR_NING)
			break;

		if (!(opciones & PPAL_OPCION_MODO_SILEN))
			fprintf( stderr,
			         "[%02ld] Generando fichero de salida...\n", idProceso );

		retorno = generarSalida( gtorsal );
	}
	while (0);

	/*
	 * Destruye las instancias de los tipo de dato abstractos
	 */

	if (cdor)    destruirContenedor( cdor );
	if (plla)    destruirPlantilla( plla );
	if (gtorent) destruirGtorEntrada( gtorent );
	if (gtorsal) destruirGtorSalida( gtorsal );

	FIN_MEDIDA_TEMPORIZADOR( EJECUTAR_SUBPROCESO )

	FINALIZAR_TEMPORIZADOR( EJECUTAR_SUBPROCESO )
	FINALIZAR_TEMPORIZADOR( CARGAR_PLANTILLA )
	FINALIZAR_TEMPORIZADOR( GENERAR_SALIDA )
	FINALIZAR_TEMPORIZADOR( CARGAR_DATOS )
	FINALIZAR_TEMPORIZADOR( P_CO_TEL )
	FINALIZAR_TEMPORIZADOR( P_CO_WEB )
	FINALIZAR_TEMPORIZADOR( P_PA_EMP )
	FINALIZAR_TEMPORIZADOR( P_PA_DIR )
/*****************************************************************************
 * ++
 * Functionname : main 
 *
 * Description  : 
 *
 * Parameters   :
 *
 * Global Variables
 *   Accessed   :  
 *
 *   Modified   :
 *
 * Returncode   : 
 *                
 * Modification History :
 *
 * Author               Date          Description
 * ------------------   -----------   -----------------------------------------
 * Jose Maria Alonso
 *
 * --
 ****************************************************************************/
int main (int argc, char *argv[])
{
  printf("Content-type: text/html \r\n\r\n");
  printf("<HTML>\n<HEAD>\n</HEAD>\n<BODY>");
  printf("<H1>Hello World!\n</H1>\n");
  printf("<H2>Si puedes ver esto, todo ha ido bien</H2>\n");
  printf("AUTH_TYPE = %s<BR>\n",getenv("AUTH_TYPE"));
  printf("CONTENT_LENGTH = %s<BR>\n",getenv("CONTENT_LENGTH"));
  printf("CONTENT_TYPE = %s<BR>\n",getenv("CONTENT_TYPE"));
  printf("GATEWAY_INTERFACE = %s<BR>\n",getenv("GATEWAY_INTERFACE"));
  printf("HTTP_ACCEPT = %s<BR>\n",getenv("HTTP_ACCEPT"));
  printf("HTTP_USER_AGENT = %s<BR>\n",getenv("$HTTP_USER_AGENT"));
  printf("PATH_INFO = %s<BR>\n",getenv("$PATH_INFO"));
  printf("PATH_TRANSLATED = %s<BR>\n",getenv("PATH_TRANSLATED"));
  printf("QUERY_STRING = %s<BR>\n",getenv("QUERY_STRING"));
  printf("REMOTE_ADDR = %s<BR>\n",getenv("REMOTE_ADDR"));
  printf("REMOTE_HOST = %s<BR>\n",getenv("REMOTE_HOST"));
  printf("REMOTE_IDENT = %s<BR>\n",getenv("REMOTE_IDENT"));
  printf("REMOTE_USER = %s<BR>\n",getenv("REMOTE_USER"));
  printf("REMOTE_METHOD = %s<BR>\n",getenv("REMOTE_METHOD"));
  printf("SCRIPT_NAME = %s<BR>\n",getenv("SCRIPT_NAME"));
  printf("SERVER_NAME = %s<BR>\n",getenv("SERVER_NAME"));
  printf("SERVER_PORT = %s<BR>\n",getenv("SERVER_PORT"));
  printf("SERVER_PROTOCOL = %s<BR>\n",getenv("SERVER_PROTOCOL"));
  printf("SERVER_SOFTWARE = %s<BR>\n",getenv("SERVER_SOFTWARE"));
  printf("</BODY>\n</HTML>\n");
  return 0;
}

#endif /* _MAIN_C */
