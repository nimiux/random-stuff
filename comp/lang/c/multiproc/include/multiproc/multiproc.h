/******************************************************************************
 * ++
 * Author       : Jose Maria Alonso
 *
 * Module Name  : multiproc.h 
 *
 * Description  :
 *
 * Status Diagrams :
 *
 * Mod. History :
 *
 * AUTHOR               DATE            DESCRIPTION
 * ----------------     --------------  ------------------------------------
 * Jose Maria Alonso    11/06/2002
 *
 * --
 ******************************************************************************/
#ifndef _MULTIPROC_H_
#define _MULTIPROC_H_

/******************************************************************************
 * Standards and Complance                                                    *
 ******************************************************************************/

/******************************************************************************
 * System Include Files                                                       *
 ******************************************************************************/
/*#include <stdlib.h>*/
/*#include <stdio.h>*/
/*#include <string.h>*/
/*#include <ctype.h>*/
/*#include <assert.h>*/
/*#include <errno.h>*/
/*#include <time.h>*/
/*#include <unistd.h>*/
/*#include <signal.h>*/
/*#include <sys/types.h>*/
/*#include <sys/wait.h>*/

/******************************************************************************
 * Application Include Files                                                  *
 ******************************************************************************/

/******************************************************************************
 * Debug and Trace Options                                                    *
 ******************************************************************************/

#define DEBUG                               1

#ifdef DEBUG
#define DBG( x )                            x
#else
#define DBG( x )
#endif

#ifdef DEBUG
#define DEFINIR_TEMPORIZADOR( n )           \
	long g_cont##n;                     \
	double g_total##n;                  \
	time_t g_inicio##n;
#else
#define DEFINIR_TEMPORIZADOR( n )
#endif

#ifdef DEBUG
#define DECLARAR_TEMPORIZADOR( n )          \
	extern long g_cont##n;              \
	extern double g_total##n;           \
	extern time_t g_inicio##n;
#else
#define DECLARAR_TEMPORIZADOR( n )
#endif

#ifdef DEBUG
#define INICIO_MEDIDA_TEMPORIZADOR( n )     \
	++g_cont##n;                        \
	time( &g_inicio##n );
#else
#define INICIO_MEDIDA_TEMPORIZADOR( n )
#endif

#ifdef DEBUG
#define FIN_MEDIDA_TEMPORIZADOR( n )                         \
	g_total##n += difftime( time( NULL ), g_inicio##n );
#else
#define FIN_MEDIDA_TEMPORIZADOR( n )
#endif

#ifdef DEBUG
#define INICIAR_TEMPORIZADOR( n )            \
	g_cont##n = 0;                       \
	g_total##n = 0.0;
#else
#define INICIAR_TEMPORIZADOR( n )
#endif

#ifdef DEBUG
#define FINALIZAR_TEMPORIZADOR( x )                                           \
	if (g_cont##x) {                                                      \
		fprintf( stderr, "Nro. llamadas a %s:  %ld.\n",               \
		         #x, g_cont##x );                                     \
		fprintf( stderr, "Tpo. promedio por llamada a %s:  %f.\n",    \
		         #x, g_total##x/g_cont##x );                          \
		fprintf( stderr, "Tpo. total de llamadas a %s:  %f.\n",       \
		         #x, g_total##x );                                    \
	} else {                                                              \
		fprintf( stderr, "%s no ha sido invocado.\n", #x );           \
	}
#else
#define FINALIZAR_TEMPORIZADOR( n )
#endif



/******************************************************************************
 * Macro Definitions                                                          *
 ******************************************************************************/
#define PPAL_LGO_NOMBFICHERO                 256

#define PPAL_LGO_CADENACONEXION              256

#define PPAL_NUMERO_MINNUMARGS                 1
#define PPAL_NUMERO_MINNUMPROCS                1
#define PPAL_NUMERO_MAXNUMPROCS               99
#define PPAL_NUMERO_TPOESPERA                  1


#define PPAL_OPCION_NING                  0x0000
#define PPAL_OPCION_MODO_SILEN            0x0001


#ifndef SQLCODE
#define SQLCODE                             (sqlca.sqlcode)
#endif /* SQLCODE */

#define SQLROWS                             (sqlca.sqlerrd[2])
#define SQLERRL                             (sqlca.sqlerrm.sqlerrml)
#define SQLERRC                             (sqlca.sqlerrm.sqlerrmc)

#define STR2VCHR( v, s )                           \
	do {                                       \
		strcpy( (char *) (v).arr, (s) );   \
		(v).len = strlen( (s) );           \
	} while (0)
 
#define VCHR2STR( s, v )                           \
	do {                                       \
		(v).arr[(v).len] = '\0';           \
		strcpy( (s), (v).arr );            \
	} while (0)
 
#define VCHRLIM( v )                               \
	do {                                       \
		(v).arr[(v).len] = '\0';           \
	} while (0)

#define VCHRNULL( v )                              \
	do {                                       \
		(v).arr[0] = '\0';                 \
		(v).len = 0;                       \
	} while (0)

#define AVCHRNULL( v )                             \
	do {                                       \
		memset( (v), 0, sizeof( (v) ) );   \
	} while (0)

#define IVCHR2STR( s, v, i )                               \
	do {                                               \
		if (!(i)) {                                \
			(v).arr[(v).len] = '\0';           \
			strcpy((s),(const char *)(v).arr); \
		} else {                                   \
			(s)[0] = '\0';                     \
		}                                          \
	} while (0)

/******************************************************************************
 * Type Definitions                                                           *
 ******************************************************************************/

/******************************************************************************
 * Global Variables                                                           *
 ******************************************************************************/

/******************************************************************************
 * Local Function Prototypes                                                  *
 ******************************************************************************/

/******************************************************************************
 * External Function Prototypes                                               *
 ******************************************************************************/

#endif  /* _MULTIPROC_H_ */
