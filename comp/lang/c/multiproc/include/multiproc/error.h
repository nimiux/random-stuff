/******************************************************************************
 * ++
 * Author       : Jose Maria Alonso
 *
 * Module Name  : error.h 
 *
 * Description  :
 *
 * Status Diagrams :
 *
 * Mod. History :
 *
 * AUTHOR               DATE            DESCRIPTION
 * ----------------     --------------  ------------------------------------
 * Jose Maria Alonso    11/06/2002
 *
 * --
 ******************************************************************************/
#ifndef _ERROR_H_
#define _ERROR_H_

/******************************************************************************
 * Standards and Complance                                                    *
 ******************************************************************************/

/******************************************************************************
 * System Include Files                                                       *
 ******************************************************************************/

/******************************************************************************
 * Application Include Files                                                  *
 ******************************************************************************/

/******************************************************************************
 * Debug and Trace Options                                                    *
 ******************************************************************************/

/******************************************************************************
 * Macro Definitions                                                          *
 ******************************************************************************/
#define ERROR_IDREGISTRO                 128

#define ERROR( c )             mostrarerror( (c), __LINE__, __FILE__, NULL )
#define ERROREX( c, ia )       mostrarerror( (c), __LINE__, __FILE__, (ia) )

#define ERR_NING                           0
#define ERR_ARGINV                         1
#define ERR_NOMEMORIA                      2
#define ERR_NOMASREGS                      3
#define ERR_NOMASETIQS                     4
#define ERR_NOMASCONTS                     5
#define ERR_NOMASVALORES                   6
#define ERR_NOMASDATOS                     7
#define ERR_FINPLANTILLA                   8
#define ERR_NOHAYSALIDA                    9
#define ERR_BRSINVALIDO                   10
#define ERR_DATAINVALIDA                  11
#define ERR_FICHABIERTO                   12
#define ERR_FICHCERRADO                   13
#define ERR_FMTINVALIDO                   14
#define ERR_OPINVALIDA                    15
#define ERR_TERM                          16

#define ERR_SO_ESINVALIDA                 17
#define ERR_SO_NOEXEC                     18
#define ERR_SO_NOSIGNAL                   19

#define ERR_BD_NOCONECT                   20
#define ERR_BD_EJEC                       21
#define ERR_BD_NOLEE                      22
#define ERR_BD_NOALOJCURS                 23

/******************************************************************************
 * Type Definitions                                                           *
 ******************************************************************************/

/******************************************************************************
 * Global Variables                                                           *
 ******************************************************************************/

/******************************************************************************
 * Local Function Prototypes                                                  *
 ******************************************************************************/
int ajustarIdProceso  ( long idProcesoNuevo );
int ajustarIdRegistro ( const char *ptrCodigo,
                        const char *ptrProgresivo );

int levantarError     ( int codError,
                        long numLinea,
                        const char *nombFichero,
                        const void *ptrInfoAdic );

/******************************************************************************
 * External Function Prototypes                                               *
 ******************************************************************************/
#endif  /* _ERROR_H_ */
