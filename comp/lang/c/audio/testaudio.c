#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/soundcard.h>

int
main (int argc, char **argv)
{
        int audio;
        int stereo     = 1;
        int tam_sample = 16;
        int veloci_dsp = 44100; 
        int prof       = APF_NORMAL;

        int ret_error;

        audio = open ("/dev/dsp", O_RDONLY, 0);
        if (audio == -1) {
                fprintf (stderr, "Imposible abrir /dev/dsp\n");
                exit(1);
        }

        ret_error = tam_sample;
        ioctl(audio, SNDCTL_DSP_SAMPLESIZE, &tam_sample);
        if (tam_sample != ret_error) {
                fprintf (stderr, "Al fijar el tamaño de los samples\n");
                exit(1);
        }

        ioctl(audio, SNDCTL_DSP_PROFILE, &prof);

        ret_error = ioctl (audio, SNDCTL_DSP_STEREO, &stereo);
        if (ret_error == -1) {
                fprintf (stderr, "Error al fijar estereo\n");
                exit(1);
        }

        ret_error = ioctl (audio, SNDCTL_DSP_SPEED, &veloci_dsp);
        if (ret_error == -1) {
                fprintf (stderr, "Al fijar la velocidad del DSP\n");
                exit(1);
        }
                
                 
        /*
         * En este punto ya es posible trabajar con el
         * descriptor 'audio' mediente las funciones de
         * sistema read() y write(), para por ejemplo, 
         * emitir sonidos.
         *
         */
        write(audio, "0000000000000000000000000000000000000000000000000000000000"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     "jkfsdolfj0945tfg 39fgrm54gfrlkej vtgr'3342'05rmo'23rocx34¡"
		     , 2550); 

        close (audio);
                
        return 0;
}
