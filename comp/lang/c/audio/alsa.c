#include <stdio.h>
#include <string.h>
#include <sys/asoundlib.h>

int
main (int argc, char **argv)
{
     int idx;
    int cards;
    int err;
     snd_ctl_t *handle;
     struct snd_ctl_hw_info info;

     cards = snd_cards();
     printf("Detectada(s) %i tarjeta(s) de sonido.\n", cards);

     if (cards <= 0) {
   printf("Necesitas configurar ALSA primero\n");
             return 0;
     }

     for (idx = 0; idx < cards; idx++) {

              if ((err = snd_ctl_open(&handle, idx)) < 0) {
                    printf("Error al abrir: %s\n", snd_strerror(err));
                    continue;
              }

              if ((err = snd_ctl_hw_info(handle, &info)) < 0) {
                     printf("Error al coger información",
                            "sobre el hardware: %s\n", 
                            snd_strerror(err));
                     continue;
              }
                
              snd_ctl_close(handle);
     }
                
     return 0;
}
