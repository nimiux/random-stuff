struct months {
    char *name;
    int number;
    int days;
    int leap_days;
}
%%
january,   1, 31, 31
february,  2, 28, 29
march,     3, 31, 31
april,     4, 30, 30
may,       5, 31, 31
june,      6, 30, 30
july,      7, 31, 31
august,    8, 31, 31
september, 9, 30, 30
october,  10, 31, 31
november, 11, 30, 30
december, 12, 31, 31
%%
#include <stdio.h>
#include <string.h>
int main () {
    char message[1000];
    const struct months *m = in_word_set("january", 7);
    if (m) {
        sprintf(message, "Recuperado: %s, %d, %d, %d",m->name, m->number, m->days, m->leap_days);
    } else {
        strcpy(message, "No se ha enontrado.");
    }
    puts(message);
    return 0;
}
