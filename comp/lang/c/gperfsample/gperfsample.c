/* ANSI-C code produced by gperf version 3.1 */
/* Command-line: gperf -CEDIGt -L ANSI-C gperfsample.gp  */
/* Computed positions: -k'1,3' */

#if !((' ' == 32) && ('!' == 33) && ('"' == 34) && ('#' == 35) \
      && ('%' == 37) && ('&' == 38) && ('\'' == 39) && ('(' == 40) \
      && (')' == 41) && ('*' == 42) && ('+' == 43) && (',' == 44) \
      && ('-' == 45) && ('.' == 46) && ('/' == 47) && ('0' == 48) \
      && ('1' == 49) && ('2' == 50) && ('3' == 51) && ('4' == 52) \
      && ('5' == 53) && ('6' == 54) && ('7' == 55) && ('8' == 56) \
      && ('9' == 57) && (':' == 58) && (';' == 59) && ('<' == 60) \
      && ('=' == 61) && ('>' == 62) && ('?' == 63) && ('A' == 65) \
      && ('B' == 66) && ('C' == 67) && ('D' == 68) && ('E' == 69) \
      && ('F' == 70) && ('G' == 71) && ('H' == 72) && ('I' == 73) \
      && ('J' == 74) && ('K' == 75) && ('L' == 76) && ('M' == 77) \
      && ('N' == 78) && ('O' == 79) && ('P' == 80) && ('Q' == 81) \
      && ('R' == 82) && ('S' == 83) && ('T' == 84) && ('U' == 85) \
      && ('V' == 86) && ('W' == 87) && ('X' == 88) && ('Y' == 89) \
      && ('Z' == 90) && ('[' == 91) && ('\\' == 92) && (']' == 93) \
      && ('^' == 94) && ('_' == 95) && ('a' == 97) && ('b' == 98) \
      && ('c' == 99) && ('d' == 100) && ('e' == 101) && ('f' == 102) \
      && ('g' == 103) && ('h' == 104) && ('i' == 105) && ('j' == 106) \
      && ('k' == 107) && ('l' == 108) && ('m' == 109) && ('n' == 110) \
      && ('o' == 111) && ('p' == 112) && ('q' == 113) && ('r' == 114) \
      && ('s' == 115) && ('t' == 116) && ('u' == 117) && ('v' == 118) \
      && ('w' == 119) && ('x' == 120) && ('y' == 121) && ('z' == 122) \
      && ('{' == 123) && ('|' == 124) && ('}' == 125) && ('~' == 126))
/* The character set is not based on ISO-646.  */
#error "gperf generated tables don't work with this execution character set. Please report a bug to <bug-gperf@gnu.org>."
#endif

#line 1 "gperfsample.gp"
struct months {
    char *name;
    int number;
    int days;
    int leap_days;
};
#include <string.h>
enum
  {
    TOTAL_KEYWORDS = 12,
    MIN_WORD_LENGTH = 3,
    MAX_WORD_LENGTH = 9,
    MIN_HASH_VALUE = 3,
    MAX_HASH_VALUE = 18
  };

/* maximum key range = 16, duplicates = 0 */

#ifdef __GNUC__
__inline
#else
#ifdef __cplusplus
inline
#endif
#endif
static unsigned int
hash (register const char *str, register size_t len)
{
  static const unsigned char asso_values[] =
    {
      19, 19, 19, 19, 19, 19, 19, 19, 19, 19,
      19, 19, 19, 19, 19, 19, 19, 19, 19, 19,
      19, 19, 19, 19, 19, 19, 19, 19, 19, 19,
      19, 19, 19, 19, 19, 19, 19, 19, 19, 19,
      19, 19, 19, 19, 19, 19, 19, 19, 19, 19,
      19, 19, 19, 19, 19, 19, 19, 19, 19, 19,
      19, 19, 19, 19, 19, 19, 19, 19, 19, 19,
      19, 19, 19, 19, 19, 19, 19, 19, 19, 19,
      19, 19, 19, 19, 19, 19, 19, 19, 19, 19,
      19, 19, 19, 19, 19, 19, 19,  5,  5,  5,
       0, 19,  5,  0, 19, 19,  0, 19, 10,  0,
       0,  5,  0, 19,  0,  0,  0, 19,  0, 19,
      19,  0, 19, 19, 19, 19, 19, 19, 19, 19,
      19, 19, 19, 19, 19, 19, 19, 19, 19, 19,
      19, 19, 19, 19, 19, 19, 19, 19, 19, 19,
      19, 19, 19, 19, 19, 19, 19, 19, 19, 19,
      19, 19, 19, 19, 19, 19, 19, 19, 19, 19,
      19, 19, 19, 19, 19, 19, 19, 19, 19, 19,
      19, 19, 19, 19, 19, 19, 19, 19, 19, 19,
      19, 19, 19, 19, 19, 19, 19, 19, 19, 19,
      19, 19, 19, 19, 19, 19, 19, 19, 19, 19,
      19, 19, 19, 19, 19, 19, 19, 19, 19, 19,
      19, 19, 19, 19, 19, 19, 19, 19, 19, 19,
      19, 19, 19, 19, 19, 19, 19, 19, 19, 19,
      19, 19, 19, 19, 19, 19, 19, 19, 19, 19,
      19, 19, 19, 19, 19, 19
    };
  return len + asso_values[(unsigned char)str[2]] + asso_values[(unsigned char)str[0]];
}

static const struct months wordlist[] =
  {
#line 12 "gperfsample.gp"
    {"may",       5, 31, 31},
#line 13 "gperfsample.gp"
    {"june",      6, 30, 30},
#line 10 "gperfsample.gp"
    {"march",     3, 31, 31},
#line 8 "gperfsample.gp"
    {"january",   1, 31, 31},
#line 18 "gperfsample.gp"
    {"november", 11, 30, 30},
#line 16 "gperfsample.gp"
    {"september", 9, 30, 30},
#line 11 "gperfsample.gp"
    {"april",     4, 30, 30},
#line 15 "gperfsample.gp"
    {"august",    8, 31, 31},
#line 17 "gperfsample.gp"
    {"october",  10, 31, 31},
#line 19 "gperfsample.gp"
    {"december", 12, 31, 31},
#line 14 "gperfsample.gp"
    {"july",      7, 31, 31},
#line 9 "gperfsample.gp"
    {"february",  2, 28, 29}
  };

static const signed char lookup[] =
  {
    -1, -1, -1,  0,  1,  2, -1,  3,  4,  5,  6,  7,  8,  9,
    10, -1, -1, -1, 11
  };

const struct months *
in_word_set (register const char *str, register size_t len)
{
  if (len <= MAX_WORD_LENGTH && len >= MIN_WORD_LENGTH)
    {
      register unsigned int key = hash (str, len);

      if (key <= MAX_HASH_VALUE)
        {
          register int index = lookup[key];

          if (index >= 0)
            {
              register const char *s = wordlist[index].name;

              if (*str == *s && !strcmp (str + 1, s + 1))
                return &wordlist[index];
            }
        }
    }
  return 0;
}
#line 20 "gperfsample.gp"

#include <stdio.h>
#include <string.h>
int main () {
    char message[1000];
    const struct months *m = in_word_set("january", 7);
    if (m) {
        sprintf(message, "Recuperado: %s, %d, %d, %d",m->name, m->number, m->days, m->leap_days);
    } else {
        strcpy(message, "No se ha enontrado.");
    }
    puts(message);
    return 0;
}
