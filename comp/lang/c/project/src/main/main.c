/******************************************************************************
 * ++
 * Author       : Jose Maria Alonso 
 *
 * Module Name  : main.c
 *
 * Description  :
 *
 * Status Diagrams :
 *
 * Mod. History :
 *
 * AUTHOR               DATE            DESCRIPTION
 * ----------------     --------------  ------------------------------------
 * Jose Maria Alonso 
 *
 * --
 ******************************************************************************/

#ifndef _MAIN_C
#define _MAIN_C

/******************************************************************************
 * Standards and Complance                                                    *
 ******************************************************************************/

/******************************************************************************
 * System Include Files                                                       *
 ******************************************************************************/
#include <stdlib.h>
#include <stdio.h>

/******************************************************************************
 * Application Include Files                                                  *
 ******************************************************************************/

#include "main.h" 

/******************************************************************************
 * Debug and Trace Options                                                    *
 ******************************************************************************/

#ifdef TRACE_MAIN
#define TRACE(x) x
#else
#define TRACE(x)
#endif

/******************************************************************************
 * Macro Definitions                                                          *
 ******************************************************************************/

/******************************************************************************
 * Type Definitions                                                           *
 ******************************************************************************/

/******************************************************************************
 * Global Variables                                                           *
 ******************************************************************************/

/******************************************************************************
 * Local Function Prototypes                                                  *
 ******************************************************************************/

/******************************************************************************
 * Local Function                                                             *
 ******************************************************************************/

/******************************************************************************
 * Main                                                                       *
 ******************************************************************************/
/*****************************************************************************
 * ++
 * Functionname : main 
 *
 * Description  : 
 *
 * Parameters   :
 *
 * Global Variables
 *   Accessed   :  
 *
 *   Modified   :
 *
 * Returncode   : 
 *                
 * Modification History :
 *
 * Author               Date          Description
 * ------------------   -----------   -----------------------------------------
 * Jose Maria Alonso
 *
 * --
 ****************************************************************************/
int main (int argc, char *argv[])
{
  printf("Content-type: text/html \r\n\r\n");
  printf("<HTML>\n<HEAD>\n</HEAD>\n<BODY>");
  printf("<H1>Hello World!\n</H1>\n");
  printf("<H2>Si puedes ver esto, todo ha ido bien</H2>\n");
  printf("AUTH_TYPE = %s<BR>\n",getenv("AUTH_TYPE"));
  printf("CONTENT_LENGTH = %s<BR>\n",getenv("CONTENT_LENGTH"));
  printf("CONTENT_TYPE = %s<BR>\n",getenv("CONTENT_TYPE"));
  printf("GATEWAY_INTERFACE = %s<BR>\n",getenv("GATEWAY_INTERFACE"));
  printf("HTTP_ACCEPT = %s<BR>\n",getenv("HTTP_ACCEPT"));
  printf("HTTP_USER_AGENT = %s<BR>\n",getenv("$HTTP_USER_AGENT"));
  printf("PATH_INFO = %s<BR>\n",getenv("$PATH_INFO"));
  printf("PATH_TRANSLATED = %s<BR>\n",getenv("PATH_TRANSLATED"));
  printf("QUERY_STRING = %s<BR>\n",getenv("QUERY_STRING"));
  printf("REMOTE_ADDR = %s<BR>\n",getenv("REMOTE_ADDR"));
  printf("REMOTE_HOST = %s<BR>\n",getenv("REMOTE_HOST"));
  printf("REMOTE_IDENT = %s<BR>\n",getenv("REMOTE_IDENT"));
  printf("REMOTE_USER = %s<BR>\n",getenv("REMOTE_USER"));
  printf("REMOTE_METHOD = %s<BR>\n",getenv("REMOTE_METHOD"));
  printf("SCRIPT_NAME = %s<BR>\n",getenv("SCRIPT_NAME"));
  printf("SERVER_NAME = %s<BR>\n",getenv("SERVER_NAME"));
  printf("SERVER_PORT = %s<BR>\n",getenv("SERVER_PORT"));
  printf("SERVER_PROTOCOL = %s<BR>\n",getenv("SERVER_PROTOCOL"));
  printf("SERVER_SOFTWARE = %s<BR>\n",getenv("SERVER_SOFTWARE"));
  printf("</BODY>\n</HTML>\n");
  return 0;
}

#endif /* _MAIN_C */
