My printf format manual

%[N$][#+-0 ][m][.][M][<LM>]    <CE>

N	number of argument

#	alternate form (add 0x)

*	parameter is taken from argument list
*N$	parameter is taken from argument N 

m	minimun field width

M	maximun field width. Maximun number of decimals

<LM>	c
	 hh char
	 h  short int
	 l  long
	 ll long long
	 L  long double
	 z  size_t

<CE>  	conversion specifier
	 d,i		decimal
	 o,u,x,X	unsigned, octal hexa
	 e,E		double
	 f,F		float
	 g,G		smart double
	 a,A		double to hexa
	 c		char
	 s		string
	 p		(void *) hexa
	 n		return char written so far
	 %		%

