#include "stdio.h" 
#include "string.h" 

#define MAX_BUF 1024 

int main(int argc, char* argv[]) 
{ 
        FILE * fsrc; 
        FILE * fdst; 
   char buffer [MAX_BUF]; 
   int  buflen; 

   if ( argc != 3) 
   { 
      printf ("\nUso:\t%s <fichero_entrada> <fichero_salida>\n\n",argv[0]); 
      return 1; 
   } 
   
   if ( ( fsrc = fopen (argv[1],"r") ) == NULL ) 
   { 
      printf ( "No se ha podido abrir: %s \n", argv[1]); 
      return 1; 
   } 

   if ( ( fdst = fopen (argv[2],"w") ) == NULL ) 
   { 
      printf ( "No se ha podido abrir: %s \n", argv[2]); 
      return 1; 
   } 

   while (! feof(fsrc) ) 
   { 
      fgets (buffer, MAX_BUF, fsrc); 
      buflen = strlen(buffer); 
      buffer [buflen -1] = '\n'; 
      fputs (buffer, fdst); 
   } 
   
   if ( fclose (fsrc) ) 
   { 
      printf ( "No se ha podido cerrar: %s \n", argv[1]);   
      return 1; 
   } 

   if ( fclose (fdst) ) 
   { 
      printf ( "No se ha podido cerrar: %s \n", argv[2]);   
      return 1; 
   } 

   return 0; 
} 
