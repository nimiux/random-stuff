/*****************************************************************************
 * ++
 * Proyecto     : Broker DICADI-GOLS.
 *
 * Autor        : Jose Maria Alonso. 27/05/2004.
 *
 * Modulo       : CDicadiValidationMsg.cc
 *
 * Descripcion  : Modulo de definicion del mensaje de pago o consulta enviado
 *                desde el sistema DICADI.
 *
 * Historial    :
 *
 * Autor                Fecha           Descripcion
 * ----------------     --------------  ------------------------------------
 * --
 ******************************************************************************/

extern "C" {
   #include <stdio.h>
   #include <stdlib.h>
   #include <assert.h>
}

#include "CDicadiValidationMsg.h"
      
#include "CGMPagarPremio.h"
#include "CGMConsultarPremio.h"
#include "CGRErrorPago.h"
#include "CGRError.h"

#include "CUtils.h" 

/****************************************************************************\
 * Opciones de Debug y Traza 
\****************************************************************************/ 
#ifdef TRACE_CDICADIVALIDATIONMSG
#define TRACE(x) x 
#else 
#define TRACE(x) 
#endif

const unsigned int CDicadiValidationMsg::MESSAGE_LEN                 =  120;

CDicadiValidationMsg::CDicadiValidationMsg(bool bispayment) {
   TRACE( printf("CDicadiValidationMsg::CDicadiValidationMsg\n"); )
   SetTransactionType(bispayment?CDicadiMsg::PAY_TRANSACTION_TYPE
                                :CDicadiMsg::QRY_TRANSACTION_TYPE);
   SetSellJulian(0);
   SetProductJulian(0);
   SetProductYear(0);
   SetProductType(0);
   SetNumber(0);
   SetSeries(0);
   SetExternalNumber(0);
   SetCheckSum(0);
   SetCasherCode(0);
}

CDicadiValidationMsg::~CDicadiValidationMsg () {
   TRACE( printf("CDicadiValidationMsg::~CDicadiValidationMsg\n"); )
}

int CDicadiValidationMsg::Initialize(CLogger *pxlogger, char *szdata) {

   TRACE( printf("CDicadiValidationMsg::Initialize\n"); )

   assert (pxlogger);
   assert (szdata);

   int iresult = CDicadiMsg::Initialize(pxlogger, szdata);
   if (iresult == CDicadiMsg::ID_VALIDATION_OK) {
      int nmsglen = strlen(szdata);
      if (nmsglen == MESSAGE_LEN) {
         int ipos = 3 * CDICADIMSG_FIELD_LEN;
         char szselljulian     [CDICADIMSG_FIELD_LEN + 1];
         char szproductjulian  [CDICADIMSG_FIELD_LEN + 1];
         char szproductyear    [CDICADIMSG_FIELD_LEN + 1];
         char szproducttype    [CDICADIMSG_FIELD_LEN + 1];
         char sznumber         [CDICADIMSG_FIELD_LEN + 1];
         char szseries         [CDICADIMSG_FIELD_LEN + 1];
         char szexternalnumber [CDICADIMSG_FIELD_LEN + 1];
         char szchecksum       [CDICADIMSG_FIELD_LEN + 1];
         char szcashercode     [CDICADIMSG_FIELD_LEN + 1];

         strcpy(szselljulian, CUtils::SubString(szdata
                                               , ipos
                                               , CDICADIMSG_FIELD_LEN));
         if (!ValidateField(szselljulian, 0, 366)) {
            GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                                , "El Juliano de Venta: %s no es correcto."
                                , szselljulian);
            iresult = CDicadiMsg::ID_VALIDATION_ERROR_FORMAT;
         } else {
            SetSellJulian(atoi(szselljulian));
            ipos += CDICADIMSG_FIELD_LEN;
            strcpy(szproductjulian, CUtils::SubString(szdata
                                                     , ipos
                                                     , CDICADIMSG_FIELD_LEN));
            if (!ValidateField(szproductjulian, 0, 366)) {
               GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                                , "El Juliano de Producto: %s no es correcto."
                                , szproductjulian);
               iresult = CDicadiMsg::ID_VALIDATION_ERROR_FORMAT;
            } else {
               SetProductJulian(atoi(szproductjulian));
               ipos += CDICADIMSG_FIELD_LEN;
               strcpy(szproductyear, CUtils::SubString(szdata
                                                      , ipos
                                                      , CDICADIMSG_FIELD_LEN));
               if (!ValidateField(szproductyear, 0, 99)) {
                  GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                                  , "El Ano de Producto: %s no es correcto."
                                  , szproductyear);
                  iresult = CDicadiMsg::ID_VALIDATION_ERROR_FORMAT;
               } else {
                  SetProductYear(atoi(szproductyear));
                  ipos += CDICADIMSG_FIELD_LEN;
                  strcpy(szproducttype
                        , CUtils::SubString(szdata
                                           , ipos
                                           , CDICADIMSG_FIELD_LEN));
                  if (!ValidateField(szproducttype,0, 9)) {
                     GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                                 , "El Tipo de Producto: %s no es correcto."
                                 , szproducttype);
                     iresult = CDicadiMsg::ID_VALIDATION_ERROR_FORMAT;
                  } else {
                     SetProductType(atoi(szproducttype));
                     ipos += CDICADIMSG_FIELD_LEN;
                     strcpy(sznumber
                           , CUtils::SubString(szdata
                                              , ipos
                                              , CDICADIMSG_FIELD_LEN));
                     if (!ValidateField(sznumber, 0, 99999)) {
                        GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                                       , "El Numero: %s no es correcto."
                                       , sznumber);
                        iresult = CDicadiMsg::ID_VALIDATION_ERROR_FORMAT;
                     } else {
                        SetNumber(atoi(sznumber));
                        ipos += CDICADIMSG_FIELD_LEN;
                        strcpy(szseries
                              , CUtils::SubString(szdata
                                                 , ipos
                                                 , CDICADIMSG_FIELD_LEN));
                        if (!ValidateField(szseries, 0, 999)) {
                           GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                                         , "La Serie: %s no es correcta."
                                         , szseries);
                           iresult = CDicadiMsg::ID_VALIDATION_ERROR_FORMAT;
                        } else {
                           SetSeries(atoi(szseries));
                           ipos += CDICADIMSG_FIELD_LEN;
                        }
                     }
                  }
               }
            }
         }
         // Se divide la validacion para claridad del codigo
         if (iresult == CDicadiMsg::ID_VALIDATION_OK) {
            strcpy(szexternalnumber, CUtils::SubString(szdata, ipos
                                                      , CDICADIMSG_FIELD_LEN));
            if (!ValidateField(szexternalnumber, 0, 99999999)) {
               GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                                     , "El Numero Externo: %s no es correcto."
                                     , szexternalnumber);
               iresult = CDicadiMsg::ID_VALIDATION_ERROR_FORMAT;
            } else {
               SetExternalNumber(atoi(szexternalnumber));
               ipos += CDICADIMSG_FIELD_LEN;
               strcpy(szchecksum, CUtils::SubString(szdata
                                                   , ipos
                                                   , CDICADIMSG_FIELD_LEN));
               if (!ValidateField(szchecksum, 0, 999)) {
                  GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                                         , "El Checksum : %s no es correcto."
                                         , szchecksum);
                  iresult = CDicadiMsg::ID_VALIDATION_ERROR_FORMAT;
               } else {
                  SetCheckSum(atoi(szchecksum));
                  ipos += CDICADIMSG_FIELD_LEN;
                  strcpy(szcashercode
                        , CUtils::SubString(szdata
                                           , ipos
                                           , CDICADIMSG_FIELD_LEN));
                  if (!ValidateField(szcashercode, 0, 99999999)) {
                     GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                                , "El Codigo de Pagador : %s no es correcto."
                                , szcashercode);
                     iresult = CDicadiMsg::ID_VALIDATION_ERROR_FORMAT;
                  } else {
                     SetCasherCode(atoi(szcashercode));
                  }
               }
            }
         }
      } else {
         GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                              , "La longitud del mensaje : %ld no es correcta."
                              , nmsglen);
         iresult = CDicadiMsg::ID_VALIDATION_ERROR_LEN;
      }
   }
   return iresult;
}

bool CDicadiValidationMsg::IsPayment() {
   return (GetTransactionType() == CDicadiMsg::PAY_TRANSACTION_TYPE);
}

void CDicadiValidationMsg::SetSellJulian(unsigned int nselljulian) {
   m_nselljulian = nselljulian;
}

void CDicadiValidationMsg::SetProductJulian(unsigned int nproductjulian) {
   m_nproductjulian = nproductjulian;
}

void CDicadiValidationMsg::SetProductYear(unsigned int nproductyear) {
   m_nproductyear = nproductyear;
}

void CDicadiValidationMsg::SetProductType(unsigned int nproducttype) {
   m_nproducttype = nproducttype;
}

void CDicadiValidationMsg::SetNumber(unsigned int nnumber) {
   m_nnumber = nnumber;
}

void CDicadiValidationMsg::SetSeries(unsigned int nseries) {
   m_nseries = nseries;
}

void CDicadiValidationMsg::SetExternalNumber(unsigned int nexternalnumber) {
   m_nexternalnumber = nexternalnumber;
}

void CDicadiValidationMsg::SetCheckSum(unsigned int nchecksum) {
   m_nchecksum = nchecksum;
}

void CDicadiValidationMsg::SetCasherCode(unsigned int ncashercode) {
   m_ncashercode = ncashercode;
}

unsigned int CDicadiValidationMsg::GetSellJulian() {
   return m_nselljulian;
}

unsigned int CDicadiValidationMsg::GetProductJulian() {
   return m_nproductjulian;
}

unsigned int CDicadiValidationMsg::GetProductYear() {
   return m_nproductyear;
}

unsigned int CDicadiValidationMsg::GetProductType() {
   return m_nproducttype;
}

unsigned int CDicadiValidationMsg::GetNumber() {
   return m_nnumber;
}

unsigned int CDicadiValidationMsg::GetSeries() {
   return m_nseries;
}

unsigned int CDicadiValidationMsg::GetExternalNumber() {
   return m_nexternalnumber;
}

unsigned int CDicadiValidationMsg::GetCheckSum() {
   return m_nchecksum;
}

unsigned int CDicadiValidationMsg::GetCasherCode() {
   return m_ncashercode;
}

char *CDicadiValidationMsg::GetDescription() {

   sprintf(m_szstringbuffer,"Ref. trans: %s. "
                            "Tipo transaccion: %u. "
                            "Juliano Venta: %u. "
                            "Juliano Producto: %u. "
                            "Ano Producto: %u. "
                            "Tipo Producto: %u. "
                            "Numero: %u. "
                            "Serie: %u. "
                            "Numero externo: %u. "
                            "Checksum: %u. "
                            "Codigo Pagador: %u."
                           , GetTransactionRef()
                           , GetTransactionType()
                           , GetSellJulian()
                           , GetProductJulian()
                           , GetProductYear()
                           , GetProductType()
                           , GetNumber()
                           , GetSeries()
                           , GetExternalNumber()
                           , GetCheckSum()
                           , GetCasherCode());
   return m_szstringbuffer;
}


CGolsResp *CDicadiValidationMsg::DoTransaction(CGateway *pxgateway
                                             , int *piresult) {
   CGMPremio *pxticket;
   CGolsResp *pxgolsrsp = NULL;
   int iresult;

   TRACE ( printf("CDicadiValidationMsg::DoTransaction()\n"); )

   assert(pxgateway);
   assert(piresult);
   
   if (IsPayment()) {
      pxticket = new CGMPagarPremio();
   } else {
      pxticket = new CGMConsultarPremio();
   }

   pxticket->SetJulianoVenta(GetSellJulian());
   pxticket->SetJulianoProducto(GetProductJulian());
   pxticket->SetCodProducto(GetProductType());
   pxticket->SetAnyoProducto(GetProductYear());
   pxticket->SetNumeroCupon(GetNumber());
   pxticket->SetSerieCupon(GetSeries());
   pxticket->SetNumeroExterno(GetExternalNumber());
   pxticket->SetCheckSum(GetCheckSum());
   pxticket->SetCodPagador(GetCasherCode());
   pxticket->SetTipoTicket(2);  // 2: Ticket virtual DICADI
   pxticket->SetTipoAplicacion(CGateway::ID_DICADI_APPLICATION);
   pxticket->SetCodAutorizacion(0);
   GetLogger()->WriteLine(CLogger::INFO, _FILE_, _LINE_
                           , "Se va a intentar %s del ticket: Numero %05u ."
                             "Serie: %03u."
                           , (IsPayment()?"el pago":"la consulta")
                           , GetNumber()
                           , GetSeries());
   iresult = pxgateway->DoTicketValidation(pxticket, &pxgolsrsp);
   GetLogger()->WriteLine(CLogger::FULL, _FILE_, _LINE_
                                , "La transaccion GOLS ha devuelto: %ld."
                                , iresult);
   if (iresult == CGateway::ID_RETURN_TRANSACTION_OK) {
      if ((pxgolsrsp->EsMensajeErrorPremio()) ||
          (pxgolsrsp->EsMensajeErrorGeneral())) {
         GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                             , "Se ha producido un error al realizar %s del "
                               "ticket: Numero: %05u Serie: %03u. El error ha "
                               "sido: %s"
                             , (IsPayment()?"el pago":"la consulta")
                             , GetNumber()
                             , GetSeries()
                             , (pxgolsrsp->EsMensajeErrorPremio()?
                               ((CGRErrorPago *) pxgolsrsp)->GetDescripcion():
                               ((CGRError *) pxgolsrsp)->GetDescripcion()));
      } else {
         if (!(((IsPayment()) && (pxgolsrsp->EsMensajePagoPremio())) ||
               ((!IsPayment()) && (pxgolsrsp->EsMensajeConsultaPremio())))) {
            GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                             , "La transaccion GOLS ha devuelto un mensaje "
                               "inesperado: %ld-%ld-%s."
                             , pxgolsrsp->GetTipo()
                             , pxgolsrsp->GetSubTipo()
                             , pxgolsrsp->GetMensaje());
            iresult = CGateway::ID_RETURN_TRANSACTION_ERROR;
         }
      }
   } else {
      // Error al realizar la transaccion
      GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                                  , "Se ha producido un error al realizar la "
                                    "transaccion: %ld."
                                  , iresult);
   }
   SAFE_DELETE(pxticket);
   *piresult = iresult;
   return pxgolsrsp;
}

char *CDicadiValidationMsg::GetMessage() {

   char szformat[100]="%%0%us%%0%uu%%0%uu%%0%uu%%0%uu%%0%uu%%0%uu%%0%uu"
                      "%%0%uu%%0%uu%%0%uu";
   char szfinalformat[100];
   
   TRACE ( printf("CDicadiValidationMsg::GetMessage()\n"); )

   sprintf(szfinalformat, szformat, CDICADIMSG_IDTRANS_FIELD_LEN 
                                  , CDICADIMSG_FIELD_LEN
                                  , CDICADIMSG_FIELD_LEN
                                  , CDICADIMSG_FIELD_LEN
                                  , CDICADIMSG_FIELD_LEN
                                  , CDICADIMSG_FIELD_LEN
                                  , CDICADIMSG_FIELD_LEN
                                  , CDICADIMSG_FIELD_LEN
                                  , CDICADIMSG_FIELD_LEN
                                  , CDICADIMSG_FIELD_LEN
                                  , CDICADIMSG_FIELD_LEN);
   sprintf(m_szstringbuffer, szfinalformat, GetTransactionRef()
                                          , GetTransactionType()
                                          , GetSellJulian()
                                          , GetProductJulian()
                                          , GetProductYear()
                                          , GetProductType()
                                          , GetNumber()
                                          , GetSeries()
                                          , GetExternalNumber()
                                          , GetCheckSum()
                                          , GetCasherCode());
   return m_szstringbuffer;
}
