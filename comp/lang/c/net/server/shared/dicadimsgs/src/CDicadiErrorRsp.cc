/*****************************************************************************
 * ++
 * Proyecto     : Broker DICADI-GOLS.
 *
 * Autor        : Jose Maria Alonso. 26/05/2004.
 *
 * Modulo       : CDicadiErrorRsp.cc
 *
 * Descripcion  : Modulo de definicion de la respuesta de error a una peticion
 *                desde el sistema Dicadi.
 *
 * Historial    :
 *
 * Autor                Fecha           Descripcion
 * ----------------     --------------  ------------------------------------
 * --
 ******************************************************************************/

extern "C" {
   #include <stdio.h>
   #include <assert.h>
}

#include "CDicadiErrorRsp.h"

#include "CGRErrorPago.h"
#include "CGRError.h"

#include "CUtils.h"

/****************************************************************************\
 * Opciones de Debug y Traza 
\****************************************************************************/ 
#ifdef TRACE_CDICADIERRORRSP
#define TRACE(x) x 
#else 
#define TRACE(x) 
#endif

CDicadiErrorRsp::CDicadiErrorRsp(CLogger *pxlogger) {
   TRACE( printf("CDicadiErrorRsp::CDicadiErrorRsp\n"); )
   SetTransactionType(CDicadiMsg::ERR_TRANSACTION_TYPE);
   m_pxlogger = pxlogger;
   memset(m_szerrordescription, '0', CDICADIERRORRSP_FIELD_DESCRIPTION_LEN);
   m_szerrordescription[CDICADIERRORRSP_FIELD_DESCRIPTION_LEN] = '\0';
}

CDicadiErrorRsp::~CDicadiErrorRsp () {
   TRACE( printf("CDicadiErrorRsp::~CDicadiErrorRsp\n"); )
}

bool CDicadiErrorRsp::Initialize(bool bvalidationok
                                , bool btransactionok
                                , bool bprotocolviolation
                                , CDicadiMsg *pxmsg
                                , CGolsResp *pxgolsrsp) {

   TRACE( printf("CDicadiErrorRsp::Initialize\n"); )
   bool breturn = true;

   SetTransactionRef(pxmsg->GetTransactionRef());
   SetResultCode(CDicadiRsp::ID_TRANSACTION_ERROR_INTERNALCONVERSION);
   SetErrorDescription((char *)"Error interno en conversion de datos");

   if ((bvalidationok) && (btransactionok)) { 
      if (pxgolsrsp->EsMensajeErrorPremio()) {
         SetResultCode(((CGRErrorPago *)pxgolsrsp)->GetCodEstadoOperacion());
         SetErrorDescription(CUtils::SubString(
                          ((CGRErrorPago *)pxgolsrsp)->GetDescripcion()
                         , 0
                         , CDICADIERRORRSP_FIELD_DESCRIPTION_LEN));
      } else {
         if (pxgolsrsp->EsMensajeErrorGeneral()) {
            SetResultCode(((CGRError *)pxgolsrsp)->GetCodError());
            SetErrorDescription(CUtils::SubString(
                             ((CGRError *)pxgolsrsp)->GetDescripcion()
                            , 0
                            , CDICADIERRORRSP_FIELD_DESCRIPTION_LEN));
         }
      }
   } else {
      if ((bvalidationok) && (!btransactionok)) {
         if (!bprotocolviolation) { 
            SetResultCode(CDicadiRsp::ID_TRANSACTION_ERROR_ONLINESYSTEMOFF);
            SetErrorDescription((char *)"Sistema on-line no disponible");
         }
      } else {
         if ((!bvalidationok) && (!btransactionok)) {
            SetResultCode(CDicadiRsp::ID_TRANSACTION_ERROR_PARAMS);
            SetErrorDescription((char *)"Error de sintaxis en parametros");
         }
      }
   }
   return breturn;      
}

void CDicadiErrorRsp::SetErrorDescription(char *szerrordescription) {
   strcpy(m_szerrordescription, szerrordescription); 
}

char *CDicadiErrorRsp::GetErrorDescription() {
   return m_szerrordescription;
}

char *CDicadiErrorRsp::GetDescription() {

   sprintf(m_szstringbuffer,"Ref. trans: %s. "
                            "Tipo transaccion: %u. "
                            "Cod. Resultado: %u. "
                            "Descripcion error: %s."
                           , GetTransactionRef()
                           , GetTransactionType()
                           , GetResultCode()
                           , GetErrorDescription());
   return m_szstringbuffer;
}

char *CDicadiErrorRsp::GetResponse() {
   sprintf(m_szstringbuffer, "%020s%010u%010u% -60s"
                           , GetTransactionRef()
                           , GetTransactionType()
                           , GetResultCode()
                           , GetErrorDescription());
   return m_szstringbuffer;
}

void CDicadiErrorRsp::Populate(char *szdata) {

   TRACE( printf("CDicadiErrorRsp::Populate\n"); )

   assert(szdata);

   SetErrorDescription(CUtils::SubString(szdata
                                      , 0
                                      , CDICADIERRORRSP_FIELD_DESCRIPTION_LEN));
}   

