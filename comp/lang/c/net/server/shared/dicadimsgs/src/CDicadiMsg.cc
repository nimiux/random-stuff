/*****************************************************************************
 * ++
 * Proyecto     : Broker DICADI-GOLS.
 *
 * Autor        : Jose Maria Alonso. 24/05/2004.
 *
 * Modulo       : CDicadiMsg.cc
 *
 * Descripcion  : Modulo que contiene la definicion abstracta de un mensaje
 *                enviado desde el sistema DICADI.
 *
 * Historial    :
 *
 * Autor                Fecha           Descripcion
 * ----------------     --------------  ------------------------------------
 * --
 ******************************************************************************/

extern "C" {
   #include <stdio.h>
   #include <stdlib.h>
   #include <string.h>
   #include <assert.h>
}

#include "CDicadiMsg.h"

#include "CDicadiBetMsg.h"
#include "CDicadiValidationMsg.h"

#include "CUtils.h"

/****************************************************************************\
 * Opciones de Debug y Traza
\****************************************************************************/
#ifdef TRACE_CDICADIMSG
#define TRACE(x) x
#else
#define TRACE(x)
#endif

char CDicadiMsg::m_szstringbuffer[1024]              =  "";

// Constantes de definicion del tipo de mensaje 
const unsigned int CDicadiMsg::BET_TRANSACTION_TYPE  =   1;
const unsigned int CDicadiMsg::PAY_TRANSACTION_TYPE  =   2;
const unsigned int CDicadiMsg::QRY_TRANSACTION_TYPE  =   3;
const unsigned int CDicadiMsg::ERR_TRANSACTION_TYPE  =  10;

// Constantes de validacion
const int CDicadiMsg::ID_VALIDATION_OK               =   0;
const int CDicadiMsg::ID_VALIDATION_ERROR_TYPE       =  -1;
const int CDicadiMsg::ID_VALIDATION_ERROR_LEN        =  -2;
const int CDicadiMsg::ID_VALIDATION_ERROR_FORMAT     =  -3;

CDicadiMsg::CDicadiMsg() {
   TRACE( printf("CDicadiMsg::CDicadiMsg\n"); )
   SetTransactionType(CDicadiMsg::ERR_TRANSACTION_TYPE);
   memset(m_sztransactionref, '0', CDICADIMSG_IDTRANS_FIELD_LEN);
   m_sztransactionref[CDICADIMSG_IDTRANS_FIELD_LEN] = '\0';
}

CDicadiMsg::~CDicadiMsg () {
   TRACE( printf("CDicadiMsg::~CDicadiMsg\n"); )
}

CLogger *CDicadiMsg::GetLogger() {
   return m_pxlogger;
}

bool CDicadiMsg::ValidateField(char *szfieldvalue, unsigned long long ulmin
                                                 , unsigned long long ulmax) {
   TRACE( printf("CDicadiBetMsg::ValidateField\n"); )
   if (!CUtils::IsANumber(szfieldvalue)) {
      return false;
   } else {
      unsigned long long ulnumber = atoll(szfieldvalue);
      GetLogger()->WriteLine(CLogger::FULL, _FILE_, _LINE_
                                     , "Validacion de: %llu. Entre %llu y %llu"
                                     , ulnumber
                                     , ulmin
                                     , ulmax);
      if ((ulnumber < ulmin) || (ulnumber > ulmax)) {
         return false;
      } else {
         return true;
      }
   }
}

void CDicadiMsg::SetTransactionRef(char *sztransactionref) {
   strcpy(m_sztransactionref, sztransactionref);
}

void CDicadiMsg::SetTransactionType(unsigned int ntransactiontype) {
   m_ntransactiontype = ntransactiontype;
}

char *CDicadiMsg::GetTransactionRef() {
   return m_sztransactionref;
}

unsigned int CDicadiMsg::GetTransactionType() {
   return m_ntransactiontype;
}

int CDicadiMsg::Initialize(CLogger *pxlogger, char *szdata) {
   int iszdatalen;
   int iresult = CDicadiMsg::ID_VALIDATION_OK; 

   TRACE( printf("CDicadiMsg::Initialize\n"); )

   assert(szdata);
   assert(pxlogger);

   m_pxlogger = pxlogger;
   // Se incializa la parte comun de los mensajes
   iszdatalen = strlen(szdata);
   if (iszdatalen >= (CDICADIMSG_IDTRANS_FIELD_LEN)) {
      char sztransactionref[(CDICADIMSG_IDTRANS_FIELD_LEN) + 1];

      strcpy(sztransactionref
            , CUtils::SubString(szdata, 0, CDICADIMSG_IDTRANS_FIELD_LEN));
      SetTransactionRef(sztransactionref);
      if (iszdatalen >= (CDICADIMSG_IDTRANS_FIELD_LEN + CDICADIMSG_FIELD_LEN)) {
         char sztransactiontype[CDICADIMSG_FIELD_LEN + 1];
         strcpy(sztransactiontype
               , CUtils::SubString(szdata, CDICADIMSG_IDTRANS_FIELD_LEN
                                         , CDICADIMSG_FIELD_LEN));
         SetTransactionType(atoi(sztransactiontype));
      }
   } else {
      GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                                    , "No se puede inicializar el mensaje. "
                                      "Longitud menor que %u caracteres."
                                    , CDICADIMSG_IDTRANS_FIELD_LEN);
      int iresult = CDicadiMsg::ID_VALIDATION_OK; 
   } 
   return iresult;
} 

CDicadiMsg *CDicadiMsg::GetInstance(CLogger *pxlogger
                                , char *szdata
                                , int *piresult) {

   CDicadiMsg *pxinstance = NULL;

   TRACE( printf("CDicadiMsg::GetInstance\n"); )

   assert(pxlogger);
   assert(szdata);
   assert (piresult);      

   *piresult = CDicadiMsg::ID_VALIDATION_OK;
   unsigned int itranstype = atoi(CUtils::SubString(szdata
                                                  , CDICADIMSG_IDTRANS_FIELD_LEN
                                                  , CDICADIMSG_FIELD_LEN));
   pxlogger->WriteLine(CLogger::INFO, _FILE_, _LINE_
                                , "El tipo de transaccion es: %ld."
                                , itranstype);
   switch (itranstype) {
      case BET_TRANSACTION_TYPE: 
         pxinstance = new CDicadiBetMsg();
         break;
      case PAY_TRANSACTION_TYPE:
         pxinstance = new CDicadiValidationMsg(true);
         break;
      case QRY_TRANSACTION_TYPE:
         pxinstance = new CDicadiValidationMsg(false);
         break;
      default: 
         pxlogger->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                                , "El tipo de transaccion es incorrecto: %ld."
                                , itranstype);
          // Se instancia por defecto un mensaje de apuesta
          pxinstance = new CDicadiBetMsg();
         *piresult = CDicadiMsg::ID_VALIDATION_ERROR_TYPE;
   }
   pxlogger->WriteLine(CLogger::INFO, _FILE_, _LINE_
                                    , "Se va a inicializar la instancia para "
                                      "la trama: %s.", szdata);
   *piresult= pxinstance->Initialize(pxlogger, szdata);
   return pxinstance;
}

