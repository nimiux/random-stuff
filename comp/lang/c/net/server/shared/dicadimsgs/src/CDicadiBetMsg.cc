/*****************************************************************************
 * ++
 * Proyecto     : Broker DICADI-GOLS.
 *
 * Autor        : Jose Maria Alonso. 24/05/2004.
 *
 * Modulo       : CDicadiBetMsg.cc
 *
 * Descripcion  : Modulo de definicion del mensaje de apuesta enviado desde el
 *                sistema DICADI.
 *
 * Historial    :
 *
 * Autor                Fecha           Descripcion
 * ----------------     --------------  ------------------------------------
 * --
 ******************************************************************************/

extern "C" {
   #include <limits.h>
   #include <stdio.h>
   #include <stdlib.h>
   #include <string.h>
   #include <assert.h>
}

#include "CDicadiBetMsg.h"
#include "CUtils.h" 

#include "CGMCompra.h"

#include "CGRApuesta.h"
#include "CGRError.h"

/****************************************************************************\
 * Opciones de Debug y Traza 
\****************************************************************************/ 
#ifdef TRACE_CDICADIBETMSG 
#define TRACE(x) x 
#else 
#define TRACE(x) 
#endif

const unsigned int CDicadiBetMsg::MESSAGE_LEN                     =  90;

CDicadiBetMsg::CDicadiBetMsg() {
   TRACE( printf("CDicadiBetMsg::CDicadiBetMsg\n"); )
   SetUserID(0);
   SetProductJulian(0);
   SetProductYear(0);
   SetProductType(0);
   SetNumber(0);
   SetSeries(0);
}

CDicadiBetMsg::~CDicadiBetMsg () {
   TRACE( printf("CDicadiBetMsg::~CDicadiBetMsg\n"); )
}

int CDicadiBetMsg::Initialize(CLogger *pxlogger, char *szdata) {
   TRACE( printf("CDicadiBetMsg::Initialize\n"); )

   assert (pxlogger);
   assert (szdata);

   int iresult = CDicadiMsg::Initialize(pxlogger, szdata);
   if (iresult == CDicadiMsg::ID_VALIDATION_OK) {
      int imsglen = strlen(szdata);    
      if (imsglen == MESSAGE_LEN) {
         int ipos = 3 * CDICADIMSG_FIELD_LEN;
         char szuserid       [CDICADIMSG_FIELD_LEN + 1];
         char szproductjulian[CDICADIMSG_FIELD_LEN + 1];
         char szproductyear  [CDICADIMSG_FIELD_LEN + 1];
         char szproducttype  [CDICADIMSG_FIELD_LEN + 1];
         char sznumber       [CDICADIMSG_FIELD_LEN + 1];
         char szseries       [CDICADIMSG_FIELD_LEN + 1];

         strcpy(szuserid, CUtils::SubString(szdata
                                           , ipos
                                           , CDICADIMSG_FIELD_LEN));
         if (!ValidateField(szuserid, 0, UINT_MAX)) {
            GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                              , "El identificador de usuario DICADI: %s no "
                                "es correcto."
                              , szuserid);
            iresult = CDicadiMsg::ID_VALIDATION_ERROR_FORMAT;
         } else {
            SetUserID(atoi(szuserid));
            ipos += CDICADIMSG_FIELD_LEN;
            strcpy(szproductjulian, CUtils::SubString(szdata
                                                     , ipos
                                                     , CDICADIMSG_FIELD_LEN));
            if (!ValidateField(szproductjulian, 0, 366)) {
               GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                               , "El Juliano de Producto: %s no es correcto."
                               , szuserid);
               iresult = CDicadiMsg::ID_VALIDATION_ERROR_FORMAT;
            } else {
               SetProductJulian(atoi(szproductjulian));
               ipos += CDICADIMSG_FIELD_LEN;
               strcpy(szproductyear
                     , CUtils::SubString(szdata, ipos, CDICADIMSG_FIELD_LEN));
               if (!ValidateField(szproductyear, 0, 99)) {
                 GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                               , "El Ano de Producto: %s no es correcto."
                               , szproductyear);
                  iresult = CDicadiMsg::ID_VALIDATION_ERROR_FORMAT;
                } else {
                  SetProductYear(atoi(szproductyear));
                  ipos += CDICADIMSG_FIELD_LEN;
                  strcpy(szproducttype
                        , CUtils::SubString(szdata
                                           , ipos
                                           , CDICADIMSG_FIELD_LEN));
                  if (!ValidateField(szproducttype,0, 9)) {
                     GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                               , "El Tipo de Producto: %s no es correcto."
                               , szproducttype);
                     iresult = CDicadiMsg::ID_VALIDATION_ERROR_FORMAT;
                   } else {
                     SetProductType(atoi(szproducttype));
                     ipos += CDICADIMSG_FIELD_LEN;
                     strcpy(sznumber
                           , CUtils::SubString(szdata
                                              , ipos
                                              , CDICADIMSG_FIELD_LEN));
                     if (!ValidateField(sznumber, 0, 99999)) {
                        GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                               , "El Numero: %s no es correcto."
                               , sznumber);
                        iresult = CDicadiMsg::ID_VALIDATION_ERROR_FORMAT;
                     } else {
                        SetNumber(atoi(sznumber));
                        ipos += CDICADIMSG_FIELD_LEN;
                        strcpy(szseries
                              , CUtils::SubString(szdata
                                                 , ipos
                                                 , CDICADIMSG_FIELD_LEN));
                        if (!ValidateField(szseries, 0, 999)) {
                           GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                               , "La serie: %s no es correcta."
                               , szseries);
                           iresult = CDicadiMsg::ID_VALIDATION_ERROR_FORMAT;
                        } else {
                           SetSeries(atoi(szseries));
                        }
                     }
                  }
               }
            }
         }
      } else {
        GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                            , "La longitud del mensaje: %ld no es correcta."
                            , imsglen);
        iresult = CDicadiMsg::ID_VALIDATION_ERROR_LEN;
      }
   }
   return iresult;
}

void CDicadiBetMsg::SetUserID(unsigned int nuserid) {
   m_nuserid = nuserid;
}

void CDicadiBetMsg::SetProductJulian(unsigned int nproductjulian) {
   m_nproductjulian = nproductjulian;
}

void CDicadiBetMsg::SetProductYear(unsigned int nproductyear) {
   m_nproductyear = nproductyear;
}

void CDicadiBetMsg::SetProductType(unsigned int nproducttype) {
   m_nproducttype = nproducttype;
}

void CDicadiBetMsg::SetNumber(unsigned int nnumber) {
   m_nnumber = nnumber;
}

void CDicadiBetMsg::SetSeries(unsigned int nseries) {
   m_nseries = nseries;
}

unsigned int CDicadiBetMsg::GetUserID() {
   return m_nuserid;
}

unsigned int CDicadiBetMsg::GetProductJulian() {
   return m_nproductjulian;
}

unsigned int CDicadiBetMsg::GetProductYear() {
   return m_nproductyear;
}

unsigned int CDicadiBetMsg::GetProductType() {
   return m_nproducttype;
}

unsigned int CDicadiBetMsg::GetNumber() {
   return m_nnumber;
}

unsigned int CDicadiBetMsg::GetSeries() {
   return m_nseries;
}

char *CDicadiBetMsg::GetDescription() {

   sprintf(m_szstringbuffer,"Ref. trans: %s. "
                            "Tipo transaccion: %u. "
                            "Usuario Dicadi: %u. "
                            "Juliano Sorteo: %u. "
                            "Ano Sorteo: %u. "
                            "Tipo Sorteo: %u. "
                            "Numero: %u. "
                            "Serie: %u."
                           , GetTransactionRef()
                           , GetTransactionType()
                           , GetUserID()
                           , GetProductJulian()
                           , GetProductYear()
                           , GetProductType()
                           , GetNumber()
                           , GetSeries());
   return m_szstringbuffer;
}


CGolsResp *CDicadiBetMsg::DoTransaction(CGateway *pxgateway, int *piresult) {
   CGMCompra xbet;
   CGolsResp *pxgolsrsp = NULL;
   int iresult;

   TRACE ( printf("CDicadiBetMsg::DoTransaction()\n"); )

   assert(pxgateway);
   assert(piresult);

   xbet.SetNumeroCupones(1);
   xbet.SetIdDICADI(GetUserID());
   xbet.SetJulianoSorteo(GetProductJulian());
   xbet.SetAnyoSorteo(GetProductYear());
   xbet.SetTipoSorteo(GetProductType());
   xbet.SetNumeroCupon(GetNumber());
   xbet.SetSerieCupon(GetSeries());
   GetLogger()->WriteLine(CLogger::INFO, _FILE_, _LINE_
              , "Se va a intentar la compra del ticket: %05u-%03u."
              , GetNumber()
              , GetSeries());
   iresult = pxgateway->DoTicketBuy(&xbet, &pxgolsrsp);
   GetLogger()->WriteLine(CLogger::INFO, _FILE_, _LINE_
                                , "La transaccion GOLS ha devuelto: %ld."
                                , iresult);
   if (iresult == CGateway::ID_RETURN_TRANSACTION_OK) {
      if (pxgolsrsp->EsMensajeApuesta()) {
         CGRApuesta *pxbet = (CGRApuesta *) pxgolsrsp;
         if (pxbet->EsRespuestaCompra()) {
            GetLogger()->WriteLine(CLogger::INFO, _FILE_, _LINE_
                                , "Respuesta Compra: "
                                  "%d-%d-%d-%d-%d-%d-%d-%d-%d-%d"
                                , pxbet->GetIndiceJuego()
                                , pxbet->GetNumeroExterno()
                                , pxbet->GetCheckSum()
                                , pxbet->GetHora()
                                , pxbet->GetTipoOperacion()
                                , pxbet->GetIdDICADI()
                                , pxbet->GetJulianoSorteo()
                                , pxbet->GetAnyoSorteo()
                                , pxbet->GetTipoSorteo()
                                , pxbet->GetNumeroCupones());
         } else {
            GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                              , "La respuesta obtenida desde GOLS no es "
                                "respuesta a una compra: %ld-%ld-%s."
                              , pxgolsrsp->GetTipo()
                              , pxgolsrsp->GetSubTipo()
                              , pxgolsrsp->GetMensaje());
            iresult = CGateway::ID_RETURN_TRANSACTION_ERROR;
         }
      } else {
         if (pxgolsrsp->EsMensajeErrorGeneral()) {
            CGRError * pxerror = (CGRError *) pxgolsrsp;
            GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                         , "Se ha producido un error al acceder a GOLS: %s."
                         , pxerror->GetDescripcion());
         } else {
            GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                        , "Mensaje inesperado recibido desde GOLS: %ld-"
                          "%ld-%s"
                        , pxgolsrsp->GetTipo()
                        , pxgolsrsp->GetSubTipo()
                        , pxgolsrsp->GetMensaje());
            iresult = CGateway::ID_RETURN_TRANSACTION_ERROR;
         }
      }
   }
   *piresult = iresult;
   return pxgolsrsp;
}

char *CDicadiBetMsg::GetMessage() {
   char szformat[64]="%%0%us%%0%uu%%0%uu%%0%uu%%0%uu%%0%uu%%0%uu%%0%uu";
   char szfinalformat[64];

   TRACE ( printf("CDicadiBetMsg::GetMessage()\n"); )
  
   sprintf(szfinalformat, szformat, CDICADIMSG_IDTRANS_FIELD_LEN  
                                  , CDICADIMSG_FIELD_LEN
                                  , CDICADIMSG_FIELD_LEN
                                  , CDICADIMSG_FIELD_LEN
                                  , CDICADIMSG_FIELD_LEN
                                  , CDICADIMSG_FIELD_LEN
                                  , CDICADIMSG_FIELD_LEN
                                  , CDICADIMSG_FIELD_LEN);
   sprintf(m_szstringbuffer, szfinalformat, GetTransactionRef()
                                          , GetTransactionType()
                                          , GetUserID()
                                          , GetProductJulian()
                                          , GetProductYear()
                                          , GetProductType()
                                          , GetNumber()
                                          , GetSeries());
   return m_szstringbuffer;
}


