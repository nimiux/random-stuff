/*****************************************************************************
 * ++
 * Proyecto     : Broker DICADI-GOLS.
 *
 * Autor        : Jose Maria Alonso. 25/05/2004.
 *
 * Modulo       : CDicadiBetRsp.cc
 *
 * Descripcion  : Modulo de definicion de la respuesta a una apuesta realizada
 *                en el sistema GOLS.
 *
 * Historial    :
 *
 * Autor                Fecha           Descripcion
 * ----------------     --------------  ------------------------------------
 * --
 ******************************************************************************/

extern "C" {
   #include <stdlib.h>
   #include <stdio.h>
   #include <assert.h>
}

#include "CDicadiBetRsp.h"
#include "CUtils.h" 

/****************************************************************************\
 * Opciones de Debug y Traza 
\****************************************************************************/ 
#ifdef TRACE_CDICADIBETRSP
#define TRACE(x) x 
#else 
#define TRACE(x) 
#endif

CDicadiBetRsp::CDicadiBetRsp(CLogger *pxlogger) {
   TRACE( printf("CDicadiBetRsp::CDicadiBetRsp\n"); )
   SetTransactionType(CDicadiMsg::BET_TRANSACTION_TYPE);
   m_pxlogger = pxlogger;
   SetSellJulian(0);
   SetSellTime(0);
   SetExternalNumber(0);
   SetCheckSum(0);
   SetNumber(0);
   SetSeries(0);
}

CDicadiBetRsp::~CDicadiBetRsp () {
   TRACE( printf("CDicadiBetRsp::~CDicadiBetRsp\n"); )
}

bool CDicadiBetRsp::Initialize(bool bvalidationok
                              , bool btransactionok
                              , bool bprotocolviolation
                              , CDicadiMsg *pxmsg
                              , CGolsResp *pxgolsrsp) {

   TRACE( printf("CDicadiBetRsp::Initialize\n"); )
   bool breturn = false;

   if (bvalidationok && btransactionok) {
      breturn = true;
      assert (pxmsg);
      assert (pxgolsrsp);
 
      SetTransactionRef(pxmsg->GetTransactionRef());
      SetResultCode(CDicadiRsp::ID_TRANSACTION_OK);
   
      CGRApuesta *pxbet = (CGRApuesta *) pxgolsrsp;
      SetSellJulian(pxbet->GetJulianoSorteo());
      SetSellTime(pxbet->GetHora());
      SetExternalNumber(pxbet->GetNumeroExterno());
      SetCheckSum(pxbet->GetCheckSum());
      CListaCupones *pxlist = pxbet->GetListaCupones();
      if (!pxlist) {
         GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                               , "No se ha podido obtener la lista de tickets "
                                 "al recuperar los datos de la apuesta.");
         breturn = false;
      } else {
         // Debe haber un unico ticket 
         unsigned int itickets = pxlist->ObtenerNumeroCupones();
         if (!itickets) {
            GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                                      , "No se han obtenido tickets al "
                                        "recuperar los datos de la apuesta.");
            breturn = false;
         } else {
            unsigned int i;
            CDataCupon *pxticket;
            GetLogger()->WriteLine(CLogger::FULL, _FILE_, _LINE_
                                     , "Recuperando informacion de tickets.");
            for (i =0; i < itickets; i++) {
               pxticket = pxlist->ObtenerCupon(i);
               GetLogger()->WriteLine(CLogger::FULL, _FILE_, _LINE_
                                             , "Ticket numero: %u - %u - %u."
                                             , i
                                             , pxticket->GetNumeroCupon()
                                             , pxticket->GetSerieCupon());
               SetNumber(pxticket->GetNumeroCupon());
               SetSeries(pxticket->GetSerieCupon());
            }
            if (i != 1) {
               GetLogger()->WriteLine(CLogger::WARNING, _FILE_, _LINE_
                                            , "Se han obtenido %u tickets. "
                                              "Cuando se esperaba solo uno."
                                            , i);
            }
         }
      }
   }
   return breturn;
}

void CDicadiBetRsp::SetSellJulian(unsigned int nselljulian) {
   m_nselljulian = nselljulian;
}

void CDicadiBetRsp::SetSellTime(unsigned int nselltime) {
   m_nselltime = nselltime;
}

void CDicadiBetRsp::SetExternalNumber(unsigned int nexternalnumber) {
   m_nexternalnumber = nexternalnumber;
}

void CDicadiBetRsp::SetCheckSum(unsigned int nchecksum) {
   m_nchecksum = nchecksum;
}

void CDicadiBetRsp::SetNumber(unsigned int nnumber) {
   m_nnumber = nnumber;
}

void CDicadiBetRsp::SetSeries(unsigned int nseries) {
   m_nseries = nseries;
}

unsigned int CDicadiBetRsp::GetSellJulian() {
   return m_nselljulian;
}

unsigned int CDicadiBetRsp::GetSellTime() {
   return m_nselltime;
}

unsigned int CDicadiBetRsp::GetExternalNumber() {
   return m_nexternalnumber;
}

unsigned int CDicadiBetRsp::GetCheckSum() {
   return m_nchecksum;
}

unsigned int CDicadiBetRsp::GetNumber() {
   return m_nnumber;
}

unsigned int CDicadiBetRsp::GetSeries() {
   return m_nseries;
}

char *CDicadiBetRsp::GetDescription() {

   sprintf(m_szstringbuffer,"Ref. trans: %s. "
                            "Tipo transaccion: %u. "
                            "Cod. Resultado: %u. "
                            "Juliano Compra: %u. "
                            "Hora Compra: %u. "
                            "Numero externo: %u. "
                            "Checksum: %u. "
                            "Numero: %u. "
                            "Serie: %u."
                           , GetTransactionRef()
                           , GetTransactionType()
                           , GetResultCode()
                           , GetSellJulian()
                           , GetSellTime()
                           , GetExternalNumber()
                           , GetCheckSum()
                           , GetNumber()
                           , GetSeries());
   return m_szstringbuffer;
}

char *CDicadiBetRsp::GetResponse() {
   sprintf(m_szstringbuffer, "%020s%010u%010u%010u%010u%010u%010u%010u%010u"
                           , GetTransactionRef()
                           , GetTransactionType()
                           , GetResultCode()
                           , GetSellJulian()
                           , GetSellTime()
                           , GetExternalNumber()
                           , GetCheckSum()
                           , GetNumber()
                           , GetSeries());
   return m_szstringbuffer;
}

void CDicadiBetRsp::Populate(char *szdata) {

   unsigned int uipos=0;

 
   TRACE( printf("CDicadiBetRsp::Populate\n"); )

   assert(szdata);
                           
   SetSellJulian(atoi(CUtils::SubString(szdata, uipos, CDICADIMSG_FIELD_LEN)));
   uipos+=CDICADIMSG_FIELD_LEN;
   SetSellTime(atoi(CUtils::SubString(szdata, uipos, CDICADIMSG_FIELD_LEN)));
   uipos+=CDICADIMSG_FIELD_LEN;
   SetExternalNumber(atoi(CUtils::SubString(szdata
                                           , uipos
                                           , CDICADIMSG_FIELD_LEN)));
   uipos+=CDICADIMSG_FIELD_LEN;
   SetCheckSum(atoi(CUtils::SubString(szdata, uipos, CDICADIMSG_FIELD_LEN)));
   uipos+=CDICADIMSG_FIELD_LEN;
   SetNumber(atoi(CUtils::SubString(szdata, uipos, CDICADIMSG_FIELD_LEN)));
   uipos+=CDICADIMSG_FIELD_LEN;
   SetSeries(atoi(CUtils::SubString(szdata, uipos, CDICADIMSG_FIELD_LEN)));

}
