/*****************************************************************************
 * ++
 * Proyecto     : Broker DICADI-GOLS.
 *
 * Autor        : Jose Maria Alonso. 25/05/2004.
 *
 * Modulo       : CDicadiRsp.cc
 *
 * Descripcion  : Modulo que contiene la definicion abstracta de una respuesta
 *                enviada al sistema DICADI.
 *
 * Historial    :
 *
 * Autor                Fecha           Descripcion
 * ----------------     --------------  ------------------------------------
 * --
 ******************************************************************************/

extern "C" {
   #include <stdlib.h>
   #include <assert.h>
   #include <stdio.h>
}

#include "CDicadiRsp.h"

#include "CDicadiBetRsp.h"
#include "CDicadiValidationRsp.h"
#include "CDicadiErrorRsp.h"

#include "CUtils.h"

/****************************************************************************\
 * Opciones de Debug y Traza
\****************************************************************************/
#ifdef TRACE_CDICADIRSP
#define TRACE(x) x
#else
#define TRACE(x)
#endif

// Constante que define la longitud de los campos
const unsigned int CDicadiRsp::FIELD_LEN                            =  10; 

char CDicadiRsp::m_szstringbuffer[1024]                             =  "";

// Constantes de validacion
const int CDicadiRsp::ID_TRANSACTION_OK                             =   0;
const int CDicadiRsp::ID_TRANSACTION_ERROR_PARAMS                   = 200;
const int CDicadiRsp::ID_TRANSACTION_ERROR_INTERNALCONVERSION       = 300;
const int CDicadiRsp::ID_TRANSACTION_ERROR_ONLINESYSTEMOFF          = 400;

CDicadiRsp::CDicadiRsp() {
   TRACE( printf("CDicadiRsp::CDicadiRsp\n"); )
   SetTransactionType(CDicadiMsg::ERR_TRANSACTION_TYPE);
   int ireflen = 2 * FIELD_LEN;
   memset(m_sztransactionref, '0', ireflen);
   m_sztransactionref[ireflen] = '\0';
   m_nresultcode = CDicadiRsp::ID_TRANSACTION_ERROR_INTERNALCONVERSION;
}

CDicadiRsp::~CDicadiRsp () {
   TRACE( printf("CDicadiRsp::~CDicadiRsp\n"); )
}

CLogger *CDicadiRsp::GetLogger() {
   return m_pxlogger;
}

void CDicadiRsp::SetTransactionRef(char *sztransactionref) {
   strcpy(m_sztransactionref, sztransactionref);
}

void CDicadiRsp::SetTransactionType(unsigned int ntransactiontype) {
   m_ntransactiontype = ntransactiontype;
}

void CDicadiRsp::SetResultCode(unsigned int nresultcode) {
   m_nresultcode = nresultcode; 
}

char *CDicadiRsp::GetTransactionRef() {
   return m_sztransactionref;
}

unsigned int CDicadiRsp::GetTransactionType() {
   return m_ntransactiontype;
}

unsigned int CDicadiRsp::GetResultCode() {
   return m_nresultcode;
}

bool CDicadiRsp::IsBet() {
   return (GetTransactionType() == CDicadiMsg::BET_TRANSACTION_TYPE);
}

bool CDicadiRsp::IsPay() {
   return (GetTransactionType() == CDicadiMsg::PAY_TRANSACTION_TYPE);
}

bool CDicadiRsp::IsQry() {
   return (GetTransactionType() == CDicadiMsg::QRY_TRANSACTION_TYPE);
}

bool CDicadiRsp::IsErr() {
   return (GetTransactionType() == CDicadiMsg::ERR_TRANSACTION_TYPE);
}

CDicadiRsp *CDicadiRsp::GetInstance(CLogger *pxlogger
                                   , bool bvalidationok 
                                   , bool btransactionok 
                                   , bool bprotocolviolation 
                                   , CDicadiMsg *pxmsg
                                   , CGolsResp *pxgolsrsp) {
   CDicadiRsp *pxinstance = NULL;

   TRACE( printf("CDicadiRsp::GetInstance(6 pars)\n"); )

   assert(pxlogger);
   pxlogger->WriteLine(CLogger::FULL, _FILE_, _LINE_
                           , "Inicializacion de la respuesta. Validacion: %ld. "
                             "Transaccion: %ld. Protocolo: %ld."
                           , bvalidationok
                           , btransactionok
                           , bprotocolviolation);
   // Creacion
   if (!(bvalidationok && btransactionok)) {
      // Ocurrio un problema al validar el mensaje o realizar la transaccion
      // Se instancia una clase de tipo error
      pxinstance = new CDicadiErrorRsp(pxlogger);
   } else {
      assert(pxgolsrsp);
      if (pxgolsrsp->EsMensajeApuesta()) {
         CGRApuesta *pxbet = (CGRApuesta *) pxgolsrsp;
         if (pxbet->EsRespuestaCompra()) {
            // Se instancia una clase de tipo Respuesta a Apuesta 
            pxinstance = new CDicadiBetRsp(pxlogger);
         }
      } else {
         if ((pxgolsrsp->EsMensajePagoPremio()) || 
             (pxgolsrsp->EsMensajeConsultaPremio())) {
            // Se instancia una clase de tipo Validacion (Pago/Consulta) 
            pxinstance = new CDicadiValidationRsp(
                                       pxgolsrsp->EsMensajePagoPremio()
                                     , pxlogger);
         } else  {
            if ((pxgolsrsp->EsMensajeErrorGeneral()) || 
                (pxgolsrsp->EsMensajeErrorPremio()))  {
               // Se instancia una clase de tipo Error 
               pxinstance = new CDicadiErrorRsp(pxlogger);
            }
         }
      }
   }
   // Inicializacion
   if (pxinstance) {
      if (!pxinstance->Initialize(bvalidationok
                                 , btransactionok
                                 , bprotocolviolation
                                 , pxmsg
                                 , pxgolsrsp)) {
         SAFE_DELETE(pxinstance);
         pxinstance = new CDicadiErrorRsp(pxlogger);
         pxinstance->Initialize(false, true, false, pxmsg, pxgolsrsp);
      }
   } else {
      pxlogger->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                                , "El tipo de respuesta de GOLS: %u-%u. "
                                  "No es una respuesta correcta al mensaje "
                                  "enviado."
                                , pxgolsrsp->GetTipo()
                                , pxgolsrsp->GetSubTipo()
                                , pxgolsrsp->GetMensaje());
      pxinstance = new CDicadiErrorRsp(pxlogger);
      pxinstance->Initialize(false, true, false, pxmsg, pxgolsrsp);
   }
   return pxinstance;
}

CDicadiRsp *CDicadiRsp::GetInstance(CLogger *pxlogger
                                   , char *szdata) {
   CDicadiRsp *pxinstance = NULL;
   unsigned int uidatalen;
   char sztransactionref[CDICADIMSG_IDTRANS_FIELD_LEN + 1];
   char sztransactiontype[CDICADIMSG_FIELD_LEN + 1];
   unsigned int uitransactiontype;
   char szresultcode[CDICADIMSG_FIELD_LEN + 1];
   unsigned int uipos=0;

   TRACE( printf("CDicadiRsp::GetInstance(2 pars)\n"); )

   assert(pxlogger);
   assert(szdata);
   pxlogger->WriteLine(CLogger::FULL, _FILE_, _LINE_
                           , "Inicializacion de la respuesta para: %s."
                           , szdata);
   uidatalen=strlen(szdata);
   if (uidatalen>= (CDICADIMSG_IDTRANS_FIELD_LEN +(3*CDICADIMSG_FIELD_LEN))) {
      // Referencia de transaccion
      strcpy(sztransactionref
            , CUtils::SubString(szdata, uipos, CDICADIMSG_IDTRANS_FIELD_LEN));
      uipos+=CDICADIMSG_IDTRANS_FIELD_LEN;
      // Tipo de transaccion
      strcpy(sztransactiontype
            , CUtils::SubString(szdata, uipos, CDICADIMSG_FIELD_LEN));
      uipos+=CDICADIMSG_FIELD_LEN;
      // Resultado de la transaccion
      strcpy(szresultcode
            , CUtils::SubString(szdata, uipos, CDICADIMSG_FIELD_LEN));
      uipos+=CDICADIMSG_FIELD_LEN;
      uitransactiontype=atoi(sztransactiontype);
      if (uitransactiontype == CDicadiMsg::BET_TRANSACTION_TYPE) {
         pxinstance=new CDicadiBetRsp(pxlogger);
      } else {
         if (uitransactiontype == CDicadiMsg::PAY_TRANSACTION_TYPE) {
            pxinstance=new CDicadiValidationRsp(true, pxlogger);
         } else {
            if (uitransactiontype == CDicadiMsg::QRY_TRANSACTION_TYPE) {
               pxinstance=new CDicadiValidationRsp(false, pxlogger);
            } else {
               if (uitransactiontype == CDicadiMsg::ERR_TRANSACTION_TYPE) {
                  pxinstance=new CDicadiErrorRsp(pxlogger);
               } 
            } 
         } 
      }
      if (pxinstance) {
         pxinstance->SetTransactionRef(sztransactionref);
         pxinstance->SetTransactionType(atoi(sztransactiontype));
         pxinstance->SetResultCode(atoi(szresultcode));
         pxinstance->Populate(szdata+uipos);
      }
   }
   return pxinstance;
}
