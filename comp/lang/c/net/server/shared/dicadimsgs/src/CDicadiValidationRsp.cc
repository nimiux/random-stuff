/*****************************************************************************
 * ++
 * Proyecto     : Broker DICADI-GOLS.
 *
 * Autor        : Jose Maria Alonso. 27/05/2004.
 *
 * Modulo       : CDicadiValidationRsp.cc
 *
 * Descripcion  : Modulo de definicion de la respuesta a una peticion de pago
 *                o consulta en el sistema GOLS.
 *
 * Historial    :
 *
 * Autor                Fecha           Descripcion
 * ----------------     --------------  ------------------------------------
 * --
 ******************************************************************************/

extern "C" {
   #include <stdio.h>
   #include <stdlib.h>
   #include <assert.h>
}

#include "CDicadiValidationRsp.h"

#include "CGRPremio.h" 

#include "CUtils.h" 

/****************************************************************************\
 * Opciones de Debug y Traza 
\****************************************************************************/ 
#ifdef TRACE_CDICADIVALIDATIONRSP
#define TRACE(x) x 
#else 
#define TRACE(x) 
#endif

CDicadiValidationRsp::CDicadiValidationRsp(bool bispayment
                                          , CLogger *pxlogger) {
   TRACE( printf("CDicadiValidationRsp::CDicadiValidationRsp\n"); )
   SetTransactionType(bispayment?CDicadiMsg::PAY_TRANSACTION_TYPE
                                :CDicadiMsg::QRY_TRANSACTION_TYPE);
   m_pxlogger = pxlogger;
   SetPrize(0);
}

CDicadiValidationRsp::~CDicadiValidationRsp () {
   TRACE( printf("CDicadiValidationRsp::~CDicadiValidationRsp\n"); )
}

bool CDicadiValidationRsp::Initialize(bool bvalidationok
                              , bool btransactionok
                              , bool bprotocolviolation
                              , CDicadiMsg *pxmsg
                              , CGolsResp *pxgolsrsp) {

   TRACE( printf("CDicadiValidationRsp::Initialize\n"); )
   bool breturn = false;

   if (bvalidationok && btransactionok) {
      breturn = true;
      assert (pxmsg);
      assert (pxgolsrsp);
   
      SetTransactionRef(pxmsg->GetTransactionRef());
      SetResultCode(CDicadiRsp::ID_TRANSACTION_OK);
   
      CGRPremio *pxprize = (CGRPremio *) pxgolsrsp;
      // El importe se traduce a euros
      SetPrize(pxprize->GetImporte()/100);
   }
   return breturn;
}

bool CDicadiValidationRsp::IsPayment() {
   return (GetTransactionType() == CDicadiMsg::PAY_TRANSACTION_TYPE);
}

void CDicadiValidationRsp::SetPrize(unsigned int nprize) {
   m_nprize = nprize;
}

unsigned int CDicadiValidationRsp::GetPrize() {
   return m_nprize;
}

char *CDicadiValidationRsp::GetDescription() {

   sprintf(m_szstringbuffer,"Ref. trans: %s. "
                            "Tipo transaccion: %u. "
                            "Cod. Resultado: %u. "
                            "Premio: %u."
                           , GetTransactionRef()
                           , GetTransactionType()
                           , GetResultCode()
                           , GetPrize());
   return m_szstringbuffer;
}

char *CDicadiValidationRsp::GetResponse() {
   sprintf(m_szstringbuffer, "%020s%010u%010u%010u"
                           , GetTransactionRef()
                           , GetTransactionType()
                           , GetResultCode()
                           , GetPrize());
   return m_szstringbuffer;
}

void CDicadiValidationRsp::Populate(char *szdata) {

   TRACE( printf("CDicadiValidationRsp::Populate\n"); )

   assert(szdata);

   SetPrize(atoi(CUtils::SubString(szdata, 0, CDICADIMSG_FIELD_LEN)));

}   

