/*****************************************************************************
 * ++
 * Proyecto     : Broker DICADI-GOLS.
 *
 * Autor        : Jose Maria Alonso. 27/05/2004.
 * 
 * Modulo       : CDicadiValidationMsg.h
 *
 * Descripcion  : Modulo de definicion del mensaje de pago o consulta enviado
 *                desde el sistema DICADI.
 *
 * Historial    :
 *
 * Autor                Fecha           Descripcion
 * ----------------     --------------  ------------------------------------
 * --
 ******************************************************************************/

#ifndef __CDICADIVALIDATIONMSG_H__
#define __CDICADIVALIDATIONMSG_H__

#include "CDicadiMsg.h"

class CDicadiValidationMsg : public CDicadiMsg {

   private:
      // Longitud de un mensaje de pago 
      const static unsigned int MESSAGE_LEN;

      unsigned int  m_nselljulian;
      unsigned int  m_nproductjulian;
      unsigned int  m_nproductyear;
      unsigned int  m_nproducttype;
      unsigned int  m_nnumber;
      unsigned int  m_nseries;
      unsigned int  m_nexternalnumber;
      unsigned int  m_nchecksum;
      unsigned int  m_ncashercode;

   public:

      CDicadiValidationMsg(bool bispayment);
      ~CDicadiValidationMsg();

      int Initialize(CLogger *pxlogger, char *szdata);
               
      bool IsPayment();

      void SetSellJulian(unsigned int nselljulian);
      void SetProductJulian(unsigned int nproductjulian);
      void SetProductYear(unsigned int nproductyear);
      void SetProductType(unsigned int nproducttype);
      void SetNumber(unsigned int nnumber);
      void SetSeries(unsigned int nseries);
      void SetExternalNumber(unsigned int nexternalnumber);
      void SetCheckSum(unsigned int nchecksum);
      void SetCasherCode(unsigned int ncashercode);

      unsigned int GetSellJulian();
      unsigned int GetProductJulian();
      unsigned int GetProductYear();
      unsigned int GetProductType();
      unsigned int GetNumber();
      unsigned int GetSeries();
      unsigned int GetExternalNumber();
      unsigned int GetCheckSum();
      unsigned int GetCasherCode();

      char *GetDescription();

      CGolsResp *DoTransaction(CGateway *pxgateway, int *piresult);

      // Metodo usado por aplicaciones cliente del broker.
      // Obtiene la trama para ser enviada al broker.
      char *GetMessage();

};

#else
   class CDicadiValidationMsg;
#endif // __CDICADIVALIDATIONMSG_H__
