/*****************************************************************************
 * ++
 * Proyecto     : Broker DICADI-GOLS.
 *
 * Autor        : Jose Maria Alonso. 24/05/2004.
 * 
 * Modulo       : CDicadiMsg.h
 *
 * Descripcion  : Modulo que contiene la definicion abstracta de un mensaje
 *                enviado desde el sistema DICADI.
 *
 * Historial    :
 *
 * Autor                Fecha           Descripcion
 * ----------------     --------------  ------------------------------------
 * --
 ******************************************************************************/

#ifndef __CDICADIMSG_H__
#define __CDICADIMSG_H__

#include "CGateway.h"

#include "CGolsResp.h"

// Constantes de definicion de la longitud de los miembros 
#define CDICADIMSG_FIELD_LEN                                               10
#define CDICADIMSG_IDTRANS_FIELD_LEN                   2*CDICADIMSG_FIELD_LEN

class CDicadiMsg {

   protected:

      CLogger *m_pxlogger;

      static char m_szstringbuffer                   [1024];

      char m_sztransactionref[21];
      unsigned int  m_ntransactiontype;

      CLogger *GetLogger();

      bool ValidateField(char *szfieldvalue, unsigned long long ulmin
                                           , unsigned long long ulmax);

   public:
      // Constantes de definicion del tipo de transaccion
      const static unsigned int BET_TRANSACTION_TYPE;
      const static unsigned int PAY_TRANSACTION_TYPE;
      const static unsigned int QRY_TRANSACTION_TYPE;
      const static unsigned int ERR_TRANSACTION_TYPE;
      
      // Constantes de validacion
      const static int ID_VALIDATION_OK;
      const static int ID_VALIDATION_ERROR_TYPE;
      const static int ID_VALIDATION_ERROR_LEN;
      const static int ID_VALIDATION_ERROR_FORMAT;
                                                                 
      CDicadiMsg();
      virtual ~CDicadiMsg() = 0;

      void SetTransactionRef(char *sztransactionref);
      void SetTransactionType(unsigned int ntransactiontype);

      char *GetTransactionRef();
      unsigned int GetTransactionType();
      
      // El metodo de validacion e inicializacion se redefine en las clases
      // hijas.
      virtual int Initialize(CLogger *pxlogger, char *szdata);

      // Devuelve una instancia adecuada en funcion del mensaje recibido.
      // Devuelve NULL si no es posible crear una instancia para ese
      // mensaje. En piresult se devuelve el resultado de la validacion.
      static CDicadiMsg *GetInstance(CLogger *pxlogger
                                   , char *szdata
                                   , int *piresult);
      // Obtiene una cadena con todos los valores de los miembros
      virtual char *GetDescription() = 0;

      // Realiza la transaccion en GOLS
      virtual CGolsResp *DoTransaction(CGateway *pxgateway, int *piresult) = 0;

      // Metodo usado por aplicaciones cliente del broker.
      // Obtiene la trama para ser enviada al broker.
      virtual char *GetMessage() = 0;

};

#else
   class CDicadiMsg;
#endif // __CDICADIMSG_H__
