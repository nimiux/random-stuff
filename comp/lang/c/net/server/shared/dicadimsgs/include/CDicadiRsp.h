/*****************************************************************************
 * ++
 * Proyecto     : Broker DICADI-GOLS.
 *
 * Autor        : Jose Maria Alonso. 25/05/2004.
 * 
 * Modulo       : CDicadiRsp.h
 *
 * Descripcion  : Modulo que contiene la definicion abstracta de una respuesta
 *                enviada al sistema DICADI.
 *
 * Historial    :
 *
 * Autor                Fecha           Descripcion
 * ----------------     --------------  ------------------------------------
 * --
 ******************************************************************************/

#ifndef __CDICADIRSP_H__
#define __CDICADIRSP_H__

#include "CDicadiMsg.h"
#include "CGolsResp.h"

class CDicadiRsp {

   protected:
      // Constante que define la longitud de los miembros dato
      const static unsigned int FIELD_LEN; 

      CLogger *m_pxlogger;

      static char m_szstringbuffer                   [1024];

      char          m_sztransactionref[21];
      unsigned int  m_ntransactiontype;
      unsigned int  m_nresultcode;

      CLogger *GetLogger();

      virtual bool Initialize(bool bvalidationok 
                             , bool btransactionok
                             , bool bprotocolviolation
                             , CDicadiMsg *pxmsg
                             , CGolsResp *pxgolsrsp) = 0;

   public:
      // Constantes que definen el codigo de resultado en la respuesta
      const static int ID_TRANSACTION_OK;
      const static int ID_TRANSACTION_ERROR_PARAMS;
      const static int ID_TRANSACTION_ERROR_INTERNALCONVERSION;
      const static int ID_TRANSACTION_ERROR_ONLINESYSTEMOFF;
                                                                 
      CDicadiRsp();
      virtual ~CDicadiRsp() = 0;

      // Getters & Setters
      void SetTransactionRef(char *sztransactionref);
      void SetTransactionType(unsigned int ntransactiontype);
      void SetResultCode(unsigned int nresultcode);

      char *GetTransactionRef();
      unsigned int GetTransactionType();
      unsigned int GetResultCode();

      // Tipos de Respuesta
      bool IsBet();
      bool IsPay();
      bool IsQry();
      bool IsErr();
      
      // Devuelve una instancia adecuada en funcion de los parametros
      static CDicadiRsp *GetInstance(CLogger *pxlogger
                                   , bool bvalidationok 
                                   , bool btransactionok 
                                   , bool bprotocolviolation 
                                   , CDicadiMsg *pxmsg
                                   , CGolsResp *pxgolsrsp);
      // Usado por aplicaciones cliente del broker.
      // Obtiene una instancia de respuesta a partir de una trama recibida.
      static CDicadiRsp *GetInstance(CLogger *pxlogger
                                   , char *szdata);

      // Obtiene una cadena con todos los valores de los miembros
      virtual char *GetDescription() = 0;

      // Obtiene una cadena con la respuesta a enviar al sistema Dicadi.
      virtual char *GetResponse() = 0;

      // Da valor a los miembros de la clase obteniendolos de una trama.
      virtual void Populate(char *szdata) = 0;

};

#else
   class CDicadiRsp;
#endif // __CDICADIRSP_H__
