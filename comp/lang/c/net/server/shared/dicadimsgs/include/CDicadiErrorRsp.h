/*****************************************************************************
 * ++
 * Proyecto     : Broker DICADI-GOLS.
 *
 * Autor        : Jose Maria Alonso. 26/05/2004.
 * 
 * Modulo       : CDicadiErrorRsp.h
 *
 * Descripcion  : Modulo de definicion de la respuesta de error a una peticion
 *                desde el sistema Dicadi.
 *
 * Historial    :
 *
 * Autor                Fecha           Descripcion
 * ----------------     --------------  ------------------------------------
 * --
 ******************************************************************************/

#ifndef __CDICADIERRORSP_H__
#define __CDICADIERRORSP_H__

#include "CDicadiRsp.h"
      
// Longitud del campo descripcion del error
#define CDICADIERRORRSP_FIELD_DESCRIPTION_LEN                      60 

class CDicadiErrorRsp : public CDicadiRsp {

   private:

      char m_szerrordescription[CDICADIERRORRSP_FIELD_DESCRIPTION_LEN + 1];

   protected:      

      bool Initialize(bool bvalidationok
                     , bool btransactionok
                     , bool bprotocolviolation
                     , CDicadiMsg *pxmsg
                     , CGolsResp *pxgolsrsp);

   public:

      CDicadiErrorRsp(CLogger *pxlogger);
      ~CDicadiErrorRsp();

      void SetErrorDescription(char *szerrordescription);

      char *GetErrorDescription();

      char *GetDescription();

      char *GetResponse();

      // Da valor a los miembros de la clase obteniendolos de una trama.
      void Populate(char *szdata);

};

#else
   class CDicadErrorRsp;
#endif // __CDICADIERRORRSP_H__
