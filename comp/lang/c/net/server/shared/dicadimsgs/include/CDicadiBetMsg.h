/*****************************************************************************
 * ++
 * Proyecto     : Broker DICADI-GOLS.
 *
 * Autor        : Jose Maria Alonso. 24/05/2004.
 * 
 * Modulo       : CDicadiBetMsg.h
 *
 * Descripcion  : Modulo de definicion del mensaje de apuesta enviado desde el
 *                sistema DICADI.
 *
 * Historial    :
 *
 * Autor                Fecha           Descripcion
 * ----------------     --------------  ------------------------------------
 * --
 ******************************************************************************/

#ifndef __CDICADIBETMSG_H__
#define __CDICADIBETMSG_H__

#include "CDicadiMsg.h"

class CDicadiBetMsg : public CDicadiMsg {

   private:
      // Longitud de un mensaje de apuesta 
      const static unsigned int MESSAGE_LEN;

      unsigned int  m_nuserid;
      unsigned int  m_nproductjulian;
      unsigned int  m_nproductyear;
      unsigned int  m_nproducttype;
      unsigned int  m_nnumber;
      unsigned int  m_nseries;
      
   public:

      CDicadiBetMsg();
      ~CDicadiBetMsg();

      int Initialize(CLogger *pxlogger, char *szdata);
                
      void SetUserID(unsigned int nuserid);
      void SetProductJulian(unsigned int nproductjulian);
      void SetProductYear(unsigned int nproductyear);
      void SetProductType(unsigned int nproducttype);
      void SetNumber(unsigned int nnumber);
      void SetSeries(unsigned int nseries);

      unsigned int GetUserID();
      unsigned int GetProductJulian();
      unsigned int GetProductYear();
      unsigned int GetProductType();
      unsigned int GetNumber();
      unsigned int GetSeries();
      
      char *GetDescription();

      CGolsResp *DoTransaction(CGateway *pxgateway, int *piresult);

      // Metodo usado por aplicaciones cliente del broker.
      // Obtiene la trama para ser enviada al broker.
      char *GetMessage();

};

#else
   class CDicadBetMsg;
#endif // __CDICADIBETMSG_H__
