/*****************************************************************************
 * ++
 * Proyecto     : Broker DICADI-GOLS.
 *
 * Autor        : Jose Maria Alonso. 25/05/2004.
 * 
 * Modulo       : CDicadiBetRsp.h
 *
 * Descripcion  : Modulo de definicion de la respuesta a una apuesta realizada
 *                en el sistema GOLS.
 *
 * Historial    :
 *
 * Autor                Fecha           Descripcion
 * ----------------     --------------  ------------------------------------
 * --
 ******************************************************************************/

#ifndef __CDICADIBETRSP_H__
#define __CDICADIBETRSP_H__

#include "CDicadiRsp.h"

class CDicadiBetRsp : public CDicadiRsp {

   private:

      unsigned int  m_nselljulian;
      unsigned int  m_nselltime;
      unsigned int  m_nexternalnumber;
      unsigned int  m_nchecksum;
      unsigned int  m_nnumber;
      unsigned int  m_nseries;

   protected:      

      bool Initialize(bool bvalidationok
                     , bool btransactionok
                     , bool bprotocolviolation
                     , CDicadiMsg *pxmsg
                     , CGolsResp *pxgolsrsp);

   public:

      CDicadiBetRsp(CLogger *pxlogger);
      ~CDicadiBetRsp();

      void SetSellJulian(unsigned int nselljulian);
      void SetSellTime(unsigned int nselltime);
      void SetExternalNumber(unsigned int nexternalnumber);
      void SetCheckSum(unsigned int nchecksum);
      void SetNumber(unsigned int nnumber);
      void SetSeries(unsigned int nseries);

      unsigned int GetSellJulian();
      unsigned int GetSellTime();
      unsigned int GetExternalNumber();
      unsigned int GetCheckSum();
      unsigned int GetNumber();
      unsigned int GetSeries();

      char *GetDescription();

      char *GetResponse();
    
      // Da valor a los miembros de la clase obteniendolos de una trama.
      void Populate(char *szdata);

};

#else
   class CDicadBetRsp;
#endif // __CDICADIBETRSP_H__
