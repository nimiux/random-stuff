/*****************************************************************************
 * ++
 * Proyecto     : Broker DICADI-GOLS.
 *
 * Autor        : Jose Maria Alonso. 27/05/2004.
 * 
 * Modulo       : CDicadiValidationRsp.h
 *
 * Descripcion  : Modulo de definicion de la respuesta a una peticion de pago
 *                o consulta en el sistema GOLS.
 *
 * Historial    :
 *
 * Autor                Fecha           Descripcion
 * ----------------     --------------  ------------------------------------
 * --
 ******************************************************************************/

#ifndef __CDICADIVALIDATIONRSP_H__
#define __CDICADIVALIDATIONRSP_H__

#include "CDicadiRsp.h"

class CDicadiValidationRsp : public CDicadiRsp {

   private:

      unsigned int  m_nprize;

   protected:      

      bool Initialize(bool bvalidationok
                     , bool btransactionok
                     , bool bprotocolviolation
                     , CDicadiMsg *pxmsg
                     , CGolsResp *pxgolsrsp);

   public:

      CDicadiValidationRsp(bool bispayment, CLogger *pxlogger);
      ~CDicadiValidationRsp();

      bool IsPayment();

      void SetPrize(unsigned int nprize);

      unsigned int GetPrize();

      char *GetDescription();

      char *GetResponse();

      // Da valor a los miembros de la clase obteniendolos de una trama.
      void Populate(char *szdata);

};

#else
   class CDicadValidationRsp;
#endif // __CDICADIVALIDATIONRSP_H__
