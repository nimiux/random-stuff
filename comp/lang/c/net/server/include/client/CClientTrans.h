/*****************************************************************************
 * ++
 * Proyecto     : Broker DICADI-GOLS.
 * 
 * Autor        : Jose Maria Alonso. 24/05/2004.
 *
 * Modulo       : CClientTrans.h
 *
 * Descripcion  : Define una transaccion del cliente DICADI.
 *
 * Historial    :
 *
 * Autor                Fecha           Descripcion
 * ----------------     --------------  ------------------------------------
 * --
 ******************************************************************************/

#ifndef __CCLIENTTRANS_H__
#define __CCLIENTTRANS_H__

#include "CLogger.h"

class CClientTrans {

   private:
      // Logger de la transaccion 
      CLogger *m_pxlogger;

   public:
      // Constantes de retorno
      const static int ID_RETURN_INITOK;
      const static int ID_RETURN_VALIDATIONERROR;

      CClientTrans(CLogger *pxlogger);
      ~CClientTrans();

      CLogger *GetLogger();

      int DoTransaction(char *szgolsconnectstring
                       , unsigned int uitimeout
                       , char *szindata
                       , char *szoutdata);

};

#else
   class CClientTrans;
#endif // __CCLIENTTRANS_H__
