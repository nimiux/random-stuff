#!/bin/sh

# Aplicación de pagos de entidades bancarias en GOLS
# Script de distribución de la aplicación de pagos y librería libgolsgateway
# 30/03/2004

# Variables globales
DISTDIR="/exapp/cpgols"   
SRCDIR="src"
LIBDIRS="doc include java scripts shared src lib"
APPDIRS="doc include scripts src bin"
BADFILENAMES="libgolsgateway\.so\.. libgolsgateway\.so cpgdaemon crypttool pushlog"
BADEXTENSIONS="\.class \.o"
MAKECONFFILENAME="Makeconf"
APPCONFFILENAME="configure.sunos.ksh"
BANKENVFILENAME="setbankenv.noncrypted.ksh"
CRYPTTOOLSCRIPT="crypttool.sh"

# Funciones auxiliares

translate_bankenvscript() {
   srcfile=$1
   dstfile=$2
   newdir=`echo ${DISTDIR} | sed 's/\\//\\\\\//g'`
   sed "s/^CONSPAGOSGOLS_ROOT=.*/CONSPAGOSGOLS_ROOT=${newdir}\/${SRCDIR}\/conspagosgols-${versionmask}/
        s/^LIBGOLSGATEWAY_PATH=.*/LIBGOLSGATEWAY_PATH=${newdir}\/lib\/lib\/ssunos5/" ${srcfile} > ${dstfile}
}

translate_makeconf() {
   type=$1
   srcfile=$2
   dstfile=$3
   newdir=`echo ${DISTDIR} | sed 's/\\//\\\\\//g'`
   if [ "${type}" = "LIB" ]  ; then 
      sed "s/^INSDIR=.*/INSDIR=${newdir}\/lib/g
           s/^DEBUG=.*/#&/
           s/^.-DTRACE_.*/#&/
           s/^CFLAGS=$(CFLAGS.*/CFLAGS=$(CFLAGSRELEASE)/" ${srcfile} > ${dstfile}
   else
      sed "s/^INSDIR=.*/INSDIR=${newdir}\/exe\/$(PORTDEPS_DIR_NAME)/g
           s/^DEBUG=.*/#&/
           s/^.-DTRACE_.*/#&/
           s/^CFLAGS=$(CFLAGS.*/CFLAGS=$(CFLAGSRELEASE)/" ${srcfile} > ${dstfile}
   fi
}

translate_configure_app() {
   srcfile=$1
   dstfile=$2
   newdir=`echo ${DISTDIR} | sed 's/\\//\\\\\//g'`
   sed "s/^LIBGOLSGATEWAY_ROOT=.*/LIBGOLSGATEWAY_ROOT=${newdir}\/${SRCDIR}\/libgolsgateway-${versionmask}/g" ${srcfile} > ${dstfile}
}

translate_crypttoolscript() {
   srcfile=$1
   dstfile=$2
   newdir=`echo ${DISTDIR} | sed 's/\\//\\\\\//g'`
   sed "s/^CONSPAGOSGOLS_ROOT=.*/CONSPAGOSGOLS_ROOT=${newdir}/" ${srcfile} > ${dstfile}
}

copy_file() {
   srcfile=$1
   dstfile=$2
   if [ "x`echo ${srcfile} | grep "${MAKECONFFILENAME}$"`" != "x" ] ; then
      echo "Configurando fichero ${MAKECONFFILENAME}"
      translate_makeconf ${MODULETYPE} ${srcfile} ${dstfile}
   else
      if [ "x`echo ${srcfile} | grep "${BANKENVFILENAME}$"`" != "x" ] ; then
         echo "Configurando fichero ${BANKENVFILENAME}"
         translate_bankenvscript ${srcfile} ${dstfile}
      else
         if [ "x`echo ${srcfile} | grep "${CRYPTTOOLSCRIPT}$"`" != "x" ] ; then
            echo "Configurando fichero ${CRYPTTOOLSCRIPT}"
            translate_crypttoolscript ${srcfile} ${dstfile}
         else
            copyfile=1
            for i in ${BADEXTENSIONS} ; do
               if [ ${copyfile} -eq 1 ] ; then
                  if [ "x`echo ${srcfile} | grep "${i}$"`" != "x" ] ; then
                     copyfile=0
                  fi
               fi
            done
            if [ ${copyfile} -eq 1 ] ; then
               for i in ${BADFILENAMES} ; do
                  if [ ${copyfile} -eq 1 ] ; then
                     if [ "x`echo ${srcfile} | grep "${i}$"`" != "x" ] ; then
                        copyfile=0
                     fi
                  fi
               done
            fi
            if [ ${copyfile} -eq 1 ] ; then
               echo "\tCopiando fichero ${srcfile} a ${dstfile}"
               cp ${srcfile} ${dstfile}
            fi
         fi
      fi
   fi
}

copy_dir() {
   # $1  Source dir
   # $2  Destination dir
   # $3  Dir name
   echo "\tCopiando directorio ${1}/${3} a ${2}"
   mkdir -p  ${2}/${3}
   for i in `ls -F1 ${1}/${3} | sed "s/\*//
												 s/@//"` ; do
      slash=`echo $i | grep '/'`
      if [ "x${slash}" = "x" ] ; then
         copy_file ${1}/${3}/$i ${2}/${3}/${i}
      else
         dir=`echo $i | sed "s/\///"`
         copy_dir ${1}/${3} ${2}/${3} ${dir}
      fi
   done 
}

install_lib() { 
   libname=$1
   envname=$2
   MODULETYPE="LIB"
   echo "Distribuyendo la librería ${libname} al entorno ${envname}"
   dstdir=${envname}/${SRCDIR}/${libname}
   mkdir -p ${dstdir}
   echo "\tCopiando script de configuración."
   cp "${SRCDIR}/${libname}/configure.sunos.sh" "${dstdir}"
   for i in ${LIBDIRS} ; do
      copy_dir ${SRCDIR}/${libname} ${dstdir} $i
   done
}

install_app() {
   appname=$1
   envname=$2
   MODULETYPE="APP"
   echo "Distribuyendo la aplicación ${appname} al entorno ${envname}"
   dstdir=${envname}/${SRCDIR}/${appname}
   mkdir -p ${dstdir}
   echo "Configurando script ${APPCONFFILENAME}."
   translate_configure_app ${SRCDIR}/${appname}/${APPCONFFILENAME} ${dstdir}/${APPCONFFILENAME}
   for i in ${APPDIRS} ; do
      copy_dir ${SRCDIR}/${appname} ${dstdir} $i
   done
}

install() {
   appname=$1
   envname=$2
   prefix=`echo ${appname} | cut -b1-3`
   if [ "${prefix}" = "lib" ] ; then
      install_lib ${appname} ${envname}
   else
      if [ "${prefix}" = "con" ] ; then
         install_app ${appname} ${envname}
      fi
   fi
}

##############################################################################

if [ "x$1" = "x" ] ; then
   echo "Uso:\n\t$0 <version> [preexpdir]"
else
   versionmask=$1
   if [ "x$2" != "x" ] ; then
      DISTDIR=$2
   fi
   
   if [ ! -d "${SRCDIR}" ] ; then
      echo "ERROR: No se puede accerder al directorio ${SRCDIR}"
   else
      if [ ! -d "${DISTDIR}/${SRCDIR}" ] ; then
         echo "ERROR: No se puede acceder al directorio ${DISTDIR}/${SRCDIR}"
      else
         found=0
         for i in `ls -d1 ${SRCDIR}/*${versionmask} 2>/dev/null` ; do
            found=1
            modname=`echo $i | sed "s/${SRCDIR}\///"`
            install $modname ${DISTDIR} 
         done
         if [ $found -eq 0 ] ; then
         echo "ERROR: No se ha encontrado la versión ${versionmask} en el directorio ${SRCDIR}"
         fi
      fi
   fi
fi

