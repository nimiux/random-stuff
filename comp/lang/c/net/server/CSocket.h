/*****************************************************************************
 * ++
 * Proyecto     : Broker DICADI-GOLS.
 * 
 * Autor        : Jose Maria Alonso. 24/05/2004.
 *
 * Modulo       : CSocket.h
 *
 * Descripcion  : Definicion de un socket del sistema.
 *
 * Historial    :
 *
 * Autor                Fecha           Descripcion
 * ----------------     --------------  ------------------------------------
 * --
 ******************************************************************************/

#ifndef __CSOCKET_H__
#define __CSOCKET_H__

extern "C" {
   #include <sys/socket.h>
   #include <stdio.h>
   #include <string.h>
}


#include "CLogger.h"

class CSocket {

   private:
      // Logger de la clase
      CLogger *m_pxlogger;

      // Socket descriptor
      int m_isockfd;
      // Socket address
      struct sockaddr_in m_xsockaddr;

      static char *GetLastError();

   public:
      // Buffers 
      const static unsigned int MAX_INBUFFER_SIZE;
      const static unsigned int MAX_OUTBUFFER_SIZE;

      // Constantes de retorno
      const static int ID_RETURN_NOPENDINGCONNS;
      const static int ID_RETURN_INITOK;
      const static int ID_RETURN_ERRORBINDINGSOCKET;
      const static int ID_RETURN_ERRORACCEPT;

      CSocket();
     ~CSocket();

      CLogger *GetLogger();

      char *GetAddress();
      unsigned int GetPort();

      // Inicializacion del socket usando un puerto
      int Initialize(unsigned int uiport
                    , unsigned int uimaxpendingconns
                    , CLogger *pxlogger);

      // Inicializacion del socket aceptando la conexion de otro socket
      int Initialize(int iinsockfd, CLogger *pxlogger);

      bool Send(char *szdata);
      bool Receive(char *szdata);

      void Close();

      CSocket *NewClient(bool *pbnewclient);

};

#else
   class CSocket;
#endif // __CSOCKET_H__
