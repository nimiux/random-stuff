/*****************************************************************************
 * ++
 * Proyecto     : Broker DICADI-GOLS.
 *
 * Autor        : Jose Maria Alonso. 24/05/2004.
 *
 * Modulo       : CSocket.cc
 *
 * Descripcion  : Definicion de un socket del sistema.
 *
 * Historial    :
 *
 * Autor                Fecha           Descripcion
 * ----------------     --------------  ------------------------------------
 * --
 ******************************************************************************/

extern "C" {
   #include <sys/socket.h>
   #include <sys/wait.h>
   #include <arpa/inet.h>
   #include <unistd.h>
   #include <assert.h>
   #include <stdio.h>
   #include <errno.h>
   #include <fcntl.h>
}

#include <CSocket.h>

#include "CUtils.h"

/****************************************************************************\
 * Opciones de Debug y Traza
\****************************************************************************/
#ifdef TRACE_CSOCKET
#define TRACE(x) x
#else
#define TRACE(x)
#endif

// Buffers
const unsigned int CSocket::MAX_INBUFFER_SIZE                      = 128;
const unsigned int CSocket::MAX_OUTBUFFER_SIZE                     = 128;

// Constantes de retorno
const int CSocket::ID_RETURN_NOPENDINGCONNS                        =  1;
const int CSocket::ID_RETURN_INITOK                                =  0;
const int CSocket::ID_RETURN_ERRORBINDINGSOCKET                    = -1;
const int CSocket::ID_RETURN_ERRORACCEPT                           = -2;

CSocket::CSocket() {
   TRACE( printf("CSocket::CSocket\n"); )
}

CSocket::~CSocket () {
   TRACE( printf("CSocket::~CSocket\n"); )
   Close();
}

char *CSocket::GetLastError() {
   static char szerrordesc[1024];
   strcpy(szerrordesc, strerror(errno));
   return szerrordesc;
}

CLogger *CSocket::GetLogger() {
   return m_pxlogger;
}

char *CSocket::GetAddress() {
   return inet_ntoa(m_xsockaddr.sin_addr);
}

unsigned int CSocket::GetPort() {
   return ntohs(m_xsockaddr.sin_port);
}

int CSocket::Initialize(unsigned int uiport
                      , unsigned int uimaxpendingconns
                      , CLogger *pxlogger) {

   TRACE( printf("CSocket::Initialize\n"); )
   int ireturn = CSocket::ID_RETURN_INITOK;
   int piyes = 1;

   assert(pxlogger);
   
   m_pxlogger = pxlogger;

   if ((m_isockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
      ireturn = CSocket::ID_RETURN_ERRORBINDINGSOCKET;
      GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                                           , "Error al crear el listener: %s"
                                           , GetLastError()); 
   } else {
      if (setsockopt(m_isockfd, SOL_SOCKET, SO_REUSEADDR, &piyes
                                          , sizeof(int)) == -1) {
         ireturn = CSocket::ID_RETURN_ERRORBINDINGSOCKET;
         GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                               , "Error al cambiar opciones del listener: %s"
                               , GetLastError()); 
      } else {
         m_xsockaddr.sin_family      = AF_INET; 
         m_xsockaddr.sin_port        = htons(uiport); 
         m_xsockaddr.sin_addr.s_addr = INADDR_ANY; 
         memset(&(m_xsockaddr.sin_zero), '\0', 8); 
         if (bind(m_isockfd, (struct sockaddr *)&m_xsockaddr
                           , sizeof(struct sockaddr)) == -1) {
            ireturn = CSocket::ID_RETURN_ERRORBINDINGSOCKET;
            GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                                      , "Error al hacer bind del listener: %s"
                                      , GetLastError()); 
         } else {
            if (listen(m_isockfd, uimaxpendingconns) == -1) {
               ireturn = CSocket::ID_RETURN_ERRORBINDINGSOCKET;
               GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                                     , "Error al configurar el listener: %s"
                                     , GetLastError()); 
            } else {
               GetLogger()->WriteLine(CLogger::FULL, _FILE_, _LINE_
                                     , "Listener configurado correctamente.");
            }
         }
      } 
   }  
   return ireturn;
}

int CSocket::Initialize(int iinsockfd, CLogger *pxlogger)  {

   TRACE( printf("CSocket::Initialize/Accept\n"); )
   int sin_size = sizeof(struct sockaddr_in);
   int ireturn = CSocket::ID_RETURN_INITOK;
   bool bend = false;

   assert(pxlogger);

   m_pxlogger = pxlogger;
  
   while (!bend) {
      if ((m_isockfd = accept(iinsockfd, (struct sockaddr *)&m_xsockaddr
                                           , (socklen_t *)&sin_size)) == -1) {
         if (errno != EINTR) {
            GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                                , "Error en inicializacion del cliente: %s"
                                , GetLastError());
            ireturn = CSocket::ID_RETURN_ERRORACCEPT;
            bend = true;
         }
      } else {
         bend = true;
      }
   }
   return ireturn;
}

bool CSocket::Send(char *szdata) {
   bool bresult = true;
   TRACE( printf("CSocket::Send\n"); )

   strcpy(szdata, CUtils::SubString(szdata, 0 , MAX_OUTBUFFER_SIZE));
   if (send(m_isockfd, szdata, strlen(szdata), 0) == -1) {
      GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                                , "Error al enviar al cliente: %s"
                                , GetLastError());
      bresult = false;
   }
   return bresult;
}

bool CSocket::Receive(char *szdata) {
   int ibytes;
   bool bresult = true;
   TRACE( printf("CSocket::Receive\n"); )

   if ((ibytes = recv(m_isockfd, szdata, CSocket::MAX_INBUFFER_SIZE, 0)) == -1){
      GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                                , "Error al recibir desde el cliente: %s"
                                , GetLastError());
      bresult = false;
   } else {
      szdata[ibytes] = '\0';
   }
   return bresult;
}

void CSocket::Close() {
   
   TRACE( printf("CSocket::Close\n"); )
   close(m_isockfd);
   shutdown(m_isockfd, 2);
}

CSocket *CSocket::NewClient(bool *pbnewclient) {
   CSocket *pxsocket = NULL;
   
   TRACE( printf("CSocket::NewClient\n"); )

   *pbnewclient = false;
   pxsocket = new CSocket();
   
   if (pxsocket) {
      int iresult =pxsocket->Initialize(m_isockfd, m_pxlogger);
      if (iresult == CSocket::ID_RETURN_INITOK) {
         *pbnewclient = true;
      } else { 
         if (iresult != CSocket::ID_RETURN_NOPENDINGCONNS) {
            GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                                , "No se ha podido inicializar la conexion con "
                                  "el cliente.");
            SAFE_DELETE(pxsocket);
         }
      } 
   } else {
      GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                                   , "No se ha podido crear la conexion con el"
                                     "cliente.");
   }
   return pxsocket;
}
