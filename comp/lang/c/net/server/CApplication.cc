/*****************************************************************************
 * ++
 * Proyecto     : Broker DICADI-GOLS.
 *
 * Autor        : Jose Maria Alonso. 20/05/2004.
 *
 * Modulo       : CApplication.cc
 *
 * Descripcion  : Modulo de Aplicacion.
 *
 * Historial    :
 *
 * Autor                Fecha           Descripcion
 * ----------------     --------------  ------------------------------------
 * --
 ******************************************************************************/

extern "C" {
   #include <sys/wait.h>
   #include <sys/timeb.h>
   #include <stdlib.h>
   #include <signal.h>
   #include <unistd.h>
   #include <errno.h>
}

#include "CApplication.h"

#include "CSocket.h"
#include "CClientTrans.h"

#include "CUtils.h"

#include "CGateway.h"

/****************************************************************************\
 * Opciones de Debug y Traza
\****************************************************************************/
#ifdef TRACE_CAPPLICATION
#define TRACE(x) x
#else
#define TRACE(x)
#endif

// Constantes de valores por defecto
const unsigned int CApplication::DEFAULT_MAXPENDINGCONNECTIONS      =  10;

// Constante de definicion de la version de la aplicacion
const unsigned int CApplication::VERSION                =  1; 
const unsigned int CApplication::SUBVERSION             =  0;
const char CApplication::DESCRIPTION[]                  =  "Broker DICADI-GOLS";

char CApplication::m_szversionbuff[6]                                   = "";

// Constantes de retorno de inicializacion
const int CApplication::ID_RETURN_INITOK                                =  0;
const int CApplication::ID_RETURN_ERRORLOGINIT                          = -1;
const int CApplication::ID_RETURN_ERRORENVINIT                          = -2;
const int CApplication::ID_RETURN_ERRORBINDINGSOCKET                    = -3;
const int CApplication::ID_RETURN_ERRORCONFIGURINGLISTENER              = -4;

CApplication::CApplication(char *szappname) {
   TRACE( printf("CApplication::CApplication\n"); )
   strcpy(m_szappname, szappname);
   sprintf(m_szversionbuff, "%ld.%02ld", VERSION, SUBVERSION);
}

CApplication::~CApplication () {
   TRACE( printf("CApplication::~CApplication\n"); )
   Stop();
}

CLogger *CApplication::CreateLogger() {

   TRACE ( printf("CApplication::CreateLogger\n"); )

   char szlogfileprefix[CLOG_MAX_LOGFILE_NAME + 1]="";
   char szseverity[128];

   CUtils::GetString((char*)ENVVAR_LOGFILEPREFIX, szlogfileprefix);
   if (!strlen(szlogfileprefix)) {
      strcpy(szlogfileprefix, DEFAULT_LOGFILE_PREFIX);
   }
   CUtils::GetString((char*)ENVVAR_SEVERITYLEVEL, szseverity);
   if (!strlen(szseverity)) {
      strcpy(szseverity, CLogger::LITERAL_ERROR);
   }
   m_pxlogger = CLogger::GetInstance(CLogger::ID_DICLOGGER
                                    , GetName()
                                    , szseverity
                                    , szlogfileprefix);
   return m_pxlogger;
}

void CApplication::Stop() {
   TRACE( printf("CApplication::Stop\n"); )
   GetLogger()->WriteLine(CLogger::INFO, _FILE_, _LINE_
                     , "Esperando la finalizacion de las conexiones cliente");
   waitpid(-1, 0 ,0);
   GetLogger()->WriteLine(CLogger::MANDATORY, _FILE_, _LINE_
                         , "Cerrando el listener.");
   SAFE_DELETE(m_pxlistener);
   GetLogger()->WriteLine(CLogger::MANDATORY, _FILE_, _LINE_
                         , "Fin de ejecucion del broker.");
   SAFE_DELETE(m_pxlogger);
}

void CApplication::ProcessRequest(CSocket *pxclientsock) {
   int itransresult;
   char szinbuf[CSocket::MAX_INBUFFER_SIZE + 1];
   char szoutbuf[CSocket::MAX_OUTBUFFER_SIZE];
   
   TRACE( printf("CApplication::ProcessRequest\n"); )

   GetListener()->Close(); // Se cierra el listener 
   // Se recibe la trama
   if (!pxclientsock->Receive(szinbuf)) {
      GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                                       , "Error al recibir la trama desde el "
                                         "cliente: %s"
                                       , pxclientsock->GetAddress());
   } else {
      unsigned long long utime1, utime2;
      // Se carga el tiempo actual en milisegundos
      utime1 = GetCurrentMs();
      GetLogger()->WriteLine(CLogger::MANDATORY, _FILE_, _LINE_
                                             , "Procesando peticion %s. "
                                               "Recibida desde cliente: %s:%u."
                                             , szinbuf
                                             , pxclientsock->GetAddress()
                                             , pxclientsock->GetPort());
      CClientTrans xtrans(GetLogger());
      itransresult = xtrans.DoTransaction(GetGOLSConnectString()
                                         , GetGOLSTimeout()
                                         , szinbuf
                                         , szoutbuf);
      if (!pxclientsock->Send(szoutbuf)) {
         GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                                  , "Error al enviar la respuesta al cliente.");
      } else {
         GetLogger()->WriteLine(CLogger::MANDATORY, _FILE_, _LINE_
                                                   , "Envio a %s:%u de: %s."
                                                   , pxclientsock->GetAddress()
                                                   , pxclientsock->GetPort()
                                                   , szoutbuf);
      }
      // Se carga el tiempo actual en milisegundos y se calcula
      // la diferencia
      utime2 = GetCurrentMs();
      utime2 = (utime1 && utime2?utime2-utime1:0);
      GetLogger()->WriteLine(CLogger::MANDATORY, _FILE_, _LINE_
                                          , "La transaccion se ha procesado "
                                            "en %llu ms."
                                          , utime2);
   }
   SAFE_DELETE(pxclientsock);
}

unsigned long long CApplication::GetCurrentMs() {
   struct timeb xtime;
   unsigned long long utime = 0;

   if (ftime(&xtime) != -1) {
      utime = xtime.time*1000+xtime.millitm;
   }
   return utime;
}

CSocket *CApplication::GetListener() {
   return m_pxlistener;
}

CLogger *CApplication::GetLogger() {
   return m_pxlogger;
}

char *CApplication::GetName() {
   return m_szappname;
}

char *CApplication::GetVersion() {
   return m_szversionbuff;
}

const char *CApplication::GetDescription() {
   return DESCRIPTION; 
}

unsigned int CApplication::GetPort() {
   return m_uiport;
}

unsigned int CApplication::GetMaxPendingConnections() {
   return m_uimaxpendingconns;
}

char *CApplication::GetGOLSConnectString() {
   return m_szgolsconnectstring;
}

unsigned int CApplication::GetGOLSTimeout() {
   return m_igolstimeout;
}

void CApplication::SetMaxPendingConnections(unsigned int uimaxpendingconns) {
   m_uimaxpendingconns = uimaxpendingconns;
}

void CApplication::SetGOLSConnectString(char *szgolsconnectstring) {
   strcpy(m_szgolsconnectstring, szgolsconnectstring);
}

void CApplication::SetGOLSTimeout(unsigned int uigolstimeout) {
   m_igolstimeout = uigolstimeout;
}

int CApplication::Initialize(unsigned int uiport) {
   
   char szgolstimeout[128] = "";
   char szmaxpendingconnections[128] = "";

   TRACE( printf("CApplication::Initialize\n"); )
        
   m_uiport = uiport;
   // Inicializa los miembros dato
   CreateLogger();
   if (!m_pxlogger) {
      return CApplication::ID_RETURN_ERRORLOGINIT;
   }

   // Muestra la version
   GetLogger()->WriteLine(CLogger::MANDATORY, _FILE_, _LINE_
                  , "%s. Version: %s", GetDescription(), GetVersion()); 
   
   // Valores de entorno para la conexion
   CUtils::GetString(ENVVAR_MAXPENDINGCONNECTIONS, szmaxpendingconnections);
   if (!strlen(szmaxpendingconnections)) {
      GetLogger()->WriteLine(CLogger::WARNING, _FILE_, _LINE_
                              , "No se puede obtener el numero maximo de "
                                "conexiones. Se toma el valor por defecto: %ld."
                              , CApplication::DEFAULT_MAXPENDINGCONNECTIONS);
      SetMaxPendingConnections(CApplication::DEFAULT_MAXPENDINGCONNECTIONS);
   } else {
      if ((!CUtils::IsANumber(szmaxpendingconnections)) ||
          (atoi(szmaxpendingconnections) <= 0)) {
         GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                        , "Se ha especificado un valor erroneo para el numero "
                          "maximo de conexiones: %s. Se toma el valor por "
                          "defecto: %ld."
                        , szmaxpendingconnections
                        , CApplication::DEFAULT_MAXPENDINGCONNECTIONS);
         SetMaxPendingConnections(CApplication::DEFAULT_MAXPENDINGCONNECTIONS);
      } else {
         SetMaxPendingConnections(atoi(szmaxpendingconnections));
         GetLogger()->WriteLine(CLogger::MANDATORY, _FILE_, _LINE_
                                            , "%s=%u"
                                            , ENVVAR_MAXPENDINGCONNECTIONS
                                            , GetMaxPendingConnections());
      }
   }

   // Valores de entorno para acceso a la pasarela GOLS
   CUtils::GetString(ENVVAR_GOLSCONNECTSTRING, m_szgolsconnectstring);
   if (!strlen(m_szgolsconnectstring)) {
      GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                        , "No se puede obtener el string de conexion a GOLS.");
      return CApplication::ID_RETURN_ERRORENVINIT;
   }
   GetLogger()->WriteLine(CLogger::MANDATORY, _FILE_, _LINE_
                                            , "%s=%s"
                                            , ENVVAR_GOLSCONNECTSTRING
                                            , m_szgolsconnectstring);
   CUtils::GetString(ENVVAR_GOLSGATEWAYTIMEOUT, szgolstimeout);
   if (!strlen(szgolstimeout))  {
      GetLogger()->WriteLine(CLogger::WARNING, _FILE_, _LINE_
                       , "No se puede obtener el timeout de acceso a GOLS. "
                         "Se toma el valor por defecto: %ld."
                       , CGateway::GATEWAY_DEFAULTTIMEOUT);
      SetGOLSTimeout(CGateway::GATEWAY_DEFAULTTIMEOUT);
   } else { 
      if ((!CUtils::IsANumber(szgolstimeout)) ||
          (atoi(szgolstimeout) <= 0)) {
         GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                     , "Se ha especificado un valor erroneo para el timeout "
                       "de acceso a GOLS: %s. Se toma el valor por defecto: "
                       "%ld."
                     , szgolstimeout
                     , CGateway::GATEWAY_DEFAULTTIMEOUT);
         SetGOLSTimeout(CGateway::GATEWAY_DEFAULTTIMEOUT);
      } else {
         SetGOLSTimeout(atoi(szgolstimeout));
         GetLogger()->WriteLine(CLogger::MANDATORY, _FILE_, _LINE_
                               , "%s=%ld"
                               , ENVVAR_GOLSGATEWAYTIMEOUT
                               , GetGOLSTimeout());
      }
   }
   // Inicializacion del listener
   m_pxlistener = new CSocket();
   int ireturn = m_pxlistener->Initialize(GetPort()
                                         , GetMaxPendingConnections()
                                         , GetLogger());
   return ireturn;
}

void CApplication::Run() {
   bool bnewclient;
   int ifork; 
 
   TRACE( printf("CApplication::Run\n"); )
         
   GetLogger()->WriteLine(CLogger::MANDATORY, _FILE_, _LINE_
                                                , "Escuchando en el puerto: %u."
                                                , GetPort());
   sigset_t sset;
   sigfillset(&sset);
   sigprocmask(SIG_BLOCK, &sset, NULL);

   while (1) {
      sigprocmask(SIG_UNBLOCK, &sset, NULL);
      CSocket *pxclientsock = GetListener()->NewClient(&bnewclient);
      sigprocmask(SIG_BLOCK, &sset, NULL);
      if (bnewclient) {
         GetLogger()->WriteLine(CLogger::MANDATORY, _FILE_, _LINE_
                                             , "Recibida peticion desde: %s:%u."
                                             , pxclientsock->GetAddress()
                                             , pxclientsock->GetPort());
         int ifork = fork();
         switch (ifork) {
            case 0: // Proceso hijo de tratamiento de la peticion
               ProcessRequest(pxclientsock);
               _exit(0);
            break;
            case (pid_t)-1: // No se pudo crear el proceso hijo
               GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                                        , "No se ha podido atender la peticion "
                                          "debido a un error del sistema.");
            break;
         }
         SAFE_DELETE(pxclientsock); // El padre cierra el socket cliente
      }
   }
}

