# -----------------------------------------------------------------------------
# Macros para construccion con gcc
# -----------------------------------------------------------------------------

# **************************
# Define System dependencies
# **************************
PORTDEPS_DIR_NAME=ssunos5

# Define local file extensions
O=.o
A=.a
EXE=
SH=.sh

LIB_DIR=lib/$(PORTDEPS_DIR_NAME)

XM_TYPE=xm

# ************************************
# Define command symbols
# ************************************
# C compiler command
CC=gcc

# C++ compiler command
CCC=g++

# Linker command
LINKER=gcc

# library command
AR=ar

# ranlib command
RANLIB=ranlib

# strip command
STRIP=strip

# change directory command
CD=cd

# copy command
CP=cp

# remove command
RM=rm -f

# mkdir command
MKDIR=mkdir -p

# link command
LN=LN -s

# touch command
TOUCH=touch

# move command
MV=mv

# make command
MAKE=/usr/ccs/bin/make

# change the file group attribute
CHGRP=chgrp

# change the rights
CHMOD=chmod

# echo something to the console
ECHO=echo

# command processor
SH_EXE=csh -f

SET_EXE_RIGHTS=$(CHMOD) 755
SET_EXE_GROUP=$(CHGRP) eworks
SET_LIC_RIGHTS=$(CHMOD) 644
SET_LIC_GROUP=$(CHGRP) eworks
SET_SRC_RIGHTS=$(CHMOD) 644
SET_SRC_GROUP=$(CHGRP) eworks
SET_DB_RIGHTS=$(CHMOD) 666
SET_DB_GROUP=$(CHGRP) eworks
SET_SDK_RIGHTS=$(CHMOD) 666
SET_SDK_GROUP=$(CHGRP) eworks

# ************************************
# Define Commands flags
# ************************************
# Global environment-specific archive A flags
GAFLAGS=cruv

# Global environment-specific archive Y flags
GYFLAGS=-v -d

# Global compile flags
GCFLAGS=-DSunOS5 -O -DLINT_ARGS -D_REENTRANT

# Global ANSI flag setting
GCFLAGS_ANSI=

# Global environment-specific linker L flags
GLFLAGS=

# Global environment-specific linker L flags for shared library
GLFLAGS_SHARED=

# Global environment-specific shared library  L flags
GSFLAGS= -G -B symbolic
SHAREDLIB_FLAGS= -fpic

# Global User-defined  DFLAGS
GDFLAGS=$(MAN_DFLAGS)

# System libraries
GSYS_LIBS= -lsocket -lnsl -lrt

REC_MAKE_BEG=($(CD)
REC_MAKE_MID=; $(MAKE)
REC_MAKE_END=)

FIND_FILES_ARG={} \;

# **************************
# C++ Runtime lib defs
# **************************
CCC_RUNTIME_LIB=-L/usr/local/lib -lstdc++
