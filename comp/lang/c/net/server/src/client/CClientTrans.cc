/*****************************************************************************
 * ++
 * Proyecto     : Broker DICADI-GOLS.
 *
 * Autor        : Jose Maria Alonso. 24/05/2004.
 *
 * Modulo       : CClientTrans.cc
 *
 * Descripcion  : Define una transaccion del cliente DICADI.
 *
 * Historial    :
 *
 * Autor                Fecha           Descripcion
 * ----------------     --------------  ------------------------------------
 * --
 ******************************************************************************/

extern "C" {
   #include <stdio.h> 
}

#include "CClientTrans.h"

#include "CDicadiMsg.h"
#include "CDicadiRsp.h"

//#include "CGolsResp.h"

#include "CUtils.h"

/****************************************************************************\
 * Opciones de Debug y Traza
\****************************************************************************/
#ifdef TRACE_CCLIENTTRANS
#define TRACE(x) x
#else
#define TRACE(x)
#endif

// Constantes de retorno 
const int CClientTrans::ID_RETURN_INITOK                                =  0;
const int CClientTrans::ID_RETURN_VALIDATIONERROR                       = -1;

CClientTrans::CClientTrans(CLogger *pxlogger) {
   TRACE( printf("CClientTrans::CClientTrans\n"); )
   m_pxlogger = pxlogger;
}

CClientTrans::~CClientTrans () {
   TRACE( printf("CClientTrans::~CClientTrans\n"); )
}

CLogger *CClientTrans::GetLogger() {
   return m_pxlogger;
}

int CClientTrans::DoTransaction(char *szgolsconnectstring
                               , unsigned int uitimeout
                               , char *szindata
                               , char *szoutdata) {

   int iinitresult;
   bool bvalidationresult=false, btransactionresult=false
      , bprotocolviolation=false;
   CGolsResp *pxgolsrsp = NULL;
   TRACE( printf("CClientTrans::DoTransaction\n"); )

   GetLogger()->WriteLine(CLogger::INFO, _FILE_, _LINE_
                            , "Se va procesar la transaccion para la trama: %s."
                            , szindata); 
   CDicadiMsg *pxmsg;
   CDicadiRsp *pxrsp;
   pxmsg = CDicadiMsg::GetInstance(GetLogger()
                                  , szindata
                                  , &iinitresult);
   if (iinitresult == CDicadiMsg::ID_VALIDATION_OK) {
      bvalidationresult = true;
      GetLogger()->WriteLine(CLogger::FULL, _FILE_, _LINE_
                                  , "Se ha validado correctamente la trama: %s."
                                  , pxmsg->GetDescription()); 
      CGateway *pxgateway = CGateway::GetInstancia( false 
                                                  , CGateway::ID_GATEWAY_TNID
                                                  , szgolsconnectstring 
                                                  , uitimeout
                                                  , GetLogger());
      if (!pxgateway) {
         GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                  , "No se ha podido crear la pasarela GOLS."); 
      } else {
         int itransresult;
         pxgolsrsp = pxmsg->DoTransaction(pxgateway, &itransresult);
         SAFE_DELETE(pxgateway);
         GetLogger()->WriteLine(CLogger::FULL, _FILE_, _LINE_
                  , "La transaccion GOLS ha devuelto: %ld.", itransresult); 
         if (itransresult == CGateway::ID_RETURN_TRANSACTION_OK) {
            btransactionresult=true;
         } else {
            if (itransresult == CGateway::ID_RETURN_PROTOCOL_VIOLATION) {
               bprotocolviolation=true;
            }
         }
      }         
   }
   pxrsp = CDicadiRsp::GetInstance(GetLogger()
                                  , bvalidationresult 
                                  , btransactionresult 
                                  , bprotocolviolation
                                  , pxmsg 
                                  , pxgolsrsp);
   strcpy(szoutdata, pxrsp->GetResponse());
   SAFE_DELETE(pxgolsrsp);
   SAFE_DELETE(pxmsg);
   SAFE_DELETE(pxrsp);
   GetLogger()->WriteLine(CLogger::INFO, _FILE_, _LINE_
                  , "La Transaccion se ha procesado correctamente. Trama: %s."
                  , szoutdata); 
   return CClientTrans::ID_RETURN_INITOK;
}

