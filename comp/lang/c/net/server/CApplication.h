/*****************************************************************************
 * ++
 * Proyecto     : Broker DICADI-GOLS.
 * 
 * Autor        : Jose Maria Alonso. 20/05/2004.
 *
 * Modulo       : CApplication.h
 *
 * Descripcion  : Modulo de aplicacion.
 *
 * Historial    :
 *
 * Autor                Fecha           Descripcion
 * ----------------     --------------  ------------------------------------
 * --
 ******************************************************************************/

#ifndef __CAPPLICATION_H__
#define __CAPPLICATION_H__

#include "CSocket.h"
#include "CLogger.h"

// Variables de entorno

// Log
#define ENVVAR_LOGFILEPREFIX            "DICADIBROKER_LOGFILEPREFIX"
#define ENVVAR_SEVERITYLEVEL            "DICADIBROKER_SEVERITYLEVEL"

// Conexiones cliente
#define ENVVAR_MAXPENDINGCONNECTIONS    "DICADIBROKER_MAXPENDINGCONNECTIONS"

// Gateway GOLS
#define ENVVAR_GOLSCONNECTSTRING        "DICADIBROKER_GOLSCONNECTSTRING"
#define ENVVAR_GOLSGATEWAYTIMEOUT       "DICADIBROKER_GOLSGATEWAYTIMEOUT"

class CApplication {

   private:
      // Constantes de valores por defecto
      const static unsigned int DEFAULT_MAXPENDINGCONNECTIONS;

      // Listener
      CSocket *m_pxlistener;

      // Logger de la aplicacion
      CLogger *m_pxlogger;

      // Nombre de la aplicacion
      char m_szappname[128];

      // Constantes de descripcion, version de la aplicacion
      const static unsigned int VERSION; 
      const static unsigned int SUBVERSION; 
      const static char         DESCRIPTION[];
      
      static char              m_szversionbuff[6];

      // Puerto TCP/IP en el que se escucha
      unsigned int m_uiport;

      // Numero maximo de conexiones pendientes
      unsigned int             m_uimaxpendingconns; 

      // Tiempo de inactividad entre acceptacion de conexiones 
      useconds_t               m_uisleeptime;

      // Acceso a GOLS
      char m_szgolsconnectstring[128];
      unsigned int  m_igolstimeout;

      CLogger *CreateLogger();

      void Stop();

      void ProcessRequest(CSocket *pxclientsock);

      unsigned long long GetCurrentMs();

   public:
      // Constantes de retorno
      const static int ID_RETURN_INITOK;
      const static int ID_RETURN_ERRORLOGINIT;
      const static int ID_RETURN_ERRORENVINIT;
      const static int ID_RETURN_ERRORBINDINGSOCKET;
      const static int ID_RETURN_ERRORCONFIGURINGLISTENER;

      CApplication(char *szappname);
     ~CApplication();

      CSocket *GetListener();
      CLogger *GetLogger();

      char *GetName();

      char *GetVersion();
      const char *GetDescription();
      unsigned int GetPort();
      unsigned int GetMaxPendingConnections();
      char *GetGOLSConnectString();
      unsigned int GetGOLSTimeout();
      
      void SetMaxPendingConnections(unsigned int uimaxpendingconns);
      void SetGOLSConnectString(char *szgolsconnectstring);
      void SetGOLSTimeout(unsigned int uigolstimeout);

      int Initialize(unsigned int uiport);

      void Run();
};

#else
   class CApplication;
#endif // __CAPPLICATION_H__
