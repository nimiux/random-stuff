#include <errno.h> 
#include <sys/socket.h> 
#include <netinet/in.h> 
   
int populate_inet_addr ( char * szaddr
                       , uint16_t nport
                       , struct sockaddr_in * psa) {
   int nresult = 0;
   in_addr_t naddr = inet_addr( szaddr );   
   if ((naddr == -1) && (strcmp(szaddr, "255.255.255.255") != 0)) {
      nresult = -1;
   } else {
      psa->sin_family = AF_INET;
      psa->sin_port = htons(nport);
      psa->sin_addr.s_addr = naddr; 
      memset(psa->sin_zero, '\0', 8);
   }
   return nresult;
}

int
main(int argc, char *argv[]) {
   struct sockaddr_in sa;
   int s;
   if (argc < 3) {
      printf ("%s <ip> <port>.\n", argv[0]);
      exit(1);
   }
   unsigned int nport = atoi(argv[2]);
   printf ("Using ip: %s port: %ld.\n", argv[1], nport);
   if (populate_inet_addr (argv[1], nport, &sa ) == -1) {
      printf ("Error creating ip address.\n");
      exit(1);
   }
   if ((s = socket (PF_INET, SOCK_STREAM, 0)) == -1) {
      perror("Error creating socket");
      exit(1);
   }
   int sopt = 1;
   printf ("Setting opts.\n");
   if (setsockopt(s, SOL_SOCKET, SO_REUSEADDR, &sopt, sizeof(int)) == -1) {
      perror("Error en setsockopt.");
      exit(1);
   }
   if (bind(s, (struct sockaddr *)&sa, sizeof(struct sockaddr)) == -1) {
      char szbuf[1024];
      sprintf(szbuf, "Error %ld binding to: %s:%ld\n.", errno
                                                      , inet_ntoa(sa.sin_addr)
                                                      , sa.sin_port);
      perror(szbuf);
      exit(1);
   }
   if (populate_inet_addr ("172.24.128.6", atoi(argv[2]), &sa ) == -1) {
      perror("Unable to prepare for connect to 172.24.128.6");
      exit(1);
   }
   if (connect(s, (struct sockaddr *)&sa, sizeof(struct sockaddr)) == -1) { 
      perror("Unable to connect to 172.24.128.6");
      exit(1);
   }

   char buf[1024];
   char *pbuf = &buf[0];
   int nread = recv (s, (void *)pbuf, (size_t)1024, 0);
   if (nread == -1) {
      perror("Unable to read from server");
   } else {
      printf("Recibidos %ld bytes: %s.\n", nread, buf);
   }
   close(s); 
}

