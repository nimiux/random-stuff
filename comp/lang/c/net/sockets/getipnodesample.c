#include <sys/socket.h>
#include <netdb.h>

int main(int argc, const char **argv) {
   char abuf[INET6_ADDRSTRLEN];
   int error_num;
   struct hostent *hp;
   char **p;

   if (argc != 2) {
      (void) printf("usage: %s hostname\n", argv[0]);
      exit (1);
   }

   /* argv[1] can be a pointer to a hostname or literal IP address */
   hp = getipnodebyname(argv[1], AF_INET6
                       , AI_ALL | AI_ADDRCONFIG | AI_V4MAPPED
                       , &error_num);
   if (hp == NULL) {
      if (error_num == TRY_AGAIN) {
         printf("%s: unknown host or invalid literal address "
                "(try again later)\n", argv[1]);
      } else {
         printf("%s: unknown host or invalid literal address\n", argv[1]);
      }
      exit (1);
   }
   for (p = hp->h_addr_list; *p; p++) {
      struct in6_addr in6;
      char **q;

      memcpy((caddr_t)&in6, *p,hp->h_length);
      (void) printf("%s\t%s", inet_ntop(AF_INET6, (void *)&in6, abuf
                                       , sizeof(abuf)), hp->h_name);
      for (q = hp->h_aliases; *q; q++)
         (void) printf("\t%s\n", *q);
      (void) putchar('\n');
   }
   freehostent(hp);
   exit (0);
}
