#ifndef __CLOGGER_H__
#define __CLOGGER_H__

#define CLOG_MAX_APPNAME_LEN                128

#define CLOG_MAX_LOGFILE_NAME               256 
#define CLOG_MAX_SEVERITY_LEN               8

#define CLOG_MAX_LOGFILE_MESSAGE_LEN        1024 

#define DEFAULT_LOGFILE_PREFIX              "/tmp/"

#define _FILE_ (char *)__FILE__
#define _LINE_ __LINE__

class CLogger {

   protected:

      char             m_szappname[CLOG_MAX_APPNAME_LEN + 1];
      unsigned int     m_nseverity;
      char             m_szlogfile [CLOG_MAX_LOGFILE_NAME + 1];

      virtual bool Initialize(char *szappname
                             , char *szseverity
                             , char *szlogfileprefix) = 0;

      static unsigned int TranslateSeverityLevel(char *szseverity);
      bool _writeline(char *pszmessage);

   public:
      const static unsigned int ID_SERVERLOGGER;
      const static unsigned int ID_TEXTLOGGER;

      const static unsigned int MANDATORY;
      const static unsigned int ERROR;
      const static unsigned int WARNING;
      const static unsigned int INFO;
      const static unsigned int FULL;

      const static char LITERAL_ERROR  [CLOG_MAX_SEVERITY_LEN + 1];
      const static char LITERAL_WARNING[CLOG_MAX_SEVERITY_LEN + 1];
      const static char LITERAL_INFO   [CLOG_MAX_SEVERITY_LEN + 1];
      const static char LITERAL_FULL   [CLOG_MAX_SEVERITY_LEN + 1];

      CLogger();
      virtual ~CLogger() = 0;

      static char *TranslateSeverityLevel(unsigned int nseverity);

      char *GetAppName();
      unsigned int GetSeverity();
      char *GetLogFileName();

      static CLogger *GetInstance(unsigned int ntype
                                , char *szappname
                                , char *szseverity
                                , char *szlogfileprefix);

      virtual void WriteLine(unsigned int nlevel, char *pszfile, int nline
                            , const char *pszformat, ...) = 0;
};

#else
   class CLogger;
#endif // __CLOGGER_H__
