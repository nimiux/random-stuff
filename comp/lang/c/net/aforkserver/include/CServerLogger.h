#ifndef __CSERVERLOGGER_H__
#define __CSERVERLOGGER_H__

#include "CDaemonLogger.h"

class CServerLogger : public CDaemonLogger {

   private:
		
		char *BuildLogFileName();
		bool UpdateLogFileName();

   public:
      CServerLogger();
      ~CServerLogger();

};

#else
   class CServerLogger;
#endif // __CServerLOGGER_H__
