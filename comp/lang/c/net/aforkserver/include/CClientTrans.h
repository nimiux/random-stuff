#ifndef __CCLIENTTRANS_H__
#define __CCLIENTTRANS_H__

#include "CLogger.h"

class CClientTrans {

   private:
      // Logger de la transaccion 
      CLogger *m_pxlogger;

   public:
      // Constantes de retorno
      const static int ID_RETURN_INITOK;
      const static int ID_RETURN_VALIDATIONERROR;

      CClientTrans(CLogger *pxlogger);
      ~CClientTrans();

      CLogger *GetLogger();

      int DoTransaction(char *connectstring
                       , char *szindata
                       , char *szoutdata);

};

#else
   class CClientTrans;
#endif // __CCLIENTTRANS_H__
