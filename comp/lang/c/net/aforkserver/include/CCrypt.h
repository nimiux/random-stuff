#ifndef __CCRYPT_H__
#define __CCRYPT_H__

#include "mycrypt.h"

class CCrypt {
   private:
      static const char SK128_DEFAULT_KEY[]; 
      static char m_szcryptbuffer[1024]; 

   public:
      static unsigned char *ConvertirAStringHex(unsigned char * szstring);
      static unsigned char *ConvertirDesdeStringHex(unsigned char * szstring);
      static bool sk128encripta(const char *szstring, char *szresult);
      static bool sk128desencripta(const char *szstring, char *szresult);
};

#else
   class CCrypt;
#endif // __CCRYPT_H__
