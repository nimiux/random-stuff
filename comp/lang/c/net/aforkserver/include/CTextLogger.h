#ifndef __CTEXTLOGGER_H__
#define __CTEXTLOGGER_H__

#include "CLogger.h"

class CTextLogger : public CLogger {

   private:
      char *BuildLogFileName();
      bool UpdateLogFileName();

      char             m_szlogfileprefix[CLOG_MAX_LOGFILE_NAME + 1];
      unsigned long    m_nlogdate;

	protected:
      bool Initialize(char * szappname
                     , char *szseverity
                     , char *szlogfileprefix);

   public:
      CTextLogger();
      ~CTextLogger();

      void WriteLine(unsigned int nlevel, char *pszfile, int nline
                    , const char *pszformat, ...);
};

#else
   class CTextLogger;
#endif // __CTEXTLOGGER_H__
