#ifndef __CAPLICATION_H__
#define __CAPLICATION_H__

#include "CLogger.h"

// Variables de entorno

// Log
#define ENVVAR_LOGFILEPREFIX            "SERVER_LOGFILEPREFIX"
#define ENVVAR_SEVERITYLEVEL            "SERVER_SEVERITYLEVEL"

// Conexiones cliente
#define ENVVAR_MAXPENDINGCONNECTIONS    "SERVER_MAXPENDINGCONNECTIONS"
#define ENVVAR_SLEEPTIME                "SERVER_SLEEPTIME"

class CAplication {

   private:
      // Constantes de valores por defecto
      const static unsigned int DEFAULT_MAXPENDINGCONNECTIONS;
      const static unsigned int DEFAULT_SLEEPTIME;

      // Logger de la aplicacion
      CLogger *m_pxlogger;

		// Nombre de la aplicacion
      char m_szappname[128];

      // Constantes de descripcion, version de la aplicacion
      const static unsigned int VERSION; 
      const static unsigned int SUBVERSION; 
      const static char         DESCRIPTION[];
      
		static char              m_szversionbuff[6];

      // Puerto TCP/IP en el que se escucha
      unsigned int m_uiport;

      // Numero maximo de conexiones pendientes
      unsigned int             m_imaxpendingconnections; 

      // Tiempo de inactividad entre acceptacion de conexiones 
      unsigned int             m_isleeptime;

      CLogger *CreateLogger();

   public:
      // Constantes de retorno
      const static int ID_RETURN_INITOK;
      const static int ID_RETURN_ERRORLOGINIT;
      const static int ID_RETURN_ERRORENVINIT;
      const static int ID_RETURN_ERRORBINDINGSOCKET;
      const static int ID_RETURN_ERRORCONFIGURINGLISTENER;

      CAplication(char *szappname);
     ~CAplication();

      CLogger *GetLogger();

      char *GetName();

      char *GetVersion();
      const char *GetDescription();
      unsigned int GetPort();
      unsigned int GetMaxPendingConnections();
      unsigned int GetSleepTime();

      int Initialize(unsigned int uiport);

      bool Run();
};

#else
   class CAplication;
#endif // __CAPLICATION_H__
