#ifndef __CDAEMONLOGGER_H__
#define __CDAEMONLOGGER_H__

#include "CLogger.h"

class CDaemonLogger : public CLogger {

   protected:
		
      virtual char *BuildLogFileName() = 0;
      bool UpdateLogFileName();

      char             m_szlogfileprefix[CLOG_MAX_LOGFILE_NAME + 1];
      unsigned long    m_nlogdate;

      CDaemonLogger();

      bool Initialize(char * szappname
                    , char *szseverity
                    , char *szlogfileprefix);

   public:
      CDaemonLogger(char *szbankcode, char *szparallelid);
      virtual ~CDaemonLogger() = 0;

      void WriteLine(unsigned int nlevel, char *pszfile, int nline
                    , const char *pszformat, ...);
};

#else
   class CDaemonLogger;
#endif // __CDAEMONLOGGER_H__
