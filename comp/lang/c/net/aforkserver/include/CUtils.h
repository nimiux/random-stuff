#ifndef __CUTILS_H__
#define __CUTILS_H__

#include "CLogger.h"

#define DATE_TIME_BUFFER_LEN            36 
#define STRING_BUFFER_LEN             4096

#define GETDATETIME                   CUtils::_getdatetime("%d/%m/%Y-%T")
#define GETDATEAAAAMMDD               CUtils::_getdatetime("%Y%m%d")
#define GETDATEAA                     CUtils::_getdatetime("%y")
#define GETDATEAAAA                   CUtils::_getdatetime("%Y")
#define GETTIMEHH24MMSS               CUtils::_getdatetime("%H%M%S")
#define GETTIMEHH24MMSSMS             CUtils::GetDateTimeMs()
#define GETJULIANDATE                 CUtils::_getdatetime("%j")

// Macro para el borrado seguro de punteros
#define SAFE_DELETE(x)       if( x != NULL ) delete x; x = NULL

class CUtils {

   private:
      static char m_szstringbuffer  [STRING_BUFFER_LEN + 1];

   public:
      static char *_getdatetime(const char *pszformat);
      static char *GetDateTimeMs();
      static void GetString(const char *pszVarName, char *pszVarValue);
      static char* SubString(char *pszString, unsigned int init, unsigned int len);
      static char* ToMays(char *pszstring);
      static void CopyString(char *pszDst, char *pszSrc, int init, int len);
      static void CopyInteger(char *pszDst, int nSrc, int init, int len);
      static bool HaveNullValue(char *pszstring);
      static char *GetPathName(char *pszpath);
      static char *GetFileName(char *pszpath);
      static bool SendMail(CLogger *pxlogger
                          , char *szemaillist
                          , char *szsubject
                          , char *szmessage);
      static bool IsANumber(char *pszstring);
      static bool FileExists(char *pszfile);
};

#else
   class CUtils;
#endif // __CUTILS_H__
