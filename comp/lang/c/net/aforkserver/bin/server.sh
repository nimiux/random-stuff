#!/bin/sh

##############################################################################
#
# Script de control de un servidor generico.
#
# Codigos de salida:
#       0 - operacion correcta
#       1 - dicbroker parado
#       2 - error en el uso del comando
#       3 - no se pudo arrancar dicbroker
#       4 - no se pudo parar dicbroker
#
##############################################################################

################################## VARIABLES #################################

LISTENER_PORT=28001

SERVER_BIN=myss
SERVER_EXE=`pwd`/${SERVER_BIN}

# Shared libraries
LD_LIBRARY_PATH=${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH

# Ficheros PID
SERVER_PIDDIR=/tmp
export SERVER_PIDDIR

SERVER_PIDFILE=${SERVER_PIDDIR}/${SERVER_BIN}.pid
SERVER_STATUSFILE=${SERVER_PIDDIR}/${SERVER_BIN}.status

# Fichero de log
SERVER_LOGFILEPREFIX=`pwd`/../log/server
export SERVER_LOGFILEPREFIX

# Conexiones
SERVER_MAXPENDINGCONNECTIONS=10
export SERVER_MAXPENDINGCONNECTIONS
# Tiempo de espera entre conexiones (en ms).
SERVER_SLEEPTIME=1000
export SERVER_SLEEPTIME

#############################################################################

ERROR=0

ARGV="$@"
if [ "x$ARGV" = "x" ] ; then 
    ARGS="help"
fi

for ARG in $@ $ARGS
do
   # check for pidfile
   if [ -f ${SERVER_PIDFILE} ] ; then
      PID=`cat $SERVER_PIDFILE`
      if [ ! "x$PID" = "x" ] && kill -0 $PID 2>/dev/null; then
         STATUS="${SERVER_BIN} (pid $PID) esta activo"
         RUNNING=1
      else
         STATUS="${SERVER_BIN} (pid $PID?) no esta activo"
         RUNNING=0
      fi
   else
      STATUS="${SERVER_BIN} (no hay fichero pid) no esta activo"
      RUNNING=0
   fi

   case $ARG in
      start)
         if [ $RUNNING -eq 1 ]; then
            echo "$0 $ARG: ${SERVER_BIN} (pid $PID) ya esta activo"
            continue
         fi
         echo "Lanzando ${SERVER_EXE}"
         if ${SERVER_EXE} ${LISTENER_PORT} ; then
            echo "$0 $ARG: ${SERVER_BIN} arrancado"
         else
            echo "$0 $ARG: ${SERVER_BIN} no pudo ser arrancado"
            ERROR=3
         fi
      ;;
      stop)
         if [ $RUNNING -eq 0 ]; then
            echo "$0 $ARG: $STATUS"
            continue
         fi
         if kill $PID 2>/dev/null; then
            echo "$0 $ARG: ${SERVER_BIN}: se ha solicitado la parada."
         else
            echo "$0 $ARG: ${SERVER_BIN} no pudo ser parado"
            ERROR=4
         fi
      ;;
      status)
         rm -f ${SERVER_STATUSFILE}
         if kill -USR1 $PID 2>/dev/null; then
            sleep 3
            if [ -f  ${SERVER_STATUSFILE} ] ; then
               rm -f ${SERVER_STATUSFILE}
               echo "$0 $ARG: ${SERVER_BIN} esta activo"
            else
               echo "$0 $ARG: ${SERVER_BIN} esta parado"
               ERROR=1
            fi
         else
            echo "$0 $ARG: ${SERVER_BIN} esta parado"
            ERROR=1
         fi
      ;;
      *)
         echo "uso: $0 (start|stop|status|help)"
         cat <<EOF

    start     - arranca ${SERVER_BIN}
    stop      - para ${SERVER_BIN}
    status    - muestra el estado de ${SERVER_BIN}
    help      - esta pantalla 

EOF
         ERROR=2
      ;;
   esac
done
exit $ERROR
