extern "C" {
   #include <stdio.h>
   #include <string.h>
   #include <assert.h>
   #include <unistd.h>
   #include <fcntl.h>
}

#include "CLogger.h"
#include "CServerLogger.h"
#include "CTextLogger.h"

#include "CUtils.h"

/****************************************************************************\
 * Opciones de Debug y Traza
\****************************************************************************/
#ifdef TRACE_CLOGGER
#define TRACE(x) x
#else
#define TRACE(x)
#endif

const unsigned int CLogger::ID_SERVERLOGGER                    =    1;
const unsigned int CLogger::ID_TEXTLOGGER                      =    2;

const unsigned int CLogger::MANDATORY                          =    0;
const unsigned int CLogger::ERROR                              =    1;
const unsigned int CLogger::WARNING                            =    2;
const unsigned int CLogger::INFO                               =    3;
const unsigned int CLogger::FULL                               =    4;

const char CLogger::LITERAL_ERROR  [CLOG_MAX_SEVERITY_LEN + 1] = "ERROR";
const char CLogger::LITERAL_WARNING[CLOG_MAX_SEVERITY_LEN + 1] = "WARNING";
const char CLogger::LITERAL_INFO   [CLOG_MAX_SEVERITY_LEN + 1] = "INFO";
const char CLogger::LITERAL_FULL   [CLOG_MAX_SEVERITY_LEN + 1] = "FULL";

CLogger::CLogger() {
   TRACE( printf("CLogger::CLogger\n"); )
}

CLogger::~CLogger() {
   TRACE( printf("CLogger::~CLogger\n"); )
}

unsigned int CLogger::TranslateSeverityLevel(char *szseverity) {

   TRACE( printf("CLogger::TranslateSeverityLevel(char *)\n"); )

   if (strcmp (szseverity, CLogger::LITERAL_ERROR) == 0) {
      return CLogger::ERROR;
   } else {
      if (strcmp (szseverity, CLogger::LITERAL_WARNING) == 0) {
         return CLogger::WARNING;
      } else {
         if (strcmp (szseverity, CLogger::LITERAL_INFO) == 0) {
            return CLogger::INFO;
         } else {
            if (strcmp (szseverity, CLogger::LITERAL_FULL) == 0) {
               return CLogger::FULL;
            } else {
               return CLogger::ERROR;
            }
         }
      }
   }
}

char *CLogger::TranslateSeverityLevel(unsigned int nseverity) {

   TRACE( printf("CLogger::TranslateSeverityLevel(unsigned int)\n"); )

	switch (nseverity) {
      case CLogger::ERROR:   return (char *) CLogger::LITERAL_ERROR;
      case CLogger::WARNING: return (char *) CLogger::LITERAL_WARNING;
      case CLogger::INFO:    return (char *) CLogger::LITERAL_INFO;
      case CLogger::FULL:    return (char *) CLogger::LITERAL_FULL;
      default:               return (char *) CLogger::LITERAL_ERROR;
   }
}

bool CLogger::_writeline(char *pszmessage) {
   int nfdlog;

   TRACE( printf("CLogger::_writeline\n"); )   
   assert(m_szlogfile);
   assert(strlen(m_szlogfile));
   assert(pszmessage);
   if ( -1 == ( nfdlog = open( m_szlogfile, O_WRONLY | O_APPEND | O_CREAT |
                                             O_SYNC, 0640 ) ) ){
      perror ( "No se puede abrir el fichero de Log.\n " );
      perror ( m_szlogfile );
      return false;
   }
   // Bloquea el fichero y escribe el mensaje
   lockf( nfdlog, F_LOCK, 0 );
   if ( strlen ( pszmessage ) != 0 ) {
      write( nfdlog, pszmessage, strlen( pszmessage ) );
   }
   lockf( nfdlog, F_ULOCK, 0 );
   if ( close ( nfdlog ) == -1 ) {
      perror ( "No se puede cerrar el fichero de Log.\n " );
      perror ( m_szlogfile );
      return false;
   }
   return true;
}

char *CLogger::GetAppName() {
   return m_szappname;
}

unsigned int CLogger::GetSeverity() {
   return m_nseverity;
}

char *CLogger::GetLogFileName() {
   return m_szlogfile;
}  

CLogger *CLogger::GetInstance(unsigned int ntype
                            , char *szappname
                            , char *szseverity
                            , char *szlogfileprefix) {
   CLogger *pxlogger = NULL;
   TRACE( printf("CLogger::GetInstance\n"); )   
   
   switch (ntype) {
      case CLogger::ID_SERVERLOGGER: 
         pxlogger = new CServerLogger();
      break;
      case CLogger::ID_TEXTLOGGER:
         pxlogger = new CTextLogger();
      break;
      default: return pxlogger;
   }
   if (!(pxlogger->Initialize(szappname, szseverity, szlogfileprefix))) {
      SAFE_DELETE(pxlogger);
   }
   return pxlogger;
}
