extern "C" {
   #include <sys/types.h>
   #include <sys/stat.h>
   #include <sys/wait.h>
   #include <stdlib.h>
	#include <stdio.h>
	#include <string.h>
	#include <signal.h>
   #include <unistd.h>
   #include <fcntl.h>
}

extern "C" {
   static void signalHandler(int isignal);
   static void sighup_handler(int isignal);
   static void sigterm_handler(int isignal);
   static void sigusr1_handler(int isignal);
   // Manejador para hacer reap de las conexiones cliente en otros sistemas *nix
   // No usado en Solaris
   //static void sigchld_handler(int isignal);
}

#include "CAplication.h" 
#include "CUtils.h" 

#define MIN_PORT_NUMBER    28000
#define MAX_PORT_NUMBER    28010 
		
#define ENVVAR_DAEMON_PIDDIR                          "SERVER_PIDDIR"

sig_atomic_t gbkeeprunning                                = true;
sig_atomic_t gbrestart                                    = false;

CAplication * gpxapp                                      = NULL;

char gszstatusfile[128]                                   = "";

static void
reportstatus() {
	int fd;
   gpxapp->GetLogger()->WriteLine(CLogger::INFO, _FILE_, _LINE_
                                        , "Generando el fichero de estado: %s."
                                        , gszstatusfile);
   if ((fd = open(gszstatusfile,O_RDWR|O_CREAT,0640)) < 0) {
      gpxapp->GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                              , "No se puede generar el fichero de estado: %s."
                              , gszstatusfile);
   } else {
      close(fd);
   }
}

// Manejador para hacer reap de las conexiones cliente en otros sistemas *nix
// No usado en Solaris
void sigchld_handler(int isignal) {
    while(wait(NULL) > 0);
}

// Reinicio de la aplicacion
void sighup_handler(int isignal) {
   gpxapp->GetLogger()->WriteLine(CLogger::FULL, _FILE_, _LINE_
                          , "Recibida SIGHUP. Reinicio del broker.");
   gbrestart = true;
   signal(SIGHUP, sighup_handler); 
}

// Finalizacion de la aplicacion
void sigterm_handler(int isignal) {
   gpxapp->GetLogger()->WriteLine(CLogger::FULL, _FILE_, _LINE_
                         , "Recibida SIGTERM. Fin de ejecucion del broker.");
   gbkeeprunning = false;
   signal(SIGTERM, sigterm_handler); 
}
      
void sigusr1_handler(int isignal) {
   gpxapp->GetLogger()->WriteLine(CLogger::FULL, _FILE_, _LINE_
                         , "Recibida SIGURS1. Estado de ejecucion del broker.");
   reportstatus();
   signal(SIGUSR1, sigusr1_handler);  
}

static void 
signalHandler( int isignal ) {
   char szsignalname[10];
   char szmessage[32];
   switch (isignal) {
      case SIGSEGV:/* segmentation violation */
         strcpy(szsignalname, "SIGSEGV");
         break;
      case SIGBUS:/* bus error */
         strcpy(szsignalname, "SIGBUS");
         break;
      case SIGINT:/* interrupt (rubout) */
         strcpy(szsignalname, "SIGINT");
         break;
      case SIGQUIT:/* quit (ASCII FS) */
         strcpy(szsignalname, "SIGQUIT");
         break;
      case SIGTRAP:/* trace trap (not reset when caught) */
         strcpy(szsignalname, "SIGTRAP");
         break;
      case SIGABRT:/* used by abort, replace SIGIOT in the future */
         strcpy(szsignalname, "SIGABRT");
         break;
      case SIGSYS:/* bad argument to system call */
         strcpy(szsignalname, "SIGSYS");
         break;
      case SIGUSR2:/* user defined signal 2 */
         strcpy(szsignalname, "SIGUSR2");
         signal(SIGUSR2,    signalHandler); 
         break;
      case SIGXCPU:/* exceeded cpu limit */
         strcpy(szsignalname, "SIGXCPU");
         break;
      case SIGXFSZ:/* exceeded file size limit */
         strcpy(szsignalname, "SIGXFSZ");
         break;
      case SIGILL:/* illegal instruction (not reset when caught) */
         strcpy(szsignalname, "SIGILL");
         break;
      default: sprintf(szsignalname, "%d", isignal);
   }
   sprintf(szmessage, "Recibida senal: %s", szsignalname);
   gpxapp->GetLogger()->WriteLine(CLogger::FULL, _FILE_, _LINE_, szmessage);
}

void 
daemonize(char *szpidpath, char *szpidfile) {
   int i,lfp;
   char str[10];
   if(getppid()==1) {
      return; 
   }
   i=fork();
   if (i<0) {
      exit(1); 
   }
   if (i>0) {
      _exit(0); 
   }
   if (setsid() < 0) { 
      exit(-1);
   }      
   i=fork();
   if (i<0) {
      exit(1); 
   }
   if (i>0) {
      _exit(0); 
   }
   for (i=getdtablesize();i>=0;--i)  {
      close(i);
   }
   i=open("/dev/null",O_RDWR);
   dup(i);
   dup(i); 
   umask(027); 
   if (chdir(szpidpath)) { 
      exit(1);
   }
   lfp=open(szpidfile,O_RDWR|O_CREAT,0640);
   if (lfp<0) {
      exit(1); 
   }             
   if (lockf(lfp,F_TLOCK,0)<0) {
      exit(0); 
   }

   sprintf(str,"%d\n",getpid());
   write(lfp,str,strlen(str));

   signal(SIGHUP,    sighup_handler); 
   signal(SIGTERM,   sigterm_handler); 
   signal(SIGUSR1,   sigusr1_handler);
   
   signal(SIGCHLD,   SIG_IGN); 
   // En otros sistemas *nix es necesario hacer el reap de procesos
   // No usado en Solaris
   /*struct sigaction sa;
   sa.sa_handler = sigchld_handler; 
   sigemptyset(&sa.sa_mask);
   sa.sa_flags = SA_RESTART;
   if (sigaction(SIGCHLD, &sa, NULL) == -1) {
      exit(-2);
   }*/

   signal(SIGINT,    signalHandler);
   signal(SIGQUIT,   signalHandler);
   signal(SIGILL,    signalHandler);
   signal(SIGTRAP,   signalHandler);
   signal(SIGABRT,   signalHandler);
   signal(SIGFPE,    signalHandler);
   signal(SIGBUS,    signalHandler);
   signal(SIGSEGV,   signalHandler);
   signal(SIGSYS,    signalHandler); 
   signal(SIGPIPE,   SIG_DFL); 
   signal(SIGALRM,   SIG_DFL); 
   signal(SIGUSR2,   signalHandler);
   signal(SIGPWR,    SIG_DFL); 
   signal(SIGWINCH,  SIG_DFL); 
   signal(SIGTSTP,   SIG_IGN); 
   signal(SIGCONT,   SIG_DFL); 
   signal(SIGTTOU,   SIG_IGN);
   signal(SIGTTIN,   SIG_IGN);
   signal(SIGXCPU,   signalHandler); 
   signal(SIGXFSZ,   signalHandler);

	

}

int 
run(char *szappname, unsigned int uiport) {
   int ireturn;

   gpxapp = new CAplication(szappname); 
   ireturn = gpxapp->Initialize(uiport);
   if (ireturn == CAplication::ID_RETURN_INITOK) {
      ireturn = gpxapp->Run();
   }
   gpxapp->GetLogger()->WriteLine(CLogger::MANDATORY, _FILE_, _LINE_
                             , "Finalizada ejecucion del broker.");
   SAFE_DELETE(gpxapp);
   return ireturn;
}

int 
main(int argc, char *argv[]) {
   bool brunasdaemon = true;
   char szappname[128];
   char szcwd[128];
   char szpidpath[128];
   char szpidfile[128];
   char szport[128];
   unsigned int uiport;

   strcpy(szappname, CUtils::GetFileName(argv[0]));
   if (argc < 2) {
      printf("Numero incorrecto de parametros\n\tUso: %s  <puerto>\n" 
            , szappname);
      return -1;
   }
   // Parametro 1: Puerto de escucha
   strcpy(szport, argv[1]);
   if ((!CUtils::IsANumber(szport)) || (atoi(szport) < MIN_PORT_NUMBER) 
                                    || (atoi(szport) > MAX_PORT_NUMBER)) {
      printf("Puerto ivalido: %s (%d-%d)\n", szport, MIN_PORT_NUMBER
                                             , MAX_PORT_NUMBER);
      return -1;
   }
   uiport=atoi(szport);
   // Parametro 2: No documentado. Ejecucion como proceso normal.
   brunasdaemon=!((argc >= 3) && (strcmp(argv[2], "-d") == 0));

   sprintf(szpidfile, "%s.pid", szappname);
   sprintf(gszstatusfile, "%s.status", szappname);
   getcwd(szcwd, 128);
   CUtils::GetString(ENVVAR_DAEMON_PIDDIR, szpidpath);
   if (!szpidpath[0]) {
      printf("No se puede obtener el valor de la variable %s. "
             "No se puede arrancar el broker.\n" 
            , ENVVAR_DAEMON_PIDDIR);
      return -1;
   } else {
      if (chdir(szpidpath)) {
         printf("No se puede usar el directorio %s "
                "como directorio de trabajo.\n", szpidpath);
         perror("Error: ");
         return -1;
      } else {
         int fd;
         if ((fd = open(szpidfile, O_RDWR | O_CREAT, 0640)) < 0) { 
            printf("No se puede escribir en el directorio: %s.\n", szpidpath );
            perror("Error: ");
            return -1;
         } else {
            if (lockf(fd,F_TLOCK,0)<0) {
               printf("No se puede bloquear el fichero: %s\n", szpidfile);
               close(fd);
               return -1;
            } else {
               close(fd);
               remove(szpidfile);
               chdir(szcwd);
               // Parametro no documentado: -d
               if (brunasdaemon) {
                  daemonize(szpidpath, szpidfile);
               }
               return run(szappname, uiport);
            } 
         }
      }
   }
}

