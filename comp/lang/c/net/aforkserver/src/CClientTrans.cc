extern "C" {
   #include <stdio.h> 
   #include <string.h> 
}

#include "CClientTrans.h"

#include "CUtils.h"

/****************************************************************************\
 * Opciones de Debug y Traza
\****************************************************************************/
#ifdef TRACE_CCLIENTTRANS
#define TRACE(x) x
#else
#define TRACE(x)
#endif

// Constantes de retorno 
const int CClientTrans::ID_RETURN_INITOK                                =  0;
const int CClientTrans::ID_RETURN_VALIDATIONERROR                       = -1;

CClientTrans::CClientTrans(CLogger *pxlogger) {
   TRACE( printf("CClientTrans::CClientTrans\n"); )
   m_pxlogger = pxlogger;
}

CClientTrans::~CClientTrans () {
   TRACE( printf("CClientTrans::~CClientTrans\n"); )
}

CLogger *CClientTrans::GetLogger() {
   return m_pxlogger;
}

int CClientTrans::DoTransaction(char *connectstring
                               , char *szindata
                               , char *szoutdata) {

   TRACE( printf("CClientTrans::DoTransaction\n"); )

   GetLogger()->WriteLine(CLogger::MANDATORY, _FILE_, _LINE_
                  , "Transaccion solicitada. Trama: %s.", szindata); 
   strcpy(szoutdata, "HO HO!");
   GetLogger()->WriteLine(CLogger::MANDATORY, _FILE_, _LINE_
                  , "Transaccion procesada con exito. Trama: %s.", szoutdata); 
   return CClientTrans::ID_RETURN_INITOK;
}

