extern "C" {
   #include <sys/wait.h>
   #include <stdlib.h>
   #include <signal.h>
   #include <unistd.h>
}

#include "CAplication.h"

#include "CSocket.h"
#include "CClientTrans.h"

#include "CUtils.h"

extern sig_atomic_t gbkeeprunning;
extern sig_atomic_t gbrestart;

/****************************************************************************\
 * Opciones de Debug y Traza
\****************************************************************************/
#ifdef TRACE_CAPLICATION
#define TRACE(x) x
#else
#define TRACE(x)
#endif

// Constantes de valores por defecto
const unsigned int CAplication::DEFAULT_MAXPENDINGCONNECTIONS      =  10;
const unsigned int CAplication::DEFAULT_SLEEPTIME                  = 100;

// Constante de definicion de la version de la aplicacion
const unsigned int CAplication::VERSION                =  1; 
const unsigned int CAplication::SUBVERSION             =  0;
const char CAplication::DESCRIPTION[]                  =  "Generic Server"; 

char CAplication::m_szversionbuff[6]                                   = "";

// Constantes de retorno de inicializacion
const int CAplication::ID_RETURN_INITOK                                =  0;
const int CAplication::ID_RETURN_ERRORLOGINIT                          = -1;
const int CAplication::ID_RETURN_ERRORENVINIT                          = -2;
const int CAplication::ID_RETURN_ERRORBINDINGSOCKET                    = -3;
const int CAplication::ID_RETURN_ERRORCONFIGURINGLISTENER              = -4;

CAplication::CAplication(char *szappname) {
   TRACE( printf("CAplication::CAplication\n"); )
   strcpy(m_szappname, szappname);
   sprintf(m_szversionbuff, "%ld.%02ld", (long) VERSION, (long) SUBVERSION);
}

CAplication::~CAplication () {
   TRACE( printf("CAplication::~CAplication\n"); )
   SAFE_DELETE(m_pxlogger);
}

CLogger *CAplication::CreateLogger() {

   TRACE ( printf("CAplication::CreateLogger\n"); )

   char szlogfileprefix[CLOG_MAX_LOGFILE_NAME + 1]="";
   char szseverity[128];

   CUtils::GetString((char*)ENVVAR_LOGFILEPREFIX, szlogfileprefix);
   if (!strlen(szlogfileprefix)) {
      strcpy(szlogfileprefix, DEFAULT_LOGFILE_PREFIX);
   }
   CUtils::GetString((char*)ENVVAR_SEVERITYLEVEL, szseverity);
   if (!strlen(szseverity)) {
      strcpy(szseverity, CLogger::LITERAL_ERROR);
   }
   m_pxlogger = CLogger::GetInstance(CLogger::ID_SERVERLOGGER
                                    , GetName()
                                    , szseverity
                                    , szlogfileprefix);
   return m_pxlogger;
}

CLogger *CAplication::GetLogger() {
   return m_pxlogger;
}

char *CAplication::GetName() {
   return m_szappname;
}

char *CAplication::GetVersion() {
   return m_szversionbuff;
}

const char *CAplication::GetDescription() {
   return DESCRIPTION; 
}

unsigned int CAplication::GetPort() {
   return m_uiport;
}

unsigned int CAplication::GetMaxPendingConnections() {
   return m_imaxpendingconnections;
}

unsigned int CAplication::GetSleepTime() {
   return m_isleeptime;
}

int CAplication::Initialize(unsigned int uiport) {
   
   char szmaxpendingconnections[128] = "";
   char szsleeptime[128] = "";

   TRACE( printf("CAplication::Initialize\n"); )
        
   m_uiport = uiport;
   // Inicializa los miembros dato
   CreateLogger();
   if (!m_pxlogger) {
      return CAplication::ID_RETURN_ERRORLOGINIT;
   }

   // Muestra la version
   GetLogger()->WriteLine(CLogger::MANDATORY, _FILE_, _LINE_
                  , "%s. Version: %s", GetDescription(), GetVersion()); 
   
	// Valores de entorno para la conexion
   CUtils::GetString(ENVVAR_MAXPENDINGCONNECTIONS, szmaxpendingconnections);
   if (!strlen(szmaxpendingconnections)) {
      GetLogger()->WriteLine(CLogger::WARNING, _FILE_, _LINE_
                              , "No se puede obtener el numero maximo de "
                                "conexiones. Se toma el valor por defecto: %ld."
                              , CAplication::DEFAULT_MAXPENDINGCONNECTIONS);
      m_imaxpendingconnections = CAplication::DEFAULT_MAXPENDINGCONNECTIONS;
   } else {
      if ((!CUtils::IsANumber(szmaxpendingconnections)) ||
          ((m_imaxpendingconnections = atoi(szmaxpendingconnections)) <= 0)) {
         GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                        , "Se ha especificado un valor erroneo para el numero "
                          "maximo de conexiones: %s. Se toma el valor por "
                          "defecto: %ld."
                        , szmaxpendingconnections
                        , CAplication::DEFAULT_MAXPENDINGCONNECTIONS);
         m_imaxpendingconnections = CAplication::DEFAULT_MAXPENDINGCONNECTIONS;
      } else {
         GetLogger()->WriteLine(CLogger::MANDATORY, _FILE_, _LINE_
                                            , "%s=%u"
                                            , ENVVAR_MAXPENDINGCONNECTIONS
                                            , GetMaxPendingConnections());
      }
   }

   CUtils::GetString(ENVVAR_SLEEPTIME, szsleeptime);
   if (!strlen(szsleeptime)) {
      GetLogger()->WriteLine(CLogger::WARNING, _FILE_, _LINE_
                               , "No se puede obtener el tiempo de inactividad "
                                 "entre conexiones. Se toma el valor por "
                                 "defecto: %ld."
                               , CAplication::DEFAULT_SLEEPTIME);
      m_isleeptime = CAplication::DEFAULT_SLEEPTIME * 1000;
   } else {
      if ((!CUtils::IsANumber(szsleeptime)) ||
          ((m_isleeptime = atoi(szsleeptime)) <= 0)) {
         GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                          , "Se ha especificado un valor erroneo para el "
                            "tiempo de espera entre conexiones: %s. Se toma "
                            "el valor por defecto: %ld."
                          , szsleeptime
                          , CAplication::DEFAULT_SLEEPTIME);
         m_isleeptime = CAplication::DEFAULT_SLEEPTIME * 1000;
      } else {
         GetLogger()->WriteLine(CLogger::MANDATORY, _FILE_, _LINE_
                               , "%s=%u"
                               , ENVVAR_SLEEPTIME
                               , GetSleepTime());
         m_isleeptime *=  1000;
      }
   }
   return CAplication::ID_RETURN_INITOK;
}

bool CAplication::Run() {
   int ireturn = CAplication::ID_RETURN_INITOK;
 
   TRACE( printf("CAplication::Run\n"); )

   CSocket xserversock;

	while (gbkeeprunning && (ireturn == CAplication::ID_RETURN_INITOK)) {
   
      ireturn = xserversock.Initialize(GetPort()
                                      , GetMaxPendingConnections()
                                      , GetLogger());
      if (ireturn != CAplication::ID_RETURN_INITOK) {
         GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                                      , "No se ha podido iniciar el listener.");
      } else {
         bool bnewclient = false;
         GetLogger()->WriteLine(CLogger::MANDATORY, _FILE_, _LINE_
                                       , "Escuchando en el puerto: %u."
                                       , GetPort());
         while (gbkeeprunning && !gbrestart) {
            CSocket *pxclientsock = xserversock.NewClient(&bnewclient);
            if (!bnewclient) {
               // Si no hay conexion de cliente se realiza espera
               usleep(GetSleepTime());
            } else {
               GetLogger()->WriteLine(CLogger::MANDATORY, _FILE_, _LINE_
                                             , "Recibida peticion desde: %s"
                                             , pxclientsock->GetAddress());
               int i = fork();
               if (!i) { 
                  // Proceso hijo de tratamiento de la peticion
                  char szinbuf[CSocket::MAX_INBUFFER_SIZE + 1];
                  xserversock.Close(); // Se cierra el listener para el hijo 
                  if (!pxclientsock->Receive(szinbuf)) {
                     GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                                   , "Error al recibir la trama desde el "
                                     "cliente: %s"
                                   , pxclientsock->GetAddress());
                  } else {
                     int itransresult;
                     char szoutbuf[CSocket::MAX_OUTBUFFER_SIZE];
                     GetLogger()->WriteLine(CLogger::INFO, _FILE_, _LINE_
                                       , "Recibidida trama: %s."
                                       , szinbuf);
                     CClientTrans xtrans(GetLogger());
                     itransresult = xtrans.DoTransaction("dataSource"
                                                        , szinbuf
                                                        , szoutbuf);
                     if (!pxclientsock->Send(szoutbuf)) {
                        GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                                  , "Error al enviar la respuesta al cliente.");
                     } else {
                        GetLogger()->WriteLine(CLogger::MANDATORY, _FILE_
                                           , _LINE_
                                           , "Envio correcto de: %s."
                                           , szoutbuf);
                     }
                  }
			         SAFE_DELETE(pxclientsock);
                  exit(0);
               } else {
                  if (i == (pid_t)-1) {
                     // No se pudo crear el proceso hijo
                     GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                                        , "No se ha podido atender la peticion "
                                          "debido a un error del sistema.");
                  } 
               } 
            }
            SAFE_DELETE(pxclientsock); // El padre cierra el socket cliente
         } // Fin del bucle principal
         GetLogger()->WriteLine(CLogger::INFO, _FILE_, _LINE_
									  , "Esperando la finalizacion de las conexiones "
                               "cliente");
         waitpid(-1, 0 ,0);
         GetLogger()->WriteLine(CLogger::MANDATORY, _FILE_, _LINE_
                                           , "Cerrando el listener.");
         xserversock.Close(); // Se cierra el listener
         if (gbrestart) {
            GetLogger()->WriteLine(CLogger::MANDATORY, _FILE_, _LINE_
                                              , "Reiniciando Broker."); 
            GetLogger()->WriteLine(CLogger::MANDATORY, _FILE_, _LINE_
                     , "%s. Version: %s", GetDescription(), GetVersion()); 
            gbrestart=false;
         }
      }
   }
   return ireturn;
}

