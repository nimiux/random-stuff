extern "C" {
   #include <ctype.h>
   #include <stdlib.h>
   #include <stdio.h>
   #include <string.h>
   #include <time.h>
   #include <assert.h>
   #include <sys/timeb.h>
}

#include <CUtils.h>
#include <CLogger.h>

/****************************************************************************\
 * Opciones de Debug y Traza
\****************************************************************************/
#ifdef TRACE_CUTILS
#define TRACE(x) x
#else
#define TRACE(x)
#endif

char CUtils::m_szstringbuffer   [STRING_BUFFER_LEN + 1] = "";

char *CUtils::_getdatetime(const char *pszformat){
   static char szdatetimebuffer [DATE_TIME_BUFFER_LEN + 1] = "";
   time_t xSystemDate;
   const struct tm *xNow;
   assert(pszformat);

   TRACE( printf("CUtils::_getdatetime %s\n",pszformat); )
   xSystemDate = time(NULL);
   xNow = localtime( &xSystemDate );
   strftime(szdatetimebuffer, DATE_TIME_BUFFER_LEN, pszformat, xNow);
   return szdatetimebuffer;
}

char *CUtils::GetDateTimeMs() {
   static char szdatetimebuffer [DATE_TIME_BUFFER_LEN + 1] = "";
   struct timeb tb;
   const struct tm *xNow;
   char szmilisecs[5] = "";
   if (!ftime(&tb)) {
      sprintf(szmilisecs, ":%03d", tb.millitm);
   } else {
      sprintf(szmilisecs, ":ERR");
   }
   xNow = localtime( &(tb.time) );
   strftime(szdatetimebuffer, DATE_TIME_BUFFER_LEN, "%d/%m/%Y-%T", xNow);
   strcat(szdatetimebuffer, szmilisecs);
   return szdatetimebuffer;
}

void CUtils::GetString( const char *pszvarname, char *pszvarvalue) {
                                   
   char *pszenvreader;
   assert ( pszvarname );
   TRACE ( printf("CUtils::GetString: %s\n", pszvarname); )

   assert ( pszvarvalue );
   pszenvreader = (char *) getenv( pszvarname );
   if ( pszenvreader == NULL ) {
      pszvarvalue[0]=0;
      TRACE ( printf("No se puede leer la variable: %s\n", pszvarname); )
   } else {
      strcpy(pszvarvalue, pszenvreader);
      TRACE ( printf("Leido: %s\n", pszvarvalue); )
   }
}

char* CUtils::SubString(char *pszstring, unsigned int init, unsigned int len) {

   assert(pszstring);
        
   m_szstringbuffer[0]=0;
   if ((init+len) > strlen(pszstring))  {
      strcpy(m_szstringbuffer, pszstring);
   } else {
      strncpy (m_szstringbuffer, &pszstring[init], len);
      m_szstringbuffer[len]=0;
   }
   return m_szstringbuffer;
}   

char* CUtils::ToMays(char *pszstring) {
   unsigned int i;

   assert(pszstring);
   m_szstringbuffer[0]=0;
   for (i=0; i<strlen(pszstring); i++) {
      m_szstringbuffer[i]=toupper(pszstring[i]);
   } 
   m_szstringbuffer[i]=0;
   return m_szstringbuffer;
}

void CUtils::CopyString(char *pszdst, char *pszsrc, int init, int len) {
   assert(pszdst);
   assert(pszsrc);
   
   strncpy(&pszdst[init], pszsrc, len);
}

void CUtils::CopyInteger(char *pszdst, int nsrc, int init, int len) {
   char szbuffer[1024];
   char szformat[1024];

   assert(pszdst);
   sprintf(szformat,"%%0%dld", len);
   sprintf(szbuffer,szformat,nsrc); 
   strncpy(&pszdst[init], szbuffer, len);
}

bool CUtils::HaveNullValue(char *pszstring) {
   int i;
   assert(pszstring);
   for (i=0;pszstring[i]==' ';i++);
   return (!pszstring[i]);
}

char *CUtils::GetFileName(char *pszpath) {
   char *p;

   assert(pszpath);
   p = (char *) strrchr(pszpath, (int) '/');
   p =(p?p+1:pszpath);
   strcpy(m_szstringbuffer, p);
   return m_szstringbuffer;
}

char *CUtils::GetPathName(char *pszpath) {
   char *p;
   int npos;

   assert(pszpath);
   strcpy(m_szstringbuffer, pszpath);
   p = (char *) strrchr(pszpath, (int) '/');
   npos =(p?(p) - (&pszpath[0]):0);
   m_szstringbuffer[npos]= 0;
   return m_szstringbuffer;
}

bool CUtils::SendMail (CLogger *pxlogger
                      , char *szemaillist
                      , char *szsubject
                      , char *szmessage) {
   char szcommand [1024];
   FILE *pxpipe;
   
        assert(szemaillist);
   assert(szsubject);
   assert(szmessage);

   pxlogger->WriteLine(CLogger::INFO, _FILE_, _LINE_
                       , "Se procede a enviar el correo con asunto: %s a: %s."
                       , szsubject
                       , szemaillist);
   sprintf( szcommand, "mailx -s \"%s\" \"%s\"", szsubject, szemaillist);
   if (!(pxpipe = popen(szcommand, "w"))) {
      pxlogger->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                        , "No se puede crear el pipe para enviar el correo.");
      return false;
   } else {
      fprintf (pxpipe, "%s", szmessage);
      if (pclose(pxpipe)) {
         pxlogger->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                         , "No se ha podido enviar el correo a: %s."
                         , szemaillist);
         return false;
      }
   }
   pxlogger->WriteLine(CLogger::INFO, _FILE_, _LINE_
                       , "Correo enviado.");
   return true;
}

bool CUtils::IsANumber(char *pszstring) {
   assert(pszstring);
   if (!pszstring[0]) {
      return false;
   } else {
      int i;
      for (i=0; (pszstring[i] && isdigit(pszstring[i])); i++);
      return (!pszstring[i]);
   }
}

bool CUtils::FileExists(char *pszfile) {
  FILE *pfile;
  
  pfile = fopen(pszfile, "rw");
  if (pfile) {
     if (!fclose(pfile)) {
        return true;
     } else {
        return false;
     }
  } else {
     return false;
  }
}
