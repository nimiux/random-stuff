extern "C" {
	#include <sys/socket.h>
	#include <sys/wait.h>
   #include <arpa/inet.h>
   #include <unistd.h>
   #include <assert.h>
   #include <stdio.h>
   #include <errno.h>
   #include <fcntl.h>
}

#include <CSocket.h>

#include "CUtils.h"

/****************************************************************************\
 * Opciones de Debug y Traza
\****************************************************************************/
#ifdef TRACE_CSOCKET
#define TRACE(x) x
#else
#define TRACE(x)
#endif

// Buffers
const unsigned int CSocket::MAX_INBUFFER_SIZE                      = 4096;
const unsigned int CSocket::MAX_OUTBUFFER_SIZE                     = 4096;

// Constantes de retorno
const int CSocket::ID_RETURN_NOPENDINGCONNS                        =  1;
const int CSocket::ID_RETURN_INITOK                                =  0;
const int CSocket::ID_RETURN_ERRORBINDINGSOCKET                    = -1;
const int CSocket::ID_RETURN_ERRORACCEPT                           = -2;

CSocket::CSocket() {
   TRACE( printf("CSocket::CSocket\n"); )
}

CSocket::~CSocket () {
   TRACE( printf("CSocket::~CSocket\n"); )
   Close();
}

char *CSocket::GetLastError() {
   static char szerrordesc[1024];
   strcpy(szerrordesc, strerror(errno));
   return szerrordesc;
}

CLogger *CSocket::GetLogger() {
   return m_pxlogger;
}

char *CSocket::GetAddress() {
   return inet_ntoa(m_xsockaddr.sin_addr);
}

int CSocket::Initialize(unsigned int uiport
                       , unsigned int uimaxpendingconns
                       , CLogger *pxlogger) {

   TRACE( printf("CSocket::Initialize\n"); )
   int ireturn = CSocket::ID_RETURN_INITOK;
   int piyes = 1;

   assert(pxlogger);

   m_pxlogger = pxlogger;

   if ((m_isockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
      ireturn = CSocket::ID_RETURN_ERRORBINDINGSOCKET;
      GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                                        , "Error al crear el listener: %s"
                                        , GetLastError()); 
   } else {
      if (setsockopt(m_isockfd, SOL_SOCKET, SO_REUSEADDR, &piyes
                              , sizeof(int)) == -1) {
         ireturn = CSocket::ID_RETURN_ERRORBINDINGSOCKET;
         GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                                 , "Error al cambiar opciones del listener: %s"
                                 , GetLastError()); 
      } else {
         int sflags = fcntl(m_isockfd, F_GETFL) | O_NONBLOCK;
         if (fcntl(m_isockfd, F_SETFL, sflags) == -1) {
            ireturn = CSocket::ID_RETURN_ERRORBINDINGSOCKET;
            GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                               , "Error al configurar non-blocking listener.");
         } else { 
            m_xsockaddr.sin_family      = AF_INET; 
            m_xsockaddr.sin_port        = htons(uiport); 
            m_xsockaddr.sin_addr.s_addr = INADDR_ANY; 
            memset(&(m_xsockaddr.sin_zero), '\0', 8); 
            if (bind(m_isockfd, (struct sockaddr *)&m_xsockaddr
                              , sizeof(struct sockaddr)) == -1) {
               ireturn = CSocket::ID_RETURN_ERRORBINDINGSOCKET;
               GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                                    , "Error al hacer bind del listener: %s"
                                    , GetLastError()); 
            } else {
               if (listen(m_isockfd, uimaxpendingconns) == -1) {
                  ireturn = CSocket::ID_RETURN_ERRORBINDINGSOCKET;
                  GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                                      , "Error al configurar el listener: %s"
                                      , GetLastError()); 
               }
            }
         } 
      } 
   }  
   return ireturn;
}

int CSocket::Initialize(int iinsockfd, CLogger *pxlogger)  {

   TRACE( printf("CSocket::Initialize/Accept\n"); )
   int sin_size = sizeof(struct sockaddr_in);
   int ireturn = CSocket::ID_RETURN_INITOK;
   bool bend = false;

   assert(pxlogger);

   m_pxlogger = pxlogger;

	do {
      bend = ((m_isockfd = accept(iinsockfd, (struct sockaddr *)&m_xsockaddr
                                           , (socklen_t *)&sin_size)) != -1);
      if ((!bend) && (errno != EINTR)) {           
         if (errno == EWOULDBLOCK) {
            ireturn = CSocket::ID_RETURN_NOPENDINGCONNS;
         } else {
            GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                                   , "Error en inicializacion del cliente: %s"
                                   , GetLastError());
            ireturn = CSocket::ID_RETURN_ERRORACCEPT;
         }
         bend = true;
      } 
   } while (!bend);
   return ireturn;
}

bool CSocket::Send(char *szdata) {
   bool bend, bresult = true;
	TRACE( printf("CSocket::Send\n"); )

   strcpy(szdata, CUtils::SubString(szdata, 0 , MAX_OUTBUFFER_SIZE));
   int ilen = strlen(szdata);
   do {
      bend = (send(m_isockfd, szdata, ilen, 0) != -1); 
      if ((!bend) && (errno != EINTR)) {
         if (errno != EWOULDBLOCK) {
            GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                                      , "Error al hacer send al cliente: %s"
                                      , GetLastError());
            bend = true;
            bresult = false;
         }
      }
   } while (!bend);
   return bresult;
}

bool CSocket::Receive(char *szdata) {
   int ibytes;
   bool bend, bresult = true;
	TRACE( printf("CSocket::Receive\n"); )

   do {
      bend = ((ibytes = recv(m_isockfd, szdata
                                      , CSocket::MAX_INBUFFER_SIZE, 0)) != -1);
      if ((!bend) && (errno != EINTR)) {
         if (errno != EWOULDBLOCK) {
            GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                                      , "Error al recibir desde el cliente: %s"
                                      , GetLastError());
            bend = true;
            bresult = false;
         }
      } else {
         szdata[ibytes] = '\0';
      }
   } while (!bend);
   return bresult;
}

void CSocket::Close() {
   
	TRACE( printf("CSocket::Close\n"); )
   close(m_isockfd);
}

CSocket *CSocket::NewClient(bool *pbnewclient) {
   CSocket *pxsocket = NULL;
   
	TRACE( printf("CSocket::NewClient\n"); )

	*pbnewclient = false;
	pxsocket = new CSocket();
   
   if (pxsocket) {
      int iresult =pxsocket->Initialize(m_isockfd, m_pxlogger);
      if (iresult == CSocket::ID_RETURN_INITOK) {
         *pbnewclient = true;
      } else { 
         if (iresult != CSocket::ID_RETURN_NOPENDINGCONNS) {
            GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                                , "No se ha podido inicializar la conexion del "
                                  "cliente.");
            SAFE_DELETE(pxsocket);
         }
      } 
   } else {
      GetLogger()->WriteLine(CLogger::ERROR, _FILE_, _LINE_
                                   , "No se ha podido crear la conexion del "
                                     "cliente.");
   }
   return pxsocket;
}
