extern "C" {
   #include <stdio.h>
   #include <string.h>
   #include <assert.h>
   #include <unistd.h>
   #include <stdarg.h>
}

#include "CTextLogger.h"

#include "CUtils.h"

/****************************************************************************\
 * Opciones de Debug y Traza
\****************************************************************************/
#ifdef TRACE_CTEXTLOGGER
#define TRACE(x) x
#else
#define TRACE(x)
#endif

CTextLogger::CTextLogger() {
   TRACE( printf("CTextLogger::CTextLogger\n"); )
}

CTextLogger::~CTextLogger() {
   TRACE( printf("CTextLogger::~CTextLogger\n"); )
}

bool CTextLogger::Initialize(char *szappname
                           , char *szseverity
                           , char *szlogfile) {
   TRACE( printf("CTextLogger::Initialize\n"); )
   
   assert(szappname);
   assert(szseverity);
   assert(szlogfile);
   assert(strlen(szappname));
   assert(strlen(szseverity));
   assert(strlen(szlogfile));

   strcpy(m_szappname, szappname);
   m_nseverity = TranslateSeverityLevel(szseverity);
   strcpy(m_szlogfile, szlogfile);
   return (_writeline((char *)""));
}
 
void CTextLogger::WriteLine(unsigned int nlevel, char *pszfile, int nline
                    , const char *pszformat
                     , ...) {
   if (nlevel <= m_nseverity) {
      va_list xArgs;
      static char pszmessage      [CLOG_MAX_LOGFILE_MESSAGE_LEN];
      static char pszfinalmessage [CLOG_MAX_LOGFILE_MESSAGE_LEN * 2];
      static char pszfinalformat [CLOG_MAX_LOGFILE_MESSAGE_LEN * 2];
      TRACE( printf("CTextLogger::WriteLine\n"); )

      assert(pszfile);
      assert(pszformat);

      // Se a�ade la palabra error en caso de que el nivel del mensaje sea 
      // ERROR
      if (nlevel == ERROR ) {
         sprintf(pszfinalformat, "%s: %s", LITERAL_ERROR, pszformat);
      } else {
         strcpy(pszfinalformat, pszformat);
      }
      // Formatea el mensaje antes de ser impreso
      va_start( xArgs , pszformat );
      vsprintf( pszmessage, pszfinalformat, xArgs );
      va_end ( xArgs );
      sprintf( pszfinalmessage, "%s %s[%d] (%s:%d) %s\n"
                         , GETDATETIME, m_szappname, getpid()
                         , pszfile, nline, pszmessage );
      _writeline(pszfinalmessage);
   }
}
   
