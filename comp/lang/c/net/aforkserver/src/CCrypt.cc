#include "CCrypt.h"

const char CCrypt::SK128_DEFAULT_KEY[]="8ke/r%&y�A#@|<*�$";

char CCrypt::m_szcryptbuffer[1024]=""; 

unsigned char *CCrypt::ConvertirAStringHex(unsigned char * szstring) {
   char szhexchar[3];
   int nlen;

   assert (szstring);
   m_szcryptbuffer[0]=0;
   nlen = strlen((char *)szstring);
   for (int i = 0; i < nlen; i++) {
      sprintf(szhexchar, "0x%02x", szstring[i]);
      strcat(m_szcryptbuffer, szhexchar);
   }
   return (unsigned char *)m_szcryptbuffer;
}

unsigned char *CCrypt::ConvertirDesdeStringHex(unsigned char * szstring) {
   char  szhexstring[5];
   unsigned char * szauxstring = szstring;
   int nlen;
   assert (szauxstring);
   m_szcryptbuffer[0]=0;
   nlen = strlen((char *)szauxstring);
   if (!(nlen % 4)) {
      int i;
      for (i = 0; i < nlen; i +=4, szauxstring +=4 ) {
         strncpy(szhexstring, (char *)szauxstring, 4);
         szhexstring[4] = 0;
         m_szcryptbuffer[i/4] = (unsigned char)strtoul(szhexstring, NULL, 16);
      }
      m_szcryptbuffer[i/4] = 0;
   }
   return (unsigned char *)m_szcryptbuffer;
}

bool CCrypt::sk128encripta(const char *szstring, char *szresult) {
   symmetric_key skey;
   int errno, keysize;

   assert(szresult);
   assert(szstring);
   keysize = strlen(CCrypt::SK128_DEFAULT_KEY);
   if ((errno = safer_sk128_desc.keysize(&keysize)) != CRYPT_OK) {
      return false;
   } else {
      if ((errno = safer_sk128_desc.setup(
                             (const unsigned char *)CCrypt::SK128_DEFAULT_KEY
                            , keysize, 0, &skey)) != CRYPT_OK) {
         return false;
      } else {
         char sztempbuffer[1024];
         memset(sztempbuffer, 0, 1024);
         safer_sk128_desc.ecb_encrypt( (unsigned char *)szstring
                                     , (unsigned char *) sztempbuffer
                                     , &skey);
         strcpy(szresult, sztempbuffer);
         strcpy(szresult, 
               (char *)ConvertirAStringHex((unsigned char *)szresult));
         return true;
      }
   }
}

bool CCrypt::sk128desencripta(const char *szstring, char *szresult) {
   symmetric_key skey;
   int errno, keysize;

   assert(szresult);
   assert(szstring);
   keysize = strlen(CCrypt::SK128_DEFAULT_KEY);
   if ((errno = safer_sk128_desc.keysize(&keysize)) != CRYPT_OK) {
      return false;
   } else {
      if ((errno = safer_sk128_desc.setup(
                             (const unsigned char *)CCrypt::SK128_DEFAULT_KEY
                            , keysize, 0, &skey)) != CRYPT_OK) {
         return false;
      } else {
         char sztempbuffer[1024];
         memset(sztempbuffer, 0, 1024);
         strcpy(sztempbuffer
                , (char *)ConvertirDesdeStringHex((unsigned char *)szstring));
         szresult[0]=0;
         if (sztempbuffer[0]) {
            safer_sk128_desc.ecb_decrypt((unsigned char *) sztempbuffer
			                               , (unsigned char *) szresult
                                        , &skey);
         }
      }
   }
   return true;
}

