extern "C" {
   #include <stdio.h>
}

#include "CServerLogger.h"

#include "CUtils.h"

/****************************************************************************\
 * Opciones de Debug y Traza
\****************************************************************************/
#ifdef TRACE_CDICLOGGER
#define TRACE(x) x
#else
#define TRACE(x)
#endif

CServerLogger::CServerLogger() {
   TRACE( printf("CServerLogger::CServerLogger\n"); )
}

CServerLogger::~CServerLogger() {
   TRACE( printf("CServerLogger::~CServerLogger\n"); )
}

char *CServerLogger::BuildLogFileName() {
   static char szlogfile[CLOG_MAX_LOGFILE_NAME + 1] = "";
   TRACE( printf("CServerLogger::BuildLogFileName\n"); )
   sprintf(szlogfile, "%s_%s.log", m_szlogfileprefix
                                           , GETDATEAAAAMMDD);
   return szlogfile;
}

