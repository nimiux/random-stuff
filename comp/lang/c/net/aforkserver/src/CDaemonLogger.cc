extern "C" {
   #include <stdio.h>
   #include <stdlib.h>
   #include <string.h>
   #include <assert.h>
   #include <unistd.h>
   #include <stdarg.h>
}

#include "CDaemonLogger.h"

#include "CUtils.h"

/****************************************************************************\
 * Opciones de Debug y Traza
\****************************************************************************/
#ifdef TRACE_CDAEMONLOGGER
#define TRACE(x) x
#else
#define TRACE(x)
#endif

CDaemonLogger::CDaemonLogger() {
   TRACE( printf("CDaemonLogger::CDaemonLogger\n"); )
}

CDaemonLogger::~CDaemonLogger() {
   TRACE( printf("CDaemonLogger::~CDaemonLogger\n"); )
}

bool CDaemonLogger::Initialize(char *szappname
                              , char *szseverity
                              , char *szlogfileprefix) {
   TRACE( printf("CDaemonLogger::Initialize\n"); )
   
   assert(szappname);
   assert(szseverity);
   assert(szlogfileprefix);

   assert(strlen(szappname));
   assert(strlen(szseverity));
   assert(strlen(szlogfileprefix));

   strcpy(m_szappname, szappname);
   m_nseverity = TranslateSeverityLevel(szseverity);
   strcpy(m_szlogfileprefix, szlogfileprefix);
   strcpy(m_szlogfile, BuildLogFileName());
   m_nlogdate=atol(GETDATEAAAAMMDD);
   return (_writeline((char *)""));
}

bool CDaemonLogger::UpdateLogFileName() {
   bool bresult     = false;
   TRACE( printf("CDaemonLogger::UpdateLogFileName\n"); )

   if (m_nlogdate != (unsigned) atol(GETDATEAAAAMMDD)) {
      static char szoldlogfile[CLOG_MAX_LOGFILE_NAME + 1] = "";
      static char sznewlogfile[CLOG_MAX_LOGFILE_NAME + 1] = "";
      char szmessage[128];
 
      strcpy(szoldlogfile, m_szlogfile);
      strcpy(sznewlogfile, BuildLogFileName());
      sprintf(szmessage, "El log continua en el fichero: %s.", sznewlogfile);
      _writeline(szmessage);
      strcpy(m_szlogfile, sznewlogfile);
      m_nlogdate = atoi(GETDATEAAAAMMDD);
      if (_writeline((char *)"")) {
         sprintf(szmessage, "Este log es la continuacion del fichero: %s.\n"
                          , szoldlogfile);
         _writeline(szmessage);
         bresult = true;
      } 
   } else {
      bresult = true;
   }
   return bresult;
}

void CDaemonLogger::WriteLine(unsigned int nlevel, char *pszfile, int nline
                    , const char *pszformat
                    , ...) {
   if (nlevel <= m_nseverity) {
      va_list xArgs;
      static char pszmessage      [CLOG_MAX_LOGFILE_MESSAGE_LEN];
      static char pszfinalmessage [CLOG_MAX_LOGFILE_MESSAGE_LEN * 2];
      static char pszfinalformat [CLOG_MAX_LOGFILE_MESSAGE_LEN * 2];
      TRACE( printf("CDaemonLogger::WriteLine\n"); )

      assert(pszfile);
      assert(pszformat);

      pszmessage[0]      = 0;
      pszfinalmessage[0] = 0;
      pszfinalformat[0]  = 0;
      // Se actualiza el nombre del fichero de log por si se ha cambiado de
      // fecha
      UpdateLogFileName();
      // Se anade la palabra error en caso de que el nivel del mensaje sea 
      // ERROR
      if (nlevel == ERROR ) {
         sprintf(pszfinalformat, "%s: %s", LITERAL_ERROR, pszformat);
      } else {
         strcpy(pszfinalformat, pszformat);
      }
      // Formatea el mensaje antes de ser impreso
      va_start( xArgs , pszformat );
      vsprintf( pszmessage, pszfinalformat, xArgs );
      va_end ( xArgs );
      sprintf( pszfinalmessage, "%s %s[%d] (%s:%d) %s\n"
                         , CUtils::GetDateTimeMs(), m_szappname, getpid()
                         , pszfile, nline, pszmessage );
      _writeline(pszfinalmessage);
   }
}
   
