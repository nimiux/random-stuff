CREATE OR REPLACE FUNCTION getfoo(n integer) RETURNS anyarray AS $$
DECLARE
   trow foo%ROWTYPE;
   a char(80);
BEGIN
   SELECT * INTO a FROM foo;
   /*RETURN trow.name;*/
   RETURN a;
END
$$ LANGUAGE plpgsql;
