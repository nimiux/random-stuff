Language: Smalltalk


Author: Christopher Oliver 
Note: Smalltalk-80 (or Squeak Smalltalk 2.2)
To be run from a workspace while the transcript is open.

| prog quote |
quote := Character value: 39.
prog := '| prog quote |
quote := Character value: 39.
prog := .
Transcript show: (prog copyFrom: 1 to: 53).
Transcript show: quote asString; show: prog; show: quote asString.
Transcript show: (prog copyFrom: 54 to: 212).
'.
Transcript show: (prog copyFrom: 1 to: 53).
Transcript show: quote asString; show: prog; show: quote asString.
Transcript show: (prog copyFrom: 54 to: 212).