ApplicationModel subclass: #MyInput
instanceVariableNames: 'person '
MyInput class>>open: aModel
^self openOn: (self new initialize: aModel)
ApplicationModel subclass: #MyView
instanceVariableNames: 'person '
MyView class>>open: aModel
^self openOn: (self new initialize: aModel)
Model subclass: #MyPerson
instanceVariableNames: 'name age'

MyInput subclass: #MyInput1
instanceVariableNames: 'name age '
MyInput1>>initialize: aPerson
person := aPerson.
name := String new asValue.
name onChangeSend: #nameChanged to: self.
age := 0 asValue.
age onChangeSend: #ageChanged to: self
MyInput1>>age
^age
MyInput1>>name
^name
MyInput1>>ageChanged
person age: self age value
MyInput1>>nameChanged
person name: self name value
MyView subclass: #MyView1
instanceVariableNames: 'name age '
MyView1>>initialize: aPerson
person := aPerson.
person addDependent: self.
name := String new asValue.
age := 0 asValue.
MyView1>>age
^age
MyView1>>name
^name
MyView1>>update: aSymbol with: aValue from: anObject
aSymbol == #name ifTrue: [self name value: aValue].
aSymbol == #age ifTrue: [self age value: aValue]
MyPerson subclass: #MyPerson1
instanceVariableNames: ' '
MyPerson1>>age: aValue
age := aValue.
self changed: #age with: age
MyPerson1>>name: aValue
name := aValue.
self changed: #name with: name
