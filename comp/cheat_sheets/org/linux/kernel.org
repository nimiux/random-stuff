Kernel Tips & Hacks

Tools recommended

- util-linux
- modutils / module-init-tools
- Filesystem utils (FS, quota,..)
- udev
- procps
- pcmcia

Recommendations

- Make compilation with a user != than root
- Separate object from source (make O=~/linux)
- Crosscompiling (make ARCH=arm CROSS_COMPILE=/usr/local/bin/arm-linux- )
- Almost all distros have an installkernel script (normally in mkinitrd)

Config

make kernelversion
make kernelrelease

make clean
make mrproper
make distclean

KCONFIG_ALLCONFIG=File with config opts
make O=~/linux
make defconfig
make oldconfig
make silentoldconfig
make config
make menuconfig
make gconfig
make xconfig
make checkconfig
make allmodconfig
make allnoconfig
make allyesconfig

make checkhelp

make checkstat
make namespacecheck

make dep
make depend

make boot
make bzImage 
make zImage 
make bzdisk
make zdisk
make bzlilo
make zlilo
make vmlinux /* Builds only static part of the kernel */ make modules
INSTALL_MODULE_PATH
make modules
make modules_install

make -j
make -jN (where N is twice # of processors)

make O=~/objdir

make SUBDIRS=drivers
make drivers
make /drivers/usb/serial
make /drivers/usb/serial/visor.ko
make M=/drivers/usb/serial

make CC="ccache gcc"
make CC="ccache distcc"

Manual Install:
KERNEL_VERSION=`make kernelversion`
cp arch/myarch/boot/bzImage /boot/bzImage-KERNEL_VERSION
cp System.map /boot/System.map-KERNEL_VERSION

Kernel Update: form 2.6.X.A to 2.6.X.B
ftp ftp.kernel.org/pub/linux/kernel/v2.6/incr
get patch-2.6.X.A-B.bz2
bzip2 -dv patch-2.6.X.A-B.bz2
cd linux-2.6.X.A
patch -p1 ../patch-2.6.X.A-B.bz2
head -n5 Makefile
cd ..
mv linux-2.6.X.A linux-2.6.X.B
make {oldconfig|silentoldconfig}

Modules:

Manual way
   ls /sys/class/net
   modulename=$(basename`readlink /sys/class/net/eth0/device/driver/module`)
   find -type f -name Makefile | xargs grep $modulename
   make menuconfig # / initiates a search

   device=$(ls /sys/class/tty/ | grep USB)
   modulename=$(basename`readlink /sys/class/tty/$device/device/driver/module`)
   ....


Devices:

FS modules
ls /sys/block
ls /sys/class/block
ls -l /sys/block/<device>
# Look for device -> ../../devices/X/..Y/Z
ls -s /sys/devices/X/..Y/Z
# Look for driver -> ../../../...../bus/YYYY
Repeat last three going up one dir until the driver-> is found.

PCI. VendorID-DeviceID
busid=$(lspci | grep -i <devicetype> | cut -c 1-7)
cd /sys/bus/pci/devices/0000:$busid
cat vendor 
cat device
cd /usr/src/linux
grep -i $vendor include/linux/pci_ids.h
grep -i $device include/linux/pci_ids.h
grep -iRl $vendor *
# find the <driverfilename> in which both vendor and device macros are used in
# a struct pci_device_id
find -type f -name Makefile | grep <driverfilename>


USB.  VendorID-ProductID (When plugged: Bus-Device )
# List USB buses. Dont show Host Controllers
lsusb | grep  -v 0000:0000
# UNPLUG the device
lsusb | grep  -v 0000:0000
vendorID=...
productID=...
cd /usr/src/linux
grep -iRl $vendorID drivers 
# find the <driverfilename> in which the pair <vendorid-productid> appears in
# a struct usb_device_id
find -type f -name Makefile | grep <driverfilename>

BINUTILS

Running: as ld gprof
Linux:   ksymoops
Info:  size objdump readelf nm strings 
Transform: c++filt addr2line
Admin: strip ar ranlib objcopy

Kernel Internals

Timing 

One tick every 10 ms.

Interesting dirs

arch/i386/boot
init
kernel  
arch/i386/kernel
mm 
arch/i386/mm
fs
drivers
ipc
net
lib
arch/i386/math-emu
 
Interesting files

.config
Config.in
.depend
include/linux/autoconf.h  (Config vars)

include/asm-i386/boot.h
arch/i386/boot/bootsect.S
arch/i386/boot/setup.S

drivers/char/serial.c
drivers/net/Space.c

include/linux/list.h (struct list_head, list_add, list_del

include/linux/wait.h (struct __wait_queue, __add_wait_queue __remove_wait_queue
include/linux/timer.h (struct timer_list, add_timer, del_timer. mod_timer
include/asm-i386/atomic.h

include/asm/semaphore.h (struct semaphore, down, up

include/linux/fs.h (NR_FILE, NR_SUPER struct file, struct block_device, struct inode, )

include/asm/current.h (get_current, 
include/linux/sched.h (struct task_struct, struct mm_struct, struct files_struct, INIT_TASK, sleep_on )
include/linux/asm-i386/processor.h (init_task, THREAD_SIZE)
TASK_	TASK status
PF_ 	Process Flags
SCHED_	SCHEDule Algorithm

include/linux/mm.h (struct page mem_map_t, 
GFP_
mm/slab.h (kmalloc, kfree, 
mm/page_alloc.h (__alloc_pages, get_zeroed_page,  


ABIs: include/linux/personality.h
PER_

kernel/fork.c (max_threads)

include/linux/init.h (struct kernel_param, __setup, module_init, module_exit)
init/main.c (start_kernel)

Boot
"To use an RD or not that's the Q"

Boot params

S   (Run init in single mode)
apic=[quiet|verbose|debug]
acpi=[force,off,noirq,ht,strict]
acpi_sleep
acpi_sci
console= {tty<n>, ttyS<n>,bbbpnf, ttyUSB0,..}
crashkernel=size[KMG]@start[KMG]
debug
easyprintk=[vga],[serial][,ttySn][,baudrate]][,keep]
elfcorehdr=address
floppy=
hd=<cyl>,<head>,<sect>
highmem=nn[KMG]
hugepages=n
ide={nodma,doubler,reverse}
ide?=noprobe, ...
ignore_loglevel
ihash_entries
init=<pathtoinitfile>
initcall_debug
initrd=<pathtoinitialRD>
irqfixup
irqpoll
noirqdebug
kstack=n
lapic
load_ramdisk=<listofRDtoloadfromfloppy>
log_buf_len=<sizeofprintkbuffer>
loglevel={0-7}
lp={0,reset,auto,<port>}
max_addr=[KMG] All physical memory greater will be ignored.
max_loop=<MAXnumofloopdev>
mem=nn[KMG] Mem to use.
netconsole=scr-port@src-ip/dev,target-port@target-ip/target-mac-addr
noapic
noinitrd
noirqbalance
noisapnp
nolapic
noresume
nosmp
nousb
panic=<timeout>
parport=setting,setting...
pause_on_oops=n
pci={off,bios,nobios,conf1,conf2,nosort,noacpi,nommconf,nomsi,biosirq,...
pnpacpi=off
pnpbios= { on | off | curr | res | no-curr | no-res  }
profile=   (/proc/profile)
prompt_ramdisk=1
quiet
ramdisk_size=<n>K
rdinit= binary  (Run binary from ramdisk instead of init form root)
resume=SWAP partition for suspend
resume_offset=
ro  (Root FS R/O on root)
root=  (Root FS)
root_delay= (Root FS Mount delay)
rootflags= (Root FS mount options)
rootfstype= (Root FS Type)
rw  (Root FS R/W on root)
selinux= {"0"|"1"}
time  (Timing data for printk)
vga=(Video Mode) ask
video=(Framebuffer configuration)
     video=atyfb:mode:1280x1024,font:SUN12x22
     video=matrox:vesa:440
     video=xxx:off - disable probing for a particular framebuffer device
     video=map:octal-number # maps the virtual consoles (VCs) to framebuffer (FB) devices
           video=map:01 will map VC0 to FB0, VC1 to FB1, VC2 to FB0, VC3 to FB1..
	    video=map:0132 will map VC0 to FB0, VC1 to FB1, VC2 to FB3, VC4 to FB2, VC5 to FB0..



sys	kernel pars modified by sysctl
filesystems	FS

Hacks

MAGIC SYSRQ KEY

ALT+PrintScreen???

loglevel(0-9) Set the console log level, which controls the types of kernel
              messages that are output to the console

reBoot    Immediately reboot the system, without unmounting or syncing
          filesystems

Crash     Reboot kexec and output a crashdump

tErminate-all-tasks Send SIGTERM to all processes, allowing them to terminate
                    gracefully
                    Send SIGTERM to all procs excepi init (PID 1)

memory-full-oom-kill(F) Call oom_kill, which kills a process to alleviate an
                        OOM condition

Help  Display help

kIll-all-tasks  Send SIGKILL to all processes, forcing them to terminate
                immediately
                Send SIGKILL to all procs except init (PID 1)

thaw-filesystems(J)  Forcibly "Just thaw it" - filesystems frozen by the
                     FIFREEZE ioctl.

saK  Kill all processes on the current virtual console (Can be used to kill X
     and svgalib programs
     This was originally designed to imitate a Secure Access Key

show-Memory-usage  Output current memory information to the console

Nice-all-rt-tasks  Reset the nice level of all high-priority and real-time
                   tasks

powerOff  Shut off the system

show-registers(P)   Output the current registers and flags to the console

show-all-timers(Q)  Display all active high-resolution timers and clock
                    sources.

unRaw      Take control of keyboard back from X
           Turns off keyboard raw mode

Sync   flush data to disk. Sync all mounted filesystems

show-task-states(T)   Output a list of current tasks and their information to
                      the console

Unmount  remount all filesystems read-only

force-fb(V)   Forcefully restores framebuffer console, except for ARM
              processors, where this key causes ETM buffer dump

show-blocked-tasks(W)   Display list of blocked (D state) tasks

dump-ftrace-buffer(Z)   Dump the ftrace buffer

Safe Reboot:
REISUB

unRaw
tErminate-all-tasks
kIll-all-tasks
Sync
Unmount
reBoot

HOWTO compile kernel with distcc
================================

This article shows how we can compile the linux kernel with two or more machines using distcc.
Requirements

This setup requires two or more linux hosts with the same baseline of gcc on an IP network, preferably a private one. Distcc must be installed on both hosts (see [1]).

The use of ccache is optional.

(see www.delorenzo.info)
Setting up

In this example, we suppose that we have one or more server machines which run a distcc daemon, and a client machine where the compilation kernel will go. The client machine may optionally participate in the compilation of the kernel. If not, the kernel will be compiled by the servers.
On the server(s)

Now, we will see the config features on the server or multiple servers.

The file /etc/conf.d/distccd

DISTCCD_OPTS="-j2" #The number of files processed by distcc
[...]
DISTCCD_OPTS="${DISTCCD_OPTS} --port 3632"  #Network port config
[...]
DISTCCD_OPTS="${DISTCCD_OPTS} --log-level critical" #Syslog config
[...]
DISTCCD_OPTS="${DISTCCD_OPTS} --allow 192.168.0.0/24" #Network config. This is the net where the server listen petitions
[...]
DISTCCD_NICE="15"

Finally, start the distcc server

[user@system ~] /etc/init.d/distccd start

If the client should help compile the kernel...

In that case, we can compile the kernel by this way:

[user@system ~] export DISTCC_HOSTS="localhost 192.168.0.10" #localhost is the client host; The 192.168.x.x is the distcc server
[user@system ~] cd /usr/src/linux
[user@system ~] make CC="distcc gcc" -j4

Note: If your machines use different architectures you may want to use for example:

make CC="distcc x86_64-pc-linux-gnu-gcc" -j4

If the client doesn't compile the kernel...

In that case, we compile the kernel by this way

[user@system ~] export DISTCC_HOSTS="192.168.0.10"
[user@system ~] cd /usr/src/linux
[user@system ~] make CC="distcc" -j4

Adding Ccache

Install Ccache:

emerge -av ccache

Set Ccache to prefix all make commands with distcc:

[user@system ~] export CCACHE_PREFIX='distcc'

If the client should help compile the kernel with Ccache

In that case, we can compile the kernel by this way:

[user@system ~] export DISTCC_HOSTS="localhost 192.168.0.10" #localhost is the client host; The 192.168.x.x is the distcc server
[user@system ~] cd /usr/src/linux
[user@system ~] make CC="ccache distcc" -j4

If the client doesn't compile the kernel but still uses Ccache

In that case, we compile the kernel by this way

[user@system ~] export DISTCC_HOSTS="192.168.0.10"
[user@system ~] cd /usr/src/linux
[user@system ~] make CC="ccache distcc" -j4

User Mode Linux
===============

http://user-mode-linux.sourceforge.net/

make mrproper
make mrproper ARCH=um
make defconfig ARCH=um
make menuconfig ARCH=um
make ARCH=um
*****************************************************************************
