Verify:
  ls /sys/firmware/efi/efivars (If the directory exist your computer supports EFI)
  ping archlinux.org
  timedatectl set-ntp true
  timedatectl status
Go to https://archlinux.org/mirrorlist and find the closest mirror that supports HTTPS:
        Add the mirrors on top of the /etc/pacman.d/mirrorlist file.
Server = https://mirror.neuf.no/archlinux/$repo/os/$arch (Norway)

    Change root password:
        passwd


fdisk /dev/sda
 g (to create a new partition table)
 n (to create a new partition)
 1
 enter
 +300M
 t
 1 (for EFI)
 n
 2
 enter
 enter
 w
mkfs.fat -F32 /dev/sda1
mkfs.ext4 /dev/sda2
mount /dev/sda2 /mnt
pacstrap -i /mnt base base-devel
genfstab -U -p /mnt >> /mnt/etc/fstab
arch-chroot /mnt
ln -sf /usr/share/zoneinfo/Europe/Madrid /etc/localtime
hwclock --systohc
pacman -S grub efibootmgr dosfstools os-prober mtools linux-headers linux-lts linux-lts-headers
pacman -S openssh vim NetworkManager
systemctl enable NetworkManager
sed -i 's/#en_US.UTF-8/en_US.UTF-8/g' /etc/locale.gen (uncomment en_US.UTF-8) or add any locale you want
locale-gen
echo KEYMAP=es > /etc/vconsole.conf
passwd
mkdir /boot/EFI
mount /dev/sda1 /boot/EFI
grub-install --target=x86_64-efi --bootloader-id=grub_uefi --recheck
cp /usr/share/locale/en\@quot/LC_MESSAGES/grub.mo /boot/grub/locale/en.mo
grub-mkconfig -o /boot/grub/grub.cfg
fallocate -l 2G /swapfile
chmod 600 /swapfile
mkswap /swapfile
echo '/swapfile none swap sw 0 0' | tee -a /etc/fstab
exit
umount -a
reboot

