==========
TMUX vs SCREEN
==========
Action                              tmux			screen

Start a new session                 tmux                        screen
                                    tmux new    
                                    tmux new-session

Re-attach a detached session        tmux attach                 screen -r
                                    tmux attach-session

Re-attach an attached session       tmux attach -d              screen -dr
  (detaching it from elsewhere)     tmux attach-session -d

Re-attach an attached session       tmux attach                 screen -x
  (keeping it attached elsewhere)   tmux attach-session

Detach from currently attached      ^b d                        ^a ^d
  session                           ^b :detach                  ^a :detach

Rename-window to newname            ^b , <newname>              ^a A <newname>
                                    ^b :rename-window <newname>

List windows                        ^b w                        ^a w

List windows in chooseable menu                                 ^a "

Go to window #                      ^b #                        ^a #

Go to last-active window            ^b l                        ^a l

Go to next window                   ^b n                        ^a n

Go to previous window               ^b p                        ^a p

See keybindings                     ^b ?                        ^a ?

List sessions                       ^b s                        screen -ls
                                    tmux ls
                                    tmux list-sessions

Toggle visual bell                                              ^a ^g

Create another shell                ^b c                        ^a c

Exit current shell                  ^d                          ^d

Split pane horizontally             ^b "

Split pane vertically               ^b %

Switch to another panel             ^b o

Kill the current panel              ^b x
                                    (logout/^d)

Close the rest of panes             ^b !

Swap location of panes              ^b ^o

Show time                           ^b t

Show numeric values of panes        ^b q

                                    ^b f
                                    ^b [
                                    ^b .

==========
