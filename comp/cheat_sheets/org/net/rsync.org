-a -rlptgoD
-r recurse into directories
-l copy symlinks as symlinks
-p preserve permissions
-t preserve modification times
-g preserve group
-o preserver owner (superuser only)
-v verbose
-q quiet
-e 
-p
-z compress file data during transfer
-A preserve ACLs (implies -p)
-D --devices --specials
-X
-H
--delete delete extraneous files from the receiving side
--devices
--specials
--link-dest
--no-whole-file

incremental rsync

rsync -avzt srcdir dstdir
rsync -avzt --link-dest=dstdir srcdir newdstdir

##################
# a backup script
#!/bin/bash
cd /var/backup/blackwidow || exit 1

DATE=`date +%F-%H-%M-%S`

rsync --exclude-from /scr/etc/backup-blackwidow.exclude     \
      -avz blackwidow:/ /var/backup/blackwidow/ROOT         \
      --backup                                              \  
      "--backup-dir=/var/backup/blackwidow/BACKUP-$DATE"    \
      --delete                                              \
      --delete-excluded                                     \
      --delay-updates                                       \
      --progress                                            \

##################
