*/!* [<history number>|<history match>]

*/ADMIN* [<server>]
Instructs the server to return information about the administrator of the
server specified by <server>, or the current server if target is
omitted.

*/AWAY* [<away message>]
Provides the server with a message to automatically send in reply to a
PRIVMSG directed at the user, but not to a channel they are on.[2] If
<message> is omitted, the away status is removed.

*/CONNECT <target server> [<port> [<remote server>]] (RFC 1459)
*/CONNECT <target server> <port> [<remote server>] (RFC 2812) 
Instructs the server <remote server> (or the current server, if <remote
server> is omitted) to connect to <target server> on port <port>.
This command should only be available to IRC Operators.
 
*/CLEAR*
Clears all text in the current chat window.

*/CTCP* [<argument>]
*/CTCP {nick} {ping|finger|version|time|userinfo|clientinfo}
Does the given ctcp request on nickname.

*/DATE* [<server>]
*/DCC CHAT*
*/DCC CLOSE* <type> <nickname> [<arguments>]
*/DCC GET* <nickname> <filename>
*/DCC LIST*
*/DCC SEND* <nickname> <filename>
*/DCC* <function> [<arguments>]
*/DESCRIBE* <nickname>|<channel> <action description>

*/DIE
Instructs the server to shut down.

*/ERROR <message>
This command is for use by servers to report errors to other servers. It
is also used before terminating client connections.[

*/HELP* [<command>]
*/HISTORY* [<number>]
*/IGNORE* [<nickname>|<user@host> [[-]<message type>]]

*/INFO* [<target>]
Returns information about the <target> server, or the current server if
<target> is omitted.[7] Information returned includes the server's
version, when it was compiled, the patch level, when it was started, and
any other information which may be considered to be relevant.

*/INVITE* <nickname> [<channel>]
Invites <nickname> to the channel <channel>.[8] <channel> does not have to
exist, but if it does, only members of the channel are allowed to invite
other clients. If the channel mode i is set, only channel operators may
invite other clients.

*/ISON <nicknames>
Queries the server to see if the clients in the space-separated list
<nicknames> are currently on the network.[9] The server returns only the
nicknames that are on the network in a space-separated list. If none of
the clients are on the network the server returns an empty list.

*/JOIN* <channels> [<keys>]
Makes the client join the channels in the comma-separated list <channels>,
specifying the passwords, if needed, in the comma-separated list
<keys>.[10] If the channel(s) do not exist then they will be created.

*/KICK* [<channel>] <nickname>
Forcibly removes <client> from <channel>.[11] This command may only be
issued by channel operators.

*/KILL <client> <comment>
Forcibly removes <client> from the network.[12] This command may only be
issued by IRC operators

*/LASTLOG* [<number of entries>|<text> [<from entry>]]
*/LEAVE* <channel>

*/LINKS [<remote server> [<server mask>]]
Lists all server links matching <server mask>, if given, on <remote
server>, or the current server if omitted.[13]

*/LIST [<channels> [<server>]]
Lists all channels on the server.[14] If the comma-separated list
<channels> is given, it will return the channel topics. If <server> is
given, the command will be forwarded to <server> for evaluation.

*/LUSERS [<mask> [<server>]]
Returns statistics about the size of the network.[15] If called with no
arguments, the statistics will reflect the entire network. If <mask> is
given, it will return only statistics reflecting the masked subset of the
network. If <target> is given, the command will be forwarded to <server>
for evaluation.

*/ME* <action description>
Sends the specifed 'action' to the current window.

*/MODE* <channel>|<nickname> [[+|-]<modechars> [<parameters>]]
  MODE <nickname> <flags> (user)
  MODE <channel> <flags> [<args>]
The MODE command is dual-purpose. It can be used to set both user and
channel modes.

User Modechars:
+b ban
+i invisible
+o operator. Give someone channel operator status
+s receive server notices
+v voice. Give someone a voice, i.e. allow him to speak on the channel
+w receive wallops

Channel Modechars:
+i invite only. The chanel is for invitation only.
+k prompt for password
+l maximum user limit
+m moderation. Only voiced users will be able to talk on the channel
+n no external messages. Only users on the same channel will be able
   to send messages into the channel
+p private. Make the channel private
+r restricted
+s secret. Make the channel secret
+t topic. Prevent normal users (i.e. non-chanops) from setting the
   channel topic

*/MOTD* [<server>]
Returns the message of the day on <server> or the current server if it is
omitted.

*/MSG* <nickname>|<channel> <text>

*/NAMES* [[<flags>] <channel mask>]
  NAMES [<channels>] (RFC 1459)
  NAMES [<channels> [<server>]] (RFC 2812)
Returns a list of who is on the comma-separated list of <channels>, by
channel name. If <channels> is omitted, all users are shown, grouped
by channel name with all users who are not on a channel being shown as
part of channel "*". If <server> is specified, the command is sent to
<server> for evaluation.

*/NICK* [<nickname>]
  NICK <nickname> [<hopcount>] (RFC 1459)
  NICK <nickname> (RFC 2812)
Allows a client to change their IRC nickname. Hopcount is for use between
servers to specify how far away a nickname is from its home
server.

*/NOTICE* <nickname>|<channel> <text>
  NOTICE <msgtarget> <message>
This command works similarly to PRIVMSG, except automatic replies must
never be sent in reply to NOTICE messages.

*/NOTIFY* [[-]<nickname>]

*/OPER <username> <password>
Authenticates a user as an IRC operator on that server/network.

*/PART <channels>
Causes a user to leave the channels in the comma-separated list
<channels>.

*/PASS <password>
Sets a connection password.[25] This command must be sent before the
NICK/USER registration combination.

*/PING <server1> [<server2>]
Tests the presence of a connection.[26] A PING message results in a PONG
reply. If <server2> is specified, the message gets passed on to it.

*/PONG <server1> [<server2>]
This command is a reply to the PING command and works in much the same
way.

*/PRIVMSG <msgtarget> <message>
Sends <message> to <msgtarget>, which is usually a user or channel.

*/QUERY* [<nickname>|<channel>]

*/QUIT* [<message>]
Disconnects the user from the server.

*/RAW {raw command}
Sends any raw command you supply directly to the server. Use with care!

*/REHASH
Causes the server to re-read and re-process its configuration file(s).[30]
This command can only be sent by IRC Operators.

*/RESTART
Restarts a server.[31] It may only be sent by IRC Operators.

*/SERVER* [<server>|<server number> [<port number>]]
 /SERVER <servername> <hopcount> <info>
The server message is used to tell a server that the other end of a new
connection is a server.[34] This message is also used to pass server data
over whole net. <hopcount> details how many hops (server connections) away
<servername> is. <info> contains addition human-readable information about
the server.

*/SERVLIST [<mask> [<type>]]
Lists the services currently on the network.

*/SILENCE {+/-nick}
Stops/lets people send you private messages.

*/SLAP {nick}
Produces the text "your-nick slaps a large trout around a bit with {nick}"

*/SQUERY <servicename> <text>
Identical to PRIVMSG except the recipient must be a service.

/*SQUIT <server> <comment>
Causes <server> to quit the network.

*/STATS* c|i|k|l|m|u|y [<server>]
  STATS <query> [<server>]
Returns statistics about the current server, or <server> if it's
specified.

*/SUMMON <user> [<server>] (RFC 1459)
  SUMMON <user> [<server> [<channel>]] (RFC 2812)
Gives users who are on the same host as <server> a message asking them to
join IRC.

*/TIME* [<server>]
Returns the local time on the current server, or <server> if specified.

*/TOPIC* [[<channel>] <topic for channel>]
Allows the client to query or set the channel topic on <channel>.[41] If
<topic> is given, it sets the channel topic to <topic>. If channel mode +t
is set, only a channel operator may set the topic.

*/TRACE* [<target>]
Trace a path across the IRC network to a specific server or client, in a
similar method to traceroute.

*/USER <username> <hostname> <servername> <realname> (RFC 1459)
  USER <user> <mode> <unused> <realname> (RFC 2812)
This command is used at the beginning of a connection to specify the
username, hostname, real name and initial user modes of the connecting
client.[43][44] <realname> may contain spaces, and thus must be prefixed
with a colon.

*/USERHOST <nickname> [<nickname> <nickname> ...]
Returns a list of information about the nicknames specified.

*/USERS* [<server>]
Returns a list of users and information about those users in a format
similar to the UNIX commands who, rusers and finger.

*/VERSION*

*/WHO* [<channel>|<wildcard expression>]
Shows all people on the IRC server with a matching address.

*/WHOIS* [<nickname>]
Shows information about the given nickname.

*/WHOWAS* [<server>] [<nickname>]
Shows information about someone who -just- left IRC.

Willikins bot:
--------------
!help
10 core modules:
auth, basics, config, filters, httputil, irclog, remote, unicode, 
userdata, wordlist;
50 plugins:
alias, autoop, autorejoin, bans, botsnack, bugzilla, chanserv, debug, 
dict, dictclient, dns, freshmeat, gentoo, gentoofixup, 
gentoosearch, gentooshortenurls, geoip, googlefight, 
greed, greet, host, iplookup, karma, keywords, linkbot, 
math, modes, nagios, nickrecover, nickserv, note, poll, 
q, quote, remind, remotectl, ri, rot, script, search, seen, shortenurls, 
spell, spotify, time, topic, translator, tumblr, 
usermodes, wserver (help <topic> for more info)

!away name
!botsnack
!bug bugnumber
!devaway name
!earch <package>
!expn mysql
!expn ruby
!expn security
!herd name
!proj project
!meta -v x11-libs/wxGTK
!meta x11-libs/wxGTK
!note <target> <message>
!rdep package
!seen nick
!time nick
!time set Europe/Madrid

# BAN example
/mode #gentoo +b

IRC- and irssi-Workshop
=======================

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
IRC- and irssi-Workshop by Wernfried 'amne' Haas for Security Treff Graz
and grml-Developers on 12 Juli 2006 at FH Joanneum.
Notes taken by Michael 'mika' Prokop.

Latest change: Mon Jul 17 21:53:20 CEST 2006 [mika]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Statusbar:
----------

[19:23] [+grml(+ir)] [2:#grml(+lnt 19)]
 clock    ^    ^      ^  ^    ^^^^ ^
          |    |      |  |    |||| |
    username   | window chan. |||| mode level 19
            + = positive/set  ||||
            i = invisible     ||||
            r = restricted    http://freenode.net/using_the_network.shtml

Switch Window:
--------------

esc-cursor or alt-cursor => switch window
alt-q => window 11
alt-w => window 12
/window 30 => window 30

People with qwertz layout probably want to swap meta-y and meta-z:

  /BIND meta-z change_window 16
  /BIND -delete meta-y

Add network and server:
-----------------------

/network add -nick mikap -realname "Michael Prokop" freenode
/server add -auto -network freenode chat.freenode.net
/network add freenode -autosendcmd /FNAUTH   => send self defined alias /FNAUTH
                                                by default to freenode

Join server:
------------

/connect freenode

Close connection to server:
---------------------------

/disconnect freenode

Autojoin channel:
-----------------

/channel add -auto #grml-workshop freenode

List channels:
--------------

/list

Display configuration of irssi:
--------------------------------

/set
/set autocreate_own_query => display setting of variable autocreate_own_query

Kick user:
----------

/kick username     => just kick
/kickban username  => kick and username can't join channel again
/ban username      => can't join channel again
/unban username    => unban again
/knockout <time> <nick> <reason> => kickban a user for specific time

Window actions:
---------------

/window move left  => move window to left
/window move 1     => move window to position 1
/layout save       => store/remember window settings

Diff stuff:
-----------

/who               => display users in channel in status window
/who mika          => display info about user mika
/wii mika          => display info about user mika including idle state (depends on network)
/names             => display users in channel in channel window
/set user_name fo  => set (ident) username to 'fo'
/away -all wenn mich jemand braucht, ich bin auf der toilette => set away-status on all networkß
/me is away        => not welcome in many channels
/mode +q idiot     => don't allow messages from user idiot to channel (freenode special)
/quit              => leave all channels and quit irssi
/WC                => leave channel and close window
/part              => leave channel but don't close window
/mod +i            => only allow invited users (/invite user)
/stats p           => display stats members
/alias FNAUTH  set autocreate_own_query OFF;msg -freenode nickserv identify PASSWORD;wait -freenode 3000;msg -freenode
                   chanserv invite #channel;msg -freenode nickserv set unfiltered on;set autocreate_own_query ON;
                   /quote capab identify-msg
/reload            => reload configuration (~/.irssi/config)
/ /CALC 3 * 3      => write "/CALC 3 * 3" into the channel
/exec -o uptime    => display uptime

Direct Client Connect:
----------------------

/dcc chat username     => direct chat with username
/msg =username message => send "message" to username without connection to server

NickServ (nick name handling):
------------------------------

/query NickServ         => create new window to talk to NickServ
help                    => get usage information
register <password>     => register your nick
info <user>             => request information about user
set password <newpass>  => set new passwort
set email foo@b.invalid => set mailaddress
set hide email          => don't display mailaddress in "info" information
link mikap_ <pass>      => link nickname mikap_ to mikap (mikap_ has to be registered as well of course)
set master mikap        => set master nickname to mikap

ChanServ (channel handling - depends from IRC net):
---------------------------------------------------

/query ChanServ
register #channel <password>    => register channel
set #grml-workshop mlock +ton-m =>
set secureops                   =>
level #channel list             => display level information
level #channel set user 50      => set user to level 50
level #channel set autoop 10    => "cmdop" -> be able to /op
access #channel add user        =>
invite #channel                 => all users in channel are allowed to send "/invite"s
recover username                +
release username                => kill username and release the nickname (also see the ghost command)

Cloak Users:
------------

http://freenode.net/faq.shtml#cloaks

Logging:
--------

/set autolog = "yes"
/set autolog_path = "~/Logs/irc/$tag_$0.%Y-%m-%d.log"
/set autolog_level = "MSGS ACTIONS KICKS PUBLIC"

Scripts:
--------

% mkdir ~/.irssi/scripts ; cd ~/.irssi/scripts ; wget http://www.irssi.org/scripts/scripts/scriptassist.pl
/script load scriptassist.pl
/scriptassist install chanact
/script load chanact
[ /statusbar chanact add ]
[ /statusbar window remove chanact ]
/statusbar chanact add chanact -after act

/script unload script.pl

http://ben.reser.org/irssi/format_identify.pl
http://wouter.coekaerts.be/irssi/scripts/format_identify.pl
/script load format_identify
/quote capab identify-msg
=> not identified users are displayed as "user?"

Nicklist (works inside GNU screen or via fifo):
-----------------------------------------------

/scriptassist install nicklist
/script load nicklist
/nicklist screen

Keybindings:
------------

/bind meta-y /window last   => toggle between last used windows

Resources:
----------

/usr/share/doc/irssi
http://www.irssi.org/documentation
http://de.wikibooks.org/wiki/Irssi

# EOF

CREATING A PRIVATE CHANNEL
1 Create your separate channel by typing
  /join #mychannel
  where mychannel is the name of your new channel.
2 Register it with
   /cs register #mychannel <password> (optional description)
3 Inside the new channel, assign a topic to it by typing
  /topic #mychannel This channel is about recent change patrol
  where This channel is about recent changes patrol is the topic of
  the new channel.
4 Invite others to join. In the larger, public channel, type
  /Invite john #mychannel
  where john is the user name and #mychannel is your channel.'
  If you want your channel to be invitation only, type
  /mode #mychannel +i
  where #mychannel is the name of your channel.
5 Set your new channel to private or secret by typing /mode #mychannel +s
  for secret and /mode #mychannel +p for private. If you don't mind your
  new channel being public, don't do anything. All new channels are
  automatically public.


OPERATOR
-------
/msg chanserv op #gentoo-es
