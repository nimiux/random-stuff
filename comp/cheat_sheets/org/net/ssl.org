How to get fingerprint of a server:

1. openssl s_client -connect service:port -showcerts
   openssl s_client -connect mail.domain.tld:25 -starttls smtp
2. Get the certificate in thecertificate.pem
3. openssl x509 -in thecertificate.pem -noout -md5 -fingerprint

# Find out cert fingerprint in the server
openssl x509 -noout -in cert.pem -fingerprint

# Generate the key
# unencrypted, 4096-bit key
#openssl genrsa -out privkey.pem 4096
# alternatively, encrypted
#openssl genrsa -des3 -out privkey.pem 4096
openssl genpkey -genparam -algorithm DSA -out dsaparam.pem -outform PEM -pkeyopt dsa_paramgen_bits:4096
openssl genpkey -paramfile dsaparam.pem -out dsakey.pem

# Generate SHA256 certificate signing request
openssl req -new -sha256 -key privkey.pem -out cert.csr

# Get the certificate
# cert.pem

# display contents
cat cert.csr

openssl x509 -in new.nibbler.org.es.csr  -out new.nibbler.org.es.cert -req -signkey new.nibbler.org.es.key -days 365

Self signing:
1. Create the key
openssl genrsa -out ca.key 1024
openssl genrsa -des3 -out mail.domain.tld.key 2048
2. Create the certificate signing request
openssl req -new -key server.key -out server.csr
openssl req -new -key mail.domain.tld.key -out mail.domain.tld.csr
3. Create a self signed keys
openssl x509 -req -days 365 -in mail.domain.tld.csr -signkey mail.domain.tld.key -out mail.domain.tld.crt
4. Remove the password from the private keys
openssl rsa -in mail.domain.tld.key -out mail.domain.tld.key.nopass
mv mail.domain.tld.key.nopass mail.domain.tld.key
5. Make ourself a trusted CAkey
openssl req -new -x509 -extensions v3_ca -keyout cakey.pem -out cacert.pem -days 3650

#openssl req -new -x509 -days 365 -key ca.key -out ca.crt
#sign.sh
#

# Root Cert
openssl req -new -x509 -extensions v3_ca -keyout ca_eccsa.key -out ca_eccsa.crt -days 365
# Show cert
openssl x509 -in ca_accsa.crt -text
# Create a CSR
openssl req -new -out theweb.csr -keyout theweb.key
# Create CA Cert
openssl x509 -CA -ca_eccsa.crt -CAkey ca_eccsa.key -CAcreateserial -CAserial serial.txt -req -in theweb.csr -ouy theweb.crt
or
openssl ca -out theweb.crt -infiles theweb.csr

# Create a PKCS12 cert
openssl pckcs12 -export -in myuser.crt -inkey myuser.key -certfile ca_eccsa.crt -name "My user" -out myuser.p12
# Signing smime
openssl smime -sign -in test.txt -out test.txt.smime -text -signer mysuer.crt -inkey myuser.key
# Cyphering
openssl smime -encrypt -sign -in test.txt -out test.txt.smime -text -signer mysuer.crt -inkey myuser.key mydestination.crt
#
# Symmetric emcryption
# Cypher
openssl enc -blowfish -e -in test.txt -out test.txt.blowfish
# Decypher
openssl enc -blowfish -d -in test.txt -out text.txt.blowfish
##################################################
#
# nano -w /etc/ssl/openssl.cnf

Hay que ajustar los siguientes valores por defecto para nuestro dominio:

countryName_default
stateOrProvinceName_default
localityName_default
0.organizationName_default
commonName_default
emailAddress_default.

(Si las variables no están presentes, hay que añadirlas.)

# cd /etc/ssl/misc
# ./CA.pl -newca
# ./CA.pl -newreq-nodes
# ./CA.pl -sign
# cp newcert.pem /etc/postfix/cert
# cp newkey.pem /etc/postfix/cert
# cp demoCA/cacert.pem /etc/postfix/cert

(Ahora hacemos lo mismo para apache.)

# openssl req -new -nodes > new.cert.csr
# openssl rsa -in privkey.pem -out new.cert.key
# openssl x509 -in new.cert.csr -out new.cert.cert -req -signkey new.cert.key -days 365

(Por ahora dejamos estos certificados ahí. Los agregaremos después de instalar Apache.)


General OpenSSL Commands

Generate a new private key and certificate signing request:
openssl req -new -out misite.csr -newkey rsa:2048 -keyout misite.key
Generate a self signed certificate:
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout misite.key -out misite.crt
Generate a certificate signing request for an existing private key:
openssl req -out misite.csr -key misite.key -new
Generate a certificate signing request based on an exisiting certificate
openssl x509 -x509toreq -in misite.crt -out misite.csr -signkey misite.key
Remove a passphrase from a private key
openssl rsa -in misite.key -out misite.pem
Sing the request
openssl x509 -req -days 365 -in server.csr -signkey server.key -out server.crt

Check a certificate signing request
openssl req -text -noout -verify -in misite.csr
Check a private key
openssl rsa -in misite.key -check
Check a certificate
openssl x509 -in misite.crt -text -noout
Check a PKCS#12 file
openssl pkcs12 -info -in misite.p12

Check an MD5 hash of the public key to ensure that it matches whith what is in a CSR or private key:
openssl x509 -noout -modulus -in misite.crt | openssl md5
openssl rsa -noout -modulus -in misite.key | openssl md5
openssl req -noout -modulus -in misite.csr | openssl md5
Check an SSL connection. All the certificates (including intermediates) should be displayed
openssl s_client -connect www.misite.com:443

Convert a DER file to PEM
openssl x509 -inform der in misite.cer -out misite.pem
Convert a PEM file to DER
openssl x509 -outform der in misite.pem -out misite.der
Convert a PKCS#12 containing a private key and certificates to PEM
openssl pkcs12 -in misite.pfx -out misite.pem -nodes
Conver a PEM certificate file and a private key to PKCS#12
openssl pkcs12 -export -out misite.pfx -inkey misite.key -in misite.crt -certifle CACert.crt


Check ciphers
$ openssl ciphers -v 'ALL:@STRENGTH'

#########
# APACHE
#########
# Creating self-signed certificates

CRTDIR="/root/CA"
HOST="myhost"
CAKEY="my-ca.key"
CACRT="my-ca.crt"
CASRL="my-ca.srl"
HOSTKEY="$HOST-server.key"
HOSTCSR="$HOST-server.csr"
HOSTCRT="$HOST-server.crt"

mkdir $CRTDIR
chmod 0770 $CRTDIR
cd $CRTDIR

# SETUP CA
# Create my own CA private key
openssl genrsa -des3 -rand fichero1.gz:fichero2.gz -out $CAKEY 1024
# View key
openssl rsa -noout -text -in  $CAKEY
# Create a keys' decrypted PEM
openssl rsa -in  $CAKEY -out $CAKEY.unencrypted
# Create self-signed CA certificate x509 with CA private key (Distinguished Name or a DN)
openssl req -new -x509 -days 3650 -key $CAKEY -out $CACRT
# View CA certificate
openssl x509 -in $CACRT -text -noout

# CREATE SERVER CERTIFICATE AND PRIVATE KEY
openssl genrsa -des3 -out $HOSTKEY 1024
# Create a Certificate Signing Request with the key
openssl req -new -key $HOSTKEY -out $HOSTCSR
# Sing CSR with self-created CA
#openssl x509 -req -in $HOSTCSR -signkey $HOSTKEY                              -days 3650 -out $HOSTCRT
echo "01" > $CASRL
openssl x509 -req -in $HOSTCSR -CA $CACRT -CAkey $CAKEY -CAcreateserial -days 3650 -out $HOSTCRT

# COPY FILES
cp $CACRT /etc/httpd/conf/ssl.crt/my-ca.crt
cp $HOSTKEY /etc/httpd/conf/ssl.crt/mars-server.key
cp $HOSTCRT /etc/httpd/conf/ssl.crt/mars-server.crt

# CREATE DIRECTORIES
Create dir for secure browsing
mkdir /var/www/SSL
chmod 0775 /var/www/SSL
mkdir /var/www/SSL/{Passneeded,Certneeded,PassAndCert}

# CONFIGURE APACHE:
DocumentRoot "/var/www/SSL"
# Note that the FQDN and server hostname must go here - clients will not be able to connect, otherwise!
ServerName mars.vanemery.com:443
ServerAdmin webmaster@vanemery.com

# Here, I am allowing only "high" and "medium" security key lengths.
SSLCipherSuite HIGH:MEDIUM

# Here I am allowing SSLv3 and TLSv1, I am NOT allowing the old SSLv2.
SSLProtocol all -SSLv2

#   Server Certificate:
SSLCertificateFile /etc/httpd/conf/ssl.crt/mars-server.crt

#   Server Private Key:
SSLCertificateKeyFile /etc/httpd/conf/ssl.key/mars-server.key

#   Server Certificate Chain:
SSLCertificateChainFile /etc/httpd/conf/ssl.crt/my-ca.crt

#   Certificate Authority (CA):
SSLCACertificateFile /etc/httpd/conf/ssl.crt/my-ca.crt

# This is needed so that you can use auto-indexing for some directories in the
# /var/www/SSL directory branch.  This can be handy if you would like to have
# a list of sensitive files for people to download.
<Directory "/var/www/SSL">
   Options Indexes
   AllowOverride None
   Allow from from all
   Order allow,deny
</Directory>

#############################################################################
***** More *****
Get the Private Key
mkdir /root/CA
chmod 0770 /root/CA
cd /root/CA
openssl genrsa -des3 -out my-ca.key 2048
openssl req -new -x509 -days 3650 -key my-ca.key -out my-ca.crt
openssl x509 -in my-ca.crt -text -noout

Get the certificate for the web server
openssl genrsa -des3 -out mars-server.key 1024
openssl req -new -key mars-server.key -out mars-server.csr
openssl x509 -req -in mars-server.csr -out mars-server.crt -sha1 -CA my-ca.crt -CAkey my-ca.key -CAcreateserial -days 3650
openssl x509 -in mars-server.crt -text -noout
chmod 0400 *.key
cp mars-server.crt /etc/httpd/conf/ssl.crt
cp mars-server.key /etc/httpd/conf/ssl.key
cp my-ca.crt /etc/httpd/conf/ssl.crt
****************************
#############################################################################

/etc/init.d/httpd start
netstat -tna

### Simple Pass ###
[root]# htpasswd -c -m /etc/httpd/.htpasswd joe
New password:
Re-type new password:
Adding password for user joe
[root]# htpasswd -m /etc/httpd/.htpasswd john
New password:
Re-type new password:
Adding password for user john

[root]# chown apache.root /etc/httpd/.htpasswd
[root]# chmod 0460 /etc/httpd/.htpasswd

Now, we need to tell Apache to require a username/password to access the Passneeded directory. Here is what we will add to /etc/httpd/conf.d/ssl.conf file:

<Directory "/var/www/SSL/Passneeded">
   AuthType Basic
   AuthName "Username and Password Required"
   AuthUserFile /etc/httpd/.htpasswd
   Require valid-user
</Directory>
### Simple Pass ###
o
### Change TCP Port ###
Listen 444

<VirtualHost _default_:444>

ServerName mars.vanemery.com:444

### Change TCP Port ###


### Client Certificate ####
# cd /root/CA
# openssl genrsa -des3 -out van-c.key 1024
# openssl req -new -key van-c.key -out van-c.csr
#openssl x509 -req -in van-c.csr -out van-c.crt -sha1 -CA my-ca.crt -CAkey my-ca.key -CAcreateserial -days 3650
# openssl pkcs12 -export -in van-c.crt -inkey van-c.key -name "Van Emery Cert" -out van-c.p12
# openssl pkcs12 -in van-c.p12 -clcerts -nokeys -info

Apache config
<Directory /var/www/SSL/Certneeded>
   SSLVerifyClient require
   SSLVerifyDepth 1
</Directory>
### Client Certificate ####



### Pass and Cert ###
<Directory "/var/www/SSL/PassAndCert">
   SSLVerifyClient require
   SSLVerifyDepth 1
   AuthType Basic
   AuthName "Restricted Area"
   AuthUserFile /etc/httpd/.htpasswd
   Require valid-user
</Directory>
### Pass and Cert ###


###################################################################
Multiples SSL Virtual Hosts con Apache2 y ModRewrite

El Problema: En ocaciones se nos ha presentado el caso en el que queremos
tener varios virtualhost en modo seguro usando SSL, en la misma maquina con
1 solo IP y esto Apache no lo soporta, pues HTTPS no es mas que HTTP
encapsulado dentro del un tunnel SSL, y apache hace una verificaciÃ³n del
Header HTTP trayendo como consecuencia que solo podamos tener un HTTPs por
cada Host/IP. El Problema radica que el Tunnel SSL es creado antes de enviar
el primer Paquete HTTP, Apache necesita un certificado SSL pero aun no tiene
un Host Header para verificar y ver si coinciden, por tanto no puede escoger
un VirtualHost, he aquÃ­ de porque solo podemos tener 1 VirtualHost por
IP o Host. La Solucion: Este truco esencialmente lo que hace es hacer la
comparacion del Host Header antes de abrir el Tunnel SSL, como??? pues con un
poco de magia del ModRewrite. Este mÃ©todo no es perfecto, pues claro no hay
magia...este metodo aun contiene algunos problemas como por ejemplo: â
El Certificado SSL que se usara sera comun para todos los Vhosts. â Las
opciones que no se pueden Override en el .htaccess seran compartidas para
todos los Vhosts. El Truco: Creamos un fichero ssl.map que contenga lo
siguiente:

/etc/apache2/ssl.map
gosa.dpe.cfg.rimed.cu /var/www/gosa/
webmail.dpe.cfg.rimed.cu /var/www/webmail/
#FIN

Editamos un SSL VirtualHost que debe contener esto:

/etc/apache2/sites-available/gosa
# GOSA SSL #
<VirtualHost gosa.dpe.cfg.rimed.cu:443>
   ServerName "gosa.dpe.cfg.rimed.cu"
   DocumentRoot "/var/www/gosa"

   RewriteEngine on

   RewriteMap lowercase int:tolower
   RewriteMap vhost txt:/etc/apache2/ssl.map

   RewriteCond %{REQUEST_URI} !^/cgi-bin/.*
   RewriteCond %{REQUEST_URI} !^/icons/.*
   RewriteCond %{HTTP_HOST} !^$
   RewriteCond ${lowercase:%{HTTP_HOST}|NONE} ^(.+)$
   RewriteCond ${vhost:%1} ^(/.*)$

   RewriteRule ^/(.*)$ %1/$1 [E=VHOST:${lowercase:%{HTTP_HOST}}]

   <Directory /var/www/gosa/>
      Options Indexes FollowSymLinks MultiViews
      AllowOverride None
      Order deny,allow
      deny from all
      allow from all
   </Directory>

   CustomLog /var/log/apache2/gosa_access.log combined
   ErrorLog /var/log/apache2/gosa_error.log

   #En este caso Yo uso un Certificado Autofirmado.
   SSLEngine on
   SSLCertificateFile /etc/apache2/seguro.pem
#FIN

Ahora creamos otro VirtualHost gosa2 que va a escuchar normalmente
por el Puerto 80 y usando mod rewrite va redirigir las peticiones HTTP a HTTPs,
asi cuando pedimos http://gosa.dpe.cfg.rimed.cu nos va a redirigir
para https://gosa.dpe.cfg.rimed.cu

/etc/apache2/sites-available/gosa2
# GOSA DPE Redir#
<VirtualHost *>
   ServerName "gosa.dpe.cfg.rimed.cu"
   DocumentRoot "/var/www/gosa

   RewriteEngine On

   Options +FollowSymlinks
   RewriteCond %{HTTPS} !=on

   RewriteRule ^/(.*) https://gosa.dpe.cfg.rimed.cu [R,L]

   <Directory /var/www/gosa/>
      Options Indexes FollowSymLinks MultiViews
      AllowOverride All
      Order deny,allow
      deny from all
      allow from all
   </Directory>

   CustomLog /var/log/apache2/gosaredir_access.log combined
   ErrorLog /var/log/apache2/gosaredir_error.log
#FIN

Hecho lo anterior solo debemos verificar que tenemos activado el
ModSSL, Modrewrite y Apache escuchando en el puerto 443 para SSL,
hacemos desde consola lo siguiente:

a2enmod rewrite
a2enmod ssl

Verificar que el fichero Ports contiene lo siguiente:

/etc/apache2/ports
Listen 80
Listen 443

Activamos los VirtualHost que creamos, desde consola:

cd /etc/apache2/sites-available/
a2ensite gosa
a2ensite gosa2

Yo uso un certificado autofirmado que lo genero con la siguiente
linea desde consola:

openssl req -x509 -nodes -days 365 -newkey rsa:1024 -keyout seguro.pem -out seguro.pem

Este Certificado lo genero y en vez de poner el valor
gosa.dpe.cfg.rimed.cu le pongo .dpe.cfg.rimed.cu
en el CN ya que este certificado sera comun para todos los Vhost.
Reiniciamos apache:

/etc/init.d/apache2 restart

Y listo todo debe funcionar bien, siguiendo los pasos anteriormente
expuestos podremos crear varios VirtualHosts que usen SSL...

Alien Torres thebug2k5@yahoo.es Revisado 9 Diciembre 2008
###################################################################

