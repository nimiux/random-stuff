my $TARGETARCH="amd64"

#PORTAGEFILE="${DISTFILESSITE}/snapshots/portage-latest.tar.bz2"
#PORTAGEMD5SUM="${DISTFILESSITE}/portage-latest.tar.bz2.md5sum"
#PORTAGEGPGSIG="${DISTFILESSITE}/portage-latest.tar.bz2.gpgsig"

=head3

=cut

sub gentoo_releng_keys {
    my ($self) = @_;

    system "gpg --keyserver subkeys.pgp.net --recv-keys 0xBB572E0E2D182910";
}

=head3

=cut

sub gentoo_get_latest {
    my ($self, $arch, $type, $distfilessite) = @_;

    system "get_url \"$distfilessite/releases/$arch/autobuilds/latest-$type-$arch.txt\" -";
}

=head3

=cut

sub gentoo_get_latest_stage3 {
    my ($self, $arch, $destination, $distfilessite) = @_;
    #my $latest = $self->gentoo_get_latest($arch, "stage3| grep "stage3-${arch}")
    #my $stage3url = "$distfilessite/releases/$arch/autobuilds/$latest";
    #get_url ${stage3url} ${destination}
}

=head3

=cut

sub gentoo_get_latest_portage {
    my ($self, $destination, $distfilessite) = @_;
    #my $portageurl = "$distfilessite/snapshots/portage-latest.tar.bz2";
    #get_url $portageurl $destination;
}

=head3

=cut

sub gentoo_get_minimal_cd_image {
    my ($self, $distfilessite, $arch, $date) = @_;

    my $dir = $self->mktempdir;
    my $minimalcdisourl = "$distfilessite/releases/$arch/autobuilds/current-iso";
    my $minimalcdiso = "install-$arch-minimal-$date.iso";
    #cd ${dir}
    #wget "${minimalcdisourl}/${minimalcdiso}"
    #wget "${MINIMALCDISOURL}/${minimalcdiso}.DIGESTS.asc"
    #gpg --verify "${minimalcdiso}.DIGESTS.asc"
}

=head3

=cut

sub gentoo_verify_image {
    my ($self) = @_;
    system "gpg --verify portage-latest.tar.bz2.gpgsig portage-latest.tar.bz2";
    # md5sum -c portage-latest.tar.bz2.md5sum
}

=head3

=cut

sub gentoo_check_sum {
    my ($self, $arch, $date, $sumfile, $sumtype) = @_;

    #grep -A 1 "SHA512 HASH" install-${arch}-minimal-${date}.iso.DIGESTS.asc | grep "install-${arch}-minimal-${date}.iso$"  | sha512sum -c -
}

=head3

=cut

=head3

=cut

sub gentoo_dobug {
    my ($self) = @_;
    #DEFAULT_BUGDIR="/root/archtest"
    #local bugid="${1}"
    #local bugdir="${2}/${bugid}"

    #[[ -z ${bugid} ]] && echo "Need a bug number" && return
    #[[ -z ${2} ]] && bugdir=${DEFAULT_BUGDIR}/${bugid}

    #checkbug ${bugid}
    #exit
    #mkdir ${bugdir}
    #(cd ${bugdir} ; tatt -b ${bugid})

    #if [[ "${1}" =~ ^[[:digit:]]+$ ]] ; then
    #    bugid="${1}"
    #    bugdir=${2}/${bugid}

    #    logfile="$(mktemp)"
    #    package=$(findpackage "${1}")

    #    echo "Processing bug #${bugid}"
    #    echo "Found package ${package}"
    #    buildpackage ${bugid} ${package} ${logfile}
    #    step1 ${package}
    #    step2 ${logfile}
    #else
    #    echo "File"
    #fi
}

=head3

=cut

sub gentoo_checkbug {
    my ($self) = @_;
    #local bugid="${1}"

    #echo "1. Checking at sources.gentoo.org that package it's been in the tree at least for 1 month"
    #echo "2. Check that bug does not depend on other bugs."
    #deps=$(bug_has_dependencies ${bugid})
    #[[ -z ${deps} ]] && echo "The bud does not depend on others" || echo "Check bugs ${deps}"
    #echo "3. Check if there are other bugs filled against package"
}

=head3

=cut

sub gentoo_list_packages {
    my ($self) = @_;
    #find -maxdepth 2 -type d | sed -n -e '/.*\/.*\/.*$/p' | \
    #    egrep -v "/app-emulation/libdsk|/app-misc/dsktool" | sort
}

=head3

=cut

sub gentoo_check_rdepend {
    my ($self) = @_;
    #qlist -e "$@" | xargs scanelf -L -n -q -F '%n #F' | tr , ' ' | xargs qfile -Cv | sort -u | awk '{print $1}' | uniq
}

=head3

=cut

sub gentoo_check_dependencies {
    my ($self) = @_;
    #if [ $# -eq 0 ]; then
    #    echo "expects package atom as first argument" >&2;
    #    exit;
    #fi
    #find /var/db/pkg -regex ".*$(echo "$@" | sed 's@=@@g').*\\/NEEDED" | xargs cat
}

=head3

=cut

sub gentoo_compile_with_mixed_flags {
    my ($self, $package) = @_;
    #CFLAGS="-march=native -O2 -g0" CXXFLAGS="-march=native -O2" emerge -1 ${package}
}

=head3

=cut

sub gentoo_clear_portage  {
    my ($self) = @_;
    #truncate -s 0 /etc/portage/package.license /etc/portage/package.use/archtest  /etc/portage/package.keywords/archtest
    #emerge --depclean
    #revdep-rebuild -i -p
}

=head3

=cut

sub gentoo_find_package {
    my ($self, $bugid) = @_;
    #local tempdir="$(mktemp -d)"

    #(cd ${tempdir} ; tatt -b ${bugid} | \
    #    sed -n -e '/^Found the following/s/^.*: \(.*\)$/\1/p' )
    #rm -rf ${tempdir}
}

=head3

=cut

sub gentoo_pretendbuild {
    my ($self) = @_;
    #local script="${1}"
    #tempfile="$(mktemp)"

    #echo "Checking tatt build..."
    #run_as_root sh ${script} > ${tempfile} 2>&1
    #grep "USE changes" ${tempfile}
    #[[ ${?} -eq 0 ]] && echo "Use changes needed, please check file ${tempfile}" \
    #    "and rerun" && exit
}

=head3

=cut

sub gentoo_build_package {
    my ($self, $package, $script, $logfile) = @_;

    #echo "Building package ${package} with different USE flags"
    #run_as_root sh ${script} > ${logfile} 2>&1
    #echo "Package build log at: ${logfile}"
}

=head3

=cut

sub gentoo_git_portage {
    my ($self, $gitportagedir, $gitportageuser) = @_;

    $self->run_as_user($gitportageuser, "git -C ${gitportagedir} pull --rebase=preserve origin master");
}

=head3

=cut

sub gentoo_sync_news {
    my ($self, $gitportagedir, $gitportageuser) = @_;
    # git://gitweb.gentoo.org/data/gentoo-news.git
    my $gitnewsdir="${gitportagedir}/metadata/news";

    $self->run_as_user(${gitportageuser}, "git -C ${gitnewsdir} pull");
}

=head3

=cut

sub gentoo_portage {
    my ($self, $portagedir, $gitportagedir, $gitportageuser) = @_;
    my $jobs=4;
    my $timestampfile = "${portagedir}/metadata/timestamp.chk";

    #git_portage "${gitportagedir}" "${gitportageuser}"
    #echo "Pulling news..."
    #sync_gentoo_news "${gitportagedir}" "${gitportageuser}"
    #echo "Syncing portage dir..."
    #run_as_root rsync -C -rlpD -vzt --delete --exclude .gitignore --exclude metadata/ --exclude distfiles/ --exclude packages/ ${gitportagedir}/ ${portagedir}
    #echo "Running egencache..."
    #egencache --jobs=${jobs} --write-timestamp --update --repo=gentoo
    #echo "Syncing news..."
    #run_as_root rsync -CrlptDvz "${gitportagedir}/metadata/news/" "${portagedir}/metadata/news/"
    #echo "Updating metadata..."
    #emerge --metadata
    #echo "Runing eix-update..."
    #eix-update
    #dateformat "%a, %d %b %Y %H:%M:%S" > "${timestampfile}"
    #change_owner "${timestampfile}" "${gitportageuser}"
}

=head3

# tatt will use package.keywords/archtest
# so you can do e.g. tatt -b $bugid or tatt -f $files
# where $files is a file that contain the package
# tatt -b $bugid will generate $package-useflag.sh
# it will read what packages need to stabilization in bug 430566
# and will make some .sh files
# now you can do: sh $package-useflag.sh
# lauch it and you will compile the package with all possible useflag
# combination
# so, read package.report and no need to use tatt for the other stuff

# DON'T forget to block the tracker if exist
# PENDING:
# 1. Ask the leads to add me to the amd64 list of dev
# 2. In the meantime, I will prepare the cvs stuff/tips for the stabilization for you. so, not needed really to prepare, we can just do it in the next days

# TATT
# After installing tatt you need to save your package.keywords,
# if you use it as a file:

#cd /etc/portage
#mv package.accept_keywords temp
#mkdir package.accept_keywords
#mv temp package.accept_keywords/test

=cut

sub gentoo_step0 {
    my ($self) = @_;
    # Step 0. Build the package
    ####echo "Step 0: Build the package"

    ####local bugid="${1}"
    ####local package="${2}"
    ####local logfile="${3}"
    ####useflagsfile

    ####run_as_root tatt -b ${bugid}
    ####useflagsfile=$(ls *-useflags.sh)
    ####sed -i -e 's/emerge -1v/emerge -p1v/' ${useflagsfile}
    #####pretendbuild ${useflagsfile}
    ####sed -i -e 's/emerge -p1v/emerge -1v/' ${useflagsfile}
    ####buildpackage ${package} ${useflagsfile} ${logfile}
}

=head3

=cut

sub gentoo_step1 {
    my ($self) = @_;
    # Step 1: Check if the package needs static-libs IUSE and relative .la clean
    # Usually a library have .so extension...shared object, and the buildsystem
    # can build a static library (.a) extension and another file with .la
    # extension static-libs IUSE will no build and will no install .a + .la (not
    # needed in ~all of cases) best example:
    # https://bugs.gentoo.org/show_bug.cgi?id=405251
    #
    # This bug will not block the stabilization, but will improve the QA
    # To do this request you will have libfoo.a libfoo.so libfoo.la
    # if you have libfoo.a and libfoo.so but there isnt libfoo.la the request
    # still make sense
    # Does not make sense in cases like libfoo.so libpluto.la
    # qlist dev-python/numpy | grep libnpymath
    # only .a
    # .a+.la or .so
    # http://blog.flameeyes.eu/2008/04/what-about-those-la-files
    #####local package="${1}"

    #####echo "Step 1: Check if package needs static-libs IUSE and relative .la cleanup"

    #####qlist -e ${package} |grep "\.a"
    #####qlist ${package} |grep "\.so"
    #####qlist ${package} |grep "\.la"
}

=head3

=cut

sub gentoo_step2 {
    my ($self) = @_;
    # Step 2: Check if there are QA notice. Not all QA notice should be
    # reported
    # Check the build log
    # e.g.
    # Not report:
    # QA Notice: Package triggers severe warnings which indicate that it may
    # exhibit random runtime failures.
    # Block and report:
    # QA Notice: TEXTREL LDFLAGS SONAME and so on
    # QA Notice: "maintainer mode detected"
    # In all cases follow this
    # http://blogs.gentoo.org/ago/2012/08/22/when-you-should-block-a-stabilization/
    # No regression = no block,  but separate bug, never as a comment
    #####local logfile="${1}"

    #####echo "Step 2: Check for QA notices."

    #####grep -i "QA Notice" ${logfile}
}

=head3

=cut

sub gentoo_step3 {
    my ($self) = @_;
    # Step 3: Check Overflow
    # Check as QA notice
    #######grep "overflow\ destination" $( find . -iname "*.log" )
    # EXAMPLE: https://432500.bugs.gentoo.org/attachment.cgi?id=322044
    #          search destination
    #          https://bugs.gentoo.org/show_bug.cgi?id=434198
    #          in this case, 0.11 was ok and 0.11-r1 is affected, a gentoo patch
    #          causes it.
    # TRACKER: https://bugs.gentoo.org/show_bug.cgi?id=fortify-source
    # so, -D_FORTIFY_SOURCE shows this warning, there should be an overflow
    # if is a regression, block, otherwise go ahead
}

=head3

=cut

sub gentoo_step4 {
    my ($self) = @_;
    # Step 4: Check the improvement in the ebuild
    # At the end of merge, automatically I do a less $ebuild and I give it a rapid
    # check
}

=head3

=cut

sub gentoo_step5 {
    my ($self) = @_;
    # Step 5: Check in the ebuild the missing die or direct use of configure/make
    # All ebuild helpers need || die especially emake
    # less /usr/lib64/portage/bin/ebuild-helpers/emake
    # All external binary, needs die (sed rm grep find mv)
    # If the sed syntax is wrong, will not die also if there is a die
    # so, sed fails only if the file is missing
    # e.g.
    # sed "s:hello:hola" salut.py
    # if salut.py is missing and there is || die, the ebuild dies otherwise it will
    # proceed and is the reason because I'd prefer patch
    # If a function comes from an eclass, usually don't need of die will die itself

    # extra_emake and extra_econf should not be used in ebuilds
    # BLOCKS: NO
    # https://bugs.gentoo.org/show_bug.cgi?id=433818#c1
}

=head3

=cut

sub gentoo_step6 {
    my ($self) = @_;
    # Step 6: Check RDEPEND
    #######qlist -e "$@" | xargs scanelf -L -n -q -F '%n #F' | tr , ' ' | xargs qfile -Cv | sort -u | awk '{print $1}' | uniq

    # Alternative:
    #!/bin/sh
    #######if [ $# -eq 0 ]; then
    #######echo "expects package atom as first argument" >&2; exit;
    #######fi
    #######find /var/db/pkg -regex ".*$(echo "$@" | sed 's@=@@g').*\\/NEEDED" | xargs cat
    # NOTE:
    # scanelf will work for c/c++ program
    # For the others like ruby python, don't care about it
    # e.g. :
    # RDEPEND in the ebuild are: A, B
    # A pulls in C
    # The ebuild has RDEPEND on A,B,C
    # you have apache2
    # and for work, it uses e.g. libxml2
    # you have a webapplication that pulls in apache2 but the webapp strctly
    # need of libxml2
    # you say, nah, I don't declare libxml2 because it is pulled in by apache2

    # BLOCKS: NO
    # A message in the bug to say, missing this as RDEPEND, do whatever you want
    # this is not a really bug, is an improvement for the future
}

=head3

=cut

sub gentoo_step7 {
    my ($self) = @_;
    # Step 7: Check repoman warnings
    # BLOCKS: NO
    # Just leave a comment as a reminder
    # repoman warning about -j1 are useless
}

=head3

=cut

sub gentoo_step8 {
    my ($self) = @_;
    # Step 8: Check cflags respecting and overwriting
    # EXAMPLE: https://bugs.gentoo.org/show_bug.cgi?id=426796
    #          https://bugs.gentoo.org/show_bug.cgi?id=406573
    #          https://bugs.gentoo.org/show_bug.cgi?id=419413
    # it's better to know which flags are bad, and generally bad
    # -g/-ggdb should be dropped in any case
    # -O after user cflags is bad due to overwriting
    # -Werror is bad. https://bugs.gentoo.org/show_bug.cgi?id=451392
    # -pipe is bad only with a big package (if I have 128mb of ram, is bad for me)
    # and I just added -Wall because it was between all :P
    # the main goal there, was drop -O2 and -g1
    # -pipe/-Wall are not bad
}

=head3

=cut

sub gentoo_step9 {
    my ($self) = @_;
    # Step 9: Check if CFLAGS and CXXFLAGS are ok
    # CC foo.c $CXXFLAGS or CXX foo.cpp $CFLAGS
    # USE: CFLAGS="-march=native -O2 -g0" CXXFLAGS="-march=native -O2"

    # BLOCKS: yes/no ????
    # File a bug
}

=head3

=cut

sub gentoo_step10 {
    my ($self) = @_;
    # Step 10: Check compiler respect
    # Never use:
    # gcc foo.c
    # cc foo.c
    # ar foo.c
    # g++ foo.cpp
    # /usr/bin/gcc foo.c
    # /usr/bin/g++ foo.cpp
    # BLOCKS: yes
    # Block the tracker
    # TRACKER: https://bugs.gentoo.org/show_bug.cgi?id=cc-directly
}

=head3

=cut

sub gentoo_step11 {
    my ($self) = @_;
    # Step 11: Check the build log is verbose
    # EXAMPLE: https://bugs.gentoo.org/show_bug.cgi?id=430578

    # BLOCKS: no
    # Report, file new bug and block tracker.
    # TRACKER: https://bugs.gentoo.org/show_bug.cgi?id=429308
}

=head3

=cut

sub gentoo_step12 {
    my ($self) = @_;
    # Step 12: Check if the init script works and is clean of deprecation
    # warnings
    # Start them
    # If there are a complicate configuration skip them
    # In much cases you need only to move $config.example to .config
    # If does not work check the latest stable, to be sure is a default
    # behaviour

    # TRACKER: https://bugs.gentoo.org/show_bug.cgi?id=381895
    # https://bugs.gentoo.org/show_bug.cgi?id=405531#c0
    # https://bugs.gentoo.org/show_bug.cgi?id=377843
    # BLOCKS: NO

    #qlist -e ${package} | grep init.d
}

=head3

=cut

sub gentoo_step13 {
    my ($self) = @_;
    # Step 13: Check if the binary works / Runtime deps for libraries
    # If you see strange behaviour, don't care at all, check always the last
    # stable and see if there are no difference
    # If anything does not work, the first thing to do is ask another AT or DEV
    # to double check
    #qlist -e ${package} | grep bin
    # In case of a library:
    # echo "=category/package-version" > in
    # reverse-dependencies -i in -o out
}

=head3

=cut

sub gentoo_gdp_checkspell {
    my ($self) = @_;
    #sed -e 's/<.*>//g' ${1} | LC_ALL="es_ES.UTF8" aspell list | sort | uniq
}

