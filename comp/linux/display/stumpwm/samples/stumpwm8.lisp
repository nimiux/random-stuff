;;;; -*- Mode: Lisp -*-

;; 20130620 (WBZ) This version of my .stumpwmrc is the culmination
;; of several days of study, experimentation, and asking of help
;; from others. (Beginning on 20130609.)
;;
;; This file is called "~/bin/stump/my.stumpwmrc" and is symbolically
;; linked as follows:
;;
;;   $ cd; ln -s ~/bin/stump/my.stumpwmrc .stumpwmrc
;;
;; All files in the "~/bin/stump" directory are:
;;
;;   all_notes.org - My study notes on this project.
;;   my.desktop ---- Saved with "dump-desktop-to-file."
;;   my.menu ------- Loaded by this file, below.
;;   my.stumpwmrc -- ***This file***, symlinked from ~/.stumpwmrc.
;;   my.windows ---- Saved with "dump-window-placement-rules."

(in-package :stumpwm)

;; Set up the root window characteristics.

(run-shell-command "xsetroot -cursor_name left_ptr -solid black -name root-window")

;; I need CONKY!!! :)

(run-shell-command "/usr/bin/conky")

;; Set the prefix key to something for both hands.

(set-prefix-key (kbd "C-i"))

;; I like at least 10 seconds for messages.

(setf *timeout-wait* 10)

;; Startup message: display the machine's name.

(setf *startup-message* (machine-instance))

;; I like messages to be centered on the screen.

(setf *message-window-gravity* :center)
(setf *input-window-gravity* :center)

;; I thought that this mode-line was fabulous!

(defvar *battery-status-command*
  "acpi -b | awk -F '[ ,]' '{printf \"%s%s\", $3, $5}' | sed s/Discharging/\-/ | sed s/Unknown// | sed s/Full// | sed s/Charging/+/")

(defvar *vol-status-command*
  "amixer get Master | grep [[:digit:]]\\+%' -o | tr -d '\\n'")

(setf *screen-mode-line-format*
      (list "[^B%n^b] %W^>"
      '(:eval (run-shell-command *battery-status-command* t))
      " | Vol. "
      '(:eval (run-shell-command *vol-status-command* t))
      " | %d"))

(setf *window-format* "%m%n%s%c")

(setf *mode-line-timeout* 1)

;; Turn on the new mode line.

(toggle-mode-line (current-screen)
        (current-head))

;; Factor this xterm-substitute someday.

(define-key *root-map* (kbd "c") 
  "exec urxvt +sb -fn \"xft:Bitstream Vera Sans Mono:pixelsize=20\"")

;; Set the mouse focus.

(setf *mouse-focus-policy* :click)

;; Define the volume control and mute keys.

(define-key *top-map* (kbd "XF86AudioLowerVolume") "exec amixer set Master 5%-")
(define-key *top-map* (kbd "XF86AudioRaiseVolume") "exec amixer set Master 5%+")
(define-key *top-map* (kbd "XF86AudioMute") "exec amixer set Master toggle")

;; Use this command to see window properties; needed by the
;; (define-frame-preference ...) functions, below.

(define-key *root-map* (kbd "I") "show-window-properties")

;; An excellent application menu tool.

(defcommand mymenu () ()
  (labels ((pick (options)
             (let ((selection (stumpwm::select-from-menu (current-screen) options "")))
               (cond
                 ((null selection)
                  (throw 'stumpwm::error "Abort."))
                 ((stringp (second selection))
                  (second selection))
                 (t
                  (pick (cdr selection)))))))
    (let ((choice (pick *app-menu*)))
      (run-shell-command choice))))

;; Load up my custom menu.

(load "~/bin/stump/my.menu")

;; Finally, my preferred setup is four "groups" or "workspaces"
;; named F1, F2, F3, and F4. Switching among them is as simple
;; as "C-i Fn" since they are self-documenting.

;; When running on my main workstation, I like "F1" to automatically
;; start up with Totem at the top left, Emacs at the top right, and
;; Firefox at the bottom. When running on my other computers, don't
;; pre-start any of them.

;; Notice that the previously-saved "my.desktop" and "my.windows"
;; files are simply recalled from files to define where to place
;; the startup programs.

(defun mywm ()
  (run-commands "grename F1")
  (restore-from-file "~/bin/stump/my.desktop")
  (run-commands "gselect 1")
  (restore-window-placement-rules "~/bin/stump/my.windows")
  (define-frame-preference "F1"
    ;; frame raise lock (lock AND raise == jumpto)
    (0 t t :class "Totem")
    (1 t t :class "Firefox")
    (2 t t :title "emacs"))
  (run-commands "exec totem" "emacs" "exec firefox"))

;; Run it at startup -IF- running on the "ztx" machine.
;; (All my other machines have displays that are too small.)

(defun run-ztx ()
  (if (string= (machine-instance) "ztx [127.0.1.1]")
    (mywm)
    ;; else do this...
    (run-commands "grename F1" "gnewbg F2" "gnewbg F3" "gnewbg F4")))

(run-ztx)

------- my.menu file below --------

;;;; -*- Mode: Lisp -*-

;; My custom menu.

(defparameter *app-menu* '(("Audio/Video"
                            ("Audacity" "audacity")
                            ("Banshee" "banshee")
                            ("MVC" "mate-volume-control")
                            ("Totem" "totem"))
                           ("BookShelf"
                            ("ARM ARM" "evince ~/Library/Programming/Assembler/ARM/arm_arm.pdf")
                            ("Bible Study" "xiphos")
                            ("Handbook of Mathematics" "evince ~/Library/Mathematics/Handbook.pdf")
                            ("My Kindle Books" "google-chrome https://read.amazon.com/")
                            ("On LISP" "evince ~/Library/Programming/LISP/onlisp.pdf"))
                           ("Ham Radio"
                            ("Xastir" "xastir"))                           
                           ("Internet"
                            ("Chrome" "google-chrome")
                            ("Firefox" "firefox")
                            ("Google Earth" "/opt/google/earth/free/google-earth")
                            ("Konqueror" "konqueror"))
                           ("Games"
                            ("GnuChess" "xboard"))
                           ("Graphics"
                            ("GIMP" "gimp"))
                           ("Mathematics"
                            ("Maxima" "wxmaxima")
                            ("Sage" "urxvt +sb -fn \"xft:Bitstream Vera Sans Mono:pixelsize=20\" -e /home/bzimmerly/src/sage-5.6/sage"))
                           ("Office Applications"
                            ("Emacs" "emacs")
                            ("Libre Office" "libreoffice"))
                           ("System Tools"
                            ("Ethereal" "gksu /usr/local/bin/ethereal")
                            ("Konqueror" "konqueror")
                            ("Network Connections" "nm-connection-editor")
                            ("VirtualBox" "VirtualBox")
                            )))

(define-key *root-map* (kbd ".") "mymenu")