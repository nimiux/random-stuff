;; to make sure firefox doesn't steal focus.
;; unfortunately it breaks other stuff with firefox, so we're disabling it for now.
;; on #stumpwm I was told the hook urgent-window-hook would be the answer.
;; (setf *deny-raise-request* nil)

;; Customize bars and modeline.
(setf *message-window-gravity* :bottom-right)
(setf *input-window-gravity* :center)

(define-key *root-map* (kbd "RET") "exec urxvtc")
(define-key *root-map* (kbd "C-RET") "exec urxvtc")
(define-key *root-map* (kbd "x") "exec rox")

(defcommand browser () ()
  "Run or switch to browser."
  (run-or-raise "conkeror" '(:class "Conkeror")))
(define-key *root-map* (kbd "f") "browser")

(defcommand systray () ()
  "Run or raise the systray"
  (run-or-raise "stalonetray" '(:class "systray")))

(defcommand irc () ()
  "Log in to my irc/twitter/emacs session"
  (switch-to-group (find-group (current-screen) "Group 2"))
  (run-shell-command "urxvtc -e /home/gert/ser.sh"))

(define-key *root-map* (kbd "t") "systray")

(defun get-group-by-number (number)
  "Returns the group that has number <number>"
  (let ((the-group (find number (screen-groups (current-screen)) :key 'group-number)))
    the-group))


;; make 9 groups by default on the background. 9 is enough for my daily use as I seldom have more
;; than 9 applications open at the same time.

;; TODO: extend this so pressing the key again will result in changing back to the group you came from

(defparameter *previous-group* (current-group))

(defcommand switch-group (groupnr) (:number)
  "Switches to the selected group, but toggles with the previous group if the current one is selected"
  (if (= (group-number (current-group)) groupnr)
      (gselect *previous-group*)
      (progn
	(setf *previous-group* (current-group))
	(gselect (get-group-by-number groupnr)))))

(dotimes (x 9)
  (define-key *top-map* (kbd (format nil "M-F~a" (1+ x))) (format nil "switch-group ~a" (1+ x))))

(define-key *top-map* (kbd "Scroll_Lock") "exec xmodmap ~/.Xmodmap")

;; (stumpwm:run-shell-command "xmodmap -e 'remove Lock = Caps_Lock'")
;; (stumpwm:run-shell-command "xmodmap -e 'keysym Caps_Lock = Control_L'")
;; (stumpwm:run-shell-command "xmodmap -e 'add Control = Control_L'")

(define-key *top-map* (kbd "C-KP_Divide") "exec amixer sset PCM 15-")
(define-key *top-map* (kbd "C-KP_Multiply") "exec amixer sset PCM 15+")

(define-key *top-map* (kbd "XF86AudioMute") "")
(define-key *top-map* (kbd "XF86AudioLowerVolume") "exec amixer sset PCM 15-")
(define-key *top-map* (kbd "XF86AudioRaiseVolume") "exec amixer sset PCM 15+")
(define-key *top-map* (kbd "XF86AudioPlay") "exec cmus-remote -u")
(define-key *top-map* (kbd "XF86AudioPrev") "exec cmus-remote -r")
(define-key *top-map* (kbd "XF86AudioNext") "exec cmus-remote -n")
(define-key *top-map* (kbd "XF86AudioStop") "exec cmus-remote -s")


(defcommand sf3 () ()
  "run sf3"
  (stumpwm:run-shell-command "~/sf3.sh"))

(define-key *input-map* (kbd "Terminate_Server") 'input-delete-backward-char)

(defun groupnames ()
       (mapcar 'group-name (screen-groups (current-screen))))

(dotimes (x 8)
  (gnewbg (concat "Group " (write-to-string (+ 2 x)))))

;; function to name the group after the first window.
;; not using this anymore since we want frame preference rules.
(defun name-group-after-first-window (currentgroup previousgroup)
  (let* ((window-list-prev (tile-group-windows previousgroup)))
    (if window-list-prev
	(let ((prev-group-name (tile-group-name previousgroup))
	      (prev-window-name (window-name (car (tile-group-windows previousgroup)))))
	  (when (not (string= prev-group-name prev-window-name))
	      (setf (tile-group-name previousgroup) prev-window-name)))
	(setf (tile-group-name previousgroup) (concat "Group " (write-to-string (tile-group-number previousgroup)))))))

;; hook the function to the focus-group. So when the focus goes to a group, we rename the previous group.
;; (add-hook *focus-group-hook* 'name-group-after-first-window)

;; adding a macro I wrote at work for executing shell functions.
(defmacro process-command ((command) &body body)
  "Sends the output of the command through the body line by line, using 'line' as the variable in which the output is dumped."
  `(let ((proc (sb-ext:run-program  (car (split-by-one-space ,command)) 
                                    (cdr (split-by-one-space ,command)) :output :stream)))
    (unwind-protect 
         (progn 
           (with-open-stream (files (sb-ext:process-output proc))
             (loop :for line = (read-line files nil nil) :while line
                :do (progn ,@body))))
      (sb-ext:process-close proc))))

(defun split-by-one-space (string)
  "Returns a list of substrings of string
divided by ONE space each.
Note: Two consecutive spaces will be seen as
if there were an empty string between them.
-- found this in the Common Lisp Cookbook."
  (loop for i = 0 then (1+ j)
     as j = (position #\Space string :start i)
     collect (subseq string i j)
     while j))

(defun msg-notify (fmt args)
  (let ((*executing-stumpwm-command* nil)
        (*message-window-gravity* :center))
    (message-no-timeout fmt args)))

(defcommand notify (msg) ((:rest "Notify: "))
  "Displays a notify message, does not timeout."
  (msg-notify "~a" msg))

;; EMACS

(defvar *es-win* nil
  "to hold the window called emacsclient")

(defun save-es-called-win ()
  (setf *es-win* (current-window))
  (switch-to-group (find-group (current-screen) "Group 9"))) ;; little hack to make it always work for me

(defun return-es-called-win (win)
  (let* ((group (window-group win))
	 (frame (window-frame win))
	 (old-frame (tile-group-current-frame group)))
    (frame-raise-window group frame win)
    (focus-all win)
    (unless (eq frame old-frame)
      (show-frame-indicator group))))

;; WINDOW PLACEMENT RULES

(clear-window-placement-rules)

(define-frame-preference "Group 8"
;; frame raise lock (lock AND raise == jumpto)
    (1 nil t :class "stalonetray")
    (0 nil t :class "Skype"))
(define-frame-preference "Group 4"
    (0 nil t :class "rdesktop")
    (0 nil t :class "jd-Main"))
(define-frame-preference "Default"
    (0 nil t :class "Firefox")
    (0 nil t :class "Shiretoko")
    (0 nil t :class "Conkeror"))
(define-frame-preference "Group 7"
    (0 nil t :class "SWT"))
(define-frame-preference "Group 3"
    (0 nil t :class "Thunderbird-bin")
    (0 nil t :class "Gwibber")
    (0 nil t :class "Sylpheed"))
(define-frame-preference "Group 9"
    (0 nil t :class "Emacs"))


;; COMPUTER SPECIFIC SETTINGS -- differs between work/home

(defparameter *hostname* "")
(process-command ("/bin/hostname")
  (setf *hostname* line))  ;; a little crude, but it does the job

(cond ((string= *hostname* "packard")
        (run-shell-command "stalonetray --window-type normal")
;;      (run-shell-command "gnome-keyring-daemon")
        (run-shell-command "nm-applet --sm-disable"))
      ((string= *hostname* "tachikoma")
;       (run-shell-command "wmsetbg -b black -e /home/gert/Desktop/vfr800-1440.jpg")
       (run-shell-command "/home/gert/win.sh"))
      ((string= *hostname* "nel")
       (run-shell-command "stalonetray --window-type normal")
;;       (run-shell-command "gnome-keyring-daemon")
       (run-shell-command "nm-applet --sm-disable")))
       
;; AUTOSTART APPLICATIONS
(browser)
(emacs)
;; (irc)
