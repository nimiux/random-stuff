
	


  XSteve's StumpWM page

After several years of using Ion2 and later Ion3 I finally switched to
StumpWM. The reasons, why I prefer StumpWM now are:

  * StumpWM is easier to learn (C-t C-h) offers a built-in command help
  * Since I am an Elisp hacker, I know (and like) lisp much better than lua
  * The mental model behind StumpWM is easier to grasp than the ion model
  * It took me two evenings to convert my ion setup to StumpWM


    Links to StumpWM ressources on the net

  * StumpWM Homepage <http://www.nongnu.org/stumpwm/>


    Allow to select StumpWM as session on Ubuntu

gksu nano /usr/share/xsessions/stumpwm.desktop


Paste this in:

[Desktop Entry]
Encoding=UTF-8
Type=XSession
Exec=stumpwm
TryExec=stumpwm
Name=StumpWM
Comment=Stump window manager


    Some parts of my ~/.stumpwmrc

Activate lisp mode when using emacs and declare the package name:

;; -*-lisp-*-
(in-package :stumpwm)


Load some packges from the git stumpwm contrib directory:

(load "/home/stefan/work/git/stumpwm/contrib/net.lisp")
(load "/home/stefan/work/git/stumpwm/contrib/cpu.lisp")


I set the prefix key to the menu key. This key is easier to type than
Ctrl-t:

;; Change the Stumpwm prefix key: Use the menu key
(set-prefix-key (kbd "Menu"))


Define a bunch of commands to switch to an already started application
or to start it if it is not yet running:

(defcommand emacs () ()
  "run emacs"
  (run-or-raise "emacs-snapshot -T emacs" '(:title "emacs")))

(defcommand firefox () ()
  "run firefox"
  (run-or-raise "firefox" '(:class "Firefox")))

(defcommand chromium () ()
  "run chromium"
  (run-or-raise "chromium-browser" '(:instance "chromium-browser")))

(defcommand aumix () ()
  "run aumix"
  (run-or-raise "xterm -name aumix -e aumix" '(:instance "aumix")))

(defcommand xterm-1 () ()
  "run an xterm instance"
  (run-or-raise "xterm -name xterm1 -e 'cd /data/txt && zsh'" '(:instance "xterm1")))

(defcommand xterm-2 () ()
  "run an xterm instance"
  (run-or-raise "xterm -name xterm2" '(:instance "xterm2")))


Turn on the modeline

;; Turn on the modeline
(if (not (head-mode-line (current-head)))
     (toggle-mode-line (current-screen) (current-head)))


;; Show time, cpu usage and network traffic in the modeline
(setf *screen-mode-line-format*
      (list '(:eval (run-shell-command "date '+%R, %F %a'|tr -d [:cntrl:]" t)) " | %t | %c| %l | [^B%n^b] %W"))


Select a random background image from a given folder

;; Define the background window
(defvar *background-image-path* "/home/stefan/background-images/")
(defun select-random-background-image ()
  "Select a random image"
  (let ((file-list (directory (concatenate 'string *background-image-path* "*.jpg")))
        (*random-state* (make-random-state t)))
    (namestring (nth (random (length file-list)) file-list))))

(run-shell-command (concatenate 'string "display -window root " (select-random-background-image)))


Define some toplevel keybindings. These bindings don't require the
prefix key. I use them to switch between my open applications:

(define-key *top-map* (kbd "M-3") "firefox")
(define-key *top-map* (kbd "M-4") "emacs")
(define-key *top-map* (kbd "M-5") "xterm-1")
(define-key *top-map* (kbd "M-6") "xterm-2")
(define-key *top-map* (kbd "M-0") "aumix")
(define-key *top-map* (kbd "M-F9") "loadrc")
(define-key *top-map* (kbd "Pause") "gother")
(define-key *top-map* (kbd "C-Pause") "grouplist")


Some keybindings for the defined prefix key:

(define-key *root-map* (kbd "DEL") "repack-window-numbers")
(define-key *root-map* (kbd "I") "show-window-properties")


Set the mouse policy to focus follows mouse;

(setf *mouse-focus-policy* :sloppy) ;; :click, :ignore, :sloppy


Create some new virtual desktops (called groups in stumpwm):

(run-commands "gnewbg Web" "gnewbg Media")


My window placement rules:

;; Clear rules
(clear-window-placement-rules)

(define-frame-preference "Default"
  ;; frame raise lock (lock AND raise == jumpto)
  (0 t   t :title "emacs")
  (0 t   t :class "XTerm"))

(define-frame-preference "Web"
  ;; frame raise lock (lock AND raise == jumpto)
  (0 t   t :class "Firefox"))

(define-frame-preference "Media"
  ;; frame raise lock (lock AND raise == jumpto)
  (0 t   t :instance "aumix")
  (0 t   t :class "MPlayer")
  (0 t   t :class "Avidemux"))




Back to my Programming Homepage <../index.html>


------------------------------------------------------------------------
Last modified: Fri Sep 2 23:59:44 CEST 2011
