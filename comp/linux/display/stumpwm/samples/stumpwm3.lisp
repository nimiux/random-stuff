
;; utility commands

(defun shell-command (command) 
          (check-type command string)
          (echo-string (current-screen) (run-shell-command command t)))
   
(define-stumpwm-command "shell-command" ((command :string "sh: " :string))
   (check-type command string)
    (shell-command command))
   
 ;;Ask user for a search string and search Google for it.
  (define-stumpwm-command "google" ((search-string :string "google " :string))
    (check-type search-string string)
    (run-shell-command (cat "surfraw google " search-string)))

