\contentsline {section}{\numberline {1}Introduction}{1}
\contentsline {subsection}{\numberline {1.1}\LaTeXe ---The new \LaTeX \nobreakspace {}release}{1}
\contentsline {subsection}{\numberline {1.2}\LaTeX 3---The long-term future of \LaTeX }{2}
\contentsline {subsection}{\numberline {1.3}Overview}{2}
\contentsline {subsection}{\numberline {1.4}Further information}{3}
\contentsline {section}{\numberline {2}Classes and packages}{3}
\contentsline {subsection}{\numberline {2.1}What are classes and packages?}{4}
\contentsline {subsection}{\numberline {2.2}Class and package options}{5}
\contentsline {subsection}{\numberline {2.3}Standard classes}{5}
\contentsline {subsection}{\numberline {2.4}Standard packages}{6}
\contentsline {subsection}{\numberline {2.5}Related software}{7}
\contentsline {subsubsection}{\numberline {2.5.1}Tools}{8}
\contentsline {section}{\numberline {3}Commands}{9}
\contentsline {subsection}{\numberline {3.1}Initial commands}{9}
\contentsline {subsection}{\numberline {3.2}Preamble commands}{9}
\contentsline {subsection}{\numberline {3.3}Document structure}{11}
\contentsline {subsection}{\numberline {3.4}Definitions}{11}
\contentsline {subsection}{\numberline {3.5}Boxes}{13}
\contentsline {subsection}{\numberline {3.6}Measuring things}{15}
\contentsline {subsection}{\numberline {3.7}Line endings}{15}
\contentsline {subsection}{\numberline {3.8}Controlling page breaks}{15}
\contentsline {subsection}{\numberline {3.9}Floats}{16}
\contentsline {subsection}{\numberline {3.10}Font changing: text}{16}
\contentsline {subsection}{\numberline {3.11}Font changing: math}{17}
\contentsline {subsection}{\numberline {3.12}Ensuring math mode}{18}
\contentsline {subsection}{\numberline {3.13}Setting text superscripts}{18}
\contentsline {subsection}{\numberline {3.14}Text commands: all encodings}{18}
\contentsline {subsection}{\numberline {3.15}Text commands: the T1 encoding}{21}
\contentsline {subsection}{\numberline {3.16}Logos}{22}
\contentsline {subsection}{\numberline {3.17}Picture commands}{22}
\contentsline {subsection}{\numberline {3.18}Old commands}{22}
\contentsline {section}{\numberline {4}\LaTeX \nobreakspace {}2.09 documents}{23}
\contentsline {subsection}{\numberline {4.1}Warning}{23}
\contentsline {subsection}{\numberline {4.2}Font selection problems}{24}
\contentsline {subsection}{\numberline {4.3}Native mode}{25}
\contentsline {section}{\numberline {5}Local modifications}{25}
\contentsline {section}{\numberline {6}Problems}{26}
\contentsline {subsection}{\numberline {6.1}New error messages}{26}
\contentsline {subsection}{\numberline {6.2}Old internal commands}{27}
\contentsline {subsection}{\numberline {6.3}Old files}{28}
\contentsline {subsection}{\numberline {6.4}Where to go for more help}{29}
\contentsline {section}{\numberline {7}Enjoy!}{29}
