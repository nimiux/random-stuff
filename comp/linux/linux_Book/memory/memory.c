#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

#define MB (1024 * 1024)
#define PHY_MEM_MB 512 

int main () {

   char *some_memory;
   size_t size_to_allocate = MB;
   int mb_obtained = 0;

   while (mb_obtained < (PHY_MEM_MB * 2)) {
      some_memory = (char *) malloc(size_to_allocate);
      if (some_memory != NULL) {
	 mb_obtained++;
	 sprintf(some_memory, "Hello world");
	 printf("%s - now allocated %d MB\n", some_memory, mb_obtained);

      } else {
	 exit (EXIT_FAILURE);
      }
      sleep(2);
   }
   exit (EXIT_SUCCESS);
}

