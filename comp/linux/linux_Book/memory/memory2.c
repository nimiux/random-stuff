#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

#define KB (1024)

int main () {

   char *some_memory;
   int size_to_allocate = KB;
   int mb_obtained = 0;
   int kb_obtained = 0;

   while (1) {
      for (kb_obtained = 0; kb_obtained < 1024; kb_obtained++) {
	 some_memory = (char *) malloc(size_to_allocate);
	 if (some_memory == NULL) {
	    exit(EXIT_FAILURE);
	 }
	 sprintf(some_memory, "Hello world");

      }
      mb_obtained++;
      printf("now allocated %d MB\n", mb_obtained);
      sleep(1);
   }
   exit (EXIT_SUCCESS);
}

