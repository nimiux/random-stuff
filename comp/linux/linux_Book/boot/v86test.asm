; v86test.asm
; Version 2.0, Mar 17, 1998
; Sample code
; by John S. Fine  johnfine@erols.com
; I do not place any restrictions on your use of this source code
; I do not provide any warranty of the correctness of this source code
;_____________________________________________________________________________
;
; This is the main module of a demonstration of the code needed to manage a
; v86 session from within a pmode kernel.
;
; This program must be loaded before the real-mode OS, using my bootp package
; or something similar.  This program then reads in and starts the MBR, which
; hopefully starts a real-mode OS.  The real-mode OS is given the maximum
; priveledges that are possible within v86 (this is a coding sample, not a
; production level module).  The real-mode OS could easily crash us in many
; ways (such as disabling A20).  A few odd tweaks are included here so that
; DOS's config.sys can contain a call to HIMEM.SYS or QEMM.SYS and those will
; fail BEFORE they test A20 and crash us.  Thus DOS will come up after
; complaining that it can't load HIMEM.
;_____________________________________________________________________________
;
; This package must be assembled with NASM.
;
; The v86.inc used in this package uses the token merging feature that I added
; to NASM version 0.97j.  It will not assemble correctly in NASM version 0.97
; or earlier.  I don't know whether Simon Tatham will include token merging in
; NASM version 0.98.
;
; This package also must be linked with my linker JLOC version 0.5 or later
;
; NASM -f obj v86test.asm
; NASM -f obj 8259.asm
; NASM -f obj idt.asm
; NASM -f obj dump.asm
; JLOC v86test.lnk test.bin v86test.map
;
; To run this using bootp, TEST.BIN must be moved to the root directory of
; your active partition (see the documentation for bootp).
;_____________________________________________________________________________

	%include "v86.inc"
	%include "gdt.inc"
	%include "idt.inc"
	%include "8259.inc"

	SEGMENT	START  USE32
	SEGMENT	CODE   USE32
	SEGMENT CONST  USE32 align=4
	SEGMENT DATA   USE32 align=4
	SEGMENT BSS    USE32 align=4
	SEGMENT PAGE   USE32 align=4096
	SEGMENT V86    USE16

	extern	init_8259	;Initialize the interrupt controller
	extern	init_idt	;Initialize the IDT
	extern	idt		;The IDT itself
	extern	UNMAPPED_PAGE4IDT  ;Explained in idt.asm

; We get here straight from bootp.  We have a temporary GDT from bootp and
; temporary page tables and a temporary stack and no TSS and no IDT.
;_____________________________________________________________________________
SEGMENT	START
	lgdt	[gdt]				;Load GDT
	lss	esp, [esp0]			;Initialize stack
	push	dword 0x3002			;Initial flags value
	popf					;Fix nested task glitch
	mov	ax, TSS				;Load TSS selector
	ltr	ax
	call	init_idt			;Load IDTR
	in	al, M_IMR			;Save interrupt mask registers
	push	eax
	in	al, S_IMR
	push	eax
	call	init_8259			;Init interrupt controller
	pop	eax				;Restore interrupt mask regs
	out	S_IMR, al
	pop	eax
	out	M_IMR, al
	call	init_page			;Move page tables out of first meg

	mov	eax, v86_15+0xFFFF0000		;cs:ip for our v86 mode int 15h
	xchg	eax, [0x15*4]			; intercept.
	mov	[old_15+0xFFFF0], eax		;BIOS's int 15h address

; Now we just switch to V86 mode in a way that will load and run the MBR
; Set up V86 stack frame to call INT 13h and have it return to 0:7C00
;_____________________________________________________________________________

	xor	ebp, ebp
	push	ebp				;gs
	push	ebp				;fs
	push	ebp				;ds
	push	ebp				;es
	push	ebp				;ss
	mov	ebx, 0x7C00-6			;sp
	push	ebx
	mov	dword [ebx], 0x7C00		;return cs:ip
	mov	word [ebx+4], 0x3002		;return flags
	add	ebx, 6				;buffer	address
	push	dword 0x23002			;flags

	mov	eax, [0x13*4]			;Vector for disk I/O
	rol	eax, 16
	push	eax				;cs
	shr	eax, 16
	push	eax				;ip

	call	announce			;Demonstrate v86_call

	mov	ax, 0x201			;read one sector
	mov	ecx, 1				;sector 1
	mov	edx, 0x80			;hard drive 0, head 0
	iret					;Go

msg:	db	"Now loading Master Boot Record . . .", 0
;
;  There are better ways to put a message up, but I wanted to demo the
;  use of v86_call
;_____________________________________________________________________________

announce:
	mov	esi, msg			;Point at message
	lodsb					;Get first byte
.loop:	push	esi				;Save our own esi
	mov	ah, 0xE				;Function 0E

	V86_CALL 10h				;Do it.

; We are back from int 10h.  We don't care about any of its results
; so we just dump them.
;_____________________________________________________________________________

	add	esp, 12*4			;Discard the entire frame
	pop	esi				;Restore
	lodsb					;Next byte
	test	al, al				;Done?
	jne	.loop				;Not yet
	ret					;Done.

	SEGMENT	CODE
;
; Here is the GPF handler.  We find the code that caused the GPF and
; look it up in a table of things we emulate, then jmp to the routine
; that does the emulation.  If we don't match the instruction we go
; (via a page fault) to the stack dump routine.
;_____________________________________________________________________________
service_0D:
patch_vec 0xD, service_0D, D_TRAP		;Patch vector to point here

	regs	1, ebx, ecx, ebp		;set up stack
%assign c_eax c_eip-4
	mov	[c_eax+ebp], eax		;Overlay error code

	push	ss
	pop	ds

	movzx	ebx, word [c_cs+ebp]		;Segment of faulting instr
	shl	ebx, 4
	add	ebx, [c_eip+ebp]		;Linear address
	mov	ecx, tbl_0D			;Table of instructions
	mov	ebx, [ebx]			;Faulting instr
.loop:	mov	eax, [ecx]			;Mask
	and	eax, ebx
	cmp	eax, [ecx+4]			;Check masked result
	lea	ecx, [ecx+12]
	jne	.loop				;Not matched yet
	jmp	[ecx-4]				;Matched: Go do it

[SEGMENT CONST]
	align	4
tbl_0D	dd	     0xFF,       0xCD,   sw_int
	dd	     0xFF,       0xCC,   sw_int3
	dd	        0,          0,   fail_0D
__SECT__

; Here we handle software interrupts from v86 mode.  Any software interrupt
; that might be called from pmode ought to have its own direct service
; routine.
;_____________________________________________________________________________

sw_int3:
	mov	bh, 3				;Vector number
	dec	word [c_eip+ebp]		;Instruction was short

sw_int:
	movzx	ebx, bh				;Interrupt number
	movzx	ecx, word [c_ss+ebp]		;Caller's SS
	shl	ecx, 4
	sub	word [c_esp+ebp], 6		;Adjust Caller's SP
	mov	eax, [ebx*4]			;Get vector
	add	ecx, [c_esp+ebp]		;Point at caller's stack
	xchg	ax, [c_eip+ebp]			;Caller's IP
	add	ax, 2				;Bump it past "INT nn" 
	rol	eax, 10h			;Reverse halves of eax
	xchg	ax, [c_cs+ebp]			;Caller's CS
	rol	eax, 10h			;Reverse halves of eax
	mov	[ecx], eax			;Store in caller's stack
	mov	eax, [c_flags+ebp]		;Caller's flags
	mov	[ecx+4], ax			;Store in caller's stack
	and	ah, 0FDh			;Clear interrupt enable
	mov	[c_flags+1+ebp], ah		;Update new flags
	pull	eax, ebx, ecx, ebp		;Restore registers
	iret					;Execute interrupt


fail_0D:
	pull	eax, ebx, ecx, ebp		;Restore registers
	jmp	UNMAPPED_PAGE4IDT+0xD		;Stack dump

; Here we hand off support for every hardware interrupt to V86 mode.  In
; 8259.asm and 8259.inc we set up the 8259 to send interrupts to our
; vectors 0x68 through 0x77.  Here we redispatch them to the v86 vectors
; 8 to 0xF and 0x70 to 0x77.
;_____________________________________________________________________________
	hw_revector	68, 8
	hw_revector	69, 9
	hw_revector	6A, 10
	hw_revector	6B, 11
	hw_revector	6C, 12
	hw_revector	6D, 13
	hw_revector	6E, 14
	hw_revector	6F, 15
	hw_revector	70, 0x70
	hw_revector	71, 0x71
	hw_revector	72, 0x72
	hw_revector	73, 0x73
	hw_revector	74, 0x74
	hw_revector	75, 0x75
	hw_revector	76, 0x76
	hw_revector	77, 0x77

; The hw_revector macro saves eax, then sets eax to the contents of the
; desired vector, then JMPs here to do the real work of revectoring.
;
; If the interrupt occurred while the CPU was in V86 mode, we can redispatch
; it the same way we handle sw interrupts from v86 mode.
;
; If the interrupt occurred while the CPU was in pmode, it is a slightly
; stickier problem.
;_____________________________________________________________________________
service_hw:
	regs	1, ebx, ebp
	push	ss
	pop	ds

	test	byte [c_flags+2+ebp], 2		;Interrupted V86?
	jz	.prot				;No interrupted kernel
	movzx	ebx, word [c_ss+ebp]		;ss
	shl	ebx, 4
	sub	word [c_esp+ebp], 6		;Make room on v86 stack
	add	ebx, [c_esp+ebp]		;Linear address
	xchg	ax, [c_eip+ebp]			;ip
	rol	eax, 16				;Swap halves of eax
	xchg	ax, [c_cs+ebp]			;cs
	rol	eax, 16
	mov	[ebx], eax
	mov	eax, [c_flags+ebp]		;Flags
	mov	[ebx+4], ax			;Saved flags
	and	byte [c_flags+1+ebp], 0xFD	;CLI
	pull	eax, ebx, ebp
	iret

.prot:	mov	ebx, [esp0]			;Get existing V86 frame limit
	push	ebx				;Save V86 frame limit
	mov	[esp0], esp			;Set new V86 frame limit
	push	eax				;Save vector (random gs)
	sub	esp, byte 12			;Random ds,es and fs
	movzx	eax, word [c_esp+ebx-v_lim]	;Get V86 sp
	movzx	ebx, word [c_ss+ebx-v_lim]	;Get V86 ss
	sub	eax, byte 6			;Make room on V86 stack
	push	ebx				;New V86 frame: ss
	push	eax				;New V86 frame: sp
	shl	ebx, 4				;V86 stack segment base

	mov	dword [eax+ebx+2], 0x3002FFFF	;Create a return point for the
	mov	word [eax+ebx], unwind		;V86 routine at FFFF:unwind

	push	dword 0x23002			;New V86 frame: flags
	movzx	eax, word [ebp-6]		;High half of vector
	push	eax				;New V86 frame: cs
	movzx	eax, word [ebp-8]		;Low half of vector
	push	eax				;New V86 frame: ip
	iretd					; "CALL" V86 interrupt routine

; The V86 segment is code that must execute in V86 mode.  JLOC adjusts things
; so that the V86 code has a base of FFFF:0.
;_____________________________________________________________________________
SEGMENT V86

; There is no way to "return" from V86 mode to pmode.  When the kernel
; inverts a call to V86 mode (into an iret) it must give the V86 routine
; somewhere to return.  That return must still be in V86 mode.  That return
; must then interrupt into pmode to cause the real return.
;
; All that explanation for one little instruction . . .
;_____________________________________________________________________________
unwind:	int	41h				;Get out of V86 mode
SEGMENT CODE

; The above instruction will bring us in here to clean up the stack and
; really return.
;_____________________________________________________________________________
service_41:
patch_vec 0x41, service_41, D_INT+D_DPL3	;Patch vector to point here
	mov	eax, ss				;Setup kernel seg registers
	mov	ds, eax
	mov	es, eax
	add	esp, 9*4			;Discard current V86 frame
	pop	dword [esp0]			;Restore old V86 frame limit
	pull	eax, ebx, ebp			;Restore registers
	iret					;Done

; In order to convince various V86 mode code to avoid stepping on things we
; left unprotected, we intercept int 15h within V86 mode just before the
; BIOS gets it.  This code is table driven to add more tweaks later.  For
; now it just messes with function 88h.
;_____________________________________________________________________________
SEGMENT V86
v86_15:
	push	bp				;Save a registers
	movzx	bp, ah				;Function number
	bt	[cs:block_15], bp		;Do we mess with this one?
	jc	.block				;Yes
	pop	bp				;No: restore register
.old:	jmp	0:0				;JMP to BIOS
old_15 equ $-4

.block:	mov	bp, sp				;(for access to C-bit)
xxx:	cmp	ah, 88h				;Special case 88h?
	jne	.fail				;Otherwise just "fail" it

	xor	ax, ax				;Zero Kb of extended RAM
	and	byte [bp+6], -2			;Clear caller's C-bit
	pop	bp				;Restore
	iret					;Return

.fail	mov	ah, 86h				;Failure code
	or	byte [bp+6], 1			;Set caller's C-bit
	pop	bp				;Restore
	iret					;Return

block_15:
	dw	0x0000		;0x
	dw	0x0000		;1x
	dw	0x0000		;2x
	dw	0x0000		;3x
	dw	0x0000		;4x
	dw	0x0000		;5x
	dw	0x0000		;6x
	dw	0x0000		;7x
	dw	0x0100		;8x
	dw	0x0000		;9x
	dw	0x0000		;Ax
	dw	0x0000		;Bx
	dw	0x0000		;Cx
	dw	0x0000		;Dx
	dw	0x0000		;Ex
	dw	0x0000		;Fx

; v86_call is used to call an interrupt routine or far subroutine in V86 mode.
;
; Before calling v86_call the client must set up the stack frame as
; follows:  (Items are shown in the order they should be pushed)
;
; v_lim+4   Workspace
; v_lim     Workspace
; v_gs      V86 gs
; v_fs      V86 fs
; v_ds      V86 ds
; v_es      V86 es
; c_ss      V86 ebp
; c_esp     V86 esi
; c_flags   V86 flags
; c_cs      Workspace
; c_eip     Workspace
; ????      V86 eax
;
; Registers ebx,ecx,edx and edi should contain their V86 values
;
; Register esi should contain the V86 cs:ip
;
; On return from v86_call ebx,ecx,edx,esi,edi and ebp will have the values
; output (or left) by the V86 routine.  eax will be trash (the value output
; by the V86 routine will be on top of the stack)
;_____________________________________________________________________________

SEGMENT CODE
v86_call:
	regs	2				;Pretend this is an interrupt.
						;EAX and the subroutine return
						;address are the 2 extra dwords
	mov	eax, esp			;Use eax as frame pointer
	rol	esi, 16				;Swap halves
	mov	[c_cs+eax], esi			;Store cs
	shr	esi, 16				;Zero extended
	mov	[c_eip+eax], esi		;Store ip
	lea	esi, [v_lim+eax]		;Other end of interrupt frame
	pop	dword [esi]			;Save return address
	cli					;No ints while esp0 invalid
	xchg	esi, [esp0]			;Get old frame limit
						;Set new frame limit
	mov	[v_lim+4+eax], esi		;Save old frame limit
	movzx	ebp, word [c_ss+esi-v_lim]	;Get V86 ss
	movzx	esi, word [c_esp+esi-v_lim]	;Get V86 sp
	shl	ebp, 1				;(need ss*16 later)
	sub	esi, byte 6			;Make room on V86 stack
	mov	word [ebp*8+esi], unwind_sw	;Store return address
	mov	dword [ebp*8+esi+2], 0x3002FFFF ;Store return cs and flags
	shr	ebp, 1				;Restore V86 ss
	xchg	ebp, [c_ss+eax]			;Set ss, Get ebp
	xchg	esi, [c_esp+eax]		;Set sp, Get esi
	pop	eax
	iretd					; "CALL" V86 interrupt routine

; See "unwind:" for explanation
;_____________________________________________________________________________
SEGMENT V86
unwind_sw:	int	46h				;Get out of V86 mode
SEGMENT CODE

; The above instruction will bring us in here to clean up the stack and
; really return.
;_____________________________________________________________________________
service_46:
patch_vec 0x46, service_46, D_INT+D_DPL3	;Patch vector to point here
	regs	0, eax
	mov	eax, ss				;Set up segment registers
	mov	ds, eax
	mov	es, eax
	mov	eax, [v_lim+4+esp]		;Get prior esp0
	mov	[esp0], eax			;Restore it
	jmp	[v_lim+esp]			;Return
	
SEGMENT CONST
gdt		start_gdt
flat_code	desc	0,   0xFFBFF,    D_CODE+D_READ+D_BIG+D_BIG_LIM
flat_data	desc	0,   0xFFFFF,    D_DATA+D_WRITE+D_BIG+D_BIG_LIM
TSS		desc	tss, tss_size-1, D_TSS
		end_gdt

SEGMENT DATA
tss:    dd      0
esp0:   dd      stack0top
ss0:    dd      flat_data
	resb	0x66 - 0xC
	dw	0x68
; I/O restrictions
	resb	8192			;0 - FFFF unrestricted
tss_size equ $-tss

SEGMENT STACK
	align	4
	resb	1024
stack0top:

SEGMENT PAGE
; v86test.lnk sets the base for this segment so that offsets within this
; segment are physical addresses rather than linear addresses.  Because
; of the nitial double mapping, a physical address in the first 4Mb can be
; used as a linear address;  The normal linear address cannot be used in
; page tables.

ptbl0	resb	4096
ptbl1	resb	4096
pdir	resb	4096
plim:

; bootp creates page tables at 0x9C000
; Before we can start DOS we need to move them somewhere else.
SEGMENT START
init_page:
	mov	esi, 0x9C000			;bootp's page tables
	mov	edi, ptbl0			;Our page tables
	mov	ecx, 3*4096/4			;Three at 4Kb each
	rep movsd				;Copy them
	mov	dword [pdir], ptbl0+7		;Page 0 pointer in dir
	mov	dword [plim-8], ptbl1+7		;Kernel page pointer in dir
	mov	dword [plim-4], pdir+7		;Self pointer in dir
	mov	eax, pdir			;Page directory we just built
	mov	cr3, eax			;Use it
	mov	byte [ptbl0+1024+2], 0		;kludge to discourage HIMEM.SYS
	ret

	end
