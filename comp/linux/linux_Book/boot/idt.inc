; idt.inc  definitions for declaring idt descriptors
; Version 1.0, Jan 29, 1998
; Sample code
; by John S. Fine  johnfine@erols.com
; I do not place any restrictions on your use of this source code
; I do not provide any warranty of the correctness of this source code
;_____________________________________________________________________________

	extern	FLAT_CODE		;Code segment selector

; Define an empty group.  JLOC interprets any fixup relative to an empty
; group, based on the location of the target in the image file rather than
; its location in memory.  Image file locations are required for patching.
;_____________________________________________________________________________

	group	image

; patch_vec  vector_num, entry_point, descriptor_type
;
; Overwrites (during the last step of JLOC) the specified entry in the idt
; with a descriptor for the specified entry point.
;
; descriptor_type is usually D_INT or D_TRAP (The only difference is that
; D_INT clears the interrupt enable on entry).  You must also add D_DPL3 to
; either if you want them accessible via "int" instruction from user code.
;_____________________________________________________________________________

	%macro	patch_vec 3
[segment PATCH]
	dd	0			;JLOC patches require this zero
	dd	idt+(%1)*8 WRT image	;Location to patch
	dd	8			;Length of patch
	desc	%2, FLAT_CODE, %3
__SECT__
	%endmacro
