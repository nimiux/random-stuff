; v86.inc  Some macros to support my v86 kernel code
; Version 2.0, Mar 17, 1998
; Sample code
; by John S. Fine  johnfine@erols.com
; I do not place any restrictions on your use of this source code
; I do not provide any warranty of the correctness of this source code
;_____________________________________________________________________________
;
; This code uses the token merging feature that I added to NASM version 0.97j.
; It will not assemble correctly in NASM version 0.97 or earlier.  I don't
; know whether Simon Tatham will include token merging in NASM version 0.98.
;_____________________________________________________________________________

;   "pull"  pops a bunch of things in reverse order.  For example,
;
;        pull  eax, ebx, ebp
;   generates
;        pop   ebp
;        pop   ebx
;        pop   eax
;_____________________________________________________________________________
       %macro pull 1-*
       %rep %0 
       %rotate -1 
		pop %1
       %endrep 
       %endmacro

;   "regs"  sets up the stack on entry to an interrupt service routine.
;
;   regs count, list of registers
;
;   Assumes that "count" dwords have ALREADY been pushed on the stack since
; entering an interrupt service routine.  It pushes the additional registers.
; If ebp is the last register pushed then ebp is also set equal to the address
; of the stack frame.
;
; It assigns a bunch of symbols to describe where things are on the stack.
;
; Each of these symbols is an offset from ebp, for example [c_ebp+ebp] is
; the saved value of ebp.
;
; c_eip, c_cs and c_flags are values pushed by the interrupt.
; c_esp and c_ss are values that are only pushed by the interrupt if the
;       cpu was not in the kernel when it was interrupted.
; v_es, v_ds, v_fs and v_gs  are values that are only pushed if the cpu
;       was in V86 mode.  Note the "v_" prefixes.
; v_lim is the far end of a V86 stack frame (to help with address
;       arithmetic when reconstructing frames).  ebp+v_lim == [esp0] if
;       the interrupt was from V86 mode.
;
; If you push additional registers, it defines additional symbols, for
; example:
;
;   regs 0, eax, ebx, ds
;
; would define c_eax, c_ebx and c_ds to show where those are on the stack.
;
; Note that if the cpu is interrupted from V86 mode, the correct saved ds and
; es are in v_ds, and v_es;  Saving ds and es again is unnecessary and would
; define c_ds and c_es which would hold null selectors, not correct
; information.
;
; If the cpu is interrupted from kernel mode, there is no need to save ds and
; es, since the whole kernel (in my designs) uses a single ds, es value.
;
; If the cpu is interrupted fron non-kernel pmode, you must save ds and es
; before modifying them.  If all three modes are supported, you must save
; ds and es, then check the mode if you must decide whether to use c_ds vs.
; v_ds.
;_____________________________________________________________________________

       %macro regs 1-*
%assign c_ ((%0+%1-1)*4)
%assign c_eip   c_
%assign c_cs    c_+4
%assign c_flags c_+8
%assign c_esp   c_+12
%assign c_ss    c_+16
%assign v_es    c_+20
%assign v_ds    c_+24
%assign v_fs    c_+28
%assign v_gs    c_+32
%assign v_lim   c_+36

       %rep %0-1
       %rotate 1 
                push	%1 
%assign c_ c_ - 4
%assign c_%1 c_
       %endrep 
       %ifidn %1,ebp
		mov	ebp, esp
       %endif
       %endmacro

; "hw_revector"  sets up the stub and the patch to revector a hardware
; interrupt to V86 mode.
;
; hw_revector  pp  vv
;
;  will redirect protected mode interrupt number pp to V86 mode interrupt
;  number vv.  Note pp is in hex WITHOUT the leading 0x or trailing h.
;  vv is a normal number.
;_____________________________________________________________________________
	%macro hw_revector 2
service_%1:
patch_vec 0x%1, service_%1, D_INT
	push	eax
	mov	eax, [ss:%2*4]
	jmp	service_hw
	%endmacro


; V86_CALL is a wrapper around v86_call for simple situations:
;
; It clobbers esi, and ebp.  It calls a V86 interupt with random values in the
; segment registers.  It leaves the 12 DWORD frame from the call on the stack.
;_____________________________________________________________________________

%macro	V86_CALL 1
	sub	esp, 8*4			;Workspace + random
						; gs, fs, ds, es, ebp and esi
	push	dword 0x23002			;flags
	sub	esp, 2*4			;More workspace
	mov	esi, [%1*4]			;We will be calling int %1
	push	eax
	call	v86_call
%endmacro
