; idt.asm  Set up IDT
; Version 3.0, Mar 20, 1998
; Sample code
; by John S. Fine  johnfine@erols.com
; I do not place any restrictions on your use of this source code
; I do not provide any warranty of the correctness of this source code
;_____________________________________________________________________________
;
; This source is compatible with NASM versions 0.95, 0.97 and up, including
; my modified version of NASM 0.97
;_____________________________________________________________________________

  %include  "gdt.inc"
  %include  "idt.inc"

  GLOBAL  init_idt		;Initialize the IDT
  GLOBAL  idt			;The IDT itself

  extern  UNMAPPED_PAGE4IDT	;The system definition must reserve a range
				;of 256 unmapped addresses for marking
				;unsupported interrupt vectors.

  extern  FLAT_CODE		;Code selector

  extern  page_fault		;Page fault handler (stack dump in most of my
				;sample source packages)

SEGMENT DATA USE32 align=16
SEGMENT DATA			;Workaround NASM problem with __SECT__

; Build an idt full of default values which will each cause unique looking
; page faults if triggered.  The descriptors for any entries that should be
; used are then overwritten at link time by JLOC's patch facility.
;_____________________________________________________________________________

idt:
	%assign vec 0
	%rep	0x80		;Half a maximum idt

	desc	UNMAPPED_PAGE4IDT+vec, FLAT_CODE, D_INT
	%assign vec vec+1
	%endrep
loadidt	dw	$-idt-1			;Length of idt
	dd	idt			;Linear address of idt

; One of the descriptors that must be overwritten is the one for the page
; fault.
;_____________________________________________________________________________

	patch_vec 0xE, page_fault, D_INT
	

SEGMENT INIT USE32

init_idt:
;
;Purpose:
;	Initialize the IDT
;Input:
;	none
;Output:
;	none
;Clobbers:
;	none
;_____________________________________________________________________________

        lidt    [loadidt]		;Load IDT
	ret				;Return

