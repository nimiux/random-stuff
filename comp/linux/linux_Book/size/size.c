#include "func.h"

int alehop (int i) {
   if (i=0) {
      alehop(i-1);
   } else {
      return i;
   }
}

void tellsize(char *type, int size) {
   printf("El tamaño de %s es %d.\n", type, size);
}
int main() {

/*   a clas;   */


   tellsize("char", sizeof(char));
   tellsize("unsigned char", sizeof(unsigned char));

   tellsize("short", sizeof(short));
   tellsize("short int", sizeof(short int));

   tellsize("int", sizeof(int));
   tellsize("unsigned int", sizeof(unsigned int));
   tellsize("unsigned short int", sizeof(unsigned short int));
   
   tellsize("long", sizeof(long));
   tellsize("unsigned long", sizeof(unsigned long));

   tellsize("long long", sizeof(long long));
   tellsize("unsigned long long", sizeof(unsigned long long));
   
   tellsize("double", sizeof(double));

   alehop(3);

   return 0;
}
