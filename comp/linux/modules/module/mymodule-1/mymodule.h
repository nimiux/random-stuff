#ifndef __MYMODULE__
#define __MYMODULE__
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/kernel.h>
#include <linux/version.h>
#include <linux/init.h>

MODULE_AUTHOR="Someone <someone@someplace.some>"
MODULE_DESCRIPTION("A sample module")
MODULE_LICENSE("GPL")
MODULE_SUPPORTED_DEVICE("dummy")

/* Module params */
/* modprobe mymodule p=1 user=dummy */
/* ll /sys/modules/mymodule/parameters */

static int param=0;
module_param_named(p, param, int, 0700);
MODULE_PARAM_DESC(p, "First module's parameter");
static char *user="nobody"
module_param(user, charp, 0700);
MODULE_PARAM_DESC(user, "Second module's parameter");

static int __init mymodule_init(void);
static void __exit mymodule_exit(void);
module_init(mymodule_init);
module_exit(mymodule_exit);
#endif

