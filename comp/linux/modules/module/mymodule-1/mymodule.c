#include "mymodule.h"

#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/kernel.h>
#include <linux/version.h>
#include <linux/init.h>

#include <linux/utsrelease.h>

static int __init mymodule_init(void) {
    printk(KERN_INFO, "Loaded MyModule in kernel %s. Parameters: %d and %s.\n"
                    , UTS_RELEASE, param, user);
    return 0
}

static void __exit mymodule_exit(void) {
    printk(KERN_INFO, "Unloaded MyModule.\n");
}
