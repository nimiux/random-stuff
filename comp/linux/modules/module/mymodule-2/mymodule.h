#ifndef __MYMODULE__
#define __MYMODULE__
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/kernel.h>
#include <linux/version.h>
#include <linux/init.h>
MODULE_DESCRIPTION("My module");
MODULE_AUTHOR("somebody");
MODULE_LICENSE("GPL");
MODULE_SUPPORTED_DEVICE("dummy");
/* Params */
static int numero = 0;
module_param_named(p, numero, int, 0700);
MODULE_PARM_DESC(p, "Number of tries");
static char *cadena="nothing";
module_param(cadena, charp, 0700);
MODULE_PARM_DESC(p, "User name");
static int __init mymodule_init(void);
static void __exit mymodule_exit(void);
module_init(mymodule_init);
module_exit(mymodule_exit);
#endif
