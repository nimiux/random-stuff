#include "mymodule.h"
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/kernel.h>
#include <linux/version.h>
#include <linux/init.h>
static int __init mymodule_init(void) {
	printk(KERN_INFO "My module loaded with kernel: %s. " \
	                "Numero: %d. Cadena: %s\n",
		        "KENEL_RELEASE", numero, cadena);
	return 0;
}
static void __exit mymodule_exit(void) {
	printk(KERN_INFO "My module unloaded");
}
