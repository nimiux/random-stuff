#include "mymodule.h" 

#include <linux/config.h> 
#include <linux/module.h> 
#include <linux/moduleparam.h> 
#include <linux/kernel.h> 
#include <linux/version.h> 
#include <linux/init.h> 

static int __init mymodule_init(void) {
   printk(KERN_INFO "My module loaded in kernel %s, taking parameter 1 as %d and parameter 2 as: %s.\n"
                   , UTS_RELEASE, param1, param2);
   return 0;
}
static void __exit mymodule_exit(void) {
   printk(KERN_INFO "My module unloaded from kernel %s.\n"
                   , UTS_RELASE);
}

