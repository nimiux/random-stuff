
#ifndef __MYMODULE_
#define __MYMODULE__

#include <linux/config.h> 
#include <linux/module.h> 
#include <linux/moduleparam.h> 
#include <linux/kernel.h> 
#include <linux/version.h> 
#include <linux/init.h> 

/* Module description */
MODULE_DESCRIPTION("Mi module");
MODULE_AUTHOR("someone");
MODULE_LICENSE("unlicense");

/* Module parameters */

static int param1=0;
static char *param2="dummy";

module_param_named(intparam,param1,int,0700); 
MODULE_PARAM_DESC(intparam, "First parameter");

module_param_named(charparam, param2, charp, 0700); 
MODULE_PARAM_DESC(charparam, "Second parameter");

/* Init an cleanup funtions */

static int __init mymodule_init(void);
static void __exit mymodule_exit(void);

module_init(mymodule_init);
module_exit(mymodule_exit);

#endif
