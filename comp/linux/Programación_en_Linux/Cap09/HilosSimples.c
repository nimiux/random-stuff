#include <pthread.h>
#include <stdio.h>
#include <time.h>

// Se�alizador compartido
volatile int Hilos=2;

// Esta funci�n contiene el c�digo
// que ejecutar�n los hilos
void* FuncionHilo(void *P)
{
  int N;
  clock_t Ahora;
  int Ticks;

  // Vamos a escribir 10 datos
  for(N=1; N<10; N++) {
    // Si el hilo ha recibido
    // alg�n argumento
    if(P)
      // escribimos el cuadrado de N
      printf("\t%d\n",N*N);
    else
      // en caso contrario escribimos N
      printf("%d\n",N);

    // Provocamos una espera aleatoria
    // de entre 0-0.5 segundos
    Ticks=rand()%CLOCKS_PER_SEC/2;
    Ahora=clock();
    while(clock()-Ahora<Ticks) ;
  } 

  // Este hilo ha terminado
  --Hilos;
} 

// Punto de entrada al programa 
int main() 
{ 
  // Vamos a crear dos hilos
  pthread_t Hilo1, Hilo2;
  void *Retorno;

  // Atributos de los hilos
  pthread_attr_t Atributos;

  // Inicializamos los atributos
  pthread_attr_init(&Atributos);

  // y establecemos los que nos interesen
  pthread_attr_setdetachstate(&Atributos,PTHREAD_CREATE_DETACHED);

  // El primer hilo recibir� un argumento
  pthread_create(&Hilo1,&Atributos,FuncionHilo,(void *)1);
  // y el segundo no
  pthread_create(&Hilo2,&Atributos,FuncionHilo,NULL);

  // Esperamos a que los hilos terminen
  while(Hilos)
    sleep(1); // sin consumir el tiempo de proceso

  // Esperamos a que terminen ambos
  pthread_join(Hilo1,&Retorno);
  pthread_join(Hilo2,&Retorno);

  return 0;
}
