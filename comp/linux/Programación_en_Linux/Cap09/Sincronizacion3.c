#include <pthread.h> 
#include <stdio.h>
#include <time.h>
#include <semaphore.h>

volatile int NuevosDatos=-1;

// Sem�foro
sem_t Semaforo;

// Matriz con los datos a tratar
int DatosIniciales[10];
// Matriz que contendr� los resultados
int Resultado[10];

// Funci�n que recuperar� los datos
void* PreparaDatos(void *P)
{
  int N;

  puts("Introduzca los 10 n�meros: ");

  // Simplemente introducimos una 
  // secuencia de n�meros
  for(N=0; N<10; N++) {
    scanf(" %d",&DatosIniciales[N]);
    NuevosDatos=N;

    // Activamos el sem�foro
    sem_post(&Semaforo);
  }
}

// Funci�n que generar� los resultados
void* GeneraResultados(void *P)
{
  int N=0;

  // Obtenemos un resultado a partir de
  // cada dato
  while(NuevosDatos < 9) {

    // esperamos el sem�foro
    sem_wait(&Semaforo);

    // procesamos los nuevos datos
    while(N<=NuevosDatos)
      Resultado[N]=DatosIniciales[N++]*5;
  }
}

// Punto de entrada al programa 
int main() 
{ 
  // Vamos a crear dos hilos
  pthread_t Hilo1, Hilo2;
  void *Retorno;
  int N;

  // Inicializamos el sem�foro  
  sem_init(&Semaforo,0,0);

  // Creamos el hilo que generar� los datos
  pthread_create(&Hilo1,NULL,PreparaDatos,NULL);
  // y el que los procesar�
  pthread_create(&Hilo2,NULL,GeneraResultados,NULL);

  // Esperamos a que terminen ambos
  pthread_join(Hilo1,&Retorno);
  pthread_join(Hilo2,&Retorno);

  // Destrucci�n del sem�foro
  sem_destroy(&Semaforo);

  // Mostramos los resultados
  puts("Dato\tResultado\n");
  // en dos columnas
  for(N=0; N<10; N++)
    printf("%d\t%d\n",DatosIniciales[N],Resultado[N]);

  return 0;
}
