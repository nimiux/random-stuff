#include <pthread.h>
#include <stdio.h>
#include <time.h>

#define NELEMENTOS 100000

// Matriz con los datos a tratar
int DatosIniciales[NELEMENTOS];
// Matriz que contendr� los resultados
int Resultado[NELEMENTOS];

// Funci�n que recuperar� los datos
void* PreparaDatos(void *P)
{
  int N;

  // Simplemente introducimos una 
  // secuencia de n�meros
  for(N=0; N<NELEMENTOS; N++)
    DatosIniciales[N] = (N+1)*2;
}

// Funci�n que generar� los resultados
void* GeneraResultados(void *P)
{
  int N;

  // Obtenemos un resultado a partir de
  // cada dato
  for(N=0; N<NELEMENTOS; N++)
    Resultado[N]=DatosIniciales[N]*5;
}


// Punto de entrada al programa 
int main() 
{ 
  // Vamos a crear dos hilos
  pthread_t Hilo1, Hilo2;
  void *Retorno;
  int N;

  // Creamos el hilo que generar� los datos
  pthread_create(&Hilo1,NULL,PreparaDatos,NULL);
  // y el que los procesar�
  pthread_create(&Hilo2,NULL,GeneraResultados,NULL);

  // Esperamos a que terminen ambos
  pthread_join(Hilo1,&Retorno);
  pthread_join(Hilo2,&Retorno);

  // Mostramos los resultados
  puts("Dato\tResultado\n");
  // en dos columnas
  for(N=0; N<NELEMENTOS; N++)
    printf("%d\t%d\n",DatosIniciales[N],Resultado[N]);

  return 0;
}
