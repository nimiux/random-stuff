#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>

int main()
{
  // Una variable con una letra
  // a convertir
  char Letra='r';

  // Una cadena con un n�mero
  char Cadena[]="234 a�os";
  int Entero; // y un entero

  // Mostramos la letra y la convertimos
  // a may�scula y min�scula
  printf("Letra original=%c\n"
	 "May�scula=%c\n"
	 "Min�scula=%c\n\n", Letra,
	 toupper(Letra),tolower(Letra));

  // Extraemos de la matriz de caracteres
  // el n�mero que contiene
  Entero=strtol(Cadena,NULL,10); 
  // Lo mostramos como n�mero
  printf("Entero=%d\n",Entero);

  // Efectuamos una operaci�n aritm�tica
  Entero+=100;
  // Convertimos a cadena
  sprintf(Cadena, "%d a�os", Entero);
  // y la mostramos de nuevo
  printf("Cadena=%s\n", Cadena);


  return 0;
}
