#include <string.h>

int main()
{
  char Mensaje[] = "Hola mundo";

  // Mostramos una cabecera
  printf("Cadena original\t"
	 "A buscar\t"
	 "Coincidencia\n");

  printf("---------------\t"
	 "--------\t"
	 "------------\n");

  // Primera b�squeda
  printf("%s\t%c\t\t%s\n", Mensaje, 'm',
	 strchr(Mensaje,'m') ? "S�" : "No");

  // Dem�s b�squedas similares
  printf("%s\t%c\t\t%s\n", Mensaje, 'z',
	 strchr(Mensaje,'z') ? "S�" : "No");

  printf("%s\t%s\t\t%s\n", Mensaje, "mundo",
	 strstr(Mensaje,"mundo") ? "S�" : "No");

  printf("%s\t%s\t\t%s\n", Mensaje, "Mundo",
	 strstr(Mensaje,"Mundo") ? "S�" : "No");

  return 0; 
}
