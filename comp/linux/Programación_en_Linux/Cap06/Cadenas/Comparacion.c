#include <string.h>

int main()
{
  // Dos cadenas que comparten un mismo inicio
  char Cadena1[] = "Una cadena de texto";
  char Cadena2[] = "Una cadena de texto larga";
  // La misma cadena 
  char Cadena3[] = "GNU/Linux";
  char Cadena4[] = "GNU/LINUX";

  // Comparamos las dos primeras completas
  printf("%d\n", strcmp(Cadena1, Cadena2));

  // Ahora comparamos los primeros caracteres,
  // hasta la longitud de la primera cadena
  printf("%d\n", strncmp(Cadena1, Cadena2, strlen(Cadena1)));

  // Cambiamos la U de may�scula a min�scula
  Cadena1[0] = 'u';
  printf("%d\n\n", strcmp(Cadena1, Cadena2));

  // Comparamos las dos cadenas siguientes
  printf("%d\n", strcmp(Cadena3, Cadena4));
  // Las comparamos de nuevo sin diferenciar 
  // entre may�sculas y min�sculas
  printf("%d\n", strcasecmp(Cadena3, Cadena4));

  return 0;
}
