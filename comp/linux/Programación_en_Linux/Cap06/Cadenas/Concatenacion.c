#include <string.h>

#define LONGITUD_MAXIMA 256

int main()
{
  char Titulo[LONGITUD_MAXIMA+1] =
    "Programación en" " Linux";

  strcat(Titulo, "\nAnaya Multimedia");
  strcat(Titulo, "\nFrancisco Charte");

  printf(Titulo);

  return 0;
}
