#include <string.h>

int main()
{
  // Una variable que supuestamente contiene
  // el c�digo de un programa
  char Codigo[] =
    "int main()\n"
    "{\n"
    "char * Codigo;";

  // Separadores que se usan en la sintaxis
  // del lenguaje
  char Separadores[] = " \n;{}";

  // Puntero a los "token" encontrados
  char * Token;
 
  // Buscamos el primero
  Token=strtok(Codigo, Separadores);

  do { // vamos mostr�ndolos
    printf("%s\n", Token);
    // hasta encontrar el �ltimo
  } while(Token=strtok(NULL, Separadores));

  return 0;
}
