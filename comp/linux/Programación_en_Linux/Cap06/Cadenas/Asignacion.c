#include <string.h>

// Funci�n para asignar una cadena a otra
void Asigna(char *Destino, char *Origen)
{
  while(*Origen) // Mientras el car�cter no sea nulo
    // copiarlo del origen al destino
    *Destino++ = *Origen++;

  *Destino='\0';
}

#define LONGITUD_MAXIMA 256

// Punto de entrada al programa
int main()
{
  // Valor inicial
  char Titulo[LONGITUD_MAXIMA+1]=
    "Programaci�n en Linux";

  printf("%s\t%d caracteres\n", 
	 Titulo, strlen(Titulo)); // lo mostramos

  // Cambiamos el valor
  strncpy(Titulo,"Ahora asigno otra cadena 1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890 Fin de la cadena",
	  LONGITUD_MAXIMA);

  printf("%s\t%d caracteres\n", 
	 Titulo, strlen(Titulo)); // y volvemos a imprimir

  return 0;
}
