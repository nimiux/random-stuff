#include <string>
#include <iostream>

using namespace std;

int main(...)
{
  // Cadena de caracteres al estilo de C
  char CadenaClasica[] = "GNU/Linux";

  // Declaramos dos cadenas con un valor
  // inicial cada una
  string Cadena1="Hola mundo", 
    Cadena2(CadenaClasica),
    Cadena3(10,'-'),
    Cadena4(Cadena1+" con C++");

  cout << Cadena4.length() << endl
       << Cadena4.capacity() << endl
       << Cadena4.max_size() << endl << endl;

  cout << Cadena1 << endl 
       << Cadena2 << endl
       << Cadena3 << endl 
       << Cadena4 << endl << endl;

  // Concatenamos las cadenas
  Cadena1 += Cadena2;

  // Mostramos la longitud, contenido y 
  // una parte de la primera cadena
  /*  cout << Cadena1.length() << " caracteres <"
       << Cadena1 << ">" << endl
       << "Primeros 5 caracteres <"
       << Cadena1.substr(0,5) << ">" << endl;
  */
  return 0;
}
