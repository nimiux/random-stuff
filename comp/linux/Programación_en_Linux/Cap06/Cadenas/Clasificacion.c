#include <ctype.h>

int main()
{
  char Codigo[] =
    "int main()\n"
    "{\n"
    "char * Codigo;";

  char * Caracter = Codigo;

  printf("Texto a clasificar: '%s'\n\n", Codigo);

  while(*Caracter) {
    printf("<%c> es %s\n", *Caracter,
	   islower(*Caracter) ? "una min�scula" :
	   isupper(*Caracter) ? "una may�scula" :
	   isdigit(*Caracter) ? "un n�mero" :
	   ispunct(*Caracter) ? "un signo de puntuaci�n" :
	   isspace(*Caracter) ? "un espacio" :
	   "un car�cter desconocido");

    Caracter++;
  }

  return 0;
}
