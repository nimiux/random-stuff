#include <stdlib.h>

// Constante con el n�mero de elementos
#define NUM_ELEMENTOS 100

int main()
{
  // Dos punteros de tipo int
  int *Numeros, *Numero;
  int Indice; // y un contador

  // Asignamos memoria para los n�meros
  //Numeros=(int *)malloc(sizeof(int)*NUM_ELEMENTOS);
  Numeros=(int *)alloca(sizeof(int)*NUM_ELEMENTOS);
  // Si se ha devuelto NULL
  if(!Numeros) {
    // no podemos continuar
    printf("No puede asignarse memoria\n");
    exit(-1);
  }

  // Tratamos el puntero como si de una matriz
  // se tratase
  for(Indice=0; Indice<NUM_ELEMENTOS; Indice++)
    // asignando valor a cada elemento
    Numeros[Indice]=Indice;

  // Utilizamos el segundo puntero para recorrer
  // la lista de n�meros
  Numero=Numeros;
  // en un bucle que va increment�ndolo
  while(*Numero!=NUM_ELEMENTOS-1)
    printf("%d\t", *Numero++);

  // �ltimo n�mero
  printf("%d\n", *Numero);

  // Liberamos la memoria previamente asignada
  //free(Numeros);

  return 0;
}
