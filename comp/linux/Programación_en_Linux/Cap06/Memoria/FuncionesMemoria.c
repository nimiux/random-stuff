#include <string.h>
#include <stdlib.h>

// Tama�o de los bloques de memoria
#define TAMANO_IMAGEN 256*1024

int main()
{
  // Asignamos un bloque hipot�ticamente
  // para contener una imagen
  void * Imagen=malloc(TAMANO_IMAGEN);
  // y un segundo bloque para copiarlo
  void * Copia=malloc(TAMANO_IMAGEN);

  // Una cadena de caracteres y un puntero
  char Cadena[] = "Bloque de memoria";
  char * Posicion; // auxiliar

  // Si no se ha podido asignar memoria
  if(!Imagen || !Copia) {
    // lo notificamos
    printf("No puede asignarse la memoria\n");
    exit(-1); // y salimos
  }

  // Calculamos la posici�n en que se encuentra
  // la mitad del bloque de memoria
  Posicion=Imagen+TAMANO_IMAGEN/2;
  // y copiamos all� la cadena de caracteres
  strcpy(Posicion, Cadena);

  // Copiamos la imagen original en la copia
  memcpy(Copia,Imagen,TAMANO_IMAGEN);

  // Damos a todos los bytes de la imagen 
  // el valor -
  memset(Imagen,'-',TAMANO_IMAGEN);

  // Comparamos los bloques para saber
  // si son iguales
  if(memcmp(Imagen,Copia,TAMANO_IMAGEN))
    printf("Los bloques no son iguales\n");
  else
    printf("Los bloques son iguales\n");

  // Buscamos en la copia de la imagen la
  // cadnea que se copi� anteriormente
  Posicion=(char *)memmem(Copia,TAMANO_IMAGEN,
		  (void *)Cadena,strlen(Cadena));

  if(Posicion) // Si la hemos encontrado
    // la mostramos
    printf("Se ha encontrado la cadena <%s>\n",Posicion);

  free(Imagen); // Liberamos ambos
  free(Copia); // bloques de memoria

  return 0;
}
