#include <iostream>

using namespace std;

// Constante con el n�mero de elementos
#define NUM_ELEMENTOS 100

int main()
{
  // Dos punteros de tipo int
  int *Numeros, *Numero;

  // Asignamos memoria para los n�meros
  Numeros=new int[NUM_ELEMENTOS];
  // Si se ha devuelto NULL
  if(!Numeros) {
    // no podemos continuar
    cout << "No puede asignarse memoria\n";
    exit(-1);
  }

  // Tratamos el puntero como si de una matriz
  // se tratase
  for(int Indice=0; Indice<NUM_ELEMENTOS; Indice++)
    // asignando valor a cada elemento
    Numeros[Indice]=Indice;

  // Utilizamos el segundo puntero para recorrer
  // la lista de n�meros
  Numero=Numeros;
  // en un bucle que va increment�ndolo
  while(*Numero!=NUM_ELEMENTOS-1)
    cout << *Numero++ << '\t';

  // �ltimo n�mero
  cout << *Numero << endl;

  // Liberamos la memoria previamente asignada
  delete [] Numeros;

  return 0;
}
