#include <time.h>

int main()
{
  // Obtenemos el tiempo transcurrido
  time_t Ahora = time(NULL);
  struct tm *Local;

  char *Dias[] = { "Domingo",
    "Lunes", "Martes", "Mi�rcoles",
    "Jueves", "Viernes", "S�bado" };

  // Obtenemos la hora local
  Local=localtime(&Ahora);

  // Y mostramos algunos datos
  printf("\n%d\t%d d�as (unos %d a�os) "
	 "- %02d:%02d:%02d\n\n",
	 Ahora, // el valor devuelto
	 Ahora/60/60/24,  // convertimos en d�as
	 Ahora/60/60/24/365, // y en a�os
	 Ahora/60/60%24, // Obtenemos n�mero de horas
	 Ahora/60%60,    // minutos
	 Ahora%60);      // y segundos

  // Mostramos la hora local
  printf("Zona horaria: %s\n"
	 "Diferencia GMT: %d\n"
	 "\n%s, %02d/%02d/%4d\t%02d:%02d:%02d\n",
	 Local->tm_zone,
	 Local->tm_gmtoff/3600,
	 Dias[Local->tm_wday],
	 Local->tm_mday, Local->tm_mon+1,
	 Local->tm_year+1900, Local->tm_hour,
	 Local->tm_min, Local->tm_sec);

  return 0;
}
