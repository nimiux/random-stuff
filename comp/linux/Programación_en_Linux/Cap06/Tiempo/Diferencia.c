#include <time.h>

// Segundos en un d�a
#define SEGUNDOS_DIA 24*60*60

int main()
{
  // Variables necesarias
  time_t Ahora, Codificado;
  struct tm *Fecha;
  int Dias;

  // Obtenemos la fecha y hora actuales
  Ahora=time(NULL);

  printf("\n%s\n",ctime(&Ahora));

  // convirtiendo a local
  Fecha=localtime(&Ahora);

  // Establecemos la fecha en que se codific�
  // el programa, 10 de febrero de 2003
  Fecha->tm_mday=10;
  Fecha->tm_mon=1;
  Fecha->tm_year=2003-1900;

  // Obtenemos al time_t de esa fecha
  Codificado=mktime(Fecha);

  // Obtenemos la diferencia en segundos, 
  // dividiendo para obtener d�as
  Dias=difftime(Ahora, Codificado)/(SEGUNDOS_DIA);

  // Mostramos el resultado
  printf("Hace %d d�as que se escribi� este programa\n", Dias);

  return 0;
}
