#include <sys/utsname.h>
#include <sys/types.h>
#include <unistd.h>

int main()
{
  // Estructura para recoger los datos
  struct utsname Datos;

  // Los leemos
  uname(&Datos);

  // Y mostramos por la consola
  printf("\nSistema: %s\n"
	 "Versi�n del sistema: %s-%s\n"
	 "Nombre del ordenador: %s\n"
	 "Descripci�n: %s\n"
	 "ID del Usuario: %d\n",
	 Datos.sysname,Datos.release,
	 Datos.version,Datos.nodename,
	 Datos.machine,getuid());

  return 0;
}
