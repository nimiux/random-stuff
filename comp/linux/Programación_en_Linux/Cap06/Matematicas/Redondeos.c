#include <math.h>

int main()
{
  printf("\tceil\tfloor\trint\n"
	 "%2.2f\t%2.2f\t%2.2f\t%2.2f\n"
	 "%2.2f\t%2.2f\t%2.2f\t%2.2f\n"
	 "%2.2f\t%2.2f\t%2.2f\t%2.2f\n",
	 19.3,ceil(19.3),floor(19.3),rint(19.3),
	 19.5,ceil(19.5),floor(19.5),rint(19.5),
	 19.7,ceil(19.7),floor(19.7),rint(19.7));


  return 0; 
}
