package AnayaMultimedia;

class Conversion {
    // El m�todo main() puede generar una excepci�n
    public static void main(String[] args) 
	throws java.io.IOException {
	char Caracter;
	// Mostramos una indicaci�n
	System.out.print("Ve introduciendo caracteres: ");
	do { // Recogemos un car�cter de la consola
	    Caracter=(char )System.in.read();
	    // y lo mostramos convertido a may�scula
	    System.out.print(Character.toUpperCase(Caracter));
	    // Hasta que se encuentre una F
	} while(Caracter != 'F');
    }
}
