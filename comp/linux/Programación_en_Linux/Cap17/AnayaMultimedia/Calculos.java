package AnayaMultimedia;

// Una clase para realizar calculos
class Calculos {
   
    long Cuadrado(int N) {
	return N*N;
    }

    long Cubo(int N) {
	return N*N*N;
    }

    // Punto de entrada al programa
    public static void main(String[] args) {
	Calculos Calculadora = new Calculos();
	System.out.println(Calculadora.Cuadrado(5));
	System.out.println(Calculadora.Cubo(5));
    }
}

