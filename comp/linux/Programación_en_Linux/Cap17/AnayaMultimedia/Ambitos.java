package AnayaMultimedia;

public class Ambitos {
    public class ClaseInternaPublica {
    }

    protected class ClaseInternaProtegida {
    }

    private class ClaseInternaPrivada {
    }
}

class ClaseAdicionalNoPublica {
}
