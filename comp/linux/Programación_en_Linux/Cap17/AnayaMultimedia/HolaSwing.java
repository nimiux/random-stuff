package AnayaMultimedia;

// Importamos los paquetes que
// vamos a necesitar
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

// Nuestra clase ser� una derivada de JFrame
class HolaSwing extends JFrame {
    // Una constante est�tica con un mensaje
    private static final String Mensaje="Hola con Java";

    // En el constructor creamos la interfaz
    HolaSwing() {
	// Llamamos al constructor de la clase
	// base facilit�ndole un t�tulo
	super(Mensaje);

	// Creamos una etiqueta que mostrar� el t�tulo
	// centrado en el espacio disponible
	JLabel label=new JLabel(Mensaje,JLabel.CENTER);
	// establecemos el tipo de borde
	label.setBorder(new EtchedBorder());

	// Creamos un bot�n
	JButton boton=new JButton("Salir");
	// y conectamos el evento de pulsaci�n con una clase
	boton.addActionListener(
	    // que creamos in situ
            new java.awt.event.ActionListener() {
		// con el correspondiente m�todo actionPerformed
		public void actionPerformed(ActionEvent x) {
		    System.exit(0);
		}
	    }
        );

	// Obtenemos el panel asociado a la ventana
	JPanel Contenido = (JPanel )getContentPane();
	// y establecemos el gestor de distribuci�n del contenido
	Contenido.setLayout(new BorderLayout());

	// A�adimos la etiqueta en la parte superior
	Contenido.add(label,BorderLayout.NORTH);
	// y el bot�n en la inferior
	Contenido.add(boton,BorderLayout.SOUTH);
    }

    // Punto de entrada al programa
    public static void main(String args[]) {
	// Creamos un objeto HolaSwing
	JFrame App=new HolaSwing();

	// A�adimos un m�todo de escucha
	App.addWindowListener(new WindowAdapter() {
		// para la se�al de cierre
		public void windowClosing(WindowEvent e) {
		    // poniendo fin a la ejecuci�n
		    System.exit(0);
		}
        });

	// Ajustamos la ventana al contenido
	App.pack();
	// y la mostramos
	App.show();
    }
}
