#!/usr/bin/perl

# Introducimos el tipo de respuesta
print "Content-type: text/html\n\n";

# Una matriz con 12 elementos
@Meses=(Enero,Febrero,Marzo,Abril,Mayor,Junio,
	Julio,Agosto,Septiembre,Octubre,Noviembre,Diciembre);

# Un bucle con un contador que toma
# los valores del 0 al 11
for $Indice (0..11) {
    # Accedemos a los elementos de la matriz
    print @Meses[$Indice],"<br>";
}

# Un condicional
if ($ENV{'QUERY_STRING'} ne "") {
    print "<hr>Se han entregado los argumentos: ";
    print $ENV{'QUERY_STRING'},"<hr>";
}

print "<p><pre>";
# Un bucle que recorre todas las claves
# existente en la lista asociativa %ENV
foreach $Clave (keys %ENV) {
    # Mostrando cada clave y su valor
    print $Clave, '=', $ENV{$Clave};
    print "<br>";
}
print "</pre>";
