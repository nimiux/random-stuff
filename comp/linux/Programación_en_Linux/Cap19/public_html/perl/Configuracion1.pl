#!/usr/bin/perl
print "Content-type: text/html\n\n";

# Obtenemos la cadena de consulta
$Argumentos=$ENV{'QUERY_STRING'};

# Solo si encontramos la cadena "Sistema"
if ($Argumentos =~ /Sistema/) {
    # Sustituimos globalmente el caracter =
    # por la secuencia de caracteres ->
    $Argumentos =~ s/=/ -> /g;
    # Extraemos los tres argumentos que esperamos
    $Argumentos =~ /(.*)&(.*)&(.*)/;
    # y los mostramos
    print "$1<p>$2<p>$3<p>";

    # Extraemos el argumento Sistema asignandolo
    # a una variable
   ($Sistema) = ($Argumentos =~ /Sistema -> ([a-zA-Z]*)&/);
   print "El sistema elegido es: $Sistema";
}

# Recuperamos de nuevo la lista de parametros
$Argumentos=$ENV{'QUERY_STRING'};
# dividiendola en tantos elementos como argumentos
# existan buscando como separador el caracter &
@Formulario=split(/&/,$Argumentos);

print "<hr>";
foreach $Entrada (@Formulario) {
    print "$Entrada<p>";
}

