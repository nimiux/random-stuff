#!/usr/bin/perl
print "Content-type: text/html\n\n";

read(STDIN,$Argumentos,$ENV{'CONTENT_LENGTH'});

@Argumentos=split(/&/,$Argumentos);
foreach $Argumento (@Argumentos) {
   ($Clave, $Valor)=split(/=/,$Argumento);
   $Formulario{$Clave}=$Valor;
}

foreach $Clave (keys %Formulario) {
    print "$Clave -> $Formulario{$Clave}<p>";
}
