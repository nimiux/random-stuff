#include <stdio.h>

int main()
{
  int n,m;

  puts("Content-type: text/html\n\n");
  puts("<table border=1>");

  for(n=1;n<=10;n++) {
    puts("<tr>");
    for(m=1;m<=10;m++) 
      printf("<td>%d</td>",n*m);
    puts("</tr>");
  }

  puts("</table>");

  return 0;
}
