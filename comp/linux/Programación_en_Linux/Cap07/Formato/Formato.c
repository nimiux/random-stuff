#include <stdio.h>

int main()
{
  // Para leer un n�mero de tel�fono
  int Prefijo,Numero;
  // Puntero a una cadena de caracteres
  char *Cadena;
  // Y matriz de caracteres
  char DNI[12];

  // Solicitamos la introducci�n de un 
  // n�mero de tel�fono
  printf("Introduzca el tel�fono en formato "
	 "(999)-999999: ");
  
  // Comprobamos que pueda ser le�do
  if(scanf(" (%3d)-%7d", &Prefijo, &Numero)!=2)
    // indicando un posible fallo
    puts("\nNo lo ha introducido correctamente\n");
  else // lo mostramos tras recuperarlo
    printf("\n\nN�mero introducido: %d %d\n", Prefijo, Numero);

  // Imprimimos en una cadena asignada autom�ticamente
  // para nosotros
  asprintf(&Cadena,"%dX%d=%d\n",7,45,7*45);
  puts(Cadena); // y la imrpimimos

  // Solicitamos el DNI
  puts("\nIntroduzca el DNI: ");
  // Como una secuencia de 10 caracteres
  // m�ximo que pueden ser n�meros y puntos
  scanf(" %10[0-9.]", DNI);
  // lo mostramos
  printf("\n%s\n", DNI);

  return 0;
}
