#include <dirent.h>

// M�xima longitud del camino
#define MAX_PATH 256

// Recogemos los par�metros de entrada que
// se pudieran facilitar al programa
int main(int argc, char *argv[])
{
  // Establecemos el camino por defecto del
  // directorio a leer
  char Camino[MAX_PATH]=".";

  // Variables para abrir el directorio
  // e ir leyendo entradas
  DIR *Directorio;
  struct dirent *Entrada;

  if(argc>=2) // si se han facilitado par�metros
    // asumir que el primero es el camino a leer
    strcpy(Camino,argv[1]);

  // Abrimos el directorio
  Directorio=opendir(Camino);
  // Si no es posible lo indicamos
  if(!Directorio) {
    printf("Fallo al abrir el directorio\n");
    exit(-1); // y salimos
  }

  printf("\nContenido del directorio: %s\n", Camino);

  // Mientras haya entradas en el directorio las
  // vamos leyendo
  while(Entrada=readdir(Directorio))
    // si se trata de un subdirectorio
    if(Entrada->d_type == DT_DIR)
      // lo mostramos entre <>
      printf("<%s>\t", Entrada->d_name);
    else // en caso contrario sin nada
      printf("%s\t", Entrada->d_name);

  putchar('\n'); 
  // Cerramos el directorio
  closedir(Directorio);

  return 0;
}
