#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>

// Asumimos que un camino de
// 512 bytes como m�ximo
#define MAX_PATH 512
int main()
{
  // Variables para contener el camino
  char CaminoOriginal[MAX_PATH],
    CaminoActual[MAX_PATH];

  // Mostramos el camino desde el que est�
  // ejecut�ndose el programa
  printf("Directorio actual: %s\n",
	 getcwd(CaminoOriginal, MAX_PATH));

  // Cambiamos otro directorio
  chdir("../..");

  // Y mostramos de nuevo el directorio actual
  printf("Directorio tras el cambio: %s\n",
	 getcwd(CaminoActual, MAX_PATH));

  // Creamos el directorio de ejemplo
  mkdir("MiDirectorio", S_IRWXU);
  puts("Se ha creado el directorio 'MiDirectorio'\n"
       "Pulse <Intro> para continuar ...");
  getchar();

  // Borramos el directorio
  rmdir("MiDirectorio");

  // Y volvemos al directorio original
  chdir(CaminoOriginal);

  printf("Se ha eliminado 'MiDirectorio' y vuelto"
	 " al directorio %s\n\n", CaminoOriginal);

  return 0;

}
