#include <stdio.h>

// Longitud m�xima de las l�neas
// a leer de la consola
#define MAX_LENGTH 256

int main()
{
  FILE *Archivo; // Archivo
  // Puntero al �rea donde se alojar�n los datos
  char *Informacion;
  // Tama�o de ese �rea
  size_t Tamano;
  // L�nea para ir pidiendo datos
  char Linea[MAX_LENGTH];
 
  // Abrimos el archivo en memoria
  Archivo=(FILE *)open_memstream(&Informacion,&Tamano);

  if(!Archivo) { // Si no se ha podido abrir
    puts("Fallo en la apertura\n");
    exit(-1); // no continuamos
  }

  // Solicitamos la introducci�n de datos
  puts("Vaya introduciendo datos a escribir. "
       "Pulse <Control-D> para terminar\n");

  // Mientras podamos ir leyendo l�neas
  while(fgets(Linea,MAX_LENGTH,stdin))
    // las vamos escribiendo en el archivo
    fputs(Linea,Archivo);

  // Provocamos la escritura en el archivo
  // en memoria, actualizando los punteros
  fflush(Archivo);

  // Podemos movernos al principio
  fseek(Archivo,0,SEEK_SET);

  puts("\n\nDatos del archivo\n\n");

  // Escribimos en la consola los datos
  // que contiene el bloque de memoria
  fwrite(Informacion,1,Tamano,stdout);

  fclose(Archivo); // Cerramos el archivo

  return 0;
}
