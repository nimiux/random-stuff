#include <stdio.h>

int main()
{
  // Abrimos el archivo Makefile para
  // lectura
  FILE *Archivo = fopen("Makefile","r");

  // Si no ha podido abrirse
  if(!Archivo) {
    // lo indicamos
    printf("Fallo en la apertura\n");
    exit(-1);
  }

  fclose(Archivo); // Cerramos el archivo

  // Abrimos para escritura la consola
  Archivo=fopen("/dev/console","w");
  fprintf(Archivo,"Esto debe ser la consola\n");

  // Reabrimos el flujo de stdout para
  // escribir en un archivo corriente
  freopen("Texto","w",stdout);
  // usando printf
  printf("Texto enviado a la consola\n");

  fcloseall(); // Cerramos todos los flujos

  return 0;
}
