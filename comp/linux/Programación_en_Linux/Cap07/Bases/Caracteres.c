#include <stdio.h>
#include <ctype.h>

int main()
{
  char c;

  // Mientras haya caracteres los
  // leemos
  while((c=getchar())!=EOF)
    // y los escribimos convertidos
    putchar(toupper(c));

  return 0;
}
