#include <stdio.h>

// Longitud m�xima de la cadena a leer
#define LONGITUD 256

int main() 
{
  // Abrimos el archivo Makefile
  FILE *Archivo=fopen("Makefile","r");
  char Cadena[LONGITUD];

  if(!Archivo) { // Si no puede abrirse
    printf("Error en la apertura\n");
    exit(-1); // salimos
  }

  // Nos movemos hasta el final del archivo
  fseek(Archivo,0,SEEK_END);
  // y obtenemos la posici�n, que equivaldr�
  // al tama�o actual del archivo
  printf("El archivo tiene %d bytes\n",
	 ftell(Archivo));

  // Nos movemos a la mitad del archivo
  fseek(Archivo,ftell(Archivo)/2,SEEK_SET);
  // leemos una l�nea y la mostramos
  printf("%s\n", fgets(Cadena,LONGITUD,Archivo));

  // Nos movemos 40 bytes hacia atr�s
  fseek(Archivo,-40,SEEK_CUR);
  // leemos una l�nea y la mostramos
  printf("%s\n", fgets(Cadena,LONGITUD,Archivo));

  fclose(Archivo); // cerramos el archivo

  return 0;
}
