#include <iostream>

using namespace std;

int main(...)
{
  // Para almacenar los datos
  char Nombre[256];
  int Edad;

  // Enviamos una cadena de caracteres
  // a la salida est�ndar
  cout << "Hola, introduce tu nombre: ";
  // y recogemos otra de la entrada est�ndar
  cin >> Nombre;

  // Usamos el dato recogido para enviarlo
  // de nuevo a la salida
  cout << "Hola " << Nombre 
       << ", �cu�ntos a�os tienes?: ";

  // Leemos de la entrada est�ndar un entero
  cin >> Edad;

  // Enviamos una nueva cadena
  cout << Nombre << " tienes " << Edad << " a�os\n";

  return 0;
}
