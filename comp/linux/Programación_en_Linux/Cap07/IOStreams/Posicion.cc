#include <iostream>
#include <fstream>

using namespace std;

int main(...)
{
  // Leemos nuevamente el archivo Makefile
  ifstream Makefile("Makefile");

  // Puntero al bloque donde alojaremos
  // su contenido
  char *Contenido;
  int Bytes;

  // Nos movemos al final del archivo
  Makefile.seekg(0,ios::end);
  // y obtenemos su tama�o
  Bytes=Makefile.tellg();

  // Asignamos el bloque de memoria
  Contenido=new char[Bytes];

  // Nos movemos al inicio del archivo
  Makefile.seekg(0);
  // leemos todo su contenido
  Makefile.read(Contenido,Bytes);
  // y lo cerramos
  Makefile.close();

  // Mostramos el contenido por
  // la salida estandar
  cout << Contenido;

  // Liberamos la memoria
  delete [] Contenido;

  return 0;
}
