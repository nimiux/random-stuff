#include <iostream>

using namespace std;

int main(...)
{
  // Datos que vamos a solicitar o mostrar
  char Nombre[21];
  int Entero=256;
  float PuntoFlotante=13.2;

  // Solicitamos el nombre
  cout << "Introduce tu nombre: ";
  // con un m�ximo de 20 caracteres
  cin.width(20);
  cin >> Nombre;
  // y lo mostramos con el formato por defecto
  cout << "'" << Nombre << "'\n";

  // Establecemos el car�cter de relleno
  cout.fill('#');
  cout.width(20); // y una longitud m�nima
  // Tambi�n establecemos el ajuste a derecha
  cout.setf(ios::right,ios::adjustfield);
  // y mostramos de nuevo el nombre
  cout << "'" << Nombre << "'\n";


  // Mostramos el n�mero entero con 
  // formato por defecto
  cout << Entero << "\n";

  // Ahora con un ancho m�nimo de 5 d�gitos
  cout.width(5);
  cout << Entero << "\n";
  // tambi�n el punto flotante
  cout << PuntoFlotante << "\n";

  // Establecemos la notaci�n exponencial
  cout.setf(ios::scientific);
  cout << PuntoFlotante << "\n";

  // Fijamos la base hexadecimal
  cout.setf(ios::hex,ios::basefield);
  // y que se muestre el prefijo de base
  cout.setf(ios::showbase);
  // para mostrar el n�mero
  cout << Entero << "\n";

  // Desactivamos el prefijo de base
  cout.unsetf(ios::showbase);
  // y mostramos de nuevo el n�mero
  cout << Entero << "\n";

  return 0;
}
