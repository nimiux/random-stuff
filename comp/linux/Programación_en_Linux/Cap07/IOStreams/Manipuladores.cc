#include <iostream>
#include <iomanip>

using namespace std;

int main(...)
{
  // Datos que vamos a solicitar o mostrar
  char Nombre[21];
  int Entero=256;
  float PuntoFlotante=13.2;

  // Solicitamos el nombre
  cout << "Introduce tu nombre: ";
  // con un m�ximo de 20 caracteres
  cin >> setw(20) >> Nombre;

  // y lo mostramos con el formato por defecto
  cout << "'" << Nombre << "'" << endl;

  // Establecemos el car�cter de relleno
  // y una longitud m�nima
  // Tambi�n establecemos el ajuste a derecha

  // y mostramos de nuevo el nombre
  cout << setfill('#') << setw(20) << right
       << "'" << Nombre << "'" << endl;


  // Mostramos el n�mero entero con 
  // formato por defecto
  cout << Entero << endl;

  // Ahora con un ancho m�nimo de 5 d�gitos
  cout << setw(5) << Entero << endl;
  // tambi�n el punto flotante
  cout << PuntoFlotante << endl;

  // Establecemos la notaci�n exponencial
  cout << scientific << PuntoFlotante << endl;

  // Fijamos la base hexadecimal
  // y que se muestre el prefijo de base

  // para mostrar el n�mero
  cout << hex << showbase << Entero << endl;

  // Desactivamos el prefijo de base

  // y mostramos de nuevo el n�mero
  cout << noshowbase << Entero << "\n";

  return 0;
}
