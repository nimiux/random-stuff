#include <iostream>
#include <fstream>

using namespace std;

int main(...)
{
  char Caracter;
  // Abrimos para lectura el archivo Makefile
  ifstream Makefile("Makefile");
  // Y creamos otro donde copiar el contenido
  ofstream CopiaMakefile("CopiaMakefile");

  // Mientras no haya errores ni EOF
  while(Makefile.good()) {
    // leemos del archivo original
    Makefile.get(Caracter);
    // y escribimos en la copia
    CopiaMakefile.put(Caracter);
  }

  Makefile.close(); // Cerramos los archivos
  CopiaMakefile.close();

  return 0;
}
