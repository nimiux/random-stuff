#include <iostream>
#include <sstream>

using namespace std;

// M''axima longitud de la cadena
#define MAX_LENGTH 256

int main(...)
{
  // Para solicitar la cadena
  char Entrada[MAX_LENGTH];
  // y almacenar los n''umeros
  int Alturas[10];

  // Solicitamos la introducci''on de la lista
  // de valores
  cout << "Introduzca la altura de los 10 puntos" << endl;
  cin.getline(Entrada,MAX_LENGTH);

  // y la mostramos tal cual
  cout << endl << "Cadena introducida: " << Entrada << endl;

  // Generamos un istringstream a partir
  // de la cadena
  istringstream Cadena(Entrada);

  // Usamos un bucle para recoger de ese
  for(int N=0; N<10; N++) 
    // objeto los n''umeros
    Cadena >> skipws >> Alturas[N];

  // Ahora los mostramos como n''umeros enteros
  cout << "Datos recuperados" << endl;
  for(int N=0; N<10; N++)
    cout << Alturas[N] << endl;

  return 0;
}
