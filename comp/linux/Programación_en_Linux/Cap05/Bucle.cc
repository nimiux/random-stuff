#include <iostream>

using namespace std;

int PideNumero()
{
  int N;

  cout << "Introduzca el n�mero: ";
  cin >> N;

  return N;
}

int main(...)
{
  int Contador, Dato, Suma;

  Suma = 0;

  for(Contador=1; Contador<=10; Contador++)
  {
    Dato=PideNumero();
    // Si el n�mero es cero salimos del bucle
    if(Dato==0) break;
    // Si es mayor que nueve lo ignoramos
    if(Dato>9) continue;
    // En caso contrario lo sumamos
    Suma=Suma+Dato;
  }

  return Suma;
}
