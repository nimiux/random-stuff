
#include <iostream>

using namespace std;

// TDiaSemana no es una variable, sino un identificador de tipo
enum TDiaSemana 
{ 
  Lunes, 
  Martes, 
  Miercoles,
  Jueves, 
  Viernes, 
  Sabado, 
  Domingo 
};

int main(...)
{
  TDiaSemana Hoy;
  TDiaSemana Indicencias[5];

  Hoy = Domingo;
  Indicencias[0] = Miercoles;

  cout << Hoy << endl;

  int Gastos[7];

  Gastos[Lunes] = 1500;

  return 0;
}
