#include <iostream>

using namespace std;

enum Meses
  {
    Enero,
    Febrero,
    Marzo,
    Abril,
    Mayo,
    Junio,
    Julio,
    Agosto,
    Septiempre,
    Octubre,
    Noviembre,
    Diciembre
  };

const int DiasMes[] =
  { 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

const int HorasDia = 24;

int main(...)
{
  int HorasAbril = DiasMes[Abril]*HorasDia;

  cout << HorasAbril << endl;
}
