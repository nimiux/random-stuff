
class TPunto // Clase de objeto TPunto
{
 private:
  int X, Y; // Coordenadas del punto
  unsigned char Color; // Color del punto
 public:
  // Da valores iniciales a los miembros
  void Inicializa(int PX, int PY, unsigned char PColor);
  void Dibuja(); // Dibuja el punto
};
