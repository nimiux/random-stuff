#include <iostream>

using namespace std;

class TObjeto
{
  int Dato;
public:
  TObjeto(int=0);
  int Valor();
  void Valor(int);
};

TObjeto::TObjeto(int N)
{
  Dato=N;
}

int TObjeto::Valor()
{
  return Dato;
}

void TObjeto::Valor(int N)
{
  Dato=N;
}


int main(...)
{
  TObjeto Objeto(5);

  cout << Objeto.Valor() << endl;

  return 0;
}
