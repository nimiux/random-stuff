// Definici�n de la clase TMatriz, que nos permitir�
// crear matrices de tama�o din�mico
class TMatriz
{
 private:
  // Puntero a la zona de memoria donde estar� la matriz
  int *Elementos;
  int NumElementos; // Numero de elementos de la matriz
 public:
  // El constructor asignar� la memoria
  TMatriz(int Num);
  // Operador para acceder a los elementos
  int & operator[](int Num);
  // El destructor liberar� la memoria asignada
  ~TMatriz();
};
