//---------------------------------------------------------------------------
#include <typeinfo>
#include "Vehiculo.h"
#include <iostream>

using namespace std;
//---------------------------------------------------------------------------
int main(...)
{
  
  TVehiculo * X[5]; // Matriz de objetos TVehiculo
  
  // Creamos una serie de objetos derivados de TVehiculo
  X[0] = new TBicicletaMontana(18);
  X[1] = new TAutomovil(5, false, 110);
  X[2] = new TBicicleta;
  X[3] = new TBicicletaMontana(21);
  X[4] = new TAutomovil(4, true, 280);
  
  cout << endl << typeid(*X[0]).name() << endl;

  // Cabecera de los datos a imprimir
  cout << "Tipo de veh�culo, Ejes, Ruedas, "
       << "Motor, Caracter�sticas adicionales"
       << endl;
  
  for(int N = 0; N < 5; N++) 
    { // Recorrer toda la matriz
      // Mostrar el nombre del veh�culo y los datos generales
      cout << X[N]->Nombre() << ", " 
	   << X[N]->NumeroDeEjes() << ", "
	   << X[N]->NumeroDeRuedas() << ", "
	   << X[N]->FuncionaConMotor();
      
      // Si el veh�culo es una bicicleta de monta�a
      if(typeid(*X[N]) == typeid(TBicicletaMontana))
	// hacer un moldeado de tipo para acceder al
	// m�todo NumeroDeCambios
	cout << ", con " 
	     << ((TBicicletaMontana *)X[N])->NumeroDeCambios() 
	     << " cambios";
      
      // Si el veh�culo es un autom�vil
      if(typeid(*X[N]) == typeid(TAutomovil))
	// hacer un moldeado de tipo para acceder al
	// m�todo Potencia
	cout << ", con " 
	     << ((TAutomovil *)X[N])->Potencia()
	     << " caballos";
      
      cout << endl; // Saltar a la siguiente l�nea
    }
  
  for(int N=0; N<5; N++)
    delete X[N]; // destruimos los objetos
  
  return 0;
}
//---------------------------------------------------------------------------
