#include <iostream>
#include "Punto.h"

using namespace std;

void TPunto::Dibuja()
{
  cout << X << ", " << Y
       << ", " << (int )Color << endl;
}

void TPunto::Inicializa(int PX, int PY, unsigned char PColor)
{
  X=PX;
  Y=PY;
  Color=PColor;
}

int main(...)
{
  TPunto UnPunto; // Creamos la variable

  // Inicializar el objeto
  UnPunto.Inicializa(120, 48, 5);

  UnPunto.Dibuja(); // Y llamamos a Dibuja()

  return 0;
}
