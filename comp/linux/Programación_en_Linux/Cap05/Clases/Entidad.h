// Clase b�sica para almacenar las 
// coordenadas de un punto
class TPunto
{
 public:
  int X, Y;
};

// Esta clase es gen�rica y almacena los datos
// m�nimos de cualquier entidad gr�fica
class TEntidad
{
 private:
  TPunto Posicion; // Posici�n
  unsigned char Color; // y color de la entidad 
 public:
  // El constructor recibir� tres par�metros
  TEntidad(int x, int y, unsigned char c);
  // Esta funci�n devolver� el nombre del objeto
  static char * Clase();

  int PosX(); // Funciones que devolver� las
  int PosY(); // coordenadas del punto
};

// Esta clase est� especializada para obketos
// c�rculo y hereda las caracter�sticas de
// la anterior
class TCirculo : public TEntidad
{
 private:
  int NRadio; // Para guardar el radio
 public:
  // Constructor
  TCirculo(int x, int y, unsigned char c, int r);
  static char * Clase(); // Devuelve el nombre de la clase
  int Radio(); // Devuelve el radio del c�rculo
};
