//---------------------------------------------------------------------------
#ifndef VehiculoH
#define VehiculoH

// La clase TVehiculo ser� la base de todas
// las dem�s clases
class TVehiculo
{
 private: // Miembros de datos, privados
  short NEjes, NRuedas;
  bool TieneMotor;
 public: // M�todos p�blicos
  // El constructor toma tres par�metros
  TVehiculo(short Ejes, short Ruedas, bool Motor);

  /* Esta funci�n es virtual abstracta, lo que significa
     que no tiene implementaci�n en esta clase, y
     tendr� que ser necesariamente implementada por
     los descendientes de TVehiculo */
  virtual char* Nombre() = 0;
  
  // Estas funciones son informativas, y servir�n para
  // obtener algunos datos del veh�culo
  short NumeroDeEjes();
  short NumeroDeRuedas();
  bool FuncionaConMotor();
};

// La clase TBicicleta est� derivada de TVehiculo,
// heredando sus miembros y m�todos
class TBicicleta : public TVehiculo
{
 public:
  TBicicleta(); // El constructor no necesita par�metros
  char* Nombre(); // Se redefine el m�todo Nombre
};

// La clase TBicicletaMontana est� derivada de TBicicleta
class TBicicletaMontana : public TBicicleta
{
 private:
  short NCambios; // N�mero de cambios
 public:
  TBicicletaMontana(short Cambios); // Constructor
  char* Nombre(); // Redefinimos el m�todo Nombre
  short NumeroDeCambios(); // Devuelve el n�mero de cambios
};

// La clase TAutomovil est� derivada de TVehiculo, y no
// de TBicicleta, abriendo as� una nueva rama en la jerarqu�a
class TAutomovil : public TVehiculo
{
 private:
  short NCambios; // N�mero de cambios
  bool CAutomatico; // Cambio autom�tico
  int NPotencia; // Potencia en caballos
 public:
  // El constructor recibe los datos adicionales necesarios
  TAutomovil(short Cambios, bool Automatico, int Potencia);
  char* Nombre(); // Redefinimos el m�todo nombre
  short NumeroDeCambios(); // Y a�adimos los m�todos necesarios
  bool EsAutomatico(); // para devolver el resto de los datos
  int Potencia();
};

//---------------------------------------------------------------------------
#endif

