//---------------------------------------------------------------------------
#include "Vehiculo.h"

// Implementación del constructorde la clase TVehiculo
TVehiculo::TVehiculo(short Ejes, short Ruedas, bool Motor)
{
  NEjes = Ejes;              // Guardar los datos facilitados
  NRuedas = Ruedas;
  TieneMotor = Motor;
}

// Implementación del método NumeroDeEjes
short TVehiculo::NumeroDeEjes()
{
  return NEjes;
}

// Implementación del método NumeroDeRuedas
short TVehiculo::NumeroDeRuedas()
{
  return NRuedas;
}

// Implementación del método FuncionaConMotor
bool TVehiculo::FuncionaConMotor()
{
  return TieneMotor;
}

/* Implementación del constructor del tipo TBicicleta,
   que no recibe parámetros y fija los datos básicos
   de este tipo de vehículo. */
TBicicleta::TBicicleta() : TVehiculo(2, 2, false)
{}

// Implementación del método Nombre, que devuelve
// el nombre del vehículo.
char* TBicicleta::Nombre()
{
  return "Bicicleta";
}

// Constructor del tipo TBicicletaMontana, que recibe
// como parámetro el número de cambios
TBicicletaMontana::TBicicletaMontana(short Cambios)
{
  NCambios = Cambios; // Guardar el dato adicional recibido
}

// Implementación del método Nombre
char* TBicicletaMontana::Nombre()
{
  return "Bicicleta de montaña";
}

// Implementación del método NumeroDeCambios
short TBicicletaMontana::NumeroDeCambios()
{
  return NCambios;
}

// Implementación del constructor del tipo TAutomovil
TAutomovil::TAutomovil(short Cambios, bool Automatico, int Potencia)
  : TVehiculo(2, 4, true)
{
  NCambios = Cambios; // Preservar el resto de los datos
  CAutomatico = Automatico;
  NPotencia = Potencia;
}

// Implementación del método Nombre de TAutomovil
char* TAutomovil::Nombre()
{
  return "Automóvil";
}

// Implementación del método NumeroDeCambios de TAutomovil
short TAutomovil::NumeroDeCambios()
{
  return NCambios;
}

// Implementación del método EsAutomatico de TAutomovil
bool TAutomovil::EsAutomatico()
{
  return CAutomatico;
}

// Implementación del método Potencia de TAutomovil
int TAutomovil::Potencia()
{
  return NPotencia;
}
//---------------------------------------------------------------------------

