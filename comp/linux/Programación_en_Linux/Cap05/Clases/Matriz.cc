#include <iostream>
#include "Matriz.h"

using namespace std;

TMatriz::TMatriz(int Num)
{
  // Conservar el n�mero de elementos
  NumElementos=Num;
  // y asignar la memoria necesaria
  Elementos=new int[NumElementos];
}

void TMatriz::Asigna(int Num, int Valor)
{
  Elementos[Num]=Valor;
}

int TMatriz::Valor(int Num)
{
  return Elementos[Num];
}

TMatriz::~TMatriz()
{
  // Liberar la memoria asignada
  delete [] Elementos;
}

int main(...)
{
  TMatriz Tabla(20);

  Tabla.Asigna(5,18);
  cout << Tabla.Valor(5) << endl;

  return 0;
}
