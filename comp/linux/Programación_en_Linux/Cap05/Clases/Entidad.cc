#include <iostream>

using namespace std;

#include "Entidad.h"

// --- M�todos de TEntidad ---

TEntidad::TEntidad(int x, int y, unsigned char c)
{
  Posicion.X=x;
  Posicion.Y=y;
  Color=c;
}

char * TEntidad::Clase()
{
  return "TEntidad";
}

int TEntidad::PosX()
{
  return Posicion.X;
}

int TEntidad::PosY()
{
  return Posicion.Y;
}

// --- M�todos de TCirculo ---

TCirculo::TCirculo(int x, int y, unsigned char c, int r)
  : TEntidad(x,y,c)
{
  NRadio=r;
}

char * TCirculo::Clase()
{
  return "TCirculo";
}

int TCirculo::Radio()
{
  return NRadio;
}

int main(...)
{
  TEntidad *Elemento;

  // Elemento apunta a un objeto TEntidad
  Elemento=new TEntidad(120,80,5);
  cout << Elemento->Clase() << endl;
  delete Elemento;

  // Elemento apunta a un objeto TCirculo, que en
  // su interior contiene un tipo TEntidad
  Elemento=new TCirculo(120,80,20,5);
  cout << Elemento->Clase() << endl;
  delete Elemento;

  TCirculo Circulo(120,80,50,2);

  // El tipo Circulo no tiene definidos los m�todos
  // PosX y PosY, pero los ha heredado de TEntidad
  cout << "Circulo.PosX=" << Circulo.PosX() << endl
       << "Circulo.PosY=" << Circulo.PosY() << endl
       << "Circulo.Radio=" << Circulo.Radio() << endl
       << "Circulo.Clase=" << Circulo.Clase() << endl;

  return 0;
}
