#include <iostream>
#include "MatrizSobrecarga.h"

using namespace std;

TMatriz::TMatriz(int Num)
{
  // Conservar el n�mero de elementos
  NumElementos=Num;
  // y asignar la memoria necesaria
  Elementos=new int[NumElementos];
}

int & TMatriz::operator[](int Num)
{
  return Elementos[Num];
}

TMatriz::~TMatriz()
{
  // Liberar la memoria asignada
  delete [] Elementos;
}

int main(...)
{
  TMatriz Tabla(20);

  Tabla[5]=18;
  cout << Tabla[5] << endl;

  return 0;
}
