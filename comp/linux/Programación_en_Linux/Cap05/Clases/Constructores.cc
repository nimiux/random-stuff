#include <iostream>

using namespace std;

#include "Constructores.h"

TObjeto::TObjeto(int N)
{
  Dato=N;
}

TObjeto::TObjeto(TObjeto &OtroObjeto)
{
  this->Dato=OtroObjeto.Dato;
}


int main(...)
{
  TObjeto MiObjeto(5), *MiOtroObjeto;

  MiOtroObjeto = new TObjeto(MiObjeto);

  cout << MiOtroObjeto->Dato << endl;

  return 0;
}
