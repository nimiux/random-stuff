#include <iostream>

using namespace std;

namespace Ambito1
{
  int N;
}

namespace Ambito2
{
  int N;
}

namespace 
{
  int N, M;
  int F(int N) { return N*N; }
}


int main(...)
{
  using Ambito1::N;

  N=5; // Variable N del primer namespace
  Ambito2::N=3; // Esta es otra variable N

  cout << F(5);

  return 0;
}
