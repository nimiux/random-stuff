#include <iostream>

using namespace std;

// Estructura para mantener
struct TTelefono // un tel�fono
{
  char Prefijo[4], Numero[7]; // con su prefijo
  bool Personal; // y el indicador asociado
};

struct TFichaAgenda
{
  char Nombre[20]; // Nombre de la persona
  char Direccion[30]; // su direcci�n
  TTelefono Telefonos[5]; // y sus tel�fonos
};

typedef TFichaAgenda Agenda[100];

int main(...)
{
  Agenda Fichas; // definimos la matriz

  // y asignamos valores a los miembros
  strcpy(Fichas[0].Nombre, "Pedro Garc�a");
  strcpy(Fichas[0].Direccion, "Avda. de la Paz, 24");
  strcpy(Fichas[0].Telefonos[0].Prefijo, "953");
  strcpy(Fichas[0].Telefonos[0].Numero, "342345");
  Fichas[0].Telefonos[0].Personal = true;
  strcpy(Fichas[0].Telefonos[1].Prefijo, "953");
  strcpy(Fichas[0].Telefonos[1].Numero, "346224");
  Fichas[0].Telefonos[1].Personal = false;

  cout << Fichas[0].Nombre << endl
       << Fichas[0].Direccion << endl
       << Fichas[0].Telefonos[0].Prefijo
       << "/" << Fichas[0].Telefonos[0].Numero << endl
       << Fichas[0].Telefonos[0].Personal << endl;
}
