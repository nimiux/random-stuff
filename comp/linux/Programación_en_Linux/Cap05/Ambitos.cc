void MuestraVariable();

int N;

// Funcion Espera
void Espera()
{
  N = 5; // asignamos un valor
}

// Funcion Duplica
void Duplica()
{
  N = N * 2; // multiplicamos por dos el valor
}

int main(...)
{
  Espera();
  Duplica();

  MuestraVariable();
  return 0;
}
