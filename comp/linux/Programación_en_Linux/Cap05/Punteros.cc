
int main(...)
{
  int N;

  /* Esta variable es un puntero
     a un valor de tipo int, pero
     no una variable de ese tipo */

  int * PunteroN;

  PunteroN = & N;

  N=5;
  *PunteroN=5;

  return 0;
}


