#include <iostream>
#include <time.h>

using namespace std;

void Espera(int Segundos)
{
  clock_t Ahora;

  // Tomamos una referencia de tiempo
  Ahora = clock();

  // No salir del bucle hasta que hayan
  // transcurrido cinco segundos
  do ; while(((double )(clock()-Ahora))/CLOCKS_PER_SEC<Segundos);
}

int Cuadrado(int Base)
{
  return Base*Base;
}

int main(...)
{
  cout << Cuadrado(20) << endl;

  cout << "Primer mensaje" << endl;
  Espera(5);
  cout << "Cinco segundos despu�s" << endl;
  Espera(10);
  cout << "Diez segundos despu�s" << endl;

  return 0;
}
