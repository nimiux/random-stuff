#include <stdio.h>
#include <arpa/inet.h>
#include <netinet/in.h>

int main()
{
  // Direcci�n en formato a.b.c.d
  char DireccionStr[256];
  // Direcci�n como n�mero de 32 bits
  struct in_addr Direccion;

  // Solicitamos la IP a convertir
  printf("\nIntroduzca la IP: ");
  scanf(" %s", DireccionStr);

  // Si la conversi�n puede efectuarse
  if(inet_aton(DireccionStr, &Direccion))
    // mostramos el n�mero obtenido
    printf("\nResultado: %d\n", Direccion.s_addr);
  else // en caso contrario un error
    puts("La IP introducida no es correcta\n");

  // Manipulamos la direcci�n
  Direccion.s_addr+=10;
  // y convertimos de nuevo a cadena
  printf("\nNueva IP: %s\n\n", inet_ntoa(Direccion));

  return 0;
}

