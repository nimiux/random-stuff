#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

int main()
{
  // Puerto donde esperamos encontrar el
  // servidor a la escucha
  const int Puerto=IPPORT_USERRESERVED+10;

  // Direcci�n del servidor
  struct sockaddr_in Servidor;

  // Identificador del socket
  int IdCliente;

  // Para leer los datos
  char FechaHora[256];

  // Preparamos la estructura con la
  // direcci�n y puerto del servidor
  Servidor.sin_family=AF_INET;
  inet_aton("192.168.0.6", &Servidor.sin_addr);
  Servidor.sin_port=htons(Puerto);

  // Creamos el socket para comunicarnos
  IdCliente=socket(PF_INET,SOCK_STREAM,0);
  // y comprobamos que es v�lido
  if(IdCliente==-1) {
    puts("Fallo en la llamada a socket()");
    exit(-1);
  }

  // Conectamos comprobando un posible fallo
  if(connect(IdCliente,(struct sockaddr*)&Servidor,sizeof(Servidor))==-1) {
    puts("Fallo en la llamada a connect()");
    exit(-1);
  }

  // Leemos los datos que nos 
  // facilita el servidor
  recv(IdCliente,FechaHora,256,0);

  // los mostramos
  printf("Dato recibido: %s", FechaHora);

  // y cerramos el socket
  close(IdCliente);

  return 0;
}

