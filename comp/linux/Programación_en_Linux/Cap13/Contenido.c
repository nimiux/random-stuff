#include <X11/Xlib.h>

int main()
{
  // Datos de conexi�n
  Display* Pantalla;
  // e identificador de la ventana
  Window Ventana;

  // Variable para la se�al de cierre
  Atom SenalCierre;
  // para recoger eventos
  XEvent Evento;

  int Fin=0; // Control del bucle de mensajes

  // Para los atributos de la ventana
  XSetWindowAttributes AtributosVentana;

  // Para obtener el color del fondo
  XColor ColorFondo,ColorFondoExacto;

  GC Contexto; // Contexto gr�fico
  Font TipoLetra; // Tipo de letra

  // Para enumerar los tipos de letra
  char **Caminos;
  int NCaminos;

  // Establecemos la conexi�n utilizando la cadena
  // de conexi�n indicada por la variable DISPLAY
  Pantalla=XOpenDisplay(NULL);

  // Comprobamos que tenemos la conexi�n
  if(!Pantalla) {
    puts("Fallo en la llamada a XOpenDisplay()");
    exit(-1);
  }

  // Obtenemos el color azul
  XAllocNamedColor(Pantalla,XDefaultColormap(Pantalla,0),
		   "blue",&ColorFondo,&ColorFondoExacto);

  // Fijamos los atributos
  AtributosVentana.background_pixel=ColorFondo.pixel;
  AtributosVentana.event_mask=ButtonPressMask|KeyPressMask;

  // Y creamos la ventana
  Ventana=XCreateWindow(
     Pantalla,XDefaultRootWindow(Pantalla),
     0,0,320,200,2,CopyFromParent,InputOutput,
     CopyFromParent,CWBackPixel|CWEventMask,
     &AtributosVentana);

  // Si no se ha creado
  if(!Ventana) {
    // comunicar el fallo
    puts("Fallo al crear la ventana");
    XCloseDisplay(Pantalla);
    exit(-1);
  }

  // Obtener los directorios con fuentes
  Caminos=XGetFontPath(Pantalla,&NCaminos);
  while(NCaminos) // y mostrarlos en consola
    puts(Caminos[--NCaminos]);

  // Establecemos el texto que aparece en
  // el borde
  XmbSetWMProperties(Pantalla,Ventana,"Ventana con contenido",
		     "Contenido",NULL,0,NULL,NULL,NULL);

  // Obtenemos el atom de WM_DELETE_WINDOW
  SenalCierre=XInternAtom(Pantalla,"WM_DELETE_WINDOW",True);
  // y lo establecemos en la propiedad WM_PROTOCOLS
  XSetWMProtocols(Pantalla,Ventana,&SenalCierre,1);

  // Generar visualmente la ventana
  XMapWindow(Pantalla,Ventana);

  // Creamos el contexto
  Contexto=XCreateGC(Pantalla,Ventana,0,NULL);
  // Obtenemos el color blanco
  XAllocNamedColor(Pantalla,XDefaultColormap(Pantalla,0),
		   "white",&ColorFondo,&ColorFondoExacto);
  // y establecerlo como color de dibujo
  XSetForeground(Pantalla,Contexto,ColorFondo.pixel);
  // Obtener un tipo de letra
  TipoLetra=XLoadFont(Pantalla,"12x24");
  // y activalor en el contexto
  XSetFont(Pantalla,Contexto,TipoLetra);

  // Mientras no se llegue al fin
  while(!Fin) {
    // Recogemos un evento
    XNextEvent(Pantalla,&Evento);
    // y seg�n su tipo
    switch(Evento.type) {
    case ClientMessage: // Si es un mensaje de la ventana
      // y es la se�a de cierre
       if(Evento.xclient.data.l[0]==SenalCierre)
	 Fin=1; // salidmos
       break;
    case ButtonPress: // Se ha pulsado un bot�n del rat�n
       // Dibujamos un c�rculo relleno en esa posici�n
      XFillArc(Pantalla,Ventana,Contexto,Evento.xbutton.x,Evento.xbutton.y,
	       10,10,0,360*64);
      break;
    case KeyPress: // Se ha pulsado una tecla
      // La escribimos en pantalla
      XDrawString(Pantalla,Ventana,Contexto,Evento.xkey.x,Evento.xkey.y,
		  XKeysymToString(XKeycodeToKeysym(Pantalla,
				  Evento.xkey.keycode,0)),1);
      break;
    }
  }

  // Destruimos la ventana
  XDestroyWindow(Pantalla,Ventana);

  // Cerramos la conexi�n
  XCloseDisplay(Pantalla);

  return 0;
}
