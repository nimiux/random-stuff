#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/cursorfont.h>

int main()
{
  // Datos de conexi�n
  Display* Pantalla;
  // e identificador de la ventana
  Window Ventana;

  // Variable para la se�al de cierre
  Atom SenalCierre;
  // para recoger eventos
  XEvent Evento;

  int Fin=0; // Control del bucle de mensajes

  // Para los atributos de la ventana
  XSetWindowAttributes AtributosVentana;

  // Para obtener el color del fondo
  XColor ColorFondo,ColorFondoExacto;

  // Establecemos la conexi�n utilizando la cadena
  // de conexi�n indicada por la variable DISPLAY
  Pantalla=XOpenDisplay(NULL);

  // Comprobamos que tenemos la conexi�n
  if(!Pantalla) {
    puts("Fallo en la llamada a XOpenDisplay()");
    exit(-1);
  }

  // Obtenemos el color azul
  XAllocNamedColor(Pantalla,XDefaultColormap(Pantalla,0),
		   "blue",&ColorFondo,&ColorFondoExacto);

  // Fijamos los atributos
  AtributosVentana.background_pixel=ColorFondo.pixel;
  AtributosVentana.event_mask=ButtonPressMask|KeyPressMask;
  AtributosVentana.cursor=XCreateFontCursor(Pantalla,XC_mouse);

  // Y creamos la ventana
  Ventana=XCreateWindow(
     Pantalla,XDefaultRootWindow(Pantalla),
     0,0,320,200,2,CopyFromParent,InputOutput,
     CopyFromParent,CWBackPixel|CWEventMask|CWCursor,
     &AtributosVentana);

  // Si no se ha creado
  if(!Ventana) {
    // comunicar el fallo
    puts("Fallo al crear la ventana");
    XCloseDisplay(Pantalla);
    exit(-1);
  }

  // Establecemos el texto que aparece en
  // el borde
  XmbSetWMProperties(Pantalla,Ventana,"Atributos de una ventana",
		     "Atributos",NULL,0,NULL,NULL,NULL);

  // Obtenemos el atom de WM_DELETE_WINDOW
  SenalCierre=XInternAtom(Pantalla,"WM_DELETE_WINDOW",True);
  // y lo establecemos en la propiedad WM_PROTOCOLS
  XSetWMProtocols(Pantalla,Ventana,&SenalCierre,1);

  // Generar visualmente la ventana
  XMapWindow(Pantalla,Ventana);

  // Mientras no se llegue al fin
  while(!Fin) {
    // Recogemos un evento
    XNextEvent(Pantalla,&Evento);
    // y seg�n su tipo
    switch(Evento.type) {
    case ClientMessage: // Si es un mensaje de la ventana
      // y es la se�a de cierre
       if(Evento.xclient.data.l[0]==SenalCierre)
	 Fin=1; // salidmos
       break;
    case ButtonPress: // Se ha pulsado un bot�n del rat�n
      // enviamos la ventana al fondo
      XLowerWindow(Pantalla,Ventana);
      break;
    case KeyPress: // Se ha pulsado una tecla
      Fin=1;    // terminamos
    }
  }

  // Destruimos la ventana
  XDestroyWindow(Pantalla,Ventana);

  // Cerramos la conexi�n
  XCloseDisplay(Pantalla);

  return 0;
}
