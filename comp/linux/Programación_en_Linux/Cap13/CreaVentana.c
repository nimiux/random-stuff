#include <X11/Xlib.h>

int main()
{
  Display* Pantalla;
  Window Ventana;

  // Establecemos la conexi�n utilizando la cadena
  // de conexi�n indicada por la variable DISPLAY
  Pantalla=XOpenDisplay(NULL);

  // Comprobamos que tenemos la conexi�n
  if(!Pantalla) {
    puts("Fallo en la llamada a XOpenDisplay()");
    exit(-1);
  }

  // Creamos una ventana con un borde blanco
  // y fondo negro
  Ventana=XCreateSimpleWindow(
     Pantalla,XDefaultRootWindow(Pantalla),
     0,0,320,200,2,
     XWhitePixel(Pantalla,XDefaultScreen(Pantalla)),
     XBlackPixel(Pantalla,XDefaultScreen(Pantalla)));

  // Si no se ha creado
  if(!Ventana) {
    // comunicar el fallo
    puts("Fallo al crear la ventana");
    XCloseDisplay(Pantalla);
    exit(-1);
  }

  // Generar visualmente la ventana
  XMapWindow(Pantalla,Ventana);
  // y enviarla al servidor X
  XFlush(Pantalla);

  // Esperamos
  sleep(10);

  // antes de destruirla
  XDestroyWindow(Pantalla,Ventana);

  // Cerramos la conexi�n
  XCloseDisplay(Pantalla);

  return 0;
}
