#include <X11/Xlib.h>

int main()
{
  // Datos de conexi�n
  Display* Pantalla;
  // e identificador de las ventanas
  Window Ventana, Hija1, Hija2;

  // Variable para la se�al de cierre
  Atom SenalCierre;
  // para recoger eventos
  XEvent Evento;

  int Fin=0; // Control del bucle de mensajes

  GC Contexto; // Contexto gr�fico

  Font TipoLetra; // Tipo de letra

  // Para seleccionar un color
  XColor Color,ColorExacto;

  char *Mensaje; // Mensaje de notificaci�n

  // Establecemos la conexi�n utilizando la cadena
  // de conexi�n indicada por la variable DISPLAY
  Pantalla=XOpenDisplay(NULL);

  // Comprobamos que tenemos la conexi�n
  if(!Pantalla) {
    puts("Fallo en la llamada a XOpenDisplay()");
    exit(-1);
  }

  // Creamos una ventana con un borde blanco
  // y fondo negro
  Ventana=XCreateSimpleWindow(
     Pantalla,XDefaultRootWindow(Pantalla),
     0,0,320,200,2,
     XWhitePixel(Pantalla,XDefaultScreen(Pantalla)),
     XWhitePixel(Pantalla,XDefaultScreen(Pantalla)));

  // Si no se ha creado
  if(!Ventana) {
    // comunicar el fallo
    puts("Fallo al crear la ventana");
    XCloseDisplay(Pantalla);
    exit(-1);
  }

  // Obtenemos el color gris
  XAllocNamedColor(Pantalla,XDefaultColormap(Pantalla,0),
		    "gray",&Color,&ColorExacto);

  // Para usarlo como fondo de dos ventanas hija que
  // aparecer�n como dos botones, con un borde negro
  Hija1=XCreateSimpleWindow(
    Pantalla,Ventana,10,10,120,48,
    3,XBlackPixel(Pantalla,XDefaultScreen(Pantalla)),
    Color.pixel);

  // Segunda ventana hija
  Hija2=XCreateSimpleWindow(
    Pantalla,Ventana,170,10,120,48,
    3,XBlackPixel(Pantalla,XDefaultScreen(Pantalla)),
    Color.pixel);

  // Obtenemos el atom de WM_DELETE_WINDOW
  SenalCierre=XInternAtom(Pantalla,"WM_DELETE_WINDOW",True);
  // y lo establecemos en la propiedad WM_PROTOCOLS
  XSetWMProtocols(Pantalla,Ventana,&SenalCierre,1);

  // Solicitamos notificaci�n de eventos de pulsaci�n
  // de botones del rat�n y teclado
  XSelectInput(Pantalla,Ventana,ButtonPressMask|KeyPressMask|ExposureMask);
  // Para las ventanas hija s�lo notificaci�n de pulsaci�n
  // con los botones del rat�n
  XSelectInput(Pantalla,Hija1,ButtonPressMask);
  XSelectInput(Pantalla,Hija2,ButtonPressMask);

  // Generar visualmente las ventanas
  XMapWindow(Pantalla,Ventana);
  XMapWindow(Pantalla,Hija1);
  XMapWindow(Pantalla,Hija2);

  // Mientras no se llegue al fin
  while(!Fin) {
    // Recogemos un evento
    XNextEvent(Pantalla,&Evento);
    // y seg�n su tipo
    switch(Evento.type) {
    case ClientMessage: // Si es un mensaje de la ventana
      // y es la se�a de cierre
       if(Evento.xclient.data.l[0]==SenalCierre)
	 Fin=1; // salidmos
       break;
    case ButtonPress: // Se ha pulsado un bot�n del rat�n
      // asignamos un mensaje u otro seg�n la
      // ventana sobre la que se haya hecho clic
      if(Evento.xbutton.window==Hija1)
	Mensaje="Has pulsado el primer bot�n";
      else if(Evento.xbutton.window==Hija2)
	Mensaje="Has pulsado el segundo bot�n";
      else
	Mensaje="Has pulsado sobre la ventana madre";

      // Creamos el contexto
      Contexto=XCreateGC(Pantalla,Ventana,0,NULL);
      // establecemos el color blanco
      XSetForeground(Pantalla,Contexto,XWhitePixel(Pantalla,0));
      // para borrar un posible mensaje previo
      XFillRectangle(Pantalla,Ventana,Contexto,10,100,300,100);
      // y el color negro
      XSetForeground(Pantalla,Contexto,XBlackPixel(Pantalla,0));
      // para mostrar el nuevo mensaje
      XDrawString(Pantalla,Ventana,Contexto,10,120,Mensaje,strlen(Mensaje));
      // liberamos el contexto
      XFreeGC(Pantalla,Contexto);
      break;
    case KeyPress: // Se ha pulsado una tecla
      Fin=1;    // terminamos
    case Expose: // Hay que dibujar el contenido de la ventana
      // Creamos el contexto
      Contexto=XCreateGC(Pantalla,Hija1,0,NULL);
      // obtenemos el tipo de letra
      TipoLetra=XLoadFont(Pantalla,"12x24");
      // lo establecemos
      XSetFont(Pantalla,Contexto,TipoLetra);
      // Fijamos los colores de primer plano y fondo
      XSetForeground(Pantalla,Contexto,XBlackPixel(Pantalla,XDefaultScreen(Pantalla)));
      XSetBackground(Pantalla,Contexto,XWhitePixel(Pantalla,XDefaultScreen(Pantalla)));
      // e imprimimos los t�tulos de los botones
      XDrawString(Pantalla,Hija1,Contexto,15,35,"Bot�n 1",7);
      XDrawString(Pantalla,Hija2,Contexto,15,35,"Bot�n 2",7);
      // Liberamos el contexto
      XFreeGC(Pantalla,Contexto);
      break;
    }
  }

  // Destruimos las ventanas
  XDestroyWindow(Pantalla,Hija1);
  XDestroyWindow(Pantalla,Hija2);
  XDestroyWindow(Pantalla,Ventana);

  // Cerramos la conexi�n
  XCloseDisplay(Pantalla);

  return 0;
}
