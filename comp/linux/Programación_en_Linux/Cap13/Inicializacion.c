#include <X11/Xlib.h>

int main()
{
  Display* Pantalla;

  // Establecemos la conexi�n utilizando la cadena
  // de conexi�n indicada por la variable DISPLAY
  Pantalla=XOpenDisplay(NULL);

  // Comprobamos que tenemos la conexi�n
  if(!Pantalla) {
    puts("Fallo en la llamada a XOpenDisplay()");
    exit(-1);
  }

   // Mostramos algunos datos
  printf("\nDisplay: %s\nServidor X: %s "
         "- %d\n%dX%d con %d bits de color\n\n",
         XDisplayString(Pantalla),
	 XServerVendor(Pantalla),
	 XVendorRelease(Pantalla),
	 XDisplayWidth(Pantalla,DefaultScreen(Pantalla)),
	 XDisplayHeight(Pantalla,DefaultScreen(Pantalla)),
	 XDefaultDepth(Pantalla,DefaultScreen(Pantalla)));

  // Cerramos la conexi�n
  XCloseDisplay(Pantalla);

  return 0;
}
