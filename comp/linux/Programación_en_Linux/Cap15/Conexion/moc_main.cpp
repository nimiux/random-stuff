/****************************************************************************
** MiWidget meta object code from reading C++ file 'main.h'
**
** Created: Mon Mar 10 06:11:52 2003
**      by: The Qt MOC ($Id:  qt/moc_yacc.cpp   3.0.5   edited Jul 8 12:26 $)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#undef QT_NO_COMPAT
#include "main.h"
#include <qmetaobject.h>
#include <qapplication.h>

#include <private/qucomextra_p.h>
#if !defined(Q_MOC_OUTPUT_REVISION) || (Q_MOC_OUTPUT_REVISION != 19)
#error "This file was generated using the moc from 3.0.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

const char *MiWidget::className() const
{
    return "MiWidget";
}

QMetaObject *MiWidget::metaObj = 0;
static QMetaObjectCleanUp cleanUp_MiWidget;

#ifndef QT_NO_TRANSLATION
QString MiWidget::tr( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "MiWidget", s, c, QApplication::DefaultCodec );
    else
	return QString::fromLatin1( s );
}
#ifndef QT_NO_TRANSLATION_UTF8
QString MiWidget::trUtf8( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "MiWidget", s, c, QApplication::UnicodeUTF8 );
    else
	return QString::fromUtf8( s );
}
#endif // QT_NO_TRANSLATION_UTF8

#endif // QT_NO_TRANSLATION

QMetaObject* MiWidget::staticMetaObject()
{
    if ( metaObj )
	return metaObj;
    QMetaObject* parentObject = QWidget::staticMetaObject();
    static const QUParameter param_slot_0[] = {
	{ "NV", &static_QUType_int, 0, QUParameter::In }
    };
    static const QUMethod slot_0 = {"NuevoValor", 1, param_slot_0 };
    static const QUMethod slot_1 = {"Incrementar", 0, 0 };
    static const QMetaData slot_tbl[] = {
	{ "NuevoValor(int)", &slot_0, QMetaData::Public },
	{ "Incrementar()", &slot_1, QMetaData::Public }
    };
    static const QUParameter param_signal_0[] = {
	{ 0, &static_QUType_int, 0, QUParameter::In }
    };
    static const QUMethod signal_0 = {"CambioValor", 1, param_signal_0 };
    static const QMetaData signal_tbl[] = {
	{ "CambioValor(int)", &signal_0, QMetaData::Public }
    };
    metaObj = QMetaObject::new_metaobject(
	"MiWidget", parentObject,
	slot_tbl, 2,
	signal_tbl, 1,
#ifndef QT_NO_PROPERTIES
	0, 0,
	0, 0,
#endif // QT_NO_PROPERTIES
	0, 0 );
    cleanUp_MiWidget.setMetaObject( metaObj );
    return metaObj;
}

void* MiWidget::qt_cast( const char* clname )
{
    if ( !qstrcmp( clname, "MiWidget" ) ) return (MiWidget*)this;
    return QWidget::qt_cast( clname );
}

// SIGNAL CambioValor
void MiWidget::CambioValor( int t0 )
{
    activate_signal( staticMetaObject()->signalOffset() + 0, t0 );
}

bool MiWidget::qt_invoke( int _id, QUObject* _o )
{
    switch ( _id - staticMetaObject()->slotOffset() ) {
    case 0: NuevoValor((int)static_QUType_int.get(_o+1)); break;
    case 1: Incrementar(); break;
    default:
	return QWidget::qt_invoke( _id, _o );
    }
    return TRUE;
}

bool MiWidget::qt_emit( int _id, QUObject* _o )
{
    switch ( _id - staticMetaObject()->signalOffset() ) {
    case 0: CambioValor((int)static_QUType_int.get(_o+1)); break;
    default:
	return QWidget::qt_emit(_id,_o);
    }
    return TRUE;
}
#ifndef QT_NO_PROPERTIES

bool MiWidget::qt_property( int _id, int _f, QVariant* _v)
{
    return QWidget::qt_property( _id, _f, _v);
}
#endif // QT_NO_PROPERTIES
