// Archivos de cabecera Qt
#include "main.h"
#include <qlcdnumber.h> 
#include <qpushbutton.h>
#include <qmainwindow.h>

int main(int argc,char **argv)
{
   // Creamos el objeto QApplication
  QApplication Aplicacion(argc,argv);
  // y una ventana
  QMainWindow Ventana;

  // Introducimos un QLCDNumber
  QLCDNumber Numero(&Ventana);
  // estaleciendo posici''on y tama�o
  Numero.setGeometry(0,0,180,45);

  // Introducimos un bot�n para incrementar
  QPushButton Incrementar("Incrementar",&Ventana);
  Incrementar.setGeometry(100,65,90,32);

  // Y otro para salir
  QPushButton Salir("Salir",&Ventana);
  Salir.setGeometry(0,65,90,32);

  // Creamos un objeto de nuestra clase
  MiWidget Objeto;

  // Conectamos el cambio del valor de nuestro
  // control con el receptor 'display' del
  // QLCDNumber
  QObject::connect(&Objeto,SIGNAL(CambioValor(int)),
	  &Numero,SLOT(display(int)));

  // Conectamos la pulsaci�n de uno de los
  // botones con el receptor Incrementar de
  // nuestro objeto
  QObject::connect(&Incrementar,SIGNAL(clicked()),
		   &Objeto,SLOT(Incrementar()));

  // Finalmente conectamos la pulsaci�n del
  // segundo bot�n con el receptor quit() de
  // QApplication
  QObject::connect(&Salir,SIGNAL(clicked()),
		   qApp,SLOT(quit()));

  // Establecemos la ventana como elemento
  // principal del programa
  qApp->setMainWidget(&Ventana);

  // y la mostramos
  Ventana.show();

  // Iniciamos el proceso de eventos
  return qApp->exec();
}
