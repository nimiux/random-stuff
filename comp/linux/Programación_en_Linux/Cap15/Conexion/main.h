// Definiciones necesarias
#include <qapplication.h>

// Creamos nuestra clase de Widget
class MiWidget : public QWidget
{
   Q_OBJECT
public:
   // El constructor poner a cero valor
   MiWidget() { valor=0; }

public slots:
  // Tenemos un receptor que permite
  // cambiar el valor
  void NuevoValor(int NV) {
      valor=NV;
      // generar la se�al
      emit CambioValor(NV);
  }

  // Este receptor incrementa
  // el valor
  void Incrementar() {
   NuevoValor(++valor);
 }

signals:
  // Se�al a generar cuando se
  // modifica el valor
  void CambioValor(int);

private:
   int valor;
};
