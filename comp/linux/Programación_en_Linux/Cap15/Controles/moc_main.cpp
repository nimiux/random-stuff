/****************************************************************************
** MiClaseVentana meta object code from reading C++ file 'main.h'
**
** Created: Tue Mar 11 04:58:12 2003
**      by: The Qt MOC ($Id:  qt/moc_yacc.cpp   3.0.5   edited Jul 8 12:26 $)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#undef QT_NO_COMPAT
#include "main.h"
#include <qmetaobject.h>
#include <qapplication.h>

#include <private/qucomextra_p.h>
#if !defined(Q_MOC_OUTPUT_REVISION) || (Q_MOC_OUTPUT_REVISION != 19)
#error "This file was generated using the moc from 3.0.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

const char *MiClaseVentana::className() const
{
    return "MiClaseVentana";
}

QMetaObject *MiClaseVentana::metaObj = 0;
static QMetaObjectCleanUp cleanUp_MiClaseVentana;

#ifndef QT_NO_TRANSLATION
QString MiClaseVentana::tr( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "MiClaseVentana", s, c, QApplication::DefaultCodec );
    else
	return QString::fromLatin1( s );
}
#ifndef QT_NO_TRANSLATION_UTF8
QString MiClaseVentana::trUtf8( const char *s, const char *c )
{
    if ( qApp )
	return qApp->translate( "MiClaseVentana", s, c, QApplication::UnicodeUTF8 );
    else
	return QString::fromUtf8( s );
}
#endif // QT_NO_TRANSLATION_UTF8

#endif // QT_NO_TRANSLATION

QMetaObject* MiClaseVentana::staticMetaObject()
{
    if ( metaObj )
	return metaObj;
    QMetaObject* parentObject = QMainWindow::staticMetaObject();
    static const QUMethod slot_0 = {"MuestraSeleccion", 0, 0 };
    static const QMetaData slot_tbl[] = {
	{ "MuestraSeleccion()", &slot_0, QMetaData::Private }
    };
    metaObj = QMetaObject::new_metaobject(
	"MiClaseVentana", parentObject,
	slot_tbl, 1,
	0, 0,
#ifndef QT_NO_PROPERTIES
	0, 0,
	0, 0,
#endif // QT_NO_PROPERTIES
	0, 0 );
    cleanUp_MiClaseVentana.setMetaObject( metaObj );
    return metaObj;
}

void* MiClaseVentana::qt_cast( const char* clname )
{
    if ( !qstrcmp( clname, "MiClaseVentana" ) ) return (MiClaseVentana*)this;
    return QMainWindow::qt_cast( clname );
}

bool MiClaseVentana::qt_invoke( int _id, QUObject* _o )
{
    switch ( _id - staticMetaObject()->slotOffset() ) {
    case 0: MuestraSeleccion(); break;
    default:
	return QMainWindow::qt_invoke( _id, _o );
    }
    return TRUE;
}

bool MiClaseVentana::qt_emit( int _id, QUObject* _o )
{
    return QMainWindow::qt_emit(_id,_o);
}
#ifndef QT_NO_PROPERTIES

bool MiClaseVentana::qt_property( int _id, int _f, QVariant* _v)
{
    return QMainWindow::qt_property( _id, _f, _v);
}
#endif // QT_NO_PROPERTIES
