// Archivos de cabecera en los que se
// definen las clases que vamos a usar
#include <qapplication.h>
#include <qmainwindow.h>
#include <qcheckbox.h>
#include <qlistbox.h>
#include <qlineedit.h>
#include <qradiobutton.h>

// Nuestra clase de ventana deriva
// de QMainWindow
class MiClaseVentana : public QMainWindow
{
  Q_OBJECT
public:
  // El constructor
  MiClaseVentana();

private slots:
  // Un receptor que mostrar� la
  // selecci�n actual
  void MuestraSeleccion();

private:
  // Y los miembros necesarios
  QCheckBox* UnidadDVD;
  QListBox* Procesador;
  QLineEdit* Comentarios;
  QRadioButton* Memoria[3];
};

