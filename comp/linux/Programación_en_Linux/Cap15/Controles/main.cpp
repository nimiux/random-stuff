// Incluimos los archivos de cabecera
// que necesitamos
#include <qapplication.h>
#include <qpushbutton.h>
#include <qbuttongroup.h>
#include <qlabel.h>
#include <qmessagebox.h>

// Y la definici�n de nuestra clase de ventana
#include "main.h"

// Punto de entrada al programa
int main(int argc,char** argv)
{
  // Creamos el objeto QApplication
  new QApplication(argc,argv);

  // Establecemos como elemento principal
  // un objeto que creamos a partir de nuestra
  // propia clase de ventana
  qApp->setMainWidget(new MiClaseVentana);

  // y ponemos el programa en marcha
  return qApp->exec();
}

//------------------------ 
//Implementaci�n de los m�todos correspondientes
//a la clase MiClaseVentana
//-----------------------

// Constructor de la clase
MiClaseVentana::MiClaseVentana()
{
  // Establecemos el t�tulo y 
  setCaption("Configuraci�n del ordenador");
  // las dimensiones de la ventana
  setGeometry(0,0,320,200);

  // Creamos un QCheckBox para permitir seleccionar
  // si se quiere o no una unidad de DVD
  UnidadDVD = new QCheckBox("Con unidad de DVD", this);
  UnidadDVD->setGeometry(10,10,160,16);

  // Creamos una lista con los tipos de procesador
  // disponibles para elegir
  Procesador = new QListBox(this);
  // a�adimos los tipos de procesador
  Procesador->insertItem("Pentium 4 3.06 Ghz HT");
  Procesador->insertItem("AMD Athlon 2800+");
  Procesador->insertItem("Pentium M 1.6 Ghz");
  Procesador->insertItem("Opteron 64 3.5");
  Procesador->setGeometry(10,35,160,64);
  // Seleccionamos por defecto el primero
  Procesador->setCurrentItem(0);

  // Creamos un QButtonGroup donde vamos a introducir
  // tres QRadioButton que permitir�n elegir la
  // cantidad de memoria
  char *Titulos[] = {"256 Mb","512 Mb","768 Mb"};
  QButtonGroup* Grupo = new QButtonGroup("Memoria",this);
  Grupo->setGeometry(185,25,110,76);

  // Creamos los botones de radio, que tienen como 
  // padre no a la ventana principal sino al QButtonGroup
  for(int N=0;N<3;N++) {
    Memoria[N] = new QRadioButton(Titulos[N],Grupo);
    Memoria[N]->setGeometry(10,15+18*N,90,18);
  }

  // Inicialmente seleccionamos 512 Mb de memoria
  Memoria[1]->setChecked(true);
 
  // Introducimos una etiqueta indicativa
  QLabel* Etiqueta = new QLabel("Comentarios",this);
  Etiqueta->setGeometry(10,112,180,18);
  // y un �rea para introducir comentarios
  Comentarios = new QLineEdit(this);
  Comentarios->setGeometry(10,130,180,18);

  // A�adimos en la parte inferior un bot�n para
  // salir de la aplicaci�n
  QPushButton *UnBoton = new QPushButton("Salir",this);
  UnBoton->setGeometry(10,170,64,24);
  QObject::connect(UnBoton,SIGNAL(clicked()),qApp,SLOT(quit()));

  // Y otro para calcular el precio de la configuraci�n
  UnBoton = new QPushButton("Calcular precio", this);
  UnBoton->setGeometry(96,170,120,24);
  // que conectamos con el receptor de nuestra clase
  QObject::connect(UnBoton,SIGNAL(clicked()),this,SLOT(MuestraSeleccion()));

  show(); // Mostramos la ventana
}

// Este m�todo se ejecutar� autom�ticamente al pulsar
// el segundo de los botones introducidos en la ventana
void MiClaseVentana::MuestraSeleccion()
{
  // Preparamos una cadena donde va a generarse una
  // descripci�n de la configuraci�n
  QString Descripcion = "Ordenador con procesador <b>";

  // Introducimos el tipo de procesador
  Descripcion += Procesador->currentText() + "</b>, ";

  // Si est� activo el QCheckBox a�adimos la unidad de DVD
  if(UnidadDVD->isChecked())
    Descripcion += "unidad de DVD, ";

  // Insertamos la cantidad de memoria que se haya elegido
  for(int N=0;N<3;N++)
    if(Memoria[N]->isChecked())
      Descripcion += "<b>" + Memoria[N]->text() + "</b> de memoria.<p>";

  // Y finalmente los comentarios
  Descripcion += "Comentarios: " + Comentarios->text();

  // Creamos un cuadro de di�logo para mostrar un mensaje
  QMessageBox* Mensaje = new QMessageBox(this);
  // con este t�tulo
  Mensaje->setCaption("Ordenador elegido");
  // y toda la descripci�n
  Mensaje->setText(Descripcion);

  // Mostramos el cuadro de di�logo
  Mensaje->show();
}
