// Archivos de cabecera Qt
#include <qt.h>

int main(int argc,char **argv)
{
   // Creamos el objeto QApplication
  QApplication Aplicacion(argc,argv);
  // y una ventana
  QMainWindow Ventana;

  // Establecemos la ventana como elemento
  // principal del programa
  qApp->setMainWidget(&Ventana);

  // Fijamos el titulo de la ventana
  Ventana.setCaption("Hola con Qt");
  // y la mostramos
  Ventana.show();

  // Iniciamos el proceso de eventos
  return qApp->exec();
}
