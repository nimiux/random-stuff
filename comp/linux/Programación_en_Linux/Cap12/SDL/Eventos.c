#include <SDL.h>
#include <stdio.h>

// Prototipo de la funci�n para dibujar puntos
void PuntoGordo(SDL_Surface* Pantalla,Uint16 X, Uint16 Y, Uint32 Color);

int main() 
{
  // Superficie que representa a la pantalla
  SDL_Surface* Pantalla;
  // Indicador de fin
  int Fin=0;
  // Indicador de dibujo
  int Dibujando=0;
  // Para recoger eventos
  SDL_Event Evento;
  // Color de dibujo
  Uint32 Color;

  // Intentamos inicializar el subsistema de v�deo
  if(SDL_Init(SDL_INIT_VIDEO)==-1) {
    // comunicando un posible fallo
    puts("Fallo en la inicializaci�n");
    exit(-1);
  }

  // Activamos el modo 640x480 con color real
  Pantalla=SDL_SetVideoMode(640,480,32,SDL_HWSURFACE);
  // comprobamos que no ha habido problemas
  if(!Pantalla) {
    puts("Fallo al establecer el modo");
    exit(-1);
  }

  // Damos color blanco a todo el fondo
  SDL_FillRect(Pantalla,NULL,SDL_MapRGB(Pantalla->format,255,255,255));
  SDL_UpdateRect(Pantalla,0,0,0,0);

  // Establecemos el color inicial
  Color=SDL_MapRGB(Pantalla->format,255,0,0);
  
  // Hasta que no se indique que hay que terminar
  while(!Fin) {
    // Esperamos un evento
    SDL_WaitEvent(&Evento);
    
    // Dependiendo del evento
    switch(Evento.type) {
    case SDL_MOUSEBUTTONDOWN: // Se ha pulsado un bot�n
      Dibujando++; // incrementamos Dibujando
      break;
    case SDL_MOUSEBUTTONUP: // Se suelta un bot�n
      --Dibujando; // reducimos Dibujando
      break;
    case SDL_MOUSEMOTION: // Si se mueve el rat�n
      if(Dibujando) // y estamos dibujando
	// pintar el punto actual
	PuntoGordo(Pantalla,Evento.motion.x,Evento.motion.y,Color);
      break;
    case SDL_KEYDOWN: // Se ha pulsado una tecla
      switch(Evento.key.keysym.sym) {
      case SDLK_ESCAPE: // Escape
	Fin=1; // Salimos del bucle
	break;
      case SDLK_r: // Establecemos color rojo
	Color=SDL_MapRGB(Pantalla->format,255,0,0);
	break;
      case SDLK_v: // Establecemos color verde
	Color=SDL_MapRGB(Pantalla->format,0,255,0);
	break;
      case SDLK_a: // Establecemos color azul
	Color=SDL_MapRGB(Pantalla->format,0,0,255);
	break;
      }
      break;
    case SDL_QUIT: // Si se cierra la ventana
      Fin=1; // Salir
      break;
    }
  }
  
  // Cerramos los subsistemas
  SDL_Quit();
  
  return 0;
}

// Funci�n para dibujar un punto
void PuntoGordo(SDL_Surface* Pantalla,Uint16 X, Uint16 Y, Uint32 Color)
{
  // Para dibujar el punto
  SDL_Rect Area={0,0,3,3};
  
  // Establecemos las coordenadas
  Area.x=X;
  Area.y=Y;

  // Dibujamos el punto gordo
  SDL_FillRect(Pantalla,&Area,Color);
  
  // Actualizamos s�lo el punto modificado
  SDL_UpdateRect(Pantalla,X,Y,3,3);
}
