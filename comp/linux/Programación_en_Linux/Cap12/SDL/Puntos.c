#include <SDL.h>
#include <stdio.h>

int main() 
{
  // Superficie que representa a la pantalla
  SDL_Surface* Pantalla;
  // Secuencia de bytes de color
  Uint32 Linea[640];
  // contadores
  int N, M;
  // Posici�n actual en la memoria
  char* Posicion;

  // Intentamos inicializar el subsistema de v�deo
  if(SDL_Init(SDL_INIT_VIDEO)==-1) {
    // comunicando un posible fallo
    puts("Fallo en la inicializaci�n");
    exit(-1);
  }

  // Activamos el modo 640x480 con color real
  Pantalla=SDL_SetVideoMode(640,480,32,SDL_HWSURFACE);
  // comprobamos que no ha habido problemas
  if(!Pantalla) {
    puts("Fallo al establecer el modo");
    exit(-1);
  }

  // Bloqueamos si es necesario
  if(SDL_MUSTLOCK(Pantalla)) 
    SDL_LockSurface(Pantalla);

  /// Establecemos la posici�n inicial obteni�ndola
  // del miembro pixels de la superficie
  Posicion=Pantalla->pixels;

  // Vamos a dibujar tres bandas
  for(M=0;M<3;M++) {

    // Preparamos la secuencia de bytes a introducir
    // en cada una, compuesta de 640 puntos
    for(N=0;N<640;N+=3) {
      switch(M) {
	// Seg�n la banda fijamos el componente de azul
      case 0:
	Linea[N]=SDL_MapRGB(Pantalla->format,0,0,(N+128)/3%256);
	break;
      case 1: // vede
	Linea[N]=SDL_MapRGB(Pantalla->format,0,(N+128)/3%256,0);
	break;
      default:  // o rojo
	Linea[N]=SDL_MapRGB(Pantalla->format,(N+128)/3%256,0,0);
      }

      // Copiamos el punto en los dos siguientes, de tal forma
      // que cada color aparezca con un ancho de 3 puntos
      Linea[N+2]=Linea[N+1]=Linea[N];
    }

    // Vamos a introducir 160 lineas por banda de color
    for(N=M*160;N<M*160+160;N++) {
      // copiamos
      memcpy(Posicion,Linea,640*Pantalla->format->BytesPerPixel);
      // actualizamos la posicion para pasar a la l�nea siguiente
      Posicion+=Pantalla->pitch;
    }
  }

  // Desbloqueamos si es necesario
  if(SDL_MUSTLOCK(Pantalla))
    SDL_UnlockSurface(Pantalla);

  // Actualizamos la pantalla
  SDL_UpdateRect(Pantalla,0,0,0,0);

  sleep(10); // esperamos para que se vea

  // Cerramos los subsistemas
  SDL_Quit();

  return 0;
}
