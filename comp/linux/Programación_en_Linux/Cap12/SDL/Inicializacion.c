#include <SDL.h>
#include <stdio.h>

int main() 
{
  // Intentamos inicializar el subsistema de vídeo
  if(SDL_Init(SDL_INIT_VIDEO)==-1) {
    // comunicando un posible fallo
    puts("Fallo en la inicialización");
    exit(-1);
  }

  // Cerramos los subsistemas
  SDL_Quit();

  // indicando que todo ha ido bien
  puts("Inicailización sin problemas");
 
  return 0;
}
