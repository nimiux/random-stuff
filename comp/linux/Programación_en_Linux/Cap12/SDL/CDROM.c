#include <SDL.h>
#include <stdio.h>

int main()
{
  SDL_CD *Unidad;
  int N;

  // Intentamos inicializar el subsistema de CDROM
  if(SDL_Init(SDL_INIT_CDROM)==-1) {
    // comunicando un posible fallo
    puts("Fallo en la inicializaci�n");
    exit(-1);
  }

  // Indicamos el n�mero de unidades disponibles
  printf("Hay disponibles %d unidades de CD-ROM\n",
	 SDL_CDNumDrives());

  // y las recorremos
  for(N=0;N<SDL_CDNumDrives();N++) {
    // mostrando el nombre de cada dispositivo
    printf("La unidad %d es %s\n",N,SDL_CDName(N));

    // Lo abrimos
    Unidad=SDL_CDOpen(N);
    if(Unidad) { // si no ha habido problemas
      // Llamamos a SDL_CDStatus
      SDL_CDStatus(Unidad);
      // mostramos el n�mero de pistas
      printf("Tiene %d pistas\n",Unidad->numtracks);
      SDL_CDEject(Unidad); // lo expulsamos
      SDL_CDClose(Unidad); // y cerramos
    }
    puts("");
  }

  // Cerramos
  SDL_Quit();

  return 0;
}
