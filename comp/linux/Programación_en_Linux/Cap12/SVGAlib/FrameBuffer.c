#include <vga.h>
#include <vgagl.h>
#include <stdio.h>

int main()
{
  int X,Y,Color=0;

  // Para mantener el contexto gr�fico de la
  // pantalla f�sica y una pantalla virtual
  GraphicsContext *Fisica, *Virtual;

  // Modo que vamos a usar
  int Modo=G320x240x256;

  // Inicializamos SVGAlib
  if(vga_init()) { // controlando un posible error
    puts("Fallo en la llamada a vga_init()");
    exit(-1);
  }

  // Establecemos el modo indicado
  if(vga_setmode(Modo)) {
    puts("Fallo al establecer el modo");
    exit(-1);
  }

  // Establecemos el contexto de la pantalla real
  gl_setcontextvga(Modo);

  // y lo guardamos
  Fisica=gl_allocatecontext();
  gl_getcontext(Fisica);

  // Establecemos el contexto de la pantalla virtual
  gl_setcontextvgavirtual(Modo);

  // Obtenemos el contexto virtual
  Virtual=gl_allocatecontext();
  gl_getcontext(Virtual);

  // y dibujamos una serie de lineas en
  // la pantalla virtual
  for(X=0;X<320;X+=10)
    for(Y=0;Y<240;Y+=10)
      gl_line(X,Y,320-X,240-Y,++Color%256);

  // Copiamos a la pantalla real
  gl_copyscreen(Fisica);

  // Mientras se ve ese gr�fico vamos
  // dibujando otro en la pantalla virtual
  Color=20;
  Y=10;

  // Borramos poniendo un color de fondo
  gl_clearscreen(6);
  // y dibujamos una serie de rect�ngulos rellenos
  for(X=10;X<220;X+=10,Y+=6) 
    gl_fillbox(X,Y,100,50,++Color%256);

  vga_getch(); // Esperamos una tecla

  // Mostramos el segundo gr�fico
  gl_copyscreen(Fisica);

  // Y esperamos otra tecla
  vga_getch();

  // y volvemos al modo de texto
  vga_setmode(TEXT);

  return 0;
}
