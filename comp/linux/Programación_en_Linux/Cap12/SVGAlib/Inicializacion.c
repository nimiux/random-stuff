#include <vga.h>
#include <stdio.h>

int main()
{
  int IDChipSet;
  int X,Y,Color=0;

  // Inicializamos SVGAlib
  if(vga_init()) { // controlando un posible error
    puts("Fallo en la llamada a vga_init()");
    exit(-1);
  }

  // Obtenemos el identificador del hardware
  IDChipSet=vga_getcurrentchipset();

  // Establecemos el modo de 640x480 puntos
  if(vga_setmode(G640x480x16)) {
    puts("Fallo al establecer el modo");
    exit(-1);
  }

  // y dibujamos una serie de lineas
  for(X=0;X<640;X+=20)
    for(Y=0;Y<480;Y+=20) {
      vga_setcolor(++Color%16);
      vga_drawline(X,Y,640-X,480-Y);
    }

  vga_getch(); // Esperamos una tecla

  // y volvemos al modo de texto
  vga_setmode(TEXT);

  // mostramos el identificador del hardware
  printf("ID del chipset: %d\n", IDChipSet);

  return 0;
}
