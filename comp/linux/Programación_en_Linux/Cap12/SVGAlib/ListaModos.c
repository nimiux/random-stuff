#include <vga.h>
#include <stdio.h>

int main()
{
  // Para recorrer los modos existentes
  int Modo,UltimoModo;
  // Para obtener informaci�n de cada modo
  vga_modeinfo* InfModo;

  if(vga_init()) { // Inicializamos la biblioteca
    puts("Fallo en la llamada a vga_init()");
    exit(-1); // si hay un fallo salir
  }

  // Obtenemos el �ndice del �ltimo modo disponible
  UltimoModo=vga_lastmodenumber();

  // y lo mostramos en la consola
  printf("\nEl �ltimo modo disponible es %d\n\n",
	 UltimoModo);

  // Cabecera para la lista
  puts("Numero\tNombre\t\tDisp\tBPP\tAncho\tAlto\tColores\t\tBSL\n");

  // Bucle para recorrer todos los modos existentes
  for(Modo=1;Modo<=UltimoModo;Modo++) {
    // Obtenemos informaci�n de cada modo
    InfModo=vga_getmodeinfo(Modo);

    // Mostrando su n�mero
    printf("%d\t%s\t%s\t%d\t%d\t%d\t%8d\t%d\n",Modo,
	   vga_getmodename(Modo), // nombre
	   vga_hasmode(Modo) ? "Si" : "No", // disponibilidad
	   InfModo->bytesperpixel, // bytes que ocupa cada punto
	   // ancho y alto en punto
	   InfModo->width,InfModo->height,
	   // n�mero de colores y bytes por l�nea de pantalla
	   InfModo->colors,InfModo->linewidth);
  }

  return 0;
}
