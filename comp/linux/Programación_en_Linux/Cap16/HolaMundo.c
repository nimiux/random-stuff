// Archivos de cabecera necesarios
#include <gtk/gtk.h>

// Prototipo de la funci�n que gestionar�
// la se�al "destroy" de la ventana
void EventoDestroy(GtkWidget* Control,gpointer D);

// Punto de entrada a la aplicaci�n
int main(int argc,char** argv)
{
  // Tan solo vamos a crear una ventana
  GtkWidget* MiVentana;

  // Inicializamos la aplicaci�n
  gtk_init(&argc,&argv);

  // Creamos loa ventana
  MiVentana=gtk_window_new(GTK_WINDOW_TOPLEVEL);
  // y establecemos su t�tulo
  gtk_window_set_title(GTK_WINDOW(MiVentana),"GTK+");

  // Insertamos en ella una etiqueta con un texto
  gtk_container_add(GTK_CONTAINER(MiVentana), 
		    gtk_label_new("Hola mundo con GTK+"));

  // Establecemos el tama�o por defecto de la ventana
  gtk_window_set_default_size(GTK_WINDOW(MiVentana),320,200);
  // y la mostramos con todo su contenido
  gtk_widget_show_all(MiVentana);

  // Conectamos la se�al de cierre de la ventana
  // con la funci�n que la va a gestionar
  g_signal_connect(G_OBJECT(MiVentana),"destroy",
		   G_CALLBACK(EventoDestroy),NULL);
  
  gtk_main(); // y ponemos en marcha el bucle de proceso

  return 0;
}

// Esta funci�n ser� invocada autom�ticamente cuando
// se cierre la ventana del programa
void EventoDestroy(GtkWidget* Control,gpointer D)
{
  // momento en el que interrumpiremos el bucle
  // de proceso de eventos para poner fin al programa
  gtk_main_quit();
}
