// Archivos de cabecera necesarios
#include <gtk/gtk.h>

// Estructura con los elementos que compondr�n
// nuestra interfaz de usuario
typedef struct {
  // Una ventana principal
  GtkWidget *MiVentana;
  // con una etiqueta y una caja de texto
  GtkWidget *lblComentarios, *txtComentarios;
  // Dos botones est�ndar, un checkbox y
  // un toggle 
  GtkWidget *btnSalir, *btnConfiguracion,
    *btnDVD, *btnGarantia;
  // y tres botones de radio
  GtkWidget *frmMemoria, *btn256Mb, *btn512Mb, *btn768Mb;
  
} Interfaz;

// Y la pulsaci�n del bot�n Configuraci�n
void MuestraConfiguracion(GtkWidget* Control,gpointer D);

// Punto de entrada a la aplicaci�n
int main(int argc,char** argv)
{
  // Elementos de la interfaz de usuario
  Interfaz MiInterfaz;

  // Y unas �reas para distribuirlos
  GtkWidget *General,*Superior,*Inferior,
    *SupIzquierda,*SupDerecha;

  // Inicializamos la aplicaci�n
  gtk_init(&argc,&argv);

  // Creamos loa ventana
  MiInterfaz.MiVentana=gtk_window_new(GTK_WINDOW_TOPLEVEL);
  // y establecemos su t�tulo
  gtk_window_set_title(GTK_WINDOW(MiInterfaz.MiVentana),"Botones");
  // Conectamos la se�al de cierre de la ventana
  // directamente con la funci�n gtk_main_quit
  g_signal_connect(G_OBJECT(MiInterfaz.MiVentana),"destroy",
		   G_CALLBACK(gtk_main_quit),NULL);

  // Creamos los controles GtkBox que nos servir�n
  // para organizar el contenido
  General=gtk_vbox_new(FALSE,10);
  Superior=gtk_hbox_new(FALSE,10);
  Inferior=gtk_hbox_new(TRUE,10);
  SupIzquierda=gtk_vbox_new(FALSE,5);
  SupDerecha=gtk_vbox_new(TRUE,10);

  // Insertamos el �rea general en la ventana
  gtk_container_add(GTK_CONTAINER(MiInterfaz.MiVentana),General);

  // incluyendo en ella las dos �reas verticales
  gtk_box_pack_start(GTK_BOX(General),Superior,TRUE,FALSE,0);
  gtk_box_pack_start(GTK_BOX(General),Inferior,TRUE,FALSE,0);

  // Insertamos en la parte superior el �rea que
  // quedar� a la izquierda
  gtk_box_pack_start(GTK_BOX(Superior),SupIzquierda,TRUE,FALSE,0);

  // Vamos creando los controles incluy�ndolos en
  // el �rea adecuada
  
  // Creamos el bot�n que permitir� elegir si se
  // quiere o no unidad de DVD
  MiInterfaz.btnDVD=gtk_check_button_new_with_label("DVD incluido");
  // y lo insertamos en el �rea superior izquierda
  gtk_box_pack_start(GTK_BOX(SupIzquierda),MiInterfaz.btnDVD,FALSE,FALSE,10);

  // Creamos el bot�n que permitir� elegir si se
  // quiere o no una extensi�n de garant�a
  MiInterfaz.btnGarantia=
	  gtk_toggle_button_new_with_label("Ampliacion de garantia");
  // y lo insertamos debajo
  gtk_box_pack_start(GTK_BOX(SupIzquierda),MiInterfaz.btnGarantia,FALSE,FALSE,10);

  // Creamos una etiqueta de texto
  MiInterfaz.lblComentarios=gtk_label_new("Comentarios");
  // poni�ndola debajo
  gtk_box_pack_start(GTK_BOX(SupIzquierda),MiInterfaz.lblComentarios,FALSE,FALSE,0);

  // Y una caja de texto
  MiInterfaz.txtComentarios=gtk_entry_new();
  // ajustando su tama�o m�nimo
  gtk_widget_set_size_request(MiInterfaz.txtComentarios,96,18);
  // y poni�ndola debajo
  gtk_box_pack_start(GTK_BOX(SupIzquierda),MiInterfaz.txtComentarios,FALSE,FALSE,0);

  // Creamos un recuadro para los botones de radio
  MiInterfaz.frmMemoria=gtk_frame_new("Memoria");
  // ajustamos el tama�o
  gtk_widget_set_size_request(MiInterfaz.frmMemoria,84,72);
  // y lo insertamos en la parte superior de la ventana
  gtk_box_pack_start(GTK_BOX(Superior),MiInterfaz.frmMemoria,TRUE,TRUE,0);
  // incluyendo en �l un contenedor secundario para los botones de radio
  gtk_container_add(GTK_CONTAINER(MiInterfaz.frmMemoria),SupDerecha);

  // Vamos creando los botones de radio
  MiInterfaz.btn256Mb=gtk_radio_button_new_with_label(NULL,"256 Mb");
  // e insert�ndolos en el �rea superior derecha
  gtk_box_pack_start(GTK_BOX(SupDerecha),MiInterfaz.btn256Mb,FALSE,FALSE,0);

  // uno debajo del otro
  MiInterfaz.btn512Mb=gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(MiInterfaz.btn256Mb),"512 Mb");
  gtk_box_pack_start(GTK_BOX(SupDerecha),MiInterfaz.btn512Mb,FALSE,FALSE,0);

  MiInterfaz.btn768Mb=gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON(MiInterfaz.btn256Mb),"768 Mb");
  gtk_box_pack_start(GTK_BOX(SupDerecha),MiInterfaz.btn768Mb,FALSE,FALSE,0);

  // Creamos el GtkButton Configuracion
  MiInterfaz.btnConfiguracion=gtk_button_new_with_label("Configuracion");
  // ajustamos el tama�o
  gtk_widget_set_size_request(MiInterfaz.btnConfiguracion,96,24);
  // y lo insertamos en la parte inferior
  gtk_box_pack_start(GTK_BOX(Inferior),MiInterfaz.btnConfiguracion,FALSE,FALSE,0);

  // y lo conectamos con la funci�n para mostrar la configuraci�n
  g_signal_connect(G_OBJECT(MiInterfaz.btnConfiguracion),"clicked",
		   G_CALLBACK(MuestraConfiguracion),&MiInterfaz);

  // Creamos el GtkButton Salir
  MiInterfaz.btnSalir=gtk_button_new_with_label("Salir");
  // ajustamos el tama�o
  gtk_widget_set_size_request(MiInterfaz.btnSalir,64,24);
  // y lo insertamos a la derecha del anterior
  gtk_box_pack_start(GTK_BOX(Inferior),MiInterfaz.btnSalir,FALSE,FALSE,0);

  // y lo conectamos con la funci�n para salir
  g_signal_connect(G_OBJECT(MiInterfaz.btnSalir),"clicked",
		   G_CALLBACK(gtk_main_quit),NULL);

  // Establecemos el tama�o por defecto de la ventana
  gtk_window_set_default_size(GTK_WINDOW(MiInterfaz.MiVentana),320,200);
  // y la mostramos con todo su contenido
  gtk_widget_show_all(MiInterfaz.MiVentana);
  
  gtk_main(); // y ponemos en marcha el bucle de proceso

  return 0;
}

// Esta macro nos ahorrar� tener que escribir
// varias veces la llamada a la funci�n y har�
// el c�digo algo m�s claro
#define GetActive(X) gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(X))

// Funci�n que se ejecutar� al pulsar el bot�n Configuraci�n
void MuestraConfiguracion(GtkWidget* Control,gpointer D)
{
  // Convertimos el puntero recibido para poder
  // acceder a los elementos de la interfaz
  Interfaz *MiInterfaz=(Interfaz*) D;
  // vamos a crear un cuadro de di�logo
  GtkDialog *dlgMensaje;

  // mostrando en �l toda la configuraci�n
  // recuper�ndola de los distintos controles
  dlgMensaje=gtk_message_dialog_new(
     GTK_WINDOW(MiInterfaz->MiVentana), 
     GTK_DIALOG_DESTROY_WITH_PARENT,
     GTK_MESSAGE_INFO,GTK_BUTTONS_OK,
     "Unidad de DVD: %s\nExtension de garantia: %s\n"
     "Memoria: %s\nComentarios: %s", 
     (GetActive(MiInterfaz->btnDVD) ? "Si" : "No"),
     (GetActive(MiInterfaz->btnGarantia) ? "Si" : "No"),
     (GetActive(MiInterfaz->btn256Mb) ? "256 Mb" :
      (GetActive(MiInterfaz->btn512Mb) ? "512 Mb" : "768 Mb")),
     gtk_entry_get_text(GTK_ENTRY(MiInterfaz->txtComentarios)));

  // Mostramos el cuadro de di�logo
  gtk_dialog_run(dlgMensaje);
  // y lo eliminamos cuando el usuario lo haya cerrado
  gtk_widget_destroy(GTK_WIDGET(dlgMensaje));
}
