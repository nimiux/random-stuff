#include <curses.h>
#include <panel.h>

int main()
{
  // Matriz con los punteros a los
  // paneles que van a crearse
  PANEL *Ventanas[5];

  // Puntero a una ventana
  WINDOW *v;

  // Coordenadas para ir moviendo
  // los paneles
  int X=10, Y=5, iX=1, iY=1;
  int Salida;
  int n;

  // Inicializamos curses
  initscr();
  // y el uso de color
  Salida=start_color();

  // Creamos un primer panel, peque�o y
  // en la esquina superior izquierda
  Ventanas[0]=new_panel(newwin(5,10,0,0));
  // dibujamos el borde
  box(panel_window(Ventanas[0]),0,0);

  // El segundo panel ocupar� toda la pantalla
  // y, por tanto, ocultar� al anterior
  Ventanas[1]=new_panel(newwin(LINES,COLS,0,0));
  // dibujamos el marco
  box(panel_window(Ventanas[1]),0,0);

  // Si start_color() devolvi� ERR
  if(Salida==ERR)
    // es que no pueden utilizarse colores
    mvwaddstr(panel_window(Ventanas[1]),1,1,
	      "El terminal no soporta color");
  else // en caso contario s�
    mvwaddstr(panel_window(Ventanas[1]),1,1,
	      "El terminal contempla el uso de color");

  // Creamos la tercera ventana, de tan
  // s�lo cuatro l�neas y situada en la
  // parte inferior de la pantalla
  Ventanas[2]=new_panel(newwin(4,COLS-4,LINES-5,2));
  // dibujamos el recuadro alrededor
  box(panel_window(Ventanas[2]),'*','*');

  // Creamos la cuarta ventana, m�s estrecha
  // y alta que la anterior y situada en el
  // extremo derecho de la pantalla
  Ventanas[3]=new_panel(newwin(LINES-4,10,2,60));
  // dibujamos el recuadro
  box(panel_window(Ventanas[3]),0,0);

  // Creamos la �ltima ventana
  Ventanas[4]=new_panel(newwin(6,35,Y,X));

  // Obtenemos la ventana asociada al panel
  v=panel_window(Ventanas[4]);
  // dibujamos el marco
  box(v,0,0);

  // Preparamos una combinaci�n de color con
  // texto blanco sobre fondo azul
  init_pair(1,COLOR_WHITE,COLOR_BLUE);
  // y la activamos en esta ventana
  wcolor_set(v,1,NULL);

  // Introducimos un texto en la ventana
  mvwaddstr(v,1,1,"El poeta no entiende de guerras, ");
  mvwaddstr(v,2,1,"de dinero ni raz�n,              ");
  mvwaddstr(v,3,1,"el poeta s�lo entiende           ");
  mvwaddstr(v,4,1,"lo que dicta su coraz�n          ");

  // Ponemos en primer plano la cuarta ventana
  top_panel(Ventanas[3]);

  // Actualizamos la imagen en pantalla
  update_panels();
  doupdate();

  while(X) { // Mientras X no sea cero

    Y+=iY; // Vamos modificando la posici�n
    X+=iX; // a la que moveremos la ventana

    // La movemos
    move_panel(Ventanas[4],Y,X);

    // y actualizamos la imagen
    update_panels();
    doupdate();

    // Actualizamos adecuadamente el incremento
    // de coordenadas
    if(X==COLS-35) iX=-1;

    if(Y==LINES-6) iY=-1;

    if(!Y) iY=1;
  }

  getch(); // Esperamos la pulsaci�n de una tecla

  // Traemos al primer plano la primera ventana
  // que, hasta ahora, ha permanecido oculta
  top_panel(Ventanas[0]);

  // Actualizamos la imagen en pantalla
  update_panels();
  doupdate();
 
  getch(); // Esperamos la pulsaci�n de una tecla

  // Ocultamos la ventana que ocupa toda
  // la pantalla
  hide_panel(Ventanas[1]);
  update_panels();
  doupdate();

  getch(); // Esperamos la pulsaci�n de una tecla

  X=60; // Posici�n horizontal

  while(X) {
    // Vamos a ir desplazando la cuarta ventana
    // desde el margen derecho al izquierdo
    move_panel(Ventanas[3],2,X--);

    update_panels();
    doupdate();
  }

  getch(); // Esperamos la pulsaci�n de una tecla

  endwin(); // Cerramos

  return 0;
}
