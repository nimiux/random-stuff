#include <ncurses.h>

int main()
{
  // Variables con la direcci�n de la
  // estructura de datos de las ventanas
  WINDOW *Ventana1, *Ventana2;
  int n;

  initscr(); // Inicializamos curses

  noecho(); // Desactivamos el eco

  // Creamos las nuevas ventanas
  Ventana1=newwin(12,20,10,10);
  Ventana2=newwin(12,20,2,40);

  // Dejamos la primera linea en blanco
  wprintw(Ventana1,"\n\r");
  wprintw(Ventana2,"\n\r");

  // Imprimimos en ellas una serie
  for(n=0; n<10; n++) { // de n�meros
    wprintw(Ventana1,"  N�mero %d\n\r",n);
    wprintw(Ventana2,"  N�mero %d\n\r",n*2);
  }

  // Dibujamos un marco alrededor
  box(Ventana1,0,0);
  box(Ventana2,0,0);
 
  // Actualizamos las ventanas
  wrefresh(Ventana1);
  wrefresh(Ventana2);

  // y esperamos una tecla
  wgetch(Ventana1);

  endwin(); // Terminamos

  return 0;
}
