#include <ncurses.h>
#include <menu.h>
#include <panel.h>
#include <stdio.h>

// Prototipos de las funciones
void Inicializacion();
int RecogeTecla(WINDOW*);

int main()
{
  // Matriz para cinco punteros ITEM
  ITEM *opcs[5];

  // Subopciones
  ITEM *subopcs[4];

  // Puntero para el men�
  MENU *menu;
  int n; // contador
  int tecla,salida;

  // Men� actual
  int actual=0;

  // Panel y ventana asociados al men�
  PANEL *PanelMenu;
  WINDOW *Ventana, *SubVentana;

  int x=10;

  // Matriz con los t�tulos de las opciones
  char *Opciones[4] = { 
    "Editar","Imprimir","Exportar","Importar"};

  // Opciones del submen�
  char *SubMenu1[] = {
    "Todas","Seleccionar","Filtro"};

  // Inicializaci�n de ncurses
  Inicializacion();

  // Creamos la ventana, subventana y el panel para el men�
  Ventana=newwin(6,15,10,10);
  SubVentana=derwin(Ventana,4,13,1,1);
  PanelMenu=new_panel(Ventana);

  // Activamos la recepci�n de teclas especiales
  keypad(SubVentana,1);

  // Establecemos el color de fondo
  wbkgd(Ventana,COLOR_PAIR(1));
  wbkgd(SubVentana,COLOR_PAIR(1));

  // Dibujamos el borde alrededor de la ventana
  box(Ventana,0,0);

  // Creamos las cuatro opciones facilitando
  // sus t�tulos y una descripci�n nula
  for(n=0;n<4;n++)
    opcs[n]=new_item(Opciones[n],"");

  // A�adimos la quinta opci�n con el valor NULL
  opcs[4]=(ITEM *)NULL;

  // Creamos las opciones del submen�
  for(n=0;n<3;n++)
    subopcs[n]=new_item(SubMenu1[n],"");

  // A�adimos el se�alizador de final
  subopcs[3]=(ITEM *)NULL;

  // Creamos el men� de opciones
  menu=new_menu(opcs);

  // Establecemos el formato 
  set_menu_mark(menu, " ");

  // Asociamos el men� con la ventana
  set_menu_win(menu,Ventana);
  set_menu_sub(menu,SubVentana);

  // y establecemos los atributos
  set_menu_fore(menu,COLOR_PAIR(2));
  set_menu_back(menu,COLOR_PAIR(1));
  set_menu_grey(menu,COLOR_PAIR(3));

  // y lo hacemos visible
  post_menu(menu);

  update_panels(); // Actualizamos la visualizaci�n
  doupdate();

  do { // Mientras no se pulse ESC estando en el men� principal

    // Enviamos a menu_driver() la constante
    // adecuada dependiendo de la tecla que se pulse
    salida=menu_driver(menu,tecla=RecogeTecla(SubVentana));

    // Si es un comando desconocido
    if(salida==E_UNKNOWN_COMMAND)
      switch(tecla) {

      case KEY_RIGHT: // Cursor a la derecha
	if(x<COLS-15) {
	  x++; // nos desplazamos a la derecha
	  move_panel(PanelMenu,10,x);
	  update_panels();
	  doupdate();
	}
	break;

      case KEY_LEFT: // Cursor a la izquierda
	if(x) {
	  x--; // nos desplazamos a la izquierda
	  move_panel(PanelMenu,10,x);
	  update_panels();
	  doupdate();
	}
	break;

      case 10: // Si se ha pulsado Intro
	if(!actual) { // y estamos en el men� principal
	  actual=1; // ahora estamos en el submen�
	  unpost_menu(menu); // ocultamos el men�
	  update_panels(); // actualizamos
	  // Activamos el submen�
	  set_menu_items(menu,subopcs);
	  post_menu(menu); // lo mostramos
	  update_panels();
	  doupdate();
	}
	break;
      case 27: // Si se ha pulsado ESC
	if(actual) { // y estamos en el submen�
	  actual=0; // volvemos al men� principal
	  unpost_menu(menu); // siguiendo los mismos
	  update_panels(); // pasos pero a la inversa
	  set_menu_items(menu,opcs);
	  post_menu(menu);
	  update_panels();
	  doupdate();

	  tecla=0; // ignoramos la tecla
	}
	break;
      default: // en cualquier otro caso
	beep();
      }
  } while(tecla!=27);

  // Ocultamos el men�
  unpost_menu(menu);
  
  update_panels();
  doupdate();

  // Liberamos el men�
  free_menu(menu);

  // y todas sus opciones
  for(n=0;n<4;n++)
    free_item(opcs[n]);

  for(n=0;n<3;n++)
    free_item(subopcs[n]);

  // Borramos el contenido de la ventana
  werase(Ventana);
  wrefresh(Ventana);
  // y la eliminamos
  delwin(Ventana);

  endwin(); // Cerramos ncurses

  return 0;
}

int RecogeTecla(WINDOW *Ventana)
{
  // Recogemos una tecla
  int tecla=wgetch(Ventana);

  switch(tecla)  // Dependiendo de la tecla
    {
      // Devolvemos un comando u otro
    case KEY_UP: return REQ_PREV_ITEM;
    case KEY_DOWN: return REQ_NEXT_ITEM;
    case KEY_HOME: return REQ_FIRST_ITEM;
    case KEY_END: return REQ_LAST_ITEM;
    case 8: return REQ_CLEAR_PATTERN;
    default: return tecla;
    }
}

void Inicializacion()
{
  initscr(); // Inicializamos ncurses

  // Y el uso de color
  start_color();

  // desactivamos el eco y teclas especiales
  noecho();
  raw();

  // Ocultamos el cursor
  curs_set(0);

  // Preparamos los colores
  init_pair(1,COLOR_WHITE,COLOR_BLUE);
  init_pair(2,COLOR_BLUE,COLOR_WHITE);
  init_pair(3,COLOR_CYAN,COLOR_BLUE);
}
