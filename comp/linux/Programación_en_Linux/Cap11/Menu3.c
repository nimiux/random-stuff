#include <ncurses.h>
#include <menu.h>
#include <stdio.h>


int main()
{
  // Matriz para cinco punteros ITEM
  ITEM *opcs[5];

  // Subopciones
  ITEM *subopcs[4];

  // Puntero para el men�
  MENU *menu;
  int n; // contador
  int tecla,salida;

  int actual=0;

  // Matriz con los t�tulos de las opciones
  char *Opciones[4] = { 
    "Editar","Imprimir","Exportar","Importar"};

  // Opciones del submen�
  char *SubMenu1[] = {
    "Todas","Seleccionar","Filtro"};

  initscr(); // Inicializamos ncurses

  // Activamos la recepci�n de teclas especiales
  keypad(stdscr,1);

  // Creamos las cuatro opciones facilitando
  // sus t�tulos y una descripci�n nula
  for(n=0;n<4;n++)
    opcs[n]=new_item(Opciones[n],"");

  // A�adimos la quinta opci�n con el valor NULL
  opcs[4]=(ITEM *)NULL;

  // Creamos las opciones del submen�
  for(n=0;n<3;n++)
    subopcs[n]=new_item(SubMenu1[n],"");

  // A�adimos el se�alizador de final
  subopcs[3]=(ITEM *)NULL;

  // Creamos el men� de opciones
  menu=new_menu(opcs);

  // Establecemos el formato 
  set_menu_format(menu,1,4);
  set_menu_mark(menu, ">");

  // y lo hacemos visible
  post_menu(menu);

  refresh(); // Actualizamos la visualizaci�n

  do { // Mientras no se pulse ESC estando en el men� principal

    // Enviamos a menu_driver() la constante
    // adecuada dependiendo de la tecla que se pulse
    salida=menu_driver(menu,tecla=RecogeTecla());

    // Si es un comando desconocido
    if(salida==E_UNKNOWN_COMMAND)
      switch(tecla) {
      case 10: // Si se ha pulsado Intro
	if(!actual) { // y estamos en el men� principal
	  actual=1; // ahora estamos en el submen�
	  unpost_menu(menu); // ocultamos el men�
	  refresh(); // actualizamos
	  // Activamos el submen�
	  set_menu_items(menu,subopcs);
	  post_menu(menu); // lo mostramos
	  refresh();
	}
	break;
      case 27: // Si se ha pulsado ESC
	if(actual) { // y estamos en el submen�
	  actual=0; // volvemos al men� principal
	  unpost_menu(menu); // siguiendo los mismos
	  refresh(); // pasos pero a la inversa
	  set_menu_items(menu,opcs);
	  post_menu(menu);
	  refresh();

	  tecla=0; // ignoramos la tecla
	}
	break;
      default: // en cualquier otro caso
	beep();
      }
  } while(tecla!=27);

  // Ocultamos el men�
  unpost_menu(menu);
  refresh();

  // Liberamos el men�
  free_menu(menu);
  // y todas sus opciones
  for(n=0;n<4;n++)
    free_item(opcs[n]);

  endwin(); // Cerramos ncurses

  return 0;
}

int RecogeTecla()
{
  // Recogemos una tecla
  int tecla=getch();

  switch(tecla)  // Dependiendo de la tecla
    {
      // Devolvemos un comando u otro
    case KEY_UP: return REQ_PREV_ITEM;
    case KEY_DOWN: return REQ_NEXT_ITEM;
    case KEY_HOME: return REQ_FIRST_ITEM;
    case KEY_END: return REQ_LAST_ITEM;
    case 8: return REQ_CLEAR_PATTERN;
    default: return tecla;
    }
}
