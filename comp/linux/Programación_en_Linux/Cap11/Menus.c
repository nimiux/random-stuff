/*
Menus.c

Programa que muestra el uso de la biblioteca 'menu'
conjuntamente con 'ncurses' y 'panel'
*/

#include <menu.h>
#include <panel.h>
#include <curses.h>
#include <stdio.h>
#include <string.h>

// Prototipo de la funci�n que se encargar�
// de imprimir los mensajes asociados a cada
// una de las opciones
void MuestraMensaje(MENU *);

// Datos para componer el men�, creados en 
// forma de estructura
struct {
  char *Opcion; // Texto de la opci�n
  char *Comentario; // Comentario aclaratorio
} Opciones[] = {
  "Agenda"," Funciones para el control de la agenda de visitas",
  "Visitas"," Gesti�n de las visitas",
  "Proveedores"," Control de la informaci�n de proveedores",
  "Clientes"," Control de la informaci�n de clientes",
  "Salir"," Abandonar el programa",
  (char *)NULL,(char *)NULL, // Final de un men�

  "Modificacion"," Registro/Modificaci�n de citas establecidas",
  "Impresion"," Listados de las citas de una determinada fecha",
  "Borrado"," Borrado de citas antiguas",
  (char *)NULL,(char *)NULL,

  "Registro"," Almacenamiento de informaci�n de visitas",
  "Informe"," Resumen de datos de las visitas seleccionadas",
  (char *)NULL,(char *)NULL,

  "Registro"," Registro/Modificaci�n de datos de proveedores",
  "Relacion"," Lista ordenada de los proveedores registrados",
  "Borrado"," Borrado selectivo de proveedores",
  "Pagos"," Registro de los pagos a proveedores",
  (char *)NULL,(char *)NULL,

  "Registro"," Registro/Modificaci�n de datos de clientes",
  "Relacion"," Lista ordenadas de los clientes registrados",
  "Borrado"," Borrado selectivo de clientes",
  "Cobros"," Registro de los pagos de clientes",
  (char *)NULL,(char *)NULL
};

// Prototipos de las funciones correspondientes a cada
// una de las opciones de los men�s
void AgendaModificacion();
void AgendaImpresion();
void AgendaBorrado();
void VisitasRegistro();
void VisitasInforme();
void ProveedoresRegistro();
void ProveedoresRelacion();
void ProveedoresBorrado();
void ProveedoresPagos();
void ClientesRegistro();
void ClientesRelacion();
void ClientesBorrado();
void ClientesCobros();

// Prototipos de otras funciones
void InicializaPantalla();
void PreparaMenus();
int RecogeTecla(PANEL *);
void LiberaTodo();
void MuestraMensaje(MENU *);
void Llama(void (*f)());
void MensajeEspera(char *s);

// Matriz conteniendo las direcciones de cada una
// de las funciones. 
// Hay un total de 4 men�s, el primero no llama
// directamente a ninguna funci�n, con 4 opciones
// como m�ximo cada uno. En aquellos men�s que tienen
// menos opciones se rellena con NULL
void *Funciones[4][4] = {
  AgendaModificacion,AgendaImpresion,AgendaBorrado,(void *)NULL,
  VisitasRegistro,VisitasInforme,(void *)NULL,(void *)NULL,
  ProveedoresRegistro,ProveedoresRelacion,ProveedoresBorrado,
  ProveedoresPagos,ClientesRegistro,ClientesRelacion,
  ClientesBorrado,ClientesCobros
};

// Hay 5 men�s
MENU *Menu[5];
// asociados a 5 paneles
PANEL *Panel[5], *Pantalla;
// Con 6 opciones como m�ximo cada uno
ITEM *Opcs[5][6];

// Punto de entrada a la aplicaci�n
int main()
{
  // Variables auxiliares
  int Pulsacion, Salida;
  int MenuActual=0;
  int I1,I2;

  // Inicializamos ncurses
  InicializaPantalla();

  // y preparamos los men�s
  PreparaMenus();

  do {
    // Mostramos el men� actual
    show_panel(Panel[MenuActual]);
    // Si hay problemas para mostrarlo
    if(post_menu(Menu[MenuActual]) != E_OK) {
      endwin(); // finalizamos
      printf("Error al mostrar un men�.\n");
      exit(-1);
    }

    // Todo va bien, actualizamos
    update_panels();
    doupdate();

    do {
      // Recogemos una pulsaci�n y procesamos
      Salida=menu_driver(Menu[MenuActual],
			 Pulsacion=RecogeTecla(Panel[MenuActual]));

      // Si es un comando desconocido
      if(Salida==E_UNKNOWN_COMMAND)
	switch(Pulsacion) // lo procesamos nosotros
	  {
	  case 10: // Se ha pulsado <Intro>
	    if(!MenuActual) { // Si el men� actual es el 0
	      // En caso de que se haya elegido la �ltima opci�n
	      if(item_index(current_item(Menu[MenuActual]))==4) {
		LiberaTodo(); // salimos
		exit(0);
	      } else // en caso contrario
		// obtenemos el �ndice del men� a abrir
		MenuActual=item_index(current_item(Menu[MenuActual]))+1;
	    } else { // Si estamos en uno de los submen�s
	      // Obtener el �ndice de la funci�n correspondiente
	      I1=item_index(current_item(Menu[0]));
	      I2=item_index(current_item(Menu[I1+1]));
	      // e invocarla
	      Llama(Funciones[I1][I2]);

	      Pulsacion=0; // ignorando la pulsaci�n
	    }

	    break;

	  case 27: // Si se pulsa <Esc>
	    if(MenuActual) { // Si estamos en un submen�
	      // lo ocultamos
	      unpost_menu(Menu[MenuActual]);
	      hide_panel(Panel[MenuActual]);
	      update_panels();
	      doupdate();
	      MenuActual=0; // y volvemos al men� principal
	    }

	    break;
	  }
    } while(Pulsacion!=10); // Mientras no se pulse <Intro>
  } while(1); // Hasta elegir la opci�n de salida

  endwin();
}

// Inicializa ncurses, los colores a utilizar y el
// panel Pantalla, imprimiendo en �ste un relleno que
// ser� el fondo sobre el que se aparecer�n los men�s
void InicializaPantalla()
{
  char *Relleno;
  int Linea;

  // Inicializaci�n de ncurses
  initscr();
  noecho();
  raw();

  // Inicializaci�n del color
  start_color();
  init_pair(1,COLOR_WHITE,COLOR_BLUE);
  init_pair(2,COLOR_BLUE,COLOR_WHITE);

  // Asociamos un panel con la pantalla completa
  Pantalla=new_panel(stdscr);

  // Establecemos un color
  wattron(stdscr,COLOR_PAIR(2));
  // y preparamos la cadena con el relleno
  Relleno=(char *)malloc(COLS+1);
  memset(Relleno,'.',COLS);

  // Recorremos todas las l�neas imprimiendo la cadena
  for(Linea=0;Linea<LINES;Linea++)
    mvaddstr(Linea,1,Relleno);

  // Actualizamos
  update_panels();
  doupdate();

  // Liberamos la memoria asignada
  free(Relleno);
}

// Crea cada uno de los ITEM, MENU y PANEL, 
// realizando las oportunas asociaciones 
// entre ellos
void PreparaMenus()
{
  int NMenu=0,NOpc=0;
  int NOpcion=0;
  int NLineas,NColumnas;

  // Mientras el �ndice de men� sea menor que 5
  while(NMenu<5) {
    // vamos creando objetos ITEM con cada opci�n
    if(!(Opcs[NMenu][NOpc]=new_item(Opciones[NOpcion].Opcion,""))) {
      endwin();
      printf("Error al crear un ITEM\n%d/%d/%d\n",NMenu,NOpc,NOpcion);
      exit(-1);
    }

    // Asociando con cada ITEM el comentario
    set_item_userptr(Opcs[NMenu][NOpc],Opciones[NOpcion].Comentario);

    NOpcion++; // Incrementamos el n�mero de opci�n
    NOpc++;

    // Si el t�tulo de la opci�n es nulo
    if(Opciones[NOpcion].Opcion==(char *)NULL) {
      // introducimos el se�alizador de fin del men�
      Opcs[NMenu][NOpc]=(ITEM *)NULL;

      // Y creamos el objeto MENU
      if(!(Menu[NMenu]=new_menu(Opcs[NMenu]))) {
	endwin();
	printf("Error al crear un MENU\n");
	exit(-1);
      }

      // Establecemos formato y color
      set_menu_mark(Menu[NMenu],"->");
      set_menu_fore(Menu[NMenu],COLOR_PAIR(2));
      set_menu_back(Menu[NMenu],COLOR_PAIR(1));

      // Cada vez que se active una opci�n 
      // llamar a la funci�n MuestraMensaje
      set_item_init(Menu[NMenu],MuestraMensaje);

      // Calcular las dimensiones del men�
      scale_menu(Menu[NMenu],&NLineas,&NColumnas);

      // Para crear una ventana que lo contenga
      if(!(Panel[NMenu]=new_panel(newwin(NLineas+2,NColumnas+2,10+NMenu*2,25+NMenu*5)))) {
	endwin();
	printf("Error al crear un PANEL\n%d/%d/%d\n",NMenu,NLineas,NColumnas);
	exit(-1);
      }

      // Establecemos la ventana y subventana
      set_menu_win(Menu[NMenu],panel_window(Panel[NMenu]));
      set_menu_sub(Menu[NMenu],derwin(panel_window(Panel[NMenu]),NLineas,NColumnas,1,1));

      // Activamos la recepci�n de teclas especiales
      keypad(panel_window(Panel[NMenu]),1);

      // Fijamos el atributo de fondo y texto
      wattron(panel_window(Panel[NMenu]),COLOR_PAIR(1));
      // y dibujamos el borde
      box(panel_window(Panel[NMenu]),ACS_VLINE,ACS_HLINE);

      // Por ahora mantenemos el nuevo men� oculto
      hide_panel(Panel[NMenu]);
      update_panels();
      doupdate();

      NMenu++; // Pasar al siguiente men�
      NOpcion++; // y la siguiente definici�n de opci�n
      NOpc=0;
    }
  }
}

// Esta estructura contiene una serie de pulsaciones
// que van a ser controladas por RecogeTecla() junto
// con las respuestas que devolver� cada una
struct {
  int Pulsacion,Comando;
} Respuesta[] = {
  KEY_UP,REQ_PREV_ITEM,
  KEY_DOWN,REQ_NEXT_ITEM,
  KEY_HOME,REQ_FIRST_ITEM,
  KEY_END,REQ_LAST_ITEM,
  KEY_LEFT,REQ_PREV_ITEM,
  KEY_RIGHT,REQ_NEXT_ITEM,
  8,REQ_CLEAR_PATTERN
};

// Esta funci�n espera una pulsaci�n de tecla en el
// panel indicado, correspondiente al men� actual,
// devolviendo el comando oportuno o el c�digo de la
// tecla si es que no hay coincidencia
int RecogeTecla(PANEL *panel)
{
  int tecla,n;

  // Recuperamos una tecla
  tecla=wgetch(panel_window(panel));

  for(n=0;n<7;n++) // Recorremos las acciones
    // Si hay coincidencia
    if(tecla==Respuesta[n].Pulsacion)
      // devolver el comando
      return Respuesta[n].Comando;

  // en caso contrario devolver la tecla
  return tecla;
}

// Oculta men�s y paneles, liberando los recursos
void LiberaTodo()
{
  int n,m;

  // Ocultar el men� principal
  unpost_menu(Menu[0]);
  hide_panel(Panel[0]);

  update_panels();
  doupdate();

  // Liberar cada uno de los men�s
  for(n=0;n<5;n++) {
    free_menu(Menu[n]);
    // cada panel
    del_panel(Panel[n]);

    // y cada una de sus opciones
    for(m=0;m<5;m++)
      if(Opcs[n][m] != (ITEM *)NULL)
	free_item(Opcs[n][m]);
  }

  // Borrar la pantalla
  werase(stdscr);
  refresh();

  endwin(); // Fin
}

// Esta funci�n es invocada indirectamente cada vez que
// nos movemos por el men�, siendo su finalidad mostrar
// el mensaje o comentario asociado a la nueva opci�n
void MuestraMensaje(MENU *Menu)
{
  char Cadena[256];

  // Recuperar la descripci�n asociada a la opci�n
  strcpy(Cadena,item_userptr(current_item(Menu)));

  // Completar con espacios hasta llenar el ancho
  // de una l�nea de pantalla
  while(strlen(Cadena)<COLS)
    strcat(Cadena, " ");

  // Establecer los colores
  wattron(stdscr,COLOR_PAIR(1));
  // y mostrar la cadena
  mvaddstr(LINES-1,0,Cadena);

  // Actualizamos
  update_panels();
  doupdate();
}

// Funci�n que se encarga de llamar a la funci�n 
// correspondiente a la opci�n seleccionada
void Llama(void (*f)())
{
  f();
}

// Funciones que efectuar�an el proceso correspondiente
// a cada opci�n
void AgendaModificacion()
{
  MensajeEspera("Opci�n de modificaci�n de la agenda");
}

void AgendaImpresion()
{
  MensajeEspera("Opci�n de impresi�n de la agenda");
}

void AgendaBorrado()
{
  MensajeEspera("Opci�n de borrado de la agenda");
}

void VisitasRegistro()
{
  MensajeEspera("Opci�n de registro de visitas");
}

void VisitasInforme()
{
  MensajeEspera("Opci�n de informe de visitas");
}

void ProveedoresRegistro()
{
  MensajeEspera("Opci�n de registro de proveedores");
}

void ProveedoresRelacion()
{
  MensajeEspera("Opci�n de relaci�n de proveedores");
}

void ProveedoresBorrado()
{
  MensajeEspera("Opci�n de borrado de proveedores");
}

void ProveedoresPagos()
{
  MensajeEspera("Opci�n de pagos de proveedores");
}

void ClientesRegistro()
{
  MensajeEspera("Opci�n de registro de clientes");
}

void ClientesRelacion()
{
  MensajeEspera("Opci�n de relaci�n de clientes");
}

void ClientesBorrado()
{
  MensajeEspera("Opci�n de borrado de clientes");
}

void ClientesCobros()
{
  MensajeEspera("Opci�n de cobros de clientes");
}

// Funci�n utilizada por las anteriores y que simplemente
// muestra en pantalla el mensaje y espera una tecla
void MensajeEspera(char *s)
{
  // Mostrar el mensaje
  mvaddstr(1,1,s);
  update_panels();
  doupdate();

  // Y esperar la pulsaci�n de una tecla
  getch();

  // Borrar el mensaje
  wattron(stdscr,COLOR_PAIR(2));
  mvaddstr(1,1,"................................................");
  update_panels();
  doupdate();
}
