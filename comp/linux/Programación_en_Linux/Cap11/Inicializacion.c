#include <ncurses.h>

int main()
{
  // Inicializamos curses
  if(initscr() == NULL) {
    // comprobando un posible fallo
    puts("Fallo al inicializar curses");
    exit(-1);
  }

  noecho(); // Desactivar el eco

  start_color(); // inicializar los colores

  // Mostramos algunos datos en stdscr
  printw("Versi�n de curses: %s\n\r"
	 "%d columnas\n\r"
	 "%d l�neas\n\r"
	 "Tama�o del tabulador: %d\n\r"
	 "%s hay colores\n\r"
	 "%d colores\n\r",
	 curses_version(),
	 COLS, LINES, TABSIZE,
	 has_colors() ? "S�" : "No",
	 COLOR_PAIRS);

  refresh(); // actualizamos

  // Esperamos la pulsaci�n de una tecla
  getch();

  endwin(); // Cerramos curses

  return 0;
}
