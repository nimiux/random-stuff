#include <ncurses.h>
#include <menu.h>
#include <stdio.h>


int main()
{
  // Matriz para cinco punteros ITEM
  ITEM *opcs[5];
  // Puntero para el men�
  MENU *menu;
  int n; // contador
  int tecla,salida;

  // Matriz con los t�tulos de las opciones
  char *Opciones[4] = { 
    "Editar","Imprimir","Exportar","Importar"};

  initscr(); // Inicializamos ncurses

  // Activamos la recepci�n de teclas especiales
  keypad(stdscr,1);

  // Creamos las cuatro opciones facilitando
  // sus t�tulos y una descripci�n nula
  for(n=0;n<4;n++)
    opcs[n]=new_item(Opciones[n],"");

  // A�adimos la quinta opci�n con el valor NULL
  opcs[4]=(ITEM *)NULL;

  // Creamos el men� de opciones
  menu=new_menu(opcs);
  // y lo hacemos visible
  post_menu(menu);

  refresh(); // Actualizamos la visualizaci�n

  do { // Mientras no se pulse ESC

    // Enviamos a menu_driver() la constante
    // adecuada dependiendo de la tecla que se pulse
    salida=menu_driver(menu,tecla=RecogeTecla());

    // Si es un comando desconocido
    if(salida==E_UNKNOWN_COMMAND)
      if(tecla!=27) // y no es ESC
	beep(); // pitar

  } while(tecla!=27);

  // Ocultamos el men�
  unpost_menu(menu);
  refresh();

  // Liberamos el men�
  free_menu(menu);
  // y todas sus opciones
  for(n=0;n<4;n++)
    free_item(opcs[n]);

  endwin(); // Cerramos ncurses

  return 0;
}

int RecogeTecla()
{
  // Recogemos una tecla
  int tecla=getch();

  switch(tecla)  // Dependiendo de la tecla
    {
      // Devolvemos un comando u otro
    case KEY_UP: return REQ_PREV_ITEM;
    case KEY_DOWN: return REQ_NEXT_ITEM;
    case KEY_HOME: return REQ_FIRST_ITEM;
    case KEY_END: return REQ_LAST_ITEM;
    case 8: return REQ_CLEAR_PATTERN;
    default: return tecla;
    }
}
