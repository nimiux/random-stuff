#include <ncurses.h>
#include <menu.h>
#include <stdio.h>


int main()
{
  // Matriz para cinco punteros ITEM
  ITEM *opcs[5];
  // Puntero para el men�
  MENU *menu;
  int n; // contador

  // Matriz con los t�tulos de las opciones
  char *Opciones[4] = { 
    "Editar","Imprimir","Exportar","Importar"};

  // Matriz con los comandos a ejecutar
  int Comandos[] = {
    REQ_NEXT_ITEM,REQ_NEXT_ITEM,REQ_NEXT_ITEM,
    REQ_PREV_ITEM,REQ_PREV_ITEM,REQ_PREV_ITEM,
    REQ_LAST_ITEM,REQ_FIRST_ITEM };

  initscr(); // Inicializamos ncurses

  // Creamos las cuatro opciones facilitando
  // sus t�tulos y una descripci�n nula
  for(n=0;n<4;n++)
    opcs[n]=new_item(Opciones[n],"");

  // A�adimos la quinta opci�n con el valor NULL
  opcs[4]=(ITEM *)NULL;

  // Creamos el men� de opciones
  menu=new_menu(opcs);
  // y lo hacemos visible
  post_menu(menu);

  refresh(); // Actualizamos la visualizaci�n

  // Esperamos la pulsaci�n de una tecla
  getch();

  // Nos desplazamos por las opciones
  for(n=0;n<8;n++) {
    menu_driver(menu,Comandos[n]);
    refresh();
    sleep(1);
  }

  getch();

  // Ocultamos el men�
  unpost_menu(menu);
  refresh();

  // Liberamos el men�
  free_menu(menu);
  // y todas sus opciones
  for(n=0;n<4;n++)
    free_item(opcs[n]);

  endwin(); // Cerramos ncurses

  return 0;
}
