/*
Peticion.c

Demuestra el uso de la biblioteca "form"
*/

#include <form.h>
#include <panel.h>
#include <ncurses.h>
#include <string.h>
#include <stdlib.h>

// Prototipos de funciones
void MuestraMensaje(FORM *);
int RecogeTecla();
void InicializaPantalla();
void ConstruyeFormulario();
void LiberaTodo();

// Posibles opciones del campo SEXO
char *OpsSexo[] = {"Varon","Hembra",(char *)NULL};

// Estructura con datos para crear los campos
struct {
  int Linea,Columna;   // Posici�n de la etiqueta del campo
  int Lineas,Columnas; // Dimensiones del campo
  char *Campo;         // Etiqueta del campo
  char *Comentario;    // Mensaje de ayuda
} Campos[] = {
  2,5,1,40,"Nombre:","Introduzca el nombre del paciente",
  4,5,1,25,"Direccion:","Escriba calle o plaza y n�mero",
  4,50,1,12,"Telefono:","Introduzca el tel�fono",
  6,5,1,6,"Sexo:","H o V o use F10 para seleccionar",
  6,30,1,2,"Edad:","Introduzca la edad, entre 10 y 80 a�os",
  6,50,1,3,"Peso:","Introduzca en peso, entre 30 y 200 kilos",
  8,5,5,40,"Comentario:","Comentario sobre tratamiento"
};

// Catorce campos m�s el �ltimo NULL
FIELD *PCampo[15];
// En un solo formulario
FORM *FichaCliente;

int main()
{
  int Salida,tecla; // Variables auxiliares

  // Inicializamos ncurses
  InicializaPantalla();

  // y construimos el formulario
  ConstruyeFormulario();

  // lo mostramos
  post_form(FichaCliente);
  refresh();

  // y controlamos el proceso de los campos
  do {
    Salida=form_driver(FichaCliente,tecla=RecogeTecla());

    // Si no se reconoce la tecla
    if(Salida==E_UNKNOWN_COMMAND)
      beep();
  } while(tecla!=27); // Hasta que se pulse <Esc>

  // Liberamos los recursos
  LiberaTodo();

  // y terminamos
  endwin();

  return 0;
}

// Estructura con las teclas que puede pulsar el usuario
// y los comandos que se enviar�n al formulario
struct {
  int Pulsacion, // Tecla
    Comando; // Comando
} Respuesta[] = {
           KEY_UP,        REQ_PREV_LINE,
	   KEY_DOWN,      REQ_NEXT_LINE,
	   KEY_LEFT,      REQ_PREV_CHAR,
	   KEY_RIGHT,     REQ_NEXT_CHAR,
	   KEY_HOME,      REQ_BEG_FIELD,
	   KEY_END,       REQ_END_FIELD,
/* <-- */  KEY_BACKSPACE, REQ_DEL_PREV,
/* Supr */ KEY_DC,        REQ_DEL_CHAR,
/* Intro */10,            REQ_NEXT_FIELD,
/* Tab */  9,             REQ_PREV_FIELD,
/* F10 */  KEY_F(10),    REQ_NEXT_CHOICE
};

// Funci�n que recoge una pulsaci�n de tecla y devuelve
// el comando que corresponda o el c�digo de tecla
int RecogeTecla()
{
  int tecla,n;

  // Esperamos la pulsaci�n de tecla
  tecla=getch();

  // Recorremos la matriz de estructuras para encontrar
  // el comando correspondiente
  for(n=0;n<11;n++)
    if(tecla==Respuesta[n].Pulsacion)
      return Respuesta[n].Comando;

  return tecla;
}

// Se encarga de la inicializaci�n de ncurses
void InicializaPantalla()
{
  // Inicializaci�n
  initscr();
  noecho();
  raw();
  keypad(stdscr,1);

  // Preparamos los colores
  start_color();

  init_pair(1,COLOR_WHITE,COLOR_BLUE);
  init_pair(2,COLOR_BLUE,COLOR_WHITE);
  init_pair(3,COLOR_YELLOW,COLOR_BLACK);
}

// Esta funci�n crea la lista de FIELD y el FORM
void ConstruyeFormulario()
{
  int n;

  // Establecemos los atributos por defecto
  set_field_fore((FIELD *)NULL,COLOR_PAIR(1));
  set_field_back((FIELD *)NULL,COLOR_PAIR(2));
  set_field_pad((FIELD *)NULL,'_');

  // Recorremos los 7 campos existentes
  for(n=0;n<7;n++) {
    // Creando un campo que actuar� como etiqueta
    PCampo[n*2]=new_field(1,strlen(Campos[n].Campo),
			  Campos[n].Linea,Campos[n].Columna,0,0);
    // no estar� activo
    field_opts_off(PCampo[n*2],O_ACTIVE);
    // y tendr� por contenido el t�tulo de cada campo
    set_field_buffer(PCampo[n*2],0,Campos[n].Campo);
    set_field_fore(PCampo[n*2],COLOR_PAIR(3));

    // Y creando otro campo que ser� en el que se pida el dato
    PCampo[n*2+1]=new_field(Campos[n].Lineas,Campos[n].Columnas,
			    Campos[n].Linea,Campos[n].Columna +
			    strlen(Campos[n].Campo)+2,0,0);
    // asociamos el mensaje de ayuda con el campo
    set_field_userptr(PCampo[n*2+1],Campos[n].Comentario);
  }

  // El �ltimo campo del formulario indica el final
  PCampo[14]=(FIELD *)NULL;

  // Establecemos ahora los atributos espec�ficos de
  // cada uno de los campos
  set_field_type(PCampo[7],TYPE_ENUM,OpsSexo,FALSE,FALSE);
  set_field_type(PCampo[9],TYPE_INTEGER,2,10,80);
  set_field_type(PCampo[11],TYPE_INTEGER,3,30,200);

  // Modificamos el ajuste de los campos num�ricos
  set_field_just(PCampo[9],JUSTIFY_RIGHT);
  set_field_just(PCampo[11],JUSTIFY_RIGHT);

  // Creamos el formulario
  FichaCliente=new_form(PCampo);

  // y asociamos la funci�n a la que hay que llamar cada
  // vez que se cambie de un campo a otro
  set_field_init(FichaCliente,MuestraMensaje);
}

// Esta funci�n libera todos los recursos asignados
void LiberaTodo()
{
  int n;

  // Liberamos el formulario
  free_form(FichaCliente);

  // y toda la lista de campos
  for(n=0;n<14;n++)
    free_field(PCampo[n]);
}

// Esta funci�n muestra el mensaje asociado al campo
// que acaba de convertirse en actual
void MuestraMensaje(FORM *c)
{
  // Mostramos el mensaje que tiene asociado el campo
  mvaddstr(LINES-5,10,field_userptr(current_field(c)));
  addstr("                                ");
  refresh();
}
