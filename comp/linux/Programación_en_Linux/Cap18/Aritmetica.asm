global main   		
extern printf		

section .data 		
	Mensaje db "El resultado es %d",0Ah,0
	;;  Valor al que vamos a sumar otro
	Valor dd 174

section .text 			
main:
	;;  Sumamos un valor al contenido en Valor
	add dword [Valor], 321
	;;  Ponemos en la fila los datos
	push dword [Valor]
	push dword Mensaje
	;;  y llamamos a printf
	call printf
	;;  extraemos datos
	pop eax
	pop eax		
	ret			; y terminamos
		
	
