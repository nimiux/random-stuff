global main   			;  Definimos como global el s�mbolo main
extern printf			;  Vamos a hacer referencia a una funci�n externa

section .data 			;  Segmento de datos
	;; con una cadena detexto   
	Saludo db "�Hola con ensamblador!",0ah,0

section .text 			;  Segmento de c�digo
main:	push dword Saludo	;  Introducimos en la pila la direcci�n de la cadena
	call printf		;  Llamamos a la funci�n printf
	pop eax			;  Extraemos el c�digo de retorno de printf
	ret			;  y terminamos
		
	
