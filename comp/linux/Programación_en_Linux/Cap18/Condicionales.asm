global main   		
extern printf		

section .data 		
	Mensaje db "Uso de condicionales",0Ah,0
        Contador db 0
	
section .text 			
main:
        mov byte [Contador],0 ; Ponemos a 0 el contador
bucle:	
	push dword Mensaje
	;;  y llamamos a printf
	call printf
	;;  extraemos datos
	pop eax

	inc byte [Contador]	; incrementamos
	cmp byte [Contador],10	; comparamos
	jb bucle 		;  y saltamos
	
	ret			; y terminamos
		
	
