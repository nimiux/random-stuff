#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>

int main()
{
  // Lista de argumentos para el proceso
  char *Parametros[]={"Contador",NULL};
  // Creamos el nuevo proceso
  pid_t PID=fork();

  if(!PID) // Si es el proceso hijo
    // sustituirlo por Contador
    execv("Contador",Parametros);
  else { // en caso contrario
    puts("Espero 5 segundos\n");
    sleep(5); // esperar 5 segundos
    kill(PID,SIGTERM); // e interumpir
    puts("Proceso hijo finalizado\n");
  }

  return 0;
}
