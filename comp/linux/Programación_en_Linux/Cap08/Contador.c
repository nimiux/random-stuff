#include <stdio.h>

int main()
{
  int N=0; // Un contador a 0

  // De manera indefinida
  while(1)
    // mostramos e incrementamos
    printf("%d\t",N++);

  return 0;
}
