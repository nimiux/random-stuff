#include <unistd.h>
#include <sys/types.h>

int main()
{
  pid_t PID; // Para guardar el PID
  int N; // Contador para el bucle

  // Creamos el nuevo proceso
  PID=fork();

  // A partir de este punto hay dos
  // procesos separados, ejecutando
  // por duplicado las siguientes sentencias

  // Si se ha producido un fallo indicarlo
  if(PID==-1) {
    puts("Fallo al crear el proceso\n");
    exit(-1); // y no continuar
  }

  // Un bucle para contar de 0 a 9
  for(N=0;N<10;N++) {
    // escribimos en la consola
    printf("N=%d\n",N);
    // y esperamos de manera aleatoria
    // sleep(rand()%3);
  }

  return 0;
}
