#include <unistd.h>
#include <sys/types.h>

int main()
{
  pid_t PID; // Para guardar el PID

  // Creamos el nuevo proceso
  PID=fork();

  // Detener el proceso padre durante
  // unos segundos
  if(PID) sleep(3);

  // Mostramos el valor obtenido de
  // fork()
  // Esta sentencia la ejecutar� tanto
  // el proceso padre como el hijo
  printf("PID obtenido de fork(): %d\n",
	 PID);

  // Usamos getpid() 
  printf("PID de este proceso: %d\n",
	 getpid());

  // y tambi�n getppid() para obtener
  // el identificador del proceso padre
  printf("PID del proceso padre: %d\n\n",
	 getppid());

  return 0;
}
