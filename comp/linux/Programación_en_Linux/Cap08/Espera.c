#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>

// Funci''on que ejecutar''a
// el proceso hijo
void ProcesoHijo()
{
  int N;

  // Contar''a de 1 a 10
  for(N=1; N<=10; N++) {
    printf(" %d ", N);
    // asegur''andose de enviar la
    // informaci''on a la consola
    fflush(stdout);

    // y con una espera entre ciclos
    sleep(2);
  }
}

// Punto de entrada al programa
int main()
{
  // Creamos el proceso
  pid_t PID=fork();
  // obtenemos una referencia de tiempo
  clock_t Ahora=clock();

  if(!PID) // Si es el proceso hijo
    ProcesoHijo(); // ejecutar ProcesoHijo()
  else { // en caso contrario
    // mostrar un mensaje
    puts("Esperando a que termine el proceso hijo ");

    // e ir vigilando al proceso hijo pero sin 
    // bloquear el proceso padre
    while(!waitpid(PID,NULL,WNOHANG)) 
      // imprimiendo un punto cada segundo
      if((clock()-Ahora)/CLOCKS_PER_SEC>=1) {
	putchar('.');
	fflush(stdout);
	Ahora=clock();
      }
      
    // El proceso ha terminado
    puts("\nEl proceso hijo ha finalizado\n");
  }

  return 0;
}
