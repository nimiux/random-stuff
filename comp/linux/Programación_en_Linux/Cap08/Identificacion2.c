#include <unistd.h>
#include <sys/types.h>

// Funci�n con el proceso que 
// ejecutar� el proceso padre
void ProcesoPadre()
{
  int N;

  // Imprimir la secuencia de 
  // n�meros pares 2-10
  for(N=1; N<=10; N+=2)
    printf("%d\n",N);
}

// Funci�n con el proceso que
// ejecutar� el proceso hijo
void ProcesoHijo()
{
  int N;

  // Imprimir la secuencia de 
  // n�meros impares 1-9
  for(N=2; N<=10; N+=2)
    printf("\t%d\n",N);
}

int main()
{
  pid_t PID; // Para guardar el PID

  // Creamos el nuevo proceso
  PID=fork();

  // Controlamos un posible error
  if(PID==-1) {
    puts("Fallo al crear el nuevo proceso\n");
    exit(-1);
  }

  // Una peque�a espera antes
  sleep(1);

  // de invocar al funci�n adecuada
  PID ? ProcesoPadre() : ProcesoHijo();

  return 0;
}
