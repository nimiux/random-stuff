#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

// M�xima longitud para las cadenas
#define MAX_LENGTH 256

// Vamos a crear dos tuber�as y, por
// tanto, obtener dos parejas de 
// descriptores de archivos
int PadreHijo[2], HijoPadre[2];

// Esta funci�n ser la que ejecute el
// proceso padre
void ProcesoPadre()
{
  // Dos archivos
  FILE *Entrada, *Salida;
  // Una cadena para recoger el resultado
  char Resultado[MAX_LENGTH];

  // Vamos indicando los distintos pasos mostrando
  // mensajes por la salida est�ndar
  puts("-- Proceso padre --\n");

  // Cerramos los descriptores de archivos
  // que no vamos a necesitar
  close(PadreHijo[0]);
  close(HijoPadre[1]);

  // Identificamos los descriptores que utilizar�
  // el proceso padre
  printf("-- PADRE: Descriptores (%d,%d) --\n",
	 HijoPadre[0],PadreHijo[1]);

  // Abrimos la salida hacia el hijo
  Salida=fdopen(PadreHijo[1],"w");

  if(!Salida) { // comprobamos que no hay problema
    puts("-- PADRE: Fallo en la apertura --\n");
    return;
  }

  // La tuber�a ya est� preparada
  puts("-- PADRE: Tuberias preparadas --\n");

  // Enviamos el dato sobre el que debe operar
  if(fputs("24",Salida)==EOF) {
    puts("-- PADRE: Fallo al escribir --\n");
    return;
  }

  // y cerramos
  fclose(Salida);

  puts("-- PADRE: Enviado el dato --\n");

  // Abrimos la tuber�a de la que recogeremos
  // el resultado devuelto por el hijo
  Entrada=fdopen(HijoPadre[0],"r");

  // Leemos ese resultado
  fgets(Resultado,MAX_LENGTH,Entrada);
  // y lo mostramos
  puts("-- PADRE: Obtenido el resultado --\n");
  printf("\n\nResultado=%s\n\n",Resultado);

  fclose(Entrada); // cerramos y terminamos
}

// Esta funci�n ser� la que ejecute el
// proceso hijo
void ProcesoHijo()
{
  // Dos archivos
  FILE *Entrada, *Salida;
  // Y un entero
  int N;

  // Vamos mostrando indicaciones del proceso
  // mediante mensajes por la consola
  puts("-- Proceso hijo --\n");

  // Cerramos los descriptores que no
  // vamos a necesitar
  close(PadreHijo[1]);
  close(HijoPadre[0]);

  // Identificamos los que s� usaremos
  printf("-- HIJO: Descriptores (%d,%d) --\n",
	 PadreHijo[0],HijoPadre[1]);

  // Abrimos la tuber�a de lectura del padre
  Entrada=fdopen(PadreHijo[0],"r");

  if(!Entrada) { // Comprobando un posible error
    puts("-- HIJO: Fallo en la apertura --\n");
    return;
  }

  puts("-- HIJO: Tuberias preparadas --\n");

  // Leemos de la tuber�a un n�mero entero
  if(!fscanf(Entrada," %d", &N)) {
    puts(" HIJO: Fallo al leer el dato --\n");
    return;
  }

  // y cerramos
  fclose(Entrada);

  puts("-- HIJO: Leido el dato --\n");

  // Abrimos ahora la tuber�a de escritura
  Salida=fdopen(HijoPadre[1],"w");
  // para enviar el resultado al padre
  fprintf(Salida,"- %d -",N*N);

  puts("-- HIJO: Devuelto el resultado --\n");

  fclose(Salida); // Cerramos y salimos
}

// Punto de entrada al programa
int main()
{
  pid_t PID;

  // Creamos las dos tuber�as
  if(pipe(PadreHijo) || pipe(HijoPadre)) 
    // controlando un posible error
    puts("Fallo al crear la tuber�a\n");
  else { // si todo va bien
    PID=fork(); // creamos el nuevo proceso
    // y desviamos la ejecuci�n a la 
    // funci�n que corresponda
    PID ? ProcesoPadre() : ProcesoHijo();
  }  

  return 0;
}
