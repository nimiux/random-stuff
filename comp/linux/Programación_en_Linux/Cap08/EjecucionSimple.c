#include <stdlib.h>

int main()
{
  int Retorno;

  // Comprobamos si hay disponible un procesador
  // de comandos, normalmente sh
  if(system(NULL))
    puts("Hay disponible un procesador de comandos\n");
  else {
    puts("No hay disponible un procesador de comandos\n");
    exit(-1);
  }
 
  // Ejecutamos el comando date, que
  // mostrar� la fecha y hora por consola
  if((Retorno=system("date")) == -1)
    puts("Fallo en la creaci�n del proceso\n");
  else
    printf("Valor de retorno: %d\n", Retorno);

  return 0;
}
