#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>

int main()
{
  int Retorno;

  // Creamos el nuevo proceso
  pid_t PID=fork();
  // Lista de parámetros que facilitaremos
  // a la función execv()
  char *Parametros[] = {"ls", "-l", NULL};

  // Si no se ha producido un error
  if(PID!=-1)
    // Si este proceso es el hijo
    if(!PID) {
      // ejecutamos el comando ls
      execvp("ls",Parametros);

      // Si se llega a esta sentencia es que no se
      // ha sustituido la imagen del programa y, por
      // tanto, la llamada a execv() ha fallado
      puts("Fallo en el proceso hijo\n");
      // salimos
      _exit(EXIT_FAILURE);
    } else { // Si este proceso es el padre
      // Mostramos el PID del padre y el hijo
      printf("El proceso padre tiene el PID: %d\n",
	   getpid());
      printf("Y el proceso hijo el PID: %d\n", PID);

      // Esperamos que termine el proceso hijo
      wait(&Retorno);
      // y mostramos el valor de retorno
      printf("Valor devuelto por el proceso: %d\n", Retorno);

    }
  else
    puts("Fallo al crear el proceso\n");

  return 0;
}
